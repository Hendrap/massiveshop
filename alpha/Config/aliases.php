<?php 
return [
	'AlphaController' => 'Alpha\Core\AlphaController',
	'Acl' => 'Spekkionu\ZendAcl\Facades\Acl',
    'Form' => 'Collective\Html\FormFacade',
    'Html' => 'Collective\Html\HtmlFacade',
    'LaravelAnalytics' => 'Spatie\LaravelAnalytics\LaravelAnalyticsFacade'
];