<?php 

	return [
		
		'subfolder' => false,

		'profiler' => true,

		'urlasset' => 'public/',

		'locales' => ['en' => 'English'],

		'default_locale' => 'en',

		'fallback_locale' => 'en',

		'whitelist' => ['127.0.0.1'],

		'blacklist_pages' => []
	];