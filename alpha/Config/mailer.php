<?php 

return [

	'sender' => ['mailclientdeveloper@gmail.com' => 'Mail Client'],
	//default transport => native,ini pakai php mail
	//kalau mau pakai yg lain lihat list transport
	'transport' => 'gmail',
	
	//type => native : php mail
	//		  smtp : smtp :)
	//		  local : kirim email dari server local
	'type'		=> 'smtp',

	//list transport
	'transports' => [
		'exim' =>'/usr/sbin/exim -bs',

		'gmail' => [
			'host' =>'smtp.gmail.com',
			'port' => 587,
			'encryption' =>'tls',
			'username' => 'mailclientdeveloper@gmail.com',
			'password' => 'antimage'
		],

	],

];