<?php

namespace Alpha\Controllers\Admin;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;
use \Entry as Entries;
use \Logger;
use \Comment as Comments;

class Comment extends AlphaController
{
	public function view(Request $req)
	{
		
		$this->layout->title = setPageTitle("Comments");
		$this->layout->content = view('alpha::admin.comment.index');
	}

	public function getData(Request $req)
	{
		Logger::info("View Taxonomy : ".$type);

        //container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');

        $cols = ['comment','date','status','action'];
        $base = [];
        foreach ($cols as $key => $value) {
            $base[$value] = "";
        }

        $comments = Comments::where('status','!=','deleted');
        $recordsTotal = $comments->count();

        if(!empty($search['value']))
        {
            $comments = $comments->where(function($q)use($search){
                $q->where('content','like','%'.$search['value'].'%');
            });
        }

        $recordsFiltered = $comments->count();

        $cols = ['content','created_at','status'];
        $order = $req->input('order');
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $comments = $comments->orderBy($col,$order[0]['dir']);
            }else{
                $comments = $comments->orderBy('created_at','desc');
            }
        }else{
            $comments = $comments->orderBy('created_at','desc');
        }

         $comments = $comments->take($take)->skip($start)->with(['user','entry'])->get();
         foreach ($comments as $key => $value) {
         	$item = $base;
         	$html = "";
         	$html .= view('alpha::admin.comments.templates.comment',['comment'=>$value]);
         	$item['html'] = $html;
         	$data[] = $item;
         }



	}

}
