<?php

namespace Alpha\Controllers\Admin;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Taxonomy as Taxonomies;
use \Entry as Entries;
use \Media as Medias;
use \Logger;
use Closure;
use Exception;
use Auth;

class Media extends AlphaController
{
	public function entryMedia(Request $req){
		$media = Medias::orderBy('created_at','desc');
		if(isset($_GET['editor'])){
			$media = $media->where('media_type','=','image');
		}

		if(isset($_GET['search'])){
			$keyword = $_GET['search'];
			$media = $media->where('title','like',"%$keyword%");
		}
		if(!empty($req->medias))
		{
			$media = $media->whereIn('id',$req->medias);
		}
		
		$media = $media->paginate(30);
		if(!empty($_GET)){
			foreach ($_GET as $key => $value) {
				$media = $media->appends([$key => $value]);
			}
		}
		$res = parseMedias($media);

		Logger::info("Open Pop up media");

		$paginate = (string)view('alpha::admin.media.templates.paginator',['paginator'=>$media]);
		return json_encode(array('paginate'=>$paginate,'res'=>$res));
	}
	public function delete(){
		$mediaId = app('request')->input('id');
		$media = Medias::find($mediaId);
		if(empty($media)) return redirect()->back();
		
		if($media->media_type == 'image'){
			$profiles = app('AlphaSetting')->images;
			foreach ($profiles as $key => $value) {
				$path = getCropImage($media->path,$value['name']);
				@unlink(base_path('public/'.$path));
			}
		}

		Logger::info("Delete Media ".$media->title);

		@unlink(base_path('public/'.$media->path));
		$media->delete();
		return redirect()->back()->with('msg','Data deleted!');
	
	}
	public function embedVideo(){

		$url = app('request')->input('url');
		$infoVideo = checkVideoURL($url);
		$imgPath = '';
		$title = '';

		if($infoVideo['type'] == 'youtube'){
			$imgPath = getImageFromEmbedVideo('youtube',$infoVideo);
			$title = $infoVideo["parsed_param"]["v"];
		}
		if($infoVideo['type'] == 'vimeo'){
			$infoVideo = @json_decode(file_get_contents("http://vimeo.com/api/oembed.json?url=".urlencode($url)), true);
			$imgPath = getImageFromEmbedVideo('vimeo',$infoVideo);			
			$title = $infoVideo["video_id"];
		}

		$title .= ' - '.time();

		$media = new Medias();
		$media->title = $title;
		$media->media_type = 'video';
		$media->url = $url;
		$media->path = $imgPath;
		$media->author = app('AdminUser')->user->id;
		$media->save();

		
		Logger::info("Create Embed Media ".$media->title);


		return json_encode(array('status'=>1));


	}
	public function uploader($type = 'image',Closure $callback){
		
		$this->layout = "";
		$req = app('request');
		$file = @$req->file($type)[0];
		$info = [];
		$info  = pathinfo($_FILES[$type]['name'][0]);
		$info['size'] = $file->getClientSize();
		$info['mime_type'] = $file->getClientMimeType();
		
		$extension = $file->getClientOriginalExtension();
		if($type != 'other'){
			if(!in_array($extension, \Config::get('alpha.media.'.$type))) throw new Exception("Invalid file format", 1);
			
		}

		$title = str_replace(' ','-',$info['filename']);
		$name = $title.time().'.'.$extension;	
		$dest = str_replace('\\','/',base_path().'/public/uploads/'.date('Y').'/'.date('m'));

		if(!is_dir($dest)) mkdir($dest,0777,true);

		$file->move($dest,$name);
		
		$media = new Medias();
		$media->title = $title;
		$media->description = '';
		$media->media_type = $type;
		$media->media_mime = $file->getClientMimeType();
		$media->media_meta = serialize($info);
		$media->url = '';
		$media->path = 'uploads/'.date('Y').'/'.date('m').'/'.$name;
		$media->author = app('AdminUser')->user->id;
		$media->save();

		Logger::info("Upload Media ".$media->title);


		$params = [];
		$params['title'] = $title;
		$params['file'] = $file;
		$params['info'] = $info;
		$params['name'] = $name;
		$params['media'] = $media;

		$callback($params);

		return $media;

	}
	public function video(Request $req){

		Logger::info("View Video(Media Library)");

		$search = $req->input('search','');
		$videos = Medias::where('media_type','=','video')->where('title','like',"%$search%")->orderBy('created_at','desc')->paginate(30);
		$this->layout->activeParent = 'main_media_library';
        $this->layout->active = 'media_'.'video';

		$this->layout->title = setPageTitle("Video");
		$this->layout->content = view('alpha::admin.media.media',[
			'medias' => $videos,
			'media_type' => 'video',
			'search' => $search
			]);
	}
	public function uploadVideo(){
		try {
			$response = $this->uploader('video',function($params){
				
			});
			$html = "";
			$html .= newLine(view('alpha::admin.media.templates.list-media',['medias'=>[$response],'media_type'=>'video']));
			return json_encode(array('status'=>1, 'res' => $html));		
		} catch (Exception $e) {
			return json_encode(array('status'=>0,'msg'=>$e->getMessage()));	
		}
	}
	public function other(Request $req){
		Logger::info("View Media Library(Other)");

		$search = $req->input('search','');
		$others = Medias::where('media_type','=','other')->where('title','like',"%$search%")->paginate(30);

		$this->layout->activeParent = 'main_media_library';
        $this->layout->active = 'media_'.'other';


		$this->layout->title = setPageTitle("Other");	
		$this->layout->content = view('alpha::admin.media.media',[
			'medias' => $others,
			'media_type' => 'other',
			'search' => $search
			]);
	}
	public function uploadOther(){
		try {
			$this->uploader('other',function($params){
				
			});
			return json_encode(array('status'=>1));		
		} catch (Exception $e) {
			return json_encode(array('status'=>0,'msg'=>$e->getMessage()));	
		}
	}
	public function audio(Request $req){
		Logger::info("View Audio(Media Library)");

		$search = $req->input('search','');

		$this->layout->activeParent = 'main_media_library';
        $this->layout->active = 'media_'.'audio';
		$this->layout->title = setPageTitle("Audio");
		$this->layout->content = view('alpha::admin.media.media',[
			'medias' => [],
			'media_type' => 'audio',
			'search' => $search
			]);
	}
	
	public function getColsAudio()
	{
		return ['obj','name','created_at','updated_at','file_size','action'];
	}

	public function uploadAudio(){
		try {
			$media = $this->uploader('audio',function($params){
				
			});

			$cols = $this->getColsAudio();
	        $item = [];
	        foreach ($cols as $col) {
	            $item[$col] = '';
	        }

			$html = '';
			$html .= view('alpha::admin.media.grid.entry',['entry'=>$media,'type'=>'audio']);
			$item['html'] = $html;

			return json_encode(array('status'=>1,'item'=>$item));		
		} catch (Exception $e) {
			return json_encode(array('status'=>0,'msg'=>$e->getMessage()));	
		}
	}

	public function image(Request $req)
	{
		Logger::info("View Media Library(Image)");

		$search = $req->input('search','');
		$medias = Medias::with(['user'])->where('media_type','=','image')->where('title','like',"%$search%")->orderBy('id','desc')->paginate(30);

		$this->layout->activeParent = 'main_media_library';
        $this->layout->active = 'media_'.'image';
		$this->layout->title = setPageTitle("Image");
		$this->layout->content = view('alpha::admin.media.media',[
			'medias' => $medias,
			'media_type' => 'image',
			'search' => $search
			]);
	}

	public function uploadImage()
	{
		try {
			$response = $this->uploader('image',function($params){
				extract($params);
				$crops = app('AlphaSetting')->images;
				foreach ($crops as $key => $value) {
					$crop = false;
					$ratio = false;
					if($value['crop'] == 'yes'){
						$crop = true;
					}
					if(@$value['aspectratio'] == 'yes'){
						$ratio = true;
					}

					cropImage('uploads/'.date('Y').'/'.date('m').'/'.$name,$value['name'],$value['width'],$value['height'],$crop,$ratio);
				}
			});
			$html = "";
			if(isset($_GET['popup']))
			{
				$html .= parseMedias([$response]);
			}else{
				$html .= newLine(view('alpha::admin.media.templates.list-media',['medias'=>[$response],'media_type'=>'image']));			
				
			}
			return json_encode(array('status'=>1, 'res' => $html));		
		} catch (Exception $e) {
			return json_encode(array('status'=>0,'msg'=>$e->getMessage()));		
		}
		
	}

	public function saveDescription()
	{

		$input = \Request::all();
		// dd($input);
		$update = Medias::find($input['idm']);
		$update->title = $input['titlem'];
		$update->description = $input['descm'];
		$update->link = @$input['link'];
		$update->touch();
		$update->save();
		

		$data = Medias::find($input['idm']);

		Logger::info("Update Media Library : ".$data->title);

		//return redirect()->back();
		return response()->json($data);
	}

	public function getData($type = 'type',Request $req)
    {
        //container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');
        //categgory
        $selectedCategory = '';
        $category = $req->input('columns');
        if(!empty($category[7]['search']['value']))
        {
            $selectedCategory = $category[7]['search']['value'];
        }
      

       
        //total records
        $base = new Medias();

        $recordsTotal = $base->where('media_type',$type);
        
        $recordsTotal = $recordsTotal->count();

        //filter entry
        $entries =$base->where('media_type',$type);


        if(!empty($search['value']))
        {
            $entries = $entries->where(function($q)use($search){
                $q->where('medias.title','like','%'.$search['value'].'%');
                $q->orWhere('medias.description','like','%'.$search['value'].'%');
            });
        }
        //total entry yg kefilter
        $recordsFiltered = $entries->count();
        //ambil data

        $entries = $entries->take($take)->skip($start);
        //sorting
        $cols = ['obj','title','created_at','updated_at','id'];
        $order = $req->input('order');
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $entries = $entries->orderBy($col,$order[0]['dir']);
            }else{
                $entries = $entries->orderBy('medias.created_at','desc');
            }
        }else{
            $entries = $entries->orderBy('medias.created_at','desc');
        }
        

        $entries = $entries->get();
        
        $cols = $cols = $this->getColsAudio();
        $base = [];
        foreach ($cols as $col) {
            $base[$col] = '';
        }

         $view = 'alpha::admin.media.grid.entry';
          

        foreach ($entries as $key => $value) {
            $item = $base;
            $item[] = $value;
            $html = '';
            $html .= view($view,['entry'=>$value,'type'=>$type]);
            $item['html'] = $html;

            $data[] = $item;


        }


        Logger::info("View Media : ".$type);

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);
    }
}	
