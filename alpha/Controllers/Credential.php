<?php 

namespace Alpha\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Logger;

/**
* 
*/
class Credential extends \AlphaController
{
	
	function __construct()
	{
		$this->layout = view('alpha::admin.public-layout');
	}
	
	public function login()
	{
		//32 adalah backend managed lihat database ,table rules
		if(\Auth::user() && app('Alpha\Core\AlphaAcl')->multiAccess([32])){
			return redirect()->route('alpha_admin_index');	  
		}


		Logger::info("Visit Login Page");
		$this->layout->title = setPageTitle("Log In");
		$this->layout->content = view('alpha::admin.login');
		
	}

	public function forgotPassword(Request $req)
	{

		Logger::info('Visit Forgot Password Page');
		$this->layout->title = setPageTitle("Forgot Password");
		$this->layout->content = view('alpha::admin.forgot-password');

	}


	public function postForgotPassword(Request $req)
	{
		// $this->validate($req,[
		// 		'email' => 'required|email'
		// 	]);

		$v = \Validator::make($req->input(),[
				'email' => 'required|email'
			]);
		
		$errors = [];

		if ($v->fails()){
			foreach ($v->errors()->messages() as $key => $value) {
				foreach ($value as $v) {
					$errors[$key] = $v;
				}
			}
	        return redirect()->back()->with('errors',$errors);
	    }

		$user = \User::whereEmail($req->email)->first();
		if(empty($user)){
			$errors = ['Account not found!'];
			return redirect()->back()->with('errors',$errors);
		}

		$newPassword = Str::random(6);

		$user->password = bcrypt($newPassword);
		$user->save();

		$email = $user->email;
		$html = '<p>Your new password is '.$newPassword.'</p>';
		$message = app('AlphaMailMessage')
		                    ->setSubject('Forgot Password')
		                    ->setBody($html,'text/html')
		                    ->setFrom(array('alphamailer2016@gmail.com' => setPageTitle("Forgot Password")))
		                    ->setTo(array($email));
		$result = app('AlphaMailSender')->send($message);


		return redirect()->route('alpha_success_password_recovery');
	}

	public function successForgotPassword(Request $req)
	{
		$this->layout->title = setPageTitle("Password Recovery Success");
		$this->layout->content = view('alpha::admin.success-forgot-password');
	}

	public function logout()
	{

		Logger::info("User Logout");

		\Auth::logout();
		 return redirect()->route('alpha_get_login');
	}

	public function postLogin()
	{

		$errors = [];
		$input = \Request::all();

		$rules = [
		'email' => 'required|email',
		'password' => 'required',
		];
	
		$v = \Validator::make($input,$rules);

		if ($v->fails()){
			foreach ($v->errors()->messages() as $key => $value) {
				foreach ($value as $v) {
					$errors[$key] = $v;
				}
			}
	        return redirect()->back()->with('errors',$errors);
	    }

	    $user = \User::whereEmail($input['email'])->first();
	    if(empty($user)) return redirect()->back()->with('errors',['User not found!']);
      	if($user->status != 'active') return redirect()->back()->with('errors',['Account is '.$user->status]);
	    //buang csrf token
	    unset($input['_token']);


	    //ambil remember
	    $remember = @$input['remember'] or false;
	    unset($input['remember']);

	    //ambil redirect
	    $redirect = @$input['redirect'] or false;
	    unset($input['redirect']);

	    if(\Auth::attempt($input,$remember)){
	    	
	    	Logger::info("Success Login");

	    	
	    	$user->last_login = date('Y-m-d H:i:s');
	    	$user->save();
	    	//redirect ke tempat yg mau dituju sebelumnya
	    	if($redirect){
		    	return redirect()->away($redirect);	    		
	    	}

	    	return redirect()->route('alpha_admin_index');	  

	    }else{

	    	Logger::info("Login Failed");

	    	$errors['email'] = 'Password & email doesn\'t match';
	    	return redirect()->back()->with('errors',$errors);

	    }

	}
}