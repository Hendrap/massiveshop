<?php 

namespace Alpha\Controllers;
use \Logger;
use LaravelAnalytics;
/**
* 
*/
class Dashboard extends \AlphaController
{
	
	function __construct()
	{
		parent::__construct();
		$this->layout->activeParent = 'dashboard';
		$this->layout->active = 'dashboard';
	}
	public function index(){
		
		Logger::info("View Dashboard");

		$isGAEnabled = false;
		if(!empty(config('laravel-analytics.siteId'))){
			$isGAEnabled = true;
		}

		$topBrowser = [];
		$originalTopBrowser = [];
		$topVisit = [];


		$pageView = [];
		$siteVisit = [];

		$siteVisitToday = 0;
		$siteVisitThisWeek = 0;
		$siteVisitThisMonth = 0;
		$siteVisitAverage = 0;

		$pageViewsToday = 0;
		$pageViewsThisWeek = 0;
		$pageViewsThisMonth = 0;
		$pageViewsAverage = 0;

		if($isGAEnabled)
		{
			$topBrowser = LaravelAnalytics::getTopBrowsers(30,4);
			$originalTopBrowser = $topBrowser;			

			$labelBrowser = [];
			$valueBrowser = [];
			foreach ($topBrowser as $key => $value) {
				$labelBrowser[] = $value['browser'];
				$valueBrowser[] = $value['sessions'];
			}
			$topBrowser = (object)[
				'labels' => $labelBrowser,
				'series' => $valueBrowser
			];


			$topVisit = LaravelAnalytics::getMostVisitedPages(30);

			$visitorPageView = LaravelAnalytics::getVisitorsAndPageViews(30);
			$visitorPageViewThisWeek = LaravelAnalytics::getVisitorsAndPageViews(6);

			foreach ($visitorPageView as $key => $value) {
				$pageViewsThisMonth += $value['pageViews'];
				$siteVisitThisMonth += $value['visitors'];
			}

			$pageViewsAverage = round($pageViewsThisMonth / 30);
			$siteVisitAverage = round($siteVisitThisMonth / 30);

			foreach ($visitorPageViewThisWeek as $key => $value) {
				$pageViewsThisWeek += $value['pageViews'];
				$siteVisitThisWeek += $value['visitors'];
			}

			foreach ($visitorPageViewThisWeek as $key => $value) {

				if($value['date']->format('d/m/Y') == date('d/m/Y'))
				{
					$pageViewsToday = $value['pageViews'];
					$siteVisitToday = $value['visitors'];
				}

				$pageView[] = (object)[
					'date' => $value['date']->format('m/d/y'),
					'alpha' => $value['pageViews']
				];

				$siteVisit[] = (object)[
					'date' => $value['date']->format('m/d/y'),
					'alpha' => $value['visitors']
				];
			}

		}

		$entries = [];
		$totalContent = 0;
		$medias = [];
		$entries['Pages'] = \Entry::whereEntryType('page')->where('status','!=','deleted')->count();
		foreach (app('AlphaSetting')->entries as $key => $value) {
			if($value['show_ui'] == 'yes' && $value['slug'] != 'page')
			{
				$entries[$value['plural']] = \Entry::whereEntryType($value['slug'])->where('status','!=','deleted')->count();
				if($value['slug'] == 'slider')
				{
					$medias[$value['plural']] = $entries[$value['plural']];
				}
			}
		}

		foreach ($entries as $key => $value) {
			$totalContent += $value;
		}

		$varMedias = ['image' => 'Images','audio' => 'Audio','video' => 'Videos'];
		$totalMedia = 0;
		foreach ($varMedias as $key => $value) {
			$medias[$value] = \Media::whereMediaType($key)->count();
		}

		foreach ($medias as $key => $value) {
			$totalMedia += $value;
		}


		

		$roles = \Role::with(['users'=>function($q){
			$q->where('users.id','!=',1);
			$q->where('users.status','!=','deleted');
		}])->where('id','!=',1)->get();

		$userByRole = array();

		foreach ($roles as $key => $value) {
			$userByRole[] = (object)array('legendLabel'=>$value->name,'magnitude'=>count($value->users));
		}
		$user = 0;
		foreach ($userByRole as $key => $value) {
			$user += $value->magnitude;
		}
		
		$browserColors = [
          'd70206',
          'f05b4f',
          'f4c63d',
          'd17905'
       ];

		$this->layout->title = setPageTitle("Dashboard");	
		$this->layout->content = view('alpha::admin.index',[
									'userByRole'=>$userByRole,
									'user'=>$user,
									'entries' => $entries,
									'medias'=>$medias,
									'totalMedia' => $totalMedia,
									'userByRole' =>$userByRole,
									'totalContent' => $totalContent,
								 	// google analytics
									'isGAEnabled' => $isGAEnabled,
									'topBrowser' => $topBrowser,
									'originalTopBrowser' => $originalTopBrowser,
									'topVisit' => $topVisit,
									'pageView' => $pageView,
									'siteVisit' => $siteVisit,
									'siteVisitToday' => $siteVisitToday,
									'siteVisitThisWeek' => $siteVisitThisWeek,
									'siteVisitThisMonth' => $siteVisitThisMonth,
									'siteVisitAverage' => $siteVisitAverage,
									'pageViewsToday' => $pageViewsToday,
									'pageViewsThisWeek' => $pageViewsThisWeek,
									'pageViewsThisMonth' => $pageViewsThisMonth,
									'pageViewsAverage' => $pageViewsAverage,
									'browserColors' => $browserColors
								]);
	}
	function getTimeElapsedAsText($iTime0, $iTime1 = 0)
	{
		if ($iTime1 == 0) { $iTime1 = time(); }
		$iTimeElapsed = $iTime1 - $iTime0;

		if ($iTimeElapsed < (60)) {
		    return "Less than a minute ago";
		} else if ($iTimeElapsed < (60*60)) {
		    $iNum = intval($iTimeElapsed / 60); $sUnit = "minute";
		} else if ($iTimeElapsed < (24*60*60)) {
		    $iNum = intval($iTimeElapsed / (60*60)); $sUnit = "hour";
		} else if ($iTimeElapsed < (30*24*60*60)) {
		    $iNum = intval($iTimeElapsed / (24*60*60)); $sUnit = "day";
		} else if ($iTimeElapsed < (365*24*60*60)) {
		    $iNum = intval($iTimeElapsed / (30*24*60*60)); $sUnit = "month";
		} else {
		    $iNum = intval($iTimeElapsed / (365*24*60*60)); $sUnit = "year";
		}

		return $iNum . " " . $sUnit . (($iNum <= 1) ? "s" : "") . " ago";
	}
}