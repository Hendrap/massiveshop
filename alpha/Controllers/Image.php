<?php

namespace Alpha\Controllers;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Setting as Settings;
use \Logger;
use \Auth;

class Image extends AlphaController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        parent::__construct();
        //setting area for developer
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([1])){
                abort(404, 'Unauthorized action.');
        }
        $this->layout->activeParent = 'main_setting';
        $this->layout->active = 'setting_image_type';

    }
    public function index()
    {
        //
        Logger::info("View Image Type Setting");
        $this->layout->title = setPageTitle("Image Types");   
        $crops = Settings::where('bundle','alpha.image')->get();
        $this->layout->content = view('alpha::admin.setting.image.index',[
            'crops' => $crops
            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //\
        Logger::info("Create Image Type Setting");

        $this->layout->title = setPageTitle("Create Image Type");   
        $this->layout->content = view('alpha::admin.setting.image.createEdit',[
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = \Request::all();
        
        $this->validate($request,[
            'name' => 'required|alpha_num',
            'width' => 'required|integer',
            'height' => 'required|integer'
        ]);

        unset($input['_token']);
        $setting = Settings::find($input['id']);
        if($input['id'] == 0 || empty($setting)){
            $setting = new Settings();
        }


        $check = Settings::where('id','!=',$input['id'])->whereSettingKey('image_'.strtolower($input['name']))->count();
        if($check > 0){
             return redirect()->back()->with('msg','Name has been taken before !');
        }

        $setting->autoload = 'yes';
        $setting->bundle = 'alpha.image';
        $setting->setting_key = 'image_'.strtolower($input['name']);
        unset($input['id']);
        $setting->setting_value = serialize($input);
        $setting->modified_by = Auth::user()->id;
        $setting->save();
       
        Logger::info("Save Image Type Setting");

         return redirect()->route('admin.setting.image.edit',[$setting->id])->with('msg','Data Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting = Settings::with(['user','user.metas'])->find($id);
        if(empty($setting)) abort(404);

        Logger::info("Edit Image Type Setting");

        $this->layout->title = setPageTitle("Edit Image Type");   
        $this->layout->content = view('alpha::admin.setting.image.createEdit',[
            'setting'=>$setting,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Logger::info("Delete Image Type Setting");

        Settings::find($id)->delete();
        return redirect()->back()->with('msg','Data Deleted!');
    }
}
