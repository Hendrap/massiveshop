<?php 

namespace Alpha\Controllers;
use Illuminate\Http\Request;
use \Logger;
use \Auth;

/**
* 
*/
class Setting extends \AlphaController
{
	
	function __construct()
	{
		parent::__construct();
		$this->layout->activeParent = 'main_setting';
	}

    public function getRules($type = 'default')
    {
        $rules = [];
        switch ($type) {
            case 'site_desc':
                $rules = ['site_name'=>'required','site_tagline'=>'required','site_desc'=>'required'];
                break;
            case 'contact':
                $rules = ['general_contact'=>'email','sales_department'=>'email','customer_service'=>'email','technical_support'=>'email'];
                break;
        }

        return $rules;
    }


	public function getPlugins(){

		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33]))
		{
               abort(404, 'Unauthorized action.');
        }

        Logger::info("View Plugins");


        $plugins = app('AlphaPlugin')->plugins;
        $providers = array();
        
        if(!empty($plugins))
        {
        	foreach ($plugins as $key => $value) {
        		$providers[] = $value->provider;
        	}
        }

        $parsedSettings = array();

        if(!empty($providers))
        {

	        $settings = \Setting::whereIn('setting_key',$providers)->whereBundle('alpha.plugins')->get();

	        foreach ($settings as $key => $value) {
	        	$parsedSettings[$value->setting_key] = $value;
	        }

        }

        if(empty($plugins)) $plugins = array();

        $this->layout->activeParent = 'main_plugin';
        $this->layout->active = 'plugins';
        $this->layout->title = setPageTitle("Plugins");	
		$this->layout->content = view('alpha::admin.setting.plugins',[
			'plugins' => $plugins,
			'parsedSettings' => $parsedSettings,
			]);

	}
	public function getStatusPlugins($id,$status){

		if(!app('Alpha\Core\AlphaAcl')->multiAccess([33])){
            abort(404, 'Unauthorized action.');
        }

         Logger::info("Change Plugin Status");


        $setting = \Setting::find($id);
        if(!empty($setting)){
        	$setting->setting_value = $status;
        	$setting->save();
        }
        return redirect()->back()->with('msg','Data Saved!');
	}

    public function freeShipping()
    {
        $this->layout->title = setPageTitle("Free Shipping");
        $this->layout->active = "setting_free_shipping";
        $this->layout->content = view('alpha::admin.setting.free_shipping');
    }

    public function currency()
    {
        $this->layout->title = setPageTitle("Currency");
        $this->layout->active = "setting_currency";
        $this->layout->content = view('alpha::admin.setting.currency');
    }

    public function dhl()
    {
        $this->layout->title = setPageTitle("DHL");
        $this->layout->active = "setting_dhl";
        $this->layout->content = view('alpha::admin.setting.dhl');
    }

    public function doku()
    {
        $this->layout->title = setPageTitle("Doku");
        $this->layout->active = "setting_doku";
        $this->layout->content = view('alpha::admin.setting.doku');
    }

	public function getGeneral(){

		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(404, 'Unauthorized action.');
        }

        Logger::info("View General Setting");

		$settings = [];
		$varsGeneralSettings = ['site_name','site_slogan','site_description','site_keywords'];
		foreach ($varsGeneralSettings as $key => $value) {
			$settings[$value] = app('AlphaSetting')->getSetting($value);
		}
		
		$this->layout->activeParent = 'general_setting';
        $this->layout->active = 'seo';


		$this->layout->title = setPageTitle("Site Description");	
		$this->layout->content = view('alpha::admin.setting.general',[
			'settings' => $settings
			]);
       
    
	}
	public function getLabels(){
		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(404, 'Unauthorized action.');
        }

        Logger::info("View Label Setting");

        $this->layout->active = 'setting_labels';
        $this->layout->activeParent = 'main_setting';
        $this->layout->title = setPageTitle("Setting Labels");	
		$this->layout->content = view('alpha::admin.setting.label',[]);

	}
	public function getExtras(){
		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(404, 'Unauthorized action.');
        }
        Logger::info("View Extras Setting");

        $this->layout->active = 'setting_extras';
        $this->layout->activeParent = 'main_setting';

        $this->layout->title = setPageTitle("Setting Extras");	
		$this->layout->content = view('alpha::admin.setting.extra',[]);

	}
	public function getContacts(){
		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(404, 'Unauthorized action.');
        }
        Logger::info("View Contacts Setting");
        
        $this->layout->activeParent = 'general_setting';
        $this->layout->active = 'contact';


        $this->layout->title = setPageTitle("Site Contact");	
		$this->layout->content = view('alpha::admin.setting.contact',[]);
        $this->layout->menu         = 'general';
        $this->layout->sub_menu     = 'contact';

	}
    public function getAnalytics(){
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(404, 'Unauthorized action.');
        }
        Logger::info("View analytics Setting");

        $this->layout->activeParent = 'general_setting';
        $this->layout->active = 'analytics';


        $this->layout->title = setPageTitle("Analytics");    
        $this->layout->content = view('alpha::admin.setting.analytics',[]);
        $this->layout->menu         = 'general';
        $this->layout->sub_menu     = 'analytics';
    }
    public function getSiteGeneral(){

		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(404, 'Unauthorized action.');
        }

        Logger::info("View General Setting");
		$settings = [];
		$varsGeneralSettings = ['domain_name','path_url','date_format','time_format','weight_format', 'distance_format', 'size_format', 'currency_format'];
		foreach ($varsGeneralSettings as $key => $value) {
			$settings[$value] = app('AlphaSetting')->getSetting($value);
		}

        $this->layout->active = 'setting_general';
        $this->layout->activeParent = 'main_setting';
		$this->layout->title = setPageTitle("General");	
		$this->layout->content = view('alpha::admin.setting.site_general',[
			'settings' => $settings
			]);
        $this->layout->menu         = 'general';
        $this->layout->sub_menu     = 'site_description';
    
	}
	public function postGeneral(Request $req,$type = ''){
		
		if(!app('Alpha\Core\AlphaAcl')->multiAccess([33])){
            abort(404, 'Unauthorized action.');
        }
        $rules = $this->getRules($type);
        if(!empty($rules))
        {
            $this->validate($req,$rules);
        }

        Logger::info("Save Setting");

		$input = \Request::all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			app('AlphaSetting')->updateSetting($key,$value,'yes');
		}

        if($type == 'analytics')
        {
            if($req->file('ga_account_json'))
            {
                $file = $req->file('ga_account_json');
                $file->move(storage_path().'/app/','ga_account.json');
            }
            if($req->file('ga_account_p12'))
            {
                $file = $req->file('ga_account_p12');
                $file->move(storage_path().'/app/','ga_account.p12');
            }

            if($req->file('ga_account_p12') && $req->file('ga_account_json')){
                $config = json_decode(@file_get_contents(storage_path('app/ga_account.json')));
                if(!empty($config))
                {
                    app('AlphaSetting')->updateSetting('ga_client_id',$config->client_id,'yes');
                    app('AlphaSetting')->updateSetting('ga_client_email',$config->client_email,'yes');
                }
                
            }
        }
		return redirect()->back()->with('success','Data Saved!');
	}

    public function viewActivityLog(Request $req)
    {

        Logger::info("View Activity Log");
        $this->layout->active = 'setting_activity_log';
        $this->layout->activeParent = 'main_setting';
        $this->layout->title = setPageTitle("Activity Log"); 
        $this->layout->content = view('alpha::admin.setting.activity');

    }

    public function getDataActivityLog(Request $req)
    {
        //container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        $search = $req->input('search');

        $recordsTotal = Logger::count();

        $loggers = Logger::leftJoin('users',function($j){
            $j->on('users.id','=','loggers.user_id');
        })->leftJoin('usermetas',function($j){
            $j->on('usermetas.user_id','=','loggers.user_id')->where('meta_key','=','first_name');
        })->select(['loggers.*','usermetas.meta_value_text','users.email']);


        if(!empty($search['value']))
        {
            $loggers = $loggers->where('loggers.description','like','%'.$search['value'].'%');
        }

        $recordsFiltered = $loggers->count();

        $cols = ['created_at','created_at','usermetas.meta_value_text','description'];
        $order = $req->input('order');
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $loggers = $loggers->orderBy($col,$order[0]['dir']);
            }else{
                $loggers = $loggers->orderBy('loggers.created_at','desc');
            }
        }else{
            $loggers = $loggers->orderBy('loggers.created_at','desc');
        }

        $loggers = $loggers->with(['user','user.metas','user.roles'])->take($take)->skip($start)->get();

        
        $cols = ['created_at','activity','name','email','role','action'];
        $base = [];
        foreach ($cols as $key => $value) {
            $base[$value] = $value;
        }

        $data = [];

        foreach ($loggers as $key => $value) {
            $item = $base;
            $html = '';
            $html .= view('alpha::admin.setting.grids.activity',['log'=>$value]);
            $item['html'] = $html;
            $data[] = $item;
        }

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);


    }
}