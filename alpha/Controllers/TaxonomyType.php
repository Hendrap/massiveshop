<?php

namespace Alpha\Controllers;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Setting as Settings;
use \Logger;
use \Auth;


class TaxonomyType extends AlphaController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        parent::__construct();
        //setting area for developer
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([1])){
                abort(404, 'Unauthorized action.');
        }
        $this->layout->active = 'setting_taxonomy_type';
        $this->layout->activeParent = 'main_setting';
    }
    public function index()
    {
        //
        Logger::info("View Taxonomy Type Setting");
        $taxonomies = Settings::where('bundle','alpha.taxonomy')->get();
        $this->layout->title = setPageTitle("Taxonomy Types");   
        $this->layout->content = view('alpha::admin.setting.taxonomy_type.index',['taxonomies' => $taxonomies]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         Logger::info("Create Taxonomy Type Setting");

         $this->layout->title = setPageTitle("Create Taxonomy Type");   
         $this->layout->content = view('alpha::admin.setting.taxonomy_type.createEdit',[
         ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = \Request::all();
        
        $this->validate($request,[
            'slug' => 'required|alpha_dash',
            'single' => 'required',
            'plural' => 'required'
        ]);

        unset($input['_token']);
        $setting = Settings::find($input['id']);
        if($input['id'] == 0 || empty($setting)){
            $setting = new Settings();
        }


        $check = Settings::where('id','!=',$input['id'])->whereSettingKey('taxonomy_'.strtolower($input['slug']))->count();
        if($check > 0){
             return redirect()->back()->with('msg','Slug has been taken before !');
        }

        $setting->autoload = 'yes';
        $setting->bundle = 'alpha.taxonomy';
        $setting->setting_key = 'taxonomy_'.strtolower($input['slug']);
        unset($input['id']);
        $setting->setting_value = serialize($input);
        $setting->modified_by = Auth::user()->id;
        $setting->save();
        Logger::info("Save Taxonomy Type Setting");

         return redirect()->route('admin.setting.taxonomy_type.edit',[$setting->id])->with('msg','Data Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting = Settings::with(['user','user.metas'])->find($id);
        if(empty($setting)) abort(404);
        Logger::info("Edit Taxonomy Type Setting");
        $this->layout->title = setPageTitle("Edit Taxonomy Type");   
        $this->layout->content = view('alpha::admin.setting.taxonomy_type.createEdit',[
            'setting'=>$setting,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Logger::info("Delete Taxonomy Type Setting");
        Settings::find($id)->delete();
        return redirect()->back()->with('delete_msg','Data Deleted!');
    }
}
