<?php

namespace Alpha\Controllers;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Setting as Settings;
use \Logger;
use \Auth;


class UserType extends AlphaController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        parent::__construct();
        //setting area for developer
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([1])){
                abort(404, 'Unauthorized action.');
        }
        $this->layout->active = 'setting_user_type';
        $this->layout->activeParent = 'main_setting';
    }
    public function index()
    {
        //
        Logger::info("View User Type Setting");
        $this->layout->title = setPageTitle("User Types");   
        $users = Settings::where('bundle','alpha.user')->get();
        $this->layout->content = view('alpha::admin.setting.user_type.index',[
            'users' => $users
            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Logger::info("Create User Type Setting");

        $this->layout->title = setPageTitle("Create User Type");   
        $this->layout->content = view('alpha::admin.setting.user_type.createEdit',[
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = \Request::all();
        
        $this->validate($request,[
            'slug' => 'required|alpha_dash',
            'meta_key.*' => 'alpha_dash',
        ]);

        unset($input['_token']);
        $setting = Settings::find($input['id']);
        if($input['id'] == 0 || empty($setting)){
            $setting = new Settings();
        }


        $metas = array();
        if(!empty($input['meta_key'])){
            foreach (@$input['meta_key'] as $key => $value) {
                $metas[] = (object)array(
                    'meta_key' => $value,
                    'meta_name' => @$input['meta_name'][$key],
                    'meta_data_type' => @$input['meta_data_type'][$key],
                    );
            }
            
        }
        unset($input['meta_key']);
        unset($input['meta_name']);
        unset($input['meta_data_type']);

        $input['metas'] = $metas;
        $check = Settings::where('id','!=',$input['id'])->whereSettingKey('user_'.strtolower($input['slug']))->count();
        if($check > 0){
             return redirect()->back()->with('msg','Slug has been taken before !');
        }

        $setting->autoload = 'yes';
        $setting->bundle = 'alpha.user';
        $setting->setting_key = 'user_'.strtolower($input['slug']);
        unset($input['id']);
        $setting->setting_value = serialize($input);
        $setting->save();
       
        Logger::info("Save User Type Setting");
 
        return redirect()->route('admin.setting.user_type.edit',[$setting->id])->with('msg','Data Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting = Settings::find($id);
        if(empty($setting)) abort(404);

        Logger::info("Edit User Type Setting");


        $this->layout->title = setPageTitle("Edit User Type");   
        $this->layout->content = view('alpha::admin.setting.user_type.createEdit',[
            'setting'=>$setting,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Logger::info("Delete User Type Setting");
        Settings::find($id)->delete();
        return redirect()->back()->with('msg','Data Deleted!');
    }
}
