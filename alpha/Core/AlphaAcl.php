<?php 
namespace Alpha\Core;
/**
* 
*/
class AlphaAcl
{
	public function isAllow($role = '',$resource = 0){
		$allowed = \Acl::isAllowed($role, 'alphaCMS', $resource);
		return $allowed;
	}
	public function multiRole($roles = array(),$resource = 0){
		$finalStatus = false;

		foreach ($roles as $key => $value) {
			$tmpStatus = $this->isAllow($value,$resource);
			$finalStatus = $finalStatus || $tmpStatus;
		}
		return $finalStatus;
	}
	public function multiAccess($resource = array()){

	
		$user = \Auth::user();
		app('AdminUser')->user = $user;			
		
		

		if(empty($user)) return false;

		$finalStatus = false;

		if(empty(app('AdminUser')->avatar)){
			$metas = \Usermeta::whereUserId($user->id)->get();
			$avatar = getEntryMetaFromArray($metas,'avatar');
			$name = getEntryMetaFromArray($metas,'first_name').' '.getEntryMetaFromArray($metas,'last_name');
			if(empty($name)) $name = $user->email;

			if(!empty($metas) && !empty($avatar))
			{
				app('AdminUser')->avatar = $avatar;
			}

			app('AdminUser')->name = $name;

		}




		if(empty(app('AdminUser')->roles)){
			$roles = $user->roles()->select('roles.name','roles.id')->get();
	    	app('AdminUser')->roles = $roles;			
		}


    	if(empty(app('AdminUser')->roles)) return false;
		foreach ($resource as $key => $value) {
			$finalStatus = $finalStatus || $this->canAccess(app('AdminUser')->roles,$value);
		}
		
		return $finalStatus;
	}
	public function canAccess($roles = array(),$resource = 0){

		$activeRoles = [];
    	
    	foreach ($roles as $key => $value) {
    		$activeRoles[] = $value->name;
    		//super administrator
    		if($value->id == 1){
    			return true;
    		}
    	}
    	
    	$allow =  $this->multiRole($activeRoles,$resource);
    	return $allow;

	}
}