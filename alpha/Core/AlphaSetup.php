<?php 
namespace Alpha\Core;
/**
* 
*/
class AlphaSetup
{
	
	function __construct()
	{
		
	}
	
	public function run()
	{
		
		$this->setRoles();			
		$this->setSingleton();
		app('AlphaSetting');
		$this->loadPlugins();
	}
	public function setSingleton()
	{

		app()->singleton('AlphaSetting',function(){
			return new AlphaSetting();
		});

		app()->singleton('AdminUser',function(){
			return new AdminUser();
		});

		app()->singleton('AlphaPlugin',function(){
			return new Plugin();
		});


		app()->singleton('AlphaMenu',function(){
			return new Menu();
		});
		
		app()->bind('AlphaMailMessage','Swift_Message');
		app()->bind('AlphaMailSender','Alpha\Core\Mailer');

	}
	public function loadPlugins(){
		$plugins = \Config::get('alpha.plugins');
		if(empty($plugins)) return false;

		foreach ($plugins as $key => $value) {
			$plugin = app('AlphaSetting')->getSetting($key);
			if(class_exists('\\'.$key)){
				app('AlphaPlugin')->addPlugin($key,@$value['name']);				
				if(empty($plugin)){
					$plugin = new \Setting();
					$plugin->bundle = 'alpha.plugins';
					$plugin->setting_key = $key;
					$plugin->setting_value = 'no';
					$plugin->save();
				}else{
					if($plugin == 'yes'){
						app()->register($key);
					}
				}
			}else{
				unset(app('AlphaPlugin')->plugins[$key]);
			}
		}
	}	
	public function setRoles()
	{
		
		$acl = app('acl');
		$acl->addResource('alphaCMS');
		$roles = \Role::with('rules')->select(['id','name'])->get();

		foreach($roles as $v){

			$acl->addRole((string)$v->name);
			$access = array();
			foreach ($v->rules as $key => $value) {
				$access[] = $value->id;
			}
			if(!empty($access)){
				$acl->allow((string)$v->name,'alphaCMS',$access);				
			}
		
		}


	}


}