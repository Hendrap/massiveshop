<?php 
namespace Alpha\Core;
class Menu
{
	public $menus;
	public $labels;

	function __construct()
	{
		$this->menus = array();
	}	

	function addLabel($obj,$key = ''){
		$this->labels[$key] = $obj;

		return $this;
	}
	function add($obj,$key = '')
	{
		$this->menus[$key] = $obj;

		return $this;
	}

	function remove($key = '')
	{
		if(empty($this->menus[$key])) return false;
		unset($this->menus[$key]);

		return $this;

	}

	function addChildTo($parent = '',$obj,$key = '')
	{
		if(empty($this->menus[$parent])) return false;

		$parentObj = $this->menus[$parent];
		if(empty($parentObj->childs)) $parentObj->childs = array();

		$parentObj->childs[$key] = $obj;
		$this->menus[$parent] = $parentObj;	

		return $this;

	}

	function removeChildFrom($parent = '',$key = '')
	{
		if(empty($this->menus[$parent])) return false;
		if(empty($this->menus[$parent]->childs)) return false;

		$parentObj = $this->menus[$parent];
		if(empty($parentObj->childs[$key])) return false;

		unset($parentObj->childs[$key]);

		$this->menus[$parent] = $parentObj;

		return $this;

	}

}