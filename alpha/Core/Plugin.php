<?php 
namespace Alpha\Core;
/**
* 
*/
class Plugin
{
	public $plugins;
	
	public function addPlugin($serviceProvider = '',$name = ''){
		$obj = new \StdClass();
		$obj->provider = $serviceProvider;
		$obj->name = $name;
		$this->plugins[$serviceProvider] = $obj;
	}
}