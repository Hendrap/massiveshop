<?php 

function getImageFromEmbedVideo($type = '',$infoVideo = array()){
	$imgPath = '';
	$targetPath = 'uploads'.'/'.date('Y').'/'.date('m');
	$filename = '';

	if(!is_dir(base_path('public/'.$targetPath))) mkdir(base_path('public/'.$targetPath), 0777, true);

	if($type == 'youtube'){
		$filename = $targetPath.'/'.'youtube-'.time().'.jpg';
		@copy("http://img.youtube.com/vi/".$infoVideo["parsed_param"]["v"]."/0.jpg",base_path('public/'.$filename));
	}
	if($type == 'vimeo'){
		$filename = $targetPath.'/'.'vimeo-'.time().'.jpg';
		@copy($infoVideo["thumbnail_url"],base_path('public/'.$filename));
	}
	return $filename;
}

function checkVideoURL($video_url = '') {
	$return = array(
		"type" => "invalid"
	);
	try {
		$parsed_url = parse_url(trim($video_url));
	}

	catch (Exception $e) {

	}



	if (!empty($parsed_url["host"])) {

		switch ($parsed_url["host"]) {

			case "youtube.com":

			case "www.youtube.com":

				if (!empty($parsed_url["query"])) {

					parse_str($parsed_url["query"], $parsed_param);



					if (!empty($parsed_param["v"])) {

						$return["type"] = "youtube";

						$return["parsed_url"] = $parsed_url;

						$return["parsed_param"] = $parsed_param;

					}

				}

				break;

			case "vimeo.com":

			case "www.vimeo.com":

				if (!empty($parsed_url["path"]) and $parsed_url["path"] != "/") {

					$return["type"] = "vimeo";

					$return["parsed_url"] = $parsed_url;

				}

				break;

			default:

				break;

		}

	}

	return $return;
}

function toAsset($path = '',$secure = false){
	$isSubFolder = Config::get('alpha.application.subfolder');
	$prefix = '';
	if($isSubFolder){
		$prefix = 'public/';
	}
	return asset($prefix.$path,$secure);
}
function workshopCrop($imgPath = '', $prefix = '',$width = 0,$height = 0,$crop = false,$ratio = true){
	set_time_limit(0);
	try {
		if(Config::get('alpha.application.subfolder')){
			$imgPath = Config::get('alpha.application.urlasset').$imgPath;
		}
		$cleanPath = str_replace('\\','/',$imgPath);
		$tmpPath = explode('/', $cleanPath);
		$fileName = $tmpPath[count($tmpPath) - 1];
		$savePath = str_replace($fileName, '', $cleanPath);
		$tmpFile = explode('.',$fileName);
		$tmpTotal = count($tmpFile);	
		if($tmpTotal <= 2){
			$newName = $tmpFile[0]."_".$prefix.'.'.$tmpFile[1];
		}else{
			$tmpExt = $tmpFile[$tmpTotal - 1];
			unset($tmpFile[$tmpTotal - 1]);
			$newName = implode('.', $tmpFile).'_'.$prefix.'.'.$tmpExt;
		}		


		$layer = PHPImageWorkshop\ImageWorkshop::initFromPath($imgPath);
		$w = $layer->getWidth();
		$h = $layer->getHeight();
		
		
		if(!empty($width) && !empty($height))
		{
			$width_ratio = $w/$width;
			$height_ratio = $h/$height;
		} else if(!empty($width))
		{
			$width_ratio = $w/$width;
			$height_ratio = 0;
		} else
		{
			$width_ratio = 0;
			$height_ratio = $h/$height;
		}

		if($crop && !empty($width) && !empty($height))
		{
			if($width_ratio<=$height_ratio) {
				if($ratio){
					$layer->resizeInPixel($width,null,true,0,0,'MM');			
				}
			}else{
				if($ratio){
					$layer->resizeInPixel(null,$height,true,0,0,'MM');
				}
			}
			if(!$ratio){
				$layer->resizeInPixel($width,$height,true,0,0,'MM');
			}
			
			$layer->cropInPixel($width,$height,0,0,'MM');
		}
		else 
		{
			if($ratio){
				if($width_ratio<=$height_ratio) {
					$layer->resizeInPixel(null,$height,true,0,0,'MM');
				}else {
					$layer->resizeInPixel($width,null,true,0,0,'MM');
				}		
			}else{
				$layer->resizeInPixel($width,$height,false,0,0,'MM');		
			}
			

		}
		
		$createFolders = true;
		$backgroundColor = null; // transparent, only for PNG (otherwise it will be white if set null)
		$imageQuality = 100; // useless for GIF, usefull for PNG and JPEG (0 to 100%)
		
		$layer->save($savePath,$newName, $createFolders, $backgroundColor, $imageQuality);

	} catch (Exception $e) {
		return false;
	}
}
function inverventionCrop($imgPath = '', $prefix = '',$width = 0,$height = 0,$crop = false,$ratio = false){
	set_time_limit(0);
	try {
		$imgPath = Config::get('alpha.application.urlasset').$imgPath;
		$cleanPath = str_replace('\\','/',$imgPath);
		$tmpPath = explode('/', $cleanPath);
		$fileName = $tmpPath[count($tmpPath) - 1];
		$savePath = str_replace($fileName, '', $cleanPath);
		$tmpFile = explode('.',$fileName);
		$tmpTotal = count($tmpFile);	
		if($tmpTotal <= 2){
			$newName = $tmpFile[0]."_".$prefix.'.'.$tmpFile[1];
		}else{
			$tmpExt = $tmpFile[$tmpTotal - 1];
			unset($tmpFile[$tmpTotal - 1]);
			$newName = implode('.', $tmpFile).'_'.$prefix.'.'.$tmpExt;
		}		

		$manager = new Intervention\Image\ImageManager(array('driver' => 'gd'));

		if($crop == true){
			$method = 'fit';
		}else{
			$method = 'resize';
		}

		$image = $manager->make(base_path($cleanPath))->{$method}($width, $height,function($c)use($ratio){
			if($ratio == true){
				 $c->aspectRatio();
	   	 		 $c->upsize();				
			}
		});

			
		$background = $manager->canvas($image->width(), $image->height());			
		$background->insert($image);
		$background->save(base_path($savePath.$newName));
		$image->destroy();
		$background->destroy();

		return true;

	} catch (Exception $e) {
		return false;
	}
}
function cropImage($imgPath = '', $prefix = '',$width = 0,$height = 0,$crop = false,$ratio = false){
	set_time_limit(0);
	//inverventionCrop($imgPath,$prefix,$width,$height,$crop,$ratio);

	workshopCrop($imgPath,$prefix,$width,$height,$crop,$ratio);
}



function getCropImage($imgPath = '', $prefix = '',$forceCrop = false){
	$profiles = app('AlphaSetting')->images;
	$selectedProfile = [];
	foreach ($profiles as $key => $value) {
		if($value['name'] == $prefix){
			$selectedProfile = $value;
		}
	}

	if(empty($selectedProfile)) return '#';


	$width = $selectedProfile['width'];
	$height = $selectedProfile['height'];





	$crop = false;
	$ratio = false;
	if($selectedProfile['crop'] == 'yes'){
		$crop = true;
	}

	if(@$selectedProfile['aspectratio'] == 'yes'){
		$ratio = true;
	}


	try {



		$realPath = str_replace('\\', '/', $imgPath);

		$tmp = explode('/', $realPath);

		$tmp = $tmp[count($tmp) - 1];

		$fileName = $tmp;

		$tmp = explode('.', $tmp);
	
		$tmpTotal = count($tmp);	
		if($tmpTotal <= 2){
			$parseName = $tmp[0].'_'.$prefix.'.'.$tmp[1];
		}else{
			$tmpExt = $tmp[$tmpTotal - 1];
			unset($tmp[$tmpTotal - 1]);
			$parseName = implode('.', $tmp).'_'.$prefix.'.'.$tmpExt;
		}		



		$targetPath = str_replace($fileName, $parseName, $imgPath);

	
		if($forceCrop == true){
			if(!file_exists(base_path(Config::get('alpha.application.urlasset').$targetPath))){
				cropImage($imgPath,$prefix,$width,$height,$crop,$ratio);
			}
		}

		if(Config::get('alpha.application.subfolder') == false){

			$targetPath = str_replace('public/', '', $targetPath);

		}



		return $targetPath;



	} catch (Exception $e) {



		return false;



	}

	

}

function getParsedLink(){



}