<?php

function setCurrency($currency = '')
{
	Session::put('defaultCurrency',$currency);
}


function formatMoney($number = 0,$currency = ''){
	
	if(empty($currency)) $currency = getCurrency();

	if($currency == 'idr') return number_format($number);

	return number_format($number,2);
}

function initCurrency()
{
	if(!Session::has('defaultCurrency'))
	{
		setCurrency(getCurrency());
	}
}


function getCurrencySymbol($currency = '')
{
	if(empty($currency)) $currency = getCurrency();
	return config('alpha.currency.'.$currency);	
}

function getCurrency()
{
	$sessionCurrency = Session::get('defaultCurrency');
	if(!empty($sessionCurrency)) return $sessionCurrency;

	return app('AlphaSetting')->getSetting('default_currency');
}

function convertCurrency($number = 0,$currency = '',$rate = 0)
{
	if($number == 0) return 0;

	if(empty($currency))
	{
		$selectedCurrency = getCurrency();		
	}else{
		$selectedCurrency = $currency;
	}

	
	if($selectedCurrency == 'idr') return $number;
	if($rate == 0)
	{
		$rate = app('AlphaSetting')->getSetting('rate_'.$selectedCurrency.'_idr');		
	}
	
	return round($number/$rate,2);
}