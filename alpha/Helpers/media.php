<?php 
function renderEntryMedia($media = array())
{
	$res = '';
	foreach ($media as $key => $value) {
			$img = asset(getCropImage('backend/assets/images/placeholder.jpg','default',true));
			$path = asset($value->path);
			if(!empty($value->url)){
				$path = $value->url;
			}
			$title = $value->title;
			$description = $value->description;
			if($value->media_type == 'image'){
				$img = asset(getCropImage($value->path,'default',true));
			}elseif ($value->media_type == 'video') {
				if(!empty($value->url)){
					$img = asset(getCropImage($value->path,'default',true));
				}
			}


			$res .= view('alpha::admin.media.templates.img-entry-media',[
				'img' => $img,
				'id' => $value->id
			]);
		}
	return $res;
}

function parseMedias($media = array()){
	$res = '';
	foreach ($media as $key => $value) {
			$img = asset(getCropImage('backend/assets/images/placeholder.jpg','default'));
			$path = asset($value->path);
			if(!empty($value->url)){
				$path = $value->url;
			}
			$title = $value->title;
			$description = $value->description;
			if($value->media_type == 'image'){
				$img = asset(getCropImage($value->path,'default',true));
			}elseif ($value->media_type == 'video') {
				if(!empty($value->url)){
					$img = asset(getCropImage($value->path,'default',true));
				}
			}


			$res .= view('alpha::admin.media.templates.entry-media',[
				'img' => $img,
				'path' => $path,
				'title' => $title,
				'id' => $value->id,
				'description' => $description,
				'type' => $value->media_type
			]);
		}
	return $res;
}

function getMedias($media = array()){
	$res = '';
	foreach ($media as $key => $value) {
			$img = asset('backend/assets/images/placeholder.jpg');
			$path = asset($value->path);
			if(!empty($value->url)){
				$path = $value->url;
			}

			$title = $value->title;
			$description = $value->description;
			if($value->media_type == 'image'){
				$img = asset(getCropImage($value->path,'default',true));
			}elseif ($value->media_type == 'video') {
				if(!empty($value->url)){
					$img = asset(getCropImage($value->path,'default',true));
				}
			}

			$res .= view('alpha::admin.media.templates.get-entry-media',[
				'img' => $img,
				'path' => $path,
				'title' => $title,
				'id' => $value->id,
				'description' => $description,
				'url' => $value->link
			]);
		}
	return $res;
}