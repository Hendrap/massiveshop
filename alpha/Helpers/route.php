<?php 

function getRouteList()
{
	$data = [];
	
	$blackList = getBlackList();

	$routeCollection = Route::getRoutes();
	foreach ($routeCollection as $value) {
		$path = $value->getPath();
		if(!in_array($path, $blackList)){
		    $data[] = $path;			
		}
	}

	return $data;
}


function getBlackList()
{
	$blackList = Config::get('alpha.application.blacklist_pages');
	$isMultilang = false;
	$defautLang = Config::get('alpha.application.default_locale');
	if(count(Config::get('alpha.application.locales')) > 1){
		$isMultilang = true;
	}

	if($isMultilang)
	{
		$tmp = $blackList;
		$blackList = [];
		foreach ($tmp as $key => $value) {
			$blackList[] = $defautLang.'/'.$value;
		}
	}

	return $blackList;
}