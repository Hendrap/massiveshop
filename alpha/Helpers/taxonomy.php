<?php 
function getTaxonomyConfig($type = ''){
	$config = getConfigBySlug('taxonomies',$type);
	return $config;
}

function getTaxonomies($entryId=null,$typeOfCategory=null){
	if (empty($entryId) and empty($typeOfCategory))
		return false;
	else
		return $taxonomies = Entry::find($entryId)->Taxonomies()->where('taxonomy_type', '=', $typeOfCategory)->first();
}

function getSeries($taxonomyType=null){
	if (empty($taxonomyType))
		return false;
	else
		return $taxonomies = Taxonomy::select()->where('taxonomy_type', '=', $taxonomyType)->get();
}
function getCategoryNameFromTaxonomies($taxonomies = []){
	$name = '';
	foreach ($taxonomies as $key => $value) {
		if($value->taxonomy_type == 'category'){
			$name = $value->name;
			break;
		}
	}
	return $name;
}
