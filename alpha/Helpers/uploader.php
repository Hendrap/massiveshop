<?php 
function copyScrapImage($url = '',$description){

    $info = pathinfo($url);

    $prefix = '';
    if(\Config::get('alpha.application.subfolder')) $prefix = 'public/';
    $name = time().'.'.@$info['extension'];
    $dest = 'uploads'.'/'.date('Y').'/'.date('m');
    if(!is_dir($prefix.$dest)) mkdir($prefix.$dest,0777,true);

    $target = $dest.'/'.$name;
    @copy(str_replace(' ', '%20', $url),$prefix.$target);
    //chmod($prefix.$target, 0777);

     $media = new \Media();
     $media->title = $info['filename'];
     $media->description = $description;
     $media->media_type = 'image';
     $media->media_mime = '';
     $media->media_meta = serialize($info);
     $media->url = '';
     $media->path = 'uploads/'.date('Y').'/'.date('m').'/'.$name;
     $media->author = Auth::user()->id;
     $media->save();
     loopCropImage('uploads/'.date('Y').'/'.date('m').'/'.$name);
     return $media;
}
function mediaUploader($name = '',$req,$description = '',$type = 'image'){
	 $file = $req->file($name);
	 $info = [];
     $info  = @pathinfo($_FILES[$name]['name']);
     if(empty($info)) $info = [];
     $info['size'] = $file->getClientSize();
     $info['mime_type'] = $file->getClientMimeType();
     $extension = $file->getClientOriginalExtension();
     if(empty($info['filename'])){
        $info['filename'] = $file->getClientOriginalName();
     }
     
     $title = $info['filename'].'-'.time();
     $titleMedia = $info['filename'];
     $name = $title.'.'.$extension;  
     $dest = str_replace('\\','/',base_path().'/public/uploads/'.date('Y').'/'.date('m'));

	 if(!is_dir($dest)) mkdir($dest,0777,true);

     $file->move($dest,$name);
     chmod($dest.'/'.$name, 0777);


     $media = new \Media();
     $media->title = $titleMedia;
     $media->description = $description;
     $media->media_type = $type;
     $media->media_mime = $file->getClientMimeType();
     $media->media_meta = serialize($info);
     $media->url = '';
     $media->path = 'uploads/'.date('Y').'/'.date('m').'/'.$name;
     $media->author = Auth::user()->id;

     if($type == 'image' || $extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif'){
         $media->media_type = 'image';
         loopCropImage('uploads/'.date('Y').'/'.date('m').'/'.$name);        
     }
     $media->save();

     return $media;
}
function loopCropImage($name){
    $crops = app('AlphaSetting')->images;
    foreach ($crops as $key => $value) {
         $crop = false;
         $ratio = false;
         if($value['crop'] == 'yes'){
             $crop = true;
         }
         if(@$value['aspectratio'] == 'yes'){
             $ratio = true;
         }
         cropImage($name,$value['name'],$value['width'],$value['height'],$crop,$ratio);
     }
}