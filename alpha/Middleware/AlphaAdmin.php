<?php 
namespace Alpha\Middleware;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;

class AlphaAdmin
{

    public function handle( $request, Closure $next ) {
     	return $next($request);
    }

}