<?php 
namespace Alpha\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
   	 	'/admin/login',
   	 	'/admin/entry/*',
        '/admin/users/get-data',
        '/admin/taxonomy/get-data/*',
        '/admin/media/get-data-media/*'
    ];
}
