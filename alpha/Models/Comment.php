<?php
namespace Alpha\Models;
/**
* 
*/
class Comment extends AlphaORM
{
	
	function __construct()
	{
		# code...
	}

	public function entry()
	{
		return $this->belongsTo('Entry');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function parent()
	{
		return $this->belongsTo('Comment','comment_parent');
	}

	public function childs()
	{
		return $this->hasMany('Comment','comment_parent');
	}
}