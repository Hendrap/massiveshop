<?php 
namespace Alpha\Models;
/**
* 
*/
class Entrymeta extends AlphaORM
{
	
	protected $fillabel = ['entry_id'];
	
	function __construct()
	{
		# code...
	}
	
	public function entry(){
		return $this->belongsTo('Entry');
	}

	public function media()
	{
		return $this->belongsTo('Media');
	}
}