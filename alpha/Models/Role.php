<?php 
namespace Alpha\Models;
/**
* 
*/
class Role extends AlphaORM
{
	protected $fillable = ['name'];
	function __construct()
	{
		# code...
	}

	public function rules(){
		return $this->belongsToMany('Rule');
	}
	public function user()
	{
		return $this->belongsTo('User','modified_by');

	}
	public function users(){
		return $this->belongsToMany('User');
	}
}