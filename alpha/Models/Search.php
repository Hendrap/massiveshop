<?php
namespace Alpha\Models;
/**
* 
*/
class Search extends AlphaORM
{
	
	function __construct()
	{
		
	}
	public static function rebuild($entry = array()){
		if($entry->entry_type == 'page'){
			$data = [];	
			$entries = Entry::select(['slug'])->whereEntryType('page')->get();
			foreach ($entries as $key => $value) {
				$data[] = $value->slug;
			}

			$data = json_encode($data);
			$setting = Setting::whereSettingKey('alpha_pages_slug')->first();
			if(empty($setting)) $setting = new Setting();

			$setting->setting_key = 'alpha_pages_slug';
			$setting->setting_value = $data;
			$setting->bundle = 'alpha';
			$setting->autoload = 'yes';
			$setting->save();
		}	
	}
}