<?php 
namespace Alpha\Models;
/**
* 
*/
class Taxonomy extends AlphaORM
{
	
	function __construct()
	{
		# code...
	}

	public function activeEntries()
	{
		return $this->belongsToMany('Entry')->where(function($q){
			$q->where('entries.status','published');
			$q->orWhere('entries.status','featured');
		});
	}

	public function entries()
	{
		return $this->belongsToMany('Entry');
	}

	public function parents(){
		return $this->belongsTo('Taxonomy','parent');
	}

	public function childs(){
		return $this->hasMany('Taxonomy','parent');
	}
	
	public function user(){
		return $this->belongsTo('User','modified_by');
	}

	public function items(){
        return $this->BelongsToMany('Morra\Catalog\Models\Item');
    }
}