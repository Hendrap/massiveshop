<?php 
namespace Alpha\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
/**
* 
*/
class User extends Authenticatable
{
    public function billingAddress()
    {
        return $this->hasMany('CatalogAddress')->where('type','billing');
    }

    public function shippingAddress()
    {
        return $this->hasMany('CatalogAddress')->where('type','shipping');
    }
    public static function getLastUpdate(){
        return static::with('modifiedBy')->orderBy('updated_at','desc')->first();
    }
    public function modifiedBy(){
        return $this->belongsTo('User','modified_by');
    }
	public function metas(){
		return $this->hasMany('Usermeta');
	}

	public function roles(){
		return $this->belongsToMany('Role');
	}
	//save user meta
	public function saveUserMetaFromInput($metas = array(),$input = array()){
        if(empty($metas)) return true;
		foreach ($metas as $key => $value) {
            $meta = \Usermeta::whereMetaKey($value->meta_key)->whereUserId($this->id)->first();
            if(empty($meta)){
                $meta = new \Usermeta();
                $meta->meta_key = $value->meta_key;
                $meta->user_id = $this->id;
            }
            $val = '';
            switch ($value->meta_data_type) {
                case 'text':
                    $val = @$input['metas'][$value->meta_key];
                    break;
                case 'int':
                    $val = (int)@$input['metas'][$value->meta_key];
                    break;
                case 'date':
                    $val = date('Y-m-d H:i:s',strtotime(@$input['metas'][$value->meta_key]));
                    break;
            }
            if(!empty($val)){
                $meta->{'meta_value_'.$value->meta_data_type} = $val;                
            }
            $meta->save();
        }
	}
    public function saveMetaUserProfile($metas = array()){
        if(empty($metas)) return true;
        foreach ($metas as $key => $value) {
            $meta = \Usermeta::whereMetaKey($key)->whereUserId($this->id)->first();
            if(empty($meta)){
                $meta = new \Usermeta();
                $meta->meta_key = $key;
                $meta->meta_value_text = $value;
                $meta->user_id = $this->id;
            }
            else{
                $meta->meta_value_text = $value;
            }
            $meta->save();
        }
        // return $meta;
    }
    
}