<?php

/*
 * Alpha Services Provider
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * Ini untuk loader semua keperluan untuk alpha
 */


namespace Alpha\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Filesystem\Filesystem;
class AlphaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        app('Alpha\Core\AlphaSetup')->run();
        $this->loadGAConfig();
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        $this->loadConfigs();
        $this->setAliases();
        $this->registerMiddlewares();
        $this->registerServiceProviders();
        $this->registerViews();
        $this->loadHelpers();
        $this->loadAlphaConfig();
        
    }

     /**
     * Load semua service yg berhubungan sama alpha
     * 
     * @return void
     */
    public function loadAlphaConfig()
    {

        //ini profiler/debug bar & console

        if(\Config::get('alpha.application.profiler')){
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }
        $this->app->register(\Darsain\Console\ConsoleServiceProvider::class);

    }

    /**
     * Load semua helpers
     * 
     * @return void
     */
    public function loadHelpers()
    {
        $helpers = $this->loadFilesFromDir(base_path().'/alpha/Helpers');
        foreach ($helpers as $key => $value) {
            include $value;
        }
    }

    /**
     * Register semua middleware
     * 
     * @return void
     */
    public function registerMiddlewares()
    {
        $middlewares = $this->loadFilesFromDir(base_path().'/alpha/Middleware');
        foreach ($middlewares as $key => $value) {
            $className = str_replace('.php', '', $value->getRelativePathname());
            $this->app['router']->middleware($className,'Alpha\Middleware\\'.$className);            
        }

        $this->app['router']->middlewareGroup('AlphaAdminGroup',[
            \Alpha\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Alpha\Middleware\VerifyCsrfToken::class,
        ]);
    }

     /**
     * Register semua views
     * 
     * @return void
     */
    public function registerViews()
    {
         $this->loadViewsFrom(base_path() . '/alpha/Views', 'alpha');
         
    }

    /**
     * Register semua providers yg ada di file config alpha
     * 
     * @return array
     */
    public function registerServiceProviders()
    {
        $providers = \Config::get('alpha.providers');
        foreach ($providers as $key => $value) {
            $this->app->register($value);
        }
        
    }

    /**
     * Ini cuma ambil informasi files php yg ada dalam suatu folder/path
     * 
     * @return array
     */
    public function loadFilesFromDir($path = '')
    {
        $files = [];
        if(!empty($path)) $files = \File::allFiles($path); 
        return $files;
    }

    /**
     * Masukin semua config yg ada di folder Config
     * bisa create sendiri untuk developer
     * @return void
     */
    public function loadConfigs()
    {
        $configFiles = $this->loadFilesFromDir(base_path().'/alpha/Config');
        foreach ($configFiles as $key => $value) {
            $this->mergeConfigFrom((string)$value, 'alpha.'.str_replace('.php', '', $value->getRelativePathname()));
        }
    }

    /**
     * Set alias untuk core,controllers,dll alpha punya
     * nantinya biar gak perlu nambahin namespace
     * @return void
     */
    public function setAliases()
    {

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $aliases = \Config::get('alpha.aliases');
        $models = $this->loadFilesFromDir(base_path('alpha/Models'));
        foreach ($models as $key => $value) {
            $className = str_replace('.php', '', $value->getrelativePathname());
            $aliases[$className] = 'Alpha\Models\\'.$className;
        }
        foreach ($aliases as $key => $value) {
          $loader->alias($key, $value);
        }

    }

    public function loadGAConfig(){
        if(file_exists(storage_path('app/ga_account.p12')) && !empty(app('AlphaSetting')->getSetting('ga_view_id')) && !empty(app('AlphaSetting')->getSetting('ga_client_email')) && !empty(app('AlphaSetting')->getSetting('ga_client_id'))){
            \Config::set('laravel-analytics.siteId','ga:'.app('AlphaSetting')->getSetting('ga_view_id'));
            \Config::set('laravel-analytics.serviceEmail',app('AlphaSetting')->getSetting('ga_client_email'));
            \Config::set('laravel-analytics.clientId',app('AlphaSetting')->getSetting('ga_client_id'));
            \Config::set('laravel-analytics.certificatePath',storage_path('app/ga_account.p12'));
            $this->app->register('Spatie\LaravelAnalytics\LaravelAnalyticsServiceProvider');
        }
    }

}
