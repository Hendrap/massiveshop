<?php 

Route::group(['middleware' => 'AlphaAdminGroup'], function () {

	Route::group(['namespace'=>'Alpha\Controllers','prefix' => 'admin',], function () {
		Route::get('refresh-csrf',['as'=>'alpha_admin_refresh_token','uses'=>function(){
		    return csrf_token();
		}]);


		//login logout 
		Route::get('/login',['as'=>'alpha_get_login','uses'=>'Credential@login']);
		Route::get('/logout',['as'=>'alpha_get_logout','uses'=>'Credential@logout']);
		Route::post('/login',['as'=>'alpha_post_login','uses'=>'Credential@postLogin']);
		Route::get('forgot-password',['as'=>'alpha_forgot_password','uses'=>'Credential@forgotPassword']);
		Route::post('forgot-password',['as'=>'alpha_post_forgot_password','uses'=>'Credential@postForgotPassword']);
		Route::get('password-recovery-success',['as'=>'alpha_success_password_recovery','uses'=>'Credential@successForgotPassword']);

		//ini ada  middleware buat bataesin akses admin
		Route::group(['middleware' => 'AuthAdmin'],function(){
			//index dashboard
			Route::get('/',['as'=>'alpha_admin_index','uses'=>'Dashboard@index']);
			//settings prefix ,seluruh halaman /admin/setting masuk sini yah
			Route::group(['prefix' => 'setting'], function () {
				//general setting,isinya seo atau site description gitu
				
				Route::post('general/{type?}',['as'=>'alpha_admin_post_setting_general','uses'=>'Setting@postGeneral']);

				//general site, untuk domain path url dll
				Route::get('site-general',['as'=>'alpha_admin_get_setting_site_general','uses'=>'Setting@getSiteGeneral']);
				Route::get('extras',['as'=>'alpha_admin_get_extras','uses'=>'Setting@getExtras']);
				Route::get('labels',['as'=>'alpha_admin_get_labels','uses'=>'Setting@getLabels']);
				Route::any('get-data-log',['as'=>'alpha_admin_get_data_log','uses'=>'Setting@getDataActivityLog']);
				Route::get('activity-log',['as'=>'alpha_admin_activity_log','uses'=>'Setting@viewActivityLog']);				
				Route::get('currency',['as'=>'alpha_admin_currency','uses'=>'Setting@currency']);				
				Route::get('dhl',['as'=>'alpha_admin_dhl','uses'=>'Setting@dhl']);				
				Route::get('doku',['as'=>'alpha_admin_doku','uses'=>'Setting@doku']);				
				Route::get('free-shipping',['as'=>'alpha_admin_free_shipping','uses'=>'Setting@freeShipping']);				

					//
				Route::get('change-status-plugins/{plugin}/{status}',['as'=>'alpha_admin_get_status_plugins','uses'=>'Setting@getStatusPlugins']);


				//config crop images
				Route::resource('image','Image');
				//Roles & access
				
				//Entry Types
				Route::resource('entry_type','EntryType');
				//Taxonomy Types
				Route::resource('taxonomy_type','TaxonomyType');
				//user types;
				Route::resource('user_type','UserType');
			});
			Route::group(['prefix' => 'general'], function () {
				Route::get('site-description',['as'=>'alpha_admin_get_setting_general','uses'=>'Setting@getGeneral']);
				Route::get('analytics',['as'=>'alpha_admin_get_analytics','uses'=>'Setting@getAnalytics']);
				Route::get('contacts',['as'=>'alpha_admin_get_contacts','uses'=>'Setting@getContacts']);
			});
			Route::get('plugins/all-plugins',['as'=>'alpha_admin_get_plugins','uses'=>'Setting@getPlugins']);

			//users
			Route::group(['prefix' => 'accounts'], function () {
				Route::resource('roles-and-access','Role');
			});

			Route::get('accounts/all-accounts',['as'=>'alpha_admin_all_account','uses'=>'Admin\User@index']);
			Route::get('accounts/new-account',['as'=>'alpha_admin_new_account','uses'=>'Admin\User@create']);
			Route::get('accounts/edit-account/{id}',['as'=>'alpha_admin_edit_account','uses'=>'Admin\User@edit']);

			Route::resource('accounts','Admin\User');
			Route::get('user/change-status/{id}/{status}',['as'=>'alpha_admin_user_change_status','uses'=>'Admin\User@status']);
			Route::get('my-account',['as'=>'alpha_admin_myaccount','uses'=>'Admin\User@myAccount']);
			Route::any('users/get-data',['as'=>'alpha_admin_user_get_data','uses'=>'Admin\User@getData']);

			Route::post('save-my-account',['as'=>'alpha_admin_savemyaccount','uses'=>'Admin\User@saveMyAccount']);

			//taxonomies
			Route::group(['prefix' => 'taxonomy'], function () {
				
				Route::get('/{type}',['as'=>'alpha_admin_taxonomy_index','uses'=>'Admin\Taxonomy@index']);
				Route::any('get-data/{type}',['as'=>'alpha_admin_taxonomy_get_data','uses'=>'Admin\Taxonomy@getData']);

				Route::get('/{type}/create',['as'=>'alpha_admin_taxonomy_create','uses'=>'Admin\Taxonomy@create']);
				Route::get('/{type}/edit/{id}',['as'=>'alpha_admin_taxonomy_edit','uses'=>'Admin\Taxonomy@edit']);
				Route::post('/delete/{id}',['as'=>'alpha_admin_taxonomy_delete','uses'=>'Admin\Taxonomy@delete']);
				Route::post('/{type}/save/{id}',['as'=>'alpha_admin_taxonomy_save','uses'=>'Admin\Taxonomy@save']);
				Route::post('/change-slug',['as'=>'alpha_admin_taxo_change_slug','uses'=>'Admin\Taxonomy@changeSlug']);
			});

			//entries
			Route::group(['prefix' => 'entry'], function () {
				
				Route::get('/{type}',['as'=>'alpha_admin_entry_index','uses'=>'Admin\Entry@index']);
				Route::any('get-data/{type}',['as'=>'alpha_admin_entry_get_data','uses'=>'Admin\Entry@getData']);

				Route::get('/{type}/create',['as'=>'alpha_admin_entry_create','uses'=>'Admin\Entry@create']);
				Route::get('/{type}/edit/{id}',['as'=>'alpha_admin_entry_edit','uses'=>'Admin\Entry@edit']);
				Route::post('/delete/{id}',['as'=>'alpha_admin_entry_delete','uses'=>'Admin\Entry@delete']);
				Route::post('/{type}/save/{id}',['as'=>'alpha_admin_entry_save','uses'=>'Admin\Entry@save']);
				Route::get('/status/{id}/{status}',['as'=>'alpha_admin_entry_status','uses'=>'Admin\Entry@status']);
				Route::post('/change-slug',['as'=>'alpha_admin_change_slug','uses'=>'Admin\Entry@changeSlug']);

				Route::get('/sequence/{EntryType}',['as'=>'alpha_admin_sequence_product','uses'=>'Admin\Entry@sequence']);
				Route::post('/save-sequence/{EntryType}',['as'=>'alpha_admin_save_sequence','uses'=>'Admin\Entry@saveSequence']);



			
			});			
			//media library
			Route::group(['prefix' => 'media'], function () {
				
				Route::any('get-data-media/{type}',['as'=>'alpha_admin_media_get_data','uses'=>'Admin\Media@getData']);
				Route::get('image',['as'=>'alpha_admin_media_image','uses'=>'Admin\Media@image']);
				Route::post('upload-image',['as'=>'alpha_admin_media_upload_image','uses'=>'Admin\Media@uploadImage']);

				Route::get('audio',['as'=>'alpha_admin_media_audio','uses'=>'Admin\Media@audio']);
				Route::post('upload-audio',['as'=>'alpha_admin_media_upload_audio','uses'=>'Admin\Media@uploadAudio']);

				Route::get('video',['as'=>'alpha_admin_media_video','uses'=>'Admin\Media@video']);
				Route::post('upload-video',['as'=>'alpha_admin_media_upload_video','uses'=>'Admin\Media@uploadVideo']);

				Route::get('other',['as'=>'alpha_admin_media_other','uses'=>'Admin\Media@other']);
				Route::post('upload-other',['as'=>'alpha_admin_media_upload_other','uses'=>'Admin\Media@uploadOther']);

				Route::post('embed-video-media',['as'=>'alpha_admin_media_embed_video','uses'=>'Admin\Media@embedVideo']);
				Route::get('entry-media',['as'=>'alpha_admin_media_entry','uses'=>'Admin\Media@entryMedia']);

				

				Route::post('delete-media',['as'=>'alpha_admin_media_delete','uses'=>'Admin\Media@delete']);
				Route::post('save-media',['as'=>'alpha_admin_media_save','uses'=>'Admin\Media@saveDescription']);

			});	

			//disccuss
			Route::group(['prefix' => 'discussions'], function () {
				Route::get('comments',['as'=>'alpha_admin_comments_index','uses'=>'Admin\Comment@view']);
				Route::any('comments-get-data',['as'=>'alpha_admin_comments_get_data','uses'=>'Admin\Comment@getData']);
			});

		});

	});

});