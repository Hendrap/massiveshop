
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Discussions</span> - Comments</h4>
        </div>

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>

                <li><a href="#">Disscussions</a></li>
                <li class="active">Comments</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h1 class="text-semibold">Comments</h1>
                        <p>Interact with your viewers</p>
                    </div>


                </div>
                <div class="panel-body">

                    <div class="table-header clearfix mb-10">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-5 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter</label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                                                 
                     
                         <select class="table-select2  pull-left filter-table-column" placeholder="Filter by Status" searchin="2">
                            <option></option>
                            <option value="all">Show All Status</option>
                            <option value="pending">Pending</option>
                            <option value="approved">Approved</option>
                            <option value="banned">Banned</option>
                            
                        </select>
                        
                          
                        
                      
 
                    </div>



                    <table class="table entry-table">
                        <thead>
                            <tr>


                                <th>Comments</th>
                                <th width="110px"><span>Date</span></th>
                                <th width="80px">Status</th>
                                <th width="50px" class="no-sort text-right">Option</th>
                                <th width="50px" class="no-sort text-right hide">Page</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>

                    <script>
                        $(document).ready(function() {



                            var entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table tbody');
                                     if(processing){
                                        
                                            $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#fff',
                                                    width: 'auto',
                                                    '-webkit-border-radius': 2,
                                                    '-moz-border-radius': 2,
                                                    backgroundColor: '#333'
                                                }
                                            });
                                     }else{
                                        $.unblockUI();
                                        $(".blockUI").remove()
                                     }
                            }).DataTable({
                                "order": [],
                                "searching" : true,
                                "lengthChange": false,
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "ajax": "{{ route('alpha_admin_comments_get_data') }}",
                                "columns": [
                                    { "data": "comment", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:""
                                    },
                                    { "data": "date", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    },
                                    sClass:"td-date"
                                    },
                                    { "data": "status", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    },
                                    sClass:""
                                    },
                                    { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:"text-right table-actions"
                                    }
                                ],
                                "columnDefs": [{
                                    "targets": 'no-sort',
                                    "orderable": false,
                                }]
                            });
                           
                            var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                   
                                });
                                
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                                
                            });
                            
                            $('#search_the_table').keyup(function(){
                                
                                
                                 
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                             $('.table-select2').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',

                                });
                            });
                            $('.table-select2.with-search').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: 1,
                                    placeholder: select_placeholder,
                                    width: '200px',

                                });

                            });
                        });

                    </script>


                </div>


            </div>

        </div>

    </div>



    <!-- Footer -->
   {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
