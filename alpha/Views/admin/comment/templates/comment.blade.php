                                <td>
                                    <div>
                                        <span class="cut-text">{{ sub_str(0,200,strip_tags($comment->content)) }}</span>

                                        <div class="comment-detail">
                                        <a href="{{ route('alpha_admin_edit_account',[$comment->user_id]) }}">
                                            {{ @$comment->user->username }}
                                        </a> 
                                        on 
                                        <a href="{{ route('alpha_admin_entry_edit',[$comment->entry->entry_type,$comment->entry->id]) }}">{{ parseMultiLang(@$comment->entry->title) }}</a></div>
                                    </div>
                                |alpha--datatable-separator--|</td>
                                |alpha--datatable-separator--|<td class="td-date">

                                   {{date_format(date_create($entry->published_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->published_at),app('AlphaSetting')->getSetting('time_format'))}}</span>

                                </td>
                                <?php 
                                    $class = 'info';
                                    if($comment->status == 'approved'){
                                        $class = 'success';
                                    }

                                    if($comment->status == 'banned')
                                    {
                                        $class = 'danger';
                                    }

                                 ?>
                                |alpha--datatable-separator--|<td><span class="label bg-{{$class}}">{{ucfirst($comment->status)}}</span></td>
                                |alpha--datatable-separator--|<td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            @if($comment->status != 'banned')
                                            <li><a href=""><i class="icon-cancel-square"></i> Banned</a></li>
                                            @endif
                                            @if($comment->status != 'approved')
                                            <li><a href=""><i class="icon-checkmark4"></i> Approved</a></li>
                                            @endif
                                            @if($comment->status != 'pending')
                                            <li><a href=""><i class="icon-clipboard2"></i> Pending</a></li>
                                            @endif
                                            <li class="border-top"><a href="#"><i class="icon-trash"></i> Delete</a></li>

                                        </ul>
                                    </div>
                                </td>
                               