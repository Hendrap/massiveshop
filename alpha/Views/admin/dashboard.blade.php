<!-- Content area -->
				<div class="content">


					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title text-uppercase">CMS Statistics</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li>
												<a data-action="collapse"></a>
											</li>
											<li>
												<a data-action="close"></a>
											</li>
										</ul>
									</div>
								</div>

								<div class="panel-body">

									<div class="row">

										<div class="col-sm-4">

											<div class="content-group">
												<h5 class="text-semibold no-margin"><i class="icon-compose position-left text-slate"></i> {{$pages}}</h5>
												<span class="text-muted text-size-small">Pages</span>
											</div>

										</div>
										<div class="col-sm-4">


											<div class="content-group">
												<h5 class="text-semibold no-margin"><i class="icon-file-empty position-left text-slate"></i> {{$entryTypes}}</h5>
												<span class="text-muted text-size-small">Entry Types</span>
											</div>

										</div>
										<div class="col-sm-4">

											<div class="content-group">
												<h5 class="text-semibold no-margin"><i class="icon-images3 position-left text-slate"></i> {{$medias}}</h5>
												<span class="text-muted text-size-small">Media Files</span>
											</div>

										</div>

									</div>

								</div>
							</div>

							<!-- <div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title text-uppercase">Catalogs</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li>
												<a data-action="collapse"></a>
											</li>
											<li>
												<a data-action="close"></a>
											</li>
										</ul>
									</div>
								</div>

								<div class="panel-body">

									<div class="row">

										<div class="col-sm-4">

											<h5 class="text-semibold no-margin"><i class=" icon-bag position-left text-slate"></i>100</h5>
											<span class="text-muted text-size-small">Products</span>

										</div>
										<div class="col-sm-4">
										
											<h5 class="text-semibold no-margin"><i class="icon-price-tags2 position-left text-slate"></i>100</h5>
											<span class="text-muted text-size-small">Brands</span>
											
										</div>
										<div class="col-sm-4">
										
											
											<h5 class="text-semibold no-margin"><i class="icon-cart-remove position-left text-slate"></i>0</h5>
											<span class="text-muted text-size-small">Out Of Stock</span>
										
										</div>

									</div>

								</div>

							</div>

 -->
						</div>
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title text-uppercase">User Statistics</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li>
												<a data-action="collapse"></a>
											</li>
											<li>
												<a data-action="close"></a>
											</li>
										</ul>
									</div>
								</div>

								<div class="panel-body">
									<div class="row">

										<div class="col-sm-4">

											<div class="content-group">
												<h5 class="text-semibold no-margin"><i class="icon-users position-left text-slate"></i> {{$user}}</h5>
												<span class="text-muted text-size-small">Total Users</span>
											</div>

										</div>
										<div class="col-sm-4">
											<div class="content-group">
												<h5 class="text-semibold no-margin"><i class="icon-user-plus position-left text-slate"></i> {{$newUser}}</h5>
												<span class="text-muted text-size-small">New User</span>
											</div>

										</div>
									</div>
									<hr>
									<div class="row">

										<div class="col-sm-4">

											<div class="content-group">
												<h5 class="text-semibold no-margin"><i class="icon-user-check position-left text-slate"></i> {{$activeSession}}</h5>
												<span class="text-muted text-size-small">Active Sessions</span>
											</div>

										</div>
										<div class="col-sm-4">


											<div class="content-group">
												<h5 class="text-semibold no-margin"><i class="icon-watch2 position-left text-slate"></i> {{ $lastLogin }}</h5>
												<span class="text-muted text-size-small">Last Login</span>
											</div>

										</div>
										<div class="col-sm-4">

											<div class="content-group">
												<h5 class="text-semibold no-margin"><i class="icon-watch2 position-left text-slate"></i> {{ $lastRegister }}</h5>
												<span class="text-muted text-size-small">Last Register</span>
											</div>

										</div>

									</div>
									<hr>
									<div class="row">
										<div class="col-md-12">

											<div class="row">
												<div class="col-sm-6">
													<h6>User Roles</h6></div>

											</div>
											<div class="chart-container">
												<div class="chart" id="user-role-chart"></div>
											</div>
											<script>
											<?php
											//dd($userByRole);
												$datachart = "[";
												foreach ($userByRole as $key => $value) {
													$datachart .= "['";
													$datachart .= $value->legendLabel."'";
													$datachart .= ",".$value->magnitude;
													$datachart .= "],";
												}
												$datachart .= ']';
											?>
												var chartuser = c3.generate({
													bindto: '#user-role-chart',
													data: {
														columns: {!! $datachart !!},
														type: 'pie',
													}
												});
											</script>


										</div>

									</div>


								</div>

							</div>
						</div>
					</div>

				{!! view('alpha::admin.partials.content-footer') !!}

				</div>
				<!-- /content area -->