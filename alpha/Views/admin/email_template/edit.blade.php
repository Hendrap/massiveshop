<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Templates</span> - Edit Template</h4>
        </div>

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('alpha_admin_entry_index',[$type]) }}">Templates</a></li>
                <li class="active">Edit Template</li>

            </ul>
        </div>

    </div>


    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
       <span class="text-semibold">Required field(s) error or missing</span>
    </div>

    <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
        <span class="text-semibold">Wrong format found</span>
    </div>
    <form name="alphaForm" action="{{ route('alpha_admin_entry_save',[$type,$entry->id]) }}" method="POST" class="form-horizontal">
        <input type="hidden" name="id" value="{{@$entry->id}}">
        <div class="row">

            <div class="col-sm-9">
                <div class="panel panel-flat">
                    <div class="panel-heading clearfix">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">{{ parseMultiLang($entry->title) }}</h5>
                        </div>
                    </div>
                    <div class="panel-body">
                        
                        @if(count($errors) > 0)
                        @foreach($errors->all() as $key => $error)
                        @if($key == 0)
                        <div class="alert bg-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            @if(Illuminate\Support\Str::contains($error,'required'))
                                <span class="text-semibold">Required field(s) error or missing</span>
                            @else
                                <span class="text-semibold">{{ str_replace('en.','',$error) }}</span>
                            @endif
                        </div>
                        @endif
                        @endforeach
                        @endif
                         <fieldset>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Template Title</label>
                                <div class="col-lg-10 clearfix">
                                @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                    <input style="display:none" id="title_{{$key}}" name="title[{{$key}}]" type="text" class="form-control" value="{{old('title.'.$key,parseMultiLang($entry->title,$key))}}">
                                @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Subject</label>
                                <div class="col-lg-10 clearfix">
                                    <input type="text" class="form-control" name="metas[subject]" value="{{ old('metas.subject',getEntryMetaFroMArray($entry->metas,'subject')) }}">
                                </div>
                            </div>
                            <div class="form-group">
                            @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                <div class="col-lg-12">
                                    <textarea style="display:none" id="content_{{$key}}" name="content[{{$key}}]" rows="10"  class="text-editor" cols="80" >{{old('content.'.$key,parseMultiLang($entry->content,$key))}}</textarea>
                                </div>
                            @endforeach
                            </div>
                            <div style="display: none" class="form-group">
                                <div class="col-lg-10">
                                    @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                        <textarea style="display:none" id="excerpt_{{$key}}" name="excerpt[{{$key}}]" class="form-control">{{old('excerpt.'.$key,parseMultiLang($entry->excerpt,$key))}}</textarea>
                                    @endforeach
                                </div>
                            </div>
                         </fieldset>
                            <div class="text-right">
                                
                                <a href="{{route('alpha_admin_entry_index',[$type])}}" class="btn bg-slate warning-page">Cancel</a>
                                
                                <button type="submit" class="btn btn-primary ml-10">Save</button>
                            </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="published" value="{{ date('Y-m-d H:i:s',strtotime(old('published_at',$entry->published_at))) }}">
                    <input type="hidden" name="status" value="published">
                </div>
            </div>


            <div class="col-sm-3">

             
                
                
                <div class="panel right-small">
                
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">Bracket Codes</h5>
                            
                        </div>


                    </div>
                    <div class="panel-body">
                        
                       <table class="table-bracket-code">
                        
                           <tr>
                                <td class="pr-10"><a href="#" class="add-code">[name]</a></td>
                                <td>Customer's Name</td>
                           </tr>
                           <tr>
                                <td class="pr-10"><a href="#" class="add-code">[email]</a></td>
                                <td>Customer's Email</td>
                           </tr>
                           <tr>
                                <td class="pr-10"><a href="#" class="add-code">[address]</a></td>
                                <td>Customer's Address</td>
                           </tr>
                           <tr>
                                <td class="pr-10"><a href="#" class="add-code">[shipto]</a></td>
                                <td>Customer's Shipping Address</td>
                           </tr>
                           <tr>
                                <td class="pr-10"><a href="#" class="add-code">[invoicenum]</a></td>
                                <td>Invoice Number</td>
                           </tr>
                           <tr>
                                <td class="pr-10"><a href="#" class="add-code">[invoice]</a></td>
                                <td>Invoice Content</td>
                           </tr>
                           <tr>
                                <td class="pr-10"><a href="#" class="add-code">[ordernum]</a></td>
                                <td>Order Number</td>
                           </tr>
                           <tr>
                                <td class="pr-10"><a href="#" class="add-code">[order]</a></td>
                                <td>Order List</td>
                           </tr>
                            <!-- dst-->
                       </table>
                        
                    </div>
                
                
                </div>
            </div>

        </div>

    </form>

     



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
    {!! view('alpha::admin.entry.templates.response-handler',['errors'=>@$errors]) !!}
    <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>
    <script>
        function hideAllTitle(){
        @foreach(Config::get('alpha.application.locales') as $key  => $lang)
            $("#title_" + "{{ $key }}").hide();
        @endforeach
        }
        function hideAllEditors(){
            @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                $("#cke_content_" + "{{ $key }}").hide();
                $("#excerpt_" + "{{ $key }}").hide();
            @endforeach
        }
        function showDefaultEditor(){
            $("#cke_content_" + defaultLang).show();
            $("#excerpt_" + defaultLang).show();
        }
        var defaultTitle = "{{ Config::get('alpha.application.default_locale') }}";
        var defaultLang = "{{ Config::get('alpha.application.default_locale') }}";
        @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                CKEDITOR.replace("content_" + "{{ $key }}",{height: '400px'});
        @endforeach
        CKEDITOR.on("instanceReady", function(event)
        {
            hideAllEditors();
            showDefaultEditor();
        });

        $("#changeLang").change(function(e){
            hideAllEditors();
            hideAllTitle();
            defaultLang = $("#changeLang option:selected").val();
            $("#title_" + defaultLang).show();
            showDefaultEditor();

        });

        $("#title_" + defaultLang).show();

        $("#title_" + defaultTitle).slug({
            slug: 'slug',
            hide:false
        });
    
        $(document).ready(function() {
    
                        initPreventClose();

           $('.select-language').each(function() {

                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    width: '200px',
                                });

            });
            
            autosize($('textarea'));
            
            
        });
         $(".add-code").click(function(e){
            e.preventDefault();
            CKEDITOR.instances["content_en"].insertHtml($(this).text());
        })
    </script>
