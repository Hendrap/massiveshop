                                <td class="title-column">
                                    <h6>
                                        <a href="{{ route('alpha_admin_entry_edit',[$type,$entry->id]) }}">
                                         @if($entry->entry_parent > 0)
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        @endif
                                        {{parseMultiLang($entry->title)}}
                                        <?php if(!empty($entry->parent)){ ?>
                                            <i class="text-info">Parent : {{parseMultiLang($entry->parent->title)}}</i>
                                        <?php } ?>
                                        </a>
                                    </h6>
                                </td>
                                |alpha--datatable-separator--|<td>{{$entry->user->username}}</td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->published_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->published_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                @if($entry->status =='published')
                                |alpha--datatable-separator--|<td class="hide">
                                <span class="label bg-success">
                                    @if(strtotime($entry->published_at) > time())
                                        Scheduled
                                    @else
                                        Published
                                    @endif
                                </span>
                                </td>
                                @elseif($entry->status =='disabled')
                                |alpha--datatable-separator--|<td class="hide"><span class="label bg-grey">Disabled</span></td>
                                @elseif($entry->status =='draft')
                                |alpha--datatable-separator--|<td class="hide"><span class="label bg-primary">Draft</span></td>
                                @endif
                                |alpha--datatable-separator--|<td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('alpha_admin_entry_edit',[$type,$entry->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
                                            @if($entry->status != 'disabled')
                                            <li><a href="{{ route('alpha_admin_entry_status',[$entry->id,'disabled']) }}"><i class="icon-cancel-square"></i> Disable</a></li>
                                            @endif
                                            @if($entry->status != 'published')
                                            <li><a href="{{ route('alpha_admin_entry_status',[$entry->id,'published']) }}"><i class="icon-checkmark4"></i> Publish</a></li>
                                            @endif
                                            @if($entry->status != 'draft')
                                            <li><a href="{{ route('alpha_admin_entry_status',[$entry->id,'draft']) }}"><i class="icon-clipboard2"></i> Draft</a></li>
                                            @endif
                                            
                                            <li class="divider"></li>
                                            
                                            <li><a href="#" class="confirm" data-id="{{$entry->id}}"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                                |alpha--datatable-separator--|<td>Category Cin</td>
