<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">{{$config['plural']}}</span> - Edit {{$config['single']}}</h4>
        </div>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('alpha_admin_entry_index',[$config['slug']]) }}">{{$config['plural']}}</a></li>
                <li class="active">Edit {{$config['single']}}</li>
            </ul>
        </div>
    </div>
    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Required field(s) error or missing</span>
                         </div>

                         <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                             <span class="text-semibold">Wrong format found</span>
                         </div>
    <form name="alphaForm" enctype="multipart/form-data" action="{{ route('alpha_admin_entry_save',[$type,$entry->id]) }}" method="POST" class="form-horizontal">
        <input type="hidden" name="id" value="{{@$entry->id}}">
        <div class="row">
            <div class="col-sm-9">
                <div class="panel panel-flat">
                    <div class="panel-heading clearfix">
                        @if(count(Config::get('alpha.application.locales')) > 1)
                        <div class="pull-right">
                            <select width="200" id="changeLang" class="select-language">
                                @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                    <option value="{{ $key }}">{{ $lang }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="panel-heading-title">
                            <div style="width:50%">
                                <h5 class="panel-title">{{ parseMultiLang($entry->title) }}</h5>
                            </div> 
                        </div>
                    </div>

                    <div class="panel-body">

                        @if(count($errors) > 0)
                        @foreach($errors->all() as $key => $error)
                        @if($key == 0)
                        <div class="alert bg-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            @if(Illuminate\Support\Str::contains($error,'required'))
                                <span class="text-semibold">Required field(s) error or missing</span>
                            @else
                                <span class="text-semibold">{{ str_replace('en.','',$error) }}</span>
                            @endif
                        </div>
                        @endif
                        @endforeach
                        @endif
                        
                        <fieldset>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Page Title</label>
                            <div class="col-lg-10 clearfix">
                                @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                    <input style="display:none" id="title_{{$key}}" name="title[{{$key}}]" type="text" class="form-control" value="{{old('title.'.$key,parseMultiLang($entry->title,$key))}}">
                                @endforeach
                                <div class="post-url pull-left"><?php echo url($config['slug']) ?>/<span class="slug">{{ old('slug',$entry->slug) }}</span>
                                    <input type="text" style="display:none" name="slug" class="slug form-control" value="{{ old('slug',$entry->slug) }}">
                                </div>&nbsp;&nbsp;&nbsp;
                                 <div class="display-inline-block mt-10 post-url-buttons">
                                    <a href="javascript:void(0)" class="edit-url-custom">Edit</a>
                                    <a href="javascript:void(0)" class="save-url-custom" style="display:none"><i class="icon-checkmark4" style="font-size:10px;"></i></a>
                                        <span class="separator-url-form" style="display:none;opacity:0;">|</span>
                                    <a href="javascript:void(0)" class="cancel-edit-url-custom" style="display:none"><i class="icon-cross" style="font-size:15px;"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                <div class="col-lg-12">
                                    <textarea style="display:none" id="content_{{$key}}" name="content[{{$key}}]" rows="10"  class="text-editor" cols="80" >{{old('content.'.$key,parseMultiLang($entry->content,$key))}}</textarea>
                                 </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Excerpt</label>
                            <div class="col-lg-10">
                                @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                    <textarea style="display:none" id="excerpt_{{$key}}" name="excerpt[{{$key}}]" class="form-control">{{old('excerpt.'.$key,strip_tags(parseMultiLang($entry->excerpt,$key)))}}</textarea>
                                @endforeach
                            </div>
                        </div>
                        @if(!empty($metas))
                        <?php $nMedia = 0; ?>
                        @foreach($metas as $meta)
                            @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'media_container' & $nMedia == 0)   
                                <div class="form-group">
                                <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                    <div class="col-lg-10">
                                        {!! view('alpha::admin.entry.templates.media-edit',['entry'=>$entry]) !!}
                                    </div>
                                </div>
                                <?php $nMedia++; ?>
                            @endif
                        @endforeach

                        @foreach($metas as $meta)
                             @if(!empty($meta->meta_key) && !empty($meta->meta_key) && (@$meta->meta_type == 'multi_lang' || empty($meta->meta_type)))
                                <div class="form-group">
                                    <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                    <div class="col-lg-10">
                                        @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                        <?php echo view('alpha::admin.meta.text',[
                                                            'meta_key' => $meta->meta_key,
                                                            'value' => old('multilangmetas.'.$meta->meta_key.'.'.$key,parseMultiLang(getEntryMetaFromArray($entry->metas,$meta->meta_key),$key)),
                                                            'isMultiLang' => true,
                                                            'lang' => $key
                                                             ]) ?>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endforeach


                       @foreach($metas as $meta)
                             @if(!empty($meta->meta_key) && !empty($meta->meta_key) && (@$meta->meta_type == 'default' || empty($meta->meta_type)))                        
                                <div class="form-group">
                                    <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                    <?php 
                                        $sizes = config('alpha.style.grids');
                                        $metaSize = @$sizes[$meta->meta_size];
                                        if(empty($metaSize)) $metaSize = 'col-lg-10';
                                        if($metaSize == 'col-lg-9') $metaSize = 'col-lg-10';
                                     ?>
                                    <div class="{{ $metaSize }}">
                                        <?php echo view('alpha::admin.meta.'.$meta->meta_data_type,[
                                                            'meta_key' => $meta->meta_key,
                                                            'value' => old('metas.'.$meta->meta_key,getEntryMetaFromArray($entry->metas,$meta->meta_key))
                                                             ]) ?>
                                    </div>
                                </div>
                            @endif
                        @endforeach


                        @foreach($metas as $meta)

                        @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'file_upload')                        
                            <div class="form-group" id="meta_{{$meta->meta_key}}">
                                <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                <div class="col-lg-10">
                                <div class="input-group">
                                    <input value="{{ getEntryMetaFromArray($entry->metas,$meta->meta_key) }}" readonly="" name="file_upload[{{$meta->meta_key}}]" id="file_upload_{{$meta->meta_key}}" style="height: 36px" type="text" class="form-control" placeholder="File name">
                                    <input class="meta_uploader" data-target="file_upload_{{$meta->meta_key}}" type="file" name="uploader[{{$meta->meta_key}}]" style="display:none">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-uploader-metas" type="button">
                                        @if(empty(getEntryMetaFromArray($entry->metas,$meta->meta_key)))
                                        Upload File
                                        @else
                                        Remove
                                        @endif
                                        </button>
                                    </span>
                                </div>
                                </div>
                            </div>
                        @endif

                        @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'media')         
                             <div class="form-group" id="meta_{{$meta->meta_key}}">
                                <label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <input value="{{ getEntryMetaFromArray($entry->metas,$meta->meta_key) }}" readonly="" name="media_container[{{$meta->meta_key}}]" id="media_container{{$meta->meta_key}}" style="height: 36px" type="text" class="form-control" placeholder="Media name">
                                        <input class="media_uploader" type="text" id="media_ids{{$meta->meta_key}}" name="media_ids[{{$meta->meta_key}}]" style="display:none">
                                        <span class="input-group-btn">
                                            <button data-meta="{{$meta->meta_key}}" class="btn btn-default btn-uploader-media" type="button">
                                            @if(empty(getEntryMetaFromArray($entry->metas,$meta->meta_key)))
                                            Open Media Library
                                            @else
                                            Remove
                                            @endif
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endif



                        @endforeach
                        @endif


                        </fieldset>
                        
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">Search Engine Optimization</h5>
                        </div>
                    </div>
                    <div class="panel-body">
                        
                        <fieldset>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="seo[title]" value="{{ old('seo.title',getEntryMetaFromArray($entry->metas,'seo_title')) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Keyword</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="seo[keys]" value="{{ old('seo.keys',getEntryMetaFromArray($entry->metas,'seo_keys')) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Description</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="seo[desc]" value="" style="height:80px;">{{ old('seo.desc',getEntryMetaFromArray($entry->metas,'seo_desc')) }}</textarea>
                            </div>
                        </div>
                            
                        </fieldset>
                        
                    </div>
                </div>
                
                
            </div>

            <div class="col-sm-3">
                <div class="panel right-small">
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">Publish</h5>
                        </div>
                    </div>
                    <div class="panel-body">
                    <?php $status = ['published','draft','disabled','scheduled_publish'] ?>
                    <?php

                        $selectedStatus = old('status',$entry->status);
                        if(strtotime($entry->published_at) > time())
                        {
                            $selectedStatus = 'scheduled_publish';
                        }
                     ?>
                        
                        
                        <div class="mr-10 ml-10">
                            <div class="form-group mb-10">
                                <select class="select-two form-control @if(count($errors->get('status'))) invalid-input @endif" name="status" id="publish_option">
                                   @foreach($status as $s)
                                        <option <?php echo ($s == $selectedStatus) ? 'selected' : '' ?> value="{{ $s }}"><?php $s = str_replace('_publish','',$s) ?>{{  Illuminate\Support\Str::title($s) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group has-feedback publish-date mb-10" style="<?php echo (old('status',$selectedStatus) == 'scheduled_publish') ? '' : 'display: none' ?> ">
                                <?php 
                                    $date = old('published_at',$entry->published_at);
                                    if(empty($date)) $date = date('Y-m-d H:i:s');
                                 ?>
                                <input type="text" name="published_at" class="date-picker form-control" value="{{ date('d M Y g:i a',strtotime($date)) }}">
                                <div class="form-control-feedback">
                                    <i class="icon-calendar" style="line-height:30px;"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="display-block">Created: {{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                <span class="display-block">Modified: {{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                            </div>
                                
                            <div class="form-group no-margin-bottom">
                                <a href="javascript:void(0)" class="btn btn-danger pull-left deleteEntry">Delete</a>
                                <button type="submit" class="btn btn-primary pull-right ml-10">Save</button>
                                <a href="{{route('alpha_admin_entry_index',[$type])}}" class="btn btn-default pull-right">Cancel</a>
                            </div>                            
                        </div>

                        </fieldset>
                        
                    </div>
                </div>
                
                <div class="panel right-small">
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">Taxonomy</h5>
                        </div>
                    </div>
                    <div class="panel-body">
                        
                        <fieldset>
                        
                        <div class="form-group no-margin-left no-margin-right">
                            <label>Parent</label>
                            <select class="entry-parent" placeholder="Select Parent" name="parent">
                              <option selected="" value="0">None</option>
                                @foreach($parents as $parent)
                                    <option <?php echo ($parent->id == old('parent',$entry->entry_parent)) ? 'selected' : '' ?> value="{{ $parent->id }}">{{ parseMultiLang($parent->title) }}</option>
                                    @if(!empty($parent->childs))
                                    @foreach($parent->childs as $p)
                                        <option <?php echo ($p->id == old('parent',$entry->entry_parent)) ? 'selected' : '' ?> value="{{ $p->id }}">&mdash; {{ parseMultiLang($p->title) }}</option>
                                    @endforeach
                                    @endif
                                @endforeach
                            </select>
                        </div>
                      
                        
                        <!--if category number < 5-->
                        @foreach($parsedTaxonomies['triTaxo'] as $key => $value)
                            <div class="form-group no-margin-left no-margin-right">
                                <label>{{  $infoTaxoContainer[$key]['single']  }}</label>
                                @foreach($value as $item)
                                    <div class="checkbox">
                                        <label>
                                            <input <?php echo in_array($item->id, old('taxonomies',$selectedTaxo)) ? 'checked' : '' ?> name="taxonomies[]" value="{{ $item->id }}" type="checkbox" class="styled">
                                            {{$item->name}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                         <!--/if category number < 5-->
                        
                        <!--if category number >= 5-->
                        @foreach($parsedTaxonomies['fiveTaxo'] as $key => $value)
                        <div class="form-group no-margin-left no-margin-right">
                            <div class="multi-select-full">
                                <select data-placeholder="Select {{ strtolower($infoTaxoContainer[$key]['single']) }}" name="taxonomies[]" class="multiselect-filtering" multiple="multiple">
                                @foreach($value as $item)
                                    <option <?php echo in_array($item->id, old('taxonomies',$selectedTaxo)) ? 'selected' : '' ?> value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        @endforeach
                        <!--/if category number >= 5-->
                        
                         <div class="form-group no-margin-left no-margin-right" style="margin-bottom:5px">
                            <label>Keyword Tags</label>
                            <input placeholder="Your keyword tags" data-role="tagsinput" value="{{ old('metas.alpha_keyword_tags',getEntryMetaFromArray($entry->metas,'alpha_keyword_tags')) }}" name="metas[alpha_keyword_tags]" type="text" class="keyword-tag form-control" style="width:100%;!important" readonly>
                        </div>
                        <div class="form-group clearfix add-keywords no-margin-left no-margin-right">
                             <div class="input-group">
                                 <input type="text" placeholder="Start typing your keyword tags" class="form-control pull-left keyword-input">
                                 <span class="input-group-btn">
                                    <button class="btn btn-primary pull-left add-tag-button" type="button">+</button>
                                </span>
                             </div>
                        </div>
                            
                        </fieldset>
                    </div>
                
                
                </div>
                

            </div>
        </div>

    </form>

     



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
     <form style="display:none" id="form_delete" action="{{route('alpha_admin_entry_delete',[$entry->id])}}" method="POST">
        {{csrf_field()}}
        <input type="hidden" name="id" value="<?php echo $entry->id ?>" >
    </form>
    <script type="text/javascript">
        $(document).on('click',".deleteEntry",function(e){
             e.preventDefault();
             swal({
            title: "Delete ?",
            text: "You will not be able to recover deleted items, are you sure want to delete ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true },
            function(isConfirm){
                if(isConfirm){
                    $("#form_delete").submit();
                }
            });
      });
    </script>
    {!! view('alpha::admin.entry.templates.response-handler',['errors'=>@$errors]) !!}
    <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(document).on('click','.delete-img-entry-media',function(e){
            e.preventDefault();
            var that = $(this)
            swal({
                title: "Delete ?",
                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true },
                function(isConfirm){
                    if(isConfirm){
                        $(that).parent().parent().parent().remove();
                    }
                });
        });
        $( "#containerMedia" ).sortable({
          revert: true
        });
        function hideAllTitle(){
        @foreach(Config::get('alpha.application.locales') as $key  => $lang)
            $("#title_" + "{{ $key }}").hide();
            $(".multilangmetas_" + "{{ $key }}").hide();
        @endforeach
        }
        function hideAllEditors(){
            @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                $("#cke_content_" + "{{ $key }}").hide();
                $("#excerpt_" + "{{ $key }}").hide();
            @endforeach
        }
        function showDefaultEditor(){
            $("#cke_content_" + defaultLang).show();
            $("#excerpt_" + defaultLang).show();
        }
        var defaultTitle = "{{ Config::get('alpha.application.default_locale') }}";
        var defaultLang = "{{ Config::get('alpha.application.default_locale') }}";
        @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                CKEDITOR.replace("content_" + "{{ $key }}",{height: '400px'});
        @endforeach
        CKEDITOR.on("instanceReady", function(event)
        {
            hideAllEditors();
            showDefaultEditor();
        });

        $("#changeLang").change(function(e){
            hideAllEditors();
            hideAllTitle();
            defaultLang = $("#changeLang option:selected").val();
            $("#title_" + defaultLang).show();
             $(".multilangmetas_" + defaultLang).show();
            showDefaultEditor();

        });

        $("#title_" + defaultLang).show();
        $(".multilangmetas_" + defaultLang).show();
        // $("#title_" + defaultTitle).slug({
        //     slug: 'slug',
        //     hide:false
        // });
    
            $('.additional-info-panel .date-picker').addClass('datepicker-top').removeClass('date-picker');
        
    $('.datepicker-top').daterangepicker({
                    timePicker: true,
                    opens: "left",
                    applyClass: 'bg-slate-600',
                    cancelClass: 'btn-default',
                    singleDatePicker: true,
                    autoApply : true,
                    drops: "up",
                    locale: {
                        format: 'MM/DD/YYYY h:mm a'
                    }
    });
            
        $(document).ready(function() {
                templateAddToEntry = '<?php echo newLine(view('alpha::admin.media.templates.img-entry-media',['img'=>'','id'=>0])) ?>';

                        initPreventClose();

           $('.select-language').each(function() {

                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    width: '200px',
                                });

            });
            

            $('.entry-parent').each(function() {
                select_placeholder = $(this).attr('placeholder');
                $(this).select2({
                     placeholder: select_placeholder,

                });
             });


            autosize($('textarea'));
            if($("#publish_option").val() == 'scheduled_publish')
            {
                $(".publish-date").show();
            }
            
        });
        var elem = $('.add-edit-product-photos-content');
        var eleminner = $('.product-photo-container');
        var eleminnertwo = $('.product-photo-container-add');
            
            function checkimagescroll(){
               
                totalinnerwidth = eleminner.innerWidth() + eleminnertwo.innerWidth() - 12;
                if(elem.scrollLeft() + elem.width() ==  totalinnerwidth){
                    $('.product-photo-navigation.next').addClass('hide');
                }
                if(elem.scrollLeft() + elem.width() <  totalinnerwidth){
                    $('.product-photo-navigation.next').removeClass('hide');
                }
                if(elem.scrollLeft()  ==  0){
                    $('.product-photo-navigation.prev').addClass('hide');
                }
                if(elem.scrollLeft()  >  0){
                    $('.product-photo-navigation.prev').removeClass('hide');
                }
            }
            checkimagescroll();
            
            
            $('.product-photo-navigation.next a').click(function(e){
                e.preventDefault();
                elem.get(0).scrollLeft += 250;
                checkimagescroll();
            });
            $('.product-photo-navigation.prev a').click(function(e){
                e.preventDefault();
                elem.get(0).scrollLeft -= 250;
                checkimagescroll();
            });


            //slug
            
             var currentSlug = "<?php echo $entry->slug ?>";
             $('.edit-url-custom').click(function(e){
                e.preventDefault;
                $('.post-url span').hide();
                $('.post-url input').show();
                $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').show();
                $('.edit-url-custom').hide();
            });
            
            
            $('.save-url-custom').click(function(e){
                e.preventDefault;
                newurl = $('.post-url input').val();
                currentSlug = newurl;
                $.ajax({
                    url:"<?php echo route("alpha_admin_change_slug") ?>",
                    data:{slug:currentSlug,entry_id:"<?php echo $entry->id ?>"},
                    type:"POST",
                    dataType:"json",
                    success:function(data){
                        $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').hide();
                        $('.post-url span').show();
                        $('.post-url input').hide();
                        $('.edit-url-custom').show();
                        currentSlug = data.slug;
                        $('.post-url span').html('').html(currentSlug);
                        $('.post-url input').val(currentSlug);;
                    }
                });
                
            });
            
            $('.cancel-edit-url-custom').click(function(e){
                e.preventDefault;
                $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').hide();
                $('.post-url span').show();
                $('.post-url input').hide();
                $('.edit-url-custom').show()
                $('.post-url input').val(currentSlug);;
                
            });
            



    </script>
    <style>
        
        .bootstrap-tagsinput input {
            
            width: 100%!important;
        }
        
    </style>
     <script type="text/javascript">
        $(".btn-uploader-metas").click(function(e){
            e.preventDefault();
            if($(this).text().trim() == "Upload File")
            {
                $(this).parent().parent().parent().find('[type=file]').click();
                $(this).text("Remove");
            }else{
                $(this).parent().parent().parent().find('[type=file]').val("");
                $(this).parent().parent().parent().find('[type=text]').val("");
                $(this).text("Upload File");
            }
            
        });
        $(".meta_uploader").change(function(e){
            $("#" +  $(this).data('target')).val($(this).get(0).files[0].name);
        });
         $(".btn-uploader-media").click(function(e){
            e.preventDefault();
            if($(this).text().trim() == "Open Media Library")
            {
                var meta = $(this).data('meta');
                loadMedias("<?php echo route('alpha_admin_media_entry')."?page=1" ?>",function(){
                    $("#modalEntryMedia").modal('show');    
                    editorName = 'metas.' + meta;
                });
            }else{
                $(this).parent().parent().find("input").val("");
                $(this).parent().parent().find("button").html("Open Media Library")
            }

        });
    </script>
