                                <td class="sorter">
                                    <i data-after="<?php echo (int)$entry->after ?>" data-before="<?php echo (int)$entry->before ?>" data-entry-id="<?php echo $entry->id ?>" class="icon-menu-open entry-data-container"></i>
                                </td>
                                |alpha--datatable-separator--|<td class="title-column">
                                    <h6>
                                        
                                         @if($entry->entry_parent > 0)
                                            @if($isSearch == false)
                                            &mdash;
                                                @if(isset($lastChild))
                                                    &mdash;
                                                @endif
                                            @endif
                                        @endif
                                        <a href="{{ route('alpha_admin_entry_edit',[$type,$entry->id]) }}">
                                        {{parseMultiLang($entry->title)}}
                                        <?php if(!empty($entry->parent)){ ?>
                                            <i class="text-info">Parent : {{parseMultiLang($entry->parent->title)}}</i>
                                        <?php } ?>
                                        </a>
                                    </h6>
                                </td>
                                |alpha--datatable-separator--|<td>{{@$entry->user->username}}</td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->published_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->published_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                @if($entry->status =='published' && strtotime($entry->published_at) <= time())
                                |alpha--datatable-separator--|<td class="hide"><span class="label bg-success">Published</span></td>
                                @elseif($entry->status == 'published' && strtotime($entry->published_at) > time())
                                |alpha--datatable-separator--|<td class="hide"><span class="label bg-primary">Scheduled</span></td>
                                @elseif($entry->status =='disabled')
                                |alpha--datatable-separator--|<td class="hide"><span class="label bg-danger">Disabled</span></td>
                                @elseif($entry->status =='draft')
                                |alpha--datatable-separator--|<td class="hide"><span class="label bg-orange">Draft</span></td>
                                @endif
                                |alpha--datatable-separator--|<td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('alpha_admin_entry_edit',[$type,$entry->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
                                            @if($entry->status != 'disabled' && $entry->status  != 'published')
                                            <li><a href="{{ route('alpha_admin_entry_status',[$entry->id,'disabled']) }}"><i class="icon-cancel-square"></i> Disable</a></li>
                                            @endif
                                            @if($entry->status != 'published')
                                            <li><a href="{{ route('alpha_admin_entry_status',[$entry->id,'published']) }}"><i class="icon-checkmark4"></i> Publish</a></li>
                                            @endif
                                            @if($entry->status != 'draft')
                                            <li><a href="{{ route('alpha_admin_entry_status',[$entry->id,'draft']) }}"><i class="icon-clipboard2"></i> Draft</a></li>
                                            @endif
                                            <li class="divider"></li>
                                            <li><a href="#" class="confirm" data-id="{{$entry->id}}"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                                |alpha--datatable-separator--|<td>Category Cin</td>
