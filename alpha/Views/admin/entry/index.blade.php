<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">{{ $info['plural'] }}</span> - All {{ $info['plural'] }}</h4>
        </div>

        <div class="heading-elements">
                <a href="{{route('alpha_admin_entry_create',$type)}}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>New {{ $info['single'] }}</a>
        </div>
        
    </div>
</div>
<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
           
                <li><a href="{{ route('alpha_admin_entry_index',[$info['slug']]) }}">{{ $info['plural'] }}</a></li>
                <li class="active">All {{ $info['plural'] }}</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">All {{ $info['plural'] }}</h5>
                        
                    </div>

                   
                </div>

                <div class="panel-body">
                    @if(!empty($info['custom_text']))
                    <p>{{ $info['custom_text'] }}</p>
                    @else
                    <p>Manage all available {{ strtolower($info['plural']) }}</p>
                    @endif
                </div> 
                
                    <div class="table-header datatable-header clearfix">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-5 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter:&nbsp;</label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                        
                        <select class="table-select2 with-search pull-left filter-table-column" placeholder="Filter by Category" searchin="7">
                            <option value="all">All Categories</option>
                            @foreach($taxoInfo->taxo as $key => $taxo)
                            <optgroup label="{{$taxo['plural']}}">
                                @foreach($taxonomies as $v)
                                    @if($v->taxonomy_type == $taxo['slug'])
                                         <option value="{{$v->id}}">{{$v->name}}</option>
                                    @endif
                                @endforeach
                            </optgroup>
                            @endforeach
                            
                        </select>
                         

                        
                         <select class="table-select2  pull-left filter-table-column" placeholder="Filter by Status" searchin="5">
                            <option value="all">All Status</option>
                            <option value="published">Published</option>
                            <option value="disabled">Disabled</option>
                            <option value="draft">Draft</option>
                            <option value="scheduled_publish">Scheduled</option>
                        </select>
                        @if(@$info['reorder'] == 'yes')
                        <div style="display: none">
                             <a href="javascript:void(0)" class="btn btn-primary set-order pull-right">Set Order</a>
                             <a href="javascript:void(0)" class="btn btn-default set-order-done pull-right hide">Done</a>                            
                        </div>
                         <label class="checkbox-inline checkbox-right checkbox-switchery pull-right">
                            <input id="toggleOrder" type="checkbox" class="switchery order-switch"> Set Order
                         </label>
                         <script type="text/javascript">
                              var elemswitch= Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                              elemswitch.forEach(function(html) {
                                    var switchery = new Switchery(html);
                               });
                              $("#toggleOrder").click(function(e){

                                if($("#toggleOrder").is(':checked')){
                                    $(".set-order").trigger('click');
                                }else{
                                    $(".set-order-done").trigger('click');
                                }

                              });
                         </script>
                        @endif
                        
 
                    </div>
                    
                    
                    <table class="table entry-table table-striped">
                        <thead>
                            <tr>
                                <th width="50px" class="sorter no-sort">&nbsp;</th>
                                <th>Page</th>
                                <th width="140px">Creator</th>
                                <th width="140px" class="hide"><span>Published</span></th>
                                <th width="140px"><span>Created</span></th>
                                <th width="140px"><span>Modified</span></th>
                                <th width="120px">Status</th>
                                <th width="50px" class="no-sort text-right"></th>
                                <th width="50px" class="no-sort text-right hide">Category</th>
                            </tr>
                        </thead>
                        <tbody>

                         </tbody>
                    </table>
                    
                    <form style="display:none" id="form_delete" action="{{route('alpha_admin_entry_delete',[0])}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="0" >
                    </form>
                    <script type="text/javascript">
                        var status = "{{session('msg')}}";
                        if (status === 'Status Changed!') {
                                swal({
                                    title: "SUCCESS",
                                    text: "All changes has been saved successfuly",
                                    confirmButtonColor: "#66BB6A",
                                    type: "success",
                                    closeOnConfirm: false,
                                    },function(isConfirm){
                                        swal.disableButtons();
                                        swal.close();
                                    });
                        }
                    </script>
                    <script type="text/javascript">
                        var entryGetDataUrl = "{{ route('alpha_admin_entry_get_data',[$type]) }}";
                        var urlSaveSequence = "{{ route('alpha_admin_save_sequence',[$type]) }}";
                        var entryType = "<?php echo $type ?>";
                        $(document).on('click',".confirm",function(e){
                                e.preventDefault();
                                $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
                                $("#form_delete").find("[name=id]").val($(this).data('id'));
                                 swal({
                                title: "Delete ?",
                                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                closeOnConfirm: false },
                                function(isConfirm){
                                    if(isConfirm){
                                        $("#form_delete").submit();
                                    }
                                });
                            });

                    </script>
                    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/tables/datatables/extensions/row_reorder.min.js') }}"></script>
                    <script type="text/javascript" src="{{ asset('backend/assets/js/entry.js') }}"></script>
                


            </div>

        </div>

    </div>



    <!-- Footer -->
   {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->

