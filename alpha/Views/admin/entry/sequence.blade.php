<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Entry</span> - {{$info['plural']}}</h4>
        </div>
        <div class="heading-elements">
            <button id="saveData" class="btn btn-labeled pull-right bg-brand"><b><i class="icon-file-download"></i></b>Save</button>
        </div>


    </div>


</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="javascript:void(0)">Entry</a></li>
                <li class="active">{{$info['plural']}}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">{{$info['plural']}}</h5>
                    </div>
                </div>

                <div class="media-list-body">

                                    <div id="containerMedia" class="row">
                                        @foreach($entries as $entry)
                                        <div data-entry-id="{{$entry->id}}" class="col-md-2 entry-item">
                                                
                                                <div class="thumbnail">
                                                    <div class="thumb">
                                                        {{ parseMultiLang($entry->title) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                    </div>

                            </div>

            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
     $( "#containerMedia" ).sortable({
                      revert: true
    });
</script>
<script type="text/javascript">
                    $("#saveData").click(function(e){
                        e.preventDefault();
                        var arr = new Array();
                        var entries = $(".entry-item");
                        n = 1;
                        $.each(entries,function(i,k){
                            obj = {};
                            obj.sequence = n;
                            obj.entry_id = $(k).data('entry-id');
                            arr.push(obj);
                            n++;
                        });

                        $("#containerMedia").block({
                           message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                           overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#333',
                                width: 'auto',
                            }
                        });

                        $.ajax({
                            url:"<?php echo route('alpha_admin_save_sequence_product',[$type]) ?>",
                            data:JSON.stringify(arr),
                            type:"POST",
                            dataType:"JSON",
                            success:function(data){
                                $("#containerMedia").unblock()
                                if(data.status == 1){
                                    swal({
                                        title: "SUCCESS",
                                        text: "All changes has been saved successfuly",
                                        confirmButtonColor: "#66BB6A",
                                        type: "success"
                                        });
                                }
                            },
                            error:function(data){
                                $("#containerMedia").unblock()
                                swal({
                                        title: "Error!",
                                        text: "Internal Server Error!",
                                        type: "error"
                                    });
                            }

                        });
                    })
</script>