<?php 
	    $err = count($errors);
	    $errMsg = "";
?>
<script type="text/javascript">
	$(document).ready(function(){
		var status = "{{session('msg')}}";
		var err = "{{$err}}";
	    if (status === 'Data Saved!') {
	        swal({
	            title: "SUCCESS",
	            text: "All changes has been saved successfuly",
	            confirmButtonColor: "#66BB6A",
	            type: "success",
	            html: true
	            });
	    }
	})
</script>


{!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'title[en]' => 'required',
                ]

            ]); 
        !!}