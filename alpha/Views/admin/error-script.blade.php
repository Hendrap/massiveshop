 <script type="text/javascript">
        function initToolTipError(el,msg){
            $(el).parent().addClass('has-error');
            $(el).tooltip({
                    animation:true,
                    placement:'bottom',
                    title:msg,
                    trigger:"focus",
                    template:'<div class="tooltip red-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                });
        }
        function destroyTooltipError(list){
            $("#errorMsgRequired").hide();
            $("#errorMsgWrongFormat").hide();
            var hasErrors = $(".has-error");
            
            $.each(hasErrors,function(i,k){
                $(k).removeClass('has-error');
            });


            $.each(list,function(i,k){
                $(k).tooltip('destroy');
            });
        }
    </script>
     @if(count($errors) > 0)
        <script type="text/javascript">
            @foreach($errors->keys() as $k => $key)
                initToolTipError("#" + "{{ str_replace('.','_',$key)}}","{{ str_replace('metas.','',@$errors->get($key)[0]) }}");
            @endforeach
        </script>
    @endif
        <script type="text/javascript">
        var validator = new FormValidator('alphaForm', [
            @foreach($fields as $key => $value)
            {
                name: "{{$key}}",
                rules: '{{$value}}'
            },
            @endforeach
        ], function(errors, event) {
            destroyTooltipError($("[name=alphaForm]").find('input'));
             destroyTooltipError($("[name=alphaForm]").find('select'));
            if (errors.length > 0) {
                for (var i = errors.length - 1; i >= 0; i--) {
                    error = errors[i];
                    initToolTipError("#" + error.id,error.message.replace('_',' ').replace('metas[','').replace(']','').replace('[',' '));  
                    if(error.rule == 'required')
                    {
                        $("#errorMsgRequired").show();
                                              
                    }
                    if(error.rule == 'numeric' || error.rule == 'valid_url' || error.rule == 'valid_email')
                    {
                        $("#errorMsgWrongFormat").show();
                    }
                }
                return false;
            }
        });
    </script>  

