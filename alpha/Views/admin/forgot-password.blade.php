 <form name="alphaForm" method="POST" action="{{ route('alpha_post_forgot_password') }}" id="login-form">
                        <div class="panel panel-body login-form">
                            
                            <div class="text-center">
                                <div class="logo">
                                    <img src="{{ asset('backend/assets/images/alpha-logo-dark.png') }}" alt="logo">
                                </div>
                                <h1 style="margin-bottom:35px;">Please provide your email address</h1>
                               
                            </div>
                            <?php
                               $n = 0;
                         ?>
                      
                            @if(count($errors) > 0)
                            @foreach($errors as $key => $error)
                            @if($n == 0)
                            <div class="alert bg-danger alert-styled-left">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                            </div>
                            <?php $n++; ?>
                            @endif
                            @endforeach
                            @endif
                             <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                                <span class="text-semibold">Required field(s) error or missing</span>
                             </div>

                             <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                                 <span class="text-semibold">Wrong format found</span>
                             </div>


                            <div class="form-group clearfix has-feedback has-feedback-left" style="margin-bottom:35px">


                                <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="Email" class="form-control">
                                <div class="form-control-feedback">
                                    <i class="icon-envelop"></i>
                                </div>

                            </div>
                            {!! csrf_field() !!}

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-login btn-primary">Submit</button>

                            </div>

                            
                        </div>
                    </form>
                     <?php
                           $n = 0;
                     ?>
                    <script type="text/javascript">
                        var errorClass = "invalid-input";
                        @if(count($errors) > 0)
                        @foreach($errors as $key => $error)
                        @if($n == 0)
                            $("#" + "{{ $key }}").addClass(errorClass);
                            <?php $n++; ?>
                        @endif
                        @endforeach
                        @endif
                    </script>

                     {!! view('alpha::admin.error-script',[
                                'errors'=>[],
                                'fields' => [
                                    'email' => 'required|valid_email',
                                ]

                        ]); 
                   !!}