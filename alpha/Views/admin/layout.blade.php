<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title or 'Alpha CMS' }}</title>

    <!-- Global stylesheets -->
    
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('bundles/img/favicon.png') }}">
    <link href="{{ asset('backend/assets/css/jquery-ui.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/swiper.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/project-color.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/video-js.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/token-input.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/chartist.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    
    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <!-- /core JS files -->

    <!--charts-->
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/visualization/c3/c3.min.js')}}"></script>
    <!--/charts-->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/pages/datatables_responsive.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/pickers/anytime.min.js')}}"></script>

<!--     <script type="text/javascript" src="{{ asset('backend/assets/js/core/libraries/jquery_ui/full.min.js')}}"></script> -->
    <script type="text/javascript" src="{{ asset('backend/assets/js/jquery-ui.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/validation/validate.min.js')}}"></script>
<!--     <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/notifications/pnotify.min.js')}}"></script>
 -->    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/inputs/autosize.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/sliders/swiper.jquery.min.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/custom.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/pages/form_select2.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/pages/datatables_basic.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/notifications/sweet_alert.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/sticky-kit/jquery.sticky-klt.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/jquery.slug.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/charts/chsrtist/chartist.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/validate.min.js') }}"></script>

    <!-- /theme JS files -->


    <!--custom css-->
    <link href="{{ asset('backend/assets/css/custom.css')}}" rel="stylesheet" type="text/css">
    

      <script type="text/javascript">
      //ini di ppakai buat alpha media image
      var editorName = '';
      var mediaSite = "<?php echo route('alpha_admin_media_entry')."?editor=true&page=1" ?>";
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
          
        $(document).ready(function(){
            
            $('#search_the_table').attr('placeholder', 'Type to filter...');
            $('.breadcrumb-line').parent().remove();
            
            
        });
        $(window).load(function() {
              $('td.text-bold').addClass('title-column').removeClass('text-bold');
             $('.dataTables_filter input').attr('placeholder', 'Type to filter...');
        });
    </script>

</head>

    <body class="navbar-bottom">
 


    
    	@include('alpha::admin.partials.header')
        
        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">
                @include('alpha::admin.partials.sidebar')
                <!-- Main content -->
                <div class="content-wrapper">


                    <?php
                        if(!empty($content)){
                            echo $content;
                        }else{
                            //echo view('alpha::admin.empty');
                        }
                    ?>
                    
                </div>

            </div>


        </div>
        @include('alpha::admin.partials.footer')
    </body>
    <?php
        $pathMedia =  strtolower(Request::path());

        if(!Illuminate\Support\Str::startsWith($pathMedia,'admin/media')){
        	echo view('alpha::admin.media.modal.entry-media');
        }
    ?>
    </html>
