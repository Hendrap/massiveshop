<form method="POST" action="{{ route('alpha_post_login') }}" id="login-form">
                        <div class="panel panel-body login-form">
                        

                            <div class="text-center">
                                <div class="logo">
                                    <img src="{{ asset('backend/assets/images/loginlogo.png') }}" alt="Alpha logo">
                                </div>
                                <h1>Please log in to continue</h1>
                            </div>
                            <?php
                               $n = 0;
                         ?>
                      
                            @if(count($errors) > 0)
                            @foreach($errors as $key => $error)
                            @if($n == 0)
                            <div class="alert bg-danger alert-styled-left">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                            </div>
                            <?php $n++; ?>
                            @endif
                            @endforeach
                            @endif
                            <fieldset>
                            
                            <div class="form-group clearfix has-feedback has-feedback-left">
                                <input id="email" type="text" name="email" value="{{ old('email') }}"  placeholder="Email" class="form-control">
                                <div class="form-control-feedback">
                                    <i class="icon-user"></i>
                                </div>
                               
                            </div>
                            <div class="form-group clearfix has-feedback has-feedback-left" style="margin-bottom:40px;">
                                
                                <input id="password" type="password" name="password" value="{{ old('password') }}"  placeholder="Password" class="form-control">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2"></i>
                                </div>
                            </div>

                            </fieldset>
                            
                            <div class="form-group text-center" style="margin-bottom:10px;">
                                <button style="background-color: #bfd400;border-color: #bfd400;" type="submit" class="btn btn-login btn-primary">Log In</button>
                            </div>

                                
                            <div class="text-center">
                                 <a style="color:#bfd400" href="{{ route('alpha_forgot_password') }}">Forgot password?</a>
                             </div>
                        </div>
                    </form>
                    <?php
                           $n = 0;
                     ?>
                    <script type="text/javascript">
                    var errorClass = "invalid-input";
                    @if(count($errors) > 0)
                    @foreach($errors as $key => $error)
                    @if($n == 0)
                        $("#" + "{{ $key }}").addClass(errorClass);
                        <?php $n++; ?>
                    @endif
                    @endforeach
                    @endif
                    </script>