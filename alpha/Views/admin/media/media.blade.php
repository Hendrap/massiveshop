<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Media Library</span> - {{ str_plural(ucfirst($media_type)) }}</h4>
        </div>
        <div class="heading-elements">
            @if($media_type == 'audio')
            <div class="button-groups pull-right" id="btnTriggerAdd{{$media_type}}">
                <label for="upload_image no-margin">
                    <label for="upload_image" class="btn btn-labeled pull-right bg-brand"><b><i class="icon-file-upload"></i></b>Upload</label>
                </label>
                <input type="file" id="btnUpload{{$media_type}}" class="hide" multiple="">
            </div>
            @else
            
                        @if($media_type == 'video')
                        <div id="btnEmbedVideo" class="button-groups pull-right mr-20">
                            <label>
                                <label class="btn btn-labeled pull-right bg-brand"><b><i class="icon-embed"></i></b>Embed Video</label>
                            </label>
                        </div>
                        @endif
                        <div class="button-groups pull-right mr-20" id="btnTriggerAdd{{$media_type}}">
                            <label for="upload_image">
                                <label for="upload_image" class="btn btn-labeled pull-right bg-brand"><b><i class="icon-file-upload"></i></b>Upload</label>
                            </label>
                            <input type="file" id="btnUpload{{$media_type}}" class="hide" multiple="">
                        </div>
            
            @endif
        </div>


    </div>


</div>

<div class="content">
    <div class="panel panel-flat">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="javascript:void(0)">Media Library</a></li>
                <li class="active">{{ str_plural(ucfirst($media_type)) }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">{{ str_plural(ucfirst($media_type)) }}</h5>
                    </div>
                </div>

                @if($media_type == 'audio')
                <div class="panel-body">

                </div>


                <div class="media-list-header table-header datatable-header clearfix">

                    
                    <div class="filter-table">
                    <div class="form-group pull-left  mr-5 no-margin-bottom">
                        <label style="height: 30px; line-height: 30px;margin-bottom: 0px;">Filter: </label>
                    </div>
                    <div class="form-group pull-left has-feedback  no-margin-bottom ml-5">
                        <input value="{{ isset($search) ? $search : '' }}" type="text" class="form-control" id="search_the_table" style="height:30px">
                        <div class="form-control-feedback" style="line-height:30px;">
                            <i class="icon-search4" style="font-size:12px"></i>
                        </div>
                    </div>
                    </div>


                </div>



                {!! view('alpha::admin.media.templates.list-audio',['media_type'=>$media_type]) !!}
                @else
                <div class="media-list">

                    <div class="media-list-header table-header datatable-header clearfix mb-15">

                        
                        <div class="filter-table">
                        <div class="form-group pull-left  mr-5 no-margin-bottom">
                            <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter:&nbsp;</label>
                        </div>
                        <div class="form-group pull-left has-feedback  no-margin-bottom ml-5">
                            <input value="{{ isset($search) ? $search : '' }}" type="text" class="form-control" id="search_the_table">
                            <div class="form-control-feedback">
                                <i class="icon-search4" style="font-size:12px"></i>
                            </div>
                        </div>
                        </div>

                    </div>

                </div>
                <div class="media-list-body">
                    <div class="media-row media-thumbnail-row" id="tmpUploadContainer{{$media_type}}">

                        {!! view('alpha::admin.media.templates.list-media',['medias'=>$medias,'media_type'=>$media_type]) !!}

                    </div>
                </div>

                @endif



            </div>
        </div>
    </div>
    @if($medias)
    <div class="row">
        <div class="col-md-6">
            <?php
                     if(!empty($_GET)){
                        foreach ($_GET as $key => $value) {
                            $medias = $medias->appends([$key => $value]);
                        }
                     }
                ?>
                {!! view('alpha::admin.media.templates.paginator',['paginator'=>$medias]); !!}
        </div>
    </div>
    @endif
</div>

<!-- Footer -->
{!! view('alpha::admin.partials.content-footer') !!}
<!-- /footer -->
</div>

<?php
    echo view('alpha::admin.media.uploader',['media_type'=>$media_type]);
 ?>
    <?php
    if($media_type == 'video'){
        echo view('alpha::admin.media.modal.embed-video');
    }
?>
        <form id="formDelete" method="POST" class="" style="display:none" action="{{ route('alpha_admin_media_delete') }}">
            <input type="hidden" name="id" value="0"> {{ csrf_field() }}
        </form>

        <script type="text/javascript">
            $(document).ready(function() {
                autosize($('textarea'));
                initPreventClose();
            });
            $("#btnEmbedVideo").click(function(e) {
                $("#modalEmbedVideo").modal('show');
            });
            $("#btnUpload{{$media_type}}").click(function(e) {
                e.preventDefault();
                $("#modalUpload{{$media_type}}").modal('show');
            });

            $(document).on('click', ".delete_media", function(e) {
                e.preventDefault();
                $("#formDelete").find('[name=id]').val($(this).data('id'));
                swal({
                        title: "Delete ?",
                        text: "You will not be able to recover deleted items, are you sure want to delete ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $("#formDelete").submit();
                        }
                    });
            });


            $("#search_the_table").keyup(function(e) {
                if (e.which == 13) {
                    cancelPrevent();
                    window.location.href = window.location.origin + window.location.pathname + "?search=" + $("#search_the_table").val();
                }
            });

        </script>
        <script type="text/javascript">
            var entryGetDataUrl = "{{ route('alpha_admin_media_get_data',['audio']) }}";

        </script>
        @if($media_type == 'audio')
        <script type="text/javascript" src="{{ asset('backend/assets/js/media-audio.js') }}"></script>
        @endif
