<div id="modal_edit_slider" class="modal fade" style="display: none;">
                        <div class="modal-dialog" style="width:750px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h5 class="modal-title text-bold">Image Description</h5>
                                   
                                </div>                              
                                <div class="modal-body">
                                   
                                      <div class="row">
                                    
                                          <div class="col-sm-4">
                                                <img id="edit_image_temp" src="{{asset('backend/assets/images/slide-thumbnail-big.png')}}" alt="image name">
                                          </div>
                                          <div class="col-sm-8">
                                          
                                              <div class="form-group no-margin">
                                                    <label>Image Title</label>
                                              </div>
                                              <div class="form-group">
                                                    <input type="text" name="media-title" placeholder="Image Title" id="edit_image_title" class="form-control">
                                              </div>
                                              
                                              <div class="form-group no-margin">
                                                    <label>Image Desc</label>
                                              </div>
                                              <div class="form-group">
                                                    <textarea name="media-desc" class="form-control" id="edit_image_desc"></textarea>
                                              </div>

                                              <div class="form-group no-margin">
                                                    <label>URL</label>
                                              </div>
                                              <div class="form-group">
                                                <input type="text" name="media-url" placeholder="URL" id="edit_image_url" class="form-control">                                              </div>
                                          </div>
                                       </div>
                                    
                                </div>

                                <div class="modal-footer">
                                    <input type="hidden" id="idm" name="id" value="0">
                                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-primary saveSlide">Save</button>
                                </div>
                                <!-- </form> -->
                            </div>
                        </div>
</div>
<script type="text/javascript">  
  var currentEditElement = {};
  $(document).on('click','.edit-slide',function(e){
       e.preventDefault();
        currentEditElement = $(this).parents('.thumbnail');
        var selectedimage =  $(this).parents('.thumbnail').find('.slide-image').attr('src');
        var selectedtitle =  $(this).parents('.thumbnail').find('.slide-title').text();
        var selecteddesc = $(this).parents('.thumbnail').find('.slide-desc').text();
        var mediaid =  $(this).parents('.thumbnail').find('.mid').attr('value');
        $('#modal_edit_slider').modal();
        $('#edit_image_temp').attr('src',selectedimage);
        $('#edit_image_title').val(selectedtitle);
        $('#edit_image_desc').val(selecteddesc);
        $("#edit_image_url").val( $(this).parents('.thumbnail').find('.url-media').val());
        $('#idm').val(mediaid);
  });
  $(document).on('click','.delete-slide',function(e){
        e.preventDefault();
        var that = $(this);
        swal({
        title: "Delete ?",
        text: "You will not be able to recover deleted items, are you sure want to delete ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true },
        function(isConfirm){
            if(isConfirm){
                $(that).parent().parent().parent().parent().parent().remove();
            }
        });



      

  });
  $(".saveSlide").click(function(e){
        var selectedtitle =  $('#edit_image_title').val();
        var selecteddesc = $('#edit_image_desc').val();
        var mediaid =  $('#idm').val();
        var datas = {
            titlem : selectedtitle,
            descm : selecteddesc,
            idm : mediaid,
            link:$("#edit_image_url").val()
        };
        $.ajax({
            dataType : 'json',
            type: 'post',
            url: "{{ route('alpha_admin_media_save') }}",
            data: datas,
            success: function (data) {
                $(currentEditElement).find('.ctitle').html(data.title);
                $(currentEditElement).find('.cdesc').html(data.description);
                $(currentEditElement).find('.url-media').val(data.link);
                $('#modal_edit_slider').modal('hide');
            }
        });
    });
</script>