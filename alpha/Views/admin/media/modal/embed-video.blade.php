<div id="modalEmbedVideo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" style="width:550px;">
    <div class="modal-content">
      	<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h5 class="modal-title">Embed Video</h5>
        </div>
        
        <div class="modal-body">
        
                <div class="form-group">
                    
                        <label>Video URL</label>
                
                        <input type="text" class="form-control"  name="url" placeholder="Paste video link here">
                        
                </div>
                    
                    <div class="clearfix">
                        
                        <div class="pull-right">
                        
                            <button type="button" class="btn bg-slate" id="closeEmbedVideo">Cancel</button>
                            <button type="button" class="btn btn-primary" id="btnTriggerAddEmbedVideo">Save</button>
                            
                        </div>
                    
                    </div>
        
        </div>
        
        
<!--
        <div class="row">
          <div class="col-md-12">
              <input type="text" name="url" class="form-control">
              <br>
          </div>
        </div>
      	<div class="row">
      		<div class="col-md-1">
      			<button class="btn bg-brand" id="btnTriggerAddEmbedVideo">Save</button>
      		</div>
          <div class="col-md-1"></div>
          <div class="col-md-1">
            <button class="btn bg-brand" id="closeEmbedVideo">Cancel</button>
          </div>
      	</div>
-->
    </div>
  </div>
</div>

<script type="text/javascript">
$("#closeEmbedVideo").click(function(e){
    e.preventDefault();
    $("#modalEmbedVideo").modal('hide');
})
  $("#btnTriggerAddEmbedVideo").click(function(e){

    $("#modalEmbedVideo").find(".modal-content").block({
      message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
       overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: '10px 15px',
            color: '#333',
            width: 'auto',
        }
    });


    $.ajax({
      url:'<?php echo route('alpha_admin_media_embed_video') ?>',
      type:'POST',
      dataType:'json',
      data:{url:$("#modalEmbedVideo").find("[name=url]").val()},
      success:function(data){
        $.unblockUI();
        if(data.status == 1){
          swal({
            title: "SUCCESS",
            text: "All changes has been saved successfuly",
            confirmButtonColor: "#66BB6A",
            type: "success",
            closeOnConfirm: false,
            },function(isConfirm){
                swal.disableButtons();
                swal.close();
          });
          cancelPrevent();
          window.location.reload();
        }else{

        }
      }

    });
  });
</script>
