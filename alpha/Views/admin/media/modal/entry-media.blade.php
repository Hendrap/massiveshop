<div id="modalEntryMedia" class="modal fade" role="dialog">
                        <div id="modalDialogEntryMedia" class="modal-dialog" style="width:750px;">
                            <div class="modal-content">

                                <div class="modal-header" id="defaultModalHeader">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h5 id="labelTitleEntryMedia" class="modal-title text-bold">Media Library</h5>
                                    <span id="labelEntryMedia" style="display:block;margin-top:20px;"></span>
                                </div>

                                <div id="catalogModalHeader" class="modal-header border-bottom" style="border-color: #ddd;">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h2 class="text-semibold modal-title">Select Photo(s)</h2>
                                    <span style="margin-top:20px;display:block;margin-bottom: 20px;">Select photo(s) from your product's uploaded photos which matches your current variant</span>
                                </div>





                                <div class="modal-body">
                                   
                                      <div class="media-popup-header clearfix mb-15">

                                        <div class="form-group pull-left  mr-5 no-margin-bottom">
                                            <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter</label>
                                        </div>
                                        <div class="form-group pull-left has-feedback  no-margin-bottom ml-5 mr-15">
                                            <input type="text" class="form-control" id="entry_media_search" style="height:36px">
                                            <div class="form-control-feedback" style="line-height:36px;">
                                                <i class="icon-search4" style="font-size:12px"></i>
                                            </div>
                                        </div>
                                                                                 
                                         <div id="btnTriggerAddimage" class="button-groups  pull-right">
                                            <label for="upload_image">
                                			         <label for="upload_image" class="btn bg-brand">Upload</label>
                                            </label>
                                             <input type="file" id="upload_image" class="hide">
                                        </div>
                                     </div>
                                    
                                     <div id="containerRowEntryMedia" class="media-popup-content">
                                    
                                        <div class="row" id="ajaxEntryMedia">
                                        </div>
                                         
                                     </div>

                                   
                                </div>

                                <div class="modal-footer">
                                	<div class="dataTables_paginate paging_simple_numbers pull-left no-margin" id="DataTables_Table_0_paginate">
                                    	<div id="entryMediaPagination"></div>
                                    </div>
                                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="btnTriggerAddImageToEntry" class="btn btn-primary select-image">Select</button>
                                </div>
                            </div>
                        </div>
</div>
<?php echo view('alpha::admin.media.uploader',['media_type'=>'image']) ?>
<script type="text/javascript">
    var entry_media_url = "<?php echo route('alpha_admin_media_entry') ?>";
    var templateAddToEntry = '<?php echo newLine(view('alpha::admin.media.templates.get-entry-media',['id'=>0,'img'=>'','title'=>'','description'=>'','url'=>''])); ?>';
</script>
<script type="text/javascript">
    var loader = '<div class="pace-demo"><div class="theme_squares"><div class="pace-progress" data-progress-text="60%" data-progress="60"></div><div class="pace_activity"></div></div></div>';
    function productimagecheck() {
                var productimagecount = $('.product-photo').length;
                if(productimagecount < 4){
                     $('.product-photo-navigation.next').addClass('hide');
                     $('.product-photo-navigation.prev').addClass('hide');
                }
                else{
                    $('.product-photo-navigation.next').removeClass('hide');
                }
            }
            

  $(document).ready(function(){
    productimagecheck();
  });   

  $(document).on('click','#ajaxEntryMedia .media-popup-thumbnail',function(e){
          e.preventDefault();
          if($(this).hasClass('selected')){
            $(this).removeClass('selected');
          }else{
            $(this).addClass('selected');
           
          }
          if($("#ajaxEntryMedia").find('.selected').length){
             $("#btnTriggerAddImageToEntry").removeAttr('disabled');
           }else{
            $("#btnTriggerAddImageToEntry").attr('disabled',true);
           }
   });



    $(document).on('click','#entryMediaPagination a',function(e){
        e.preventDefault();
        loadMedias($(this).attr('href'),function(){

        });
    });
    $(document).on('keypress','#entry_media_search',function(e){
        
    if(e.which == 13)
    {
      e.preventDefault();
  		var keyword = $("#entry_media_search").val();
  		loadMedias(entry_media_url + "?search=" + keyword,function(){

  		});
    }
    });


    function loadMedias(url,callback){
    $("#ajaxEntryMedia").html('');


    $("#labelTitleEntryMedia").hide();
    $("#labelTitleEntryMedia").html('Media Library');

    $("#ajaxEntryMedia").block({

       message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
  });

  var txt = $(".blockMsg").css('cssText')
  txt += "top:20px!important";
  $(".blockMsg").css('cssText',txt); 
 
        $("#btnTriggerAddImageToEntry").attr('disabled',true);
        $("#entryMediaPagination").hide();
        $("#modalContainerEntryMedia").attr('class','modal-dialog modal-xs');
        $("#modalContainerEntryMedia h2").hide();
        $("#modalEntryMedia").modal('show');
        $.ajax({
            url:url,
            type:'GET',
            dataType:'json',
            success:function(data){
        $.unblockUI();
                $("#modalContainerEntryMedia").attr('class','modal-dialog modal-lg');
                $("#ajaxEntryMedia").html(data.res);
                $("#entryMediaPagination").html(data.paginate);
                $(".media-popup-header").show();
                $("#entryMediaPagination").show();
                $("#modalContainerEntryMedia h2").show();
                $("#labelTitleEntryMedia").show();
                $("#defaultModalHeader").show();
                $("#catalogModalHeader").hide();
                $("#modalDialogEntryMedia").css('width','750px');
                $("#containerRowEntryMedia").removeClass('image-list');
                $("#containerRowEntryMedia").addClass('media-popup-content')
                callback();
        
            }
        });
    }

    $("#btnShowEntryMedia").click(function(e){
        e.preventDefault();
        $("#labelEntryMedia").html("Select image(s) from your media library or upload new image(s)");
    		loadMedias("<?php echo route('alpha_admin_media_entry')."?page=1" ?>",function(){
    			$("#modalEntryMedia").modal('show');	
    			editorName = '';
    		});
	 });

	$("#btnTriggerAddImageToEntry").click(function(e){
		e.preventDefault();
    var arrImg = new Array();
		if(editorName == ''){
			var selected = $("#modalEntryMedia").find('.selected');
			$.each(selected,function(i,k){
        var newItem = $(templateAddToEntry);
        var title = $(k).parent().find('.title-entry-media').text();
        var desc = $(k).parent().find('.title-entry-desc').text();
        var id = $(k).parent().find('.mid').val();
        var img = $(k).parent().find('img').attr('src');
        $(newItem).find(".mid").val(id);
        $(newItem).find(".ctitle").html(title);
        $(newItem).find(".cdesc").html(desc);
        $(newItem).find("img").attr('src',img);
        if($(".add-image-cont").length)
        {
          $(".add-image-cont").after(newItem);
        }
        if($("#containerMedia").length){
          img = img.replace('_entrymedia','_default');
          $(newItem).find("img").attr('src',img);
          $("#containerMedia").prepend(newItem);
        }
      });

		}else{
			var selected = $("#modalEntryMedia").find('.selected');
			$.each(selected,function(i,k){
        var altText = $(k).parent().find('.title-entry-media').text();
        var imgSrc = $(k).find('img').attr('src').replace('_entrymedia','');
        if(editorName.indexOf("metas.") >= 0)
        {
            if(i == 0)
            {
              //dipake uploader
              $("#media_ids" + editorName.replace("metas.","")).val($(k).parent().find('.mid').val());
              var txt = altText;
              if(txt.trim() == "")
              {
                txt = imgSrc
              }
              $("#media_container" + editorName.replace("metas.","")).val(txt);
              $("#media_container" + editorName.replace("metas.","")).parent().find('button').html("Remove");
            }
        }else if(editorName.indexOf("medias.opener") >= 0){
                   arrImg.push( $(k).parent().find('.mid').val() );
        }else{
  				
  				var editor = CKEDITOR.instances[editorName];
  				editor.insertHtml('<p><img  alt="' +altText+ '" data-cke-saved-src="' +imgSrc+ '" src="' +imgSrc+ '" '+
  						'style="width:400px;" /></p>');
        }
			});
		}

    if(arrImg.length)
    {
      $(openerMedia).val(JSON.stringify(arrImg));
    }
    if(typeof productimagecheck == "function"){
      productimagecheck();
    }
		$("#modalEntryMedia").modal('hide');	
           

    });

    $("#btnTriggerEntryMediaUpload").click(function(e){
        e.preventDefault();
        $("#modalEntryMedia").modal('hide');
        $("#modalUploadimage").modal('show');
    });
</script>
