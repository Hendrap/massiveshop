@if($type == 'image')
<div class="col-sm-3">
    <div class="media-popup-thumbnail">
         <img onerror="this.src='{{ asset(getCropImage('backend/assets/images/placeholder.jpg','entrymedia',true)) }}'" src="<?php echo $img ?>" alt="image name">
    </div>
    <input type="hidden" value="{{ $id }}" name="media[]" class="mid">
    <p style="display: none" class="title-entry-media">{{$title}}</p>
    <p style="display: none" class="title-entry-desc">{{$description}}</p>
</div>
@else
<div class="col-sm-3">
	<div class="media-popup-thumbnail video-audio-content">
	<h6><i class="icon-play"></i><br>{{$title}}</h6>
	<img src="{{ asset(getCropImage('backend/assets/images/placeholder.jpg','entrymedia',true)) }}" alt="image name">
     <input type="hidden" value="{{ $id }}" name="media[]" class="mid">
	</div>
</div>
@endif