<div class="media-grid ui-sortable-handle">
        <div class="thumbnail">
            <div class="thumb">
                                                       
                <input type="hidden" value="{{ $id }}" name="media[]" class="mid">
                <input type="hidden" value="{{ $url }}" name="url" class="url-media">                                            

                <img onerror="this.src="{{ asset('backend/assets/images/placeholder.jpg') }}"" src="<?php echo $img ?>" alt="image name"  class="slide-image">
                                                                
                <div class="caption-overflow">
                    <span>
                        <a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded edit-slide"><i class="icon-pencil5"></i></a>
                        <a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 delete-slide"><i class="icon-trash"></i></a>
                    </span>
                </div>
            </div>

            <div class="caption">
                <h6 class="no-margin-top text-semibold">
                    <a href="#" class="text-default slide-title ctitle">{{$title}}</a>
                </h6>
                <span class="slide-desc cdesc">{{$description}}</span>
            </div>
        </div>
</div>