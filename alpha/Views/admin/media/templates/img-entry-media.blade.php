<div class="product-photo">
	<div class="caption-overflow">
	    <span>
	        <a href="javascript:void(0)" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 delete-img-entry-media"><i class="icon-trash"></i></a>
	    </span>
	</div>
	<img src="<?php echo $img ?>" alt="image name">
	<input type="hidden" name="media[]" value="<?php echo $id ?>" class="mid">
</div>
