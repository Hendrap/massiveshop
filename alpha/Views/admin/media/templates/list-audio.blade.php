<table id="tmpUploadContainer{{$media_type}}" class="table entry-table table-striped">
    <thead>
        <tr>
            <th class="no-sort" style="width:50px"></th>
            <th>Name</th>
            <th width="140px"><span>Uploaded</span></th>
            <th width="140px"><span>Modified</span></th>
            <th width="110px"><span>File Size</span></th>
            <th width="50px" class="no-sort text-right"></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
