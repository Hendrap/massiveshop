
<?php $n = 0; ?>
@foreach($medias as $value)
<?php $info = @unserialize($value->media_meta); ?>
<?php if(!empty($value->path) && $media_type == 'image'){
    $n++;
 ?>
    <div class="media-grid">
        <div class="thumbnail">
            <div class="thumb">
                <img src="{{ asset(getCropImage($value->path,'default',true)) }}" alt="image name">
                <input type="hidden" name="idi" value="{{$value->id}}" class="media-id">
                <div class="caption-overflow">
                <span>
                    <a href="#" data-id="{{ $value->id }}" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 delete_media"><i class="icon-trash"></i></a>
                </span>
                </div>
            </div>
            <div class="caption">
                <h6 class="no-margin-top text-semibold">
                    <a target="_blank" href="{{ asset($value->path) }}" class="text-default media-name">{{$value->title}}</a>
                    
                </h6>
                <div class="clearfix">
                    
                    <div class="pull-left">
                    
                        <span class="small-span">{{ @$value->user->username }}</span>
                        <span class="media-desc hide">{{$value->description}}</span>
                        
                    </div>
                
                    <div class="pull-right">
                        <span class="filesize">{{ @formatBytes((int)@$info['size']) }}</span>
                    </div>
                    
                
                </div>
                
            </div>
        </div>
    </div>
<?php }else{ ?>
    @if(!empty($value->path))
    <?php  $n++; ?>
    <div class="col-lg-3 col-sm-6">
        <div class="thumbnail video-thumbnail">
            <input type="hidden" name="idi" value="{{$value->id}}" class="media-id">
            <div class="thumb thumb-video">
                <video  {!! !empty($value->url) ? 'poster="'.asset(getCropImage($value->path,'embedmedia',true)).'"' : '' !!} class="video_thumb video-js" controls>
                    <source src="{{asset($value->path)}}" type="video/mp4">
                </video>
            </div>
            <div class="caption clearfix">
                <div class="pull-right">
                       <a href="#" data-id="{{ $value->id }}" class="icon-trash text-muted delete_media"></a>
                </div>
                <h6 class="no-margin-top text-semibold">
                    
                    @if(empty($value->url))
                    <a target="_blank" href="{{ asset($value->path) }}" class="text-default media-name">{{$value->title}}</a>
                    @else
                    <a target="_blank" href="{{ $value->url }}" class="text-default media-name">{{$value->title}}</a>
                    @endif
                    
                </h6>
                <div class="clearfix">
                    <span class="small-span pull-left">{{ $value->user->username }}</span>
                    <div class="pull-right">
                        <span class="filesize">
                         @if(empty($value->url))
                            {{ formatBytes((int)@$info['size']) }}
                         @else
                            Embed Video
                        @endif
                        </span>
                    </div>
                </div>
                <span class="media-desc hide">{{$value->description}}</span>
            </div>
        </div>
    </div>
    @endif
<?php } ?>
@endforeach

@if($n == 0)
<div class="row">
    <div class="col-lg-12">
        <table id="tmpUploadContaineraudio" class="empty_state table entry-table table-striped dataTable no-footer dtr-inline" role="grid" aria-describedby="tmpUploadContaineraudio_info">
            <thead>
                
            </thead>
            <tbody style="position: relative; zoom: 1;">
            <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr></tbody>
        </table>
  </div>
</div>
@endif
<script>

    $(window).load(function(){
        var t=0; 
        var t_elem;  
        $('.thumb-video').each(function () {
            $this = $(this);
            if ( $this.outerHeight() > t ) {
                t_elem=this;
                t=$this.outerHeight();
            }
        });
        $('.thumb-video').height(t);
        
        
    });
    

</script>
