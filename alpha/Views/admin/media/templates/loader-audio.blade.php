<tr role="row" class="odd alpha-the-loader">
    <td></td>
    <td class="uploading" tabindex="0">
        <span class="text-bold text-italic display-block mb-5">Uploading</span>
        <div class="progress progress-xxs">
        <div class="progress-bar progress-bar-info" style="width: 0%">
            <span class="sr-only">0% Complete</span>
            <button style="display: none">X</button>
        </div>
    </div>
    </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>