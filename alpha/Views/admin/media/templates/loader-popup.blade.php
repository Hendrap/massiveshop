<div class="col-sm-3 alpha-the-loader">
    <div class="media-popup-thumbnail uploading">
        <img src="{{ asset('backend/assets/images/slide-thumbnail-big.png') }}" alt="image name" style="opacity:0;">
        <div class="progress-overlay">
            <div class="progress">
                <div class="progress-bar" style="width: 0%">
                    <span class="sr-only">0% Complete</span>
                </div>
            </div>
            <span class="percentage">0%</span>
            <button style="display: none">X</button>
        </div>
    </div>
</div>