<div class="media-grid alpha-the-loader video col-lg-3 col-sm-6">
    <div class="thumbnail video-thumbnail">
        <div class="thumb">
            <div class="sixteen-nine">
                <div class="content">
                    <div class="progress-overlay">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                <span class="sr-only">50% Complete</span>
                            </div>
                        </div>
                        <span class="percentage">60%</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="caption">
            <h6 class="no-margin-top text-semibold">
                                                    <a href="#" class="text-default media-name"><i>Uploading..</i></a>
                                                    <div class="pull-right">
                                                       <a href="#" class="icon-pencil5 text-muted hide edit-media"></a>
                                                       <a href="#" class="icon-trash text-muted hide"></a>
                                                    </div>
                                                </h6>
            <div class="clearfix">
                <span class="small-span pull-left"><i>Uploading..</i></span>
                <div class="pull-right">
                    <span class="filesize"><i>Uploading..</i></span>
                </div>
            </div>
            <span class="media-desc hide">S<i>Uploading..</i></span>
        </div>
        <button style="display:none" class="btn btn-default">Start</button>
    </div>
</div>

<style>

    .alpha-the-loader.video{
        width: 25%!important;
        margin-left: 0px!important;
    }
    
</style>
