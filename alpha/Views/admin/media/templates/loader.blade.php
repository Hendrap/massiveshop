<div class="media-grid alpha-the-loader"><div class="thumbnail">
                                              <input type="hidden" name="idi" value="" class="media-id">
                                              <div class="thumb">
                                                  <img src="{{ asset('backend/assets/images/slide-thumbnail-medium.png') }}" alt="image name" style="opacity:0">
                                                  <div class="progress-overlay">
                                                              <div class="progress">
                                                                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                                  </div>
                                                              </div>
                                                  </div>
                                              </div>
                                              <div class="caption">
                                                  <h6 class="no-margin-top text-semibold">
                                                      <a href="#" class="text-default"><i>Uploading...</i></a>
                                                </h6>
                                                  
                                                  <div class="clearfix">
                                                  
                                                      <div class="pull-left">
                                                      
                                                          <span class="small-span"><i>Uploading...</i></span>
                                                  <span class="image-desc hide"><i>Uploading...</i></span>
                                                      
                                                      </div>
                                                      
                                                      
                                                      <div class="pull-right">
                                                         <span class="filesize"><i>Uploading...</i></span>
                                                      </div>
                                                  
                                                  </div>
                                                  
                                                  
                                              </div>
                                              <button style="display:none" class="btn btn-default">Start</button>
                                          </div></div>
