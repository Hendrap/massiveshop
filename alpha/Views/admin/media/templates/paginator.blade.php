<?php
$link_limit = 10;
?>


@if ($paginator->lastPage() > 1)
<div class="dataTables_paginate paging_simple_numbers pull-left no-margin">
    <a data-alpha-cms-blacklink="yes" href="{{ $paginator->url(1) }}" class="paginate_button previous {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">←</a>
    <span>
         @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php
            $half_total_links = floor($link_limit / 2);
            $from = $paginator->currentPage() - $half_total_links;
            $to = $paginator->currentPage() + $half_total_links;
            if ($paginator->currentPage() < $half_total_links) {
               $to += $half_total_links - $paginator->currentPage();
            }
            if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
            }
            ?>
            @if ($from < $i && $i < $to)
                <a data-alpha-cms-blacklink="yes" href="{{ $paginator->url($i) }}" class="paginate_button {{ ($paginator->currentPage() == $i) ? ' current' : '' }}">{{ $i }}</a>
            @endif
        @endfor
    </span>
    <a data-alpha-cms-blacklink="yes" href="{{ $paginator->url($paginator->lastPage()) }}" class="paginate_button next {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">→</a>
</div>
@endif
