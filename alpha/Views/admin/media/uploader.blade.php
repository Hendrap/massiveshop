<script src="{{ asset('backend/fileupload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('backend/fileupload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-audio.js') }}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-video.js') }}"></script>
<!-- The File Upload validation plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-validate.js') }}"></script>
<div id="modalUpload{{$media_type}}" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="padding:20px">
        <h2>Upload {{str_plural(ucfirst($media_type))}}</h2>
        <form style="display:none" name="{{$media_type}}FileUpload" id="{{$media_type}}FileUpload">
          <input type="file" multiple name="{{$media_type}}[]">
        </form>      
    </div>
  </div>
</div>



<script type="text/javascript">
var errors = 0;
var stats = false;
var loaderVideo = '<?php echo newLine(view("alpha::admin.media.templates.loader-video")) ?>';
var loaderImage = '<?php echo newLine(view("alpha::admin.media.templates.loader")) ?>';
var loaderAudio = '<?php echo newLine(view("alpha::admin.media.templates.loader-audio")) ?>';
var loaderUploadPopUp = '<?php echo newLine(view("alpha::admin.media.templates.loader-popup")) ?>';
var jqXHR = {};
var urlUpload = "{{ route('alpha_admin_media_upload_'.$media_type) }}";
var idContainer = "tmpUploadContainer{{$media_type}}";


if($("#" + idContainer).length == 0)
{
  idContainer = "ajaxEntryMedia";
  urlUpload += "?popup=true";
}

$("#btnTriggerAdd{{$media_type}}").click(function(e){
  e.preventDefault();
  $("#{{$media_type}}FileUpload").find("[type=file]").click();
});
$("#{{$media_type}}FileUpload").change(function(e){
  e.preventDefault();  
   var buttons = $("#" + idContainer + " button");
    $.each(buttons,function(i,k){
              $(k).click();
    });
});

$('#{{$media_type}}FileUpload').fileupload({
  url:urlUpload,
  add: function (e, data) {
    var type = "{{$media_type}}"; 
    var tpl = '';
    if (type === "video" ) {
      tpl = $(loaderVideo);
    }else if(type === 'audio'){
      tpl = $(loaderAudio);
    }else{
      if(idContainer == 'ajaxEntryMedia')
      {
        tpl = $(loaderUploadPopUp)
      }else{
        tpl = $(loaderImage);   
        
      }
    }
    tpl.find('button').click(function (e) {
                    e.preventDefault();
                    data.submit();
                    initPreventClose();
                    data.context.find('button').remove();
                });
    if(type === 'audio')
    {
      data.context = tpl.prependTo($(".entry-table").find("tbody"));
    }else{
      data.context = tpl.prependTo($("#" + idContainer));
      
    }

  },
  progress: function(e, data){
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('.media-loading').show();
        if(progress >= 95){
          progress = 95;
        }
        data.context.find('.progress-bar').attr('aria-valuenow',progress).attr('style','width:' + progress + "%");
        data.context.find('.percentage').text(progress+'%');
    },
  progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
    },
  done: function (e, data) {
        $(data.context).remove();
        e.preventDefault();
        var response = JSON.parse(data._response.result);
        if(response.status == 1){
          $("#" + idContainer).prepend(response.res);
          if(typeof entry_table != "undefined")
          {
            entry_table.row.add(response.item).draw();
          }
       }else{
          sweetAlert("Upload Failed", response.msg, "error");

          //alert("Error Occurred!" + response.msg);
       }
       if($(".alpha-the-loader").length == 0)
       {
          cancelPrevent();
       }
    },
  error:function(e,data){
    sweetAlert("Upload Failed", "Internal Server Error", "error");
  },
  complete:function(a,b){
    
  }
});
function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }
    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }

    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}  
</script>
