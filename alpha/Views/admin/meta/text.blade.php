@if(isset($isMultiLang))
<input style="display: none" name="multilangmetas[{{@$meta_key}}][{{$lang}}]" type="text" class="form-control multilangmetas_{{$lang}}" value="{{@$value}}">	
@else
<input name="metas[{{@$meta_key}}]" type="text" class="form-control" value="{{@$value}}">	
@endif