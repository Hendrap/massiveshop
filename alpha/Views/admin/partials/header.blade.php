<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
            <a class="navbar-brand big" href="{{ url('/') }}"><img src="{{ asset('bundles/img/logo-small.png') }}" alt="Logo Big" style=""></a>
            <a class="navbar-brand small" href="{{ url('/') }}"><img src="{{ asset('backend/assets/images/logo-mini.jpg') }}" alt="Logo Small" style=""></a>
            <style>
                .navbar-brand.small img{
                    left: 50%!important;
                    -moz-transform: translate(-50%,-50%);
                    -webkit-transform: translate(-50%,-50%);
                    transform: translate(-50%,-50%);
                }
            </style>
            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>


    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a style="color:#263238" class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
             <li class="header-search" style="width:250px">
                <div class="hidden-xs">
                    <form class="form-horizontal clearfix">
                         <div class="form-group has-feedback has-feedback-left no-margin pull-left">
                            <label class="control-label col-lg-2"></label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <input id="topSearchTxt" type="text" class="form-control" placeholder="" style="width:200px;">
                                    <div class="form-control-feedback" style="left:0">
                                        <i class="icon-search4"></i>
                                    </div>
                                    <div class="input-group-btn">
                                        <button id="topSearchButton" type="button" class="btn btn-primary">Search</button>
                                        <button type="button" class="btn btn-primary dropdown-toggle " data-toggle="dropdown">
                                            <span class="caret" style="margin-top:-2px"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                           <?php $entries = app('AlphaSetting')->entries; ?>
                                            <li><a class="changeSlug" href="javascript:void(0)" data-slug="page">Pages</a></li>
                                            <li><a class="changeSlug" href="javascript:void(0)" data-slug="email_template">Templates</a></li>
                                            <li><a class="changeSlug" href="javascript:void(0)" data-slug="slider">Slideshow Manager</a></li>
                                            @foreach($entries as $key => $value)
                                            @if($value['show_ui'] == 'yes')
                                            <li><a class="changeSlug" href="javascript:void(0)" data-slug="{{ $value['slug'] }}">{{ $value['plural'] }}</a></li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                         </div>
                         <script type="text/javascript">
                                    var topSlug = 'page';
                                    var topBaseUrl = "<?php echo route('alpha_admin_entry_index',['__id__']) ?>";
                                    $("#topSearchButton").click(function(e){
                                        window.location.href = topBaseUrl.replace('__id__',topSlug) + '?search=' + $("#topSearchTxt").val();
                                    });

                                    $(".changeSlug").click(function(e){
                                        topSlug = $(this).data('slug');
                                    });

                                    $("#topSearchTxt").on('keypress keyup keydown',function(e){
                                        if(e.which == 13){
                                             $("#topSearchButton").click();
                                        }
                                    });

                                </script>

                    </form>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            
            <li>
                <a style="color:#263238" href="{{route('alpha_get_logout')}}" class="logout-button"><i class="icon-switch2"></i> Logout</a>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


  
