            @if($active == 'email_template')
                {{ ($activeParent = 'general_setting') ? '' : '' }}
            @endif
            @if($active == 'slideshow')
                {{ ($activeParent = 'main_media_library') ? '' : '' }}
            @endif
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <a href="javascript:void(0)" class="profile-link">
                                    <div class="media-left"><img onerror="this.src='{{ asset('backend/assets/images/avatar-resized.jpg') }}'" src="{{ asset(app("AdminUser")->avatar) }}" class="img-circle img-sm" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">{{ app("AdminUser")->name }}</span>
                                        <div class="text-size-mini text-muted">
                                        {{ app("AdminUser")->user->email }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">
                                <!-- Main -->
                                <li class="{{ ($active == 'dashboard') ? 'active' : '' }}"><a href="{{route('alpha_admin_index')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                                <!--site management-->
                                <li class="navigation-header"><span>SITE MANAGEMENT</span></li>
                                <li class="{{ ($activeParent == 'general_setting') ? 'active' : '' }}">
                                    <a href="#" class="has-ul"><i class="icon-equalizer2"></i> <span>General</span></a>
                                    <ul class="{{ ($activeParent == 'general_setting') ? '' : 'hidden-ul' }}">
                                        <li class="{{ ($active == 'seo') ? 'active' : '' }}">
                                            <a href="{{route('alpha_admin_get_setting_general')}}"> <span>Site Description</span></a>
                                        </li>

                                        <li class="{{ ($active == 'analytics') ? 'active' : '' }}">
                                            <a href="{{route('alpha_admin_get_analytics')}}"> <span>Analytics</span></a>
                                        </li>
                                        <li class="{{ ($active == 'contact') ? 'active' : '' }}">
                                            <a href="{{route('alpha_admin_get_contacts')}}"> <span>Site Contact</span></a>
                                        </li>
                                        <li class="{{ ($active == 'email_template') ? 'active' : '' }}">
                                            <a href="{{url('admin/entry/email_template')}}"> <span>Templates</span></a>
                                        </li>
                                    </ul>

                                </li>
                                <li class="{{ ($activeParent == 'main_account') ? 'active' : '' }}">
                                    <a href="#" class="has-ul"><i class="icon-user"></i> <span>Accounts</span></a>
                                    <ul class="{{ ($activeParent == 'main_account') ? '' : 'hidden-ul' }}">
                                        <li class="{{ ($active == 'all_account') ? 'active' : '' }}">
                                            <a href="{{route('alpha_admin_all_account')}}"> <span>All Accounts</span></a>
                                        </li>
                                        <li class="{{ ($active == 'role') ? 'active' : '' }}">
                                            <a href="{{route('admin.accounts.roles-and-access.index')}}"> <span>Roles &amp; Access</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="{{ ($activeParent == 'main_plugin') ? 'active' : '' }}">
                                    <a href="#" class="has-ul"><i class="icon-puzzle2"></i> <span>Plugins</span></a>
                                    <ul class="{{ ($activeParent == 'main_plugin') ? '' : 'hidden-ul' }}">
                                        <li class="{{ ($active == 'plugins') ? 'active' : '' }}">
                                                <a href="{{route('alpha_admin_get_plugins')}}"> <span>All Plugins</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <!--/site management-->
                                
                                
                                <!--content management-->
                                <li class="navigation-header"><span>CONTENT MANAGEMENT</span></li>
                                <?php $entries = app('AlphaSetting')->entries; ?>
                                <li class="{{ ($activeParent == 'page') ? 'active' : '' }}">
                                    <a href="#" class="has-ul"><i class="icon-files-empty"></i> <span>Pages</span></a>
                                    <ul class="{{ ($activeParent == 'page') ? '' : 'hidden-ul' }}">
                                        <li class="{{ ($active == 'page') ? 'active' : '' }}">
                                            <a href="{{ route('alpha_admin_entry_index',['page']) }}"> <span>All Pages</span></a>
                                        </li>
                                        
                                    </ul>
                                </li>
                                 <li class="{{ ($activeParent == 'main_taxonomy') ? 'active' : '' }}">
                                    <a href="#" class="has-ul"><i class="icon-folder2"></i> <span>Taxonomy</span></a>
                                    <ul class="{{ ($activeParent == 'main_taxonomy') ? '' : 'hidden-ul' }}">
                                         <?php $taxonomies = app('AlphaSetting')->taxonomies; ?>
                                          @foreach($taxonomies as $value)
                                              @if($value['show_ui'] == 'yes')
                                               <li class="{{ ($active == 'taxonomy_'.$value['slug']) ? 'active' : '' }}"><a href="{{ route('alpha_admin_taxonomy_index',[$value['slug']]) }}"><span>{{ucfirst($value['plural'])}}</span></a></li>
                                               @endif
                                          @endforeach
                                    </ul>
                                </li>
                                <li class="{{ ($activeParent == 'main_media_library') ? 'active' : '' }}">
                                    <a href="#" class="has-ul"><i class="icon-media"></i> <span>Media Library</span></a>
                                    <ul class="{{ ($activeParent == 'main_media_library') ? '' : 'hidden-ul' }}">
                                        <li class="{{ ($active == 'media_image') ? 'active' : '' }}">
                                            <a href="{{route('alpha_admin_media_image')}}"> <span>Images</span></a>
                                        </li>
                                        <li class="{{ ($active == 'media_audio') ? 'active' : '' }}">
                                            <a href="{{route('alpha_admin_media_audio')}}"> <span>Audio</span></a>
                                        </li>
                                        <li class="{{ ($active == 'media_video') ? 'active' : '' }}">
                                            <a href="{{route('alpha_admin_media_video')}}"> <span>Videos</span></a>
                                        </li>
                                        <li class="{{ ($active == 'slideshow') ? 'active' : '' }}">
                                            <a href="{{ route('alpha_admin_entry_index',['slideshow']) }}"> <span>Slideshow Manager</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="{{ ($activeParent == 'blog') ? 'active' : '' }}">
                                    <a href="#" class="has-ul"><i class="icon-magazine"></i> <span>Articles</span></a>
                                    <ul class="{{ ($activeParent == 'blog') ? '' : 'hidden-ul' }}">
                                        <li class="{{ ($active == 'blog') ? 'active' : '' }}">
                                            <a href="{{ route('alpha_admin_entry_index',['blog']) }}"> <span>All Articles</span></a>
                                        </li>
                                    </ul>
                                </li>
                                @foreach($entries as $key => $value)
                                @if($value['show_ui'] == 'yes')
                                <li class="{{ ($activeParent == $value['slug']) ? 'active' : '' }}">
                                    <a href="#" class="has-ul"><i class="icon-files-empty"></i> <span>{{ $value['plural'] }}</span></a>
                                    <ul class="{{ ($activeParent == $value['slug']) ? '' : 'hidden-ul' }}">
                                        <li class="{{ ($active == $value['slug']) ? 'active' : '' }}">
                                            <a href="{{ route('alpha_admin_entry_index',[$value['slug']]) }}"> <span>All {{ $value['plural'] }}</span></a>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                                @endforeach
                            
                                @if(!empty(app('AlphaMenu')->labels) && !empty(app('AlphaMenu')->menus))
                                @foreach(app('AlphaMenu')->labels as $key => $value)
                                     <li class="navigation-header"><span>ORDER MANAGEMENT</span></li>
                                     @foreach(app('AlphaMenu')->menus as $menuKey => $parent )
                                        @if($parent->label == $key)
                                            <li>
                                                <a href="javascript:;"><i class="{{$parent->class}}"></i> <span>{{ $parent->name }}</span></a>
                                                @if(count($parent->childs))
                                                <ul class="{{ ($activeParent == $menuKey) ? '' : 'hidden-ul' }}">
                                                    @foreach($parent->childs as $childKey => $childObj)
                                                        <li class="{{ ($active == $childKey) ? 'active' : '' }}">
                                                            <a href="{{ $childObj->link }}"> <span>{{ $childObj->name }}</span></a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            <li>
                                        @endif
                                     @endforeach
                                @endforeach
                                @endif
                               
                                <!--statistics-->
                                <li class="navigation-header"><span>CONFIGURATIONS</span></li>
                                <li class="{{ ($activeParent == 'main_setting') ? 'active' : '' }}">
                                    <a class="has-ul" href="#"><i class="icon-gear"></i> <span>Settings</span></a>
                                    
                                    <ul class="{{ ($activeParent == 'main_setting') ? '' : 'hidden-ul' }}">
                                        <li class="{{ ($active == 'setting_general') ? 'active' : '' }}"><a href="{{route('alpha_admin_get_setting_site_general')}}">General</a></li>
                                        <li class="{{ ($active == 'setting_entry_type') ? 'active' : '' }}"><a href="{{route('admin.setting.entry_type.index')}}">Entries</a></li>
                                        <li class="{{ ($active == 'setting_taxonomy_type') ? 'active' : '' }}"><a href="{{route('admin.setting.taxonomy_type.index')}}">Taxonomy</a></li>
<!--                                        <li class="{{ ($active == 'setting_user_type') ? 'active' : '' }}"><a href="{{route('admin.setting.user_type.index')}}">User</a></li>-->
                                        <li class="{{ ($active == 'setting_image_type') ? 'active' : '' }}"><a href="{{route('admin.setting.image.index')}}">Image Handling</a></li>
                                        <li class="{{ ($active == 'setting_activity_log') ? 'active' : '' }}"><a href="{{route('alpha_admin_activity_log')}}">Activity Log</a></li>
                                        <li class="{{ ($active == 'setting_currency') ? 'active' : '' }}"><a href="{{route('alpha_admin_currency')}}">Currency</a></li>
                                        <li class="{{ ($active == 'setting_dhl') ? 'active' : '' }}"><a href="{{route('alpha_admin_dhl')}}">DHL</a></li>
                                        <li class="{{ ($active == 'setting_doku') ? 'active' : '' }}"><a href="{{route('alpha_admin_doku')}}">Doku</a></li>
                                        <li class="{{ ($active == 'setting_free_shipping') ? 'active' : '' }}"><a href="{{route('alpha_admin_free_shipping')}}">Free Shipping</a></li>


<!--
                                        <li class="{{ ($active == 'setting_extras') ? 'active' : '' }}"><a href="{{route('alpha_admin_get_extras')}}">Extras</a></li>
                                        <li class="{{ ($active == 'setting_labels') ? 'active' : '' }}"><a href="{{route('alpha_admin_get_labels')}}">Labels</a></li>
-->
                                    </ul>

                                </li>
                                
                                <!--/statistics-->
                                
                                
                                
                               
                                
                                
                                
                                
                                
                                
                                

                                <!--statistics-->
                                <!-- <li class="navigation-header"><span>CONFIGURATIONS</span></li>
                                <li>
                                    <a class="has-ul" href="#"><i class="icon-gear"></i> <span>Settings</span></a>
                                    

                                    <ul class="hidden-ul">
                                        <li><a href="{{route('admin.setting.entry_type.index')}}">Entry Types</a></li>
                                        <li><a href="{{route('admin.setting.taxonomy_type.index')}}">Taxonomy Types</a></li>
                                        <li><a href="{{route('admin.setting.user_type.index')}}">User Type</a></li>
                                        <li><a href="{{route('admin.setting.image.index')}}">Image Types</a></li>
                                        <li><a href="{{route('alpha_admin_get_extras')}}">Extras</a></li>
                                        <li><a href="{{route('alpha_admin_get_labels')}}">Labels</a></li>
                                    </ul>

                                </li> -->
                                
                                
                                <!-- /main -->

                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->

                </div>
            </div>
            <!-- /main sidebar -->
