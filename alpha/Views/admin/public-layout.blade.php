<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }}</title>

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('bundles/img/favicon.png') }}">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/project-color.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <!-- /core JS files -->

    <!--charts-->
   <!--  <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/visualization/c3/c3.min.js') }}"></script> -->
    <!--/charts-->

    <!-- Theme JS files -->
<!--     <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
 -->    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<!--     <script type="text/javascript" src="{{ asset('backend/assets/js/pages/datatables_responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/pickers/anytime.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/core/libraries/jquery_ui/full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/plugins/forms/validation/validate.min.js') }}"></script>
 -->

    <script type="text/javascript" src="{{ asset('backend/assets/js/core/app.js') }}"></script>
<!--  <script type="text/javascript" src="assets/js/pages/editor_ckeditor.js"></script>-->
<!--     <script type="text/javascript" src="{{ asset('backend/assets/js/pages/form_select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/js/pages/datatables_basic.js') }}"></script>
 -->
    <!-- /theme JS files -->
    <script type="text/javascript" src="{{ asset('backend/assets/js/validate.min.js') }}"></script>


    <!--custom css-->
    <link href="{{ asset('backend/assets/css/custom.css') }}" rel="stylesheet" type="text/css">

</head>

<body class="login-container"  style="background:#fff">

    

    <!-- Page container -->
    <div class="page-container page-container-login">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                   <?php if(!empty($content)) echo $content; ?>
                    
                    <style>
                        .form-control-feedback{
                            top: 0px!important;
                        }
                    </style>




                    <!-- Footer -->
                    {!! view('alpha::admin.partials.content-footer') !!}
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

</body>

</html>
