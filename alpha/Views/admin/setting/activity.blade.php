<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - Activity Log</h4>
        </div>
    </div>
</div>

<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="#">Settings</a></li>
                <li class="active">Activity Log</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                       <h5 class="panel-title">Activity Log</h5>
                        
                    </div>

                    
                </div>
                <div class="panel-body">
                    <p>Manage all accounts activity log</p>
                </div>
                    <table class="table entry-table table-striped">
                        <thead>
                            <tr>
                                <th width="50px" class="no-sort text-center">&nbsp;</th>
                                <th class="td-date" width="150px">
                                    <span>Activity Date</span>
                                </th>
                                <th width="190px">User &amp; Role</th>
                                <th>Activity</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                    
                    <script>
                    var entry_table = {};
                        $(document).ready(function() {
                            
                             entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                          $('.dataTable, .dataTable tbody').css('position','static');
                                            if(typeof entry_table.ajax != 'undefined')
                                            {
                                                $(e.target).find('tbody').html('');
                                            }
                                            $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                        $('.dataTable, .dataTable tbody').css('position','relative');
                                        $.unblockUI();
                                        $(".blockUI").remove();

                                     }
                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "initComplete":function()
                                {
                                   
                                },
                                "order": [],
                                "pageLength": 50,
                                "searching" : true,
                                "lengthChange": false,
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "ajax": "{{ route('alpha_admin_get_data_log') }}",
                                "columnDefs": [{
                                    "targets": 'no-sort',
                                    "orderable": false,
                                }],
                                "columns": [

                                    { "data": "created_at", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:""
                                    },

                                    { "data": "activity", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    },
                                    sClass:"td-date"
                                    },

                                    { "data": "name", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    },
                                    sClass:""
                                    },

                                    { "data": "email", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:""
                                    }

                                ]
                            });

                            table.on( 'draw', function () {
                                  $('[data-popup="popover"]').popover()
                            });
                            
                            $('.datatable-header').addClass('clearfix');
                            $('.dataTables_filter').find('input').attr('placeholder','Type to filter');
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                            
                        });

                    </script>


                


            </div>

        </div>

    </div>



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
