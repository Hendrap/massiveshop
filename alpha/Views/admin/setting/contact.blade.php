
<script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Site Management</span> - Site Contact</h4>
        </div>

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            
                <li><a href="#">General</a></li>
                <li class="active">Site Contact</li>

            </ul>
        </div>

    </div>


    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">Site Contact</h5>
                        
                    </div>

                    
                </div>
                <div class="panel-body">
                    <p class="content-group-lg">Use the followings to define default contact information </p>
                    @if(count($errors) > 0)
                     @foreach($errors->all() as $key => $error)
                     @if($key == 0)
                     <div class="alert bg-danger alert-styled-left">
                         <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                         @if(Illuminate\Support\Str::contains($error,'required'))
                             <span class="text-semibold">Required field(s) error or missing</span>
                         @else
                             <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                         @endif
                     </div>
                     @endif
                     @endforeach
                     @endif
                     <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                         <span class="text-semibold">Required field(s) error or missing</span>
                     </div>

                     <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                         <span class="text-semibold">Wrong format found</span>
                     </div>

                    <form name="alphaForm" id="form-site-description" class="form-horizontal form-validate-jquery" method="POST" action="{{ route('alpha_admin_post_setting_general',['contact']) }}">
                        {{ csrf_field() }}
                        <fieldset class="content-group">
                            <legend class="text-semibold">
                               Physical Address
                            </legend>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Street Address</label>
                                <div class="col-lg-6">
                                     <textarea class="form-control ckeditor" name="street_address" id="street_address">{!! app('AlphaSetting')->getSetting('street_address') !!}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">City</label>
                                <div class="col-lg-4">
                                     <input name="city" type="text" class="form-control" id="city" value="{{ app('AlphaSetting')->getSetting('city') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">State/Province</label>
                                <div class="col-lg-4">
                                    <input type="text" name="state" class="form-control" id="state" value="{{ app('AlphaSetting')->getSetting('state') }}" >
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-lg-2">Postal Code</label>
                                <div class="col-lg-4">
                                    <input type="text" name="postal_code" class="form-control" id="postal_code" value="{{ app('AlphaSetting')->getSetting('postal_code') }}">
                                </div>
                            </div>                    
                            <div class="form-group">
                                <label class="control-label col-lg-2">Country</label>
                                <div class="col-lg-4">
                                    <input type="text" name="country" class="form-control" id="country" value="{{ app('AlphaSetting')->getSetting('country') }}">                               
                                </div>
                            </div>
                        </fieldset>

                         <fieldset class="content-group">
                            <legend class="text-semibold">
                               Phone Numbers
                            </legend>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Phone</label>
                                <div class="col-lg-4">
                                    <input name="phone" type="text" class="form-control" id="phone" value="{{ app('AlphaSetting')->getSetting('phone') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Alt. Phone</label>
                                <div class="col-lg-4">
                                    <input type="text" name="alt_phone" class="form-control" id="alt_phone" value="{{ app('AlphaSetting')->getSetting('alt_phone') }}" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Fax</label>
                                <div class="col-lg-4">
                                    <input type="text" name="fax" class="form-control" id="fax" value="{{ app('AlphaSetting')->getSetting('fax') }}">
                                </div>
                            </div>                    
                            <div class="form-group">
                                <label class="control-label col-lg-2">Mobile Phone</label>
                                <div class="col-lg-4">
                                    <input type="text" name="mobile_phone" class="form-control" id="mobile_phone" value="{{ app('AlphaSetting')->getSetting('mobile_phone') }}">                               
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="content-group">
                            <legend class="text-semibold">
                               Email Address
                            </legend>
                            <div class="form-group">
                                <label class="control-label col-lg-2">General Contact</label>
                                <div class="col-lg-4">
                                    <input name="general_contact" type="text" class="form-control" id="general_contact" value="{{ app('AlphaSetting')->getSetting('general_contact') }}">
                                    <span class="help-block">All general inquiries will be directed to this email address</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Sales Department</label>
                                <div class="col-lg-4">
                                    <input type="text" name="sales_department" class="form-control" id="sales_department" value="{{ app('AlphaSetting')->getSetting('sales_department') }}" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Customer Service</label>
                                <div class="col-lg-4">
                                    <input type="text" name="customer_service" class="form-control" id="customer_service" value="{{ app('AlphaSetting')->getSetting('customer_service') }}">
                                </div>
                            </div>                    
                            <div class="form-group">
                                <label class="control-label col-lg-2">Technical Support</label>
                                <div class="col-lg-4">
                                    <input type="text" name="technical_support" class="form-control" id="technical_support" value="{{ app('AlphaSetting')->getSetting('technical_support') }}">                               
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Notification</label>
                                <div class="col-lg-4">
                                    <input type="text" name="admin_email_contact" class="form-control" id="admin_email_contact" value="{{ app('AlphaSetting')->getSetting('admin_email_contact') }}">                               
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="content-group">
                            <legend class="text-semibold">
                               Social Media
                            </legend>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Skype ID</label>
                                <div class="col-lg-4">
                                    <input name="skype_id_contact" type="text" class="form-control" id="skype_id_contact" value="{{ app('AlphaSetting')->getSetting('skype_id_contact') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Facebook Fan Page</label>
                                <div class="col-lg-4">
                                    <input name="facebook_fan_page" type="text" class="form-control" id="facebook_fan_page" value="{{ app('AlphaSetting')->getSetting('facebook_fan_page') }}">
                                    <span class="help-block">Example : https://www.facebook.com/xxxxx</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Twitter</label>
                                <div class="col-lg-4">
                                    <input type="text" name="twitter" class="form-control" id="twitter" value="{{ app('AlphaSetting')->getSetting('twitter') }}" >
                                    <span class="help-block">Example : https://www.twitter.com/xxxxx</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Instagram</label>
                                <div class="col-lg-4">
                                    <input type="text" name="instagram" class="form-control" id="instagram" value="{{ app('AlphaSetting')->getSetting('instagram') }}">
                                    <span class="help-block">Example : https://www.instagram.com/xxxxx</span>
                                </div>
                            </div>                    
                            <div class="form-group">
                                <label class="control-label col-lg-2">Pinterest</label>
                                <div class="col-lg-4">
                                    <input type="text" name="pinterest" class="form-control" id="pinterest" value="{{ app('AlphaSetting')->getSetting('pinterest') }}">                               
                                    <span class="help-block">Example : https://www.pinterest.com/xxxxx</span>
                                </div>
                            </div>
                        </fieldset>


                        <div class="text-right">
                            <button type="submit" class="btn btn-primary" id="save-contact">Save</button>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

    <script>
        $(document).ready(function() {
            autosize($('textarea'));
        });

    </script>

    <!-- Footer -->
   {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
   <?php
    $err = count($errors);
    ?>
   {!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'postal_code' => 'numeric',
                    'general_contact' => 'valid_email',
                    'sales_department' => 'valid_email',
                    'customer_service' => 'valid_email',
                    'technical_support' => 'valid_email',
                    'facebook_fan_page' => 'valid_url',
                    'twitter'  => 'valid_url',
                    'instagram'  => 'valid_url',
                    'pinterest'  => 'valid_url'
                ]

        ]); 
   !!}

<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
        var status = "{{session('success')}}";
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }
    })
</script>
