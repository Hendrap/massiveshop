<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - DHL</h4>
        </div>

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            
                <li><a href="#">Settings</a></li>
                <li class="active">DHL</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">DHL Settings</h5>
                        
                    </div>
                    
                </div>
                <div class="panel-body">


                   
                    @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold">{{ $error }}</span>
                    </div>
                    @endif
                    @endforeach
                    @endif
                        <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                           <span class="text-semibold">Required field(s) error or missing</span>
                        </div>

                        <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Wrong format found</span>
                        </div>
                    <form name="alphaForm" id="form-site-description" method="POST" action="{{ route('alpha_admin_post_setting_general') }}" class="form-horizontal">
                    {{csrf_field()}}
                        
                        <fieldset>
                        

                        <div class="form-group">
                            <label class="control-label col-lg-2">Origin Country Code</label>
                            <div class="col-lg-4">
                                <input type="text" id="origin_country_code" name="origin_country_code" class="form-control" value="{{ app('AlphaSetting')->getSetting('origin_country_code') }}" placeholder="">
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="control-label col-lg-2">Origin Country Name</label>
                            <div class="col-lg-4">
                                <input type="text" id="origin_country_name" name="origin_country_name" class="form-control" value="{{ app('AlphaSetting')->getSetting('origin_country_name') }}" placeholder="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-lg-2">Origin Company Name</label>
                            <div class="col-lg-4">
                                <input type="text" id="origin_company_name" name="origin_company_name" class="form-control" value="{{ app('AlphaSetting')->getSetting('origin_company_name') }}" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Origin Address</label>
                            <div class="col-lg-4">
                                <input type="text" id="origin_address" name="origin_address" class="form-control" value="{{ app('AlphaSetting')->getSetting('origin_address') }}" placeholder="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-lg-2">Origin City</label>
                            <div class="col-lg-4">
                                <input type="text" id="origin_city" name="origin_city" class="form-control" value="{{ app('AlphaSetting')->getSetting('origin_city') }}" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Origin Postal Code</label>
                            <div class="col-lg-4">
                                <input type="text" id="origin_postal_code" name="origin_postal_code" class="form-control" value="{{ app('AlphaSetting')->getSetting('origin_postal_code') }}" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Contact Person Name</label>
                            <div class="col-lg-4">
                                <input type="text" id="contact_person_name" name="contact_person_name" class="form-control" value="{{ app('AlphaSetting')->getSetting('contact_person_name') }}" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Contact Phone Number</label>
                            <div class="col-lg-4">
                                <input type="text" id="contact_phone_number" name="contact_phone_number" class="form-control" value="{{ app('AlphaSetting')->getSetting('contact_phone_number') }}" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Contact Email</label>
                            <div class="col-lg-4">
                                <input type="text" id="contact_email" name="contact_email" class="form-control" value="{{ app('AlphaSetting')->getSetting('contact_email') }}" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Commodity Code</label>
                            <div class="col-lg-4">
                                <input type="text" id="commodity_code" name="commodity_code" class="form-control" value="{{ app('AlphaSetting')->getSetting('commodity_code') }}" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Commodity Name</label>
                            <div class="col-lg-4">
                                <input type="text" id="commodity_name" name="commodity_name" class="form-control" value="{{ app('AlphaSetting')->getSetting('commodity_name') }}" placeholder="">
                            </div>
                        </div>
       
                        </fieldset>

                        <div class="text-right">
                            
                                <button type="submit" class="btn btn-primary" id="save-contact">Save</button>
                           
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

    <script>
        $(document).ready(function() {
            $('select').each(function() {

                select_placeholder = $(this).attr('placeholder');
                select_width = $(this).attr('width');
                $(this).select2({
                        minimumResultsForSearch: Infinity,
                        placeholder: select_placeholder,
                        width: select_width,

                });

            });
            
        });

    </script>

    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>



<!-- /content area -->
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
        var status = "{{session('msg')}}";
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }
    })
</script>
