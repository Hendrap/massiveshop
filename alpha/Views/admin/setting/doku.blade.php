<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - General</h4>
        </div>

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            
                <li><a href="#">Settings</a></li>
                <li class="active">Doku</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">Doku Settings</h5>
                        
                    </div>
                    
                </div>
                <div class="panel-body">


                   
                    @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold">{{ $error }}</span>
                    </div>
                    @endif
                    @endforeach
                    @endif
                        <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                           <span class="text-semibold">Required field(s) error or missing</span>
                        </div>

                        <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Wrong format found</span>
                        </div>
                    <form name="alphaForm" id="form-site-description" method="POST" action="{{ route('alpha_admin_post_setting_general') }}" class="form-horizontal">
                    {{csrf_field()}}
                        
                        <fieldset>

                        </fieldset>

                        <div class="text-right">
                            
                                <button type="submit" class="btn btn-primary" id="save-contact">Save</button>
                           
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

    <script>
        $(document).ready(function() {
            $('select').each(function() {

                select_placeholder = $(this).attr('placeholder');
                select_width = $(this).attr('width');
                $(this).select2({
                        minimumResultsForSearch: Infinity,
                        placeholder: select_placeholder,
                        width: select_width,

                });

            });
            
        });

    </script>

    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
        var status = "{{session('msg')}}";
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }
    })
</script>
