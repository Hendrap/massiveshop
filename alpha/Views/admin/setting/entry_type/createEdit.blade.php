<?php
    
    $taxonomies = Setting::whereBundle('alpha.taxonomy')->get();

    $slug = old('slug');
    $single = old('single');
    $plural = old('plural');
    $reorder = old('reorder');
    $custom_text = old('custom_text');
    $show_ui = old('show_ui');
    $meta_key = old('meta_key',array());
    $meta_name = old('meta_name',array());
    $meta_type = old('meta_type',array());
    $meta_data_type = old('meta_data_type',array());
    $entry_taxonomy = old('taxonomies',array());
    $filters = old('filters',array());
    

    if(!empty($setting)){
        $value = unserialize($setting->setting_value);
        $slug = old('slug',@$value['slug']);
        $single = old('single',@$value['single']);
        $plural = old('plural',@$value['plural']);
        $reorder = old('reorder',@$value['reorder']);
        $show_ui = old('show_ui',@$value['show_ui']);
        $filters = old('filters',@$value['filters']);
       

        $default = @$value['taxonomies'];
        $custom_text = old('custom_text',@$value['custom_text']);
        if(empty($default)) $default = [];
        if(empty($filters)) $filters = [];
        $entry_taxonomy = old('taxonomies',$default);
        $tmp_meta_key = array();
        $tmp_meta_name = array();
        $tmp_meta_data_type = array();
        $tmp_meta_type = array();
        $tmp_meta_size = array();

        if(!empty($value['metas'])){
            foreach ($value['metas'] as $k => $val) {
                $tmp_meta_key[] = $val->meta_key;
                $tmp_meta_name[] = $val->meta_name;
                $tmp_meta_data_type[] = $val->meta_data_type;
                $tmp_meta_type[] = @$val->meta_type;
                $tmp_meta_size[] = @$val->meta_size;
            }
            
        }

        $meta_key = old('meta_key',$tmp_meta_key);
        $meta_name = old('meta_name',$tmp_meta_name);
        $meta_data_type = old('meta_data_type',$tmp_meta_data_type);
        $meta_type = old('meta_type',$tmp_meta_type);
        $meta_size = old('meta_size',$tmp_meta_size);
    }
    if(empty($setting)){
        $setting = new StdClass();
        $setting->id = 0;
    }
 ?>


<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - <?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Entry Type</h4>

        </div>

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">

                <li><a href="{{ url('admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="#">Settings</a></li>
                <li class="active"><?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Entry Type</li>


            </ul>
        </div>

    </div>


    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">

                        <h5 class="panel-title">
                            @if($setting->id == 0)
                                Entry Type
                            @else
                                {{ $single }}
                            @endif
                        </h5>

                       
                    </div>

                    
                </div>
                <div class="panel-body">
                    @if($setting->id != 0)
                        <p class="content-group-lg">Created on {{date_format(date_create($setting->created_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->created_at),app('AlphaSetting')->getSetting('time_format'))}} by {{ generateName($setting->user) }}, last modified on {{date_format(date_create($setting->updated_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</p>                    
                    @endif
                    @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        @if(Illuminate\Support\Str::contains($error,'required'))
                            <span class="text-semibold">Required field(s) error or missing</span>
                        @else
                            <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                        @endif
                    </div>
                    @endif
                    @endforeach
                    @endif

                    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                        <span class="text-semibold">Required field(s) error or missing</span>
                    </div>

                    <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                        <span class="text-semibold">Wrong format found</span>
                    </div>

                    <form name="alphaForm" id="form-site-description" action="{{route('admin.setting.entry_type.store')}}" method="POST" class="form-horizontal form-validate-jquery">

                     	{{csrf_field()}}

                        <fieldset>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Slug</label>
                            <div class="col-lg-4">
                                <input id="slug" name="slug" type="text" class="form-control" value="{{$slug}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Label Single</label>
                            <div class="col-lg-4">

                                <input id="single" type="text" name="single" class="form-control" value="{{$single}}" >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Label Plural</label>
                            <div class="col-lg-4">
                                <input id="plural" type="text" name="plural" class="form-control" value="{{$plural}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Custom Text</label>
                            <div class="col-lg-4">
                                <input id="custom_text" type="text" name="custom_text" class="form-control" value="{{$custom_text}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Show UI</label>
                            <div class="col-lg-1">

                                <select id="show_ui" name="show_ui">
                                    <?php $options = ['yes','no']?>
                                    @foreach($options as $v)
                                    <option {{ ($v == $show_ui ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="control-label col-lg-2">Reorder ?</label>
                            <div class="col-lg-1">

                                <select id="reorder" name="reorder">
                                    <?php $options = ['yes','no']?>
                                    @foreach($options as $v)
                                    <option {{ ($v == $reorder ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    
                        <div class="form-group">
                            <label class="control-label col-lg-2">Taxonomies</label>
                             
                            <div class="col-lg-8">
                               
                                <div class="row">
                                    @foreach($taxonomies as $taxo)
                                    <?php $value = unserialize($taxo->setting_value) ?>
                                    <div class="col-sm-4">
                                        <div class="checkbox">
                                            <label>
                                                    <input value="{{$taxo->id}}" name="taxonomies[]" <?php echo in_array($taxo->id, $entry_taxonomy) ? 'checked' : ''  ?> type="checkbox" class="styled">
                                                   {{ $value['single'] }}
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                                
                            </div>
                           
                        </div>


                         <div class="form-group">
                            <label class="control-label col-lg-2">Filters</label>
                             
                            <div class="col-lg-8">
                               
                                <div class="row">
                                    @foreach($taxonomies as $taxo)
                                    <?php $value = unserialize($taxo->setting_value) ?>
                                    <div class="col-sm-4">
                                        <div class="checkbox">
                                            <label>
                                                    <input value="{{$taxo->id}}" name="filters[]" <?php echo in_array($taxo->id, $filters) ? 'checked' : ''  ?> type="checkbox" class="styled">
                                                   {{ $value['single'] }}
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                                
                            </div>
                           
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Metas</label>
                            <div id="meta_container" class="col-lg-8">
                                @foreach($meta_key as $thekey => $theval)
                                    <?php echo newLine(view('alpha::admin.setting.entry_type.template.metas',[
                                        'meta_key' => $theval,
                                        'meta_name' => $meta_name[$thekey],
                                        'meta_data_type' => $meta_data_type[$thekey],
                                        'meta_type' => $meta_type[$thekey],
                                        'meta_size' =>  @$meta_size[$thekey],
                                    ])); ?>
                                @endforeach

                                @if(empty($meta_key))
                                    <?php echo newLine(view('alpha::admin.setting.entry_type.template.metas')) ?>
                                @endif

                            </div>
                        </div>
                            
                        </fieldset>

                       <div class="text-right bottom-button">
                            
                                <a class="btn bg-slate"  href="{{route('admin.setting.entry_type.index')}}">Cancel</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            
                        </div>
                        
                        <input type="hidden" name="id" value="{{$setting->id}}">
                    </form>

                </div>

            </div>

        </div>

    </div>
     <?php
    $err = count($errors);
    $errMsg = "";
    ?>
    {!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'slug' => 'required',
                    'single' => 'required',
                    'plural'  => 'required'
                    
                ]

            ]); 
        !!}

    <style>
        
        .checker span{
            color: #2196F3;
            border: 2px solid #2196F3;
        }
    
    </style>
    <script>
            var meta_template = '<?php echo newLine(view('alpha::admin.setting.entry_type.template.metas')) ?>';
            function initSelect2()
            {
                $('select').each(function() {

                    select_placeholder = $(this).attr('placeholder');
                    select_width = $(this).attr('width');
                    $(this).select2({
                        minimumResultsForSearch: "Infinity",
                        placeholder: select_placeholder,
                        width: select_width,

                    });

                });
                }
        $(document).ready(function() {
           
            initSelect2();
            
        });

        $(document).on('click',".addmeta",function(e){
            e.preventDefault();
            $("#meta_container").append(meta_template);
                        initSelect2();

        });
        $(document).on('click',".delete_meta",function(e){
            e.preventDefault();
            $(this).parent().parent().remove();
        });
        var status = "{{session('msg')}}";
        var err = "{{$err}}";
        
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }

    </script>

    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>

<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
    })
</script>
