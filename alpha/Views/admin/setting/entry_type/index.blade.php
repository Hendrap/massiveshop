
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - Entry Type</h4>
        </div>
        <div class="heading-elements">
           
            
            <a href="{{ route('admin.setting.entry_type.create') }}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>Create New</a>
        </div>
    </div>


</div>


<!-- /page header -->
<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="#">Settings</a></li>
                <li class="active">Entry Type</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">Entry Type</h5>
                       
                    </div>

                    
                </div>
                <div class="panel-body">
                         <p>Explain and describe what will this form do to the main website</p>
                </div>
                
                    <table class="table entry-table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Slug</th>
                                <th class="no-sort text-right"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($entries))
                            @foreach($entries as $entry)
                            <tr>
                                <td class="column-title"><h6><a href="{{ route('admin.setting.entry_type.edit',[$entry->id]) }}">{{ucfirst(str_replace('_',' ',$entry->setting_key))}}</a></h6></td>
                                <td>{{$entry->setting_key}}</td>
                                <td class="text-right table-actions">
                                    
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('admin.setting.entry_type.edit',[$entry->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
                                            <li class="divider"></li>
                                            
                                            <li class=""><a class="confirm" data-id="{{$entry->id}}"  href="#"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                    
                                    
                                    
<!--
                                    <a href="{{ route('admin.setting.entry_type.edit',[$entry->id]) }}" data-popup="tooltip" title="Edit" data-placement="Top"><i class="icon-pencil7"></i></a>
                                    <a href="#" class="confirm" data-id="{{$entry->id}}"   data-popup="tooltip" title="Delete" data-placement="Top"><i class="icon-trash"></i></a>
-->
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                    <script>
                        $(document).ready(function() {
                            $('.entry-table').DataTable({
                                "order": [],
                                "pageLength": 50,
                                "lengthChange": false,
                                "columnDefs": [{
                                    "targets": 'no-sort',
                                    "orderable": false,
                                }]
                            });
                            $('.datatable-header').addClass('clearfix');
//                            var createnewbutton = '
//                            $('.datatable-header').append(createnewbutton);
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                        });

                    </script>
                    <form style="display:none" id="form_delete" action="{{route('admin.setting.entry_type.destroy',[0])}}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="id" value="0" >
    {{ method_field('DELETE') }}
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $(".confirm").click(function(e){
            e.preventDefault();
            $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
            $("#form_delete").find("[name=id]").val($(this).data('id'));
            
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false },
                function(isConfirm){
                    if(isConfirm){
                        $("#form_delete").submit();
                    }
                    swal("Deleted!", "Your Entry Type has been deleted.", "success");
            });
        });

    });
    
</script>

               


            </div>

        </div>

    </div>



    <!-- Footer -->
   {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
