<div class="meta-list clearfix">
	<div class="form-group pull-left mr-20">
		<input type="text" name="meta_key[]" value="{{ @$meta_key }}" class="form-control no-margin">
		<span class="text-muted">Meta Key</span>
	</div>
	<div class="form-group pull-left mr-20">
		<input type="text" name="meta_name[]" value="{{ @$meta_name }}" class="form-control no-margin">
		<span class="text-muted">Meta Name</span>
	</div>
	<div class="form-group pull-left mr-20">
		<select name="meta_type[]" class="form-control" width="80">
			<option value="general">General</option>
			<option value="file_upload">File Upload</option>
			<option value="media">Media</option>
		</select>
	</div>
	<div class="form-group pull-left mr-10">
		<select name="meta_data_type[]" class="form-control" width="80">
		<?php $types = ['text','int','date'] ?>
		<?php foreach($types as $v){ ?>
		<option <?php echo ($v == @$meta_data_type) ? 'selected' : '' ?> value="{{$v}}">{{ucfirst($v)}}</option>
		<?php } ?></select>
	</div>
	<div class="form-group pull-left" style="height: 30px; line-height: 30px;">
		<a href="#" class="delete_meta" style="color:#333">
			<i class="icon-trash"></i>
		</a>&nbsp;
		<a href="#" id="add_meta" style="color:#333"><i class="icon-plus3"></i></a>
	</div>
</div>