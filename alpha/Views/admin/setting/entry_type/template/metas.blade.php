<div class="meta-list clearfix">
	<div class="form-group pull-left mr-20">
		<input name="meta_key[]" value="{{ @$meta_key }}" type="text" class="form-control no-margin">
		<span class="text-muted">Meta Key</span>
	</div>
	<div class="form-group pull-left mr-20">
		<input name="meta_name[]" value="{{ @$meta_name }}" type="text" class="form-control no-margin">
		<span class="text-muted">Meta Name</span>
	</div>
	<div class="form-group pull-left mr-20">
		<select name="meta_type[]" class="form-control" width="80">
			<option <?php echo ('default' == @$meta_type) ? 'selected' : '' ?> value="default">General</option>
			<option <?php echo ('file_upload' == @$meta_type) ? 'selected' : '' ?> value="file_upload">File Upload</option>
			<option <?php echo ('media' == @$meta_type) ? 'selected' : '' ?> value="media">Media</option>
			<option <?php echo ('media_container' == @$meta_type) ? 'selected' : '' ?> value="media_container">Media Container</option>
			<option <?php echo ('multi_lang' == @$meta_type) ? 'selected' : '' ?> value="multi_lang">Multi Language</option>
		</select>
	</div>
	<div class="form-group pull-left mr-20">
	<select name="meta_size[]" class="form-control">
       	@foreach(config('alpha.style.grids') as $k => $v)
       	  <option {{ ($k == @$meta_size ) ? 'selected' : '' }} value="{{$k}}">{{($k)}}</option>
       	@endforeach
     </select>

     </div>                  
	<div class="form-group pull-left mr-10">
		<?php $types = ['text','int','date'] ?>
		<select name="meta_data_type[]" width="80">
		<?php foreach($types as $v){ ?>
		<option <?php echo ($v == @$meta_data_type) ? 'selected' : '' ?> value="{{$v}}">{{ucfirst($v)}}</option>
		<?php } ?>
		</select>
	</div>
	<div class="form-group pull-left" style="height: 30px; line-height: 30px;">
		<a class="delete_meta" href="#" style="color:#333">
			<i class="icon-trash"></i>
		</a>&nbsp;
		<a class="addmeta" href="#" style="color:#333"><i class="icon-plus3"></i></a>
	</div>
</div>