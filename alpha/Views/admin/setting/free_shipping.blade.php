<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - General</h4>
        </div>

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            
                <li><a href="#">Settings</a></li>
                <li class="active">Free Shipping</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">Free Shipping Settings</h5>
                        
                    </div>
                    
                </div>
                <div class="panel-body">


                   
                    @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold">{{ $error }}</span>
                    </div>
                    @endif
                    @endforeach
                    @endif
                        <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                           <span class="text-semibold">Required field(s) error or missing</span>
                        </div>

                        <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Wrong format found</span>
                        </div>
                    <form name="alphaForm" id="form-site-description" method="POST" action="{{ route('alpha_admin_post_setting_general') }}" class="form-horizontal">
                    {{csrf_field()}}
                        
                        <fieldset>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Enabled ?</label>
                                <div class="col-lg-4">
                                   <select name="is_enable_free_shipping" width="150">
                                        <option <?php if(app('AlphaSetting')->getSetting('is_enable_free_shipping') == 'yes'){echo 'selected';}?> value="yes">Yes</option>
                                        <option <?php if(app('AlphaSetting')->getSetting('is_enable_free_shipping') == 'no'){echo 'selected';}?> value="no">No</option>
                                   </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Minimum Order(IDR)</label>
                                <div class="col-lg-2">
                                    <input type="number" id="free_shipping_min_order" name="free_shipping_min_order" class="form-control" value="{{ app('AlphaSetting')->getSetting('free_shipping_min_order') }}" placeholder="">
                                </div>
                            </div>

                            

                        </fieldset>
                        <fieldset>
                        <input type="hidden" id="free_shipping_products" name="free_shipping_products" value="">
                            <legend class="text-semibold">
                                Free Shipping Products
                            </legend>
                                <div class="form-group">
                                <div class="col-lg-6" id="vuePromo">
                                    <table class="table pb-10 border-bottom table-order-items" style="border-color:#ddd;z-index: 98;position: relative;">
                                    
                                        <thead>
                                        
                                            <tr>
                                                <th>Item</th>
                                                <th></th>
                                            </tr>
                                            
                                        </thead>
                                        <tbody>
                                           <tr v-for="product in products">
                                                <td>${preventNullValue(product.product_title)} [SKU : ${preventNullValue(product.sku)}]</td>
                                                <td><a @click="deleteProduct($index)" href="javascript:void(0)" class="text-black"><i class="icon-trash"></i></a></td>
                                            </tr>
                                            <tr class="table-input-row">
                                                <td colspan="2">
                                                    <div class="add-product-container">
                                                        <input v-model="inputSearchProduct" debounce="500" type="text" class="form-control" placeholder="Search by product name or SKU" id="add-product">
                                                        <div v-show="searchProducts.length" class="product-search-result">
                                                            <ul class="autocomplete-search-result-list product-search-result-list">
                                                                <li v-for="product in searchProducts">
                                                                    <a @click="selectProduct($index)" href="javascript:void(0)">
                                                                        <h5>${preventNullValue(product.product_title)} [SKU : ${preventNullValue(product.sku)}]</h5>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-right">
                            
                                <button type="submit" class="btn btn-primary" id="save-contact">Save</button>
                           
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

    <script>
        $(document).ready(function() {
            $('select').each(function() {

                select_placeholder = $(this).attr('placeholder');
                select_width = $(this).attr('width');
                $(this).select2({
                        minimumResultsForSearch: Infinity,
                        placeholder: select_placeholder,
                        width: select_width,

                });

            });
            
        });

    </script>

    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
        var status = "{{session('msg')}}";
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }
    })
</script>
<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/framework/vue.js') }}"></script>
<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/framework/vue-resource.min.js') }}"></script>
<script type="text/javascript">
    Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
        c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "," : t, 
        s = n < 0 ? "-" : "", 
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
     };

    function showLoading(el)
    {
        $(el).block({
            message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: '10px 15px',
                color: '#333',
                width: 'auto',
                "margin-top": '120px',
            }
        });
    }
    
    function hideLoading()
    {
        $(".blockUI").remove()
    }

    function unReactiveData(data){
        return JSON.parse(JSON.stringify(data));
    }
    
    $("#add-product").focusin(function(e){
        e.stopPropagation();
        $('.product-search-result-list').addClass('show');
    });

    $(document).click(function(){
        $('.product-search-result-list').removeClass('show');
    });

    $("#add-product").click(function(e){
        $('.product-search-result-list').addClass('show');
    })
    var urlSearchProducts = "<?php echo route('catalog_pos_get_products') ?>";

    var data = {
        products:[],
        searchProducts:[],
        showSearchInputProducts:false,
        showProductsEmptyState:true,
        inputSearchProduct:""
    };

    Vue.http.headers.common['X-CSRF-TOKEN'] = "{{ csrf_token() }}";
    Vue.config.delimiters = ['${', '}'];

    $("#form-site-description").submit(function(e){
        $("#free_shipping_products").val(JSON.stringify(vuePromo.products));
    });

    data.products = <?php echo app('AlphaSetting')->getSetting('free_shipping_products'); ?>;
    var vuePromo = new Vue({
        el:"#vuePromo",
        data:data,
        watch:{
             'inputSearchProduct':function(val,oldVal){
                if(val != oldVal){
                    if(val.trim() != "")
                    {
                        this.getProducts();                        
                    }
                }
            },
        },
        methods:{
            preventNullValue(val){
                if(val == null) return "";

                return val;
            },
            getProducts()
            {
              $("#add-product").prop('disabled',true);
               this.$http.post(urlSearchProducts,{q:$("#add-product").val()},function(data,s,r){
                    $('.product-search-result-list').addClass('show');
                    this.searchProducts = data.items;   
                    this.$nextTick(function(){
                      $("#add-product").prop('disabled',false);
                      $("#add-product").focus();
                    });
               });
            },
            selectProduct(index)
            {
                var item = {};
                item.item_id =  unReactiveData(this.searchProducts[index].item_id);
                item.product_title = unReactiveData(this.searchProducts[index].product_title);
                item.sku = unReactiveData(this.searchProducts[index].sku);

                this.products.push(item);
                this.searchProducts.splice(index,1);
                this.inputSearchProduct = "";

            },
            initSearchProducts()
            {
                this.showSearchInputProducts = true;
                this.showProductsEmptyState = false;                
            },
            deleteProduct(index)
            {
                var that = this;
                swal({
                    title: "Delete ?",
                    text: "You will not be able to recover deleted items, are you sure want to delete ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: true },
                    function(isConfirm){
                        if(isConfirm){
                            that.products.splice(index,1);
                        }
               });
            }
        }
    });
</script>