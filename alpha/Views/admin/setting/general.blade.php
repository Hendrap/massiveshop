
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">General</span> - Site Description</h4>
        </div>

    </div>


</div>



<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                
                <li><a href="#">General</a></li>
                <li class="active">Site Description</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

    <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">Site Description</h5>
                        
                    </div>

                    
                </div>
    <div class="panel-body">
        
        <p class="content-group-lg">Please provide site name and description, including the default SEO keywords.</p>
        
         @if(count($errors) > 0)
         @foreach($errors->all() as $key => $error)
         @if($key == 0)
         <div class="alert bg-danger alert-styled-left">
             <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
             @if(Illuminate\Support\Str::contains($error,'required'))
                 <span class="text-semibold">Required field(s) error or missing</span>
             @else
                 <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
             @endif
         </div>
         @endif
         @endforeach
         @endif

         <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
             <span class="text-semibold">Required field(s) error or missing</span>
         </div>

         <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
             <span class="text-semibold">Wrong format found</span>
         </div>

        <form class="form-horizontal" name="alphaForm" method="POST"  action="{{ route('alpha_admin_post_setting_general',['site_desc']) }}">
            {{ csrf_field() }}
            <fieldset>
                <legend class="text-semibold">
                    GENERAL INFORMATION
                </legend>
                        
                <div class="form-group">
                            <label class="control-label col-lg-2">Site Name</label>
                            <div class="col-lg-10">
                                <input type="text" name="site_name" class="form-control" id="site_name" value="{{ old('site_name',app('AlphaSetting')->getSetting('site_name'))  }}" >
                            </div>
                        </div>

                <div class="form-group">
                            <label class="control-label col-lg-2">Site Tagline</label>
                            <div class="col-lg-10">
                                <input type="text" name="site_tagline" class="form-control" id="site_tagline" value="{{ old('site_tagline',app('AlphaSetting')->getSetting('site_tagline')) }}">
                            </div>
                        </div>
                        
                <div class="form-group">
                            <label class="control-label col-lg-2">Site Description</label>
                            <div class="col-lg-10">
                              <textarea name="site_desc" class="form-control" id="site_desc" value="">{{ old('site_desc',app('AlphaSetting')->getSetting('site_desc')) }}</textarea>
                            </div>
                        </div>
            </fieldset>
            
            <fieldset>
                <legend class="text-semibold">
                    Search Engine Optimization
                </legend>

                <div class="form-group">
                            <label class="control-label col-lg-2">SEO Title</label>
                            <div class="col-lg-10">
                                <input type="text" name="seo_title" class="form-control" placeholder="Customize your page title for SEO purpose" id="seo_title" value="{{ app('AlphaSetting')->getSetting('seo_title') }}">
                            </div>
                        </div>
                
                <div class="form-group">
                            <label class="control-label col-lg-2">SEO Description</label>
                            <div class="col-lg-10">
                                <textarea name="seo_desc" class="form-control" value="" id="seo_desc" placeholder="Default site description being used on your website">{{ app('AlphaSetting')->getSetting('seo_desc') }}</textarea>
                            </div>
                        </div>
                
                <div class="form-group">
                            <label class="control-label col-lg-2">SEO Keywords</label>
                            <div class="col-lg-10">
                                <input type="text" name="seo_keywords" class="form-control" placeholder="Default SEO keywords being used on your website" id="seo_keywords" placeholder="" value="{{ app('AlphaSetting')->getSetting('seo_keywords') }}">
                            </div>
                        </div>
                
            </fieldset>

        
            <div class="text-right">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div>
    <?php
    $err = count($errors);
    ?>
   {!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'site_name' => 'required',
                    'site_tagline' => 'required',
                    'site_desc' => 'required',
                    'seo_title' => 'required',
                    'seo_desc' => 'required',
                    'kurs_id' => 'required',
                    'kurs_euro' => 'required',
                    'seo_keywords' => 'required'
                ]

        ]); 
   !!}
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
        


        var status = "{{session('success')}}";
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }
    })
</script>
