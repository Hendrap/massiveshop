<td>
<?php 
$html = '';
$server = json_decode($log->server);
$html .= 'REQUEST URI : '.@$server->REQUEST_URI.'<br><br>';
$html .= 'IP : '.@$server->REMOTE_ADDR.'<br><br>';
$html .= 'USER AGENT :<br> '.@$server->HTTP_USER_AGENT.'<br><br>';
$html .= 'REQUEST TIME : '.date(app('AlphaSetting')->getSetting('date_format').' '.app('AlphaSetting')->getSetting('time_format'),@$server->REQUEST_TIME);
 ?>
<a data-popup="popover" data-trigger="hover" title="Detailed Information" data-html="true" data-content="<?php echo $html ?>" style="color:#000"><i class="icon-eye2"></i></a>
</td>
|alpha--datatable-separator--|<td class="td-date">{{ date(app('AlphaSetting')->getSetting('date_format'),strtotime($log->created_at)) }}<br><span class="text-muted">{{ date(app('AlphaSetting')->getSetting('time_format'),strtotime($log->created_at)) }}</span></td>
|alpha--datatable-separator--|<td>
@if(!empty($log->user->metas))
{{ getEntryMetaFromArray($log->user->metas,'first_name').' '.getEntryMetaFromArray($log->user->metas,'last_name') }}
@else
	{{$log->email}}

@endif<br><span class="text-muted">{{@$log->user->roles[0]->name}}</span></td>
|alpha--datatable-separator--|<td><h6>{{$log->description}}</h6></td>