<?php
    $name = old('name');
    $width = old('width');
    $height = old('height');
    $crop = old('crop');
    $aspectratio = old('aspectratio');
    if(!empty($setting)){
        $value = unserialize($setting->setting_value);
        $name = old('name',@$value['name']);
        $width = old('width',@$value['width']);
        $height = old('height',@$value['height']);
        $crop = old('crop',@$value['crop']);
        $aspectratio = old('aspectratio',@$value['aspectratio']);
    }
    if(empty($setting)){
        $setting = new StdClass();
        $setting->id = 0;
    }

 ?>
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - <?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Image Handling</h4>
        </div>

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            
                <li><a href="#">Settings</a></li>
                <li class="active"><?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Image Handling</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">
                        @if($setting->id == 0)
                            Image Handling
                        @else
                            {{ ucfirst($name) }}
                        @endif

                        </h5>
                       
                    </div>

                    
                </div>
                <div class="panel-body">
                @if($setting->id != 0)
                        <p class="content-group-lg">Created on {{date_format(date_create($setting->created_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->created_at),app('AlphaSetting')->getSetting('time_format'))}} by {{ generateName($setting->user) }}, last modified on {{date_format(date_create($setting->updated_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($setting->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</p>                    
                    @endif
                    @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        @if(Illuminate\Support\Str::contains($error,'required'))
                            <span class="text-semibold">Required field(s) error or missing</span>
                        @else
                            <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                        @endif
                    </div>
                    @endif
                    @endforeach
                    @endif
                    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                           <span class="text-semibold">Required field(s) error or missing</span>
                        </div>

                        <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Wrong format found</span>
                        </div>
                    <form name="alphaForm" id="form-site-description" method="POST" action="{{route('admin.setting.image.store')}}" class="form-horizontal form-validate-jquery">

                       
                        <fieldset>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Name</label>
                            <div class="col-lg-4">
                                <input id="name" type="text" name="name" value="{{$name}}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">Width</label>
                            <div class="col-lg-2">
                                <input id="width" type="text" name="width" value="{{$width}}" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Height</label>
                            <div class="col-lg-2">
                                <input id="height" type="text" name="height" value="{{$height}}" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Croppable</label>
                            <div class="col-lg-1">
                                <select id="crop" name="crop">
                                    <?php $options = ['yes','no']?>
                                    @foreach($options as $v)
                                    <option {{ ($v == $crop ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Aspect Ratio</label>
                            <div class="col-lg-1">
                                <select id='aspectratio' name="aspectratio">
                                    <?php $options = ['yes','no']?>
                                    @foreach($options as $v)
                                    <option {{ ($v == $aspectratio ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        </fieldset>
                            
                        {{csrf_field()}}

                        <div class="text-right bottom-button">
                            
                                <input type="hidden" name="id" value="{{$setting->id}}">
                                <a class="btn bg-slate"  href="{{route('admin.setting.image.index')}}">Cancel</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
    <style>
        
        .checker span{
            color: #2196F3;
            border: 2px solid #2196F3;
        }
    
    </style>
    <script>

        $(document).ready(function() {
           
            $('select').each(function() {

                select_placeholder = $(this).attr('placeholder');
                select_width = $(this).attr('width');
                $(this).select2({
                    minimumResultsForSearch: Infinity,
                    placeholder: select_placeholder,
                    width: select_width,

                });

            });
            
        });

    </script>

    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
<?php
    $err = count($errors);
    $errMsg = "";
?>
{!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'slug' => 'required',
                    'width' => 'required|numeric',
                    'height'  => 'required|numeric'
                    
                ]

            ]); 
        !!}
    
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
        var status = "{{session('msg')}}";
        var err = "{{$err}}";
        
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }

    })
</script>
