<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - Image Handling</h4>
        </div>
        <div class="heading-elements">
           
             <a href="{{ route('admin.setting.image.create') }}" class="btn btn-labeled heading-btn bg-brand"><b><i class="icon-file-plus"></i></b>New Image Type</a>
        </div>
    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
           
                <li><a href="#">Settings</a></li>
                <li class="active">Image Handling</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">Image Handling</h5>
                        
                    </div>

                   
                </div>
                <div class="panel-body">
                    
                </div>
                    <div class="table-header datatable-header clearfix">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-10 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter: </label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom" style="width:165px">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                        
                        
                       
 
                    </div>

                    <table class="table entry-table table-one-line table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th width="95px">Height</th>
                                <th width="95px">Width</th>
                                <th width="125px">Croppable</th>
                                <th width="50px" class="no-sort text-right"></th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($crops as $crop)
                            <tr>
                                <?php $value = unserialize($crop->setting_value) ?>
                                <td class="title-column"><h6><a href="{{ route('admin.setting.image.edit',[$crop->id]) }}">{{ ucfirst($value['name']) }}</a></h6></td>
                                
                                <td>{{ @$value['width'] }}</td>
                                <td>{{ @$value['height'] }}</td>
                                <td>Yes</td>
                               
                                 <td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('admin.setting.image.edit',[$crop->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
                                           <li class="divider"></li>
                                            <li><a class="confirm" data-id="{{$crop->id}}"  href="{{ route('admin.setting.image.destroy',[$crop->id]) }}?_method=delete"><i class="icon-trash"></i> Delete</a></li>
                                            
                                        </ul>
                                    </div>
                                </td>
                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <script>
                        $(document).ready(function() {


                            
                           var entry_table = $('.entry-table').DataTable({
                                "order": [],
                                "searching" : true,
                                "lengthChange": false,
                                "pageLength": 50,
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "columnDefs": [{
                                    "targets": 'no-sort',
                                    "orderable": false,
                                }]
                            });
                           
                           var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                   
                                });
                                
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                                
                            });
                            
                            $('#search_the_table').keyup(function(){
                                
                                
                                entry_table.search($(this).val()).draw();
                                 
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                             $('.table-select2').each(function() {

                                 
                                 
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',

                                });

                            });
                            
                        });

                    </script>


                


            </div>

        </div>

    </div>



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
<form style="display:none" id="form_delete" action="{{route('admin.setting.image.destroy',[0])}}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="id" value="0" >
    {{ method_field('DELETE') }}
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $(".confirm").click(function(e){
            e.preventDefault();
            
            $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
            $("#form_delete").find("[name=id]").val($(this).data('id'));
                
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false },
                function(isConfirm){
                    // console.log(isConfirm);
                    if(isConfirm){
                        $("#form_delete").submit();
                    }
                    swal("Deleted!", "Your Image Type has been deleted.", "success");
            });
        });

    });
    
</script>
