<!-- Page header -->

<div class="page-header page-header-default">

    <div class="page-header-content">

        <div class="page-title">

            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Plugins</span> - All Plugins</h4>

        </div>



    </div>





</div>





<!-- /page header -->





<!-- Content area -->

<div class="content">



    <div class="panel panel-flat">



        <div class="breadcrumb-line">

            <ul class="breadcrumb">

                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>

                <li><a href="#">Plugins</a></li>

                <li class="active">All Plugins</li>



            </ul>

        </div>

     

    </div>



    <div class="row">



        <div class="col-xs-12">



            <div class="panel panel-flat">



                <div class="panel-heading">

                    <div class="panel-heading-title">

                        <h5 class="panel-title">Plugins</h5>

                       

                    </div>



                   

                </div>

                <div class="panel-body">

                     <p>List of all plugins installed on your site.</p>

                </div>

                

                    @if(count($errors) > 0)

                    @foreach($errors->all() as $key => $error)

                    @if($key == 0)

                    <div class="alert bg-danger alert-styled-left">

                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>

                        <span class="text-semibold">{{ $error }}</span>

                    </div>

                    @endif

                    @endforeach

                    @endif

                    <div class="table-header datatable-header clearfix">

                    

                        <div class="filter-table clearfix pull-left mr-5">

                            <div class="form-group pull-left  mr-5 no-margin-bottom">

                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter</label>

                            </div>

                            <div class="form-group pull-left has-feedback  no-margin-bottom">

                                <input type="text" class="form-control" id="search_the_table" style="height:36px">

                                <div class="form-control-feedback" style="line-height:36px;">

                                        <i class="icon-search4" style="font-size:12px"></i>

                                </div>

                            </div>

                            

                        </div>

                        

                        <select class="table-select2 pull-left filter-table-column" placeholder="Filter by Status" searchin="1">

                            <option value="all">All Status</option>

                            <option value="disabled">Disabled</option>

                            <option value="active">Active</option>

                            

                        </select>

                      

                        

                      

                        

                    </div>

                    



                    <table class="table entry-table table-striped">

                        <thead>

                            <tr>

                                <th>Name</th>

                                <th width="120px">Status</th>

                                <th width="50px" class="no-sort text-right"></th>

                                <th class="hide"></th>

                            </tr>

                        </thead>

                        <tbody>

                            

                            @foreach($plugins as $key => $plugin)

                                <?php $setting = $parsedSettings[$plugin->provider]; ?>

                                @if(!empty($setting))

                                <tr>

                                    <td class="title-column"><h6>{{ $plugin->name }}</h6></td>

                                   

                                    <?php

                                        

                                        if($setting->setting_value == 'yes'){

                                            $class = 'success';

                                            $label = 'Active';

                                        }else{

                                            $class = 'danger';

                                            $label = 'Disabled';

                                        }

                                     ?>

                                    <td><label class="label bg-{{$class}}">{{$label}}</label></td>

                                <td class="text-right table-actions">

                                    <div class="btn-group">

                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">

                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>

                                        </button>



                                        <ul class="dropdown-menu dropdown-menu-right">

                                            @if($setting->setting_value  == 'yes')

                                                <li><a href="{{ route('alpha_admin_get_status_plugins',[$setting->id,'no']) }}"><i class="icon-cancel-square"></i> Disable</a></li>

                                            @else

                                                <li><a href="{{ route('alpha_admin_get_status_plugins',[$setting->id,'yes']) }}"><i class="icon-check"></i> Enable</a></li>

                                            @endif

                                            

                                        </ul>

                                    </div>

                                </td>

                                <td class="hide">{{strtolower($label)}} </td>

                            </tr>

                            @endif

                            @endforeach

                        </tbody>

                    </table>



                    <script>

                        var entry_table = {};

                        $(document).ready(function() {



                            var status = "{{session('msg')}}";

                            if (status === 'Data Saved!') {

                                swal({

                                    title: "SUCCESS",

                                    text: "All changes has been saved successfuly",

                                    confirmButtonColor: "#66BB6A",

                                    type: "success"

                                    });

                            }

                            

                            entry_table = $('.entry-table').DataTable({

                                "order": [],

                                "searching" : true,

                                "lengthChange": false,

                                "dom": 'rt<"datatable-footer"ilp><"clear">',

                                "columnDefs": [{

                                    "targets": 'no-sort',

                                    "orderable": false,

                                }]

                            });

                           

                            var filterby;

                            var searchin;

                            

                            $('.filter-table-column').change(function(){

                                

                                thisfilter = $(this);

                                thisfilter.children('option:selected').each(function(){

                                    filterby = $(this).val();

                                    searchin = thisfilter.attr('searchin');

                                });

                                

                                if(filterby == 'all'){

                                   entry_table.columns(searchin).search("").draw();

                                }

                                else{

                                   entry_table.columns(searchin).search(filterby).draw();

                                }

                                

                            });

                            

                            $('#search_the_table').keyup(function(e){

                                if(e.which == 13)

                                {

                                 entry_table.search($(this).val()).draw();

                                }

                            });

                            

                            $('select').select2({

                                minimumResultsForSearch: Infinity,

                                width: 'auto'

                            });

                             $('.table-select2').each(function() {



                                select_placeholder = $(this).attr('placeholder');

                                $(this).select2({

                                    minimumResultsForSearch: Infinity,

                                    placeholder: select_placeholder,

                                    width: '200px',



                                });



                            });

                        });





                    </script>





                





            </div>



        </div>



    </div>







    <!-- Footer -->

    {!! view('alpha::admin.partials.content-footer') !!}

    <!-- /footer -->



</div>

<!-- /content area -->

















            </div>

            <!-- /main content -->



        </div>

        <!-- /page content -->

