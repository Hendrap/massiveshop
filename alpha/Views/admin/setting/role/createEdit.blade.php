<?php

    if(empty($role)){
        $role = new StdClass();
        $role->id = 0;
        $role->name = '';
        $role->rules = array();
    }

    $roleRules = [];
    $tmpRules = Rule::where('id','!=',1)->get();
    $rules = array();
    
    foreach ($tmpRules as $key => $value) {
        @$rules[$value->group][] = $value->id;
    }

    if(!empty($role)){
        foreach ($role->rules as $key => $value) {
            $roleRules[] = $value->id;
        }
    }

    $roleRules = old('rules',$roleRules);

?>
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Accounts</span> - {{ ($role->id == 0 ? 'Create' : 'Edit') }} Role</h4>
        </div>

    </div>


</div>


<!-- /page header -->



<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                
                <li><a href="{{ route('alpha_admin_all_account') }}">Accounts</a></li>
                <li class="active">{{ ($role->id == 0 ? 'Create' : 'Edit') }} Role</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">

                        <h5 class="panel-title">
                        @if($role->id == 0)
                            Role
                        @else
                            {{ $role->name }}
                        @endif
                        </h5>
                        
                    </div>

                    
                </div>
                <div class="panel-body">

                    @if($role->id > 0)
                    <p class="content-group-lg">Created on {{date_format(date_create($role->created_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($role->created_at),app('AlphaSetting')->getSetting('time_format'))}} am by {{ generateName($role->user) }}, last modified on {{date_format(date_create($role->updated_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($role->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</p>
                    @endif
                   @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        @if(Illuminate\Support\Str::contains($error,'required'))
                            <span class="text-semibold">Required field(s) error or missing</span>
                        @else
                            <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                        @endif
                    </div>
                    @endif
                    @endforeach
                    @endif
                    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Required field(s) error or missing</span>
                         </div>

                         <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                             <span class="text-semibold">Wrong format found</span>
                         </div>

                    <form name="alphaForm" action="{{route('admin.accounts.roles-and-access.store')}}" method="POST" id="form-site-description" class="form-horizontal form-validate-jquery">
                        
                    <div class="fieldset">
                    <input type="hidden" name="id" value="{{$role->id}}">
                        
                        <div class="form-group">
                            <label class="control-label col-lg-1">Role</label>
                            <div class="col-lg-11">
                                <input id="name" type="text" name="name" class="form-control" value="{{old('name',$role->name)}}">
                            </div>
                        </div>

                        
                        <table class="table role-table table-one-line">
                        
                            <tr>
                                <th>Access</th>
                                <th width="50px">Write</th>
                                <th width="50px">Read</th>
                            </tr>
                            
                            @foreach($rules as $key => $rule)
                            <tr>
                                <td class="text-bold">
                                	{{$key}}
                                </td>
                                @foreach($rule as $r)
                                <td><input <?php echo in_array($r, $roleRules) ? 'checked' : ''; ?> value="{{$r}}" name="rules[]"  type="checkbox" class="styled"></td>
                                @endforeach

                                @if(count($rule) < 2)
                                    <td></td>
                                @endif
                            </tr>
                            @endforeach
                            {{csrf_field()}}
                        </table>
                       
                    
                    </div>
                
                       <div class="text-right">
                            
                            <a href="{{route('admin.accounts.roles-and-access.index')}}" class="btn btn-primary bg-slate">Cancel</a>
                           <button type="submit" class="btn btn-primary ml-10" >Save</button>
                           
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

        <?php
            $err = count($errors);
            $errMsg = "";
        ?>
        {!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'name' => 'required',
                ]

            ]); 
        !!}

    <script>
        
        $(document).ready(function() {

            autosize($('textarea'));
            var status = "{{session('msg')}}";
            if (status === 'Data Saved!') {
                swal({
                    title: "SUCCESS",
                    text: "All changes has been saved successfuly",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                    });
            }

            var err = "{{$err}}";
            
            
            
        });

    </script>
    <style>
        .checker span{
            color: #2196F3;
            border: 2px solid #2196F3;
        }
    </style>
    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
    })
</script>
