<div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Accounts</span> - Role &amp; Access</h4>

            </div>
            <div class="heading-elements">
            
                <a href="{{ route('admin.accounts.roles-and-access.create') }}" class="btn btn-labeled pull-right bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>New Role</a>
            
            </div>
        </div>


    </div>

<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('alpha_admin_all_account') }}">Accounts</a></li>
                <li class="active">Roles &amp; Access</li>


            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">Roles &amp; Access</h5>

                       
                    </div>


                </div>
                <div class="panel-body">

                    <p class="content-group-lg">Create custom roles and limit access to certain features.</p>
                </div>

                    <div class="table-header datatable-header clearfix">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-5 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter: </label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                        
                        
                        
                       
                    </div>
                    <table class="table entry-table table-striped">
                        <thead>
                            <tr>
                                <th>Role</th>

                                <th width="140px"><span>Created</span></th>
                                <th width="150px"><span>Modified</span></th>
                                <th width="50px" class="no-sort text-right"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)

                                <tr>
                                <td class="title-column"><h6><a href="{{ route('admin.accounts.roles-and-access.edit',[$role->id]) }}">{{$role->name}}</a></h6></td>
                                    <td class="td-date">
                                      
                                            @if(!empty($role->created_at))
                                                {{date_format($role->created_at,app('AlphaSetting')->getSetting('date_format'))}}
                                                <br><span class="text-muted">{{date_format($role->created_at,app('AlphaSetting')->getSetting('time_format'))}}</span>
                                            @endif
                                        
                                    </td>
                                    <td class="td-date">
                                        
                                            @if(!empty($role->updated_at))
                                                 {{date_format($role->updated_at,app('AlphaSetting')->getSetting('date_format'))}}
                                                <br><span class="text-muted">{{date_format($role->updated_at,app('AlphaSetting')->getSetting('time_format'))}}</span>
                                            @endif
                                        
                                    </td>
                                    <td class="text-right table-actions">

                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('admin.accounts.roles-and-access.edit',[$role->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" class="confirm" data-id="{{$role->id}}"  href="{{ route('admin.accounts.roles-and-access.destroy',[$role->id]) }}?_method=delete"><i class="icon-trash"></i> Delete</a></li>
                                            
                                        </ul>
                                    </div>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <script>
                        $(document).ready(function() {

                            var entry_table = $('.entry-table').DataTable({
                                "order": [],
                                "searching" : true,
                                "lengthChange": false,
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "pageLength": 50,
                                "columnDefs": [{
                                    "targets": 'no-sort',
                                    "orderable": false,
                                }]
                            });

                            
                            $('#search_the_table').keyup(function(){
                                
                                entry_table.search($(this).val()).draw();
                                 
                            });
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });

                        });

                    </script>


                


            </div>

        </div>

    </div>



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
<form style="display:none" id="form_delete" action="{{route('admin.accounts.roles-and-access.destroy',[0])}}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="id" value="0" >
    {{ method_field('DELETE') }}
</form>

<script type="text/javascript">
    $(document).ready(function(){
        
        $(".confirm").click(function(e){
            e.preventDefault();
            $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
            $("#form_delete").find("[name=id]").val($(this).data('id'));
            swal({
                title: "Delete ?",
                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false },
                function(isConfirm){
                if(isConfirm){
                    $("#form_delete").submit();
                }
            });
            
        });

    });
    
</script>
