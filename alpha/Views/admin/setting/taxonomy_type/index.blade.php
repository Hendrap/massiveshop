


<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Settings</span> - Taxonomy Config</h4>
        </div>
        <div class="heading-elements">
            <a href="{{ route('admin.setting.taxonomy_type.create') }}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>Create New</a>
        </div>
    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
               
                <li><a href="#">Settings</a></li>
                <li class="active">Taxonomy Config</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">Taxonomy Type</h5>
                    </div>

                    
                </div>
                <div class="panel-body">
                    
                </div>

                <table class="table entry-table table-one-line table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Slug</th>
                                <th width="50px" class="no-sort text-right"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($taxonomies as $taxonomy)
                            <tr>
                            <?php $value = unserialize($taxonomy->setting_value) ?>
                                <td class="title-column"><h6><a href="{{ route('admin.setting.taxonomy_type.edit',[$taxonomy->id]) }}">{{ ucfirst($value['single']) }}</a></h6></td>
                                <td>{{ @$value['slug'] }}</td>
                                 <td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('admin.setting.taxonomy_type.edit',[$taxonomy->id]) }}"><i class="icon-pencil"></i> Edit</a></li>
                                            <li class="divider"></li>
                                            
                                            <li><a class="confirm" data-id="{{$taxonomy->id}}"  href="{{ route('admin.setting.taxonomy_type.destroy',[$taxonomy->id]) }}?_method=delete" href="#"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            
                            @endforeach

                        </tbody>
                    </table>
                    <script>
                        $(document).ready(function() {


                            
                            $('.entry-table').DataTable({
                                "order": [],
                                "pageLength": 50,
                                "lengthChange": false,
                                "columnDefs": [{
                                    "targets": 'no-sort',
                                    "orderable": false,
                                }]
                            });
                             $('#search_the_table').keyup(function(){
                                
                                
                                entry_table.search($(this).val()).draw();
                            });
                            $('.datatable-header').addClass('clearfix');
//                            var createnewbutton = ''
//                            $('.datatable-header').append(createnewbutton);
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                        });

                    </script>
               
            </div>
        </div>
    </div>
</div>
<form style="display:none" id="form_delete" action="{{route('admin.setting.taxonomy_type.destroy',[0])}}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="id" value="0" >
    {{ method_field('DELETE') }}
</form>


<script type="text/javascript">
    $(document).ready(function(){
        $(".confirm").click(function(e){
            e.preventDefault();
            
            $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
            $("#form_delete").find("[name=id]").val($(this).data('id'));
                
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false },
                function(isConfirm){
                    // console.log(isConfirm);
                    if(isConfirm){
                        $("#form_delete").submit();
                    }
                    swal("Deleted!", "Your Taxonomy Type has been deleted.", "success");
            });
        });

    });
    
</script>
