<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>

                <li><a href="{{ route('alpha_admin_entry_index',['slider']) }}">{{ $config['plural'] }}</a></li>
                <li class="active">Edit {{ $config['single'] }}</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">{{ $config['single'] }} Content</h5>

                    </div>


                </div>
                <div class="panel-body">

                        @if(count($errors) > 0)
                        @foreach($errors->all() as $key => $error)
                        @if($key == 0)
                        <div class="alert bg-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            @if(Illuminate\Support\Str::contains($error,'required'))
                                <span class="text-semibold">Required field(s) error or missing</span>
                            @else
                                <span class="text-semibold">{{ str_replace('en.','',$error) }}</span>
                            @endif
                        </div>
                        @endif
                        @endforeach
                        @endif

                    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                       <span class="text-semibold">Required field(s) error or missing</span>
                    </div>

                    <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                        <span class="text-semibold">Wrong format found</span>
                    </div>
                    <form name="alphaForm" class="form-horizontal" action="{{ route('alpha_admin_entry_save',[$type,$entry->id]) }}" method="POST">
                    	{{csrf_field()}}
                        
                        <fieldset>
                        
                    	<input type="hidden" name="id" value="{{@$entry->id}}">
                    	<input type="hidden" name="status" value="{{@$entry->status}}">
                    	<input type="hidden" name="published_at" value="{{date('Y-m-d H:i:s')}}">
                        <div class="form-group">

                            <div class="col-sm-2">
                                <label>{{ $config['single'] }} Title</label>
                            </div>
                            <div class="col-sm-10">
                            @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                <input style="display:none" id="title_{{$key}}" name="title[{{$key}}]" type="text" class="form-control" value="{{old('title.'.$key,parseMultiLang($entry->title,$key))}}">
                            @endforeach
                             @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                <input style="display:none" id="excerpt_{{$key}}" name="excerpt[{{$key}}]" type="hidden" class="form-control" value="{{old('title.'.$key,parseMultiLang($entry->excerpt,$key))}}">
                            @endforeach
                            </div>

                        </div>
                        <div class="form-group">

                            <div class="col-sm-2">
                                <label>Description</label>
                            </div>
                            <div class="col-sm-10">
                            @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                <textarea class="form-control" style="display:none" id="content_{{$key}}" name="content[{{$key}}]">{{old('content.'.$key,strip_tags(parseMultiLang($entry->content,$key)))}}</textarea>
                            @endforeach
                            </div>

                        </div>
                            
                            
                        </fieldset>
                        
                        <div class="text-right mb-20">

                            
                            <a href="{{ route('alpha_admin_entry_index',['slider']) }}" class="btn bg-slate mr-10">Cancel</a>
                            
                            <button type="submit" class="btn btn-primary pull-right">Save</button>

                        </div>
                        
                        <div class="slider-list">

                            <div class="slider-list-header clearfix mb-15">

                                <div class="form-group pull-left  mr-5 no-margin-bottom">
                                    <label style="height: 30px; line-height: 30px;margin-bottom: 0px;">Media</label>
                                </div>
                            </div>

                            <div class="slider-list-body">
                            <div class="media-row slider-thumbnail-row ui-sortable">
                                
                                    <div id="containerMedia">
                                    	<div class="media-grid  add-image-cont">
                                             <a href="#" class="add-image">

                                                <div class="thumbnail">
                                                    
                                                    
                                                           
                                                            <div class="thumb-overlay" id="btnShowEntryMedia">

                                                                <span>Add Image(s)</span>
                                                                <span class="btn btn-rounded btn-primary">+</span>

                                                            </div>
                                                     
                                                    
                                                    <div class="thumb add-new">
                                                        <img src="{{asset('backend/assets/images/slide-thumbnail-big.png')}}" alt="image name">
                                                    </div>
                                                     <div class="caption">
                                                        <h6 class="no-margin-top text-semibold">
                                                            <a href="#" class="text-default">&nbsp;</a>
                                                           
                                                            
                                                            
                                                        </h6>
                                                        <span>&nbsp;</span>
                                                    </div>
                                                   
                                                </div>
                                                </a>
                                            </div>
                                        <?php
                            
                                            $res = getMedias($entry->medias);
                                            echo $res;
                                             ?>
                                    </div>
                                    
                                </div>
                            </div>
                            </div>

                        


                    </form>

                </div>


            </div>

        </div>

    </div>
    <?php echo view('alpha::admin.media.modal.edit-image') ?>
    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->
</div>
<!-- /content area -->

{!! view('alpha::admin.entry.templates.response-handler',['errors'=>@$errors]) !!}
<script type="text/javascript">
    $(document).ready(function () {

    $( "#containerMedia" ).sortable({
      revert: true
    });
    function hideAllTitle(){
        @foreach(Config::get('alpha.application.locales') as $key  => $lang)
            $("#title_" + "{{ $key }}").hide();
        @endforeach
    }
    function hideAllEditors(){
        @foreach(Config::get('alpha.application.locales') as $key  => $lang)
            $("#content_" + "{{ $key }}").hide();
        @endforeach
    }
    function showDefaultEditor(){
        $("#content_" + defaultLang).show();
    }
    var defaultTitle = "{{ Config::get('alpha.application.default_locale') }}";
    var defaultLang = "{{ Config::get('alpha.application.default_locale') }}";
   
    
    hideAllEditors();
    showDefaultEditor();
    

    $("#changeLang").change(function(e){
        hideAllEditors();
        hideAllTitle();
        defaultLang = $("#changeLang option:selected").val();
        $("#title_" + defaultLang).show();
        showDefaultEditor();

    });

    $("#title_" + defaultLang).show();

    $("#title_" + defaultTitle).slug({
        slug: 'slug',
        hide:false
    });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
    })
</script>
