<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">{{ $info['plural'] }}</span> - All {{ $info['plural'] }}</h4>
        </div>
        <div class="heading-elements">
               
            
            
            <a href="{{ route('alpha_admin_entry_create',[$info['slug']]) }}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>New {{ $info['single'] }}</a>
        </div>
        

    </div>
</div>
<div class="content">

    

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h5 class="panel-title">All {{$info['plural']}}</h5>
                        
                    </div>
                   
                </div>

                <div class="panel-body">
                    
                   <p>Manage all available {{ strtolower($info['plural']) }}</p>
                    
                </div>
                
                   <div class="table-header datatable-header clearfix">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-5 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter:&nbsp;</label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                        
                       
                         

                        
                        <select class="table-select2  pull-left filter-table-column" placeholder="Filter by Status" searchin="5">
                            <option value="all">All Status</option>
                            <option value="published">Published</option>
                            <option value="disabled">Disabled</option>
                            <option value="draft">Draft</option>
                            <option value="scheduled_publish">Scheduled</option>
                        </select>
                        
                        
 
                    </div>
                    
                      
                    
                    <table class="table entry-table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th width="150px">Image Count</th>
                                <th width="140px">Created by</th>
                                <th width="140px" class="hide"><span>Published</span></th>
                                <th width="140px"><span>Created</span></th>
                                <th width="140px"><span>Last Modified</span></th>
                                <th width="125px">Status</th>
                                <th width="50px" class="no-sort text-right">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>

                    <form style="display:none" id="form_delete" action="{{route('alpha_admin_entry_delete',[0])}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="0" >
                    </form>

                    <script type="text/javascript">
                        var entryGetDataUrl = "{{ route('alpha_admin_entry_get_data',[$type]) }}";
                        $(document).on('click','.confirm',function(e){
                                e.preventDefault();
                                $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
                                $("#form_delete").find("[name=id]").val($(this).data('id'));
                                swal({
                                    title: "Delete ?",
                                    text: "You will not be able to recover deleted items, are you sure want to delete ?",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Yes, delete it!",
                                    closeOnConfirm: false },
                                    function(isConfirm){
                                    // console.log(isConfirm);
                                    if(isConfirm){
                                        $("#form_delete").submit();
                                    }
                                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                });
                            });
                    </script>
                    <script type="text/javascript" src="{{ asset('backend/assets/js/entry-slider.js') }}"></script>
                


            </div>

        </div>

    </div>



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>

<script type="text/javascript">
    $(document).ready(function(){
        var status = "{{session('msg')}}";
        if (status === 'Status Changed!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                },
                function (argument) {
                    swal.disableButtons();
                    swal.close();
                });
        }
    });
</script>
