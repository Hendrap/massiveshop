<form id="login-form">
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <div class="logo">
                                    <img src="{{ asset('backend/assets/images/alpha-logo-dark.png') }}" alt="logo">
                                </div>
                                
                                <div class="success-info">
                                <p>Password reset instructions has been sent to your email. Please make sure you check your spam folder too.</p>
                                </div>
                                
                                <a href="{{ route('alpha_get_login') }}" class="back-to-login">Back to Login</a>
                                
                            </div>

                           

                            
                        </div>
                    </form>