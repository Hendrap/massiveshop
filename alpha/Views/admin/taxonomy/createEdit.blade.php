<?php
    $id = 0;
    $name = old('name');
    $slug = old('slug');
    $desc = old('desc');
    $parent = old('parent');
    $seo_title = old('seo_title');
    $seo_keyword = old('seo_keyword');
    $seo_description = old('seo_description');
    if(!empty($taxonomy)){
        $id = $taxonomy->id;
        $name = old('name',$taxonomy->name);
        $slug = old('slug',$taxonomy->taxonomy_slug);
        $desc = old('desc',$taxonomy->description);
        $parent = old('parent',$taxonomy->parent);
        $seo_title = old('seo_title',$taxonomy->seo_title);
        $seo_keyword = old('seo_keyword',$taxonomy->seo_keyword);
        $seo_description = old('seo_description',$taxonomy->seo_description);
    }

 ?>
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Taxonomy</span> - {{ $info['single'] }}</h4>
        </div>

    </div>


</div>


<!-- /page header -->
   

<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>

                <li><a href="{{ route('alpha_admin_taxonomy_index',[$type]) }}">Taxonomy</a></li>
                <li class="active">{{$info['single']}}</li>

            </ul>
        </div>

    </div>


    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
        <span class="text-semibold">Required field(s) error or missing</span>
    </div>

    <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
        <span class="text-semibold">Wrong format found</span>
    </div>
    <form name="alphaForm" action="{{route('alpha_admin_taxonomy_save',[$type,$id])}}" method="POST" class="form-horizontal">

        <div class="row">

            <div class="col-sm-12">

                <div class="panel panel-flat">

                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">
                            @if($id == 0)
                                Main Content
                            @else
                                {{ $taxonomy->name }}
                            @endif
                            </h5>

                        </div>


                    </div>
                    <div class="panel-body">
                        @if($id != 0)
                        <p class="content-group-lg">Created on {{date_format(date_create($taxonomy->created_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($taxonomy->created_at),app('AlphaSetting')->getSetting('time_format'))}} by {{ generateName($taxonomy->user) }}, last modified on {{date_format(date_create($taxonomy->updated_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($taxonomy->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</p>
                        @endif
                    @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        @if(Illuminate\Support\Str::contains($error,'required'))
                            <span class="text-semibold">Required field(s) error or missing</span>
                        @else
                            <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                        @endif
                    </div>
                    @endif
                    @endforeach
                    @endif

                    <fieldset>
                       <div class="form-group">
                            <label class="control-label col-lg-2">Category Name</label>
                            @if($id == 0)
                            <div class="col-lg-10">
                                <input type="text" id="name" class="form-control" name="name" value="{{$name}}">
                                <div class="post-url pull-left"><?php echo url('category/'.$type) ?>/<span class="slug">{{$slug}}</span>
                                    <input type="text" style="display:none" name="slug" class="slug form-contro" value="{{$slug}}">
                                </div>&nbsp;&nbsp;&nbsp;
                                <div class="display-inline-block mt-10 post-url-buttons">
                                    <a href="#" class="edit-url" style="display: inline;">Edit</a>
                                    <a href="javascript:void(0)" class="save-url" style="display:none"><i class="icon-checkmark4" style="font-size:10px;"></i></a>
                                    <span class="separator-url-form" style="display:none;opacity:0;">|</span>
                                    <a href="javascript:void(0)" class="cancel-edit-url" style="display:none"><i class="icon-cross" style="font-size:15px;"></i></a>
                                </div>
                            </div>
                            @else
                            <div class="col-lg-10">
                                <input type="text" id="name" class="form-control" name="name" value="{{$name}}">
                                
                                <div class="post-url pull-left"><?php echo url('category/'.$type) ?>/<span class="slug">{{ $slug }}</span>
                                    <input type="text" style="display:none" name="slug" class="slug form-control" value="{{ $slug }}">
                                </div>&nbsp;&nbsp;&nbsp;
                                <div class="display-inline-block mt-10 post-url-buttons">
                                    <a href="javascript:void(0)" class="edit-url-custom">Edit</a>
                                    <a href="javascript:void(0)" class="save-url-custom" style="display:none"><i class="icon-checkmark4" style="font-size:10px;"></i></a>
                                    <span class="separator-url-form" style="display:none;opacity:0;">|</span>
                                    <a href="javascript:void(0)" class="cancel-edit-url-custom" style="display:none"><i class="icon-cross" style="font-size:15px;"></i></a>
                                </div>

                            </div>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            
                            <label class="control-label col-lg-2">Parent</label>
                            <div class="col-lg-10">
                                    <select class="form-control" name="parent" style="width:200px;">
                                            <option value="0">None</option>
                                        @foreach($taxonomies as $taxo)
                                            <option <?php echo ($taxo->id == $parent) ? 'selected' : '' ?> value="{{ $taxo->id }}">{{ $taxo->name }}</option>
                                            @foreach($taxo->childs as $v)
                                                <option <?php echo ($v->id == $parent) ? 'selected' : '' ?> value="{{ $v->id }}">&mdash; {{ $v->name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                <div>
                                <div style="margin-top: 15px;">
                                    <p class="text-muted no-margin" style="font-size:11px;line-height:1em;">Choose one of the main category from the list.<br>Only if you are creating a sub-category. If not, just leave it.</p>
                                </div>
                                </div>
                            </div>
                            
                        </div>
                         <div class="form-group">
                            <label class="control-label col-lg-2">Description</label>
                            <div class="col-lg-10">
                                  <textarea name="desc" class="form-control" style="height:80px;">{{$desc}}</textarea>
                            </div>
                        </div>
                    </fieldset>

                   


                    
                    <fieldset>
                        
                        <legend class="text-semibold">
                            Search Engine Optimization
                        </legend>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="seo_title" value="{{$seo_title}}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Keyword</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="seo_keyword" value="{{$seo_keyword}}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Description</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="seo_description" style="height:80px;">{{$seo_description}}</textarea>
                            </div>
                        </div>
                    
                    </fieldset>
                        
                        <div class="text-right">
                            
                            <a href="{{route('alpha_admin_taxonomy_index',[$type])}}" class="btn btn-primary bg-slate">Cancel</a>
                            
                            <button type="submit" class="btn btn-primary ml-10" >Save</button>
                            
                        </div>
                    </div>
                    
                </div>

               {{csrf_field()}}

            </div>


        </div>

    </form>
    <?php $err = count($errors); ?>
<script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        @if($id == 0)
        $('[name=name]').slug({
            slug: 'slug',
            hide:false
        });
        @endif
        CKEDITOR.replace("desc");
    });

    @if($id > 0)
            var currentSlug = "<?php echo $slug ?>";
             $('.edit-url-custom').click(function(e){
                e.preventDefault;
                $('.post-url span').hide();
                $('.post-url input').show();
                $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').show();
                $('.edit-url-custom').hide();
            });
            
            
            $('.save-url-custom').click(function(e){
                e.preventDefault;
                newurl = $('.post-url input').val();
                currentSlug = newurl;
                $.ajax({
                    url:"<?php echo route("alpha_admin_taxo_change_slug") ?>",
                    data:{slug:currentSlug,id:"<?php echo $id ?>"},
                    type:"POST",
                    dataType:"json",
                    success:function(data){
                        $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').hide();
                        $('.post-url span').show();
                        $('.post-url input').hide();
                        $('.edit-url-custom').show();
                        currentSlug = data.slug;
                        $('.post-url span').html('').html(currentSlug);
                        $('.post-url input').val(currentSlug);;
                    }
                });
                
            });
            
            $('.cancel-edit-url-custom').click(function(e){
                e.preventDefault;
                $('.cancel-edit-url-custom, .save-url-custom, .separator-url-form').hide();
                $('.post-url span').show();
                $('.post-url input').hide();
                $('.edit-url-custom').show()
                $('.post-url input').val(currentSlug);;
                
            });

    @endif
</script>
    
    



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->

{!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'name' => 'required',
                ]

            ]); 
        !!}
    
    
    <script>

        $(document).ready(function() {

            
           $('select').each(function() {

                select_placeholder = $(this).attr('placeholder');
                $(this).select2({
                        placeholder: select_placeholder,
                        width: '200px',

                });

            });
            
            autosize($('textarea'));
            
            
        });

    </script>


    <script type="text/javascript">
        var status = "{{session('msg')}}";
        var err = "{{$err}}";
        
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success"
                });
        }
    </script>



            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

<script type="text/javascript">
    $(document).ready(function(){
        initPreventClose();
    })
</script>
