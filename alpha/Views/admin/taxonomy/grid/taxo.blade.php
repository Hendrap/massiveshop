                                <td class="title-column">
                                     <h6>
                                         @if($taxo->parent > 0)
                                            @if($isSearch == false)
                                            &mdash;
                                                @if(isset($lastChild))
                                                    &mdash;
                                                @endif
                                            @endif
                                        @endif
                                        <a href="{{route('alpha_admin_taxonomy_edit',[$type,$taxo->id])}}">
                                        {{$taxo->name}}
                                        </a>
                                        <?php
                                        if(!empty($taxo->parents)){
                                         ?>
                                          <i class="text-info">Parent : {{$taxo->parents->name}}</i>
                                        <?php } ?>
                                     </h6>
                                </td>
                                |alpha--datatable-separator--|<td>{{ucfirst(@$taxo->user->username)}}</td>
                            
                                |alpha--datatable-separator--|<td class="td-date">
                                            {{date_format(date_create($taxo->created_at),app('AlphaSetting')->getSetting('date_format'))}}
                                            <br><span class="text-muted">{{date_format(date_create($taxo->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                           {{date_format(date_create($taxo->updated_at),app('AlphaSetting')->getSetting('date_format'))}}
                                            <br><span class="text-muted">{{date_format(date_create($taxo->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                    
                                </td>
                                |alpha--datatable-separator--|<td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{route('alpha_admin_taxonomy_edit',[$type,$taxo->id])}}"><i class="icon-pencil5"></i> Edit</a></li>
                                            
                                            <li class="divider"></li>
                                            
                                            <li><a class="confirm" data-id="{{$taxo->id}}"  href="{{route('alpha_admin_taxonomy_delete',[$taxo->id])}}"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                               
