

<!-- Page header -->

<div class="page-header page-header-default">

    <div class="page-header-content">

        <div class="page-title">

            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Taxonomy</span> - {{ $info['plural'] }}</h4>

        </div>



        <div class="heading-elements">

               

            <a href="{{ route('alpha_admin_taxonomy_create',[$type]) }}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>New Category</a>

        </div>

        

    </div>





</div>





<!-- /page header -->



 @if(session('msg'))

    <div class="alert bg-success alert-styled-left">

        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>

        <span class="text-semibold">{{session('msg')}}</span>

    </div>

@endif

<!-- Content area -->

<div class="content">



    <div class="panel panel-flat">



        <div class="breadcrumb-line">

            <ul class="breadcrumb">

                <li><a href="{{url('admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>

           

                <li><a href="{{ route('alpha_admin_taxonomy_index',[$type]) }}">Taxonomy</a></li>

                <li class="active">{{ $info['plural'] }}</li>



            </ul>

        </div>



    </div>



    <div class="row">



        <div class="col-xs-12">



            <div class="panel panel-flat">



                <div class="panel-heading">

                    <div class="panel-heading-title">

                        <h5 class="panel-title">{{ $info['plural'] }}</h5>

                        

                    </div>



                   

                </div>

                <div class="panel-body">

                    <p>Manage all available {{ strtolower($info['plural']) }}.</p>

                </div>

                

                    <div class="table-header datatable-header clearfix">

                    

                        <div class="filter-table clearfix pull-left mr-5">

                            <div class="form-group pull-left  mr-5 no-margin-bottom">

                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter:&nbsp;</label>

                            </div>

                            <div class="form-group pull-left has-feedback  no-margin-bottom">

                                <input type="text" class="form-control" id="search_the_table" style="height:36px">

                                <div class="form-control-feedback" style="line-height:36px;">

                                        <i class="icon-search4" style="font-size:12px"></i>

                                </div>

                            </div>

                            

                        </div>

                        

                        

                        

 

                    </div>

                    

                    

                    <table class="table entry-table table-striped">

                        <thead>

                            <tr>

                                <th>Name</th>

                                <th width="140px">Creator</th>

                                <th width="140px"><span>Created</span></th>

                                <th width="140px"><span>Modified</span></th>

                                <th width="50px" class="no-sort text-right"></th>

                                

                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                    <form style="display:none" id="form_delete" action="{{route('alpha_admin_taxonomy_delete',[0])}}" method="POST">

    {{csrf_field()}}

    <input type="hidden" name="id" value="0" >

</form>



<script type="text/javascript">

        $(document).on('click',".confirm",function(e){

            e.preventDefault();

                $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));

                $("#form_delete").find("[name=id]").val($(this).data('id'));

                

                //console.log($(this).data('id'));

            swal({
                title: "Delete ?",
                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false },
                function(isConfirm){
                if(isConfirm){
                    $("#form_delete").submit();
                }

            });

        });

</script>

                    <script>

                        var urlGetData = "{{ route('alpha_admin_taxonomy_get_data',[$type]) }}";

                    </script>

                    <script type="text/javascript" src="{{ asset('backend/assets/js/taxonomy.js') }}"></script>

                





            </div>



        </div>



    </div>







    <!-- Footer -->

    {!! view('alpha::admin.partials.content-footer') !!}

    <!-- /footer -->



</div>

<!-- /content area -->

