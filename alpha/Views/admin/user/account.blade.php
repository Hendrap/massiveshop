<?php 
	$email = old('email');
	$username = old('username');
	$userStatus = old('status');
	$userRole = old('role');
	$oldUserMeta = old('metas');
	$userMetas = array();
	if(!empty($user)){
		$userMetas = $user->metas;
		$email = old('email',$user->email);
		$username = old('username',$user->username);
		$userStatus = old('status',$user->status);
		$userRole = old('role',@$user->roles[0]->id);
		$oldUserMeta = old('metas',$userMetas);
	}else{
		$user = new StdClass();
		$user->id = 0;
	}


?>
<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title"><?php echo ($user->id == 0) ? 'Add a new' : 'Edit' ?> Account</h5>
					</div>
	<div class="panel-body">
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<form class="form-horizontal" action="{{route('alpha_admin_savemyaccount')}}" method="POST">
				<fieldset class="content-group">
					
				
				<h2>Main Info</h2>
				<div class="form-group">
					<label class="control-label col-lg-2">Email</label>
					<div class="col-lg-10">
						<input name="email" type="text" class="form-control" value="{{$email}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Username</label>
					<div class="col-lg-10">
						<input name="username" type="text" class="form-control" value="{{$username}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Password</label>
					<div class="col-lg-10">
						<input name="password" type="password" class="form-control" value="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Retype Password</label>
					<div class="col-lg-10">
						<input name="password_confirmation" type="password" class="form-control" value="">
					</div>
				</div>
				

				
				
				
				{{csrf_field()}}
				
				
					<input type="hidden" name="id" value="{{@$user->id}}">
					<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
			</div>
					</fieldset>
				</form>
			</div>
		</div>
