
<?php
    $email = old('email');
    $userStatus = old('status');
    $userRole = old('role',8);
    $oldUserMeta = old('metas');
    $userMetas = array();
    $avatar = asset('backend/assets/images/avatar-default.jpg');
    if(!empty($user)){
        $email = old('email',$user->email);
        $userStatus = old('status',$user->status);
        $userRole = old('role',@$user->roles[0]->id);
        $oldUserMeta = old('metas',$userMetas);
        if (empty($oldUserMeta) && isset($dataMetas)) {
            $oldUserMeta = $dataMetas;
            if (!empty($dataMetas['avatar'])) {
                $avatar = asset(getCropImage($dataMetas['avatar'],'default'));
            }
        }
    }else{
        $user = new StdClass();
        $user->id = 0;
    }


?>
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Accounts</span> - {{ ($user->id == 0) ? 'New' : 'Edit' }} Accounts</h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('alpha_admin_all_account') }}">Accounts</a></li>
                <li class="active">{{ ($user->id == 0) ? 'New' : 'Edit' }} Account</li>
            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading" >
                    <div class="panel-heading-title">
                        @if($user->id == 0)
                        <h5 class="panel-title">{{ ($user->id == 0) ? 'New' : 'Edit' }} Account</h5>
                        @else
                        <h5 class="panel-title">{{ generateName($user) }}</h5>
                        @endif  
                    </div>


                </div>
                <div class="panel-body">
                    @if($user->id != 0)
                    <p class="content-group-lg">Created on {{date_format(date_create($user->created_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($user->created_at),app('AlphaSetting')->getSetting('time_format'))}} by {{ generateName($user->modifiedBy) }}, last modified on {{date_format(date_create($user->updated_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($user->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</p>
                    @endif
                    
                    @if(count($errors) > 0)
                    @foreach($errors->all() as $key => $error)
                    @if($key == 0)
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        @if(Illuminate\Support\Str::contains($error,'required'))
                            <span class="text-semibold">Required field(s) error or missing</span>
                        @else
                            <span class="text-semibold">{{ str_replace('metas.','',$error) }}</span>
                        @endif
                    </div>
                    @endif
                    @endforeach
                    @endif
                    <div class="new-account-main">

                        
                         <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Required field(s) error or missing</span>
                         </div>

                         <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                             <span class="text-semibold">Wrong format found</span>
                         </div>
                        <form name="alphaForm" enctype="multipart/form-data" class="form-account" action="{{route('admin.accounts.store')}}" method="POST">
                        {{ csrf_field() }}
                            <fieldset>
                            
                                <legend class="text-semibold">
                                    User Profile
                                </legend>
                            
                            
                            
                            <div class="form-group">


                                <div class="clearfix">
                                    <label for="upload_avatar" class="change-avatar">
                                    <img onerror="this.src='{{ asset('backend/assets/images/avatar-resized.jpg') }}'" src="{{$avatar}}" id="image-preview" alt="avatar" class="user-avatar display-inline-block valign-bottom">
                                        <div class="overlay"><span class="btn btn-rounded btn-primary"><i class="icon-camera"></i></span></div>
                                    </label>
                                    
                                     <input name="avatar" type="file" id="upload_avatar" class="hide">
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-sm-6">

                                    <div class="form-group">

                                        <label>Name</label>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input id="metas_first_name" type="text" class="form-control" placeholder="First Name" value="{{@$oldUserMeta['first_name']}}" name="metas[first_name]">
                                            </div>
                                            <div class="col-sm-6">
                                                <input id="metas_last_name" type="text" class="form-control" placeholder="Last Name" name="metas[last_name]" value="{{@$oldUserMeta['last_name']}}">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label>Email</label>


                                        <input id="email" type="text" class="form-control" placeholder="mail@domain.com" name="email" value="{{$email}}">

                                    </div>
                                    <div class="form-group">

                                        <label>Password</label>

                                        <div class="row password-form">
                                            <div class="col-sm-6">
                                                <input id="password" type="password" class="form-control password" placeholder="Type Password" name="password">
                                            </div>
                                            <div class="col-sm-6">
                                                <input id="password_confirmation" type="password" class="form-control pas repassword" placeholder="Re-Type Password" name="password_confirmation">
                                            </div>
                                        </div>
                                        <div class="row password-generated hide">
                                            <div class="col-sm-12">
                                                <p class="no-margin text-italic">Password will be automatically generated and the user will be notified by email.</p>
                                            </div>
                                        </div>
                                        <div class="checkbox generate-password">
                                            <label>
                                                    <input type="checkbox" class="styled checkbox-password generate-password">
                                                    Generate Password
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                        <div class="row" style="width:540px;max-width:100%;">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Role</label>
                                                    <br>
                                                    <select name="role" width="210" placeholder="Select Role">
                                                        @foreach($roles as $role)
                                                            <option <?php echo ($role->id == $userRole) ? 'selected' : '' ?>  value="{{$role->id}}">{{$role->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <br>

                                                    <div class="clearfix">
                                                        <select {{ $user->id == 0  ? "disabled" : "" }} name="status" placeholder="Day" width="110">

                                                            <?php
                                                                $status = ['pending','active','disabled'];
                                                                if($user->id != 0) $status[] = 'deleted';
                                                             ?>
                                                            @foreach($status as $v)
                                                                <option <?php echo ($v == $userStatus) ? 'selected' : '' ?> value="{{$v}}">{{ucfirst($v)}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($user->id == 0)
                                                            <input type="hidden" name="status" value="pending">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                    </div>

                                    <div class="form-group">

                                        <label>Date of Birth</label>
                                        <br>

                                        <div class="clearfix">
                                            <select id="metas_day" name="metas[day]" placeholder="Day" width="110">
                                                    <option value="">Date</option>
                                                @for($i=1; $i<=31; $i++)
                                                	<option <?php echo ($i == @$oldUserMeta['day']) ? 'selected' : '' ?>  value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>

                                            <select id="metas_month" name="metas[month]" placeholder="Month" width="120">
                                             <option value="">Month</option>
                                            	@for($i=1; $i<=12; $i++)
                                                	<option <?php echo ($i == @$oldUserMeta['month']) ? 'selected' : '' ?>  value="{{$i}}">{{date('F',strtotime('2000-'.$i.'-01'))}}</option>
                                                @endfor
                                            </select>

                                            <select id="metas_year" name="metas[year]" placeholder="Year" width="90">
                                                <option value="">Year</option>
                                                @for($i=1980; $i<=date('Y'); $i++)
                                                	<option <?php echo ($i == @$oldUserMeta['year']) ? 'selected' : '' ?>  value="{{$i}}">{{$i}}</option>
                                                @endfor

                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label>Gender</label>
                                        <br>

                                        <div class="clearfix">

                                            <label class="radio-inline">
                                                <input type="radio" name="metas[gender]" value="Male" class="styled control-primary" <?php if(@$oldUserMeta['gender'] == 'Male'){echo 'checked="checked"';}?>> Male
                                            </label>

                                            <label class="radio-inline">
                                                <input type="radio" name="metas[gender]" value="Female" class="styled control-primary" <?php if(@$oldUserMeta['gender'] == 'Female'){echo 'checked="checked"';}?>> Female
                                            </label>

                                        </div>

                                    </div>



                                </div>

                            </div>

                            </fieldset>
                            
                            
                            <div class="text-right">
                            
                            <input type="hidden" name="id" value="{{@$user->id}}">
                            
                            <a href="{{route('admin.accounts.index')}}" class="btn btn-primary bg-slate">Cancel</a>
                            <button type="submit" class="btn btn-primary ml-10" >Save</button>
                            
                            
                            </div>
                            
                            
                        </form>


                    </div>


                </div>


                

            </div>

        </div>

    </div>
    
    <?php
    $err = count($errors);
    ?>
   

   {!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'metas[first_name]' => 'required',
                    'metas[last_name]' => 'required',
                    'email' => 'required|valid_email',
                    'password_confirmation'=>'matches[password]'
                ]

        ]); 
   !!}


    <script>
        $(document).ready(function() {
            initPreventClose();
            var status = "{{session('msg')}}";
            var err = "{{$err}}";
            if (status === 'Data Saved!') {
                swal({
                    title: "SUCCESS",
                    text: "All changes has been saved successfuly",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                    });
            }

            //Email has been taken before !

            $(".control-primary").uniform({
                radioClass: 'choice',
                wrapperClass: 'border-primary-600 text-primary-800'
            });
            
            $('select').each(function() {

                select_placeholder = $(this).attr('placeholder');
                select_width = $(this).attr('width');
                $(this).select2({
                    minimumResultsForSearch: Infinity,
                    placeholder: select_placeholder,
                    width: select_width,

                });

            });

            $(".checkbox-password").change(function() {
                if(this.checked) {
                    $('.password-form').addClass('hide');
                    $('.password-generated').removeClass('hide');
                    
                }
                else{
                     $('.password-form').removeClass('hide');
                     $('.password-generated').addClass('hide');
                }
            });
        });

        $('.generate-password').click(function () {
        	var pass = wpiGenerateRandomNumber(10);
        	$('.password').val(pass);
        	$('.repassword').val(pass);
        });
        function wpiGenerateRandomNumber(length) {

        var i = 0;
        var numkey = "";
        var randomNumber;

        while( i < length) {

            randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;

            if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
            if ((randomNumber >=58) && (randomNumber <=90)) { continue; }
            if ((randomNumber >=91) && (randomNumber <=122)) { continue; }
            if ((randomNumber >=123) && (randomNumber <=126)) { continue; }

            i++;
            numkey += String.fromCharCode(randomNumber);

        }

        return numkey;

    }

    </script>

    <script type="text/javascript">
        function resizeImage(base64, maxWidth, maxHeight) {
          // Max size for thumbnail
          if(typeof(maxWidth) === 'undefined') var maxWidth = 500;
          if(typeof(maxHeight) === 'undefined') var maxHeight = 500;

          // Create and initialize two canvas
          var canvas = document.createElement("canvas");
          var ctx = canvas.getContext("2d");
          var canvasCopy = document.createElement("canvas");
          var copyContext = canvasCopy.getContext("2d");

          // Create original image
          var img = new Image();
          img.src = base64;

          // Determine new ratio based on max size
          var ratio = 1;
          if(img.width > maxWidth)
            ratio = maxWidth / img.width;
          else if(img.height > maxHeight)
            ratio = maxHeight / img.height;

          // Draw original image in second canvas
          canvasCopy.width = img.width;
          canvasCopy.height = img.height;
          copyContext.drawImage(img, 0, 0);

          // Copy and resize second canvas to first canvas
          canvas.width = img.width * ratio;
          canvas.height = img.height * ratio;
          ctx.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvas.width, canvas.height);

          return canvas.toDataURL();

        }

    	function imagePreviewListener(source,target){
          document.getElementById(source).onchange = function () {
          var reader = new FileReader();
          reader.onload = function(e) {
             var thumb = resizeImage(e.target.result,200,200);
             document.getElementById(target).src = thumb;
          }
          
          reader.readAsDataURL(this.files[0]);
         }
        }
        imagePreviewListener('upload_avatar','image-preview');
    </script>
    <!-- Footer -->
   {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->
</div>
