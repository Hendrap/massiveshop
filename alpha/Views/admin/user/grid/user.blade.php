                                    <td class="title-column">
                                        <a href="{{ route('alpha_admin_edit_account',[$user->id]) }}">
                                        <h6>{{ generateName($user) }}</h6>
                                        </a>
                                        <span class="text-muted">{{$user->email}}</span></td>
                                    |alpha--datatable-separator--|<td>{{@$user->roles[0]->name}}</td>
                                    |alpha--datatable-separator--|<td class="td-date">
                                       
                                            {{ date(app('AlphaSetting')->getSetting('date_format'),strtotime($user->created_at)) }}
                                            <br><span class="text-muted">{{ date(app('AlphaSetting')->getSetting('time_format'),strtotime($user->created_at)) }}</span>
                                       
                                    </td>
                                    |alpha--datatable-separator--|<td class="td-date">
                                            @if($user->last_login == '1970-01-01 00:00:00' || empty($user->last_login))
                                                Never
                                            @else
                                                {{ date(app('AlphaSetting')->getSetting('date_format'),strtotime($user->last_login)) }}
                                                 <br><span class="text-muted">{{ date(app('AlphaSetting')->getSetting('time_format'),strtotime($user->last_login)) }}</span>
                                            @endif
                                           
                                        
                                    </td>
                                    |alpha--datatable-separator--|<td>
                                    <?php
                                    $className ='default';
                                    switch ($user->status) {
                                        case 'active':
                                            $className = 'success';
                                            break;
                                        case 'disabled':
                                            $className = 'danger';
                                            break;
                                        case 'pending':
                                            $className = 'orange';
                                            break;
                                        
                                        
                                    }
                                     ?>
                                    <span class="label bg-{{ $className }}">{{ ucfirst($user->status) }}</span>

                                    </td>


                                    |alpha--datatable-separator--|<td class="table-actions">
                                            <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-icon dropdown-toggle" type="button" aria-expanded="false">
                                                        <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <a href="{{ route('alpha_admin_edit_account',[$user->id]) }}">
                                                            <i class="icon-pencil5"></i> Edit</a>
                                                        </li>

                                                        <?php if($user->status != 'pending'){ ?>
                                                        <li>
                                                            <a href="{{ route('alpha_admin_user_change_status',[$user->id,'pending']) }}">
                                                            <i class=" icon-undo"></i> Pending</a>
                                                        </li>
                                                        <?php } ?>


                                                        <?php if($user->status != 'disabled'){ ?>
                                                        <li>
                                                            <a href="{{ route('alpha_admin_user_change_status',[$user->id,'disabled']) }}">
                                                            <i class="icon-cancel-square"></i> Disable</a>
                                                        </li>
                                                        <?php } ?>

                                                        <?php if($user->status != 'active'){ ?>
                                                        <li>
                                                            <a href="{{ route('alpha_admin_user_change_status',[$user->id,'active']) }}">
                                                            <i class=" icon-checkmark4"></i> Enable</a>
                                                        </li>
                                                        <?php } ?>
                                                        
                                                        <li class="divider"></li>
                                                        
                                                        <li>
                                                            <a class="confirm" data-id="{{$user->id}}"  href="{{ route('admin.accounts.destroy',[$user->id]) }}?_method=delete">
                                                            <i class=" icon-trash"></i> Delete</a>
                                                        </li>
                                                                                            
                                                    </ul>
                                        </div>
                                 </td>
