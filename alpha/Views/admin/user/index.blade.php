<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Accounts</span> - All Accounts</h4>
        </div>
        <div class="heading-elements">
            
            <a href="{{route('alpha_admin_new_account')}}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>New Account</a>
            
        </div>
    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h1 class="text-semibold">User Accounts</h1>
                        
                    </div>

                   
                </div>
                <div class="panel-body">
                    
                    <p>Manage user accounts and access to use the website. Only administrators are allowed to access the admin panel.</p>
                    
                </div>
                    <div class="table-header datatable-header clearfix">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-5 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter:&nbsp;</label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                        
                        <select class="table-select2 pull-left filter-table-column" placeholder="Filter By Role">
                            <option data-col="1" value="0">All Roles</option>
                            @foreach($roles as $role)
                            <option data-col="1" value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>


                        <select class="table-select2 pull-left filter-table-column" placeholder="Filter By Status">
                            <option data-col="4" value="0">All Status</option>
                            <option data-col="4" value="active">Active</option>
                            <option data-col="4" value="pending">Pending</option>
                            <option data-col="4" value="disabled">Disabled</option>
                        </select>
                        
                        
                        
                        
                    </div>
                    
                    <table class="table entry-table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                
                                <th width="140px">Role</th>
                                <th width="140px"><span>Created</span></th>
                                <th width="140px"><span>Last Login</span></th>
                                <th width="120px">Status</th>
                                <th width="50px" class="no-sort text-right"></th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>

                    <script>
                        var userGetData = "{{route('alpha_admin_user_get_data')}}";
                        var entry_table = {};
                        $(document).ready(function() {


                            
                              entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                         $('.dataTable, .dataTable tbody').css('position','static');
                                            if(typeof entry_table.ajax != 'undefined')
                                            {
                                                $(e.target).find('tbody').html('');
                                            }

                                            $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                        $.unblockUI();
                                        $('.dataTable, .dataTable tbody').css('position','relative');
                                        checktablestate(entry_table);
                                        $(".blockUI").remove();
                                     }
                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "order": [],
                                "searching" : true,
                                "lengthChange": false,
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "serverSide": true,
                                "pageLength": 50,
                                "ajax": {
                                    "url": userGetData,
                                    "type": "POST"
                                },
                                "columnDefs": [{
                                    "targets": 'no-sort',
                                    "orderable": false,
                                }],
                                 "columns": [
                                    { "data": "username", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:"text-bold"
                                    },


                                    { "data": "role", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    }

                                    },

                                    { "data": "created_at", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    },
                                    sClass:"td-date"

                                    },


                                    { "data": "last_login", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:"td-date"

                                    },
                                    { "data": "status", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[4];
                                    }

                                    },
                                     { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[5];
                                    },
                                    sClass:"table-actions"

                                    }

                                ],
                            });
                           
                            var filterby;
                            
                            $('.filter-table-column').change(function(){
                                var col = "";
                                var val = "";
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    val = $(this).val();
                                    col = $(this).attr('data-col');
                                });
                                
                                entry_table.columns(col).search(val).draw();
                                
                                
                            });
                            
                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13)
                                {
                                 entry_table.search($(this).val()).draw();
                                }
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                             $('.table-select2').each(function() {

                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',

                                });

                            });
                        });

                    </script>
                    <form style="display:none" id="form_delete" action="{{route('admin.accounts.destroy',[0])}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="0" >
                        {{ method_field('DELETE') }}
                    </form>

                    <script type="text/javascript">
                            $(document).ready(function(){
                                var status = "{{session('msg')}}";
                                if (status === 'Status Changed!') {
                                    swal({
                                        title: "SUCCESS",
                                        text: "All changes has been saved successfuly",
                                        confirmButtonColor: "#66BB6A",
                                        type: "success"
                                        });
                                }
                            });
                            $(document).on('click',".confirm",function(e){
                                e.preventDefault();
                                $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
                                $("#form_delete").find("[name=id]").val($(this).data('id'));
                                
                                 swal({
                                title: "Delete ?",
                                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                closeOnConfirm: false },
                                function(isConfirm){
                                    // console.log(isConfirm);
                                    if(isConfirm){
                                        $("#form_delete").submit();
                                    }
                                });
                            });
                            
                        
                        
                    </script>

                


            </div>

        </div>

    </div>



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
