<?php 

namespace App;

use Illuminate\Http\Request;
use CatalogOrder;


class CustomShippingPromo
{
	public function doAction($shippingCost = 0,$subTotal = 0,$shippingAddress,$items = array())
	{
		$items = $this->parseItems($items);
	
		$methods = ['minimumTotal','itemsInCart'];
		foreach ($methods as $key => $value) {
			$shippingCost = $this->{$value}($shippingCost,$subTotal,$shippingAddress,$items);
		}

		return $shippingCost;
	}

	public function minimumTotal($shippingCost = 0,$subTotal = 0,$shippingAddress,$itemsIds = array())
	{
		$isActive = app('AlphaSetting')->getSetting('is_enable_free_shipping');
		if($isActive == 'yes')
		{
			if(in_array($shippingAddress->countryCode, config('alpha.promo.free_shipping.countryCode')))
			{
				if($subTotal >= app('AlphaSetting')->getSetting('free_shipping_min_order'))
				{
					return 0.00;					
				}
			}
		}

		return $shippingCost;


	}

	public function itemsInCart($shippingCost = 0,$subTotal = 0,$shippingAddress,$itemsIds = array())
	{
		$disountItemIds = $this->getFreeShippingProducts();
		$diff = array_diff($disountItemIds,$itemsIds);

		if(count($diff) < count($disountItemIds))
		{
			if(in_array($shippingAddress->countryCode, config('alpha.promo.free_shipping.countryCode')))
			{
				return 0.00;
			}
		}

		return $shippingCost;
	}

	public function parseItems($items)
	{
		$itemIds = [];
		$items = json_decode(json_encode($items));
		foreach ($items as $key => $value) {
			$itemIds[] = $value->item_id;
		}

		return $itemIds;
	}

	public function getFreeShippingProducts()
	{
		$products = json_decode(app('AlphaSetting')->getSetting('free_shipping_products'));
		$itemIds = [];
		foreach ($products as $key => $value) {
			$itemIds[] = $value->item_id;
		}

		return $itemIds;
	}
}