<?php

namespace App;

use Illuminate\Http\Request;
use DHL\Entity\AM\GetQuote;
use DHL\Entity\GB\ShipmentRequest;
use DHL\Datatype\AM\PieceType;
use DHL\Datatype\GB\Piece;
use DHL\Entity\GB\BookPURequest;
use DHL\Entity\GB\CancelPURequest;
use DHL\Client\Web as DHLWebClient;
use DB;
use User;
use CatalogOrder;


class DHL
{
	public $client;
	public $env;

	public function __construct($env = 'production')
	{
		$this->env = $env;
		$this->client = new DHLWebClient($env);
	}

	public function parseGetQuoteResponse($res,$subTotal,$to,$items)
	{

		if(!empty($res->GetQuoteResponse->Note))
		{
			return ['status'=>0,'msg'=>(string)$res->GetQuoteResponse->Note->Condition->ConditionData];
		}

		$shippingMethods = [];
		if(!empty($res->GetQuoteResponse->BkgDetails->QtdShp))
		{
			foreach ($res->GetQuoteResponse->BkgDetails->QtdShp as $key => $value) {
				if(!empty($value->ShippingCharge) && $value->ShippingCharge > 0)
				{
					if(in_array($value->GlobalProductCode, config('alpha.application.dhl.allowedProducts')))
					{
						$shippingCost = app('App\CustomShippingPromo')->doAction((int)$value->ShippingCharge,$subTotal,$to,$items);
						$shippingMethods[] = (object)[
							'id' => (string)$value->GlobalProductCode,
							'GlobalProductCode' => (string)$value->GlobalProductCode,
							'LocalProductCode' => (string)$value->LocalProductCode,
							'ProductShortName' => (string)$value->ProductShortName,
							'LocalProductName' => (string)$value->LocalProductName,
							'ShippingCharge' => convertCurrency((int)$shippingCost),
							'label' => (string)$value->ProductShortName.' - '.getCurrencySymbol().' '.formatMoney(convertCurrency((int)$shippingCost))
						];						
					}
				}
			}			
		}


		if(count($shippingMethods) == 0) return ['status'=>0,'msg'=>'Shipping Method is not available.'];

		return ['status'=>1,'shippingMethods'=>$shippingMethods];
	}

/*

	sample param buat getQuote

	$from = [
		'countryCode' => app('AlphaSetting')->getSetting('country_code'),
		'postalCode' => app('AlphaSetting')->getSetting('postal_code'),
		'city' =>  app('AlphaSetting')->getSetting('city')
	];

	$to = [
		'countryCode' => 'ID',
		'postalCode' => '62362',
		'city' =>  'Tuban'
	];

	$items = [];
	
	$items[] = [
		'height' => 2,
		'depth' => 2,
		'width' => 2,
		'weight' => 2
	];

*/

	public function getQuote($from = array(),$to = array(),$items = array(),$subTotal = 1)
	{
		//dpr($to);
		$req = new GetQuote();

        $req->SiteID = config('alpha.application.dhl.siteId');
        $req->Password = config('alpha.application.dhl.password');

		$req->MessageTime = date('c');
        $req->MessageReference = date('Ymdhisusiu');
        $req->BkgDetails->Date = date('Y-m-d');

        if(!empty($items))
        {
	        foreach ($items as $key => $value) {

	        	$piece = new PieceType();
                $piece->PieceID = ($key + 1);
                $piece->Height = $value['height'];
                $piece->Depth = $value['depth'];
                $piece->Width = $value['width'];
                $piece->Weight = $value['weight'];

				$req->BkgDetails->addPiece($piece);
	        }
        }

        if($from['countryCode'] == $to['countryCode'])
        {
	        $req->BkgDetails->IsDutiable = 'N';        	
        }else{
        	
        	// $dhlCountry = DB::table('dhl_countries')->where('country_code','=',$to['countryCode'])->first();

        	$req->BkgDetails->IsDutiable = 'Y';
    //     	$req->Dutiable->DeclaredValue = '0.00';
    //     	if(!empty($dhlCountry))
    //     	{
				// $req->Dutiable->DeclaredCurrency = $dhlCountry->currency_code;        	        		
         }



//        }
        
       	$req->Dutiable->DeclaredValue = $subTotal;
       	$req->Dutiable->DeclaredCurrency = 'IDR';

        $req->BkgDetails->ReadyTime = 'PT'.date("H", strtotime("+2 hours")).'H'.date('i').'M';
        $req->BkgDetails->ReadyTimeGMTOffset = date('P');
        $req->BkgDetails->DimensionUnit = 'CM';
        $req->BkgDetails->WeightUnit = 'KG';
        $req->BkgDetails->PaymentCountryCode = $from['countryCode'];
        $req->BkgDetails->PaymentAccountNumber = config('alpha.application.dhl.shipperAccountNumber');

        $req->From->CountryCode = $from['countryCode'];
        $req->From->Postalcode = $from['postalCode'];
        $req->From->City = $from['city'];

        $req->To->CountryCode = $to['countryCode'];
        $req->To->Postalcode = $to['postalCode'];
        $req->To->City = $to['city'];

        DB::table('cache')->insert([
				'entity_id' => 0,
				'type' => 'quote_dhl_request',
				'data' => $req->toXML(),
				'created_at' => date('Y-m-d H:i:s')
			]);

        $xmlResponse = $this->client->call($req);

		DB::table('cache')->insert([
				'entity_id' => 0,
				'type' => 'quote_dhl_response',
				'data' => $xmlResponse,
				'created_at' => date('Y-m-d H:i:s')
			]);


        $parsedResponse = simplexml_load_string($xmlResponse);

        $res = $this->parseGetQuoteResponse($parsedResponse,$subTotal,(object)$to,$items);

        return $res;


	}

	public function validateShipment($orderId = 0,$NewShipper = 'Y',$shipmentDate = '')
	{
		
		set_time_limit(0);

		$order = CatalogOrder::with(['user','details','shipping','billing'])->find($orderId);
		if(empty($order)) return false;

		$countryCode = DB::table('countries')->where('name','=',$order->shipping->country)->first();

		$req = new ShipmentRequest();

        $req->SiteID = config('alpha.application.dhl.siteId');
        $req->Password = config('alpha.application.dhl.password');


        $req->MessageTime = date('c');
        $req->MessageReference = date('Ymdhisusiu');
        $req->RegionCode = 'AP';
        $req->RequestedPickupTime = 'Y';
        $req->NewShipper = $NewShipper;
        $req->LanguageCode = 'IN';
        $req->PiecesEnabled = 'Y';


        $req->Billing->ShipperAccountNumber = config('alpha.application.dhl.shipperAccountNumber');
        $req->Billing->ShippingPaymentType = 'S';
        $req->Billing->BillingAccountNumber = config('alpha.application.dhl.billingAccountNumber');
        $req->Billing->DutyPaymentType = 'R';
        $req->Billing->DutyAccountNumber = "";


        $req->Consignee->CompanyName = $order->shipping->first_name.' '.$order->shipping->last_name;
        $req->Consignee->addAddressLine($order->shipping->address);
        $req->Consignee->City = $order->shipping->city;
        $req->Consignee->PostalCode = $order->shipping->postal_code;
        $req->Consignee->CountryCode = $countryCode->code;
        $req->Consignee->CountryName = $order->shipping->country;
        $req->Consignee->Contact->PersonName = $order->shipping->first_name.' '.$order->shipping->last_name;
        $req->Consignee->Contact->PhoneNumber = $order->shipping->phone;
		$req->Consignee->Contact->Email = $order->user->email;


		$req->Shipper->ShipperID = config('alpha.application.dhl.shipperAccountNumber');
        $req->Shipper->CompanyName = app('AlphaSetting')->getSetting('origin_company_name');
        $req->Shipper->RegisteredAccount = config('alpha.application.dhl.shipperAccountNumber');
        $req->Shipper->addAddressLine(app('AlphaSetting')->getSetting('origin_address'));
        $req->Shipper->City = app('AlphaSetting')->getSetting('origin_city');
 		$req->Shipper->PostalCode = app('AlphaSetting')->getSetting('origin_postal_code');
        $req->Shipper->CountryCode = app('AlphaSetting')->getSetting('origin_country_code');
        $req->Shipper->CountryName = app('AlphaSetting')->getSetting('origin_country_name');
 		$req->Shipper->Contact->PersonName = app('AlphaSetting')->getSetting('contact_person_name');
        $req->Shipper->Contact->PhoneNumber = app('AlphaSetting')->getSetting('contact_phone_number');
		$req->Shipper->Contact->Email = app('AlphaSetting')->getSetting('contact_email');


		$req->Commodity->CommodityCode = app('AlphaSetting')->getSetting('commodity_code');
        $req->Commodity->CommodityName = app('AlphaSetting')->getSetting('commodity_name');


        $req->Dutiable->DeclaredValue = "1.00";
        $req->Dutiable->DeclaredCurrency = 'USD';

        $totalWeight = 0;
        $productContent = [];

        foreach ($order->details as $key => $value) 
        {

        	$piece = new Piece();
	        $piece->PieceID = ($key + 1);
	        $piece->Weight = round($value->shipping_weight,2);
	        $piece->Width = round($value->shipping_width,2);
	        $piece->Height = round($value->shipping_height,2);
	        $piece->Depth = round($value->shipping_depth,2);

	        $req->ShipmentDetails->addPiece($piece);

	        $totalWeight += round($value->shipping_weight,2);

	        $productContent[] = strip_tags($value->product_title);
        }

        if(count($productContent) == 0)
        {
        	$productContent = "Our Products";
        }else{
        	$productContent = implode('|', $productContent);
        }

        $req->ShipmentDetails->NumberOfPieces = count($order->details);
		$req->ShipmentDetails->Weight = $totalWeight;
		$req->ShipmentDetails->WeightUnit = 'K';
		$req->ShipmentDetails->GlobalProductCode = $order->carrier_id;
		$req->ShipmentDetails->Date = date('Y-m-d',strtotime($shipmentDate));
		$req->ShipmentDetails->Contents = $productContent;
		$req->ShipmentDetails->DimensionUnit = 'C';		
        // $req->ShipmentDetails->InsuredAmount = '1.00';

        if($order->shipping->country == 'Indonesia')
        {
			$req->ShipmentDetails->IsDutiable = 'N';
        }else{        	
			$req->ShipmentDetails->IsDutiable = 'Y';
        }

		$req->ShipmentDetails->CurrencyCode = 'IDR';

			
		$req->EProcShip = 'N';
		$req->LabelImageFormat = 'PDF';

		$xmlResponse = $this->client->call($req);

		DB::table('cache')->insert([
				'entity_id' => $order->id,
				'type' => 'order_validate_shipment_response',
				'data' => $xmlResponse,
				'created_at' => date('Y-m-d H:i:s')
			]);

		DB::table('cache')->insert([
				'entity_id' => $order->id,
				'type' => 'order_validate_shipment_request',
				'data' => $req->toXML(),
				'created_at' => date('Y-m-d H:i:s')
			]);


		$parsedResponse = simplexml_load_string(utf8_encode($xmlResponse));
		
		file_put_contents(str_replace('\\', '/', public_path()).'/uploads/pdf/dhl-'.$order->id.'.pdf', base64_decode((string)$parsedResponse->LabelImage->OutputImage));
		
		$order->tracking_number = (string)$parsedResponse->AirwayBillNumber;
		$order->save();


		return $parsedResponse;
		

	}

	public function pickup(Request $httpReq)
	{

		set_time_limit(0);

		$order = CatalogOrder::with(['details'])->find($httpReq->order_id);
		$totalWeight = 0;
		foreach ($order->details as $key => $value) {
			$totalWeight += round($value->shipping_weight,2);
		}

		$req = new BookPURequest();
		$req->SiteID = config('alpha.application.dhl.siteId');
        $req->Password = config('alpha.application.dhl.password');


        $req->MessageTime = date('c');
        $req->MessageReference = date('Ymdhisusiu');

        $req->RegionCode = 'AP';

        $req->Requestor->AccountType = 'D';
        $req->Requestor->AccountNumber = config('alpha.application.dhl.shipperAccountNumber');
        $req->Requestor->RequestorContact->PersonName = app('AlphaSetting')->getSetting('contact_person_name');
        $req->Requestor->RequestorContact->Phone = app('AlphaSetting')->getSetting('contact_phone_number');

        $req->Place->CompanyName = app('AlphaSetting')->getSetting('origin_company_name');
        $req->Place->LocationType = 'B';
        $req->Place->City = app('AlphaSetting')->getSetting('origin_city');

        $req->Place->PostalCode = app('AlphaSetting')->getSetting('origin_postal_code');
        $req->Place->CountryCode = app('AlphaSetting')->getSetting('origin_country_code');

		$req->Place->PackageLocation = $httpReq->package_location;
		$req->Place->Address1 = app('AlphaSetting')->getSetting('origin_address');

		$req->Pickup->PickupDate = date('Y-m-d',strtotime($httpReq->pickup_date));
        $req->Pickup->ReadyByTime = date('H:i',strtotime($httpReq->ready_time));
        $req->Pickup->CloseTime = date('H:i',strtotime($httpReq->close_time));
        $req->Pickup->Pieces = count($order->details);
        $req->Pickup->SpecialInstructions = $httpReq->special_instructions;
        $req->Pickup->Weight->Weight = $totalWeight;
        $req->Pickup->Weight->WeightUnit = 'K';

        $req->PickupContact->PersonName = app('AlphaSetting')->getSetting('contact_person_name');
        $req->PickupContact->Phone = app('AlphaSetting')->getSetting('contact_phone_number');

        if($this->env == 'staging')
        {
        	$req->PickupContact->PersonName = "Testing Only";
        	$req->PickupContact->Phone = "0123456789";
        }


		$req->ShipmentDetails->AWBNumber = $order->tracking_number;
        $req->ShipmentDetails->NumberOfPieces = count($order->details);
        $req->ShipmentDetails->Weight = $totalWeight;
        $req->ShipmentDetails->WeightUnit = 'K';
        $req->ShipmentDetails->DimensionUnit = 'C';
        $req->ShipmentDetails->GlobalProductCode = $order->carrier_id;
        $req->ShipmentDetails->DoorTo = $httpReq->door_to;

		$xmlResponse = $this->client->call($req);

		DB::table('cache')->insert([
				'entity_id' => $order->id,
				'type' => 'order_pick_up_response',
				'data' => $xmlResponse,
				'created_at' => date('Y-m-d H:i:s')
			]);

		DB::table('cache')->insert([
				'entity_id' => $order->id,
				'type' => 'order_pick_up_request',
				'data' => $req->toXML(),
				'created_at' => date('Y-m-d H:i:s')
			]);

		$parsedResponse = simplexml_load_string(utf8_encode($xmlResponse));
		return $parsedResponse;

	}

	public function cancelPickUp($orderId = 0)
	{
		$order = CatalogOrder::find($orderId);
		
		$reqPickup = DB::table('cache')
		->where('entity_id','=',$order->id)
		->whereType('order_pick_up_request')
		->orderBy('id','desc')
		->first();

		$reqPickup = simplexml_load_string($reqPickup->data);

		$resPickup = DB::table('cache')
		->where('entity_id','=',$order->id)
		->whereType('order_pick_up_response')
		->orderBy('id','desc')
		->first();

		$resPickup = simplexml_load_string($resPickup->data);



		$req = new CancelPURequest();

        $req->SiteID = config('alpha.application.dhl.siteId');
        $req->Password = config('alpha.application.dhl.password');

		$req->MessageTime = date('c');
        $req->MessageReference = date('Ymdhisusiu');

        $req->RegionCode = 'AP';
		$req->ConfirmationNumber = (string)$resPickup->ConfirmationNumber;
        $req->RequestorName = (string)$reqPickup->Requestor->RequestorContact->PersonName;
        $req->CountryCode = "ID";
        $req->OriginSvcArea = (string)$resPickup->OriginSvcArea;
        $req->PickupDate = (string)$reqPickup->Pickup->PickupDate;
        $req->CancelTime = date('h:i');

        $xmlResponse = $this->client->call($req);

		DB::table('cache')->insert([
				'entity_id' => $order->id,
				'type' => 'order_cancel_pick_up_response',
				'data' => $xmlResponse,
				'created_at' => date('Y-m-d H:i:s')
			]);

		DB::table('cache')->insert([
				'entity_id' => $order->id,
				'type' => 'order_cancel_pick_up_request',
				'data' => $req->toXML(),
				'created_at' => date('Y-m-d H:i:s')
			]);

		$parsedResponse = simplexml_load_string($xmlResponse);

		return $parsedResponse;



	}

}