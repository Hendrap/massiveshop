<?php

namespace App;

use Illuminate\Http\Request;
use CatalogOrder;

class Doku
{

	public function checkOrderStatus($orderNumber,$mallid,$sharedKey,$env)
	{
		$endPoint = config("alpha.application.doku.checkStatus.$env");
		$order = CatalogOrder::whereOrderNumber($orderNumber)->first();
		if(empty($order)) die("Invalid");

		$dokuData = json_decode($order->data);

		if(empty($dokuData)) die("Invalid");

		if($dokuData->currency == 360)
		{
			$words  = sha1($mallid.$sharedKey.trim($orderNumber));			
		}else{
			$words  = sha1($mallid.$sharedKey.trim($orderNumber).$dokuData->currency);
		}

														
		$data = "MALLID=".$mallid."&CHAINMERCHANT="."NA"."&TRANSIDMERCHANT=".$orderNumber."&SESSIONID=".$dokuData->sessionId."&PAYMENTCHANNEL=&WORDS=".$words;
				
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $endPoint);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);        
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$output = curl_exec($ch);
		$curl_errno = curl_errno($ch);
		$curl_error = curl_error($ch);
		curl_close($ch);        

		if ($curl_errno > 0)
		{
			die("Error");
		}             
				
		libxml_use_internal_errors(true);
		$xml = simplexml_load_string($output);
		
		if($xml->RESULTMSG == 'SUCCESS')
		{
			$order->status = 'APPROVED';
			$order->save();
		}

		return $xml;
	}

	public function getIpClient()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
	}

	public function isValidIp()
	{
		$ipClient = $this->getIpClient();

		$dokuIprRange =  '103.10.129';

		if(substr($ipClient,0,strlen($dokuIprRange)) !== $dokuIprRange)
		{
			return false;
		}

		return true;


	}

	public function getMerchantCredentials($env)
	{
		$mallid = config("alpha.application.doku.credentials.$env.mallid");
		$sharedKey = config("alpha.application.doku.credentials.$env.sharedKey");

		return [$mallid,$sharedKey];
	}

	public function handleNotify(Request $req)
	{
		\Log::info(json_encode($_POST)); 

		$statusIP = $this->isValidIp();
		if($statusIP == false)
		{
			return json_encode(['status'=>'Ip Ga Valid']);
		}

		$env = config('alpha.application.doku.env');
		list($mallid,$sharedKey) = $this->getMerchantCredentials($env);
		$credential = $mallid.$sharedKey;

		if($req->CURRENCY == 360)
		{
			$words = sha1($req->AMOUNT.$credential.$req->TRANSIDMERCHANT.$req->RESULTMSG.$req->VERIFYSTATUS);
		}else{
			$words = sha1($req->AMOUNT.$credential.$req->TRANSIDMERCHANT.$req->RESULTMSG.$req->VERIFYSTATUS.$req->CURRENCY);			
		}

		if($words == $req->WORDS)
		{
			echo "CONTINUE";

			if($req->RESULTMSG == 'SUCCESS' && $req->RESPONSECODE == "0000" && $req->STATUSTYPE == "P")
			{
				$order = CatalogOrder::whereOrderNumber($req->TRANSIDMERCHANT)->first();
				if(!empty($order))
				{
					app('App\Http\Controllers\CheckoutController')->sendEmailOrder($order->id,'payment-confirmation',app('AlphaSetting')->getSetting('admin_email_contact'));
					$order->status = 'APPROVED';
					$order->save();
				}
				//$xml = $this->checkOrderStatus($req->TRANSIDMERCHANT,$mallid,$sharedKey,$env);
			}

		}else{
			echo "Stop";
		}

	}

	public function twoPlaceDecimalFormat($number = 0)
	{
		return number_format((float)$number, 2, '.', '');
	}

	public function capture(
		$items = array(),
		$subTotal = 0,
		$shippingCost = 0,
		$orderNumber = "",
		$email,
		$name,
		$currency
		)
	{


		$subTotal = $this->twoPlaceDecimalFormat($subTotal);
		$shippingCost = $this->twoPlaceDecimalFormat($shippingCost);

		$grandTotal = $subTotal + $shippingCost;

		$grandTotal = $this->twoPlaceDecimalFormat($grandTotal);

		$env = config('alpha.application.doku.env');
		$endPoint = config("alpha.application.doku.endpoints.$env");
		list($mallid,$sharedKey) = $this->getMerchantCredentials($env);
	
		$currency = config("alpha.application.doku.currency.$currency");

		$basket = [];
		foreach ($items as $key => $value) {
			$basket[] = str_replace(',', ' ',$value->product_title).','.$value->original_price.','.$value->qty.','.$value->original_total;
		}

		$basket[] = "Shipping Cost,$shippingCost,1,$shippingCost";

		$basket = implode(';', $basket);
		if($currency == 360)
		{
			$words = sha1($grandTotal.$mallid.$sharedKey.$orderNumber);
		}else{
			$words = sha1($grandTotal.$mallid.$sharedKey.$orderNumber.$currency);
		}
		
		$sessionId = sha1(time());
		$requestDateTime = date('YmdHis');

		$jsonData = (object)[
			'subTotal' => $subTotal,
			'grandTotal' => $grandTotal,
			'shippingCost' => $shippingCost,
			'mallid' => $mallid,
			'sharedKey' => $sharedKey,
			'basket' => $basket,
			'words' => $words,
			'orderNumber' => $orderNumber,
			'sessionId' => $sessionId,
			'requestDateTime' => $requestDateTime,
			'email' => $email,
			'name' => $name,
			'currency' => $currency
		];

		$order = CatalogOrder::whereOrderNumber($orderNumber)->first();
		if(!empty($order))
		{
			$order->data = json_encode($jsonData);
			$order->save();
		}

		return view('app::clean.templates.doku-request-payment',[
				'basket' => $basket,
				'words' => $words,
				'sharedKey' => $sharedKey,
				'mallid' => $mallid,
				'endPoint' => $endPoint,
				'grandTotal' => $grandTotal,
				'orderNumber' => $orderNumber,
				'email' => $email,
				'name' => $name,
				'requestDateTime' => $requestDateTime,
				'sessionId' => $sessionId,
				'currency' => $currency
			]);	






	}

}