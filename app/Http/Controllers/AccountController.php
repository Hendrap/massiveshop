<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CoreController as BaseController;
use App\Http\Controllers\UserController;
use App\Http\Requests;
use Auth;
use DB;
use Session;
use User;
use Usermeta;
use Hash;
use Taxonomy;
use CatalogAddress;

class AccountController extends BaseController
{
    public $template = 'massive';
    public $with;
    
    public function __construct()
    {
         parent::__construct();
    }

    public function account()
    {
        if (empty(Auth::user())) {
            return redirect('/');
        }
        
        $billing = CatalogAddress::where('user_id','=',Auth::user()->id)->where('is_checkout','!=',1)->where('type','=','billing')->first();
        $shipping = CatalogAddress::where('user_id','=',Auth::user()->id)->where('is_checkout','!=',1)->where('type','=','shipping')->get();
        
        $this->layout->content = view('app::'.$this->template.'.page.account-main',[
            'billing' => $billing,
            'shipping' => $shipping
        ]);

        
        $this->layout->menu = '';
        $this->layout->title = 'Account | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function forget_password()
    {
        $this->layout->menu = '';
        $this->layout->title = 'Forget Password | '.app('AlphaSetting')->getSetting('site_name');
        $this->layout->content = view('app::'.$this->template.'.page.forget-password',[
            'siteTitle' => 'Hi,Everyone!',
            ]);    
    }

    public function change_password()
    {
        $this->layout->menu = '';
        $this->layout->title = 'Change Password | '.app('AlphaSetting')->getSetting('site_name');
        $this->layout->content = view('app::'.$this->template.'.page.account-change-password',[
            'siteTitle' => 'Hi,Everyone!',
            ]);    
    }

    public function change_password_send()
    {
        $input = \Request::all();
        $mail = $input['email'];
        $length = 32;
        $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $str = "";
        $date = date('Y-m-d h:i:s');
    
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        $cek = User::whereEmail($input['email'])->first();
        if (count($cek)) {
            $user_id = $cek->user_id;
        }else {
            return redirect()->back()->with('send','notfound');
        }
        $data = DB::table('usermetas')->insert(['user_id' => $user_id, 'meta_key' => 'forget_password', 'meta_name' => $mail, 'meta_value_text' => base64_encode($str), ]);
        $nama = User::where('email','=',$mail)->first();
        
        $parseEmail = array(
            "[[name]]" => $nama->username,
            "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($input['email']),
            "[[forget_url]]" => url('/forget-password-page').'/'.base64_encode($str),
        );   
        $emailMessages = parseEmail("forget-password",$parseEmail);
        $subject = DB::table('entries')->where('entry_type','email_template')->where('slug','forget-password')->first();
        $message = app('AlphaMailMessage')->setSubject(parseMultiLang($subject->title))->setBody($emailMessages->msg,'text/html')->setFrom(config('alpha.mailer.sender'))->setTo(array($input['email']));
        $mailer = app('AlphaMailSender')->send($message);
        if ($mailer) {
            return redirect()->back()->with('send','send');   
        }
    }

    public function change_password_callback($key)
    {
        //echo $key;
        $data = DB::table('usermetas')->where('meta_value_text','=',$key)->first();
        $this->layout->menu = '';
        $this->layout->title = 'Change Password'.app('AlphaSetting')->getSetting('site_name');
        $this->layout->content = view('app::'.$this->template.'.page.account-change-password',[
            'siteTitle' => 'Hi,Everyone!',
            'data' => $data
            ]);

    }

    public function forget_password_prosses()
    {
        $input = \Request::all();

        $rules = array(
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        );

        $Validator = \Validator::make($input,$rules);
        $messages = $Validator->errors();

        if ($Validator->fails()) {

            return redirect()->back()->with('errors',$messages);
        }

        $login = \Request::only('email','password');
        $data = DB::table('users')->whereEmail($input['email'])->first();
        $pass = Hash::make($input['password']);
        //$key = \Request::segment(2);
        if (!empty($data)) {
            
            $addresses_s = DB::table('users')->where('email','=',$input['email'])->update(array('password' => $pass));
            if ($addresses_s) {
                if (\Auth::attempt(array('email' => $input['email'], 'password' => $input['password']))) {
                $user = \User::whereEmail($input['email'])->first();
                $user->last_login = date('Y-m-d H:i:s');
                $user->save();
                $url = url('/');
                $parseEmail = array(
                        "[[name]]" => $data->username,
                        "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($input['email']),
                    );   
                    $emailMessages = parseEmail("forget-password-feedback",$parseEmail);
                    $subject = DB::table('entries')->where('entry_type','email_template')->where('slug','forget-password-feedback')->first();
                    $message = app('AlphaMailMessage')->setSubject(parseMultiLang($subject->title))->setBody($emailMessages->msg,'text/html')->setFrom(config('alpha.mailer.sender'))->setTo(array($input['email']));
                    $mailer = app('AlphaMailSender')->send($message);
                    return redirect($url);
                }else{
                  return redirect()->back();
                }
            }
            
        }
    }

    public function change_password_prosses()
    {
        $input = \Request::all();
        $rules = array(
            'old-password' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        );

        $Validator = \Validator::make($input,$rules);
        $messages = $Validator->errors();

        if ($Validator->fails()) {

            return redirect()->back()->with('errors',$messages);
        }

        $input = \Request::all();
        $data = DB::table('users')->whereEmail($input['email'])->first();
        if (!empty($data)) {
            if (Hash::check($input['old-password'], $data->password)) {
                    $addresses_s = DB::table('users')->where('email','=',$input['email'])->update(array('password' => Hash::make($input['password'])));
                    $parseEmail = array(
                        "[[name]]" => $data->username,
                        "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($input['email']),
                    );   
                    $emailMessages = parseEmail("change-password-feedback",$parseEmail);
                    $subject = DB::table('entries')->where('entry_type','email_template')->where('slug','change-password-feedback')->first();
                    $message = app('AlphaMailMessage')->setSubject(parseMultiLang($subject->title))->setBody($emailMessages->msg,'text/html')->setFrom(config('alpha.mailer.sender'))->setTo(array($input['email']));
                    $mailer = app('AlphaMailSender')->send($message);
                    // forget-password-feedback
                    return redirect('/account/change-password')->with('success','success');
            }
            return redirect('/account/change-password')->with('success','failed');
        }
    }

    public function add_address()
    {

        $country = DB::table('countries')->get();
        $this->layout->content = view('app::'.$this->template.'.page.add-address',[
            'country' => $country,
            ]);

       $this->layout->menu = '';
       $this->layout->title = 'Add Address | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function save_add_address(Request $req)
    {
        $userCtrl = new UserController();

        $Validator = \Validator::make($req->all(),$userCtrl->getAddressRules());
        $messages = $Validator->errors();
        if($Validator->fails()){
            return redirect()->back()->withInput()->with('errors',$messages);
        }
        
        $address = $userCtrl->createUpdateAddress(Auth::user()->id,$req,'shipping',$req->label);

        return redirect(url('account'))->with('success_msg_account','Address Added');

    }

    public function edit_address_ship($id)
    {
        // Session::put('urlr',\Request::url());
        $country = DB::table('countries')->get();
        $data = DB::table('catalog_addresses')->where('id','=',$id)->first();
        $this->layout->content = view('app::'.$this->template.'.page.edit-address',[
            'siteTitle' => 'Hi,Everyone!',
            'data' => $data,
            'country' => $country,
            ]);
        $this->layout->menu = '';
        $this->layout->title = 'Edit Address | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function editBillingAddress($id)
    {

        $data = CatalogAddress::whereType('billing')->where('user_id',Auth::user()->id)->where('id','=',$id)->first();
        $country = DB::table('countries')->get();
        $this->layout->content = view('app::'.$this->template.'.page.edit-billing-address',[
            'data' => $data,
            'country' => $country,
            ]);

        $this->layout->menu = '';
        $this->layout->title = 'Edit Billing Address | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function editShippingAddress($id)
    {
        $data = CatalogAddress::whereType('shipping')->where('user_id',Auth::user()->id)->where('id','=',$id)->first();
        $country = DB::table('countries')->get();
        $this->layout->content = view('app::'.$this->template.'.page.edit-shipping-address',[
            'data' => $data,
            'country' => $country,
            ]);

        $this->layout->menu = '';
        $this->layout->title = 'Edit Shipping Address | '.app('AlphaSetting')->getSetting('site_name');
    }
   
    public function saveShippingAddress(Request $req)
    {
        $userCtrl = new UserController();

        $Validator = \Validator::make($req->all(),$userCtrl->getAddressRules());
        $messages = $Validator->errors();
        if($Validator->fails()){
            return redirect()->back()->withInput()->with('errors',$messages);
        }
        
        $address = $userCtrl->createUpdateAddress(Auth::user()->id,$req,'shipping',$req->label);

        return redirect(url('account'))->with('success_msg_account','Address Saved');
    }

    public function saveBillingAddress(Request $req)
    {
       
        $userCtrl = new UserController();

        $Validator = \Validator::make($req->all(),$userCtrl->getAddressRules());
        $messages = $Validator->errors();
        if($Validator->fails()){
            return redirect()->back()->withInput()->with('errors',$messages);
        }
        
        $address = $userCtrl->createUpdateAddress(Auth::user()->id,$req,'billing',$req->label);

         return redirect(url('account'))->with('success_msg_account','Address Saved');
    }

    public function delete_address($id)
    {
         $delete = DB::table('catalog_addresses')->where('user_id',Auth::user()->id)->where('id','=',$id)->delete();
         return redirect(url('account'))->with('success_msg_account','Address Deleted');
    }

    public function register(Request $request)
    {
        $country = DB::table('countries')->get();
        $this->layout->content = view('app::'.$this->template.'.page.login-register',[
            'country' => $country,
            ]);
        $this->layout->menu = '';
        $this->layout->title = 'Register | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function order_history()
    {
        $data = DB::table('catalog_orders')->where('user_id','=',Auth::user()->id)->orderBy('created_at','DESC')->paginate(5);
        $this->layout->content = view('app::'.$this->template.'.page.account-order-history',[
            'siteTitle' => 'Hi,Everyone!',
            'data' => $data
            ]);
        $this->layout->menu = '';
        $this->layout->title = 'Order History | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function order_detail($id)
    {
        
        if(!Auth::user()) abort(404);

        $order = \CatalogOrder::with(['billing','shipping','user','details'])->where('id','=',$id)->where('user_id','=',Auth::user()->id)->first();
        if(empty($order)) abort(404);

        $this->layout->content = view('app::'.$this->template.'.page.account-order-detail',[
            'siteTitle' => 'Hi,Everyone!',
            'order' => $order,
            ]);
        $this->layout->menu = '';
        $this->layout->title = 'Order Detail | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function registerSave(Request $req)
    {
       
        
        $userCtrl = new UserController();

        $validator = \Validator::make($req->all(),$userCtrl->getRegisterRules($req));
        $messages = $validator->errors();
        if ($validator->fails()) {
            return redirect()->back()->withInput($req->all())->with('errors',$messages);

        }
        $email = $req->billing['email'];

        $check = \User::whereEmail($email)->first();
        if(count($check)){
             return redirect()->back()->with('email-error','Email has been taken before !')->withInput();
        }

        $userCtrl->register($req);
            
        return redirect()->back()->with('msg-success','Register success! , Please check email to validate your account.');
      
    }

    public function account_validate($key)
    {
        $email = base64_decode($key);
        $update = DB::table('users')->where('email',$email)->update(array('status' => 'active'));
        $login = DB::table('users')->where('email',$email)->first();
        
        if (\Auth::attempt(array('email' => $email, 'password' => $login->password))) {
               return redirect('/')->with('success_msg','Account Activated');
            }else{
           return redirect('/')->with('success_msg','Account Activated');
        }

        //return redirect('/');
    }

    public function login(Request $request)
    {
        $errors = [];
        $input = \Request::except('url');
        $inurl = \Request::all();

        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];
    
        $v = \Validator::make($input,$rules);
        $messages = $v->errors();

        if ($v->fails()){
            return response()->json(array(
                'errors' => $messages
            ));
        }

        unset($input['_token']);

        if(\Auth::attempt($input)){
            
            $user = \User::whereEmail($input['email'])->first();
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();


            return redirect($inurl['url']);
        }
            
        $errors = 'Password & email doesn\'t match';
        // return redirect()->back()->with('errors',$errors);
        return response()->json(array(
            'errorss' => $errors
        ));

        
    }

    public function logout()
    {
        if (\Auth::user()) {
            \Auth::logout();
            Session::flush();
            return redirect('/');
        }
        return redirect('/');
    }

    public function getCity()
    {
        $searchTerm = \Request::input('term');
        $data = array();
        $cities =  DB::select("SELECT DISTINCT regency FROM alp_catalog_jne WHERE regency LIKE '%".$searchTerm."%' ORDER BY regency ASC");
        foreach ($cities as $value) {
            $data[] = $value->regency;
        }

        return json_encode($data);
    }

    public function getDistricts()
    {
        $searchTerm = \Request::input('term');
        $city = \Request::input('city');
        $data = array();
        if(empty($city)){
            $data[] = "Chose city";
        }
        $districts =  DB::select("SELECT DISTINCT district FROM alp_catalog_jne WHERE district LIKE '%".$searchTerm."%' AND regency = '".$city."' ORDER BY regency ASC");
        foreach ($districts as $value) {
            $data[] = $value->district;
        }

        return json_encode($data);
    }
}
