<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\CoreController as BaseController;
use Alpha\Requests;
use Alpha\Core\AlphaSetting;
use Illuminate\Http\Request;
use Usermeta;
use Entry;
use Entrymeta;
use Taxonomy;
use DB;


class ArticleController extends BaseController
{
  	public $template = 'massive';
  	public $with;
    

    public function __construct()
    {
	      parent::__construct();
    }

    public function index()
    {
    	$latest = Entry::with(['user'])->where('published_at','<=',date('Y-m-d H:i:s'))
		      ->where('entry_type','=','news')
		      ->whereStatus('published')
		      ->orderBy('published_at','DESC')
		      ->paginate(5);

		$this->layout->content = view('app::'.$this->template.'.page.news-event-list',[
            'news' => $latest,
            ]);

    }

    public function detail()
    {
    	
    }
}
