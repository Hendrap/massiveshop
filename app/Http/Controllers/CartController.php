<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CoreController as BaseController;
use Illuminate\Http\Request;
use Cart;
use Entry;
use CatalogItem;
use Auth;
use DB;
use Jne;
use Session;
use URL;
use App\Veritrans\Veritrans;
use PayPal\Api;
use User;
use Taxonomy;
use CatalogOrder;
use CatalogOrderDetail;
//shipping dhl
use DHL\Entity\GB\ShipmentResponse;
use DHL\Entity\GB\ShipmentRequest;
use DHL\Datatype\GB\Piece;
use DHL\Datatype\GB\SpecialService;
//getQuote
use DHL\Entity\AM\GetQuote;
use DHL\Datatype\AM\PieceType;
//tracking
use DHL\Entity\EA\KnownTrackingRequest as Tracking;
use DHL\Entity\EA\TrackingResponse as TrackingResponse;
use DHL\Datatype\EA\TrackingPieces;
//book PU request
use DHL\Entity\GB\BookPURequest as book;
use DHL\Datatype\GB\Requestor;
//book cancel PU request
use DHL\Entity\EA\CancelPickupRequest as bookCancel;
//book modify PU request
use DHL\Entity\GB\ModifyPURequest as bookModify;
use DHL\Client\Web as WebserviceClient;
use GuzzleHttp\Client;
// require(__DIR__ . '/../../init.php');

class CartController extends BaseController 
{
    
    public $template = 'clean';
    public $with;

    public function __construct()
    {
        parent::__construct();
    }

    public function cart()
    {
        $items = $this->layout->cart['orderDetails'];
        $subTotal = $this->layout->cart['order']->original_subtotal;

        $this->layout->content = view('app::'.$this->template.'.page.cart',[
            'items' => $items,
            'subTotal' => $subTotal
            ]);

        $this->layout->menu = 'product';
        $this->layout->title = 'Cart | '.app('AlphaSetting')->getSetting('site_name');
    }
    
    public function add_to_cart(Request $req)
    {
        $userId = 0;
        if(Auth::user()) $userId = Auth::user()->id;

        $res = \CatalogCart::addItemToCart($req->item_id,(int)$req->qty,$userId);
        if($res['status'] == 1)
        {
            $selectedItem = [];
            $items = translateCartToParsedData();
            $subTotal = $items['order']->original_subtotal;

            foreach ($items['orderDetails'] as $key => $value) {
                if($value->item_id == $res['item']->id)
                {
                    $selectedItem = $value;
                }
            }

            return json_encode($res);
        }

        return json_encode($res);
    }

    public function getCartData()
    {
        $items = array();
        
        $res = \CatalogCart::getItems();    

        foreach ($res as $key => $value) {
            $items[$key]['item_id'] = $value->item_id;
            $items[$key]['session_id'] = $value->session_id;
            $items[$key]['product_name'] = parseMultiLang($value->item->entry->title)." - ".$value->item->item_title;
            $items[$key]['qty'] = $value->qty;
            $items[$key]['original_price'] = $value->original_price;    
        }
        return json_encode($items);
    }

    public function remove_from_cart(Request $req)
    {
        $userId = 0;
        if(Auth::user()) $userId = Auth::user()->id;
        //dd($request->item_id);
        $deletecart = \CatalogCart::removeItemFromCart($req->item_id,$userId);

        return json_encode($deletecart);
        //return response()->json($requests);
    }

    public function checkout()
    {
        Session::put('checkout-login',\Request::url());
        if(!Auth::check()){
            return redirect('/checkout-login-page');
        }
        return redirect('/checkout-shipping');
    }

    public function checkout_login_page()
    {

        $this->layout->content = view('app::'.$this->template.'.page.checkout-login',[
            'siteTitle' => 'Hi,Everyone!',
            ]);
        $this->layout->menu = 'product';
        $this->layout->title = 'Checkout Login | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function checkout_login()
    {
        $this->layout->menu = 'product';
        $this->layout->content = view('app::'.$this->template.'.page.checkout-login',[
            'siteTitle' => 'Hi,Everyone!',
            ]);
        $this->layout->title = 'Checkout Login | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function checkout_login_in(Request $request)
    {
        $errors = [];
        $input = \Request::all();

        $rules = [
        'email' => 'required|email',
        'password' => 'required',
        ];
    
        $v = \Validator::make($input,$rules);
        //dd($v);

        if ($v->fails()){
            foreach ($v->errors()->messages() as $key => $value) {
                foreach ($value as $v) {
                    $errors[] = $v;
                }
            }
            // dd();
            return redirect()->back()->with('errors',$errors);
        }

        //buang csrf token
        unset($input['_token']);

        // //ambil remember
        // $remember = @$input['remember'] or false;
        // unset($input['remember']);

        //ambil redirect
        // $redirect = @$input['redirect'] or false;
        // unset($input['redirect']);
        //dd($input);
        if(\Auth::attempt($input)){
            
            $user = \User::whereEmail($input['email'])->first();
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();
            //redirect ke tempat yg mau dituju sebelumnya
            // if($redirect){
            //     return redirect()->away($redirect);             
            // }

            return redirect()->back();    

        }else{

            $errors[] = 'Password & email doesn\'t match';
            return redirect()->back()->with('errors',$errors);

        }
    }

    public function checkout_sign_up()
    {
        $input = \Request::all();
        
        $inputaddress = \Request::except('password','repassword','_token','useaddress','submit', 'receiver_name', 'receiver_address', 'receiver_country', 'receiver_city', 'receiver_state', 'receiver_postal_code','receiver_phone','receiver_first_name','receiver_district');
        $inputship = \Request::only('receiver_first_name','email', 'receiver_address', 'receiver_country', 'receiver_city', 'receiver_state', 'receiver_postal_code','receiver_phone','receiver_district');
        //dd($input);
        
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
        );

        $v = \Validator::make($input,$rules);

        if ($v->fails()) {

            return redirect()->back()->withError($v);
        }
        // if ($Validator->fails()){
  //           foreach ($Validator->errors()->messages() as $key => $value) {
  //               foreach ($value as $k => $v) {
  //                   $errors[] = $v;
  //               }
  //           }
  //           // dd($errors);
  //           // dd($v->errors()->messages());
  //           return redirect()->back()->with(['errors' => $errors, 'msg' => 'fail']);
  //       }

        //validasi edit-create
        // $key = 'receiver_first_name';
        // $k = substr($key,9);
        // dd($k);exit;
        
        unset($input['_token']);

        //dd($addresses);

        $user = \User::find($input['email']);
        if($input['email'] == 0 || empty($user)){
            $user = new \User();
        }

        //email validation
        $check = \User::whereEmail($input['email'])->where('email','!=',$input['email'])->count();
        if($check > 0){
             return redirect()->back()->with('msg','Email has been taken before !');
        }
        //username
        $check = \User::where('email','!=',$input['email'])->where('username','like',$input['first_name'].'%')->count();
        if($check > 0){
            $input['first_name'] = $input['first_name'].'-'.$check;
        }
        //user 
        $user->email = $input['email'];
        $user->username = $input['first_name'];
        $user->status = 'inactive';
        if(!empty($input['password'])){
            $user->password = bcrypt($input['password']);
        }
        //$user->modified_by = app('AdminUser')->user->id;
        $user->touch();
        $user->save();
        //user metas
        $user->saveUserMetaFromInput($this->metas,$input);
        //role
        $user->roles()->attach('Registered');
        
    }

    public function checkout_shipping()
    {
        Session::put('urlr',\Request::url());
        $id = Auth::user()->id;
        $data = DB::table('catalog_addresses')->where('user_id','=',$id)->where('type','=','shipping')->get();
        $jne_city = \CatalogShippingMethod::getCity(@$data[0]->city);
        $jne_dist = \CatalogShippingMethod::getDistrict(@$data[0]->city,@$data[0]->district);
        $this->layout->content = view('app::'.$this->template.'.page.checkout-shipping',[
            'siteTitle' => 'Hi,Everyone!',
            'data' => $data
            ]);
        $this->layout->menu = 'product';
        $this->layout->title = app('AlphaSetting')->getSetting('site_name').' | Shipping';
    }

    public function get_jne()
    {
        $id = Auth::user()->id;
        $data = DB::table('catalog_addresses')->where('user_id','=',$id)->where('type','=','shipping')->get();
        //$data_jne = DB::table('catalog_addresses')->where('user_id','=',$id)->first();
        //dd($data_jne);
        $jne_city = \CatalogShippingMethod::getCity($data[0]->city);
        $jne_dist = \CatalogShippingMethod::getDistrict($data[0]->city,$data[0]->district);
        $jne_cost = \CatalogShippingMethod::getShippingCost(array('city' => $data[0]->city,'district'=>$data[0]->district,'jne_type'=>'jne_oke/reg/yes','weight'=>'1'));
        dd($jne_cost);exit();
    }

    public function change_address()
    {
        $address = \Request::all();
        $add = $address['data'];
        $id = Auth::user()->id;
        $weight = 0;
        $weight_data = \CatalogCart::getItems(array('item.entry.medias', 'item', 'item.entry'));
        foreach ($weight_data as $key => $value) {
            //dd($value->item->entry->id);
            $get_weight = DB::table('entrymetas')->where('entry_id','=',$value->item->entry->id)->where('meta_key','=','weight')->get();

            $weight = $get_weight[0]->meta_value_text + $weight;
        }
        
        $data = DB::table('catalog_addresses')->where('user_id','=',$id)->where('type','=','shipping')->where('label','=',$add)->first();
        //$data_jne = DB::table('catalog_addresses')->where('user_id','=',$id)->first();
        //dd($data_jne);
        $jne_city = \CatalogShippingMethod::getCity($data->city);
        // dd($jne_city);
        $jne_dist = \CatalogShippingMethod::getDistrict($data->city,$data->district);
        //dd($jne_dist);
        $jne_cost = \CatalogShippingMethod::getShippingCost(array('city' => $data->city,'district'=>$data->district,'jne_type'=>'jne_oke','weight'=>$weight));
        $jne_select = "";

        if(empty($jne_dist[0])){
            $jne_select .= '<option data-label="jne_oke" data-type="jne_oke" data-id="" value="">Wrong District</option>';
            return response()->json(array(
                'datas' => $data,
                'jne_s' => $jne_select,
                'status' => 0,
                'msg'=>"Jne Tidak ada"
            ));
        }
        $oke = $jne_dist[0]->jne_oke;
        $reg = $jne_dist[0]->jne_reg;
        $yes = $jne_dist[0]->jne_yes;
        
        if ($jne_dist[0]->jne_oke > 0) {
            $jne_select .= '<option data-label="jne_oke" data-type="jne_oke" data-id="'.$jne_dist[0]->id.'" value="jne_oke, '.$oke*$weight.'">JNE OKE ('.number_format($oke*$weight).')</option>';
        }

        if ($jne_dist[0]->jne_reg > 0) {
            $jne_select .= '<option data-label="label_jne_reg" data-type="jne_reg" data-id="'.$jne_dist[0]->id.'" value="jne_reg, '.$reg*$weight.'" selected>JNE REGULER ('.$reg*$weight.')</option>';
        }

        if ($jne_dist[0]->jne_yes > 0) {
            $jne_select .= '<option data-label="label_jne_yes" data-type="jne_yes" data-id="'.$jne_dist[0]->id.'" value="jne_yes, '.$yes*$weight.'">JNE YES ('.number_format($yes*$weight).')</option>';
        }
        return response()->json(array(
            'datas' => $data,
            'jne_city' => $jne_city,
            'jne_dist' => $jne_dist,
            'jne_cost' => $jne_cost,
            'jne_select' => $jne_select,
            'status' => 1,
            'weight' => $weight_data
        ));
    }

    public function change_address_nonmember()
    {
        $address = \Request::all();

        $weight = 0;
        $weight_data = \CatalogCart::getItems(array('item.entry.medias', 'item', 'item.entry'));
        foreach ($weight_data as $key => $value) {
            //dd($value->item->entry->id);
            $get_weight = DB::table('entrymetas')->where('entry_id','=',$value->item->entry->id)->where('meta_key','=','weight')->get();
            $weight = $weight + $get_weight[0]->meta_value_text;
        }
        
        //dd($data_jne);
        $jne_city = \CatalogShippingMethod::getCity($address['city']);
        // dd($jne_city);
        $jne_dist = \CatalogShippingMethod::getDistrict($address['city'],$address['district']);
        //dd($jne_dist);
        $jne_cost = \CatalogShippingMethod::getShippingCost(array('city' => $address['city'],'district'=>$address['district'],'jne_type'=>'jne_oke','weight'=>$weight));
        $jne_select = "";

        if(empty($jne_dist[0])){
            $jne_select .= '<option data-label="jne_oke" data-type="jne_oke" data-id="" value="">Wrong District</option>';
            return response()->json(array(
                //'datas' => $data,
                'jne_s' => $jne_select,
                'status' => 0,
                'msg'=>"Jne Tidak ada"
            ));
        }
        $oke = $jne_dist[0]->jne_oke;
        $reg = $jne_dist[0]->jne_reg;
        $yes = $jne_dist[0]->jne_yes;
        
        if ($jne_dist[0]->jne_oke > 0) {
            $jne_select .= '<option data-label="jne_oke" data-type="jne_oke" data-id="'.$jne_dist[0]->id.'" value="jne_oke, '.$oke*$weight.'">JNE OKE ('.number_format($oke*$weight).')</option>';
        }

        if ($jne_dist[0]->jne_reg > 0) {
            $jne_select .= '<option data-label="label_jne_reg" data-type="jne_reg" data-id="'.$jne_dist[0]->id.'" value="jne_reg, '.$reg*$weight.'" selected>JNE REGULER ('.number_format($reg*$weight).')</option>';
        }

        if ($jne_dist[0]->jne_yes > 0) {
            $jne_select .= '<option data-label="label_jne_yes" data-type="jne_yes" data-id="'.$jne_dist[0]->id.'" value="jne_yes, '.$yes*$weight.'">JNE YES ('.number_format($yes*$weight).')</option>';
        }
        return response()->json(array(
            //'datas' => $data,
            'jne_city' => $jne_city,
            'jne_dist' => $jne_dist,
            'jne_cost' => $jne_cost,
            'jne_select' => $jne_select,
            'status' => 1,
            'weight' => $weight_data
        ));
    }

    public function checkout_shipping_save()
    {
        $input = \Request::all();

        $rules = array(
            'jne' => 'required',
        );

        $Validator = \Validator::make($input,$rules);
        $messages = $Validator->errors();

        if ($Validator->fails()) {

            return redirect()->back()->with('errors',$messages)->withInput();
        }

        $data_address = DB::table('catalog_addresses')->where('id','=',$input['addressId'])->where('label','=',$input['label'])->first();
        $data_addressb = DB::table('catalog_addresses')->where('user_id','=',Auth::user()->id)->whereType('billing')->first();
        $data_order = CatalogOrder::where('order_number',Session::get('order_number_old'))->orderBy('created_at','DESC')->first();
        Session::forget('order_number_old');
        if (empty($input['jne'])  || $input['jne'] == 'Disable') {
            return redirect()->back()->withInput();
        }
            $method_price = explode('-', $input['jne']);
            if (count($data_order) >= 1) {
                $order_number = $data_order->order_number;
            }else{
                $order_number = getOrderNumber(6);
            }
            //dd($order_number);
            $insert = Session::put([
                'shipping_id' => $input['addressId'],
                'user_id' => Auth::user()->id,
                'order_number' => $order_number,
                'billing_id' => @$data_addressb->id,
                //'shipping_email' => $data_address->email,
                //'shipping_address_1' => $data_address->address,
                //'shipping_firstname' => $data_address->first_name,
                //'shipping_lastname' => $data_address->last_name,
                //'shipping_company' => $data_address->company,
                //'shipping_city' => $data_address->city,
                //'shipping_postcode' => $data_address->postal_code,
                //'shipping_country' => $data_address->country,
                //'shipping_state' => $data_address->state,
                //'shipping_district' => $data_address->district,
                //'shipping_province' => $data_address->province,
                //'shipping_phone' => $data_address->phone,
                //'customer_note' => $data_address->order_notes,
                'shipping_method' => $method_price[0],
                'shipping_price' => $method_price[1]

            ]);
            // dd(Session::all());

            
            return redirect('/checkout-payment')->withInput();
        //exit;
    }

    public function checkout_payment()
    {
        if (!empty(Session::get('user_id'))) {
            $id = Session::get('user_id');
        }else{
            $id = Auth::user()->id;
        }

        $billing = DB::table('catalog_addresses')->where('type','=','billing')->where('id','=',Session::get('billing_id'))->first();
        $shipping = DB::table('catalog_addresses')->where('type','=','shipping')->where('id','=',Session::get('shipping_id'))->first();
        //$dataprice = CatalogOrder::where('order_number',Session::get('order_number'))->orderBy('created_at','DESC')->first();
        $detail = CatalogOrder::with(array('details','details.item','details.item.entry.medias', 'details.item.entry'))->where('order_number',Session::get('order_number'))->first(); 
        $data = \CatalogCart::getItems(array('item.entry.medias', 'item', 'item.entry'));
        $subtotal = 0;
        if(!empty(Session::get('state')) || !count($data)){
            //$data = CatalogOrderDetail::with(array('product','product.medias'))->where('order_id',$dataprice->id)->get();
            foreach ($detail->details as $val) {
                if ($shipping->country == 'Indonesia') {
                    $subtotal += $val->item->price_idr * $val->qty;    
                }else{
                    $subtotal += $val->item->price * $val->qty;
                }
            }
        }else{
            foreach ($data as $val) {
                if ($shipping->country == 'Indonesia') {
                    $subtotal += $val->item->price_idr * $val->qty;    
                }else{
                    $subtotal += $val->item->price * $val->qty;
                }
            }
        }
           
        $input = \Request::all();
        // dd(Session::all());exit;
        foreach ($input as $key => $value) {
            if(!empty($value)){
                Session::put([$key => $value]);
            }
        }

        if ($shipping->country == 'Indonesia') {
            $paymentMethod = 'credit card';
        }else{
            $paymentMethod = 'paypal';
        }

        $note = 'no note';
        if (!empty($shipping->order_notes)) {
            $note = $shipping->order_notes;
        }

        if (!count($detail)) {
            if (empty($shipping)) {
                $shipping = DB::table('catalog_addresses')->where('id','=',Session::get('shippingID'))->first();
            }
              $dat = translateCartToParsedData();
              $dat = \CatalogPromoContainer::process($dat);
              $dat['order']->shipping_amount = Session::get('shipping_price');
              $dat['order']->subtotal = $subtotal;
              //masukin promo
              $dat['order']->tax = 0;
              $dat['order']->customer_note = 'No Note';
              $dat['order']->status = 'Unpaid';
              $dat['order']->order_number = Session::get('order_number');

              foreach ($billing as $key => $value) {
                //dpr($key.''.$value);
                if ($key == 'postal_code') {
                    $key = 'postcode';
                }
                if ($key == 'address') {
                    $key = 'address_1';
                }
                if ($key == 'first_name') {
                    $key = 'firstname';
                }
                if ($key == 'last_name') {
                    $key = 'lastname';
                }
                if ($key == 'order_notes') {
                    $key = 'customer_note';
                }
                $dat['order']->{'payment'.'_'.$key} = $value;
              }
              foreach ($shipping as $key => $value) {
                if ($key == 'postal_code') {
                    $key = 'postcode';
                }
                if ($key == 'address') {
                    $key = 'address_1';
                }
                if ($key == 'first_name') {
                    $key = 'firstname';
                }
                if ($key == 'last_name') {
                    $key = 'lastname';
                }
                if ($key == 'order_notes') {
                    $key = 'customer_note';
                }
                $dat['order']->{'shipping'.'_'.$key} = $value;
              }
              $dat['order']->shipping_id = Session::get('shipping_id');
              $dat['order']->status = 'Unpaid';
              $dat['order']->payment_method = $paymentMethod;
              $dat['order']->shipping_method = 'DHL';
              $dat['order']->customer_note = $note;
              $dat['payment'] = (object)[
                    'bank' => $paymentMethod,
                    'type' => $paymentMethod,
                    'user_id' => $id,
                    'amount' => floatval($dat['order']->shipping_amount + $subtotal),
                    'user_account' => '',
                    'user_holder' => '',
                    'user_bank' => '',
                    'refnumber' => ''
              ];
              app()->bind('Alpha_Catalog_PaymentMethod','Morra\Catalog\Core\PaymentMethod\Bank');
              $res = \CatalogCheckout::process($dat);
              //Session::forget('shipp-amount');    
            $datas = DB::table('catalog_orders')->where('order_number',Session::get('order_number'))->first();

            $detail = CatalogOrder::with(array('details','details.item','details.item.entry.medias', 'details.item.entry'))->where('id',$datas->id)->first();
            $dataUser = User::find(Session::get('user_id'));
            $datemail = date('d F Y');

            $dataMessage ='';

            $total = $detail->grand_total;
            $jumlah_desimal ="0";
            $pemisah_desimal =",";
            $pemisah_ribuan =".";
            $currency = '';
            if (strtolower($shipping->country) == 'indonesia') {
                $currency = 'IDR';
            }else{
                $currency = '$';
            }
            //foreach ($detail as $key => $val) {
              
              foreach ($detail->details as $key => $value) {
                if (strtolower($shipping->country) == 'indonesia') {
                    $angka = $value->item->price_idr;
                }else{
                    $angka = $value->item->price;
                }
                $dataMessage .= '
                        <tr>
                            <td style="border-bottom: 1px solid #e6e7e8;padding-left:10px;" valign="middle" height="30" width="50%" style="width:50%">
                                <b>'.$value->item->item_title.'</b>
                            </td>
                            <td style="border-bottom: 1px solid #e6e7e8" valign="middle" height="30" align="right" style="width:25%">
                                <b>x '.$value->qty.'</b>
                            </td>
                            <td style="border-bottom: 1px solid #e6e7e8;padding-right:10px;" valign="middle" height="30" align="right" style="width:25%">
                                <b>'.$currency.'  '.number_format($angka, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).'</b>
                            </td>
                        </tr>
                    ';
                }
            //}
            $grand_total = $total + Session::get('shipping_price');
            $addressMail = '
                    <td>
                                                                    <b>Billing to</b><br>
                                                                    '.$billing->first_name.' '.$billing->last_name.'<br>
                                                                    '.$billing->address.'<br>
                                                                    '.$billing->district.'<br>
                                                                    '.$billing->city.' '.$billing->postal_code.'
                                                                </td>
                                                                <td>
                                                                    <b>Shipping to</b><br>
                                                                    '.$shipping->first_name.' '.$shipping->last_name.'<br>
                                                                    '.$shipping->address.'<br>
                                                                    '.$shipping->district.'<br>
                                                                    '.$shipping->city.' '.$shipping->postal_code.'
                                                                </td>
                ';

            $parseEmail = array(
                    "[[name]]" => $dataUser->username,
                    "[[data]]" => $dataMessage,
                    "[[order_number]]" => $datas->order_number,
                    "[[date]]" => $datemail,
                    "[[total]]" => $currency.number_format($total, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan),
                    "[[shipping]]" => $currency.Session::get('shipping_price'),
                    "[[grand_total]]" => $currency.number_format($grand_total, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan),
                    "[[address]]" => $addressMail,
                    "[[comfirm_url]]" => url('/account/order-detail/').'/'.$datas->id,
                    "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($dataUser->email),
                    "[[email]]" =>"careforyou@neneq.com"
                );   
            $emailMessages = parseEmail("order-confirmation",$parseEmail);

            $message = app('AlphaMailMessage')->setSubject('Order Confirmation')->setBody($emailMessages->msg,'text/html')->setFrom(array('careforyou@gmail.com'))->setTo(array($dataUser->email));
            $mailer = app('AlphaMailSender')->send($message);
        }

        $ids =[]; 
        $data_cart = \CatalogCart::getItems();
        foreach($data_cart as $v){
          $ids[]=$v->id;
        }

        DB::table('catalog_carts')->whereIn('id',$ids)->delete();
        // $data = CatalogOrder::with(array('details.item.entry.medias', 'details.item', 'details.item.entry'))->where('order_number',Session::get('order_number'))->first();
        $data = $detail->details;
        $this->layout->content = view('app::'.$this->template.'.page.checkout-payment',[
                'siteTitle' => 'Hi,Everyone!',
                'data' => $data,
                'dataprice' => $detail,
                'shipping' => $shipping,
                'billing' => $billing
                ]);
        $this->layout->menu = 'product';
        $this->layout->title = 'Payment | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function checkout_payment_save()
    {
        $input = \Request::all();
        $rules = array(
            'cb' => 'required', 
        );

        $Validator = \Validator::make($input,$rules);
        $messages = $Validator->errors();
    
        if ($Validator->fails()) {
            return redirect()->back()->with('errors',$messages);
        }

        if ($input['method'] == 'paypal') {
            return redirect('/paypal-payment');
        }
        if ($input['method'] == 'credit') {
            return redirect('/doku/payment');
        }
    }

    public function redirect_paypal() {
        $cart = \CatalogCart::getItems(array('item.entry.medias', 'item', 'item.entry'));
        $totalprice = 0;
        $subtotal = 0;
        $items = array();
        
        $billing = DB::table('catalog_addresses')->where('id','=',Session::get('billing_id'))->first();
        $shipping = DB::table('catalog_addresses')->where('id','=',Session::get('shipping_id'))->first();
        if (empty($shipping)) {
            $shipping = DB::table('catalog_addresses')->where('id','=',Session::get('shippingID'))->first();
        }

        //$id = 1;
        $dataUser = User::find(Session::get('user_id'));
        $datemail = date('d F Y');

        $data = DB::table('catalog_orders')->where('order_number',Session::get('order_number'))->first();
        
        $id =  $data->id;

        $date = date('Y-m-d h:i:s');
        $data = DB::table('catalog_orders')->where('order_number','=',Session::get('order_number'))->first();
        
        $update_status = DB::table('catalog_orders')->where('id','=',$data->id)->update(array('status' => 'Processing'));
        $data = DB::table('catalog_orders')->where('order_number',Session::get('order_number'))->first();
        $detail = DB::table('catalog_order_details')->where('order_id',$data->id)->get();
        $dataUser = User::find(Auth::user()->id);

        $dataMessage ='';

        $total = 0;
        foreach ($detail as $key => $value) {
            $total = $total + $value->final_amount;
            $dataMessage .= '
                <tr>
                    <td style="border-bottom: 1px solid #e6e7e8;padding-left:10px;" valign="middle" height="30" width="50%" style="width:50%">
                        <b>'.parseMultilang($value->product_title).'</b>
                    </td>
                    <td style="border-bottom: 1px solid #e6e7e8" valign="middle" height="30" align="right" style="width:25%">
                        <b>x '.$value->qty.'</b>
                    </td>
                    <td style="border-bottom: 1px solid #e6e7e8;padding-right:10px;" valign="middle" height="30" align="right" style="width:25%">
                        <b>'.$value->amount.'</b>
                    </td>
                </tr>
            ';
        }
        $grand_total = $total + Session::get('shipping_price');
        $addressMail = '
            <td>
                                                            <b>Billing to</b><br>
                                                            '.@$billing->first_name.' '.@$billing->last_name.'<br>
                                                            '.@$billing->address.'<br>
                                                            '.@$billing->district.'<br>
                                                            '.@$billing->city.' '.@$billing->postal_code.'
                                                        </td>
                                                        <td>
                                                            <b>Shipping to</b><br>
                                                            '.$shipping->first_name.' '.$shipping->last_name.'<br>
                                                            '.$shipping->address.'<br>
                                                            '.$shipping->district.'<br>
                                                            '.$shipping->city.' '.$shipping->postal_code.'
                                                        </td>
        ';
        $parseEmail = array(
            "[[name]]" => $dataUser->username,
            "[[data]]" => $dataMessage,
            "[[order_number]]" => $data->order_number,
            "[[date]]" => $datemail,
            "[[total]]" => $total,
            "[[shipping]]" => Session::get('shipping_price'),
            "[[grand_total]]" => $grand_total,
            "[[address]]" => $addressMail,
            "[[order_url]]" => url('/account/order-detail').'/'.$data->id,
            "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($dataUser->email)
        );   
        $emailMessages = parseEmail("payment-confirmation",$parseEmail);
        $subject = DB::table('entries')->where('entry_type','email_template')->where('slug','payment-confirmation')->first();
        // $emailMessages = parseEmail("payment-confirmation",$parseEmail);
        $message = app('AlphaMailMessage')->setSubject(parseMultilang($subject->title))->setBody($emailMessages->msg,'text/html')->setFrom(array('careforyou@neneq.com'))->setTo(array($dataUser->email));
        //$mailer = app('AlphaMailSender')->send($message);
        

        //$detail = DB::table('catalog_order_details')->where('order_id','=',$id)->get();
        //$dataMessage ='';

        // $total = 0;
        // foreach ($detail as $key => $value) {
        //     $total = $total + $value->final_amount;
        //     $angka = $value->final_amount;
        //     $jumlah_desimal =0;
        //     $pemisah_desimal =",";
        //     $pemisah_ribuan =".";
        //     $dataMessage .= '
        //         <tr>
        //             <td style="border-bottom: 1px solid #e6e7e8;padding-left:10px;" valign="middle" height="30" width="50%" style="width:50%">
        //                 <b>'.parseMultilang($value->product_title).'</b>
        //             </td>
        //             <td style="border-bottom: 1px solid #e6e7e8" valign="middle" height="30" align="right" style="width:25%">
        //                 <b>x '.$value->qty.'</b>
        //             </td>
        //             <td style="border-bottom: 1px solid #e6e7e8;padding-right:10px;" valign="middle" height="30" align="right" style="width:25%">
        //                 <b> $  '.number_format($angka, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).'</b>
        //             </td>
        //         </tr>
        //     ';
        // }
        // $grand_total = $total + Session::get('shipping_price');
        // $addressMail = '
        //     <td>
        //         <b>Billing to</b><br>
        //         '.$billing->first_name.' '.$billing->last_name.'<br>
        //         '.$billing->address.'<br>
        //         '.$billing->district.'<br>
        //         '.$billing->city.' '.$billing->postal_code.'
        //     </td>
        //     <td>
        //         <b>Shipping to</b><br>
        //         '.$shipping->first_name.' '.$shipping->last_name.'<br>
        //         '.$shipping->address.'<br>
        //         '.$shipping->district.'<br>
        //         '.$shipping->city.' '.$shipping->postal_code.'
        //     </td>
        // ';

        // $parseEmail = array(
        //     "[[name]]" => $dataUser->username,
        //     "[[data]]" => $dataMessage,
        //     "[[order_number]]" => $data->order_number,
        //     "[[date]]" => $datemail,
        //     "[[total]]" => number_format($total, 0, $pemisah_desimal, $pemisah_ribuan),
        //     "[[shipping]]" => Session::get('shipping_price'),
        //     "[[grand_total]]" => number_format($grand_total, 0, $pemisah_desimal, $pemisah_ribuan),
        //     "[[address]]" => $addressMail,
        //     "[[comfirm_url]]" => url('/checkout-confirm-page').'/'.$data->id,
        //     "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($dataUser->email)
        // );   
        // $emailMessages = parseEmail("order-confirmation",$parseEmail);
        
        // $message = app('AlphaMailMessage')->setSubject('Order Confirmation')->setBody($emailMessages->msg,'text/html')->setFrom(array('careforyou@neneq.com'))->setTo(array($dataUser->email));
        
        foreach ($cart as $val) {
            $subtotal += $val->item->price * $val->qty;
        }
        $totalprice = floatval($subtotal) + floatval(Session::get('shipping_price'));
        // dd(floatval(Session::get('shipping_price')));exit();
        foreach ($cart as $data){
            //set paypal summary 
            $item = new \PayPal\Api\Item();
            $item->setQuantity($data->qty);
            $item->setName(ucwords( $data->item->item_title));
            $item->setPrice($data->item->price);
            $item->setCurrency("USD");
            //$item->setSku("aa");
            array_push($items, $item);
        }
            // dd($item);

        if (Session::has('coupon'))
        {
            $coupon = Session::get('coupon');
            $option = array();
            $option["total"] =  $totalprice;
            $option["checkDiscount"] =  true;
            $arr = checkCoupon($coupon, $option);
            $totalprice -= $arr["discount"];
        }

        // bypass jika gratis
        if ($totalprice <= 0 and !empty($cart)) {
            return redirect("/checkout-payment");
        }
        else {
            $sdkConfig = array(
                "mode" => "sandbox"
            );
            //$oauthCredential = new \PayPal\Auth\OAuthTokenCredential("AafpfxC9J34-6pO381nZLI_cfH8CInu1TcSUosEHR3Gqjp5dqN2GOTVlERZg","EGqOahCa-FfQ1GWHp5jLaSAY7Q4yeTo6zYaxNGpZBKVnQvjUGaEsdlbreWcQ");
            // $oauthCredential = new \PayPal\Auth\OAuthTokenCredential("AZyu7pERdk0fVGkoyv6g8vLE1jJf9DQ33PR-wQC3szxfAKTFne_az_wFKh4qlsx0FHe12BiVzmWfoIAo","EMb0zNKLpLAI-c2top1TBEzQlxk5e3PZUB7dVOPUizvfz2kwo-dQb12V4P2f2cMQim_IZhwpCkTKFrDx");
            //sandbox account 
            $oauthCredential = new \PayPal\Auth\OAuthTokenCredential("AR_b0PPTk-74Pg3s_C-oh1OitPRP-8RorA69G5Zs-oLCGFkQ7r22P4wOaEqp3-FrSwpkt_5zBssCpMzg","ENaStvDllwZRCXXbrr1zUVOFqJpJTSo2NxWGzM7LDEr6cSspmaUXDAEnj3TI1UJgbO9cIma1bSw3UyxU");

    //      $accessToken = $oauthCredential->getAccessToken($sdkConfig);
           
            $apiContext = new \PayPal\Rest\ApiContext($oauthCredential);
            $apiContext->setConfig($sdkConfig);

            $payer = new \PayPal\Api\Payer;
            $payer->setPaymentMethod("paypal");

            $details = new \PayPal\Api\Details();
            $details->setSubtotal($subtotal);
            $details->setShipping(Session::get('shipping_price'));

            $amount = new \PayPal\Api\Amount();
            $amount->setCurrency("USD");
            $amount->setTotal($totalprice);
            $amount->setDetails($details);
            // dd(Session::get('shipping_amount'));exit;
            //set summary -- edit by septian

            $item_list = new \PayPal\Api\ItemList();
            $item_list->setItems($items);

            $transaction = new \PayPal\Api\Transaction();
            $transaction->setDescription("GTrove Payment via Paypal");
            $transaction->setAmount($amount);
            $transaction->setItemList($item_list);

            //end set summary -- edit by septian

            $redirectUrls = new \PayPal\Api\RedirectUrls();
            $redirectUrls->setReturnUrl(URL::to("/checkout-payment-success"));
            $redirectUrls->setCancelUrl(URL::to("/checkout-payment"));

            $payment = new \PayPal\Api\Payment();
            $payment->setIntent("sale");
            $payment->setPayer($payer);
            $payment->setRedirectUrls($redirectUrls);
            $payment->setTransactions(array($transaction));
            // dd($payment);exit;
            try {
                $payment->create($apiContext);
                // save payment ID to session
                Session::put("paypal_payment_id", $payment->getId());

                foreach ($payment->getLinks() as $v)
                    if ($v->getRel() == "approval_url")
                        //$mailer = app('AlphaMailSender')->send($message);
                        return redirect($v->getHref());

            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                header("Content-type: application/json");
                // dd($ex->getData());
                // echo $ex->getData();

                $messages->setError("Paypal Error");
                return Redirect()->back()->with(array('messages' => $messages->messages, 'message' => $message));
            }
        }
    }

    public function redirect_paypal_success()
    {
        $input = \Request::all();

        $insert = DB::table('paypal')->insert(array(
            'id_user' => Auth::user()->id, 
            'order_number' => Session::get('order_number'),
            'payment_id' => $input['paymentId'],
            'token' => $input['token'],
            'payer_id' => $input['PayerID'],
            )); 
        $update_status = DB::table('catalog_orders')->where('order_number','=',Session::get('order_number'))->update(array('status' => 'Pending'));
        $ids =[]; 
        $data = \CatalogCart::getItems();
        foreach($data as $v){
          $ids[]=$v->id;
        }
        $mailer = app('AlphaMailSender')->send($message);
        DB::table('catalog_carts')->whereIn('id',$ids)->delete();
        Session::put('state','Success');
        return redirect('/checkout-payment')->with('state', 'success');
    }


    public function checkout_non_member()
    {
        $input = \Request::all();
        //dd($input);
        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
        );

        $Validator = \Validator::make($input,$rules);
        $messages = $Validator->errors();
        // dd($messages->first('email'));
        if ($Validator->fails()) {
            return redirect()->back()->with('errors',$messages)->withInput();
        }

        $check = User::whereEmail($input['email'])->first();
        if (count($check)) {
            return redirect()->back()->with('msg','alreadyregistered')->withInput();
        }

        Session::put(array(
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname'],
            'email' => $input['email'],
        ));
        $country = DB::table('countries')->get();
        $data = \CatalogCart::getItems(array('item.entry.medias', 'item', 'item.entry'));
        // $jne_city = \CatalogShippingMethod::getCity(@$data[0]->city);
        // $jne_dist = \CatalogShippingMethod::getDistrict(@$data[0]->city,@$data[0]->district);
        $this->layout->content = view('app::'.$this->template.'.page.checkout-shipping-non-member',[
            'siteTitle' => 'Hi,Everyone!',
            'data' => $data,
            'country' => $country,
            ]);
        $this->layout->menu = '';
        $this->layout->title = 'Shipping non member| '.app('AlphaSetting')->getSetting('site_name');
    }

    public function proses_checkout_non_member()
    {
        $input = \Request::all();//dd($input);
        // dd($_GET['ship-status']);exit;
        $inputaddress = \Request::except('ship-status','password','repassword','_token','useaddress','submit', 'receiver_email', 'receiver_name', 'receiver_address', 'receiver_country', 'receiver_city', 'receiver_state', 'receiver_company', 'receiver_apartemen', 'receiver_postal_code','receiver_phone','receiver_first_name','receiver_last_name','receiver_district','receiver_order_notes', 'jne');
        $inputship = \Request::only('receiver_first_name','email', 'receiver_address', 'receiver_country', 'receiver_city', 'receiver_state', 'receiver_postal_code','receiver_phone','receiver_district');
        //dd($input);
        
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'country' => 'required',
            
            'address' => 'required',
            'state' => 'required',
            'district' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postal_code' => 'required',
            'phone' => 'required',
            
        );

        if ($input['ship-status'] != '') {
            $rules1 = array(
                'receiver_first_name' => 'required',
                'receiver_address' => 'required',
                'receiver_country' => 'required',
                'receiver_state' => 'required',
                'receiver_district' => 'required',
                'receiver_city' => 'required',
                'receiver_state' => 'required',
                'receiver_postal_code' => 'required',
                'receiver_phone' => 'required',
            );
            $merged_array = array_merge($rules,$rules1);
            // dd($merged_array);
        }else{
            $merged_array = $rules;
        }

        $Validator = \Validator::make($inputaddress,$rules);
        $messages = $Validator->errors();

        if ($Validator->fails()) {
            //dd($messages);exit;
            return redirect()->back()->with('errors',$messages)->withInput(\Request::all());
        }
        
        unset($input['_token']);
            $cek = User::find(Session::get('firstname').' '.Session::get('lastname'));
            if (!empty($cek)) {
                return redirect()->back()->with('errors','Username Already Registered')->withInput(\Request::all());       
            }
            $user = new User;
            $user->username = Session::get('firstname').' '.Session::get('lastname');
            $user->email = Session::get('email');
            $user->password = bcrypt(Session::get('email'));
            $user->touch();
            $user->save();

            //user metas
            $user->saveUserMetaFromInput($this->metas,$inputaddress);
            //role
            $user->roles()->attach('Registered');

            $userid = \User::where('email','=',Session::get('email'))->first();
            //dd($userid);
            $addresses_id = DB::table('catalog_addresses')->insertGetId(['type' => 'billing']);
            if ($input['ship-status'] == '') {
                foreach ($inputaddress as $key => $value) {
                    $addresses = DB::table('catalog_addresses')->where('id','=',$addresses_id)->update(array($key => $value, 'user_id' => $userid->id, 'label' => 'home'));
                    //dpr($key.' => '.$value.' => '.$addresses_id);
                }
                $addresses_ids = DB::table('catalog_addresses')->insertGetId(['type' => 'shipping']);
                foreach ($inputaddress as $key => $value) {
                    $addresses_s = DB::table('catalog_addresses')->where('id','=',$addresses_ids)->update(array($key => $value, 'user_id' => $userid->id, 'label' => 'home'));
                }
            }else{
                foreach ($inputaddress as $key => $value) {
                    $addresses = DB::table('catalog_addresses')->where('id','=',$addresses_id)->update(array($key => $value,'user_id' => $userid->id, 'label' => 'home'));
                    //dpr($key.' => '.$value.' => '.$addresses_id);
                    //dpr($addresses);
                }
                $addresses_ids = DB::table('catalog_addresses')->insertGetId(['type' => 'shipping']);
                foreach ($inputship as $key => $value) {
                    if ($key == 'email') {
                        $k = $key;
                    }else{
                        $k = substr($key,9);
                    }
                    $addresses = DB::table('catalog_addresses')->where('id','=',$addresses_ids)->update(array($k => $value,'user_id' => $userid->id, 'label' => 'home'));
                    //dpr($key.' => '.$value.' => '.$addresses_id);
                }
            }
            $data_address = DB::table('catalog_addresses')->where('user_id','=',$userid->id)->where('type','=','shipping')->first();
            $data_login = User::where('id','=',$userid->id)->first();
            
            // $method = substr($input['jne'], 0,-7);
            // $method_price = substr($input['jne'], 9);
            $method = explode('-', $input['jne']);
            //exit;
            $order_number = getOrderNumber(6);
            $insert = Session::put([
                'shipping_id' => $data_address->id,
                'user_id' => $userid->id,
                'order_number' => $order_number,
                'shipping_method' => $method[0],
                'shipping_price' => $method[1],
                'billing_id' => $addresses_id,
                'shippingID' => $addresses_ids,

            ]);
            // dd(Session::all());
            Auth::attempt(['email' => $data_login->email, 'password' => $data_login->password]);
            Session::put('currency',$input['country']);
            
            return redirect('/checkout-payment')->withInput();
    }

    public function checkout_confirm_page($id='')
    {
        if (!empty($id)) {
            $data = DB::table('catalog_orders')->where('user_id','=',Auth::user()->id)->where('order_number','=',$id)->get();
        }else{
            $data = DB::table('catalog_orders')->where('user_id','=',Auth::user()->id)->orderBy('created_at','DESC')->get();
        }
        
        $this->layout->content = view('app::'.$this->template.'.page.payment-confirmation',[
            'siteTitle' => 'Hi,Everyone!',
            'data' => $data,
            ]);
        $this->layout->menu = 'product';
        $this->layout->title = 'Confirm Page'.app('AlphaSetting')->getSetting('site_name');
    }

    public function doku_confirmation($id=''){
        dd('fuck');

    }

    public function checkout_confirm()
    {
        $input = \Request::all();
        $rules = array(
            'bank-name' => 'required',
            'bank-account-name' => 'required',
            'bank-number' => 'required',
            'amount' => 'required',
            'ref-number' => 'required',
            'date-trans' => 'required',
            'month-trans' => 'required',
            'year-trans' => 'required',
        );

        $Validator = \Validator::make($input,$rules);
        $messages = $Validator->errors();

        if ($Validator->fails()) {
            //dd($messages);exit;
            return redirect()->back()->with(array('errors' => $messages, 'msg' => 'fail'))->withInput(\Request::all());
        }
        //checkout
        
        $date = date('Y-m-d h:i:s');
        $data = DB::table('catalog_orders')->where('order_number','=',$input['order_number'])->first();
        $update = DB::table('catalog_payment_banktransfer')->where('order_id','=',$data->id)->update(array('bank' => $input['bank-name'], 'type' => 'bank', 'user_account' => $input['bank-account-name'], 'user_holder' => $input['bank-account-name'], 'user_bank' => $input['bank-account-name'], 'refnumber' => $input['ref-number'], 'transfer_date' => $input['year-trans'].'-'.$input['month-trans'].'-'.$input['date-trans'], 'created_at' => $date, 'updated_at' => $date));
        $update_status = DB::table('catalog_orders')->where('id','=',$data->id)->update(array('status' => 'Processing'));
        $data = DB::table('catalog_orders')->where('order_number',$input['order_number'])->first();
        $detail = DB::table('catalog_order_details')->where('order_id',$data->id)->get();
        $dataUser = User::find(Auth::user()->id);

        $billing = DB::table('catalog_addresses')->where('user_id','=',$dataUser->id)->where('type','billing')->first();
        $shipping = DB::table('catalog_addresses')->where('id','=',$data->shipping_id)->first();
        $datemail = date('d F Y');
        $dataMessage ='';

        $total = 0;
        foreach ($detail as $key => $value) {
            $total = $total + $value->final_amount;
            $dataMessage .= '
                <tr>
                    <td style="border-bottom: 1px solid #e6e7e8;padding-left:10px;" valign="middle" height="30" width="50%" style="width:50%">
                        <b>'.parseMultilang($value->product_title).'</b>
                    </td>
                    <td style="border-bottom: 1px solid #e6e7e8" valign="middle" height="30" align="right" style="width:25%">
                        <b>x '.$value->qty.'</b>
                    </td>
                    <td style="border-bottom: 1px solid #e6e7e8;padding-right:10px;" valign="middle" height="30" align="right" style="width:25%">
                        <b>'.$value->amount.'</b>
                    </td>
                </tr>
            ';
        }
        $grand_total = $total + Session::get('shipping_price');
        $addressMail = '
            <td>
                                                            <b>Billing to</b><br>
                                                            '.@$billing->first_name.' '.@$billing->last_name.'<br>
                                                            '.@$billing->address.'<br>
                                                            '.@$billing->district.'<br>
                                                            '.@$billing->city.' '.@$billing->postal_code.'
                                                        </td>
                                                        <td>
                                                            <b>Shipping to</b><br>
                                                            '.$shipping->first_name.' '.$shipping->last_name.'<br>
                                                            '.$shipping->address.'<br>
                                                            '.$shipping->district.'<br>
                                                            '.$shipping->city.' '.$shipping->postal_code.'
                                                        </td>
        ';
        $parseEmail = array(
            "[[name]]" => $dataUser->username,
            "[[data]]" => $dataMessage,
            "[[order_number]]" => $data->order_number,
            "[[date]]" => $datemail,
            "[[total]]" => $total,
            "[[shipping]]" => Session::get('shipping_price'),
            "[[grand_total]]" => $grand_total,
            "[[address]]" => $addressMail,
            "[[order_url]]" => url('/account/order-detail').'/'.$data->id,
            "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($dataUser->email)
        );   
        $emailMessages = parseEmail("payment-confirmation",$parseEmail);
        $subject = DB::table('entries')->where('entry_type','email_template')->where('slug','payment-confirmation')->first();
        // $emailMessages = parseEmail("payment-confirmation",$parseEmail);
        $message = app('AlphaMailMessage')->setSubject(parseMultilang($subject->title))->setBody($emailMessages->msg,'text/html')->setFrom(array('careforyou@neneq.com'))->setTo(array($dataUser->email));
        $mailer = app('AlphaMailSender')->send($message);
        if ($mailer) {
          return redirect()->back()->with('confirm','confirmed');   
        }
    }
    
    public function thank_you()
    {
        $ids =[];   
        $data = \CatalogCart::getItems();
        foreach($data as $v){
            $ids[]=$v->id;
        }
        DB::table('catalog_carts')->whereIn('id',$ids)->delete();
        return redirect('/checkout-payment')->with('state', 'success');
    }

    public function dhl()
    {
        $input = \Request::all();
        $add = $input['data'];
        $weight = 0;
        $dt = date('Y-m-d');
        $tm = date('h:i:s');
        $hr = date('P');
        $msgr = date('Ymdhisusiu');
        $id = Auth::User()->id;
        
        $weight_data = \CatalogCart::getItems(array('item.entry.medias', 'item', 'item.entry'));
        if (empty($weight_data[0])) {
            $w_data = CatalogOrder::with(array('details','details.item','details.item.metas'))->where('order_number',Session::get('order_number_old'))->first();
        }
        
        $datas = DB::table('catalog_addresses')->where('user_id','=',$id)->where('type','=','shipping')->where('label','=',$add)->first();
        $countrycode = DB::table('countries')->where('name',$datas->country)->first();
        $code = $countrycode->code;
        // DHL Settings
        $config['dhl'] = array(
            'id' => 'cvcantik',
            'pass' => 'CZupT6vHLg',
        );
        $dhl = $config['dhl'];

        // Test a getQuote using DHL XML API
        $dhldata = new GetQuote();
        $dhldata->SiteID = $dhl['id'];
        $dhldata->Password = $dhl['pass'];

        // Set values of the request
        $dhldata->MessageTime = $dt.'T'.$tm.$hr;
        $dhldata->MessageReference = $msgr;
        $dhldata->BkgDetails->Date = date('Y-m-d');
        $pid = 1;
        $tprice = 0;
        //dd($w_data);
        if (empty($weight_data[0])) {
            foreach($w_data->details as $detail){   
                if ($datas->country == 'Indonesia') {
                    $tprice = $detail->qty * $detail->item->price_idr;
                }else{
                    $tprice = $detail->qty * $detail->item->price;
                }
                $piece = new PieceType();
                
                $piece->PieceID = $pid;
                $piece->Height = getEntryMetaFromArray($detail->item->metas, 'height');
                $piece->Depth = getEntryMetaFromArray($detail->item->metas, 'depth');
                $piece->Width = getEntryMetaFromArray($detail->item->metas, 'width');
                $piece->Weight = getEntryMetaFromArray($detail->item->metas, 'weight');
                $pid = $pid+1; 
            }
        }else{
            foreach ($weight_data as $key => $value) {
                if ($datas->country == 'Indonesia') {
                    $tprice = $value->qty * $value->item->price_idr;
                }else{
                    $tprice = $value->qty * $value->item->price;
                }
                $get_weight = DB::table('entrymetas')->where('entry_id','=',$value->item->entry->id)->get();
                $piece = new PieceType();
                $piece->PieceID = $pid;
                $piece->Height = getEntryMetaFromArray($get_weight, 'height');
                $piece->Depth = getEntryMetaFromArray($get_weight, 'depth');
                $piece->Width = getEntryMetaFromArray($get_weight, 'width');
                $piece->Weight = getEntryMetaFromArray($get_weight, 'weight');
                $pid = $pid+1; 
            }
        }

        $dhldata->BkgDetails->addPiece($piece);
        $dhldata->BkgDetails->IsDutiable = 'N';
        $dhldata->BkgDetails->ReadyTime = 'PT'.date("H", strtotime("+2 hours")).'H'.date('i').'M';
        $dhldata->BkgDetails->ReadyTimeGMTOffset = date('P');
        $dhldata->BkgDetails->DimensionUnit = 'CM';
        $dhldata->BkgDetails->WeightUnit = 'KG';
        // $dhldata->BkgDetails->PaymentCountryCode = $countrycode->country_code;
        $dhldata->BkgDetails->PaymentCountryCode = $code;
        if ($datas->country == 'Indonesia') {
            $dhldata->BkgDetails->QtdShp->LocalProductCode = 'N';
            $dhldata->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'AA';
            $dhldata->BkgDetails->PaymentAccountNumber = "540427101";
        }else{
            $dhldata->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'AA';
        }
        // $dhldata->BkgDetails->IsDutiable = 'Y';

        // Request Paperless trade

        $dhldata->From->CountryCode = app('AlphaSetting')->getSetting('country_code');
        $dhldata->From->Postalcode = app('AlphaSetting')->getSetting('postal_code');
        $dhldata->From->City = app('AlphaSetting')->getSetting('city');

        $dhldata->To->CountryCode = $code;
        // $dhldata->To->Postalcode = $datas->postal_code;
        $dhldata->To->City = $datas->city;
        // $dhldata->Dutiable->DeclaredValue = (float)$tprice;
        // $dhldata->Dutiable->DeclaredCurrency = 'USD';

        // Call DHL XML API
        $start = microtime(true);
        // dd($dhldata->toXML());
        // echo $dhldata->toXML();
        $client = new WebserviceClient('production');
        $xml = $client->call($dhldata);
        $data = simplexml_load_string($xml);
        // dd($data);

        $address = \Request::all();
        $add = $address['data'];
        $id = Auth::user()->id;
        
        $currencyCode = DB::table('currency_code')->where('iso','LIKE','%'.$code.'%')->first();
        // dd($currencyCode->iso);
        $ship='';
        $shipingPrice = 0;
        $totalPrice = 0;
        $total = 0;
        $title = '';
        foreach ($data as $value) {
            foreach ($value->BkgDetails as $val) {
                
                // dd($val->QtdShp->QtdSInAdCur);
                foreach ($val->QtdShp as $v) {
                    if ($v->ProductShortName == $title) {
                        
                    }else{
                        $title = $v->ProductShortName;
                        
                        foreach ($val->QtdShp->QtdSInAdCur as $va) {
                            $shipingPrice = $va->WeightCharge;
                            $totalPrice = (float)$va->TotalAmount;
                            $total = $totalPrice;
                            // - $shipingPrice
                            $code = $va->CurrencyCode;
                            // || $code == 'IDR'
                            if ($code == 'USD' || $code == 'IDR') {
                                $ship .= '<option data-label="dhl" data-type="dhl" data-id="" value="'.$title.' - '.(float)$total.'">'.$title.' - '.$code.' '.(float)$total.' '.'</option>';
                            }
                        }
                    }                
                }
            }
        }
        Session::put('currency',$datas->country);
        // dd($cek);

        $this->layout->menu = 'product';
        return response()->json(array(
            'datas' => $datas,
            'jne_select' => $ship,
            'status' => 1,
        ));
        // echo PHP_EOL . 'Executed in ' . (microtime(true) - $start) . ' seconds.' . PHP_EOL;
        // echo $xml . PHP_EOL;
    }

    public function dhl_non_member()
    {
        $input = \Request::all();
        //$add = $input['data'];
        $weight = 0;
        $dt = date('Y-m-d');
        $tm = date('h:i:s');
        $hr = date('P');
        $msgr = date('Ymdhisusiu');
        //$id = Auth::User()->id;
        $weight_data = \CatalogCart::getItems(array('item.entry.medias', 'item', 'item.entry'));
        if (empty($weight_data[0])) {
            $w_data = CatalogOrder::with(array('details','details.item','details.item.metas'))->where('order_number',Session::get('order_number'))->first();
        }
        
        $countrycode = DB::table('countries')->where('name',$input['country'])->first();
        // DHL Settings
        $config['dhl'] = array(
            'id' => 'cvcantik',
            'pass' => 'CZupT6vHLg',
        );
        $dhl = $config['dhl'];

        // Test a getQuote using DHL XML API
        $dhldata = new GetQuote();
        $dhldata->SiteID = $dhl['id'];
        $dhldata->Password = $dhl['pass'];

        // Set values of the request
        $dhldata->MessageTime = $dt.'T'.$tm.$hr;
        $dhldata->MessageReference = $msgr;
        $dhldata->BkgDetails->Date = date('Y-m-d');
        $pid = 1;
        $tprice = 0;

        if (empty($weight_data[0])) {
            foreach($w_data->details as $detail){   
                if ($datas->country == 'Indonesia') {
                    $tprice = $detail->qty * $detail->item->price_idr;
                }else{
                    $tprice = $detail->qty * $detail->item->price;
                }
                $piece = new PieceType();
                
                $piece->PieceID = $pid;
                $piece->Height = getEntryMetaFromArray($detail->item->metas, 'height');
                $piece->Depth = getEntryMetaFromArray($detail->item->metas, 'depth');
                $piece->Width = getEntryMetaFromArray($detail->item->metas, 'width');
                $piece->Weight = getEntryMetaFromArray($detail->item->metas, 'weight');
                $pid = $pid+1; 
            }
        }else{
            foreach ($weight_data as $key => $value) {
                $tprice = $value->qty * $value->item->price;
                //dd($value->item->entry->id);
                $get_weight = DB::table('entrymetas')->where('entry_id','=',$value->item->entry->id)->get();
                $piece = new PieceType();
                $piece->PieceID = $pid;
                $piece->Height = getEntryMetaFromArray($get_weight, 'height');
                $piece->Depth = getEntryMetaFromArray($get_weight, 'depth');
                $piece->Width = getEntryMetaFromArray($get_weight, 'width');
                $piece->Weight = getEntryMetaFromArray($get_weight, 'weight');
                $pid = $pid+1; 
            }
        }
        
        $dhldata->BkgDetails->addPiece($piece);
        $dhldata->BkgDetails->IsDutiable = 'N';
        // $dhldata->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'AA';
        $dhldata->BkgDetails->ReadyTime = 'PT'.date("H", strtotime("+2 hours")).'H'.date('i').'M';
        $dhldata->BkgDetails->ReadyTimeGMTOffset = date('P');
        $dhldata->BkgDetails->DimensionUnit = 'CM';
        $dhldata->BkgDetails->WeightUnit = 'KG';
        $dhldata->BkgDetails->PaymentCountryCode = $countrycode->code;
        // $dhldata->BkgDetails->PaymentAccountNumber = "540427101";
        // $dhldata->BkgDetails->IsDutiable = 'Y';

        // Request Paperless trade
        if ($input['country'] == 'Indonesia') {
            $dhldata->BkgDetails->QtdShp->LocalProductCode = 'N';
            $dhldata->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'AA';
            // $dhldata->BkgDetails->PaymentAccountNumber = "540427101";
        }else{
            $dhldata->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'AA';
        }
        // $dhldata->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'WY';

        $dhldata->From->CountryCode = app('AlphaSetting')->getSetting('country_code');
        $dhldata->From->Postalcode = app('AlphaSetting')->getSetting('postal_code');
        $dhldata->From->City = app('AlphaSetting')->getSetting('city');

        $dhldata->To->CountryCode = $countrycode->code;
        // $dhldata->To->Postalcode = $input['postal'];
        $dhldata->To->City = $input['city'];

        // Call DHL XML API
        $start = microtime(true);
        // echo $dhldata->toXML();exit;
        $client = new WebserviceClient('production');
        $xml = $client->call($dhldata);
        $data = simplexml_load_string($xml);
        // dd($data);exit; 
        
        $ship='';
        $shipingPrice = 0;
        $totalPrice = 0;
        $total = 0;
        $title = '';
        $currencyCode = DB::table('currency_code')->where('iso','LIKE','%'.$countrycode->code.'%')->first();
        foreach ($data as $value) {
            foreach ($value->BkgDetails as $val) {
                
                // dd($val->QtdShp->QtdSInAdCur);
                foreach ($val->QtdShp as $v) {
                    if ($v->ProductShortName == $title) {
                        
                    }else{
                        $title = $v->ProductShortName;
                        
                        foreach ($val->QtdShp->QtdSInAdCur as $va) {
                            $shipingPrice = $va->WeightCharge;
                            $totalPrice = (float)$va->TotalAmount;
                            $code = $va->CurrencyCode;
                            if ($code == 'USD' || $code == 'IDR') {
                                $total = $totalPrice;
                                if ($total != 0) {
                                    $ship .= '<option data-label="dhl" data-type="dhl" data-id="" value="'.$title.' - '.(float)$total.'">'.$title.' - '.$code.' '.(float)$total.' '.'</option>';
                                }
                            }
                        }
                    }                
                }
            }
        }
        // dd($ship);
        $this->layout->menu = 'product';
        return response()->json(array(
            //'datas' => $datas,
            'jne_select' => $ship,
            'shipping' => $total,
            'status' => 1,
        ));
        // echo PHP_EOL . 'Executed in ' . (microtime(true) - $start) . ' seconds.' . PHP_EOL;
        // echo $xml . PHP_EOL;
    }

   // public function redirect_bank()
//     {
//         $input = \Request::all();
//         //checkout
        
        
//           Session::forget('cb');
//           $data = translateCartToParsedData();
//           $data_jne = substr(Session::get('shipping_method'), 0,3);
//           $data_jneType = substr(Session::get('shipping_method'), 4,3);
//           $data_method = Session::get('method');
//           $billing = DB::table('catalog_addresses')->where('id','=',Session::get('billing_id'))->first();
//           $shipping = DB::table('catalog_addresses')->where('id','=',Session::get('shipping_id'))->first();
//           if (empty($shipping)) {
//             $shipping = DB::table('catalog_addresses')->where('id','=',Session::get('shippingID'))->first();
//           }

//           $note = 'no note';
//           if (!empty($shipping->order_notes)) {
//               $note = $shipping->order_notes;
//           }
//           $data['order']->shipping_amount = Session::get('shipping_price');
//           //masukin promo
//           $data = \CatalogPromoContainer::process($data);

//           $data['order']->tax = 0;
//           $data['order']->customer_note = $note;
//           $data['order']->status = 'Unpaid';
//           $data['order']->shipping_method = $data_jne;
//           $data['order']->carrier_id = 1;
//           $data['order']->carrier_name = ucfirst($data_jne);
//           $data['order']->carrier_level = ucfirst($data_jneType);
//           $data['order']->payment_method = $data_method;
//           $data['order']->order_number = Session::get('order_number');

//           if(!empty($billing)){
//               foreach ($billing as $key => $value) {
//                 //dpr($key.''.$value);
//                 if ($key == 'postal_code') {
//                     $key = 'postcode';
//                 }
//                 if ($key == 'address') {
//                     $key = 'address_1';
//                 }
//                 if ($key == 'first_name') {
//                     $key = 'firstname';
//                 }
//                 if ($key == 'last_name') {
//                     $key = 'lastname';
//                 }
//                 if ($key == 'order_notes') {
//                     $key = 'customer_note';
//                 }
//                 $data['order']->{'payment'.'_'.$key} = $value;
//               }            
//           }

//           foreach ($shipping as $key => $value) {
//             if ($key == 'postal_code') {
//                 $key = 'postcode';
//             }
//             if ($key == 'address') {
//                 $key = 'address_1';
//             }
//             if ($key == 'first_name') {
//                 $key = 'firstname';
//             }
//             if ($key == 'last_name') {
//                 $key = 'lastname';
//             }
//             if ($key == 'order_notes') {
//                 $key = 'customer_note';
//             }
//             $data['order']->{'shipping'.'_'.$key} = $value;
//           }
//           $data['order']->shipping_id = Session::get('shipping_id');
//           $data['order']->status = 'Unpaid';
//           $data['payment'] = (object)[
//                 'bank' => Session::get('bank-acc'),
//                 'type' => Session::get('method'),
//                 'user_id' => Session::get('user_id'),
//                 'amount' => $data['order']->shipping_amount + $data['order']->subtotal,
//                 'user_account' => Session::get('bank-an'),
//                 'user_holder' => '',
//                 'user_bank' => '',
//                 'refnumber' => ''
//           ];
//           app()->bind('Alpha_Catalog_PaymentMethod','Morra\Catalog\Core\PaymentMethod\Bank');
//           $res = \CatalogCheckout::process($data);
//           Session::forget('shipp-amount');
//           $data = DB::table('catalog_orders')->where('order_number',Session::get('order_number'))->first();
//           $detail = DB::table('catalog_order_details')->where('order_id',$data->id)->get();
//           $dataUser = User::find(Session::get('user_id'));
//           $datemail = date('d F Y');
//           $dataMessage ='';

//           $total = 0;
//           foreach ($detail as $key => $value) {
//             $total = $total + $value->final_amount;

//             $angka = $value->amount;
//             $jumlah_desimal ="0";
//             $pemisah_desimal =",";
//             $pemisah_ribuan =".";
             
            
// //hasil : Rp 9.500.000

//             $dataMessage .= '
//                 <tr>
//                     <td style="border-bottom: 1px solid #e6e7e8;padding-left:10px;" valign="middle" height="30" width="50%" style="width:50%">
//                         <b>'.parseMultilang($value->product_title).'</b>
//                     </td>
//                     <td style="border-bottom: 1px solid #e6e7e8" valign="middle" height="30" align="right" style="width:25%">
//                         <b>x '.$value->qty.'</b>
//                     </td>
//                     <td style="border-bottom: 1px solid #e6e7e8;padding-right:10px;" valign="middle" height="30" align="right" style="width:25%">
//                         <b> $  '.number_format($angka, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).'</b>
//                     </td>
//                 </tr>
//             ';
//         }
//         $grand_total = $total + Session::get('shipping_price');     
//         $addressMail = '
//             <td>
//                                                             <b>Billing to</b><br>
//                                                             '.@$billing->first_name.' '.@$billing->last_name.'<br>
//                                                             '.@$billing->address.'<br>
//                                                             '.@$billing->district.'<br>
//                                                             '.@$billing->city.' '.@$billing->postal_code.'
//                                                         </td>
//                                                         <td>
//                                                             <b>Shipping to</b><br>
//                                                             '.$shipping->first_name.' '.$shipping->last_name.'<br>
//                                                             '.$shipping->address.'<br>
//                                                             '.$shipping->district.'<br>
//                                                             '.$shipping->city.' '.$shipping->postal_code.'
//                                                         </td>
//         ';
//         $parse = array(
//             "[[name]]" => $dataUser->username,
//             "[[data]]" => $dataMessage,
//             "[[order_number]]" => $data->order_number,
//             "[[date]]" => $datemail,
//              "[[total]]" => $total,
//              "[[shipping]]" => Session::get('shipping_price'),
//              "[[grand_total]]" => $grand_total,
//              "[[address]]" => $addressMail,
//              "[[comfirm_url]]" => url('/checkout-confirm-page').'/'.Session::get('order_number'),
//              "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($dataUser->email),
//              "[[email]]" =>"careforyou@neneq.com"
//         );   
//         //$emailMessages = parseEmail("order-confirmation",$parseEmail);
//         $emailMessages = parsemailtem('order-confirm',$parse);
//        // dd($emailMessages->view);
//         $subject = DB::table('entries')->where('entry_type','email_template')->where('slug','order-confirm')->first();
//         $message = app('AlphaMailMessage')->setSubject(parseMultilang($subject->title))->setBody($emailMessages->view,'text/html')->setFrom(array('careforyou@neneq.com'))->setTo(array($dataUser->email));
//         $mailer = app('AlphaMailSender')->send($message);
//         $ids =[]; 
//           $data = \CatalogCart::getItems();
//           foreach($data as $v){
//             $ids[]=$v->id;
//           }
//           DB::table('catalog_carts')->whereIn('id',$ids)->delete();
//           if ($mailer) {
//               return redirect('/checkout-payment')->with('state', 'success');     
//           }
        
//     }
}
