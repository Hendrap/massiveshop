<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\CoreController as BaseController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use CatalogAddress;
use CatalogCheckout;
use CatalogOrder;
use Auth;
use DB;
use Session;
use Log;

class CheckoutController extends BaseController
{
  	public $template = 'clean';
  	public $with;
    

    public function __construct()
    {
	    parent::__construct();
    }

    public function handleNotify(Request $req)
    {
        Log::info(json_encode($_POST)); 
        app('App\Doku')->handleNotify($req);
    }

    public function createDummyAddress($userId)
    {

       $userCtrl = new UserController();
       list($shipping,$billing) = $userCtrl->createDummyAddress($userId);
       return [$shipping,$billing];
    }

    public function parseProductsEmail($order)
    {
         $products = "";
         foreach ($order->details as $key => $value) {
            $products .= view('app::clean.templates.mail-product-order',[
                'value' => $value,
                'symbol' => getCurrencySymbol($order->currency)
              ]);
        }

        return $products;
    }

    public function parseAddressEmail($order)
    {
        $address = "";

        $address .= view('app::clean.templates.mail-address-order',[
                'billing' => $order->billing,
                'shipping' => $order->shipping
              ]);

        return $address;
    }

    public function parseBodyEmail($order,$products,$address)
    {
         $parseEmail = array(
                    "[[name]]" => $order->billing->first_name.' '.$order->billing->last_name,
                    "<td>[[data]]</td>" => $products,
                    "[[data]]" => $products,
                    "[[order_number]]" => $order->order_number,
                    "[[date]]" => date('d F Y'),
                    "[[total]]" => formatMoney($order->subtotal_after_discount),
                    "[[shipping]]" => formatMoney($order->shipping_amount),
                    "[[grand_total]]" =>formatMoney($order->grand_total),
                    "<td>[[address]]</td>" => $address,
                    "[[address]]" => $address,
                    "[[comfirm_url]]" => url('/account/order-detail/').'/'.$order->id,
                    "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($order->user->email),
                    "[[email]]" =>"careforyou@neneq.com",
                    "[[tracking_number]]" => $order->tracking_number,
                    "[[order_url]]" => url('/account/order-detail/'.$order->id),
                    'Subtotal' => 'Subtotal(IDR)',
                    'Shipping' => 'Shipping(IDR)',
                    'Total' => 'Total(IDR)',
                    'Shipping(IDR) to' => 'Shipping to',
                    '[[reason]]' => $order->cancel_reason

        );   

         return $parseEmail;
    }

    public function sendEmail($order,$emailMessages,$cc = '')
    {
        if($order->user->email == 'admin@hellomorra.com')
        {
          $order->user->email = 'luthfifs97@gmail.com';
        }
        $message = app('AlphaMailMessage')
        ->setSubject($emailMessages->subject)
        ->setBody($emailMessages->msg,'text/html')
        ->setFrom(config('alpha.mailer.sender'))
        ->setTo(array($order->user->email));

        if(!empty($cc))
        {
            $message = $message->addCc($cc);
        }

        $mailer = false;

        try {
          $mailer = app('AlphaMailSender')->send($message);      
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return $mailer;

    }

    public function getOrderById($orderId)
    {
         $order = CatalogOrder::with(array('details','billing','shipping','user'))->where('id',$orderId)->first();
         return $order;
    }

    public function sendEmailOrder($orderId = 0,$template = 'order-confirmation',$cc = '')
    {

        $order = $this->getOrderById($orderId);

        if(empty($order)) return false;

        $products = $this->parseProductsEmail($order);
        $address = $this->parseAddressEmail($order);
        
        $parseEmail = $this->parseBodyEmail($order,$products,$address);

        $emailMessages = parseEmail($template,$parseEmail);
        
        return $this->sendEmail($order,$emailMessages,$cc);

    }




  

    public function isValidCheckout()
    {
        $items = $this->layout->cart['orderDetails'];
        if(empty($items)) return redirect(url('/cart'));

    }

    public function validateAddressAndShippingMethod()
    {
        if(!(Session::has('shippingAddressId') && Session::has('shippingMethod'))) redirect('/checkout');

        $billing = $this->getUserAddressbyId(0,'billing');

        if(empty($billing) || empty($billing->address)) redirect(url('/checkout/billing-and-shipping'));

        $shipping = $this->getUserAddressbyId(Session::get('shippingAddressId'),'shipping');

        if(empty($shipping)) redirect(url('/checkout'));

        if(empty($shipping->address)) redirect(url('/checkout/billing-and-shipping'));

        $selectedShippingMethod = [];

        $res = $this->getQuoteDHL($shipping->postal_code,$shipping->city,$shipping->country);
        if($res['status'] == 0) redirect(url('/checkout'));

        foreach ($res['shippingMethods'] as $key => $value) {
            if($value->id == Session::get('shippingMethod'))
            {
                $selectedShippingMethod = $value;
            }
        }

        if(empty($selectedShippingMethod)) redirect(url('/checkout'));

        return [$billing,$shipping,$selectedShippingMethod];
    }

    public function placeOrder(Request $req)
    {
         $this->isValidCheckout();

         $Validator = \Validator::make($req->all(),[
                'terms_and_conditions' => 'required'
            ]);


      
         $messages = $Validator->errors();
         if($Validator->fails()){
            return redirect()->back()->withInput()->with('errors',$messages);
         }
         

         $isIdr = true;
         if($req->payment_method == 'paypal')
         {
            setCurrency('usd');
            $isIdr = false;
            $originalCurrency = getCurrency();
            $originalData = translateCartToParsedData(); 
         }

         $checkoutCurrency = getCurrency();

         setCurrency('idr');
         
         list($billing,$shipping,$selectedShippingMethod) = $this->validateAddressAndShippingMethod();

         $userCtrl = new UserController();
         $billing = $userCtrl->cloneAddress($billing,'billing',$billing->label);
         $shipping = $userCtrl->cloneAddress($shipping,'shipping',$shipping->label);

         $data = translateCartToParsedData();

         $data['order']->currency = $checkoutCurrency;
         $data['order']->rate_usd = app('AlphaSetting')->getSetting('rate_usd_idr');      
         $data['order']->rate_eur = app('AlphaSetting')->getSetting('rate_eur_idr');          

         $data['order']->billing_id = $billing->id;
         $data['order']->shipping_id = $shipping->id;
         foreach ($userCtrl->getAddressFields() as $key => $value) {
              $data['order']->{'shipping_'.$value} = $shipping->{$value};
              $data['order']->{'payment_'.$value} = $billing->{$value};
         }

         $data['order']->carrier_id = $selectedShippingMethod->id;
         $data['order']->shipping_amount = $selectedShippingMethod->ShippingCharge;
         $data['order']->tax = 0;
         $data['order']->customer_note = $shipping->order_notes;
         $data['order']->status = 'PENDING';
         $data['order']->store = 19;
         $data['order']->source = 1;
         if($req->payment_method == 'credit')
         {
            $data['order']->payment_method = 5;          
         }else{
            $data['order']->payment_method = 6;
         }

         $checkoutResponse = CatalogCheckout::process($data);

         $billing->is_checkout = 1;
         $billing->save();

         $shipping->is_checkout = 1;
         $shipping->save();

         CatalogCheckout::cleanUp($data);
 
         $mailResponse = $this->sendEmailOrder($checkoutResponse['id'],'order-confirmation');

         $this->layout = "";

         //capture payment
         if($isIdr)
         {
             return app('App\Doku')->capture(
                $data['orderDetails'],
                $data['order']->subtotal,
                $selectedShippingMethod->ShippingCharge,
                $checkoutResponse['order_number'],
                Auth::user()->email,
                $billing->first_name.' '.$billing->last_name,
                $data['order']->currency
              );
            
         }else{
           $url = app('App\Paypal')->capture(
                $originalData['orderDetails'],
                $originalData['order']->subtotal,
                convertCurrency($selectedShippingMethod->ShippingCharge,$originalCurrency),
                $checkoutResponse['order_number'],
                Auth::user()->email,
                $billing->first_name.' '.$billing->last_name,
                strtoupper($originalCurrency),
                $shipping
            );
           if($url)
           {
                return redirect($url);
           }


         }
         
    }

    public function payment()
    {
        $this->isValidCheckout();

        list($billing,$shipping,$selectedShippingMethod) = $this->validateAddressAndShippingMethod();

        $items = $this->layout->cart['orderDetails'];
        $subTotal = $this->layout->cart['order']->subtotal;
        $grandTotal = $subTotal + $selectedShippingMethod->ShippingCharge;

        $this->layout->title = setPageTitle("Payment"); 
        $this->layout->content = view('app::'.$this->template.'.page.checkout-payment',[
                'billing' => $billing,
                'shipping' => $shipping,
                'items' => $items,
                'selectedShippingMethod' => $selectedShippingMethod,
                'subTotal' => $subTotal,
                'grandTotal' => $grandTotal
            ]);

    }


    public function getUserAddressbyId($id = 0,$type = 'shipping')
    {
         $address = CatalogAddress::whereType($type)->where('is_checkout','!=',1)->where('user_id',Auth::user()->id);

         if($id > 0) $address = $address->where('id','=',$id);

         $address = $address->first();

         return $address;
    }

    public function billAndShip()
    {   
        $this->isValidCheckout();

        if(!Auth::user()) return redirect('/checkout/guest');
        $country = DB::table('countries')->get();
        $billing = $this->getUserAddressbyId(0,'billing');
        $shipping = $this->getUserAddressbyId(0,'shipping');

        if(empty($billing) || empty($shipping)){
          list($billing,$shipping) = $this->createDummyAddress(Auth::user()->id);
        }

        $subTotal = $this->layout->cart['order']->subtotal;
        $items = $this->layout->cart['orderDetails'];

        $this->layout->title = setPageTitle("Shipping");
        $this->layout->content = view('app::'.$this->template.'.page.checkout-shipping-non-member',[
            'country' => $country,
            'billing' => $billing,
            'shipping' => $shipping,
            'subTotal' => $subTotal,
            'items' => $items
          ]);
    }

    public function billAndShipSaveData(Request $req)
    {
        $this->isValidCheckout();

        $userCtrl = new UserController();
        $rules = $userCtrl->getRegisterRules($req,true);
        $rules['shipping_method'] = 'not_in:-1';

        $Validator = \Validator::make($req->all(),$rules);
        $messages = $Validator->errors();
        if($Validator->fails()){
            return redirect()->back()->withInput()->with('errors',$messages);
        }


        $user = Auth::user();

        CatalogAddress::whereUserId($user->id)->whereLabel('Generated Shipping Address')->delete();
        CatalogAddress::whereUserId($user->id)->whereLabel('Generated Billing Address')->delete();

        $billingAddress = $userCtrl->createAddress($user->id,$req,"billing",'Billing');

        if($req->billToShip == 'no')
        {
          $shippingAddress = $userCtrl->createAddress($user->id,$req,"shipping",'Shipping');          
        }else{
          $shippingAddress = $userCtrl->cloneAddress($billingAddress,'shipping',"Shipping");
        }

        Session::put('shippingAddressId',$shippingAddress->id);
        Session::put('shippingMethod',$req->shipping_method);

        return redirect('/checkout/payment');


    }


    public function guestRegister(Request $req)
    {

        $this->isValidCheckout();
        
        $check = \User::whereEmail($req->email)->first();
        if(count($check)){
             return redirect()->back()->with('msg','Email has been taken before !')->withInput();
        }


        $userCtrl = new UserController();

        $Validator = \Validator::make($req->all(),$userCtrl->getRegisterUserRules());
        $messages = $Validator->errors();
        if($Validator->fails()){
            return redirect()->back()->withInput()->with('errors',$messages);
        }

        $user = $userCtrl->saveUser($req,true);
        $this->createDummyAddress($user->id);
        Auth::login($user);

        return redirect('/checkout/billing-and-shipping');
        
    }

    public function guest()
    {
        $this->isValidCheckout();
        
        if(Auth::user()) return redirect(url('checkout'));

        $this->layout->title = setPageTitle("Guest Checkout");
        $this->layout->content = view('app::'.$this->template.'.page.checkout-login');
    }

    public function addAddress()
    {
        $country = DB::table('countries')->get();
        $this->layout->content = view('app::'.$this->template.'.page.checkout-add-address',[
            'country' => $country,
            ]);

       $this->layout->menu = '';
       $this->layout->title = 'Add Shipping Address | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function editAddress($id)
    {
        $data = $this->getUserAddressbyId($id,'shipping');
        $country = DB::table('countries')->get();
        $this->layout->content = view('app::'.$this->template.'.page.checkout-edit-address',[
            'data' => $data,
            'country' => $country,
            ]);

        $this->layout->menu = '';
        $this->layout->title = 'Edit Shipping Address | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function saveAddress(Request $req)
    {
        $userCtrl = new UserController();

        $Validator = \Validator::make($req->all(),$userCtrl->getAddressRules());
        $messages = $Validator->errors();
        if($Validator->fails()){
            return redirect()->back()->withInput()->with('errors',$messages);
        }
        
        $address = $userCtrl->createUpdateAddress(Auth::user()->id,$req,'shipping',$req->label);

        return redirect(url('checkout'));
    }

    public function checkout()
    {
        $this->isValidCheckout();

        if(!Auth::user()) return redirect('/checkout/guest');

        $shippingAddress = CatalogAddress::whereUserId(Auth::user()->id)->where('is_checkout','!=',1)->whereType('shipping')->get();
        $billingAddress = CatalogAddress::whereUserId(Auth::user()->id)->where('is_checkout','!=',1)->whereType('billing')->first();
            
        if(empty($billingAddress)) return redirect('/checkout/billing-and-shipping');
        if(empty($billingAddress->address)) return redirect('/checkout/billing-and-shipping');

        $this->layout->title = setPageTitle("Shipping");
        $this->layout->content = view('app::'.$this->template.'.page.checkout-shipping',[
              'shippingAddress' => $shippingAddress
          ]);
    }

    public function saveCheckoutShipping(Request $req)
    {
        $this->isValidCheckout();

        if(!Auth::user()) abort(404);
        
        $address = $this->getUserAddressbyId($req->addressId,'shipping');

        if(empty($address)) return json_encode(['status'=>0,'msg'=>"Invalid Shipping Address."]);

        if(!is_string($req->shippingMethod)) return json_encode(['status'=>0,'msg'=>"Invalid Shipping Method."]);

        Session::put('shippingAddressId',$req->addressId);
        Session::put('shippingMethod',$req->shippingMethod);

        return json_encode(['status'=>1]);

    }

    public function getRealtimeDHLQuote(Request $req)
    {
         $res = $this->getQuoteDHL($req->postal_code,$req->city,$req->country);

         return json_encode($res);
    }

    public function parseCartToDHLFormat()
    {
        $items = [];
        $cartItems = $this->layout->cart['orderDetails'];
        foreach ($cartItems as $key => $value) {
            for ($i=0; $i < $value->qty; $i++) { 
                $items[] = [
                    'height' => $value->shipping_height,
                    'depth' => $value->shipping_depth,
                    'width' => $value->shipping_width,
                    'weight' => $value->shipping_weight,
                    'item_id' => $value->item_id
                ];
            }
        }

        return $items;
    }

    public function generateOriginAddressDHLFormat()
    {
         $from = [
            'countryCode' => app('AlphaSetting')->getSetting('country_code'),
            'postalCode' => app('AlphaSetting')->getSetting('origin_postal_code'),
            'city' =>  app('AlphaSetting')->getSetting('origin_city')
        ];

        return $from;
    }

    public function generateTargetAddressDHLFormat($postalCode,$city,$country)
    {
        $countryCode = DB::table('countries')->where('name','=',$country)->first();

        $to = [
            'countryCode' => $countryCode->code,
            'postalCode' => $postalCode,
            'city' =>  $city
        ];

        return $to;
    }

    public function getQuoteDHL($postalCode,$city,$country)
    {
         $res = app('App\DHL')->getQuote(
                    $this->generateOriginAddressDHLFormat(),
                    $this->generateTargetAddressDHLFormat($postalCode,$city,$country),
                    $this->parseCartToDHLFormat(),
                    $this->layout->cart['order']->idr_subtotal
              );

         return $res;
    }

    public function getShippingMethod(Request $req)
    {
        if(!Auth::user()) return json_encode(['status' => 0,'Address Not Found!']);

        $address = $this->getUserAddressbyId($req->id,'shipping');

        if(empty($address)) return json_encode(['status' => 0,'Address Not Found!']);

        $res = $this->getQuoteDHL($address->postal_code,$address->city,$address->country);

        return json_encode($res);



    }
}