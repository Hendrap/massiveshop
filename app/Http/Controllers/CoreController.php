<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Alpha\Core\CoreFrontController as BaseController;
use Illuminate\Http\Request;
use Taxonomy;


class CoreController extends BaseController
{
  	public $template = 'clean';
  	public $with;
    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        initCurrency();
        $this->layout = view('app::'.$this->template.'.layout');

        $this->layout->menu = "";
    	$this->layout->title = setHomeTitle();
        
    	$categories = Taxonomy::where('taxonomy_type','=','category')->where('parent','=','0')->get();
	    $this->layout->categories = $categories;
    
    	$cart = translateCartToParsedData();
    	$this->layout->cart = $cart;
    	$this->layout->total_cart = $cart['order']->totalQty;



    }

}
