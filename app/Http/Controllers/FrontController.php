<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\CoreController as BaseController;
use Alpha\Requests;
use Alpha\Core\AlphaSetting;
use Illuminate\Http\Request;
use Entry;
use Entrymeta;
use Taxonomy;
use DB;
use Input;
use Response;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Swift_SendmailTransport;
use Auth;
use Usermeta;
use User;
use Session;

class FrontController extends BaseController
{
  	public $template = 'massive';
  	public $with;
    

    public function __construct()
    {
	      parent::__construct();
    }

    public function thankYou(Request $req)
    {
        if(!empty($req->paymentId))
        {
            app('App\Paypal')->executePayment($req->paymentId,$req->PayerID);
        }
      
        $this->layout->title = setPageTitle("Thank You");
        $this->layout->content = view('app::'.$this->template.'.page.thanks',[
                'siteTitle' => app('AlphaSetting')->getSetting('site_name'),
                ]);
    }

    private function getEntryView($entry)
    {
        $defaultView = 'app::'.$this->template.'.pages.detail';
        $view = $defaultView;

        //entry type
        if(\View::exists($defaultView.'-'.$entry->entry_type)){
          $view = $defaultView.'-'.$entry->entry_type;
        }

        //slug
        if(\View::exists($defaultView.'-'.$entry->slug)){
          $view = $defaultView.'-'.$entry->slug;
        }

        //entry id
        if(\View::exists($defaultView.'-'.$entry->id)){
          $view = $defaultView.'-'.$entry->id;
        }
        return $view;

    }

    public function postTestimonial(Request $req)
    {
        $this->validate($req,[
            'firstname' => 'required',
            'email' => 'required|email',
            'testimonial' => 'required'
          ]);

        $entry = new Entry();
        $name = trim($req->firstname.' '.$req->lastname);
        $entry->title = '[:en]'.$name.' | '.$req->email;
        $entry->content = '[:en]'.$req->testimonial;
        $entry->author = 1;
        $entry->entry_parent = 0;
        $entry->entry_type = 'testimonial';
        $entry->status = 'draft';
        $entry->published_at = date('Y-m-d H:i:s');
        $entry->modified_by = 1;
        $entry->slug = str_slug($name.'-'.time());
        $entry->save();

        $metas = ['firstname','lastname','email'];
        
        $metasToDB = [];

        foreach ($metas as $key => $value) {
          $meta = new Entrymeta();
          $meta->entry_id = $entry->id;
          $meta->meta_key = $value;
          $meta->meta_value_text = $req->input($value);

          $metasToDB[] = $meta->toArray();
        }

        DB::table('entrymetas')->insert($metasToDB);

        //dd([$entry,$metasToDB]);

        return redirect()->back()->with('msg','Testimonial Submitted!');
    }

    public function testimonial()
    {

      $this->layout->title = setPageTitle("Testimonial");
      $this->layout->content = view('app::'.$this->template.'.page.testimonial',[
                'siteTitle' => app('AlphaSetting')->getSetting('site_name'),
                ]);
    }

    public function changeCurrency($type)
    {
        $whiteList = ['idr','usd','eur'];
        if(!in_array($type, $whiteList)) return redirect('/');

        setCurrency($type);

        return redirect()->back();
    }

    public function notfound()
    {
    	$this->layout->title = app('AlphaSetting')->getSetting('site_name');
    	$this->layout->content = view('app::'.$this->template.'.page.404',[
                'siteTitle' => app('AlphaSetting')->getSetting('site_name'),
                ]);

      return $this->layout;
    }

    public function index()
    {
      
      Session::put('newsletter','true');

      $slider = Entry::with(array('medias'))->where('entry_type','=','slider')->orderBy('published_at','desc')->whereStatus('published')->get(); 

      $offer = Entry::with(array('medias','metas'))->where('published_at','<=',date('Y-m-d H:i:s'))->where('entry_type','=','special-offer')->whereStatus('published')->take(4)->orderBy('sequence','asc')->get(); 
      $data = Entry::with(array('medias','items','taxonomies','taxonomies.parents'))->where('entry_type','=','catalog_variant_product')->where('status','!=','deleted')->paginate(8);

      $testimonials = Entry::with(['metas'])->whereStatus('published')->whereEntryType('testimonial')->take(4)->orderby('sequence','asc')->get();


      $site_intro = Entry::with(array('medias','metas','metas.media'))->where('entry_type','=','page')->whereSlug('site-intro')->first();

    	$this->layout->content = view('app::'.$this->template.'.page.home-grid-4',[
            'siteTitle' => app('AlphaSetting')->getSetting('site_name'),
            'slider' => $slider,
            'offer' => $offer,
            'data' => $data,
            'testimonials' => $testimonials,
            'site_intro' =>$site_intro,
            'col_size' => 3,
            ]);
	    $this->layout->menu = 'index';
      $this->layout->title = app('AlphaSetting')->getSetting('site_name').' | '.app('AlphaSetting')->getSetting('site_slogan');
    }


    public function indexByCategories($category)
    {
      
      $taxo = Taxonomy::whereTaxonomySlug($category)->first();

      $offer = Entry::with(array('medias','metas'))->where('published_at','<=',date('Y-m-d H:i:s'))->where('entry_type','=','special-offer')->whereStatus('published')->take(4)->orderBy('sequence','asc')->get(); 
      $data = Entry::select('entries.*')->with(array('medias','items','taxonomies','taxonomies.parents'))
                ->join('entry_taxonomy', 'entry_taxonomy.entry_id', '=', 'entries.id')
                ->where('entry_taxonomy.taxonomy_id',$taxo->id)
                ->where('entries.entry_type','=','catalog_variant_product')->where('entries.status','!=','deleted')->paginate(8);

      $this->layout->content = view('app::'.$this->template.'.page.home-grid-3',[
            'siteTitle' => $taxo->name,
            'offer' => $offer,
            'data' => $data,
            'col_size' => 4,
            ]);

      $this->layout->menu = 'index';
      $this->layout->title = 'Categories | '.$taxo->name;
    }

    public function about()
    {
       $about = Entry::with(array('medias'))->where('entry_type','=','page')->whereSlug('about-us1')->orWhere('slug','=','about-us2')->orWhere('slug','=','about-us3')->orWhere('slug','=','about-us4')->orderBy('created_at','asc')->get();

       $this->layout->content = view('app::'.$this->template.'.page.about-us',[
            'about' => $about
            ]);
	     $this->layout->menu = 'about';
       $this->layout->title = 'About | '.app('AlphaSetting')->getSetting('site_name');
    }

    //Acnologia 
    public function categories()
    {
      $data = Taxonomy::where('taxonomy_type','=','category')->where('parent','=','0')->get();

      $this->layout->content = view('app::'.$this->template.'.page.category',[
            'data' => $data
            ]);

	    $this->layout->menu = 'product';
      $this->layout->title = 'Categories | '.app('AlphaSetting')->getSetting('site_name');
    }

    //Acnologia 
    public function categoryDetail($slug)
    {
      
      $selectedCategory = Taxonomy::with(['childs.activeEntries','childs.activeEntries.taxonomies','childs.activeEntries.medias'])
      ->where('taxonomy_type','=','category')
      ->whereTaxonomy_slug($slug)->first();

      if(empty($selectedCategory)) abort(404);

      $this->layout->content = view('app::'.$this->template.'.page.category-detail',[
            'selectedCategory' => $selectedCategory,
            ]);


	    $this->layout->menu = 'product';
      $this->layout->title = $selectedCategory->name.' | '.app('AlphaSetting')->getSetting('site_name');

    }
    
    
    public function getOtherProducts($id)
    {
        $otherProducts = Entry::with(['medias','items','taxonomies'])->whereStatus('published')->whereEntryType('catalog_variant_product')->where('id','!=',$id)->take(3)->orderByRaw('RAND()')->get();

        return $otherProducts;
    }
    //Acnologia 
    public function itemDetail($slug, $variant = null)
    {

      $data = Entry::with(['medias','items','taxonomies','metas'])->whereEntryType('catalog_variant_product')->whereSlug($slug)->first();
      
      if(empty($data)) abort(404);

      $data = parseProduct($data);

      // get variant
      $listVariant = $data->items->toArray();
      $variant_column = array_column($listVariant, 'item_title');
      $variant_index = array_search($variant, $variant_column);

      $categoryProduct = [];
      $categoryProductSecond = [];
      $selectedItem = [];


      foreach ($data->taxonomies as $key => $value) {
          if($value->taxonomy_type == 'category' && $value->parent != 0)
          {
              $categoryProduct = Taxonomy::whereId($value->parent)->first();
              $categoryProductSecond = $value;
          }
      }

      foreach ($data->items as $key => $value) {
          $selectedItem = $value;
          if($selectedItem->stock_available > 100)
          {
              $selectedItem->stock_available = 10;
          }
      }

      $otherProducts = [];

      if(!empty($categoryProduct))
      {

          $tmpProducts = $categoryProduct->entries()
          ->select(['entries.*'])
          ->with(['medias','items','taxonomies'])
          ->where(function($w){
                $w->where('entries.status','published');
                $w->orWhere('entries.status','featured');
            })
          ->whereEntryType('catalog_variant_product')
          ->where('entries.id','!=',$data->id)
          ->take(10)
          ->get();


          if(count($tmpProducts) == 0)
          {
            $tmpProducts = $categoryProductSecond->entries()
            ->select(['entries.*'])
            ->with(['medias','items','taxonomies'])
            ->where(function($w){
                $w->where('entries.status','published');
                $w->orWhere('entries.status','featured');
            })
            ->whereEntryType('catalog_variant_product')
            ->where('entries.id','!=',$data->id)
            ->take(10)
            ->get();
          }

           $otherProducts = $tmpProducts;

      }


      if(count($otherProducts) == 0){
         $otherProducts = $this->getOtherProducts($data->id);
      }

      $this->layout->content = view('app::'.$this->template.'.page.product-detail',[
            'data' => $data,
            'categoryProduct' => $categoryProduct,
            'selectedItem' => $selectedItem,
            'otherProducts' => $otherProducts,
            'activeVariant' => $data->items[$variant_index],
            ]);
	    $this->layout->menu = 'product';
      $this->layout->title = parseMultiLang($data->title).' | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function news($slug = '')
    {

      $currentNews = [];

      if(!empty($slug))
      {
          $currentNews = Entry::where('published_at','<=',date('Y-m-d H:i:s'))
          ->where('entry_type','=','news')
          ->whereStatus('published')
          ->whereSlug($slug)
          ->first();

          if(empty($currentNews)) abort(404);
      }
      
      $latest = Entry::where('published_at','<=',date('Y-m-d H:i:s'))
      ->where('entry_type','=','news')
      ->whereStatus('published')
      ->orderBy('published_at','DESC')
      ->take(3)
      ->get();

      $dates = Entry::distinct(DB::raw("DATE_FORMAT(published_at,'%Y-%m') as date"))
      ->select(DB::raw("DATE_FORMAT(published_at,'%Y-%m') as date"))
      ->whereEntryType('news')
      ->whereStatus('published')
      ->where('published_at','<=',date('Y-m-d H:i:s'))
      ->orderBy('published_at','desc')
      ->take(3)
      ->get();

   

      $archiveNews = [];
      foreach ($dates as $key => $value) {

        $archiveNews[$value->date] = Entry::whereEntryType('news')
                                      ->whereStatus('published')
                                      ->select(['title','id','slug','published_at'])
                                      ->where('published_at','<=',date('Y-m-d H:i:s'))
                                      ->where(DB::raw("DATE_FORMAT(published_at,'%Y-%m')"),'=',$value->date)
                                      ->orderByRaw("DATE_FORMAT(published_at,'%Y-%m-%d %H:%i:%s') DESC")
                                      ->get();
      }

      

      if(empty($currentNews))
      {
        if(count($latest)) $currentNews = $latest[0];        
      }

      $this->layout->content = view('app::'.$this->template.'.page.news',[
            'latest' => $latest,
            'archiveNews' => $archiveNews,
            'currentNews' => $currentNews
            ]);
	    $this->layout->menu = 'news';
      $this->layout->title = 'News | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function news_detail($slug)
    {
      $detail = Entry::with(array('medias'))->where('entry_type','=','news')->where('slug','=',$slug)->first();
      $latest= Entry::where('entry_type','=','news')->orderBy('created_at','DESC')->whereStatus('published')->take(3)->get();
      $data = DB::select("SELECT DISTINCT(DATE_FORMAT(created_at,'%Y-%m')) as tanggal FROM alp_entries where entry_type='news' and status='published'");
      $topnews = '';
      $this->layout->content = view('app::'.$this->template.'.page.news',[
            'data' => $data,
            'latest' => $latest,
            'detail' => $detail,
            'topnews' => $topnews
            ]);
	  $this->layout->menu = 'news';
      $this->layout->title = parseMultiLang($detail->title).' | '.app('AlphaSetting')->getSetting('site_name');
      //return response()->json(['data' => $data, 'msg' => 'success']);
    }

    public function newsletter()
    {
    	$input = \Request::all();
    	$rules = array(
                'name' => 'required',
                'email' => 'required|email',
            );

            $Validator = \Validator::make($input,$rules);
            $messages = $Validator->errors();

            if ($Validator->fails()) {

                return redirect()->back()->with('errors',$messages)->withInput();
            }
    	$data = Usermeta::where('meta_value_text','=',$input['email'])->first();
      //echo count($data);
    	if(count($data)){
    		return redirect()->back()->with('already_taken','yes');
    	}

    	$chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $str = "";
            $date = date('Y-m-d h:i:s');
            $length = 32;
            for ($i = 0; $i < $length; $i++) {
                $str .= $chars[mt_rand(0, strlen($chars) - 1)];
            }

    	$insert = new \Usermeta();
    	
      $insert->meta_name = "newsletter";
    	$insert->meta_key = "newsletter";
    	$insert->meta_value_text = $input['email'];
      $insert->save();
      if (Auth::check()) {
          $name = Auth::user()->username;
      }else{
        $name = $input['name'];
      }
  		$parseEmail = array(
                "[[name]]" => $name,
  	            
  	            "[[email]]" => app('AlphaSetting')->getSetting('email_contact'),
  	            //"[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($dataUser->email)
                "[[unsubscribe]]" => url('/unsubscribe').'/'.base64_encode($input['email'])
  	        );   
  	  $emailMessages = parseEmail("newsletter",$parseEmail);
  		$message = app('AlphaMailMessage')->setSubject('newsletter')->setBody($emailMessages->msg,'text/html')->setFrom(array('careforyou@neneq.com' => 'NENEQ'))->setTo($input['email']);
  	    $mailer = app('AlphaMailSender')->send($message);
  	    if ($mailer) {
  	            return redirect()->back()->with('newsletter','success'); 
  		}
    }

    public function newsletter_confirm($key)
    {
      	//$data = Usermeta::where('meta_value_text','=',$key)->first();
      	return redirect('/')->with('newsletter','success');
        
    }

    public function unsubscribe_proses($key)
    {
	   $data = base64_decode($key);
	    $this->layout->content = view('app::'.$this->template.'.page.unsubscribe',[
            'data' => $data,
            ]);
	    $this->layout->menu = '';
      	$this->layout->title = 'Unsubscribe | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function unsubscribe_save(){
    	$input = \Request::all();

    	$insert = new \Usermeta();
    	$insert->meta_name = "reason";
    	$insert->meta_key = $input['email'];
    	$insert->meta_value_text = $input['reason']; 
    	$insert->save();

    	return redirect('/');
    }

    public function unsubscribe($key)
    {
		  $email = base64_decode($key);

    	$data = Usermeta::where('meta_name','newsletter')->where('meta_value_text',$email)->first();
      //dd($data);exit();
    	$parseEmail = array(
            "[[name]]" => $data->meta_value_text,
        );   
        $emailMessages = parseEmail("newsletter",$parseEmail);
    	  $message = app('AlphaMailMessage')->setSubject('Unsubscribe')->setBody($emailMessages->msg,'text/html')->setFrom(array('careforyou@neneq.com' => 'NENEQ'))->setTo($email);
        $mailer = app('AlphaMailSender')->send($message);
        $data = DB::table('usermetas')->where('meta_key','newsletter')->where('meta_value_text',$email)->delete();
        if ($mailer) {
            return redirect('unsubscribe_proses/'.$key);
	     }
      }

    public function contact()
    {
      //dpr($message);
      //$result = $mailer->send($message);
      $this->layout->content = view('app::'.$this->template.'.page.contact',[
            'siteTitle' =>'',
            ]);
	    $this->layout->menu = 'contact';
      $this->layout->title =  'Contact | '.app('AlphaSetting')->getSetting('site_name');
    }

    public function contact_save()
    {
        $input = \Request::all();
        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        );

        $Validator = \Validator::make($input,$rules);
        $messages = $Validator->errors();
  
        if ($Validator->fails()) {

            return redirect()->back()->with('errors',$messages)->withInput();
        }
        $parseEmail = array(
                "firstname" => $input['firstname'],
                "lastname" => $input['lastname'],
                "emailcontact" => $input['email'],
                "subject" => $input['subject'],
                "message" => $input['message'],
            );
        $emailMessages = parseEmail("contact-us-message",$parseEmail);

        $subject = Entry::whereEntryType('email_template')->whereSlug('contact-us-message')->first();

        $message = app('AlphaMailMessage')
        ->setSubject(parseMultiLang($subject->title))
        ->setBody($emailMessages->msg, 'text/html')
        ->setFrom(config('alpha.mailer.sender'))
        ->setReplyTo(array($input['email']))
        ->setTo(app('AlphaSetting')->getSetting('admin_email_contact'));
        $mailer = app('AlphaMailSender')->send($message);


        //if ($mailer) {
          return redirect('/contact-us')->with('success','success');  
        // }else{
        //   return redirect('/contact-us')->with('failed','failed');  
        // }
    }

    public function special_offer()
    {
        $data = Entry::with(array('medias','metas'))->where('published_at','<=',date('Y-m-d H:i:s'))->where('entry_type','=','special-offer')->whereStatus('published')->take(4)->get(); 
        $this->layout->content = view('app::'.$this->template.'.page.special',[
            'data' => $data
            ]);
	      $this->layout->menu = 'offer';
        $this->layout->title = 'Special Offer | '.app('AlphaSetting')->getSetting('site_name');
    }


    public function staticPage()
    {

       $segments = \Request::segments();
       sort($segments);

       if(empty($segments[0])) abort(404);

       $slug = $segments[0];

        $page = Entry::with(['medias','metas'])->whereEntryType('page')->where('slug','=',$slug)->whereStatus('published')->first();
        if(empty($page)) abort(404);


        $this->layout->content = view('app::'.$this->template.'.page.static',[
              'page' => $page
        ]);

        $this->layout->menu = '';
        $this->layout->title = parseMultiLang($page->title).' | '.app('AlphaSetting')->getSetting('site_name');

    } 


    public function reference()
    {
        $data = Entry::with(['medias','metas'])
        ->where('entry_type','=','reference')
        ->whereStatus('published')
        ->orderBy('sequence','asc')
        ->get();

        $this->layout->content = view('app::'.$this->template.'.page.reference',[
            'data' => $data
            ]);
	      $this->layout->menu = 'reference';
        $this->layout->title = 'References | '.app('AlphaSetting')->getSetting('site_name');
    }


    public function entries()
    {
       $segments = \Request::segments();
       
       if(array_key_exists($segments[0], \Config::get('alpha.application.locales'))){
            unset($segments[0]);
       }    
       sort($segments);

       if(empty($segments[0])) abort(404);

       $entryType = $segments[0];
       
       $entries = \Entry::with($this->with)->whereEntryType($entryType)->where('status','=','published')->where('published_at','<=',date('Y-m-d H:i:s'))->paginate(30);
      
       $config = getEntryConfig($entryType);

       $this->layout->title = setPageTitle($config['plural']);

       $view = 'app::'.$this->template.'.pages.pages';

       //entry type
       if(\View::exists($view.'-'.$entryType)){
            $view = $view.'-'.$entryType;
       }
       
       $this->layout->content = view($view,[
        'entries' => $entries,
        'pageTitle' => $config['plural'],
        'entryType' => $entryType
        ]);

    }
    public function defaultPage($slug)
    {
        // $entry = \Entry::with($this->with)->whereEntryType('page')->whereSlug($slug)->whereStatus('published')->where('published_at','<=',date('Y-m-d H:i:s'))->first();

        // if(empty($entry)) abort(404);
        
        // $view = $this->getEntryView($entry);

        // $this->layout->title = setPageTitle(parseMultiLang($entry->title));
        // $this->layout->content = view($view,[
        //   'entry' => $entry
        //   ]);

        $segments = \Request::segments();
         
        if(array_key_exists($segments[0], \Config::get('alpha.application.locales'))){
             unset($segments[0]);
        }    
        sort($segments);

        if(empty($segments[0])) abort(404);

        $slug = $segments[0];

        $entry = \Entry::with($this->with)->whereEntryType('page')->whereSlug($slug)->whereStatus('published')->where('published_at','<=',date('Y-m-d H:i:s'))->first();

        if(empty($entry)) abort(404);
        
        $view = $this->getEntryView($entry);

        $this->layout->title = setPageTitle(parseMultiLang($entry->title));
        $this->layout->content = view($view,[
          'entry' => $entry
          ]);

    }

    public function entriesDetail($slug)
    {
        $entry = \Entry::with($this->with)->whereSlug($slug)->whereStatus('published')->where('published_at','<=',date('Y-m-d H:i:s'))->first();
        if(empty($entry)) abort(404);

        $view = $this->getEntryView($entry);

        $this->layout->title = setPageTitle(parseMultiLang($entry->title));
        $this->layout->content = view($view,[
          'entry' => $entry
          ]);
    }

    public function taxonomies()
    {
      $segments = \Request::segments();
      $taxonomyType = $segments[count($segments) - 1];
      $info = getTaxonomyConfig($taxonomyType);
      if(empty($info)) abort(404);

      $taxonomies = \Taxonomy::whereTaxonomyType($taxonomyType)->paginate(30);
      $view = 'app::'.$this->template.'.pages.taxonomies';

      if(\View::exists($view.'-'.$taxonomyType)){
        $view = $view.'-'.$taxonomyType;
      }

      $this->layout->title = setPageTitle($info['plural']);
      $this->layout->content = view($view,[
        'taxonomies' => $taxonomies,
        'pageTitle' => $info['plural']
        ]);
    }

    public function taxonomyEntries($taxonomySlug)
    {
        $taxonomy = \Taxonomy::whereTaxonomySlug($taxonomySlug)->first();

        if(empty($taxonomy)) abort(404);

        $entries = $taxonomy->entries()->with($this->with)->whereStatus('published')->where('published_at','<=',date('Y-m-d H:i:s'))->paginate(30);
        

        $defaultView = 'app::'.$this->template.'.pages.taxonomy';
        $view = $defaultView;

        //taxo type
        if(\View::exists($defaultView.'-'.$taxonomy->taxonomy_type)){
          $view = $defaultView.'-'.$taxonomy->entry_type;
        }

        //slug
        if(\View::exists($defaultView.'-'.$taxonomy->taxonomy_slug)){
          $view = $defaultView.'-'.$taxonomy->slug;
        }

        //taxo id
        if(\View::exists($defaultView.'-'.$taxonomy->id)){
          $view = $defaultView.'-'.$taxonomy->id;
        }

        $this->layout->title = setPageTitle($taxonomy->name);
        $this->layout->content = view($view,[
          'entries' => $entries,
          'pageTitle' => $taxonomy->name
          ]);

    }
}
