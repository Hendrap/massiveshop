<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Alpha\Core\CoreFrontController as BaseController;
use Illuminate\Http\Request;
use Taxonomy;
use Log;

class PaymentController extends BaseController
{
	public function handleNotifyDoku(Request $req)
    {
		$this->layout = "";
        app('App\Doku')->handleNotify($req);
    }

	public function handleNotifyPaypal(Request $req)
    {
    	$this->layout = "";
        app('App\Paypal')->handleNotification($req);
    }
}