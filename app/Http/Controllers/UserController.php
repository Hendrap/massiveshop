<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\CoreController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Hash;
use User;
use DB;
use Usermeta;
use Log;
use Auth;
use CatalogAddress;

class UserController extends BaseController
{
    public $template = 'clean';
    public $with;

    public function createDummyAddress($userId)
    {
       $shipping = new CatalogAddress();
       $shipping->label = "Generated Shipping Address";
       $shipping->type = 'shipping';
       $shipping->user_id = $userId;
       $shipping->save();

       $billing = new CatalogAddress();
       $billing->label = "Generated Billing Address";
       $billing->type = 'billing';
       $billing->user_id = $userId;
       $billing->save();

       return [$shipping,$billing];
    }


    public function facebookLogin(Request $req)
    {
        $facebookId = $req->id;
        $token = $req->token;

        if(Auth::user()) return json_encode(['status'=>0,'msg'=>'Error!']);


        $userInfo = @file_get_contents('https://graph.facebook.com/me?fields=name,email&access_token='.$token);
                
        $userInfo = json_decode($userInfo,true);
        if(empty($userInfo['name'])) return json_encode(['status'=>0,'msg'=>'Invalid Access Token!']);

        $userPassword = Str::random(8);

        $isNew = false;

        $user = User::whereEmail($userInfo['email'])->first();

        if(!empty($user)){
            if($user->status != 'active') return json_encode(['status'=>0,'msg'=>'Account '.$user->status]);        
        }else{
            $user = new User();
            $user->password = Hash::make($userPassword);
            $user->username = $userInfo['email'];
            $isNew = true;
        }

        $user->email = $userInfo['email'];
        $user->status = 'active';
        $user->last_login = date('Y-m-d H:i:s');
        $user->save();

        $insertToMeta[] = [
            'user_id' => $user->id,
            'meta_key' => 'first_name',
            'meta_value_text' => $userInfo['name']
        ];

        DB::table('usermetas')->insert($insertToMeta);

        $user->roles()->sync([5]);
        if($isNew)
        {
            list($shipping,$billing) = $this->createDummyAddress($user->id);
            
            $billing->first_name = $userInfo['name'];
            $billing->save();

            $shipping->first_name = $userInfo['name'];
            $shipping->save();

        }

        Auth::login($user);
        return json_encode(['status'=>1]);
    }

    public function sendEmailRegister($req)
    {
    	$validateKey = base64_encode($req->email);

        $parseEmail = array(
                "name" => $req->first_name,
                "validate_url" => url('/account-validate').'/'.$validateKey,
                "unsubscribe" => url('/unsubscribe').'/'.base64_encode($req->email)
        );

       	$parsedEmail = parseEmail("register-1",$parseEmail);
       	$message = app('AlphaMailMessage')
       	->setSubject($parsedEmail->subject)
       	->setBody($parsedEmail->msg,'text/html')
       	->setFrom(config('alpha.mailer.sender'))
       	->setTo(array($req->email));

       	$mailer = false;

       	try {   		
	       	$mailer = app('AlphaMailSender')->send($message);
       	} catch (Exception $e) {
       		Log::error($e->getMessage());
       	}

       	return $mailer;
    }

    public function getAddressFields()
    {
    	return ['first_name','last_name','company','address','city','country','state','district','postal_code','order_notes','phone'];
    }

    public function cloneAddress($address,$type,$label)
    {	
    	$newAddress = new CatalogAddress();
    	foreach ($this->getAddressFields() as $key => $value) {
    		$newAddress->{$value} = $address->{$value};
    	}

    	$newAddress->type = $type;
    	$newAddress->user_id = $address->user_id;
    	$newAddress->label = $label;
    	$newAddress->save();

    	return $newAddress;
    }

    public function createUpdateAddress($userId,$req,$type,$label)
    {
        $address = CatalogAddress::find($req->id);
        if(empty($address))
        {
            $address = new CatalogAddress();                        
        }

        foreach ($this->getAddressFields() as $key => $value) {
            if(!empty($req->{$value}))
            {
                $address->{$value} = $req->{$value};
            }
        }

        $address->type = $type;
        $address->user_id = $userId;
        $address->label = $label;
        $address->save();

        return $address;
    }

    public function createAddress($userId,$req,$type,$label)
    {
        
        $address = new CatalogAddress();            
        foreach ($this->getAddressFields() as $key => $value) {
    		if(!empty($req->input($type)[$value]))
    		{
	    		$address->{$value} = $req->input($type)[$value];    			
    		}
    	}

    	$address->type = $type;
    	$address->user_id = $userId;
    	$address->label = $label;
    	$address->save();

    	return $address;
    }

    public function saveUser($req,$isCheckout = false)
    {
        $user = new User();
        $user->email = $req->email;
        $user->username = $req->email;

        if($isCheckout)
        {
            $user->status = 'active';
        }else{        
            $user->status = 'inactive';
        }

        $user->password = bcrypt($req->password);
        $user->last_login = date('Y-m-d H:i:s');
        $user->save();

        $metas = [];

        if(!empty($req->first_name)) $metas[] = 'first_name';
        if(!empty($req->last_name)) $metas[] = 'last_name';
        if(count($metas) > 0)
        {
            foreach ($metas as $key => $value) {
                $meta = new Usermeta();
                $meta->meta_key = $value;
                $meta->user_id = $user->id;
                $meta->meta_value_text = $req->{$value};
                $meta->save();
            }
        }

        $user->roles()->sync([5]);
        return $user;

    }

    public function register($req)
    {

    	$req->email = $req->input('billing')['email'];
    	$req->password = $req->input('billing')['password'];
    	$req->first_name = $req->input('billing')['first_name'];

    	$user = $this->saveUser($req);

        $billingAddress = $this->createAddress($user->id,$req,"billing",'Billing');

        if($req->billToShip == 'yes')
        {
        	$shippingAddress = $this->cloneAddress($billingAddress,'shipping',"Shipping");
        }else{
        	$shippingAddress = $this->createAddress($user->id,$req,"shipping",'Shipping');
        }

        $this->sendEmailRegister($req);

        return [$user,$billingAddress,$shippingAddress];

    }

    public function getAddressRules()
    {
         $rules = array(
            'label' => 'required',
            'first_name' => 'required',
            'address' => 'required',
            'country' => 'required',
            'address' => 'required',
            'state' => 'required',
            'district' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postal_code' => 'required',
            'phone' => 'required',
        );


        return $rules;
    }

    public function getRegisterUserRules()
    {
        $rules = array(
            'first_name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        );

        return $rules;

    }

    public function getRegisterRules($req,$isCheckout = false)
    {
    	  $billingRules = array(
            'billing.first_name' => 'required',
            // 'billing.last_name' => 'required',
            // 'billing.address' => 'required',
            // 'billing.country' => 'required',
            // 'billing.address' => 'required',
            // 'billing.state' => 'required',
            // 'billing.district' => 'required',
            // 'billing.city' => 'required',
            // 'billing.state' => 'required',
            // 'billing.postal_code' => 'required',
            'billing.phone' => 'required',
        );

        if($isCheckout == false)
        {
            $billingRules['billing.email'] = 'required|email';
            $billingRules['billing.password'] = 'required|confirmed';
        }

        // if ($req->billToShip == 'no') {
        //     $shippingRules = array(
        //         'shipping.first_name' => 'required',
        //         'shipping.address' => 'required',
        //         'shipping.state' => 'required',
        //         'shipping.district' => 'required',
        //         'shipping.city' => 'required',
        //         'shipping.state' => 'required',
        //         'shipping.postal_code' => 'required',
        //         'shipping.phone' => 'required',
        //     );

        //     $billingRules = array_merge($billingRules,$shippingRules);
        // }

        return $billingRules;


    }
}	