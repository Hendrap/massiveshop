<?php 
namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;

class MultiLanguage{

    public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $locale = $request->segment(1);
        if(count(\Config::get('alpha.application.locales')) > 1){
           
            $defaultLocale = $this->app->config->get('alpha.application.default_locale');
            if(empty($defaultLocale)) $defaultLocale = 'en';
            if ( ! array_key_exists($locale, $this->app->config->get('alpha.application.locales'))) {
                
                $locale = $defaultLocale;

                $segments = $request->segments();

                array_unshift($segments,$locale);

                $this->app->setLocale($locale);

                $_SESSION['lang'] = $locale;
                return $this->redirector->to(implode('/', $segments));

            }
            $this->app->setLocale($locale);
            $_SESSION['lang'] = $locale;
            
        }
        return $next($request);
        
    }

}