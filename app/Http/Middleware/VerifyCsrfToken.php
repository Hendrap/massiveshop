<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'doku/*',
        '/checkout/thank-you',
		'checkout/thank-you',
        '/cart/save-data',
        '/cart/remove-item'

    ];
}
