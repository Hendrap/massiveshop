<?php 
	//entries
// 	if(!empty(app('AlphaSetting')->entries)){
// 		foreach (app('AlphaSetting')->entries as $key => $value) {
			
// 			//index
// //			Route::get('/'.$value['slug'],['as'=>'alpha_front_entry_'.$value['slug'],'uses'=>'FrontController@staticPage']);

// 			//detail
// //			Route::get('/'.$value['slug'].'/{slug}',['as'=>'alpha_front_entry_'.$value['slug'].'_detail','uses'=>'FrontController@staticPage']);

// 		}
// 	}

	//taxonomies
	// if(!empty(app('AlphaSetting')->taxonomies)){
	// 	foreach (app('AlphaSetting')->taxonomies as $key => $value) {

	// 		//taxonomies
	// 		//Route::get('/categories/'.$value['slug'],['as'=>'alpha_front_taxonomy_'.$value['slug'],'uses'=>'FrontController@taxonomies']);

	// 		//taxonomies - entries
	// 		//Route::get('/categories/'.$value['slug'].'/{slug}',['as'=>'alpha_front_taxonomy_'.$value['slug'].'_entries','uses'=>'FrontController@taxonomyEntries']);

	// 	}
	// }

	// Route::get('/{page}',['as'=>'alpha_front_page_default','uses'=>'FrontController@defaultPage']);

	//pages
	$pages = json_decode(app('AlphaSetting')->getSetting('alpha_pages_slug'));
	if(!empty($pages))
	{
		$blackList = Config::get('alpha.application.blacklist_pages');
		foreach ($pages as $key => $value) {
			if(!empty($value))
			{
				if(!in_array($value, $blackList)){
					Route::get('/'.$value,'FrontController@staticPage');									
				}
			}
		}
	}

	//sample blacklist,custom route
	// Route::get('/black-page',function(){
	// 	echo "Custom Route!";
	// });