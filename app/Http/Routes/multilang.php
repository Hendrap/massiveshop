<?php 

if(count(\Config::get('alpha.application.locales')) > 1){
		if(!isset($_SESSION['lang'])){
			$_SESSION['lang'] = 'id';
		}
		app('config')->set('alpha.application.default_locale',$_SESSION['lang']);
}
