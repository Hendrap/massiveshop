<!doctype html>
<html class="no-js" lang="en" dir="ltr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge" charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="_token" content="{!! csrf_token() !!}" />
	<title>@if(!empty($title)) {{$title}} @endif</title>
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('bundles/img/favicon.png') }}">
	<link rel="stylesheet" href="{{ asset('bundles/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('bundles/css/swiper.min.css') }}">
	<link rel="stylesheet" href="{{ asset('bundles/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('bundles/css/nice-select.css') }}">
	<script src="{{ asset('bundles/js/vendor/jquery.js') }}"></script>
	<script src="{{ asset('bundles/js/vendor/swiper.jquery.min.js') }}"></script>
	<script src="{{ asset('backend/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('bundles/js/vendor/jquery.spinner.min.js') }}"></script>
	<script src="{{ asset('bundles/js/app.js') }}"></script>
	<script src="{{ asset('bundles/js/vendor/jquery.nice-select.min.js') }}"></script>
	<style type="text/css">
		@media only screen and (max-width: 768px) {
		   .mobile-center{
		   	text-align: center;;
		   }
		   .mobile-space-bottom{
		   	margin-bottom: 10px;
		   }
		}

	</style>

</head>

<body>
	<script>
		function setCookie(cname, cvalue, exdays) {
	        var d = new Date();
	        d.setTime(d.getTime() + (exdays*24*60*60*1000));
	        var expires = "expires="+d.toUTCString();
	        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	    }

	    function getCookie(cname) {
	        var name = cname + "=";
	        var ca = document.cookie.split(';');
	        for(var i = 0; i < ca.length; i++) {
	            var c = ca[i];
	            while (c.charAt(0) == ' ') {
	                c = c.substring(1);
	            }
	            if (c.indexOf(name) == 0) {
	                return c.substring(name.length, c.length);
	            }
	        }
	        return "";
	    }

	    
		
		var user = getCookie("newsletter");
		    if (user != "") {
		    	$('.newsletter-popup').addClass('hide').removeClass('show');
		    } else {
		       	$('.newsletter-popup').removeClass('hide').addClass('show');
		        setCookie('newsletter','true',1);
		    }
		$(document).ready(function(){
			$('.close.newsletter').click(function(e){
				e.preventDefault();
				$('.newsletter-popup').hide();
			});

		});
	</script>
	@if(!Auth::user())
		{!! view('app::clean.templates.fb') !!}
	@endif
	<div class="newsletter-popup">
	
		<div class="newsletter-popup-body">
			<a class="close newsletter">
				X
			</a>
			<div class="newsletter-popup-content">
				
				<div>
					<h4>SUBSCRIBE FOR OUR NEWSLETTER &amp; GET EXCLUSIVE OFFERS</h4>
					@if(Session::get('already_taken') == 'yes')
				            <div class="alert alert-danger" role="alert">Email already suscribed!</div>
					    {{Session::forget('already_taken')}}
				        @endif
				     @if(Session::get('newsletter') == 'success')
				            <div class="alert alert-success" role="alert">Thank you for subscribing</div>
					    {{Session::forget('newsletter')}}
				        @endif
				    @if(isset($errors))
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        @if($errors->has('name') && $errors->has('email'))
					        {{$errors->first('name', 'The name  and email field is required.')}}
					        @elseif($errors->has('name'))
					        {{$errors->first('name', 'The name field is required.')}}
					        @elseif($errors->has('email'))
					        {{$errors->first('email', 'The email field is required.')}}
					        @endif
					    </div>
					@endif
					@endif

					<form method="POST" action="{{url('newsletter')}}">
						{{ csrf_field() }}
						<input type="name" name="name" placeholder="Sure! My name is...">
						<input type="email" name="email" placeholder="And my e-mail...">
						<button type="submit" class="btn">SUBSCRIBE</button>
					</form>
				</div>
				
			</div>
			
		</div>
	
	</div>
	
	
	<div class="wrapper">
		
		<!--login popup-->
		<div class="login-popup-cont">
					<div id="loginPopUp" class="login-pop">

						<div class="content">

							<h6>Account</h6>
							<div class="separator"></div>
							@if(Auth::User())
								<a style="text-align:center;display:block;" class="btn btn-account-control" href="{{url('account')}}">My Account</a>
								<a style="text-align:center;display:block;" class="btn btn-account-control logout" href="{{url('logout')}}">Logout</a>
							@else
							<form method="POST" action="{{url('login')}}" id="form-login">
								{{ csrf_field() }}
								
								<div class="left" ><span class="text-danger notmatch"></span></div>
		                        <div class="left" ><span class="text-danger ermail"></span></div>
								<input type="email" name="email" placeholder="Email">

								<div class="left"><span class="text-danger erpass"></span></div>
								<input type="password" name="password" placeholder="Password">
							  	<input type="hidden" name="url" value="{{\Request::url()}}">
								<a href="{{url('/forget-password')}}" class="forgotlink">Forgot?</a>
								<button type="submit">LOG IN</button>
							</form>

							<a style="text-align:center;display:block;" href="{{url('register')}}">Create an account?</a>
							
							<div class="separator-two">
								<div class="line"></div><span>OR</span><div class="line"></div>
							</div>
							<button type="submit" class="login-fb neneq-fb-login"><i class="fa fa-facebook"></i>LOGIN VIA FACEBOOK</button>
							@endif
						</div>

					</div>
					
		</div>

				<!--login popup-->
		<header>

			<div class="header-container">
				<div class="top">
					<div class="logo">

						<a href="{{url('/')}}"><img src="{{ asset('bundles/img/logo.png') }}" alt="neneq logo"></a>

					</div>
					<div class="currency-mobile mob-show">
							<a href="{{ url('/change-currency/usd') }}">USD</a> | <a href="{{ url('/change-currency/eur') }}">EUR</a> | <a href="{{ url('/change-currency/idr') }}">IDR</a>
					</div>
					<div class="cart-icon">
					<ul style="float: right;margin-left: 20px;">
						<li style="margin-bottom: 5px"  class=""><a href="{{ url('/change-currency/usd') }}" class="fa fa-dollar" style="color:white;"> <span style="font-size: 12px">USD</span></a></li>
						<li style="margin-bottom: 5px" class=""><a href="{{ url('/change-currency/eur') }}" class="fa fa-euro" style="color:white;"> <span style="font-size: 12px">EUR</span></a></li>
						<li class=""><a href="{{ url('/change-currency/idr') }}" style="color: white;font-size: 12px;font-weight: 700;">Rp <span style="font-size: 12px;font-weight: normal;font-family: FontAwesome;">IDR</span></a></li>
					</ul>

					@if(!Auth::user())
						<a class="cart-trigger login-trigger" href="#"><img src="{{ asset('bundles/img/userlogo.png') }}" alt="user"></a>
					@else
						<!-- kalau user sudah login pakai yang ini -->
						<a class="cart-trigger login-trigger" href="#"><img src="{{ asset('bundles/img/userlogo.png') }}" alt="user"></a>
						<!-- /kalau user sudah login pakai yang ini -->
					@endif
					@if(!empty($total_cart))
						<a class="cart-trigger" href="{{url('/cart')}}"><img src="{{ asset('bundles/img/cart-logo.png') }}" alt="cart"><div class="cart-nominal" id="qt-cart">{{$total_cart}}</div></a>
					@else
						<a class="cart-trigger" href="{{url('/cart')}}"><img src="{{ asset('bundles/img/cart-logo.png') }}" alt="cart"></a>
					@endif
					</div>
					
				</div>
				
				<div class="mob-menu">
				
					<div class="left">
						<a href="#" class="mob-menu-trigger"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span><span>MENU</span></a>
					</div>
				
					<div class="right">
						@if(!Auth::user())
						<a href="{{url('/cart')}}" class="login-trigger"><img src="{{ asset('bundles/img/mobile-userlogo.png') }}" alt="user"></a>
						@else
						<a href="{{url('/cart')}}" class="login-trigger"><img src="{{ asset('bundles/img/mobile-userlogo.png') }}" alt="user"></a>
						@endif
						<a href="{{url('/cart')}}"><img src="{{ asset('bundles/img/mobile-cart-logo.png') }}" alt="cart"></a>

					</div>
				</div>
				
				<nav class="menu">

					<ul>
						<li><a href="{{url('/about')}}">about us</a>
							<div class="underoos @if($menu == 'about')<?php echo 'active'; ?>@endif"></div>
						</li>
						<li><a href="{{url('/products')}}" class="submenu-trigger">products</a>
							<div class="arrow"><img src="{{asset('bundles/img/arrow-down-brown.png')}}"></div>
							<div class="underoos @if($menu == 'product')<?php echo 'active'; ?>@endif"></div>
							<div class="submenu">
								<ul>
									<li class="mob-show">
										<a href="{{url('/products')}}">All Products</a>
									</li>
									@foreach($categories as $category)
									<li>
										<a href="{{url('/products')}}/{{$category->taxonomy_slug}}">{{$category->name}}</a>
									</li>
									@endforeach
								</ul>
							</div>
						</li>
						<li><a href="{{url('/news')}}">news &amp; event</a>
							<div class="underoos @if($menu == 'news')<?php echo 'active'; ?>@endif"></div>
						</li>
						<li><a href="{{url('/special-offer')}}">special offer</a>
							<div class="underoos @if($menu == 'offer')<?php echo 'active'; ?>@endif"></div>
						</li>
						<li><a href="{{url('/contact-us')}}">contact us</a>
							<div class="underoos @if($menu == 'contact')<?php echo 'active'; ?>@endif"></div>
						</li>
						<li><a href="{{url('/reference')}}">references</a>
							<div class="underoos @if($menu == 'reference')<?php echo 'active'; ?>@endif"></div>
						</li>
					</ul>

				</nav>

				

			</div>
			@if(Session::has('success_msg'))
				<div class="text-success" style="
				    height: 50px;
				    width: 100%;
				    padding-top: 15px;
				    background: #bfd22b;
				    text-align: center;
				    color: black;
				    /* margin: 200px; */">
				    {{Session::get('success_msg')}}
				 </div>
			 @endif
			
		</header>
		
		
			<?php if(!empty($content)) echo $content; ?>
		</div>
		
		
		<footer style="height: 100%">
		
			<!-- <div class="logo-footer">
				<a href="{{url('/')}}"><img src="{{ asset('bundles/img/logo-small.png') }}" alt="neneq logo"></a>
			</div> -->
					@if($menu == 'index')
					<div style="position:relative;height:67px;background: url({{ asset('bundles/img/brown-gradient.png') }}) center center no-repeat;"></div>
					@endif

					<div style="min-height: 100%;margin-bottom: 20px;padding-top: 20px" class="row main-container">
						<div class="col-sm-3 col-md-3 text-left mobile-center"><a href="{{url('/')}}"><img src="{{ asset('bundles/img/logo-small.png') }}" alt="neneq logo"></a></div>
						<div class="col-sm-6 col-md-6 text-center mobile-space-bottom" style="color: white">
							<p style="margin-bottom: 5px">Copyright<img style="margin-bottom: 5px" src="{{ asset('bundles/img/c.png') }}"> {{ date('Y') }} Neneq<img src="{{ asset('bundles/img/r.png') }}">. All rights reserved.</p>
							<p>
								<a style="color: white" href="{{ url(getPrivacyPolicy()->slug) }}">{{ parseMultiLang(getPrivacyPolicy()->title) }}</a> | 
								<a style="color: white" href="{{ url(getTermAndCondition()->slug) }}">{{ parseMultiLang(getTermAndCondition()->title) }}</a> |
								<a style="color: white" href="{{ url(getFAQ()->slug) }}">{{ parseMultiLang(getFAQ()->title) }}</a> |
								<a style="color: white" href="{{ url(getShippingAndReturn()->slug) }}">{{ parseMultiLang(getShippingAndReturn()->title) }}</a> 
							</p>
						</div>
						<div class="col-sm-3 col-md-3 text-right mobile-center">
							<a style="margin: 10px" href="{{ app('AlphaSetting')->getSetting('facebook_fan_page') }}"><img src="{{ asset('bundles/img/facebook-icon.png') }}"></a>
							<a style="margin: 10px" href="{{ app('AlphaSetting')->getSetting('instagram') }}"><img src="{{ asset('bundles/img/instagram-icon.png') }}"></a>

						</div>
					</div>											
			
		</footer>

	</div>

	<script type="text/javascript">
	    var frm = $('#form-login');
	    var current = "{{ Request::url() }}";
	    frm.submit(function (ev) {
		
	        $.ajax({
	            type: "POST",
		    	dataType : "json",
	            url: frm.attr('action'),
	            data: frm.serialize(),
	            success: function (data) {
	            	if (data.status == 1) {
	            	    location.reload();
	            	}
	            				
	                $('.notmatch').empty();
	                $('.notmatch').html(data.errorss);
	            	$('.ermail').empty();
	                $('.ermail').html(data.errors.email);
	                $('.erpass').empty();
	                $('.erpass').html(data.errors.password);
	                
	            }
	        });

	        ev.preventDefault();
	    });
	</script>
	<script type="text/javascript">
		$(".underoos").click(function(e){
			window.location.href = ($(this).parents('li').find('a').attr('href'));
		})
		$(".menu li").click(function(e){
			window.location.href = ($(this).find('a').attr('href'));
		});
	</script>
</body>

</html>
