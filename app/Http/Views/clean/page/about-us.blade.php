        <div class="main"> 
        
            <div class="main-container about">

                <h1>ABOUT US</h1>

                <section class="aboutus top">
                    <div class="aboutus-banner ancient" style="background: url({{asset($about[0]->medias[0]->path)}});">
                        <div class="img-mobile">
                            <img src="{{ asset($about[0]->medias[0]->path) }}" alt="ancient secret family recipe">
                        </div>
                        <div class="text">
                            {!!parseMultiLang($about[0]->content)!!}
                        </div>
                    </div>

                    <div class="aboutus-banner founder" style="background: url({{asset($about[1]->medias[0]->path)}});">
                        <div class="img-mobile">
                            <img src="{{ asset($about[1]->medias[0]->path) }}" alt="ancient secret family recipe">
                        </div>
                        <div class="text">
                            {!!parseMultiLang($about[1]->content)!!}
                        </div>
                    </div>

                </section>
                <section class="aboutus bottom">
                    {!!parseMultiLang($about[2]->content)!!}
                </section>
                <section class="aboutus bottom mobile">
                    <div class="philosophy-swiper">

                        <div class="swiper-wrapper">

                            <div class="swiper-slide">
                                <div class="content">
                                    <h3>PHILOSOPHY : Care For You</h3>
                                    <p>A delicate touch to create long lasting beauty, Cantik Sejahtera does it with meaningful-product that would create a positive impact. Holding an unwavering commitment to honesty is the company's main philosophy. Cantik Sejahtera promises that all NeneQ Java Essentials, JE java essentials and PLUSH by je-which includes the skin care, hair care and fragrance series will deliver what it promised.</p>
                                    <div class="separator"></div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="content">
                                    <h3>MISSION</h3>
                                    <p>To continously invest in our in-house research and development in order to continue improving and maintaining beauty worldwide.</p>
                                    <p>The company believes in beautifying women through natural organic substance. We will continue to understand the needs of customer's personal and beauty care and satisfy their request with our various ranges of products.</p>
                                    <div class="separator"></div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="content">
                                    <h3>VISION</h3>
                                    <p>Cantik Sejahtera aims to become the top leading beauty products manufacturing and distributing company in Indonesia and also to be world recognised-by collaborating with world leading beauty products companies.</p>

                                </div>
                            </div>

                        </div>


                    </div>
                    <div class="swiper-pagination home-product new"></div>
                </section>
                <section class="aboutus last">
                    <div class="rnd">

                        <div class="left">
                            <img src="{{ asset($about[3]->medias[0]->path) }}" alt="sandwood">
                        </div>
                        <div class="left">
                            {!!parseMultiLang($about[3]->content)!!}
                        </div>

                    </div>
                </section>
                
            </div>

        </div>
    </div>
