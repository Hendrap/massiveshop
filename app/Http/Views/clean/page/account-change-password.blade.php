		
		<div class="main">
		
			<div class="main-container account">
			
				<h1>ACCOUNT</h1>
				
				<div class="account-cont">
				
				
					<div class="right">
					
						<ul>
						
							<li>
								<a href="{{url('/account')}}" >Account Dashboard</a>
							</li>
							<li>
								<a href="{{url('/account/order-history')}}">Order History</a>
							</li>
							<li class="active">
								<a href="{{url('/account/change-password')}}" class="active">Change Password</a>
							</li>
							
						</ul>
					
					</div>
					
				
					<div class="left">
						
						<div class="account-detail">
						
							<div class="block change-pswd">
								<p class="head-block change-password-title">Change Password</p>
								@if(Session::get('success') == 'success')
								<p class="head-block" style="font-weight:400"><span>Success!</span> Your password have been changed.</p>
								<script>
								
									$('.change-password-title').hide();
									
								</script>
								{{Session::forget('success')}}
								@endif
								
								<div class="separator"></div>
								<form method="POST" @if(Auth::user()) action="{{url('/change-password-confirm')}}" @else action="{{url('/forget-password-confirm')}}" @endif>
								{{ csrf_field() }}
								<table width="100%;">
									<tr>
										<td>Email Address</td>
										<td><input type="email" name="email" value="@if(!empty($data->meta_name)) {{$data->meta_name}} @endif @if(!empty(Auth::user())) {{Auth::user()->email}} @endif" readonly></td>
									</tr>
									@if(Auth::user())
									<tr>
										<td>Old Password</td>
										<td>
											<input type="password" name="old-password">
											@if(count($errors->get('old-password')))
												<div class="left"><span class="text-danger" style="display:inline">{{$errors->first('old-password')}}</span></div>
											@endif
										</td>
										
									</tr>
									@endif
									<tr>
										<td>New Password</td>
										<td>
											<input type="password" name="password">
											@if($errors->first('password') == "The password confirmation does not match.")
											@elseif(count($errors->get('password')))
											
												<div class="left"><span class="text-danger" style="display:inline">{{$errors->first('password')}}</span></div>
											
											@endif
										</td>
									</tr>

									<tr>
										<td>Retype New Password </td>
										<td>
											<input type="password" name="password_confirmation">
											@if(count($errors->get('password_confirmation')) || $errors->first('password') == "The password confirmation does not match.")
												<div class="left"><span class="text-danger" style="display:inline">@if($errors->first('password') == "The password confirmation does not match.") {{$errors->first('password')}} @else {{$errors->first('password_confirmation')}} @endif</span></div>
											@endif
										</td>
										
									</tr>
								
								</table>
								
								<button type="submit">SUBMIT</button>
								</form>
							</div>
							
							
							
						</div>
						
					</div>
					
				</div>
				
			</div>
		
		</div>
