		<script type="text/javascript" src="{{ asset('bundles/js/vendor/bootstrap.min.js') }}"></script>

		<div class="main">
		
			<div class="main-container account">
			
				<h1>ACCOUNT</h1>
				
				<div class="account-cont">
				
				
					<div class="right">
					
						<ul style="margin-top: 24px;">
						
							<li style="border-bottom: 1px solid #bfd22b">
								<a href="{{url('/account')}}" class="active">Account Dashboard</a>
							</li>
							<li style="border-bottom: 1px solid #bfd22b">
								<a href="{{url('/account/order-history')}}">Order History</a>
							</li>
							<li style="border-bottom: 1px solid #bfd22b">
								<a href="{{url('/account/change-password')}}">Change Password</a>
							</li>
							
						</ul>
						
					<!-- 	<div class="address-list">
							@if(Session::get('success') == 'success')
								<p class="head-block"><span>Success!</span></p>
							@endif
							<h4>Your Shipping Addresses</h4>
							
							<div class="separator"></div>
							
							<div class="content">
				                <div class="address-block">
									@foreach($shipping as $ship)
									<div class="address">
										<div class="address-name"><span>{{$ship->label}}</span></div>
										<div class="address-detail">
											<p>
												<b>{{$ship->first_name}} {{$ship->last_name}}</b><br>
												{{$ship->address}}<br>
												{{$ship->state}}<br>
												{{$ship->city}} {{$ship->postal_code}}
											</p>
											<a href="{{url('delete-address')}}/{{$ship->id}}">delete</a>&nbsp;|&nbsp;<a href="{{url('edit-shipping-address')}}/{{$ship->id}}">edit</a>
										</div>
									</div>
									@endforeach
									
									<a href="{{url('/add-address')}}">Add Address</a>
				                </div>
							</div>
							
						</div>
					 -->
					</div>
					
				
					<div class="left">
						
						<div class="account-detail">
							@if(Session::has('success_msg_account'))
							<div class="alert alert-success">
								<strong>Success!</strong> {{Session::get('success_msg_account')}}
							</div>
							@endif

							<div class="block">
								<p class="head-block">Welcome @if(!empty($billing->first_name)) {{$billing->first_name}} @endif @if(!empty($billing->last_name)) {{$billing->last_name}} @endif.</p>
								<p class="head-block">Here’s the detail of your account</p>
								
								<div class="separator"></div>
								
								<table>
								
									<tr>
										<td>Full Name</td>
										<td>@if(!empty($billing->first_name)) {{$billing->first_name}} @endif @if(!empty($billing->last_name)) {{$billing->last_name}} @endif</td>
									</tr>
									<tr>
										<td>Company Name</td>
										<td>@if(!empty($billing->company)) {{$billing->company}} @endif</td>
									</tr>
									<tr>
										<td>Address</td>
										<td>@if(!empty($billing->address)) {{$billing->address}} @endif</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>@if(!empty($billing->state)) {{$billing->state}}, @endif @if(!empty($billing->city)) {{$billing->city}} @endif @if(!empty($billing->postal_code)) {{$billing->postal_code}}. @endif @if(!empty($billing->country)) {{$billing->country}} @endif</td>
									</tr>
									<tr>
										<td>Phone</td>
										<td>@if(!empty($billing->phone)) {{$billing->phone}} @endif</td>
									</tr>
								
								</table>
								
								<a href="{{ url('edit-billing-address/'.$billing->id) }}">EDIT ACCOUNT</a>
							</div>
							
							@if(!empty($shipping))
							<div class="block">
								<p class="head-block">Your Shipping Addresses</p>
								
								
								<div class="separator"></div>
								
								<table>
									@foreach($shipping as $ship)
									<tr>
										<td>{{$ship->label}}</td>
										<td style="padding-left: 44px;">{{$ship->first_name}} {{$ship->last_name}}</td>
										<td><a style="all:unset;" href="{{url('edit-shipping-address')}}/{{$ship->id}}">Edit</a>&nbsp;|&nbsp;<a class="deleteBtn" style="all:unset;color:red" href="{{url('delete-address')}}/{{$ship->id}}">Delete</a></td>
									</tr>
									@endforeach
								
								</table>
								
								<a href="{{url('/add-address')}}">Add Address</a>
							</div>
							@endif

						</div>
						
					</div>
					
				</div>
				
			</div>
		
		</div>
		<div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Are you sure ?</h4>
		        </div>
		        <div class="modal-footer">
		          <button id="confirmDeleteButton" type="button" class="btn btn-danger">Delete</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		    </div>
		 </div>
		<script type="text/javascript">
			var currentUrl = "";
			$(".deleteBtn").click(function(e){
					e.preventDefault();
					currentUrl = $(e.target).attr('href');
					$("#myModal").modal();
			});

			$("#confirmDeleteButton").click(function(e){
					e.preventDefault();
					window.location.href = currentUrl;
			});
		</script>