		
		<div class="main">
		
			<div class="main-container account">
			
				<h1>ORDER HISTORY</h1>
				
				<div class="account-cont">
				
				
					<div class="right">
					
						<ul>
						
							<li>
								<a href="{{url('/account')}}" >Account Dashboard</a>
							</li>
							<li class="active">
								<a href="{{url('/account/order-history')}}" class="active">Order History</a>
							</li>
							<li>
								<a href="{{url('/account/change-password')}}">Change Password</a>
							</li>
							
						</ul>
					
					</div>
					
				
					<div class="left">
						
						
							<div class="order-detail">
								
								<div>
									<div class="left"><h5>ORDER #{{ $order->order_number }}</h5></div>
									<div class="right"><h5>STATUS: {{ $order->status }}</h5></div>
								</div>
								<div class="billship">
									<div class="left">
										
										<h6>BILLING</h6>
										
										<p>{{ $order->billing->first_name.' '.$order->billing->last_name }}</p>
										<p>{{ $order->billing->address }}</p>
										<p>{{ $order->billing->country }}</p>
										<p>{{ $order->billing->city.' '.$order->billing->postal_code }}</p>
									
									</div>
									<div class="right">
										
										<h6>SHIPPING</h6>
									
										<p>{{ $order->shipping->first_name.' '.$order->shipping->last_name }}</p>
										<p>{{ $order->shipping->address }}</p>
										<p>{{ $order->shipping->country }}</p>
										<p>{{ $order->shipping->city.' '.$order->shipping->postal_code }}</p>
										
									</div>
								</div>
								
								<div class="separator"></div>
								
								<table class="list-item">
								
									<thead>
										<tr>
											<td colspan="2" style="width:50%">
												<span>PRODUCT DETAILS</span>
											</td>
											<td style="width: 15%">
												<span>QTY</span>
											</td>
											<td class="text-left-important" style="width: 35%">
												<span>PRICE</span>
											</td>
										</tr>
									</thead>
									<tbody>
										
										@foreach($order->details as $item)
										<tr>
										<td>
												<img src="{{ $item->image_thumbnail }}" alt="{{ $item->product_title }}">
											</td>
											<td>
												<p>{{ $item->product_title }}</p>
												<span class="mob-data">QTY: {{ $item->qty }}</span>
												<span class="mob-data">PRICE: IDR {{ number_format($item->original_price) }}</span>
											</td>
											<td>
												<span>{{ $item->qty }}</span>
											</td>
											<td class="text-left-important">
												<span>IDR {{ number_format($item->original_price) }}</span>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								
								<table class="total-table">
									<tr>
										<td class="order-history-subtotal"><span>SUBTOTAL</span></td>
										<td><span>IDR {{ number_format($order->subtotal_before_discount) }}</span></td>
									</tr>
									<tr>
										<td class="order-history-subtotal"><span>SHIPPING</span></td>
										<td><span>IDR {{ number_format($order->shipping_amount) }}</span></td>
									</tr>
									<tr class="total">
										<td class="order-history-subtotal"><span>TOTAL</span></td>
										<td><span>IDR {{ number_format($order->grand_total) }}</span></td>
									</tr>
								</table>
								
								<div class="buttons-order-detail">
									<div class="left">
										<a href="{{url('/account')}}">back</a>
									</div>
								</div>
							</div>
						
						
					</div>
					
				</div>
				
			</div>
		
		</div>
