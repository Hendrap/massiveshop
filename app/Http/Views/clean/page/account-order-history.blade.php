
		<div class="main">
		
			<div class="main-container account">
			
				<h1>ORDER HISTORY</h1>
				
				<div class="account-cont">
				
				
					<div class="right">
					
						<ul>
						
							<li>
								<a href="{{url('/account')}}">Account Dashboard</a>
							</li>
							<li class="active">
								<a href="{{url('/account/order-history')}}" class="active">Order History</a>
							</li>
							<li>
								<a href="{{url('/account/change-password')}}" >Change Password</a>
							</li>
							
						</ul>
					
					</div>
					
				
					<div class="left">
						
						
							<div class="order-list">
								<table>
									<thead>
										<td>Order Number</td>
										<td>Date</td>
										<td>Status</td>
										<td>&nbsp;</td>
									</thead>
									@if(count($data))
									@foreach($data as $order)
									<tr>
										<td>#{{$order->order_number}}</td>
										<td>{{date_format(date_create($order->created_at),'j/n/y')}}</td>
										<td>{{$order->status}}</td>
										<td><a href="{{url('/account/order-detail')}}/{{$order->id}}">VIEW ORDER</a></td>
									</tr>
									<tr class="mobile-link">
										
										<td colspan="3"><a href="{{url('/account/order-detail')}}/{{$order->id}}">VIEW ORDER</a></td>
										
									</tr>
									@endforeach
				    @else
					<tr>
						<td style="text-align: left;padding-left: 12px;">No Order</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				    @endif
								</table>
							</div>
			    @if(!count($data))
				<div class="cart-buttons" style="padding-top:50px;">
					<a href="{{url('/products')}}" class="continue-shopping">CONTINUE SHOPPING</a>
				</div>
			    @endif
						{!! $data->render() !!}
						
					</div>
					
				</div>
				
			</div>
		
		</div>
