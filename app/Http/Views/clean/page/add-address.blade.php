     
        <div class="main">
         
            <div class="main-container register">
            
                <h1>SHIPPING ADDRESS</h1>
                
                <form id="form-register" action="{{url('/save-shipping-address')}}" method="POST">
                {{ csrf_field() }}
                    <h4>Please fill your additional shipping detail</h4>
                    <div class="separator"></div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Address Name<span> *</span>
                            </label>
                            <input type="text" name="label">
                        </div>
                        @if(count($errors->get('label')))
                            <div class="left"><span class="text-danger">{{$errors->first('label')}}</span></div>
                        @endif
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Receiver Name<span> *</span>
                            </label>
                            <input type="text" name="first_name">
                        </div>
                        @if(count($errors->get('first_name')))
                            <div class="left"><span class="text-danger">{{$errors->first('first_name')}}</span></div>
                        @endif
                        
                    </div>
                    
                    
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Company Name
                            </label>
                            <input type="text" name="company">
                        </div>
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Address<span> *</span>
                            </label>
                            <input type="text" name="address" placeholder="Street Address">
                        </div>
                        @if(count($errors->get('address')))
                            <div class="left"><span class="text-danger">{{$errors->first('address')}}</span></div>
                        @endif
                        
                    </div>
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               &nbsp;
                            </label>
                            <input type="text" name="address2" value="@if(!empty($data->address)){{$data->address}}@endif" placeholder="Apartment, suite, unit etc. (optional)">
                        </div>
                        <div class="left"></div>
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Country<span> *</span>
                            </label>
                            <select name="country">
                                @foreach($country as $value)
                                    <option value="{{$value->name}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if(count($errors->get('country')))
                            <div class="left"><span class="text-danger">{{$errors->first('country')}}</span></div>
                        @endif
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Town / City<span> *</span>
                            </label>
                            <input type="text" name="city" placeholder="Street Address" id="city">
                        </div>
                        @if(count($errors->get('city')))
                            <div class="left"><span class="text-danger">{{$errors->first('city')}}</span></div>
                        @endif
                        
                    </div>

		            <div class="input-row">
                    
                        <div class="left">
                            <label>
                               District<span> *</span>
                            </label>
                            <input type="text" name="district" placeholder="District" id="district">
                        </div>
                        @if(count($errors->get('district')))
                            <div class="left"><span class="text-danger">{{$errors->first('district')}}</span></div>
                        @endif
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               State<span> *</span>
                            </label>
                            <input type="text" name="state" placeholder="">
                        </div>
                       @if(count($errors->get('state')))
                            <div class="left"><span class="text-danger">{{$errors->first('state')}}</span></div>
                        @endif
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                              Zip<span> *</span>
                            </label>
                            <input type="text" name="postal_code" placeholder="">
                        </div>
                        @if(count($errors->get('postal_code')))
                            <div class="left"><span class="text-danger">{{$errors->first('postal_code')}}</span></div>
                        @endif
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Phone<span> *</span>
                            </label>
                            <input type="tel" name="phone" placeholder="">
                        </div>
                        @if(count($errors->get('phone')))
                            <div class="left"><span class="text-danger">{{$errors->first('phone')}}</span></div>
                        @endif
                        
                    </div>
                        <button type="submit">CONTINUE</button>
                        
                 
                </form>
                
            </div>
            
        
        </div>

<script type="text/javascript">
   $( document ).ready(function() {
        // $( "#city" ).autocomplete({
        //     source: "{{ URL::to('getcity') }}",
        //     minLength: 1,
        //     search: function( event, ui ) {
        //         $( "#district" ).val('');
        //     }
        // });

        // $("#district").autocomplete({
        //   source: function(request, response) {
        //     $.getJSON("{{ URL::to('getdistrict') }}", { city: $('#city').val(), term:$('#district').val() },
        //               response);
        //   },
        //   minLength: 1,
        // });
    });
</script>

{{--autocomplete style--}}
<style type="text/css">
    .ui-autocomplete {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 999999999;
        float: left;
        display: none;
        min-width: 160px;
        padding: 4px 0;
        margin: 0 0 10px 25px;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }

    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }

    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }

    .ui-helper-hidden-accessible{
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
</style>
