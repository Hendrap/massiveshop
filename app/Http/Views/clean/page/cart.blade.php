
        <div class="main">

            <div class="main-container cart">
                    <h1>SHOPPING CART</h1>
                    @if(count($items))
                        <table class="table-cart">
                            @foreach($items as $item)
                                <tr>
                                    <td>
                                    
                                    <img src="{{ $item->image_cart }}" alt="{{ $item->product_title }}">
                                    
                                    </td>
                                    <td>
                                        <h6>{{ $item->product_title }}</h6>
                                        <div class="mob-data"><span>x {{$item->qty}} </span></div>
                                        <div class="mob-data"><span>{{ getCurrencySymbol() }} {{ formatMoney($item->original_price) }}</span></div>
                                        <a href="#" onclick="remove_from_cart(<?= $item->item_id ?> , <?= $item->user_id ?>)">REMOVE</a>
                                    </td>
                                    <td align="right"><span>x {{$item->qty}}</span></td>
                                    <td align="right"><span>{{ getCurrencySymbol() }} {{ formatMoney($item->original_price) }}</span></td>
                                </tr>
                            @endforeach
                        </table>
                        <table class="table-cart-total" align="right">
                    
                            <tr>
                                <td>SUBTOTAL</td>
                                <td>{{ getCurrencySymbol() }} {{ formatMoney($subTotal) }}</td>
                            </tr>
                            
                        </table>
                    @else
                        <h1 align="center">Cart is empty</h1>
                    @endif
                        <div class="cart-buttons">
                            <a href="{{url('/products')}}" class="continue-shopping">CONTINUE SHOPPING</a>
                            @if(count($items))
                            <a href="{{url('/checkout')}}" class="proceed-checkout">PROCEED TO CHECK OUT!</a>
                            @endif
                        </div>
                
            </div>
            
        </div>
<script type="text/javascript">
    function remove_from_cart(item_id, userID) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            type: 'POST',
            dataType: 'json',   
            url: '<?php echo url("/remove-from-cart"); ?>',
            data: {item_id : item_id},
            success: function(data,status) {
                location.reload();
            }
        })
    }
</script>
