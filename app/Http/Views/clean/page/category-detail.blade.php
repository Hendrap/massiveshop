		<div class="main">

			<div class="main-container cat-detail">
				<h1>{{$selectedCategory->name}}</h1>

				<div class="top-image">
					<?php
						 $doc = new DOMDocument();
                            $doc->loadHTML($selectedCategory->description);
                            $xpath = new DOMXPath($doc);
                            $src = $xpath->evaluate("string(//img/@alt)"); 

						$getimage = DB::table('medias')->where('title','LIKE','%'.$src.'%')->first();
						$totalCategories = count($selectedCategory->childs) - 1;
					?>

					@if(!empty($getimage))
						<img alt="{{asset($getimage->title)}}" class="right" src="{{asset(getCropImage($getimage->path, 'category'))}}" style="margin-left:15px" />
					@else
						<img alt="" class="right" src="{{asset('bundles/img/neneq-white-logo.png')}}" style="margin-left:15px; background:grey;" />
					@endif
				</div>
				<div class="separator">
					<img src="{{asset('bundles/img/separator-white-2.png')}}" alt="separtor">
				</div>

				@foreach($selectedCategory->childs as $key => $category)
					<div class="product-cat-container">
						
								
							<h3>{{ $category->name }}</h3>
							<div class="sub-separator"></div>
							<p>{!! $category->description !!}</p>
							<div class="row category-detail">
							@foreach($category->activeEntries as $entry)
								<div class="col-sm-4 col-xs-12">
									{!! view('app::clean.templates.product',['product'=>$entry]) !!}
								</div>
							@endforeach
							</div>
					</div>
					@if($totalCategories != $key)
					<div class="separator">
						<img src="{{asset('bundles/img/separator-white-2.png')}}">
					</div>
					@endif
				@endforeach


				<div style="display: none;" class="product-cat-container">
					<h3> Product Not Found</h3>
				</div>

			</div>

		</div>
<script type="text/javascript">
	if($(".product-cat-container").length == 0) $(".product-cat-container").show();
</script>
