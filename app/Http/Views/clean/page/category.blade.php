    
        <div class="main">
        
            <div class="main-container category">
                <h1>PRODUCTS</h1>
                
                <section class="categories">
                @foreach($data as $cat)
                   <div class="banner-category">
                        <?php
                            $doc = new DOMDocument();
                            $doc->loadHTML($cat->description);
                            $xpath = new DOMXPath($doc);
                            $src = $xpath->evaluate("string(//img/@alt)"); 

                            $getimage = DB::table('medias')->where('title','LIKE','%'.$src.'%')->first();
                        ?>
			@if(@$getimage->path)
                        <a href="{{url('/products')}}/{{$cat->taxonomy_slug}}"><img alt="product-banner-1-1464074155" class="right" src="{{asset(getCropImage(@$getimage->path, 'category'))}}" style="margin-left:15px" />
			@else			
			<a href="{{url('/products')}}/{{$cat->taxonomy_slug}}"><img alt="product-banner-1-1464074155" class="right" src="{{asset('bundles/img/neneq-white-logo.png')}}" style="margin-left:15px; background:grey;" />
			@endif
                        <h3>{{$cat->name}}</h3></a>
                    </div>
                @endforeach
                </section>
                    
            </div>
        
        </div>
