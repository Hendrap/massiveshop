 
        <div class="main">


            <div class="main-container checkout">
                <h1>CHECKOUT</h1>
                
                
                <div class="fancy-checkout-header">
                
                    <ul>
                    
                        <li class="active">
                            <span>LOGIN / REGISTER</span>
                            <div class="round-indicator active"></div>
                        </li>
                        <li>
                            <span>SHIPPING</span>
                            <div class="round-indicator"></div>
                        </li>
                        <li>
                            <span>PAYMENT</span>
                            <div class="round-indicator"></div>
                        </li>
                        <li>
                            <span>CONFIRM</span>
                            <div class="round-indicator"></div>
                        </li>
                        
                    </ul>
                    <div class="separator"></div>
                </div>
                
                <form method="POST" action="{{url('/checkout/guest-register')}}">
                {{ csrf_field() }}
                <div class="checkout-login">
                
                    <div class="left">
                    
                        <h4>GUEST CHECKOUT</h4>
                        
                        <div class="left">
                            <span class="text-danger">

                            @if(!empty(Session::get('msg')))
                                Email already registered. 
                            @endif

                            @if($errors->has('first_name') || $errors->has('last_name') || $errors->has('email'))
                                Please check the input.
                            @endif

                            @if($errors->has('password'))
                                {{ $errors->first('password') }}
                            @endif

                            </span>
                            <br><br>
                        </div>

                        <div class="input-row">
                            <input value="{{ old('first_name') }}" type="text" @if($errors->has('first_name')) style="border:1px red solid" @endif class="half" name="first_name" placeholder="Firstname">
                            <input value="{{ old('last_name') }}" type="text" @if($errors->has('last_name')) style="border:1px red solid" @endif class="half" name="last_name" placeholder="Lastname">
                        </div>
                        <div class="input-row">
                            <input value="{{ old('email') }}" type="email" @if($errors->has('email') || !empty(Session::get('msg'))) style="border:1px red solid" @endif name="email" placeholder="E-mail Address">
                        </div>
                        <div class="input-row">
                            <input type="password" @if($errors->has('password')) style="border:1px red solid" @endif class="half" name="password" placeholder="Password">
                            <input type="password" @if($errors->has('password')) style="border:1px red solid" @endif class="half" name="password_confirmation" placeholder="Re-type password">
                        </div>

                    </div>
                    <div class="right">
                    
                        <h4>Have an account?</h4>
                        
                        <a class="login-trigger" href="javascript:void(0)">LOGIN</a>
                        <a href="javascript:void(0)" class="fb neneq-fb-login"><i class="fa fa-facebook"></i> LOGIN VIA FACEBOOK</a>
                        
                    </div>
                    
                </div>
                <div class="cart-buttons">
                
                        <a href="{{ url('/') }}" class="continue-shopping">CONTINUE SHOPPING</a>
                        <button type="submit" class="proceed-checkout">CONTINUE</button>
                        
                </div>
                </form>
            </div>
            
            
        </div>