
		<div class="main">

			<div class="main-container checkout">
				<h1>CHECKOUT</h1>
				<div class="fancy-checkout-header">

					<ul>

						<li>
							<span>LOGIN / REGISTER</span>
							<div class="round-indicator"></div>
						</li>
						<li>
							<span>SHIPPING</span>
							<div class="round-indicator"></div>
						</li>
						<li class="active">
							<span>PAYMENT</span>
							<div class="round-indicator  active"></div>
						</li>
						<li>
							<span>CONFIRM</span>
							<div class="round-indicator"></div>
						</li>

					</ul>
					<div class="separator"></div>
				</div>

 
				<div class="checkout-payment">

					<div class="left">

						<div class="order-detail">

							<div>
								<div class="left"></div>
								<div class="right"></div>
							</div>
							<div class="billship">
								<div class="left nomarginmargin">

									<h6>BILLING</h6>

									<p class="bold">
									@if(!empty($billing->first_name)){{$billing->first_name}}@endif @if(!empty($billing->last_name)){{$billing->last_name}}@endif</p>
									<p>@if(!empty($billing->address)){{$billing->address}}@endif</p>
									<p>@if(!empty($billing->city)){{$billing->city}}@endif</p>
									<p>@if(!empty($billing->district)){{$billing->district}}@endif @if(!empty($billing->postal_code)){{$billing->postal_code}}@endif</p>

								</div>
								<div style="padding-left: 90px!important" class="right nomarginmargin">

									<h6>SHIPPING</h6>

									<p class="bold">@if(!empty($shipping->first_name)){{$shipping->first_name}}@endif @if(!empty($shipping->last_name)){{$shipping->last_name}}@endif</p>
									<p>@if(!empty($shipping->address)){{$shipping->address}}@endif</p>
									<p>@if(!empty($shipping->city)){{$shipping->city}}@endif</p>
									<p>@if(!empty($shipping->district)){{$shipping->district}}@endif @if(!empty($shipping->postal_code)){{$shipping->postal_code}}@endif</p>

								</div>
							</div>

							<div class="separator"></div>
			
							<table class="list-item">

								<thead>
									<tr>
										<td colspan="2" style="width:50%">
											<span>PRODUCT DETAILS</span>
										</td>
										<td style="width: 10%">
											<span>QTY</span>
										</td>
										<td class="tdoneline" style="width: 40%">
											<span>PRICE</span>
										</td>
									</tr>
								</thead>
								<tbody>
								<?php
									
								?>
								@foreach($items as $item)
									<tr>
										<td>
											<img src="{{ ($item->image_cart) }}" alt="{{ ($item->product_title) }}">

										</td>
										<td>
											<span>{{ ($item->product_title) }}</span>
											<span class="mob-data">QTY: {{ ($item->qty) }}</span>
											<span class="mob-data">PRICE: {{ getCurrencySymbol() }} {{ formatMoney($item->original_price) }}</span>
										</td>
										<td>
											<span>{{ ($item->qty) }}</span>
										</td>
										<td class="tdoneline">
											<span>{{ getCurrencySymbol() }} {{ formatMoney($item->original_price) }}</span>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>

							<table class="total-table">
								<tr>
									<td style="width: 30%"></td>
									<td style="width: 30%"><span>SUBTOTAL</span></td>
									<td class="tdoneline" style="width: 40%;padding-left: 11%"><span>{{ getCurrencySymbol() }} {{ formatMoney($subTotal) }}</span></td>
								</tr>
								<tr>
									<td style="width: 30%"></td>
									<td style="width: 30%"><span>SHIPPING</span></td>
									<td class="tdoneline" style="width: 40%;padding-left: 11%"><span>{{ getCurrencySymbol() }} {{ formatMoney($selectedShippingMethod->ShippingCharge) }}</span></td>
								</tr>
								<tr class="total">
									<td style="width: 30%"></td>
									<td style="width: 30%"><span>TOTAL</span></td>
									<td class="tdoneline" style="width: 40%;padding-left: 11%"><span>{{ getCurrencySymbol() }} {{ formatMoney($grandTotal) }}</span></td>
								</tr>
							</table>
					
						</div>

					</div>

					<div class="right">
						<form action="{{url('/checkout/place-order')}}" method="POST">
						{{ csrf_field() }}
							<div class="payment-panel">
						
								<h4>Payment</h4>
								<div class="separator"></div>
								
								<select class="method-select" name="payment_method" id="method-select">
										<option value="credit">Credit Card</option>
										<option value="paypal">Paypal</option>
								</select>

								
								
								<p>For any questions related to payment process, please contact <a href="mailto:careforeyou@neneq.com">careforyou@neneq.com</a></p>
								@if(count($errors->get('terms_and_conditions')))
		                    		<div class="left"><span class="text-danger">Please check terms and conditions agreement!<br><br></span></div>
		                		@endif
								<div class="agree">
									<input type="checkbox" name="terms_and_conditions" value="accept">
									<p>I hereby agree to the terms of sales provided by CV. Cantik Sejahtera</p>
								</div>
								
								<button type="submit">CONTINUE</button>
							</div>
						</form>
					</div>
					
				</div>
				
			</div>


		</div>
		<style type="text/css">
			.nomarginmargin p{
				margin-bottom: 3px!important;
			}

			.tdoneline{
				padding-left: 10%;
				text-align: left!important;
			}
		</style>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#method-select").niceSelect();
			});
		</script>
	
