

		<div id="vueBillAndShip" class="main">


			<div class="main-container checkout">
				<h1>CHECKOUT</h1>


				<div class="fancy-checkout-header">

					<ul>

						<li>
							<span>LOGIN / REGISTER</span>
							<div class="round-indicator"></div>
						</li>
						<li class="active">
							<span>SHIPPING</span>
							<div class="round-indicator active"></div>
						</li>
						<li>
							<span>PAYMENT</span>
							<div class="round-indicator"></div>
						</li>
						<li>
							<span>CONFIRM</span>
							<div class="round-indicator"></div>
						</li>

					</ul>
					<div class="separator"></div>
				</div>


				<div class="checkout-shipping">

					<div class="left">

						<form class="form-shipping" method="POST" action="{{url('/checkout/billing-and-shipping/save')}}">
						{{ csrf_field() }}
							<h4>Account Detail</h4>
							<div class="separator"></div>

		             <div class="input-row">
                        <div class="left">
                            <label>
                                First Name<span> *</span>
                            </label>
                            <input type="text"  name="billing[first_name]" value="{{old('billing.first_name',$billing->first_name)}}" class="firstname">
                        </div>
                        @if(count($errors->get('billing.first_name')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.first_name'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Last Name<span> *</span>
                            </label>
                            <input type="text"  name="billing[last_name]" value="{{old('billing.last_name',$billing->last_name)}}" class="lastname">
                        </div>
                        @if(count($errors->get('billing.last_name')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.last_name'))}}</span></div>
                        @endif
                    </div>
                    
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Company Name
                            </label>
                            <input type="text"  name="billing[company]" value="{{old('billing.company',$billing->company)}}">
                        </div>
                        @if(count($errors->get('billing.company')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.company'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Address<span> *</span>
                            </label>
                            <input type="text"  name="billing[address]" placeholder="Street Address" value="{{old('billing.address')}}">
                        </div>
                        @if(count($errors->get('billing.address')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.address',$billing->address))}}</span></div>
                        @endif
                    </div>
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               &nbsp;
                            </label>
                            <input type="text" placeholder="Apartment, suite, unit etc. (optional)" name="billing[address_2]" value="{{old('billing.address_2',$billing->address_2)}}">
                        </div>
                        <div class="left"></div>
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Country<span> *</span>
                            </label>
                            <select @change="onRequiredAddressChange" class="reqFieldDHL" v-model="billCountry" name="billing[country]">
                                @foreach($country as $value)
                                    <option value="{{$value->name}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if(count($errors->get('billing.country')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.country'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Town / City<span> *</span>
                            </label>
                            <input @change="onRequiredAddressChange" type="text"  name="billing[city]" placeholder="" class="city reqFieldDHL" v-model="billCity">
                        </div>
                        @if(count($errors->get('billing.city')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.city'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               District<span> *</span>
                            </label>
                            <input type="text"  name="billing[district]" placeholder="" class="district" value="{{old('billing.district',$billing->district)}}">
                        </div>
                        @if(count($errors->get('billing.district')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.district'))}}</span></div>
                        @endif
                    </div>

                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               State<span> *</span>
                            </label>
                            <input type="text"  name="billing[state]" placeholder="" value="{{old('billing.state',$billing->state)}}">
                        </div>
                        @if(count($errors->get('billing.state')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.state'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                              Zip<span> *</span>
                            </label>
                            <input @change="onRequiredAddressChange" class="reqFieldDHL" v-model="billPostalCode" type="text"  name="billing[postal_code]" placeholder="" id="postal_code">
                        </div>
                        @if(count($errors->get('billing.postal_code')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.postal_code'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Phone<span> *</span>
                            </label>
                            <input type="tel"  name="billing[phone]" id="phone" placeholder="" value="{{old('billing.phone',$billing->phone)}}">
                        </div>
                        @if(count($errors->get('billing.phone')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.phone'))}}</span></div>
                        @endif
                    </div>



							<div class="input-row checks">

								<label>Ship to a Different Address?</label>
                                <input @change="onRequiredAddressChange" class="reqFieldDHL" v-model="billToShip" type="checkbox" name="billToShip" value="no">
							</div>
							<div class="separator"></div>



							<div class="shipping-info-reg">

								
							 <div class="input-row">
	                            <div class="left">
	                                <label>
	                                    Receiver Name<span> *</span>
	                                </label>
	                                <input placeholder="First Name" type="text" name="shipping[first_name]" value="{{old('shipping.first_name',$shipping->first_name)}}" class="firstname">
	                            </div>
	                            @if(count($errors->get('shipping.first_name')))
	                                <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.first_name'))}}</span></div>
	                            @endif
	                        </div>
	                    
	                        <div class="input-row">

	                            <div class="left">
	                                <label>
	                                    Address<span> *</span>
	                                </label>
	                                <input type="text" name="shipping[address]" placeholder="Street Address" value="{{old('shipping.address',$shipping->address)}}">
	                            </div>
	                            @if(count($errors->get('shipping.address')))
	                                <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.address'))}}</span></div>
	                            @endif
	                        </div>
	                        <div class="input-row">

	                            <div class="left">
	                                <label>
	                                   &nbsp;
	                                </label>
	                                <input type="text" placeholder="Apartment, suite, unit etc. (optional)" name="shipping[address_2]" value="{{old('shipping.address_2',$shipping->address_2)}}">
	                            </div>
	                            <div class="left"></div>
	                        </div>
	                        <div class="input-row">
	                    
	                            <div class="left">
	                                <label>
	                                   Country<span> *</span>
	                                </label>
	                                <select @change="onRequiredAddressChange" class="reqFieldDHL" v-model="shipCountry" name="shipping[country]">
	                                    @foreach($country as $value)
	                                        <option value="{{$value->name}}">{{$value->name}}</option>
	                                    @endforeach
	                                </select>
	                            </div>
	                            
	                            @if(count($errors->get('shipping.country')))
	                                <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.country'))}}</span></div>
	                            @endif
	                        </div>
	                    	<div class="input-row">
	                    
	                        <div class="left">
	                            <label>
	                               Town / City<span> *</span>
	                            </label>
	                            <input @change="onRequiredAddressChange" v-model="shipCity" type="text" name="shipping[city]" placeholder="" class="reqFieldDHL rcity">
	                        </div>
	                        @if(count($errors->get('shipping.city')))
	                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.city'))}}</span></div>
	                        @endif
	                    </div>
	                    
	                    <div class="input-row">
	                    
	                        <div class="left">
	                            <label>
	                               District<span> *</span>
	                            </label>
	                            <input type="text" name="shipping[district]" placeholder="" class="rdistrict" value="{{old('shipping.district',$shipping->district)}}">
	                        </div>
	                        @if(count($errors->get('shipping.district')))
	                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.district'))}}</span></div>
	                        @endif
	                    </div>

	                    <div class="input-row">
	                    
	                        <div class="left">
	                            <label>
	                               State<span> *</span>
	                            </label>
	                            <input type="text" name="shipping[state]" placeholder="" value="{{old('shipping.state',$shipping->state)}}">
	                        </div>
	                        @if(count($errors->get('shipping.state')))
	                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.state'))}}</span></div>
	                        @endif
	                    </div>
	                    
	                    <div class="input-row">
	                    
	                        <div class="left">
	                            <label>
	                              Zip<span> *</span>
	                            </label>
	                            <input @change="onRequiredAddressChange" class="reqFieldDHL" v-model="shipPostalCode" type="text" name="shipping[postal_code]" placeholder="" id="postal_code">
	                        </div>
	                        @if(count($errors->get('shipping.postal_code')))
	                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.postal_code'))}}</span></div>
	                        @endif
	                    </div>
	                    
	                    <div class="input-row">
	                    
	                        <div class="left">
	                            <label>
	                               Phone<span> *</span>
	                            </label>
	                            <input type="tel" name="shipping[phone]" id="phone" placeholder="" value="{{old('shipping.phone',$shipping->phone)}}">
	                        </div>
	                        @if(count($errors->get('shipping.phone')))
	                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.phone'))}}</span></div>
	                        @endif
	                    </div>


							<div class="separator"></div>
							</div>
							<div class="input-row">

								<div class="left">
									<label>
										Order Notes
									</label>
									<input type="text" style="border-color:#321e00" value="{{ old('shipping.order_notes',$shipping->order_notes) }}" name="shipping[order_notes]" placeholder="Notes about your order, e.g. special notes for delivery.">
								</div>

							</div>
							
							<div class="input-row">

								<div class="left" style="display: table-cell;">
									<label v-show="showLoaderSendData" style="display: table-cell;">
										Shipping Method
									</label>

									<label v-show="!showLoaderSendData">
										Shipping Method
									</label>

									<select v-show="!showLoaderSendData" v-model="shippingMethod" name="shipping_method" class="jne-list">
										<option value="-1">Select Shipping Method</option>
										<option :value="shipMethod.id" v-for="shipMethod in shippingMethods">${shipMethod.label}</option>
									</select>
									<div style="display: none;margin: 0px;font-size: 5px; left: 200px;display: table-cell;" v-show="showLoaderSendData" class="loader"></div>
									<span v-show="errorMsg != ''" class="text-danger">${errorMsg}</span>
								</div>
								@if(count($errors->get('shipping_method')))
									<div class="left"><span class="text-danger">{{$errors->first('shipping_method')}}</span></div>
								@endif
							</div>






					</div>
					<div class="right">
						<h4 style="margin-bottom: 24px!important;">Your Order</h4>

						<div class="separator-non-member"></div>
					
						<table class="shipping-order">
							@foreach($items as $item)
							<tr>
								<td><span>{{ $item->product_title }}</span>&nbsp;<span class="qty">x {{ $item->qty }}</span></td>
								<td><span class="single-price">{{ getCurrencySymbol() }} {{ formatMoney($item->original_total) }}</span></td>
							</tr>
							@endforeach
							<tr class="totals">
								<td><span>Subtotal</span></td>
								<td><span class="shipping-total">{{ getCurrencySymbol() }} ${subTotal.formatMoney(0)}</span></td>
							</tr>
							<tr class="totals">
								<td><span>Shipping</span></td>
								<td><span class="shipping-val">{{ getCurrencySymbol() }} ${shippingCost.formatMoney(0)}</span></td>
							</tr>
							<tr class="totals">
								<td><span style="font-weight:700">Total</span></td>
								<td><span class="qty total-harga">{{ getCurrencySymbol() }} ${grandTotal.formatMoney(0)}</span></td>
							</tr>
						</table>
					
					</div>

				</div>
				<div class="cart-buttons" style="padding-bottom:60px">
					<a href="{{ url('/') }}" class="continue-shopping">CONTINUE SHOPPING</a>
					<button @click="submitData($event)" type="submit" class="proceed-checkout">CONTINUE</button>
				</div>
						</form>
			</div>


		</div>
		<style type="text/css">
			.loadinggif
			{
			   background: url('{{ asset('bundles/img/ajax-loader.gif') }}') no-repeat right center;
			}

			.loader {
				margin: 60px auto;
				font-size: 10px;
				position: relative;
				text-indent: -9999em;
				border-top: 1.1em solid rgba(179,180,194, 0.2);
				border-right: 1.1em solid rgba(179,180,194, 0.2);
				border-bottom: 1.1em solid rgba(179,180,194, 0.2);
				border-left: 1.1em solid #b3b4c2;
				-webkit-transform: translateZ(0);
				-ms-transform: translateZ(0);
				transform: translateZ(0);
				-webkit-animation: load8 1.1s infinite linear;
				animation: load8 1.1s infinite linear;
			}
			.loader,
			.loader:after {
				border-radius: 50%;
				width: 5em;
				height: 5em;
			}
			@-webkit-keyframes load8 {
				0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
				}
				100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
				}
			}
			@keyframes  load8 {
				0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
				}
				100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
				}
			}
		</style>


		<script>
			$(document).ready(function(){

				$('input[type="checkbox"]').on('change', function(e) {
					if(e.target.checked){
						$(".shipping-info-reg").show();
					}else{
						$(".shipping-info-reg").hide();
					}
				});
		        @if(old('billToShip') == 'no')
		            $(".shipping-info-reg").show();
		        @endif
			});

		</script>
		<script type="text/javascript">
			    Number.prototype.formatMoney = function(c, d, t){
			    var n = this, 
			        c = isNaN(c = Math.abs(c)) ? 2 : c, 
			        d = d == undefined ? "." : d, 
			        t = t == undefined ? "," : t, 
			        s = n < 0 ? "-" : "", 
			        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
			        j = (j = i.length) > 3 ? j % 3 : 0;
			       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
			     };


			var phone = document.getElementById('phone');
		    phone.onkeydown = function(e) {
		        if(!((e.keyCode > 95 && e.keyCode < 106)
		          || (e.keyCode > 47 && e.keyCode < 58)
		          || e.keyCode == 8)) {
		            return false;
		        }
		    }

		    var postal = document.getElementById('postal_code');
		    postal.onkeydown = function(e) {
		        if(!((e.keyCode > 95 && e.keyCode < 106)
		          || (e.keyCode > 47 && e.keyCode < 58)
		          || e.keyCode == 8)) {
		            return false;
		        }
		    }
		</script>
		<script type="text/javascript">
			var data = {};
			data.shippingMethod = -1;
			data.shippingMethods = [];
			data.billToShip = {{ (old('billToShip') == 'no') ? 'true' : 'false' }};

			data.billPostalCode = "{{old('billing.postal_code',$billing->postal_code)}}";
			data.billCity = "{{old('billing.city',$billing->city)}}";
			data.billCountry = "{{old('billing.country',$billing->country)}}";
			if(data.billCountry == "") data.billCountry = "Indonesia";

			data.shipPostalCode = "{{old('shipping.postal_code',$shipping->postal_code)}}";
			data.shipCity = "{{old('shipping.city',$shipping->city)}}";
			data.shipCountry = "{{old('shipping.country',$shipping->country)}}";
			if(data.shipCountry == "") data.shipCountry = "Indonesia";

			data.errorMsg = "";
			data.showLoaderSendData = false;

			data.subTotal = {{ $subTotal }}
			data.shippingCost = 0;

		</script>
		<script type="text/javascript" src="{{ asset('bundles/js/vue.js') }}"></script>
		<script type="text/javascript" src="{{ asset('bundles/js/vue-resource.min.js') }}"></script>
		<script type="text/javascript">
			var urlGetShippingMethods = "{{ url('/checkout/get-quote-dhl') }}";
			Vue.http.headers.common['X-CSRF-TOKEN'] = "{{ csrf_token() }}";
			var vueShipping = new Vue({
				delimiters: ['${', '}'],
				el:"#vueBillAndShip",
				data:data,
				mounted:function(){
						 @if(count($errors) > 0)
						 	this.onRequiredAddressChange();
						 @endif
				},
				computed:{
					grandTotal()
					{
						return this.subTotal + this.shippingCost;
					}
				},
				watch:{
					'shippingMethod':function(val,oldVal)
					{
						if(val == -1)
						{
							this.shippingCost = 0;
						}else{
							this.shippingCost = 0;
							for (var i = this.shippingMethods.length - 1; i >= 0; i--) {
								if(this.shippingMethods[i].id == val)
								{
									this.shippingCost = JSON.parse(JSON.stringify(this.shippingMethods[i].ShippingCharge));;
								}
							}
						}
					}
				},
				methods:{
					submitData(e)
					{
						e.preventDefault();

						if(this.shippingMethod == -1)
						{	
							this.errorMsg = "Please select Shipping Method first.";
							return false;
						}

						$(".form-shipping").submit();
					},

					onRequiredAddressChange()
					{
						

						if(this.billToShip)
						{
							if(this.shipPostalCode.trim() == "" || this.shipCity.trim() == "" || this.shipCountry.trim() == "") return false;

							this.getShippingMethods(this.shipPostalCode,this.shipCity,this.shipCountry);

						}else{

							if(this.billPostalCode.trim() == "" || this.billCity.trim() == "" || this.billCountry.trim() == "") return false;

							this.getShippingMethods(this.billPostalCode,this.billCity,this.billCountry);
						}
					},

					getShippingMethods(postal_code,city,country)
					{
						if(this.showLoaderSendData) return false;
						if(this.showLoaderSendData == false) this.showLoaderSendData = true;
						
						$(".reqFieldDHL").prop('disabled',true);
						$('.reqFieldDHL').addClass('loadinggif');
						this.shippingMethod = -1;
						this.errorMsg = "";

						this.$http.post(
							urlGetShippingMethods,
							{postal_code:postal_code,city:city,country:country}
						).then(function(response){



							if(this.showLoaderSendData == true) this.showLoaderSendData = false;
							$(".reqFieldDHL").prop('disabled',false);
							$('.reqFieldDHL').removeClass('loadinggif');

							if(response.body.status == 1)
							{
								this.shippingMethods = response.body.shippingMethods;
							}

							if(response.body.status == 0)
							{
								this.errorMsg = response.body.msg;
							}
						},function(response){
							if(this.showLoaderSendData == true) this.showLoaderSendData = false;
							$(".reqFieldDHL").prop('disabled',false);
							$('.reqFieldDHL').removeClass('loadinggif');
							
							this.errorMsg = "Shipping Method is not available";
						});
					}
				}
			});

		</script>
