		<div class="main" id="vueShipping">
			<div class="main-container checkout">
				<h1>CHECKOUT</h1>
				<div style="display: none" v-show="showLoaderSendData" class="loader"></div>
				<div v-show="!showLoaderSendData" class="fancy-checkout-header">
					<ul>
						<li>
							<span>LOGIN / REGISTER</span>
							<div class="round-indicator"></div>
						</li>
						<li class="active">
							<span>SHIPPING</span>
							<div class="round-indicator active"></div>
						</li>
						<li>
							<span>PAYMENT</span>
							<div class="round-indicator"></div>
						</li>
						<li>
							<span>CONFIRM</span>
							<div class="round-indicator"></div>
						</li>
					</ul>
					<div class="separator"></div>
				</div>
				
				<form v-show="!showLoaderSendData" id="formship" action="{{url('/checkout-shipping-save')}}" method="GET">
					<div class="checkout-shipping">
						<div class="left">
							<h4>SHIPPING ADDRESS</h4>

			                <div style="display: none" class="left"><span class="text-danger">Please select shipping address!</span><br></div>

							<div style="margin-bottom:18px;">
								<select v-model="selectedAddress" name="label" class="label-address">
									<option value="-1">Choose Shipping Address</option>
									<option :value="address.id" v-for="address in shippingAddress">${address.label}</option>
								</select>

							    <a href="{{url('/checkout/add-address')}}" style="font-weight:600">ADD</a>
							    <div v-show="parseInt(selectedAddress) != -1" class="eship">
									&nbsp;&nbsp;|&nbsp;&nbsp;<a :href="'{{ url('/checkout/edit-address/') }}' + '/' + selectedAddress" style="font-weight:600">EDIT</a>
								</div>

							</div>
							<div class="address-box">
								<p v-show="!showLoader" class="bold name">${selectedShippingAddress.label}</p>
								<p style="margin-bottom: 0px!important" v-show="!showLoader">${selectedShippingAddress.first_name} ${selectedShippingAddress.last_name}</p>
								<p v-show="!showLoader">${selectedShippingAddress.address}<br>${selectedShippingAddress.district}<br>${selectedShippingAddress.city} ${selectedShippingAddress.postal_code}</p>
								<div v-show="showLoader" class="loader"></div>
							</div>
						</div>
						<div class="right">
						<h4 style="margin-bottom:22px!important">SHIPPING METHOD</h4>
						<span v-show="errorMsg != ''" class="text-danger">${errorMsg}<br><br></span>
						<select v-model="shippingMethod" class="jne-list">
							<option value="-1">Select Shipping Method</option>
							<option :value="shippingMethod.id" v-for="shippingMethod in shippingMethods">${shippingMethod.label}</option>
						</select>
						
						</div>
			
					</div>
			
					<div class="cart-buttons">
							<a href="{{url('/products')}}" class="continue-shopping">CONTINUE SHOPPING</a>
							<a @click="saveData($event)" class="proceed-checkout" href="javascript:void(0)">CONTINUE</a>
					</div>
				</form>
			</div>
		</div>
		<style type="text/css">
			.loadinggif
			{
			   background: url('{{ asset('bundles/img/ajax-loader.gif') }}') no-repeat right center;
			}

			.loader {
				margin: 60px auto;
				font-size: 10px;
				position: relative;
				text-indent: -9999em;
				border-top: 1.1em solid rgba(179,180,194, 0.2);
				border-right: 1.1em solid rgba(179,180,194, 0.2);
				border-bottom: 1.1em solid rgba(179,180,194, 0.2);
				border-left: 1.1em solid #b3b4c2;
				-webkit-transform: translateZ(0);
				-ms-transform: translateZ(0);
				transform: translateZ(0);
				-webkit-animation: load8 1.1s infinite linear;
				animation: load8 1.1s infinite linear;
			}
			.loader,
			.loader:after {
				border-radius: 50%;
				width: 5em;
				height: 5em;
			}
			@-webkit-keyframes load8 {
				0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
				}
				100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
				}
			}
			@keyframes  load8 {
				0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
				}
				100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
				}
			}
		</style>
		<script type="text/javascript">
			var urlGetShippingMethod = "{{ url('/checkout/get-shipping-method') }}";
			var urlSaveShippingInfo = "{{ url('/checkout/save-shipping-info') }}";
			var urlPayment = "{{ url('/checkout/payment') }}";

			var data = {};
			data.shippingMethod = -1;
			data.shippingMethods = [];
			data.shippingAddress = {!! json_encode($shippingAddress) !!};
			data.selectedAddress = -1;
			data.showLoader = false;
			data.showLoaderSendData = false;
			data.selectedShippingAddress = {
				label:"",
				address:"",
				district:"",
				city:"",
				postal_code:""
			};
			data.isReactiveEvent = true;
			data.errorMsg = "";
		</script>
		<script type="text/javascript" src="{{ asset('bundles/js/vue.js') }}"></script>
		<script type="text/javascript" src="{{ asset('bundles/js/vue-resource.min.js') }}"></script>
		<script type="text/javascript">
			Vue.http.headers.common['X-CSRF-TOKEN'] = "{{ csrf_token() }}";
			var vueShipping = new Vue({
				delimiters: ['${', '}'],
				el:"#vueShipping",
				data:data,
				watch:{
					'selectedAddress':function(val,oldVal)
					{
						if(this.isReactiveEvent == false) return false;
						this.destroyShippingMethod();
						this.getShippingMethod(val);

					}
				},
				methods:{
					
					destroyShippingMethod()
					{
						this.shippingMethod = -1;
						this.shippingMethods = [];
						this.errorMsg = "";
					},

					getShippingMethod(addressId)
					{
						if(addressId == -1){
							this.selectedShippingAddress = {};
							return false;
						}
						this.showLoader = true;
						this.$http.post(urlGetShippingMethod,{id:addressId}).then(function(response){
							this.showLoader = false;
							for (var i = this.shippingAddress.length - 1; i >= 0; i--) {
								if(parseInt(this.shippingAddress[i].id) == parseInt(addressId))
								{
									this.selectedShippingAddress = JSON.parse(JSON.stringify(this.shippingAddress[i]));
								}
							}
							if(response.body.status == 1)
							{
								this.shippingMethods = response.body.shippingMethods;
							}else{
								this.errorMsg = response.body.msg;
							}
						});
					},

					validateData()
					{
						if(parseInt(this.selectedAddress) == -1){
							return false;
						}

						if(parseInt(this.shippingMethod) == -1){
							return false;
						}

						return true;

					},

					saveData(e)
					{
						e.preventDefault();

						if(!this.validateData()) return false;
						this.showLoaderSendData = true;	
						this.$http.post(
							urlSaveShippingInfo,

							{
								shippingMethod:this.shippingMethod,
								addressId:this.selectedAddress
							}

							).then(function(response){
								
								

								if(response.body.status == 0)
								{
									this.showLoaderSendData = false;
									alert(response.body.msg);
								}

								if(response.body.status == 1)
								{
									window.location.href = urlPayment;
								}
							});
					}

				}
			});
		</script>