

		<div class="main">

		

			<div class="main-container contact">

				<h1>CONTACT US</h1>

				<div class="contact-content">

				

				

					<div class="left">

						

						<h4 class="sendmail">SEND US AN EMAIL</h4>
						
							@if(Session::get('success'))
							<div class="block change-pswd">
								<p class="head-block" style="margin-bottom:10px;"><span style="    font-weight: 600;
	color: #bfd22b;">Success!</span> Email Sent! Thank you</p>
							</div>
							<script>
								$('.sendmail').hide();
							</script>
							@endif
						

						<div class="separator"></div>

						

						<form id="contact-form" method="POST" action="<?php echo url('contactSave'); ?>">

							{{ csrf_field() }}
							@if(Session::get('failed'))
							<div class="left"><span class="text-danger">Failed! Please fill required field.</span></div>
							@endif

							<div class="input-row">

								<label>Name</label>

								<input class="name" type="text" name="firstname" placeholder="First Name"> <input class="name" type="text" name="lastname" placeholder="Last Name">
								@if(count($errors->get('firstname')))
									<div class="left"><span class="text-danger">{{$errors->first('firstname')}}</span></div>
								@endif
								@if(count($errors->get('lastname')))
									<div class="right" style="margin-top:-19px;text-align: right;"><span class="text-danger">{{$errors->first('lastname')}}</span></div>
								@endif
							</div>

							

							<div class="input-row">

								<label>Email</label>

								<input type="email" placeholder="Your Email" name="email">
								@if(count($errors->get('email')))
									<div class="left"><span class="text-danger">{{$errors->first('email')}}</span></div>
								@endif
							</div>

							

							<div class="input-row">

								<label>Subject</label>

								<input type="text" placeholder="Your Subject" name="subject">
								@if(count($errors->get('subject')))
									<div class="left"><span class="text-danger">{{$errors->first('subject')}}</span></div>
								@endif
							</div>

							

							<div class="input-row">

								<label>Message</label>

								<textarea name="message" placeholder="Your Message"></textarea>
								@if(count($errors->get('message')))
									<div class="left"><span class="text-danger">{{$errors->first('message')}}</span></div>
								@endif
							</div>

							

							<button type="submit">SEND</button>

							

						</form>

						

					</div>



					<div class="right">

						<h4>ADDRESS</h4>

						<p>{!! app('AlphaSetting')->getSetting('street_address') !!}</p>

						<div class="separator"></div>

						<h4>Phone</h4>

						<span>{{ app('AlphaSetting')->getSetting('phone') }}</span>

						<h4>Fax</h4>

						<span>{{ app('AlphaSetting')->getSetting('fax') }}</span>

						<h4>E-mail</h4>

						<span>{{ app('AlphaSetting')->getSetting('general_contact') }}</span>

						<h4>Skype ID</h4>

						<span>{{ app('AlphaSetting')->getSetting('skype_id_contact') }}</span>

					</div>

				

				</div>

				

				

				

			</div>

			

		</div>
