 
        <div class="main">
        
            <div class="main-container register">
            
                <h1>BILLING ADDRESS</h1>
                
                <form id="form-register" action="{{url('/save-billing-address')}}" method="POST">
                {{ csrf_field() }}
                    <h4>Please fill your additional billing detail</h4>
                    <div class="separator"></div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Address Name<span> *</span>
                            </label>
                            <input type="text" name="label" value="{{ old('label',$data->label) }}">
                        </div>
                        @if(count($errors->get('label')))
                            <div class="left"><span class="text-danger">{{$errors->first('label')}}</span></div>
                        @endif
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Receiver Firstname<span> *</span>
                            </label>
                            <input type="text" name="first_name" value="{{ old('first_name',$data->first_name) }}">
                        </div>
                        @if(count($errors->get('first_name')))
                            <div class="left"><span class="text-danger">{{$errors->first('first_name')}}</span></div>
                        @endif
                    </div>


                     <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Receiver Lastname<span> *</span>
                            </label>
                            <input type="text" name="last_name" value="{{ old('last_name',$data->last_name) }}">
                        </div>
                        @if(count($errors->get('last_name')))
                            <div class="left"><span class="text-danger">{{$errors->first('last_name')}}</span></div>
                        @endif
                    </div>
                    
                    
                    
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Company Name
                            </label>
                            <input type="text" name="company" value="{{ old('company',$data->company) }}">
                        </div>
                        @if(count($errors->get('company')))
                            <div class="left"><span class="text-danger">{{$errors->first('company')}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Address<span> *</span>
                            </label>
                            <input type="text" name="address" placeholder="Street Address" value="{{ old('address',$data->address) }}">
                        </div>
                        @if(count($errors->get('address')))
                            <div class="left"><span class="text-danger">{{$errors->first('address')}}</span></div>
                        @endif
                    </div>
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               &nbsp;
                            </label>
                            <input type="text" name="address2" value="{{ old('address2',$data->address2) }}" placeholder="Apartment, suite, unit etc. (optional)">
                        </div>
                        <div class="left"></div>
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Country<span> *</span>
                            </label>
                            <select name="country">
                                @foreach($country as $value)
                                    <option value="{{$value->name}}" @if(old('country',$data->country) == $value->name) <?= 'selected';?> @endif>{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if(count($errors->get('country')))
                            <div class="left"><span class="text-danger">{{$errors->first('country')}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Town / City<span> *</span>
                            </label>
                            <input type="text" name="city" placeholder="" id="city" value="{{ old('city',$data->city) }}">
                        </div>
                        @if(count($errors->get('city')))
                            <div class="left"><span class="text-danger">{{$errors->first('city')}}</span></div>
                        @endif
                    </div>

                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               District<span> *</span>
                            </label>
                            <input type="text" name="district" placeholder="District" id="district" value="{{ old('district',$data->district) }}">
                        </div>
                        @if(count($errors->get('district')))
                            <div class="left"><span class="text-danger">{{$errors->first('district')}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               State<span> *</span>
                            </label>
                            <input type="text" name="state" placeholder="" value="{{ old('state',$data->state) }}">
                        </div>
                        @if(count($errors->get('state')))
                            <div class="left"><span class="text-danger">{{$errors->first('state')}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                              Zip<span> *</span>
                            </label>
                            <input type="text" name="postal_code" placeholder="" value="{{ old('postal_code',$data->postal_code) }}">
                        </div>
                        @if(count($errors->get('postal_code')))
                            <div class="left"><span class="text-danger">{{$errors->first('postal_code')}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Phone<span> *</span>
                            </label>
                            <input type="tel" name="phone" placeholder="" value="{{ old('phone',$data->phone) }}">
                        </div>
                        @if(count($errors->get('phone')))
                            <div class="left"><span class="text-danger">{{$errors->first('phone')}}</span></div>
                        @endif
                    </div>
                    <input type="hidden" name="id" value="{{ $data->id }}">
                        <button type="submit">CONTINUE</button>
                        
                 
                </form>
                
            </div>
            
        
        </div>
