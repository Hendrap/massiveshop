

        <div class="main">
            
            <div class="main-container account forget">
            
                <h1>FORGOT PASSWORD</h1>
                
                <p>We will send your password to your email. Please fill your email address below:</p>
                <form id="forget-pswd" action="{{url('/account/change_password/send')}}" method="post">
                    {!! csrf_field() !!}
			@if(Session::get('success') == 'success')
                                <p class="head-block"><span>Success!</span> Your password have been changed.</p>
                                {{Session::forget('success')}}
                                @endif
                                @if(Session::get('send') == 'send')
                                <div class="alert alert-success">
						<strong>Success!</strong> Check your email to change password
					</div>
                                {{Session::forget('send')}}
                                @endif
				@if(Session::get('send') == 'notfound')
                                <div class="alert alert-danger">
						Email Address not found
					</div>
                                {{Session::forget('send')}}
                                @endif
                    <span>Email Address</span>
                    <input type="email" name="email">
                    <button type="submit">SUBMIT</button>
                </form>
            </div>

        </div>
