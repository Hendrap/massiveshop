
<style>.swiper-pagination.home-product .swiper-pagination-bullet {
	margin-right: 10px;
	background-color: #FFE;
	border: #fff;
	opacity: 1;
}</style>
<div class="main home">
			<div class="banner banner-home">
			
				<div class="banner-home-swiper">
					
					<div class="swiper-wrapper">
						@foreach($slider as $slide)
							@foreach($slide->medias as $imgslider)
								<div class="swiper-slide"><img src="{{asset(getCropImage($imgslider->path,'sliderv2',true))}}" alt="image name here"></div>
							@endforeach
						@endforeach
					</div>
					
					<a href="#" class="scrolldown"><img src="{{ asset('bundles/img/arrow-down.png') }}" alt="V"></a>

				</div>
				
			</div>
			<div class="main-container" id="main-container-home">
			
				<section class="introduction">
					@if(!empty($site_intro))
					<div class="left"><img src="{{asset($site_intro->medias[0]->path)}}" alt="premium ingredients"></div>
					<div class="left">
						<div class="site_intro-text">
							<h3>{!! strip_tags(parseMultiLang($site_intro->content))!!}</h3>
							<hr>
							<p>{!! strip_tags(parseMultiLang($site_intro->excerpt)) !!}	</p>
						</div>
					</div>
					@endif
				</section>
				
				@if(isset($site_intro->alphaMediaImage->path))
				<section class="home-product-banner" style="margin-bottom: 30px;">
					<img style="width: 100%" src="{{ asset(getCropImage($site_intro->alphaMediaImage->path,'banner',true)) }}" alt="Banner image">
				</section>
				@endif
				<section style="position: relative;margin-top: 50px;margin-bottom: 70px;" class="home-testi-banner">
				
					<div style="overflow: hidden;" class="home-testi-swiper">
					<h3 style="margin-bottom:15px!important;text-align:center;font-weight: 400;font-size: 26px;line-height: 31.2px;width: 50%;margin: auto;">TESTIMONIALS</h3>
					<hr style="background-color: #bfd22b;border-color: #bfd22b;width: 50%;margin: auto;margin-bottom: 15px">
					<div class="swiper-wrapper">
						@foreach($testimonials as $testimonial)
							<div style="text-align: center" class="swiper-slide">
								<p style="margin-bottom: 10px">{{ parseMultiLang(strip_tags($testimonial->content)) }}</p>
								<b style="font-weight: 500;">- {{ parseMultiLang($testimonial->title) }} -</b>
							</div>						
						@endforeach
					</div>
					<div style="text-align: center;margin-top: 40px">
						<a style="color:rgb(165, 169, 170);" href="{{ url('testimonial') }}">(submit your testimonial here)</a>
					</div>
					</div>
				</section>



				<section class="home-product-banner">
				
					<div class="home-product-swiper">
					
					<div class="swiper-wrapper">
						@foreach($offer as $imgoffer)
						
							<div class="swiper-slide"><a href="{{ getEntryMetaFromArray($imgoffer->metas,'link') }}"><img src="{{asset(getCropImage($imgoffer->medias[0]->path,'category'))}}" alt="{{$imgoffer->medias[0]->title}}"></a></div>
						
						@endforeach
					</div>
					
					</div>
					<div class="swiper-pagination home-product banner"></div>
				</section>
				
				<section class="new-product">
				
					<div class="new-product-header">
					
						<h3>NEW</h3>
					
					</div>
					
					<div class="home-product-new-swiper">
						<div class="swiper-wrapper">
							@foreach($data as $k => $n)
								<div class="swiper-slide">
									{!! view('app::clean.templates.product',['product'=>$n]) !!}
								</div>
							@endforeach
						</div>
					</div>
					<div class="swiper-pagination home-product new"></div>
				</section>
				
			</div>
		</div>
