<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=560">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Neneq - Order Confirmation</title>

	
	<!-- CSS Reset -->
	<style type="text/css">

		
		
		/* What it does: Stops email clients resizing small text. */
		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}
		
		/* What it does: Centers email on Android 4.4 */
		div[style*="margin: 16px 0"] {
			margin:0 !important;
		}
		
		/* What it does: Stops Outlook from adding extra spacing to tables. */
		table,
		td {
			mso-table-lspace: 0pt !important;
			mso-table-rspace: 0pt !important;
		}
				
		/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			Margin: 0 auto !important;
		}
		table table table {
			table-layout: auto;
		}
		
		/* What it does: Uses a better rendering method when resizing images in IE. */
		img {
			-ms-interpolation-mode:bicubic;
		}
		
		/* What it does: Overrides styles added when Yahoo's auto-senses a link. */
		.yshortcuts a {
			border-bottom: none !important;
		}
		
		/* What it does: A work-around for iOS meddling in triggered links. */
		.mobile-link--footer a,
		a[x-apple-data-detectors] {
			color:inherit !important;
			text-decoration: underline !important;
		}
		
	</style>
	
	

</head>
<body>
<table cellpadding="0" cellspacing="0" border="0" bgcolor="#fff" style="border-collapse:collapse;"><tr><td valign="top">
	<center>

		

		
	
		
			<table cellspacing="0" cellpadding="0" border="0" width="560" align="center">
			<tr>
			<td>
		

			
			
			<!-- Email Body : BEGIN -->
			<table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="560" style="max-width: 560px;">
				
				<!-- Hero Image, Flush : BEGIN -->
				<tr>
					<td style="padding-top:20px;">
						<img src="{{asset('bundles/email/header.jpg')}}" width="560" height="" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 560px;">
					</td>
				</tr>
				<!-- Hero Image, Flush : END -->

				
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="560">
							<tr>
								<td style="padding: 40px;padding-bottom:10px;  text-align: left; font-family: sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #000;">
									Hi, [[name]]
									<br>
									<br>
									Thank you for shopping with us! We are now checking out your shopping cart.
									<br><br>
The payment should be confirmed within 2 x 24 hours after you receive this e-mail, otherwise your order will be considered as canceled order. Your order will not be processed until we receive your payment
									<br>
									<br>
									<span style="font-weight:700">[[date]]</span>
									<br>
									<br>
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="table-layout:fixed;">
									
										<tr>
											<td colspan="3" style="background-color:#bdd030;padding-left:10px;" valign="middle" height="30">
												
												<b>Order #[[order_number]]</b>
												
											</td>
										</tr>
										[[data]]
									
									</table>
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="table-layout:fixed;" bgcolor="e6e7e8">
										
										<tr>
											<td align="right" width="300" height="30" valign="middle">
											</td>
											<td align="left" valign="middle">
												Subtotal
											</td>
											<td align="right" style="padding-right:10px;" height="30" valign="middle">
												[[total]]
											</td>
										</tr>
										<tr>
											<td align="right"  width="300" valign="middle">
											</td>
											<td align="left" height="20" valign="middle">
												Shipping
											</td>
											<td align="right" style="padding-right:10px;" height="20" valign="middle">
												[[shipping]]
											</td>
										</tr>
										<tr>
											<td align="right"  width="300" valign="middle">
											</td>
											<td align="left" height="30" valign="middle">
												<b>Total</b>
											</td>
											<td align="right" style="padding-right:10px;" height="30" valign="middle">
												<b>[[grand_total]]</b>
											</td>
										</tr>
										
									</table>
									<br>
									<br>
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="table-layout:fixed;" bgcolor="">
										<tr>
											[[address]]
										</tr>
									</table>
									<br>
									<br>
									
									<hr>
									
								
									<br>
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="table-layout:fixed;" bgcolor="">
										
										<tr>
											<td>
												<b>Please make your payment to:</b><br><br>
												
												BNI<br>
												<b>Branch:</b><br>
												Bali<br>
												<b>Account Number:</b><br>
												999 999 999<br>
												<b>Account Name</b><br>
												PT. Cantik Sejahtera
											</td>
											<td align="right" valign="bottom">
											
												<a href="[[comfirm_url]]"><img src="{{asset('bundles/email/confirm-button.jpg')}}" alt="confirm payment"></a>
												
												
											</td>
										</tr>
										
									</table>
									
									
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				

				

			</table>
			<!-- Email Body : END -->
			
			<!-- Email Footer : BEGIN -->
			<table cellspacing="0" cellpadding="0" border="0" align="left" width="560" style="max-width: 560px;">
				<tr>
					<td style="padding: 10px 40px 10px 40px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: left; color: #000;">
						<hr>
						<img src="{{asset('bundles/email/logo.jpg')}}" alt="neneq" style="padding-top:18px;padding-bottom:14px;	"><br>
						<p style="margin:0">
							This is an automatic email generated by our system. Please do NOT reply to this email.<br>For further assistance, kindly email to : [[email]]
							<br>
							<br>
							<a href="[[unsubscribe]]" style="text-decoration:underline;color:#000;">Unsubscribe</a>
						</p>
					</td>
				</tr>
			</table>
			
			</td>
			</tr>
			</table>
			
		
	</center>
</td></tr></table>
</body>
</html>

