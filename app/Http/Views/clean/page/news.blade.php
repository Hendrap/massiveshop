		
		<div class="main">
		
			<div class="main-container news">
			
				<h1>NEWS &amp; EVENT</h1>
				
				<ul class="recent-news mob-hide">
				@foreach($latest as $k)
					<li><a href="{{url('news')}}/{{$k->slug}}">{{parseMultiLang($k->title)}}</a></li>
				@endforeach
				</ul>
				
				<div class="news-main">
				
					<div class="left">
						
						<div class="news-archive">
							
							@foreach($archiveNews as $key => $value)
							<div class="archive-content">
							
								<h6>{{ date('F Y',strtotime($key)) }}</h6>
								<ul>
									@foreach ($value as $v) 
									<li class="link" data-id="{{$v->slug}}">
										<a href="{{url('news')}}/{{$v->slug}}">{{parseMultiLang($v->title)}}</a>
									</li>
									@endforeach
								</ul>
							</div>
							@endforeach
							
						</div>
						
					</div>
				
					<div class="right">
						@if(!empty($currentNews))
						<div class="news-content">
							<h2>
								{{parseMultiLang($currentNews->title)}}
							</h2>
							<div class="separator"></div>
							<span class="date">
								{{ date('d F Y',strtotime($currentNews->published_at)) }}
							</span>
							
							<article>
								@foreach($currentNews->medias as $m1)
									<img src="{{asset(getCropImage($m1->path, 'news'))}}" alt="image name goes here">
								@endforeach
								<p class="news-text">
									{!!parseMultiLang($currentNews->content)!!}
								</p>
							
							</article>
							
						</div>
						@endif

					</div>
				</div>
				
				<ul class="recent-news mob-show">
				@foreach($latest as $k)
					<li><a href="{{url('news')}}/{{$k->slug}}">{{parseMultiLang($k->title)}}</a></li>
				@endforeach
				</ul>
				
			</div>
			
		</div>