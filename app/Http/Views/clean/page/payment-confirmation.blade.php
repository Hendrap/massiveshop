
		<div class="main">


			<div class="main-container checkout">
				<h1>CHECKOUT</h1>


				<div class="fancy-checkout-header">

					<ul>

						<li>
							<span>LOGIN / REGISTER</span>
							<div class="round-indicator"></div>
						</li>
						<li>
							<span>SHIPPING</span>
							<div class="round-indicator"></div>
						</li>
						<li>
							<span>PAYMENT</span>
							<div class="round-indicator"></div>
						</li>
						<li class="active">
							<span>CONFIRM</span>
							<div class="round-indicator  active"></div>
						</li>

					</ul>
					<div class="separator"></div>
				</div>


				<div class="payment-confirmation">

					@if(empty($errors))
					<p class="head-block" style="font-weight:400"><span>Success!</span> Your payment has been confirmed</p>
					@endif
		            @if(!empty(Session::get('confirm')))
					<p class="head-block" style="font-weight:400"><span>Success!</span> Your payment has been confirmed.</p>
					{{Session::forget('confirm')}}
					@endif
					<form id="form-confirm-payment" method="POST" action="{{url('/checkout-confirm')}}">
					{{ csrf_field() }}
					<h4>Please fill all the required fields</h4>
					<div class="separator"></div>
					
					<div class="input-row order-number">
					
						<div class="left">
							<label>
								Order Number<span> *</span>
							</label>
							<select name="order_number">
							@foreach($data as $on)
								<option value="{{$on->order_number}}">{{$on->order_number}}</option>
							@endforeach
							</select>
						</div>
						<!-- <div class="left"><span class="text-danger">Please fill in this field</span></div> -->
						
					</div>
					
					<div class="input-row">
					
						<div class="left">
							<label>
								Bank Name<span> *</span>
							</label>
							<input type="text" name="bank-name">
						</div>
						@if(count($errors->get('bank-name')))
							<div class="left"><span class="text-danger">{{$errors->first('bank-name')}}</span></div>
						@endif
					</div>
					
					<div class="input-row">
					
						<div class="left">
							<label>
								Account Name<span> *</span>
							</label>
							<input type="text" name="bank-account-name">
						</div>
						@if(count($errors->get('bank-account-name')))
							<div class="left"><span class="text-danger">{{$errors->first('bank-account-name')}}</span></div>
						@endif
					</div>
					
					<div class="input-row">
					
						<div class="left">
							<label>
								Account Number<span> *</span>
							</label>
							<input type="text" name="bank-number" id="bank-number">
						</div>
						@if(count($errors->get('bank-number')))
							<div class="left"><span class="text-danger">{{$errors->first('bank-number')}}</span></div>
						@endif
					</div>
						
					<div class="input-row">
					
						<div class="left">
							<label>
								Amount<span> *</span>
							</label>
							<input type="text" name="amount" id="amount">
						</div>
						@if(count($errors->get('amount')))
							<div class="left"><span class="text-danger">{{$errors->first('amount')}}</span></div>
						@endif
					</div>
						
					<div class="input-row small-select">
					
						<div class="left">
							<label>
								Tranfer Date<span> *</span>
							</label>
							<select name="date-trans" class="small">
								
							</select>
							
							<select class="small" name="month-trans">
								<option value="1">Jan</option>
								<option value="2">Feb</option>
								<option value="3">Mar</option>
								<option value="4">Apr</option>
								<option value="5">May</option>
								<option value="6">Jun</option>
								<option value="7">Jul</option>
								<option value="8">Aug</option>
								<option value="9">Sep</option>
								<option value="10">oct</option>
								<option value="11">Nov</option>
								<option value="12">Dec</option>
							</select>
							
							<select class="small" name="year-trans">
								
							</select>
						</div>
						
					</div>
						
					<div class="input-row">
					
						<div class="left">
							<label>
								Ref Number<span> *</span>
							</label>
							<input type="text" name="ref-number">
						</div>
						@if(count($errors->get('ref-number')))
							<div class="left"><span class="text-danger">{{$errors->first('ref-number')}}</span></div>
						@endif
					</div>
					
					<button type="submit">CONFIRM</button>
						
					</form>
				
				</div>
				
			</div>


		</div>

<script type="text/javascript">
	var accountNumber = document.getElementById('bank-number');
	accountNumber.onkeydown = function(e) {
		if(!((e.keyCode > 95 && e.keyCode < 106)
			|| (e.keyCode > 47 && e.keyCode < 58)
			|| e.keyCode == 8)) {
			return false;
		}
	}

	var amount = document.getElementById('amount');
	amount.onkeydown = function(e) {
		if(!((e.keyCode > 95 && e.keyCode < 106)
			|| (e.keyCode > 47 && e.keyCode < 58)
			|| e.keyCode == 8 || e.keyCode == 188)) {
			return false;
		}
	}

	var ysel = document.getElementsByName("year-trans")[0],
		msel = document.getElementsByName("month-trans")[0],
		dsel = document.getElementsByName("date-trans")[0];
	for (var i = <?= date('Y');?>; i >= 2000; i--) {
		var opt = new Option();
		opt.value = opt.text = i;
		ysel.add(opt);
	}
	ysel.addEventListener("change", validate_date);
	msel.addEventListener("change", validate_date);

	function validate_date() {
		var y = +ysel.value, m = msel.value, d = dsel.value;
		if (m === "2")
			var mlength = 28 + (!(y & 3) && ((y % 100) !== 0 || !(y & 15)));
		else var mlength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1];
		dsel.length = 0;
		for (var i = 1; i <= mlength; i++) {
			var opt = new Option();
			opt.value = opt.text = i;
			if (i == d) opt.selected = true;
			dsel.add(opt);
		}
	}
	validate_date();

</script>
<script>
			$(document).ready(function() {
				$('select').niceSelect();
			});
			</script>
