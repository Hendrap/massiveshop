	<style>
		.ingredients p{
			font-size: 12px!important;
		}
		.small-price p{
			font-size: 10px;
		}
		.convertcurrency{
			vertical-align: text-bottom;
		}
		.contact-us{
			margin-top: 20px;
			background-color: #bfd22b;
			margin-bottom: 0;
		    font-weight: 400;
		    text-align: center;
		    color: #ffffff;
		    border: 1px solid transparent;
		    white-space: nowrap;
		    padding: 6px 12px;
		    font-size: 14px;
		    line-height: 1.42857143;
		    border-radius: 4px;
		}
	</style>
		
		<div class="main">
			
			<div class="main-container product-detail">
			
				<!--add cart message-->
				
					
					<div class="add-cart-message">
						
						<div class="top">
							<div class="message">
								
							</div>
							
							<div class="product-added">
								<table>
									<tr>
										<td width="95%">
											<span id="itmName"></span>
											<span id="itmQTY"></span>
										</td>
										<td align="center">
											<a href="#">X</a>
										</td>
									</tr>
								
								</table>
							</div>
						</div>
						<div class="bottom">
						
							<div class="subtotal">
								<span id="sub"></span>
								<span>*EXCLUDE SHIPPING</span>
								<a href="{{url('/checkout')}}">CHECK OUT</a>
							</div>
							
						</div>
						
					</div>
				
				
				<!--/add cart message-->
				
				<div class="breadcrumb">
					@if(!empty($categoryProduct))
						<a href="{{url('/products')}}/{{$categoryProduct->taxonomy_slug}}">{{$categoryProduct->name}}</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<span>{{parseMultiLang($data->title)}}</span>
					@endif
				</div>
				<div class="product">

					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="product-image">
								<img itemprop="image" src="{{asset(getCropImage($data->medias[0]->path, 'productdetail',true))}}" alt="{{$data->medias[0]->title}}">
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="product-detail">
								<div class="top">
									<h1 itemprop="name">{{parseMultiLang($data->title)}}</h1>

									
										@if($data->isContactOnly == false && $data->isComingSoon == false)
										<div class="price">
											@if(isSale($selectedItem))
												<span itemprop="price" id="showharga"> {{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($selectedItem->sale_price)) }}</span>
											@else
												<span itemprop="price" id="showharga"> {{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($selectedItem->price)) }}</span>
											@endif
											&nbsp;&nbsp;&nbsp;&nbsp;
					    				</div>
					    				@endif

					    			@if($data->isContactOnly)
					    				<a href="{{url('/contact-us')}}" class="btn contact-us">Contact Us</a>
					    			@endif
					    		
								</div>
								<div class="middle">

										@if($selectedItem->stock_available > 0 && $data->isContactOnly == false && $data->isComingSoon == false)
											<span>Quantity</span>
											<select id="qty-select">
												@for($i=1;$i<=$selectedItem->stock_available;$i++)
													<option class="qty" value="{{$i}}">{{$i}}</option>
												@endfor
											</select>
										@endif


										
										@if($selectedItem->stock_available <= 0)
											<button class="btn add-to-cart no-stock" disabled>SOLD OUT</button>
										@endif

										@if($data->isComingSoon)
											<button class="btn add-to-cart no-stock" disabled>COMING SOON</button>
										@endif

										@if($selectedItem->stock_available > 0 && $data->isContactOnly == false && $data->isComingSoon == false)
											<button class="btn add-to-cart" onclick="addtocart();">BUY</button>
										@endif
		
								</div>
								<div class="bottom" itemprop="description">
									
								<div class="description howtouse-desc">
									{!! parseMultiLang($data->content) !!}								
								</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="separator-big"></div>
				</div>
				
				@if(count($otherProducts) > 0)
				<div class="rec-products">
					<h3>OTHER PRODUCTS</h3>
					<div class="rec-product-swiper">
						<div class="swiper-wrapper">
						@foreach($otherProducts as $product)
						<div class="swiper-slide">
							{!! view('app::clean.templates.product',['product'=>$product]) !!}
						</div>
						@endforeach
						</div>
					</div>
					
					<div style="display: none" class="swiper-pagination home-product new"></div>
				</div>
				@endif


			</div>
		
		</div>

<script type="text/javascript">
	$(document).ready(function() {
	  $("#qty-select").niceSelect();
	});
</script>
@if($selectedItem->stock_available > 0 && $data->isContactOnly == false && $data->isComingSoon == false)
<script type="text/javascript">

	function addtocart() {

		var item_id = {{ $selectedItem->id }};
		var userID = 0;
		var qty = $("#qty-select").val();
		var price = $(".prices").text();
		
		var invinite = false;

		@if(Auth::user())
			userID = "{{ Auth::user()->id }}";
		@endif

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: 'POST',
			dataType: 'json',
			data: {item_id : item_id, qty : qty, userID : userID},
			url: '<?php echo url('/add-to-cart'); ?>',
			success: function(data,status) {
			
				if(data.status == 1)
				{
					$(".add-cart-message").html(data.html);

					$('.add-cart-message').addClass('active');

					setTimeout(function(){
						window.location.reload();
					},3000);

				}else{
					alert(data.msg);
				}

			}
		})
	}
</script>
@endif
