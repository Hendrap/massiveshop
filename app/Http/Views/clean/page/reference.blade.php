 
        <div class="main">
            
            <div class="main-container reference">
                
                <h1>REFERENCES</h1>
                @foreach($data as $ref)
                <div class="reference-box">
                    @if(!empty($ref->medias[0]))
                    <img src="{{ asset(getCropImage($ref->medias[0]->path,'sliderv2',true)) }}" alt="image name here">
                    @endif
                    <h3>{{parseMultiLang($ref->title)}}</h3>
                    <div class="separator"></div>
                    {!!parseMultiLang($ref->content)!!}
                    <?php 
                        $link  = getEntryMetaFromArray($ref->metas,'link');
                        if(!Illuminate\Support\Str::contains($link,'http')){
                            $link = "//".$link;
                        }
                     ?>
                    <a href="{{ $link }}" target="_blank"><button class="btn">GO TO WEBSITE</button></a>
                    
                </div>
                @endforeach
                
            </div>
        
        </div>
