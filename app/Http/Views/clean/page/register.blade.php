 
        <div class="main">
        
            <div class="main-container register">

                <h1>REGISTER ACCOUNT</h1>

                <form id="form-register" method="POST" action="{{url('registerSave')}}">
                {{ csrf_field() }}
		          <h4>Account Detail</h4>
                  <br>
		        @if(count(Session::get('msg')))
                <div class="left"><span class="text-danger">Email already taken</span></div>
                @elseif(count(Session::get('msg-success')))
                <div class="left"><span class="text-success">Register success, please check your email to activate account!.</span></div>
                @endif
                    
                    <div class="separator"></div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                First Name<span> *</span>
                            </label>
                            <input type="text"  name="billing[first_name]" value="{{old('billing.first_name')}}" class="firstname">
                        </div>
                        @if(count($errors->get('billing.first_name')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.first_name'))}}</span></div>
                        @endif
                       <!-- <div class="left"><span class="text-danger">Please fill in this field</span></div>-->
                        
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Last Name<span> *</span>
                            </label>
                            <input type="text"  name="billing[last_name]" value="{{old('billing.last_name')}}" class="lastname">
                        </div>
                        @if(count($errors->get('billing.last_name')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.last_name'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Email Address<span> *</span>
                            </label>
                            <input type="email"  name="billing[email]" value="{{old('billing.email')}}" >
                        </div>
                        @if(count($errors->get('billing.email')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.email'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Password<span> *</span>
                            </label>
                            <input type="password"  name="billing[password]">
                        </div>
                        @if(count($errors->get('billing.password')))
                            <div class="left"><span class="text-danger">@if($errors->first('billing.password') != 'The password confirmation does not match.'){{ cleanUpPrefix($errors->first('billing.password')) }}@endif</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Retype Password<span> *</span>
                            </label>
                            <input type="password"  name="billing[password_confirmation]">
                        </div>
                        @if(count($errors->get('billing.password')))
                            <div class="left"><span class="text-danger">@if($errors->first('billing.password') == 'The password confirmation does not match.') The password confirmation does not match.
 @else {{ cleanUpPrefix($errors->first('billing.password')) }} @endif</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Company Name
                            </label>
                            <input type="text"  name="billing[company]" value="{{old('billing.company')}}">
                        </div>
                        @if(count($errors->get('billing.company')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.company'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                                Address<span> *</span>
                            </label>
                            <input type="text"  name="billing[address]" placeholder="Street Address" value="{{old('billing.address')}}">
                        </div>
                        @if(count($errors->get('billing.address')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.address'))}}</span></div>
                        @endif
                    </div>
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               &nbsp;
                            </label>
                            <input type="text" placeholder="Apartment, suite, unit etc. (optional)" name="billing[address2]" value="{{old('billing.address2')}}">
                        </div>
                        <div class="left"></div>
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Country<span> *</span>
                            </label>
                            <select name="billing[country]">
                                @foreach($country as $value)
                                    <option {{ (old('billing.country','Indonesia') == $value->name) ? 'selected' : ''  }} value="{{$value->name}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if(count($errors->get('billing.country')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.country'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Town / City<span> *</span>
                            </label>
                            <input type="text"  name="billing[city]" placeholder="" class="city" value="{{old('billing.city')}}">
                        </div>
                        @if(count($errors->get('billing.city')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.city'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               District<span> *</span>
                            </label>
                            <input type="text"  name="billing[district]" placeholder="" class="district" value="{{old('billing.district')}}">
                        </div>
                        @if(count($errors->get('billing.district')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.district'))}}</span></div>
                        @endif
                    </div>

                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               State<span> *</span>
                            </label>
                            <input type="text"  name="billing[state]" placeholder="" value="{{old('billing.state')}}">
                        </div>
                        @if(count($errors->get('billing.state')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.state'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                              Zip<span> *</span>
                            </label>
                            <input type="text"  name="billing[postal_code]" placeholder="" value="{{old('billing.postal_code')}}" id="postal_code">
                        </div>
                        @if(count($errors->get('billing.postal_code')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.postal_code'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Phone<span> *</span>
                            </label>
                            <input type="tel"  name="billing[phone]" id="phone" placeholder="+62" value="{{old('billing.phone')}}">
                        </div>
                        @if(count($errors->get('billing.phone')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.phone'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Order Notes
                            </label>
                            <input type="text" name="billing[order_notes]" placeholder="Notes about your order, e.g. special notes for delivery." value="{{old('billing.order_notes')}}">
                        </div>
                        @if(count($errors->get('billing.order_notes')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('billing.order_notes'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               &nbsp;
                            </label>
                            <div class="right radio-cont">
                                <div>
                                    <input {{ (old('billToShip','yes') == 'no') ? 'checked' : '' }} type="checkbox" name="billToShip" class="" value="no"> <span>I want to change my address for shipping information</span><br>
                                </div>
                            </div>
                        </div>
                        <div class="left"><span class="text-danger"></span></div>
                        <script>
							 $(document).ready(function(){

                                $('input[type="checkbox"]').on('change', function(e) {
                                    if(e.target.checked){
                                        $(".shipping-info-reg").show();
                                    }else{
                                        $(".shipping-info-reg").hide();
                                    }
                                });
                                @if(old('billToShip') == 'no')
                                    $(".shipping-info-reg").show();
                                @endif
                            });


						</script>
                    </div>
                    
                    
                    <div class="shipping-info-reg">
                        
                        <h4>Please fill your shipping information</h4>
                        <div class="separator"></div>
                        
                        <div class="input-row">

                            <div class="left">
                                <label>
                                    Receiver Name<span> *</span>
                                </label>
                                <input type="text" name="shipping[first_name]" value="{{old('shipping.first_name')}}" class="firstname">
                            </div>
                            @if(count($errors->get('shipping.first_name')))
                                <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.first_name'))}}</span></div>
                            @endif
                        </div>
                    
                        <div class="input-row">

                            <div class="left">
                                <label>
                                    Address<span> *</span>
                                </label>
                                <input type="text" name="shipping[address]" placeholder="Street Address" value="{{old('shipping.address')}}">
                            </div>
                            @if(count($errors->get('shipping.address')))
                                <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.address'))}}</span></div>
                            @endif
                        </div>
                        <div class="input-row">

                            <div class="left">
                                <label>
                                   &nbsp;
                                </label>
                                <input type="text" placeholder="Apartment, suite, unit etc. (optional)" name="shipping[address2]" value="{{old('shipping.address2')}}">
                            </div>
                            <div class="left"></div>
                        </div>
                        <div class="input-row">
                    
                            <div class="left">
                                <label>
                                   Country<span> *</span>
                                </label>
                                <select name="shipping[country]">
                                    @foreach($country as $value)
                                        <option {{ (old('shipping.country','Indonesia') == $value->name) ? 'selected' : ''  }} value="{{$value->name}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            @if(count($errors->get('shipping.country')))
                                <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.country'))}}</span></div>
                            @endif
                        </div>
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Town / City<span> *</span>
                            </label>
                            <input type="text" name="shipping[city]" placeholder="" class="rcity" value="{{old('shipping.city')}}">
                        </div>
                        @if(count($errors->get('shipping.city')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.city'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               District<span> *</span>
                            </label>
                            <input type="text" name="shipping[district]" placeholder="" class="rdistrict" value="{{old('shipping.district')}}">
                        </div>
                        @if(count($errors->get('shipping.district')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.district'))}}</span></div>
                        @endif
                    </div>

                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               State<span> *</span>
                            </label>
                            <input type="text" name="shipping[state]" placeholder="" value="{{old('shipping.state')}}">
                        </div>
                        @if(count($errors->get('shipping.state')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.state'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                              Zip<span> *</span>
                            </label>
                            <input type="text" name="shipping[postal_code]" placeholder="" value="{{old('shipping.postal_code')}}" id="postal_code">
                        </div>
                        @if(count($errors->get('shipping.postal_code')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.postal_code'))}}</span></div>
                        @endif
                    </div>
                    
                    <div class="input-row">
                    
                        <div class="left">
                            <label>
                               Phone<span> *</span>
                            </label>
                            <input type="tel" name="shipping[phone]" id="phone" placeholder="+62" value="{{old('shipping.phone')}}">
                        </div>
                        @if(count($errors->get('shipping.phone')))
                            <div class="left"><span class="text-danger">{{cleanUpPrefix($errors->first('shipping.phone'))}}</span></div>
                        @endif
                    </div>
                </div>
                    
                    
                        <button type="submit" name="submit" value="register">CONTINUE</button>
                        
                 
                </form>
                
            </div>
            
        
        </div>

<script type="text/javascript">

    var number = document.getElementById('phone');
    number.onkeydown = function(e) {
        if(!((e.keyCode > 95 && e.keyCode < 106)
          || (e.keyCode > 47 && e.keyCode < 58) 
          || e.keyCode == 8)) {
            return false;
        }
    }

    var postal = document.getElementById('postal_code');
    postal.onkeydown = function(e) {
        if(!((e.keyCode > 95 && e.keyCode < 106)
          || (e.keyCode > 47 && e.keyCode < 58) 
          || e.keyCode == 8)) {
            return false;
        }
    }

    var postalReceiver = document.getElementById('receiver_postal_code');
    postalReceiver.onkeydown = function(e) {
        if(!((e.keyCode > 95 && e.keyCode < 106)
          || (e.keyCode > 47 && e.keyCode < 58) 
          || e.keyCode == 8)) {
            return false;
        }
    }

    $('.firstname').bind('keyup blur',function(){ 
        var node = $(this);
        node.val(node.val().replace(/[^a-z]/g,'') ); }
    );

    $('.lastname').bind('keyup blur',function(){ 
        var node = $(this);
        node.val(node.val().replace(/[^a-z]/g,'') ); }
    );

</script>

{{--autocomplete style--}}
<style type="text/css">
    .ui-autocomplete {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 999999999;
        float: left;
        display: none;
        min-width: 160px;
        padding: 4px 0;
        margin: 0 0 10px 25px;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }

    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }

    .ui-state-hover, .ui-state-active {
        color: white;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }

    .ui-helper-hidden-accessible{
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
</style>
