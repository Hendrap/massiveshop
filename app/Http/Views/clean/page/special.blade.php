 
        <div class="main">
        
            <div class="main-container category special-offer">
                <h1>SPECIAL OFFER</h1>
                
                <section class="categories">
                @foreach($data as $d)
                    <div class="banner-category">
                        <?php $link=getEntryMetaFromArray($d->metas,'link'); ?>
                        <a href="@if(!empty($link)) {{ url($link) }} @endif">
                            <img src="{{asset(getCropImage($d->medias[0]->path, 'about',true))}}" alt="{{$d->medias[0]->title}}">
                            <h3>{{parseMultiLang($d->title)}}</h3>
                        </a>
                    </div>
                @endforeach
                </section>
                    
            </div>
        
        </div>
