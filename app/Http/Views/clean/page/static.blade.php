 
        <div class="main">
            
            <div style="background: white" class="main-container">
                
                <h1>{{ parseMultiLang($page->title) }}</h1>

                <div class="the-content">
                    @if(!empty($page->medias[0]))
                        <img src="{{ asset($page->medias[0]->path) }}" alt="image name here">
                    @endif
                    {!!parseMultiLang($page->content)!!}
                </div>
                
            </div>
        
        </div>

        <style type="text/css">

            .main-container{
                width: 80%;
                margin: auto;
            }
            .main-container h1{
                padding-bottom: 0px;
            }
            .the-content strong{
                font-weight: 500;
            }
            .the-content {
                margin-top: 75px;
                margin-bottom: 75px;
                padding-left: 5%;
                padding-right: 5%;
            }
            .the-content *{
                width: 100%;
                text-align: left;
                max-width:100%;
                line-height: 20px;
                list-style: initial;
            }
            .the-content ul,ol{
                padding: inherit;
            }

        </style>