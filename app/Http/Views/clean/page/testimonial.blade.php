

		<div class="main">

		

			<div style="background: white" class="main-container contact">

				<h1 style="width: 50%;margin: auto;">
					TESTIMONIAL					
				</h1>
				<div style="height: 2px;width: 50%;margin: auto!important;background-color: #bfd22b;border-color: #bfd22b;margin: 20px 0;"></div>
				<div class="contact-content" style="margin-top: 30px">

				

				
					<div style="width: 475px;margin: auto;">


						

						<form id="contact-form" method="POST" action="<?php echo url('post-testimonial'); ?>">

							{{ csrf_field() }}
							@if(Session::has('msg'))
							<div class="left"><span class="text-success">{{ Session::get('msg') }}<br><br></span></div>
							@endif
							@if(count($errors))
							<div class="left"><span class="text-danger">Failed! Please fill required field.<br><br></span></div>
							@endif

							<div class="input-row">

								<label>Name</label>

								<input class="name" type="text" name="firstname" placeholder="First Name"> <input class="name" type="text" name="lastname" placeholder="Last Name">
								@if(count($errors->get('firstname')))
									<div class="left"><span class="text-danger">{{$errors->first('firstname')}}</span></div>
								@endif
								@if(count($errors->get('lastname')))
									<div class="right" style="margin-top:-19px"><span class="text-danger">{{$errors->first('lastname')}}</span></div>
								@endif
							</div>

							

							<div class="input-row">

								<label>Email</label>

								<input type="email" placeholder="Your Email" name="email">
								@if(count($errors->get('email')))
									<div class="left"><span class="text-danger">{{$errors->first('email')}}</span></div>
								@endif
							</div>

							


							

							<div class="input-row">

								<label>Testimonial</label>

								<textarea name="testimonial" placeholder="Your Testimonial"></textarea>
								@if(count($errors->get('testimonial')))
									<div class="left"><span class="text-danger">{{$errors->first('testimonial')}}</span></div>
								@endif
							</div>

							
							<div style="text-align: center">
								<button style="float: none;" type="submit">SEND</button>						
							</div>
	
							

						</form>

						

					</div>



					
				

				</div>

				

				

				

			</div>

			

		</div>
