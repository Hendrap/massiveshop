

			<div class="main">
			
			<div class="main-container account forget">
			
				<h1>Unsubscribe</h1>
				
				<p>
					You have been removed from Neneq Mailing List
					<br>
					If you have a moment, please let us know why you unsubscribed:
				</p>
	
				 
	    
	    	<form action="{{url('unsubscribe-save')}}" method="post" class="form-unsubscribe">
	    		{{ csrf_field() }}
	      		<textarea class="unsub-reason" name="reason"></textarea>
	    		
				<input type="hidden" name="email" value="{{$data}}">
				<button class="btn btn-register" type="submit">SUBMIT</button>
	    	</form>
			</div>

		</div>
