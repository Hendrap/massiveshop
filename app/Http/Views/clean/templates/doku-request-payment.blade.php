<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<form id="dokuForm" action="{!! $endPoint !!}" method="post">
	<input name="MALLID" type="hidden" value="{!! $mallid !!}">
	<input name="BASKET" type="hidden" value="{!! $basket !!}">
	<input name="AMOUNT" type="hidden" value="{!! $grandTotal !!}">
	<input name="PURCHASEAMOUNT" type="hidden" value="{!! $grandTotal !!}">
	<input name="CHAINMERCHANT" type="hidden" value="NA">
	<input name="TRANSIDMERCHANT" type="hidden" value="{!! $orderNumber !!}">
 	<input name="WORDS" type="hidden" value="{!! $words !!}">
	<input name="CURRENCY" type="hidden" value="{!! $currency !!}">
	<input name="PURCHASECURRENCY" type="hidden" value="{!! $currency !!}">
	<input name="COUNTRY" type="hidden" value="ID">
	<input name="SESSIONID" type="hidden" value="{!! $sessionId !!}">
	<input name="REQUESTDATETIME" type="hidden" value="{!! $requestDateTime !!}">
	<input name="NAME" type="hidden" value="{!! $name !!}">
	<input name="EMAIL" type="hidden" value="{!! $email !!}">
	<input name="PAYMENTCHANNEL" type="hidden" value=""> 
	<button style="display: none" type="submit">Submit</button>
</form>

<script type="text/javascript">
	document.getElementById('dokuForm').submit()
</script>
</body>
</html>