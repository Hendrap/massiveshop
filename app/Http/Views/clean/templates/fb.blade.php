<script type="text/javascript">
  var alphaJS = {};

  window.fbAsyncInit = function() {
    FB.init({
      appId      : "{{ config('alpha.application.facebookAppId') }}",
      xfbml      : true,
      version    : 'v2.8'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  
  alphaJS.facebookLogin = function(callback){
    FB.login(function(response){
      callback(response);
    },{scope: 'public_profile,email'});
  }


alphaJS.neneqFacebookLogin = function(){
  alphaJS.facebookLogin(function(response){
    if(response.status == 'connected'){
      $.ajax({
        url:"{{ url('facebook-login') }}",
        data:{_token:$("meta[name=_token]").attr('content'),id:response.authResponse.userID,token:response.authResponse.accessToken},
        type:"POST",
        dataType:"json",
        success:function(data){
          if(data.status == 1){
            window.location.reload();
          }else{
            alert(data.msg);
          }
        }
      });
    }else{
      alert("Login Failed!");
    }
  });
}

$(document).ready(function(){
  $(".neneq-fb-login").click(function(e){
      e.preventDefault();
      alphaJS.neneqFacebookLogin();
  });
})

</script>