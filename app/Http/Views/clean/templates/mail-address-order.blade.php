<td>
    <b>Billing to</b><br>
    {{ $billing->first_name.' '.$billing->last_name }} <br>
    {{ $billing->address }} <br>
    {{ $billing->district }} <br>
    {{ $billing->city.' '.$billing->postal_code }} 
</td>
<td>
    <b>Shipping to</b><br>
    {{ $shipping->first_name.' '.$shipping->last_name }} <br>
    {{ $shipping->address }} <br>
    {{ $shipping->district }} <br>
    {{ $shipping->city.' '.$shipping->postal_code }} 
</td>