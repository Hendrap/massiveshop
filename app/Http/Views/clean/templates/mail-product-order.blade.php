<tr>
    <td style="border-bottom: 1px solid #e6e7e8;padding-left:10px;" valign="middle" height="30" width="50%" style="width:50%">
        <b>{{ $value->product_title }}</b>
    </td>
    <td style="border-bottom: 1px solid #e6e7e8" valign="middle" height="30" align="right" style="width:25%">
        <b>x {{ $value->qty }}</b>
    </td>
    <td style="border-bottom: 1px solid #e6e7e8;padding-right:10px;" valign="middle" height="30" align="right" style="width:25%">
        <b>{{ formatMoney($value->original_price) }}</b>
    </td>
</tr>