						<div class="top">
							<div class="message">
								<img src="{{ asset("bundles/img/add-success.jpg") }}" alt=""><span>Item Succesfuly added to cart</span>
							</div>
							
							<div class="product-added">
								<table>
									<tr>
										<td width="95%">
											<span id="itmName">{{ $selectedItem->product_title }}</span>
											<span id="itmQTY">{{ getCurrencySymbol() }} {{ formatMoney($selectedItem->original_price) }} @ {{ number_format($selectedItem->qty) }}</span>
										</td>
									</tr>
								
								</table>
							</div>
						</div>
						<div class="bottom">
						
							<div class="subtotal">
								<span>{{ getCurrencySymbol() }} {{ formatMoney($subTotal) }}</span>
								<span>*EXCLUDE SHIPPING</span>
								<a href="{{url('/checkout')}}">CHECK OUT</a>
							</div>
							
						</div>