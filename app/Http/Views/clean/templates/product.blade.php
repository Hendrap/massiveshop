<?php $product = parseProduct($product); ?>
<div class="product-grid">
	<div class="image">
	@if(count($product->medias))
		<a href="{{url('product')}}/{{$product->slug}}">
			<img src="{{ asset(getCropImage($product->medias[0]->path, 'product')) }}" alt="{{parseMultiLang($product->title)}}">
		</a>
	@endif
	</div>

	<div class="desc">
		<a href="{{url('product')}}/{{$product->slug}}" class="viewproduct">View Product</a>
		<div class="desc-item">
			<h4 class="product-name">{{parseMultiLang($product->title)}}</h4>
			@if(count($product->items) && $product->isComingSoon == false && $product->isContactOnly == false)
				@if(isSale($product->items[0]))
					<span class="price"> {{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($product->items[0]->sale_price)) }}</span>
				@else
					<span class="price"> {{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($product->items[0]->price)) }}</span>
				@endif
			@endif
		</div>
	</div>
</div>