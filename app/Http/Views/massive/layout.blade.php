<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">

    <!--favicon icon-->
    <link rel="icon" type="image/png" href="img/favicon.png">

    <title>{{ $title or app('AlphaSetting')->getSetting('site_name') }}</title>

    <!--common style-->

    
<link href='http://fonts.googleapis.com/css?family=Abel|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic,200italic,200' rel='stylesheet' type='text/css'>

<link href="{{ asset('massive/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('massive/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('massive/css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('massive/css/shortcodes/shortcodes.css') }}" rel="stylesheet">
    <link href="{{ asset('massive/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('massive/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ asset('massive/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('massive/css/style-responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('massive/css/default-theme.css') }}" rel="stylesheet">    
    <link href="{{ asset('massive/css/blog.css') }}" rel="stylesheet">   

    <script src="{{ asset('massive/js/jquery-1.10.2.min.js') }}"></script>
    <!-- Include Vue -->
    <script type="text/javascript" src="{{ asset('massive/js/vue/vue.js') }}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!--  preloader start -->
<div id="tb-preloader">
    <div class="tb-preloader-wave"></div>
</div>
<!-- preloader end -->


<div class="wrapper" id="app">

        <!--header start-->
        <header id="header" class=" header-full-width ">

            <div class="header-sticky light-header ">

                <div class="container">
                    <div id="massive-menu" class="menuzord">

                        <!--logo start-->
                        <a href="{{url('/')}}" class="logo-brand">
                            <img class="retina" src=" {{ asset('massive/img/logo.png') }}" alt=""/>
                        </a>
                        <!--logo end-->

                        <!--mega menu start-->
                        <ul class="menuzord-menu pull-right">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><a href="#">Product</a>
                                <?php $category = Taxonomy::select()->where('taxonomy_type', 'category')->get(); ?>
                                <ul class="dropdown">
                                    @foreach($category as $c => $cat)
                                    <li><a href="{{url('products/category',$cat->taxonomy_slug)}}">{{$cat->name}}</a> </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="{{url('/news-event')}}">News & Event</a></li>
                            <li><a href="#">Special Offer</a></li>
                            @if(Auth::User())
                            <li class=""><a href="#">My Account</a>
                                <ul class="dropdown">
                                    <li><a href="#">My Profile</a></li>
                                    <li><a href="#">Order History</a></li>
                                    <li><a href="{{url('/logout')}}">Logout</a></li>
                                </ul>
                            </li>
                            @else
                            <li class=""><a href="{{url('/login')}}">Login</a>
                            </li>
                            @endif

                            <li  class="nav-icon nav-divider">
                                <a href="javascript:void(0)">|</a>
                            </li>

                            <li class="nav-icon">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-search"></i> Search
                                </a>
                                <div class="megamenu megamenu-quarter-width">
                                    <div class="megamenu-row">
                                        <div class="col12">
                                            <form role="form">
                                                <div class="">
                                                    <input type="text" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li  class="nav-icon cart-info" id="shopping-cart">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-shopping-cart"></i> (@{{itemCount}})
                                </a>
                                <div class="megamenu megamenu-quarter-width">
                                    <div class="megamenu-row">
                                        <div class="col12">

                                            <!--cart-->
                                            <table class="table cart-table-list table-responsive">
                                                <div>
                                                    
                                                <tr is="cart-item" v-for="cd in cartData"
                                                           v-bind:item="cd"
                                                           v-bind:key="cd.item_id"></tr>
                                                </div>
                                            </table>
                                            <div class="total-cart pull-right">
                                                <ul>
                                                    <li><span>Total</span> <span>Rp. @{{sumTotal}}</span></li>
                                                </ul>
                                            </div>
                                            <div class="s-cart-btn pull-right">
                                                <a href="#" class="btn btn-small btn-theme-color light-hover""> View cart</a>
                                                <a href="#" class="btn btn-small btn-light-solid  btn-transparent"> Checkout</a>
                                            </div>
                                            <!--cart-->

                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <!--mega menu end-->

                        <!-- Template -->
                        <template id="cart-item-template">
                            <tr>
                                <td><a href="#"><img src="{{asset('/massive/img/product/1.jpg')}}" alt=""/></a></td>
                                <td><a href="#"> @{{item.product_name}}</a></td>
                                <td>X@{{item.qty}}</td>
                                <td>Rp.@{{formatPrice(item.original_price)}}</td>
                                <td><a href="#" class="close" v-on:click="removeItemFromCart(item.item_id)"><img src=" {{ asset('massive/img/product/close.png') }}" alt=""/></a></td>
                            </tr>
                        </template>
                        <!-- Template end -->

                    </div>
                </div>
            </div>

        </header>
        <!--header end-->

        @if(!empty($content))

            <?php echo $content; ?>

        @endif

        <!--footer start 1-->
        <footer id="footer" class="dark">
            <div class="primary-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#" class="m-bot-20 footer-logo">
                                <img class="retina" src=" {{ asset('massive/img/logo-dark.png') }}" alt=""/>
                            </a>
                            <p>Massive is  fully responsible, performance oriented and SEO optimized, retina ready WordPress theme.</p>
                            
                        </div>
                        <div class="col-md-3">
                            <h5 class="text-uppercase">recent posts</h5>
                            <ul class="f-list">
                                <li><a href="#">Standard Blog post</a></li>
                                <li><a href="#">Quotation post</a></li>
                                <li><a href="#">Audio Post</a></li>
                                <li><a href="#">Massive Video Demo</a></li>
                                <li><a href="#">Blog Image Post</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h5 class="text-uppercase">quick link</h5>
                            <ul class="f-list">
                                <li><a href="#">About Massive</a></li>
                                <li><a href="#">Career</a></li>
                                <li><a href="#">Terms & Condition</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h5 class="text-uppercase">Recent Work</h5>
                            <ul class="r-work">
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/1.jpg') }}" alt=""/></a>
                                </li>
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/2.jpg') }}" alt=""/></a>
                                </li>
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/3.jpg') }}" alt=""/></a>
                                </li>
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/4.jpg') }}" alt=""/></a>
                                </li>
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/5.jpg') }}" alt=""/></a>
                                </li>
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/6.jpg') }}" alt=""/></a>
                                </li>
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/7.jpg') }}" alt=""/></a>
                                </li>
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/8.jpg') }}" alt=""/></a>
                                </li>
                                <li>
                                    <a href="#"><img src=" {{ asset('massive/img/recent-work/9.jpg') }}" alt=""/></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="secondary-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="m-top-10">Copyright 2012 - 2015 Massive Theme by <a href="http://themebucket.net/" class="f-link" target="_blank">ThemeBucket</a> | All Rights Reserved</span>
                        </div>
                        <div class="col-md-6">
                            <div class="social-link circle pull-right">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--footer 1 end-->

    </div>


<!-- Placed js at the end of the document so the pages load faster -->
<script src="{{ asset('massive/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('massive/js/menuzord.js') }}"></script>
<script src="{{ asset('massive/js/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('massive/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('massive/js/jquery.isotope.js') }}"></script>
<script src="{{ asset('massive/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('massive/js/jquery.countTo.js') }}"></script>
<!-- <script src="{{ asset('massive/js/visible.js') }}"></script> -->
<script src="{{ asset('massive/js/smooth.js') }}"></script>
<script src="{{ asset('massive/js/wow.min.js') }}"></script>
<script src="{{ asset('massive/js/imagesloaded.js') }}"></script>
    <!--common scripts-->
<script src="{{ asset('massive/js/scripts.js?6') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#page-render .pagination').addClass('custom-pagination');

        // init cart
        shopping.getItemCart();
    });
    var CartPostUrl = "{{route('post_cart')}}";
    var getDataCartUrl = "{{route('get_cart_data')}}";
    var CartRemoveItemUrl = "{{route('remove_from_cart')}}";
</script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.5"></script>
<script type="text/javascript" src="{{ asset('massive/js/vue/cart.js') }}"></script>

</body>
</html>
