
        <!--page title start-->
        <section class="page-title">
            <div class="container">
                <h4 class="text-uppercase">{{strtoupper($siteTitle)}}</h4>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Category</a></li>
                    <li class="active">{{strtoupper($siteTitle)}}</li>
                </ol>
            </div>
        </section>
        <!--page title end-->

        <!--body content start-->
        <section class="body-content ">

            <div class="page-content product-grid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <!--product option-->
                            <div class="clearfix m-bot-50 inline-block">
                                <div class="pull-left m-top-10">
                                    Showing 1–10 of 55 results
                                </div>
                                <div class="pull-right">
                                    <form method="post" action="#" >
                                        <select class="form-control">
                                            <option>Default sorting</option>
                                            <option>Sort by popularity</option>
                                            <option>Sort by average ratings</option>
                                            <option>Sort by newness</option>
                                            <option>Sort by price: low to high</option>
                                            <option>Sort by price: high to low</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <!--product option-->
                            <div class="row">
                               
                                @foreach($data as $k => $n)
                                
                                {!! view('app::massive.templates.product',['product'=>$n, 'col_size'=> $col_size]) !!}

                                @endforeach

                                <div class="text-center col-md-12" id="page-render">
                                    {!! $data->render() !!}
                                </div>

                            </div>

                        </div>

                        <div class="col-md-3 col-md-offset-1 ">
                            <!--product category-->
                            <div class="widget">
                                <div class="heading-title-alt text-left heading-border-bottom">
                                    <h6 class="text-uppercase">product category</h6>
                                </div>
                                <?php $category = Taxonomy::select()->where('taxonomy_type', 'category')->get(); ?>
                                <ul class="widget-category">
                                    @foreach($category as $c => $cat)
                                    <li><a href="{{url('products/category',$cat->taxonomy_slug)}}">{{$cat->name}}</a> </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!--product category-->

                            <!--price filter-->
                            <div class="widget">
                                <div class="heading-title-alt text-left heading-border-bottom">
                                    <h6 class="text-uppercase">price filter</h6>
                                </div>
                                <form method="post" action="#">

                                    <div class="row">
                                        <div class="col-xs-6 form-group">
                                            <input type="text" name="price-from" id="price-from" class=" form-control" placeholder="From, Rp." maxlength="100">
                                        </div>

                                        <div class="col-xs-6 form-group">
                                            <input type="text" name="price-to" id="price-to" class=" form-control" placeholder="To, Rp." maxlength="100">
                                        </div>
                                        <div class=" col-xs-12 form-group">
                                            <button class="btn btn-small btn-dark-border  btn-transparent">Filter</button>
                                        </div>
                                    </div>



                                </form>
                            </div>
                            <!--price filter-->


                            <!--latest product-->
                            <div class="widget">
                                <div class="heading-title-alt text-left heading-border-bottom">
                                    <h6 class="text-uppercase">Special Offers</h6>
                                </div>
                                <ul class="widget-latest-post">
                                    @foreach($offer as $k => $n)
                                    <li>
                                        <div class="thumb"><a href="#"><img src="{{ asset(getCropImage($n->medias[0]->path,'product')) }}" alt=""></a></div>
                                        <div class="w-desk">
                                            <a href="#">{{ parseMultiLang($n->title) }}</a>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!--latest product-->

                            <!--product tags-->
                            <div class="widget">
                                <div class="heading-title-alt text-left heading-border-bottom">
                                    <h6 class="text-uppercase">PRODUCT TAGS</h6>
                                </div>
                                <div class="widget-tags">
                                    <a href="">Accessories</a>
                                    <a href="">Branding</a>
                                    <a href="">Belts</a>
                                    <a href="">Cloth</a>
                                    <a href="">Kids</a>
                                    <a href="">Watch</a>
                                    <a href="">Shoes</a>
                                </div>
                            </div>
                            <!--product tags-->

                        </div>

                    </div>
                </div>
            </div>


        </section>