        <!--page title start-->
        <section class="page-title">
            <div class="container">
                <h4 class="text-uppercase">Show All Products</h4>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Product</a></li>
                    <li class="active">All Products</li>
                </ol>
            </div>
        </section>
        <!--page title end-->

        <!--body content start-->
        <section class="body-content">

            <div class="page-content product-grid" id="product">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!--product option-->
                            <div class="clearfix m-bot-50 inline-block">
                                <div class="pull-left m-top-10">
                                    Showing 1–10 of 55 results
                                </div>
                                <div class="pull-right">
                                    <form method="post" action="#" >
                                        <select class="form-control">
                                            <option>Default sorting</option>
                                            <option>Sort by popularity</option>
                                            <option>Sort by average ratings</option>
                                            <option>Sort by newness</option>
                                            <option>Sort by price: low to high</option>
                                            <option>Sort by price: high to low</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <!--product option-->
                            <div class="row">
                                
                                @foreach($data as $k => $n)
                                
                                {!! view('app::massive.templates.product',['product'=>$n, 'col_size'=> $col_size]) !!}

                                @endforeach

                                <div class="text-center col-md-12" id="page-render">
                                    {!! $data->render() !!}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </section>
        <!--body content end-->