
	<!--page title start-->
    <section class="page-title">
        <div class="container">
            <h4 class="text-uppercase"> Login Register</h4>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Account</a></li>
                <li class="active"> Login Register</li>
            </ol>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">


        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">


                        <section class="normal-tabs line-tab">
                            <ul class="nav nav-tabs">
                                <li class="">
                                    <a data-toggle="tab" href="#tab-login">Login</a>
                                </li>
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-register">register</a>
                                </li>

                            </ul>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div id="tab-login" class="tab-pane">
                                        <div class="login register ">
                                            <div class=" btn-rounded">
                                                <form  class=" " action="{{url('login')}}" method="post">
                                                	{{ csrf_field() }}

                                                    <div class="form-group">
                                                        <input type="text"  name="email" value="" class="form-control" placeholder="Enter Your Email Here">
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="password" name="password" value="" class="form-control " placeholder="Password">
                                                    </div>

							  						<input type="hidden" name="url" value="{{\URL::previous()}}">

                                                    <div class="form-group">
                                                        <button class="btn btn-small btn-dark-solid full-width"  value="login">Login
                                                        </button>
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="checkbox" value="remember-me" id="checkbox1"> <label for="checkbox1">Remember  me</label>
                                                        <a class="pull-right" data-toggle="modal" href="#forgotPass"> Forgot Password?</a>
                                                    </div>

                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="tab-register" class="tab-pane active">
                                        @if(count(Session::get('errors')))
                                            <div class="alert alert-danger" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <i class="fa fa-lg fa-times-circle"></i> <strong>Oh snap!</strong> Change a few things up and try submitting again.
                                                {{Session::get('errors')}}
                                            </div>
                                        @endif

                                        @if(count(Session::get('email-error')))
                                            <div class="alert alert-danger" role="alert">
                                                <strong>Oops!</strong> Email already taken!.
                                            </div>
                                        @elseif(count(Session::get('msg-success')))
                                            <div class="alert alert-success" role="alert">
                                                <strong>Well done!</strong> please check your email to activate account!.
                                            </div>
                                        @endif
                                        <form class=" login" action="{{url('registerSave')}}" method="post">
                                            {{ csrf_field() }}

                                            <div class="form-group">
                                                <input type="text"  class="form-control" name="billing[first_name]" placeholder="Name" value="{{old('billing.first_name')}}">
                                            </div>

                                            <div class="form-group">
                                                <input type="text"  class="form-control" name="billing[email]" placeholder="Email" value="{{old('billing.email')}}">
                                            </div>

                                            <div class="form-group">
                                                <input type="text"  class="form-control" name="billing[phone]" placeholder="Phone" value="{{old('billing.phone')}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="password"  class="form-control" name="billing[password]" placeholder="Choose Password">
                                            </div>

                                            <div class="form-group">
                                                <input type="password"  class="form-control" name="billing[password_confirmation]" placeholder="Confirm Password">
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-small btn-dark-solid full-width " id="login-form-submit"
                                                        name="login-form-submit" value="login">Register
                                                </button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>

        </div>


    </section>
    <!--body content end-->