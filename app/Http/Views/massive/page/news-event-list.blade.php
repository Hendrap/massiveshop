
        <!--page title start-->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-uppercase">Blog Listing</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">News & Event</a></li>
                            <li class="active">Article</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->

        <!--body content start-->
        <section class="body-content ">

            @foreach($news as $k => $n)
            <div class="page-content">
                <div class="container">
                    <div class="row">

                        <!--blog list-->
                        <div class="post-list-aside">
                            <div class="post-single">
                                <div class="col-md-5">
                                    <div class="post-img">
                                        <img src="{{ asset(getCropImage($n->medias[0]->path,'news')) }}" alt="">
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="post-desk text-center">
                                        <ul class="post-cat">
                                            <li><a href="#">interface</a></li>
                                            <li><a href="#">standard</a></li>
                                        </ul>
                                        <h4 class="text-uppercase">
                                            <a href="{{route('article_detail',$n->slug)}}">{{ parseMultiLang($n->title) }}</a>
                                        </h4>
                                        <div class="date">
                                            <a href="#" class="author">Administrator</a> {{ date_format($n->created_at, 'd M Y') }}
                                        </div>
                                        <p>
                                            Phasellus fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo sem quam pellente. Awesome sliders give you the opportunity to showcase your content.
                                            fringilla suscipit risus nec eleifend. Pellentesque eu quam sem, ac malesuada leo sem quam pellente. Awesome sliders give you the opportunity to showcase your content.
                                        </p>
                                        <a href="{{route('article_detail',$n->slug)}}" class="p-read-more">Read More <i class="icon-arrows_slim_right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--blog list-->

                    </div>
                </div>
            </div>

            <hr/>
            @endforeach

            <!--pagination-->

            <div class="text-center page-content" id="page-render">
                {!! $news->render() !!}
            </div>
            <!--pagination-->


        </section>
        <!--body content end-->