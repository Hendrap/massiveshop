        <!--page title start-->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-uppercase">Shop Single</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Product</a></li>
                            <li class="active">Shop Single</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->

        <!--body content start-->
        <?php 
            $variant = $activeVariant; 
            $availableToBuy = true;
            if($data->isComingSoon && $data->isContactOnly || $variant->stock_available == 0)
                $availableToBuy = false;

        ?>
        <section class="body-content" id="product-detail">

            <div class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="post-list-aside">
                                <div class="post-single">
                                    <div class="post-slider-thumb post-img text-center">
                                        <ul class="slides">
                                            <li data-thumb="{{asset(getCropImage($data->medias[0]->path, 'productdetail',true))}}">
                                                <a href="javascript:;" title="Freshness Photo"><img src="{{asset(getCropImage($data->medias[0]->path, 'productdetail',true))}}" alt=""/></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="product-title">
                                <h2 class="text-uppercase">{{parseMultiLang($data->title)}} - {{$variant->item_title}}</h2>
                            </div>


                            @if($data->isComingSoon && $data->isContactOnly)

                                <div class="product-price txt-xl">COMING SOON, PLEASE CONTACT US ...</div>
                                                            
                            @else
                            
                                <div class="product-price txt-xl">
                                        @if(isSale($variant))
                                            <span class="border-tb p-tb-10"> {{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($variant->sale_price)) }} <del>{{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($variant->price)) }}</del></span>
                                        @else
                                            <span class="border-tb p-tb-10"> {{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($variant->price)) }} <del></del></span>
                                        @endif
                                </div>

                                <ul class="portfolio-meta m-bot-10 m-top-10">
                                    <li><span> Item No </span> {{$variant->sku}}</li>
                                    <li><span> Condition </span> New </li>
                                </ul>   
                                <p>
                                    {!! parseMultiLang($data->excerpt) !!}
                                </p>

                                <ul class="portfolio-meta m-bot-10 stock">
                                    @if($availableToBuy == true)
                                        <li><span><strong class="number-item"> {{ $variant->stock_available }}</strong> Item </span> <span class="status">In Stock</span></li>
                                    @else
                                        <li><span class="status">Out of Stock</span></li>
                                    @endif
                                </ul>
                                <div class="p-values">
                                    <ul class="portfolio-meta m-bot-10">
                                        <li>
                                            <span> Variant </span>
                                            <span>
                                                <select class="form-control" id="variant-select">
                                                    @foreach($data->items as $v => $var)
                                                    <option @if($var->item_title == $variant->item_title) selected @endif>{{$var->item_title}}</option>
                                                    @endforeach
                                                </select>
                                            </span>
                                        </li>
                                    </ul>
                                    <ul class="p-quantity m-bot-10 ">
                                        <li><label> Quantity</label></li>
                                        <li>
                                            <input type="number" id="buyQty" class="form-control" value="1" />
                                        </li>

                                        </li>
                                    </ul>
                                </div>

                                <div class="clearfix">
                                    <a href="#" class="btn btn-medium btn-dark-solid @if($availableToBuy == false) disabled @endif" id="addToCartProduct"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                </div>

                            @endif

                            
                        </div>
                    </div>
                    <div class="row page-content">
                        <div class="col-md-12">
                            <!--tabs border start-->
                            <section class="normal-tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a data-toggle="tab" href="#tab-one">More Info</a>
                                    </li>
                                    <li class="">
                                        <a data-toggle="tab" href="#tab-two">Specification</a>
                                    </li>
                                    <li class="">
                                        <a data-toggle="tab" href="#tab-three">Review (2)</a>
                                    </li>

                                </ul>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div id="tab-one" class="tab-pane active">
                                            <h4 class="text-uppercase">Product Description</h4>
                                            {!! parseMultiLang($data->content) !!}  
                                        </div>
                                        <div id="tab-two" class="tab-pane">
                                            <table class="table table-striped table-bordered">
                                                <tbody>
                                                
                                                @foreach($data->metas as $m => $meta)
                                                    <?php $skip = ['seo_title', 'seo_keys', 'seo_desc', 'alpha_keyword_tags']; 
                                                    if(in_array($meta->meta_key, $skip))
                                                        continue;
                                                    ?>

                                                    <tr>
                                                        <td>{{$meta->meta_name}}</td>
                                                        <td>{{$meta->meta_value_text}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                        <div id="tab-three" class="tab-pane">
                                            <ul class="media-list comments-list clearlist">

                                                <!-- comment item start-->
                                                <li class="media">
                                                    <a class="pull-left" href="#"><img class="media-object review-avatar" src="{{asset('massive/img/post/a1.png')}}" alt=""></a>
                                                    <div class="media-body">
                                                        <div class="comment-info">
                                                            <div class="reviewer text-uppercase">
                                                                <a href="#">John Doe</a>
                                                            </div>
                                                            July 5, 2015, at 1:20
                                                             <span class="review-rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </span>
                                                        </div>

                                                        <p>
                                                            Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
                                                        </p>
                                                    </div>
                                                </li>
                                                <!-- comment item end -->

                                                <!-- comment item -->
                                                <li class="media">
                                                    <a class="pull-left" href="#"><img class="media-object review-avatar" src="{{asset('massive/img/post/a1.png')}}" alt=""></a>
                                                    <div class="media-body">
                                                        <div class="comment-info">
                                                            <div class="reviewer text-uppercase">
                                                                <a href="#">Margarita Smith</a>
                                                            </div>
                                                            July 5, 2015, at 1:30
                                                            <span class="review-rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </span>
                                                        </div>

                                                        <p>
                                                            Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo..
                                                        </p>
                                                    </div>
                                                </li>
                                                <!-- comment item end -->

                                            </ul>

                                            <!--add review start-->
                                            <div class="heading-title-alt text-left heading-border-bottom">
                                                <h4 class="text-uppercase">ADD REVIEW</h4>
                                            </div>

                                            <form method="post" action="#" id="form" role="form" class="blog-comments">

                                                <div class="row">

                                                    <div class="col-md-6 form-group">
                                                        <!-- Name -->
                                                        <input type="text" name="name" id="name" class=" form-control" placeholder="Name *" maxlength="100" required="">
                                                    </div>

                                                    <div class="col-md-6 form-group">
                                                        <!-- Email -->
                                                        <input type="email" name="email" id="email" class=" form-control" placeholder="Email *" maxlength="100" required="">
                                                    </div>


                                                    <div class="form-group col-md-12">
                                                        <select class="form-control">
                                                            <option value="">Rating -- Select One --</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>

                                                    <!-- Comment -->
                                                    <div class="form-group col-md-12">
                                                        <textarea name="text" id="text" class=" form-control" rows="6" placeholder="Comment" maxlength="400"></textarea>
                                                    </div>

                                                    <!-- Send Button -->
                                                    <div class="form-group col-md-12">
                                                        <button type="submit" class="btn btn-small btn-dark-solid ">
                                                            Submit Review
                                                        </button>
                                                    </div>


                                                </div>

                                            </form>
                                            <!--add review end-->

                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--tabs border end-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="heading-title-alt text-left ">
                                <h3 class="text-uppercase">similar items </h3>
                                <span class="text-uppercase">We have some similar product currently in stock</span>
                            </div>

                            <!--portfolio carousel-->

                            <div id="portfolio-carousel" class=" portfolio-with-title col-3 ">
                                @foreach($otherProducts as $k => $n)
                                    {!! view('app::massive.templates.other-product',['product'=>$n]) !!}
                                @endforeach
                            </div>

                            <!--portfolio carousel-->

                        </div>
                    </div>
                </div>
            </div>

            <!--subscribe start-->
            <div class="subscribe-box gray-bg ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="subscribe-info">
                                <h4 class=" ">DON’T WANNA PURCHASE NOW? </h4>
                                <span class=" ">subscribe to get in touch </span>
                            </div>
                            <div class="subscribe-form">
                                <form class="form-inline">
                                    <input type="text" class="form-control" placeholder="Enter your email address">
                                    <button type="submit" class="btn btn-medium btn-rounded btn-dark-solid text-uppercase">subscribe</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--subscribe end-->

        </section>
        <!--body content end-->

        <script>
            $(function(){
              // bind change event to select
              $('#variant-select').on('change', function () {
                  var url = $(this).val(); // get selected value
                  if (url) { // require a URL
                      window.location = "{{url('product',$data->slug)}}/"+ url; // redirect
                  }
                  return false;
              });

              $('#addToCartProduct').click(function(){
                    //save json data product from controller  
                    var json_item = {
                        "item_id" : "{{$variant->id}}",
                        "session_id" : "",
                        "product_name" : "{{parseMultiLang($data->title)}} - {{$variant->item_title}}",
                        "qty" : 0,
                        "original_price" : "{{$variant->price}}"
                    };
                    json_item.qty = $("#buyQty").val();
                    
                    shopping.addItemToCart(json_item);
              });

            });
        </script>
