<?php $product = parseProduct($product); ?>
<div class="portfolio-item">
    <div class="thumb">
        <img src="@if(!empty($product->medias[0])) {{ asset(getCropImage($product->medias[0]->path, 'product')) }} @else {{ asset('massive/img/product/1.jpg') }} @endif" alt="">
        <div class="portfolio-hover">
            <div class="action-btn">
                <a href="@if(!empty($product->medias[0])) {{ asset(getCropImage($product->medias[0]->path, 'media')) }} @else {{ asset('massive/img/product/1.jpg') }} @endif" class="popup-link" title="{{parseMultiLang($product->title)}}"> <i class="icon-basic_magnifier"></i>  </a>
            </div>
        </div>
    </div>
    <div class="portfolio-title">
        <h4><a href="{{url('product',$product->slug)}}" class="" title="lightbox view">{{parseMultiLang($product->title)}}</a></h4>
        @if(isSale($product->items[0]))
            <p class="txt-xl">{{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($product->items[0]->sale_price)) }}</p>
        @else
            <p class="txt-xl">{{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($product->items[0]->price)) }}</p>
        @endif
    </div>
</div>