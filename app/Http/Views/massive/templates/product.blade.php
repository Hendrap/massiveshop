<?php $product = parseProduct($product); ?>

                                <div class="col-md-{{$col_size}}">
                                    <!--product list-->
                                    <div class="product-list">
                                        <div class="product-img">
                                            <a href="{{url('product',$product->slug)}}"><img src="@if(!empty($product->medias[0])) {{ asset(getCropImage($product->medias[0]->path, 'product')) }} @else {{ asset('massive/img/product/1.jpg') }} @endif" alt=""/></a>
                                            <!-- <a href="#"><img src="img/product/1-alt.jpg" alt="{{parseMultiLang($product->title)}}"/></a> -->
                                            @if($product->items[0]->stock_available > 0 && $product->isComingSoon == false && $product->isContactOnly == false)
                                            <div class="sale-label">
                                                Sale
                                            </div>
                                            @endif
                                        </div>
                                        <div class="product-title">
                                            <h5><a href="{{url('product',$product->slug)}}">{{parseMultiLang($product->title)}}</a></h5>
                                        </div>
                                        <div class="product-price">
                                            @if(isSale($product->items[0]))
                                                <del>{{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($product->items[0]->price)) }} </del>  {{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($product->items[0]->sale_price)) }}
                                            @else
                                                <del></del>{{ getCurrencySymbol() }} {{ formatMoney(convertCurrency($product->items[0]->price)) }}
                                            @endif
                                        </div>
                                        <div class="product-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="product-btn">
                                            <a href="#" class="btn btn-extra-small btn-dark-border  "><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        </div>
                                    </div>
                                    <!--product list-->
                                </div>