<?php

Route::group(['middleware' => ['web','multilang'],'namespace'=>'App\Http\Controllers'], function () {

	//index
	Route::get('auth',function(){
		Auth::login(User::find(145));
	});

	Route::get('/','FrontController@index');
	
	//currency
	Route::get('/change-currency/{type}','FrontController@changeCurrency');


	//static pages	
	Route::get('/about', 'FrontController@about');
	Route::get('/contact-us', 'FrontController@contact');
	Route::post('/contactSave', 'FrontController@contact_save');
	Route::get('/notfound','FrontController@notfound');
	Route::get('/reference', 'FrontController@reference');
	//inclue page statix
	include 'Routes/default.php';


	//testimonial
	Route::get('/testimonial','FrontController@testimonial');
	Route::post('/post-testimonial','FrontController@postTestimonial');

	//news stuff
	Route::get('/special-offer', 'FrontController@special_offer');	
	Route::get('/news/{slug?}', 'FrontController@news');


	//crednetial
	Route::post('/login','AccountController@login');
	Route::get('/logout','AccountController@logout');
	Route::post('/facebook-login','UserController@facebookLogin');


	//register
	Route::get('/login','AccountController@register');
	Route::post('/registerSave','AccountController@registerSave');
	Route::get('/account-validate/{key}','AccountController@account_validate');


	//main account
	Route::get('/account','AccountController@account'); 


	//account orders
	Route::get('/account/order-history','AccountController@order_history');
	Route::get('/account/order-detail/{id}','AccountController@order_detail');


	//account change password
	Route::get('/account/change-password','AccountController@change_password');
	Route::post('/account/change_password/send','AccountController@change_password_send');
	Route::post('/change-password-confirm','AccountController@change_password_prosses');
	

	//forget password
	Route::get('/forget-password','AccountController@forget_password');
	Route::get('/forget-password-page','AccountController@forget_password');
	Route::get('/forget-password-page/{key}','AccountController@change_password_callback');
	Route::post('/forget-password-confirm','AccountController@forget_password_prosses');
	

	//account address
	Route::get('/add-address','AccountController@add_address'); 
	Route::post('/save-address','AccountController@save_add_address'); 
	Route::get('/delete-address/{id}','AccountController@delete_address');
	Route::get('/edit-address-ship/{id}','AccountController@edit_address_ship');
	
	Route::get('/edit-shipping-address/{id}','AccountController@editShippingAddress'); 
	Route::post('/save-shipping-address','AccountController@saveShippingAddress'); 

	Route::get('/edit-billing-address/{id}','AccountController@editBillingAddress'); 
	Route::post('/save-billing-address','AccountController@saveBillingAddress'); 


	//newsletter
	Route::post('/newsletter','FrontController@newsletter');
	Route::get('/confirm-newsletter/{key}','FrontController@newsletter_confirm');
	Route::get('/unsubscribe/{key}','FrontController@unsubscribe');
	Route::get('/unsubscribe_proses/{key}','FrontController@unsubscribe_proses');
	Route::post('/unsubscribe-save','FrontController@unsubscribe_save');


	//prodicts
	Route::get('/products', 'FrontController@categories');
	Route::get('/products/category/{slug}', 'FrontController@indexByCategories');
	Route::get('/product/{slug}', 'FrontController@itemDetail');
	Route::get('/product/{slug}/{varian}', 'FrontController@itemDetail');
	

	//cart

	Route::post('/cart/save-data',['as'=>'post_cart','uses'=>'CartController@add_to_cart']);
	Route::get('/cart/get-data',['as'=>'get_cart_data','uses'=>'CartController@getCartData']);
	Route::post('/cart/remove-item',['as'=>'remove_from_cart','uses'=>'CartController@remove_from_cart']);
	Route::get('/cart','CartController@cart');

	// News & Event
	Route::get('/news-event', 'ArticleController@index');
	Route::get('/news-event/{slug}', ['as' => 'article_detail', 'uses' => 'ArticleController@detail']);




	//-------------checkout

	//address stuff
	Route::get('/checkout/add-address','CheckoutController@addAddress'); 
	Route::get('/checkout/edit-address/{id}','CheckoutController@editAddress'); 
	Route::post('/checkout/save-address','CheckoutController@saveAddress');

	//guest register
	Route::get('/checkout/guest','CheckoutController@guest'); 
	Route::post('/checkout/guest-register','CheckoutController@guestRegister'); 

	//shipping address & method
	Route::get('/checkout/billing-and-shipping','CheckoutController@billAndShip');
	Route::post('/checkout/billing-and-shipping/save','CheckoutController@billAndShipSaveData');
	Route::post('/checkout/get-quote-dhl','CheckoutController@getRealtimeDHLQuote');

	Route::get('/checkout','CheckoutController@checkout');
	Route::post('/checkout/get-shipping-method','CheckoutController@getShippingMethod');
	Route::post('/checkout/save-shipping-info','CheckoutController@saveCheckoutShipping');	

	//payment method
	Route::get('/checkout/payment','CheckoutController@payment');
	Route::post('/checkout/place-order','CheckoutController@placeOrder');

	//confirm order
	Route::get('/checkout/confirm/{id}','CheckoutController@payment');

	//reposnse page
	Route::get('/checkout/thank-you','FrontController@thankYou');
	Route::post('/checkout/thank-you','FrontController@thankYou');

});

Route::any('/webhook/notify/doku/order','App\Http\Controllers\PaymentController@handleNotifyDoku');
Route::any('/webhook/notify/paypal/order','App\Http\Controllers\PaymentController@handleNotifyPaypal');
