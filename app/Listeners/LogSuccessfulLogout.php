<?php
/*
 *
 * @author CadisEtramaDiRaizel (Luthfifs97@gmail.com)
 * 
 * 
*/
namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SomeEvent  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        \CatalogCart::beforeLogout(@(int)$event->user->id);
    }
}
