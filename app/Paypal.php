<?php 

namespace App;

use Illuminate\Http\Request;
use DB;
use Log;
use CatalogOrder;
use PayPal\Api\Item;
use PayPal\Auth\OAuthTokenCredential as PaypalAuth;
use PayPal\Rest\ApiContext;
use PayPal\Api\Payer;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\ItemList;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\Webhook;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\VerifyWebhookSignature;
use PayPal\Api\WebhookEvent;
use PayPal\Api\ShippingAddress;
use PayPal\Api\InputFields;
use PayPal\Api\WebProfile;
use PayPal\Exception\PayPalConnectionException;

class Paypal
{
	public function getApiContext()
	{
		$sdkConfig = array(
			"mode" => config('alpha.application.paypal.env')
		);


		$oauthCredential = new PaypalAuth(config('alpha.application.paypal.clientId'),config('alpha.application.paypal.secretKey'));

		$apiContext = new ApiContext($oauthCredential);
		$apiContext->setConfig($sdkConfig);
	
		return $apiContext;
	}

	public function capture(
		$items = array(),
		$subTotal = 0,
		$shippingCost = 0,
		$orderNumber = "",
		$email = "",
		$name = "",
		$currency = "",
		$shipping
		)
	{

		$apiContext = $this->getApiContext();

		$payer = new Payer();
		$payer->setPaymentMethod("paypal");



		$details = new Details();
        $details->setSubtotal($subTotal);
        $details->setShipping($shippingCost);

		$amount = new Amount();
		$amount->setCurrency($currency);
		$amount->setTotal($subTotal + $shippingCost);
		$amount->setDetails($details);


		$parsedItems = [];
		foreach ($items as $key => $value) {
			$item = new Item();
			$item->setQuantity($value->qty);
			$item->setName($value->product_title);
			$item->setPrice($value->original_price);
			$item->setCurrency($currency);
			$item->setSku($value->item_id);

			$parsedItems[] = $item;

		}

		$countryCode = DB::table('countries')->where('name','=',$shipping->country)->first();

		// $shippingAddress = new ShippingAddress();
		// $shippingAddress->setCity($shipping->city);
		// $shippingAddress->setCountryCode($countryCode->code);
		// $shippingAddress->setPostalCode($shipping->postal_code);
		// $shippingAddress->setLine1($shipping->address);
		// $shippingAddress->setState($shipping->state);
		// $shippingAddress->setPhone($shipping->phone);
		// $shippingAddress->setRecipientName($shipping->first_name.' '.$shipping->last_name);


		$itemList = new ItemList();
		$itemList->setItems($parsedItems);
		//$itemList->setShippingAddress($shippingAddress);



		$transaction = new Transaction();
		$transaction->setDescription($orderNumber);
		$transaction->setAmount($amount);
		$transaction->setItemList($itemList);


		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl(url("/checkout/thank-you"));
		$redirectUrls->setCancelUrl(url("/"));


		$payment = new Payment();
		$payment->setIntent("sale");
		$payment->setPayer($payer);
		$payment->setRedirectUrls($redirectUrls);
		$payment->setTransactions(array($transaction));
		$payment->setExperienceProfileId(config('alpha.application.paypal.profileId'));


		try {

			$payment->create($apiContext);

		}catch(PayPalConnectionException $e) {

			Log::info($e->getData());
			Log::info($e->getMessage());
			abort(500,'Internal Server Error');

		}

		$order = CatalogOrder::whereOrderNumber($orderNumber)->first();
		if(!empty($order))
		{
			$order->paypal_payment_id = $payment->getId();
			$order->save();
		}

		return $payment->getApprovalLink();


	}

	public function executePayment($paymentId,$payerId)
	{
		$apiContext = $this->getApiContext();

	    $payment = Payment::get($paymentId, $apiContext);
		
		$execution = new PaymentExecution();
		$execution->setPayerId($payerId);

		try {

			$result = $payment->execute($execution, $apiContext);
		
		} catch (PayPalConnectionException $e) {
			Log::info($e->getData());
			Log::info($e->getMessage());
			abort(500,'Internal Server Error');

		}


	}

	public function verifyNotification($req)
	{
		$headers = getallheaders();

		$headers = array_change_key_case($headers, CASE_UPPER);

		$signatureVerification = new VerifyWebhookSignature();
		$signatureVerification->setAuthAlgo($headers['PAYPAL-AUTH-ALGO']);
		$signatureVerification->setTransmissionId($headers['PAYPAL-TRANSMISSION-ID']);
		$signatureVerification->setCertUrl($headers['PAYPAL-CERT-URL']);
		$signatureVerification->setWebhookId(config('alpha.application.paypal.webhookId'));
		$signatureVerification->setTransmissionSig($headers['PAYPAL-TRANSMISSION-SIG']);
		$signatureVerification->setTransmissionTime($headers['PAYPAL-TRANSMISSION-TIME']);

		$webhookEvent = new WebhookEvent();
		$webhookEvent->fromJson(json_encode($req->input()));
		$signatureVerification->setWebhookEvent($webhookEvent);
		$request = clone $signatureVerification;

		try {
			 $output = $signatureVerification->post($this->getApiContext());
		} catch (Exception $e) {
			Log::info("Error");
			Log::info($e->getData());
			Log::info($e->getMessage());
			abort(500,'Internal Server Error');
		}

		return $output->getVerificationStatus();

	}

	public function handleNotification(Request $req){

		$json = json_decode(json_encode($req->all()));

		if(strtolower($this->verifyNotification($req)) == 'success'){
			if($req->event_type == 'PAYMENT.SALE.COMPLETED')
			{
				if($json->resource->state == 'completed')
				{
					$order = CatalogOrder::where('paypal_payment_id','=',$json->resource->parent_payment)->first();
					if(!empty($order))
					{
						app('App\Http\Controllers\CheckoutController')->sendEmailOrder($order->id,'payment-confirmation',app('AlphaSetting')->getSetting('admin_email_contact'));
						$order->status = 'APPROVED';
						$order->save();
					}
				}
			}
		}
	}

	public function createProfile()
	{
		$inputFields = new InputFields();
        $inputFields->setNoShipping(1);

        $webProfile = new WebProfile();
		$webProfile->setName("Neneq")->setInputFields($inputFields)->setTemporary(false);


		$createProfileResponse = $webProfile->create($this->getApiContext());

		return $createProfileResponse->getId();


	}

}	