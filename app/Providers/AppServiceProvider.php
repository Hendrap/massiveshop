<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $req = app('Illuminate\Http\Request');
        $this->app->register('App\Providers\AuthServiceProvider');
        $this->app->register('App\Providers\EventServiceProvider');
        $this->loadViewsFrom(base_path('/app/Http/Views'), 'app');

        if($req->segment(1) != 'admin'){
            $this->app->register('App\Providers\RouteServiceProvider');
            $this->app['router']->middleware('multilang', \App\Http\Middleware\MultiLanguage::class);                         
        }
        include(base_path('vendor/veritrans/Veritrans.php'));

        
    }
}
