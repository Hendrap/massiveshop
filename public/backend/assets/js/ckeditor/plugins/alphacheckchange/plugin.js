﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

(function() {
	CKEDITOR.plugins.add( 'alphacheckchange', {
		hidpi: true, // %REMOVE_LINE_CORE%
		onLoad: function(editor) {
			initPreventClose(editor);			
		},
		init: function( editor ) {
			 initPreventClose(editor);
		}
	});
})();
