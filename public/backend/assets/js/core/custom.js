function checktablestate(elem){
                            
                                if(elem.data().length == 0){
                                    $('.dataTables_wrapper').addClass('empty_state');
                                }
                           
                            }
$(document).ready(function(){
    
    $(".content-wrapper").find('.icon-arrow-left52.position-left').hide();

    $(window).load(function(){
        $('.table-actions .btn-group').each(function(){
       
            $(this).addClass('dropdown');
            $(this).removeClass('dropup');
        });
    });
    $('.sidebar-main-toggle').click(function(){
        $('.navbar-header').toggleClass('minimize');
    })
    
    
    //create new
    
     $( "#publish_option" ).change(function () {
                $( "#publish_option option:selected" ).each(function() {
                    if($(this).val() == 'scheduled_publish'){
                        $('.publish-date').show();
                    }
                    else{
                        $('.publish-date').hide();
                    }
                });
            });
            
            $('.edit-url').click(function(e){
                e.preventDefault;
                $('.post-url span').hide();
                $('.post-url input').show();
                $('.cancel-edit-url, .save-url, .separator-url-form').show();
                $('.edit-url').hide();
            });
            
            
            $('.save-url').click(function(e){
                e.preventDefault;
                $('.cancel-edit-url, .save-url, .separator-url-form').hide();
                $('.post-url span').show();
                $('.post-url input').hide();
                $('.edit-url').show();
                newurl = $('.post-url input').val();
                $('.post-url span').html('').html(newurl);
            });
            
            $('.cancel-edit-url').click(function(e){
                e.preventDefault;
                $('.cancel-edit-url, .save-url, .separator-url-form').hide();
                $('.post-url span').show();
                $('.post-url input').hide();
                $('.edit-url').show();
            });
            
            $(document).on('keydown', '.post-url input', function(e) {
                if (e.keyCode == 32) return false;
            });

             $('.select-two').each(function() {

                select_placeholder = $(this).attr('placeholder');
                $(this).select2({
                     minimumResultsForSearch: Infinity,
                     placeholder: select_placeholder,
                                   
                });

             });
             $('.multiselect-filtering').multiselect({
                    enableFiltering: true,
                    templates: {
                        filter: '<li class="multiselect-item multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text"></li>'
                    },
                    onChange: function() {
                        $.uniform.update();
                    }
                });
           
            function insertslide(arraydata){
               
                var slidelist = $('input[name="radio_slide"]:checked').data('slides');
                var imagetoparse = '';
                var imagetemp;
                $.each(slidelist, function( index, value ) {
                    imagetemp = '<div class="swiper-slide"><img src="assets/images/'+value+'" alt="'+value+'"></div>';
                    imagetoparse =  imagetoparse + imagetemp;
                });
                var slideindetifier = $('input[name="radio_slide"]:checked').attr('id');
                var slidelayout = '<div class="summernote-slider'+slideindetifier+'"><div class="swiper-wrapper">'+imagetoparse+'</div></div>';
                
//                $('.text-editor').summernote('pasteHTML', slidelayout);
                
                
            };
            
            // $('.add-slider-to-editor').click(function(e){
            //     if (!$('input[name="radio_slide"]:checked').val()) {
            //         new PNotify({
                        
            //             text: 'No slideshow selected.',
            //             icon: 'icon-blocked',
            //             type: 'error'
            //         });
            //     }
            //     else{
            //          insertslide();
            //         $('#modal_add_slide').modal('hide')
            //     }
               
            // });
                            
            var addslideButton = function (context) {
              var ui = $.summernote.ui;

              // create button
              var button = ui.button({
                contents: '<i class="icon-stack-play"/> Add Slideshow',
                click: function () {
                  // invoke insertText method with 'hello' on editor module.
                  $('#modal_add_slide').modal()
                }
              });

              return button.render();   // return button as jquery object
            }
            
            
            // $('.text-editor').summernote({
                
               
            //     toolbar: [
            //         ['style', ['style']],
            //         ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //         ['fontname', ['fontname']],
            //         ['fontsize', ['fontsize']],
            //         ['color', ['color']],
            //         ['para', ['ul', 'ol', 'paragraph']],
            //         ['height', ['height']],
            //         ['table', ['table']],
            //         ['insert', ['link', 'picture', 'video']],
                    
            //         ['view', ['fullscreen', 'codeview']],
            //         ['help', ['help']],
            //         ['mybutton', ['addslide']]
            //     ],
               
            //     buttons: {
            //         addslide: addslideButton
            //       }

            // });
            $('.note-btn').attr('title', '');
            $(".styled, .multiselect-container input").uniform({ radioClass: 'choice'});
            
             $('.date-picker').daterangepicker({
                    timePicker: true,
                    opens: "left",
                    applyClass: 'bg-slate-600',
                    cancelClass: 'btn-default',
                    singleDatePicker: true,
                    autoApply : true,
                    locale: {
                        format: 'DD MMM YYYY hh:mm a'
                    }
             });
            
            
            $('.keyword-tag').tagsinput({
                    itemValue: 'text',
                    itemText: 'text',
            });

            $('.keyword-tag').on('itemAdded',function(e){
                 $(".bootstrap-tagsinput").find('input').hide();
            }).on('itemRemoved',function(e){
               if($(".keyword-tag").val() == ""){
                $(".bootstrap-tagsinput").find('input').show();
               }
            });
            
            setTimeout(function(){
               if($(".keyword-tag").val() == ""){
                   $(".bootstrap-tagsinput").find('input').show();
               }else{
                    $(".bootstrap-tagsinput").find('input').hide();
               }
                $(".bootstrap-tagsinput").find('input').attr('readonly',true);

            },200);

            var keywordinput = $('.keyword-input').text();
            $('.keyword-input').on("change paste keyup", function() {
                keywordinput = $.trim($(this).val());
                
            });
            $('.add-tag-button').click(function(e){

                e.preventDefault;
                if(keywordinput.length>0){
                    $('.keyword-input').val("");
                    $('.keyword-tag').tagsinput('add', keywordinput);
                }
                if($('.bootstrap-tagsinput').find('span.tag').length > 0 ){
                    $('.bootstrap-tagsinput').css('border-color','transparent');
                }
                
                
                
                
               
            });
    
//         $(document.body).on('click', '.bootstrap-tagsinput span.tag>span' ,function(e){
//
//                    console.log('yeah');
//                   if($('.bootstrap-tagsinput').find('span.tag').length < 1 ){
//                        $('.bootstrap-tagsinput').css('border-color','#ddd');
//                    }
//
//                });
    
            $('.keyword-tag').on('itemRemoved', function(event) {
                    if($('.bootstrap-tagsinput').find('span.tag').length < 1 ){
                        $('.bootstrap-tagsinput').css('border-color','#ddd');
                    }
            });
    
    
        //prevent exit
    
    
    
});

var alertCloseStatus = false;

function cancelPrevent()
{
    var warnMessage = null;
    alertCloseStatus = false;
    window.onbeforeunload = function () {
            if (warnMessage != null ) return warnMessage;
    }
}
function initPreventClose(editor)
{
    var warnMessage = " Warning! You are about to leave unsaved changes.";
    $('input[type!=button],submit,textarea,select').change(function () {
        alertCloseStatus = true;
        window.onbeforeunload = function () {
            if (warnMessage != null) return warnMessage;
        }
    });

    if(typeof editor != "undefined")
    {
            editor.on('change', function(){
                 if(editor.checkDirty()){
                         alertCloseStatus = true;
                        window.onbeforeunload = function () {
                            if (warnMessage != null) return warnMessage;
                        }
                 }
            });
    }



    $('input:submit').click(function(e) {
        cancelPrevent();
    });

     $('button[type=submit]').click(function(e){
        cancelPrevent();
     });

     $(document).on('click','a',function(e){
        var nextUrl = $(this).attr('href');
        var blacklist = $(this).attr('data-alpha-cms-blacklink');
        if(nextUrl != "" && typeof nextUrl != "undefined" && nextUrl != "#" && nextUrl.indexOf('javascript') === -1 && (typeof blacklist == "undefined" || blacklist == ""))
        {
            if(typeof blacklist == "undefined" && alertCloseStatus == true){
                e.preventDefault();
                var warningtwotitle = "<div class='custom-alert-icon-container pulseWarning'><svg baseProfile='tiny' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 14.2 14.5'><g fill='#F8BB86'><path d='M11.6 12H11V1.9s0-.1-.1-.1H3.8c-.2 0-.4.2-.4.4v10h-.7c-.2 0-.3.1-.3.3v.2c0 .2.1.3.3.3h8.9c.2 0 .3-.1.3-.3v-.2c0-.3-.2-.5-.3-.5zm-1.3-9.4V12L7 11.3V3.7l3.3-1.1zm-4.1.8v8.2c0 .2.1.3.3.4h.4-2.7V2.5h4.2L6.5 3c-.2.1-.3.2-.3.4z'/><path d='M7.7 8.3h.2c.2 0 .4-.2.4-.4s-.2-.4-.4-.4h-.2c-.2.1-.4.3-.4.5s.2.3.4.3z'/></g></svg></div><br>Leaving so soon?"
                swal({   
                    title: warningtwotitle,
                    text: "Unsaved changes will be lost. Leave now?",
                    showCancelButton: true,
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: "Yes, Let Me Leave",
                    cancelButtonText: "Continue Editing",
                    customClass: "warningalert",
                    html: true,
                    closeOnConfirm: true },
                    function(isConfirm){
                        if(isConfirm){
                            cancelPrevent();
                            window.location.href = nextUrl;
                        }else{
                            e.preventDefault();
                        }
                        
                });
            }
        }
     });

}

$('.table-header').removeClass('mb-10');
    $('#search_the_table').attr('placeholder','Type to filter');



$(document).on('click','.play-media',function(e){
        e.preventDefault();
        
            var sounds = document.getElementsByTagName('audio');
            for(i=0; i<sounds.length; i++) sounds[i].pause();
        
        $(this).parent().find('audio').get(0).play();
        
        $('.play-media').removeClass('hide');
        $('.pause-media').addClass('hide');
        
        $(this).addClass('hide');
        $(this).parent().find('.pause-media').removeClass('hide');
    });
    
    $(document).on('click','.pause-media',function(e){
        e.preventDefault();
        
        var sounds = document.getElementsByTagName('audio');
        for(i=0; i<sounds.length; i++) sounds[i].pause();
        
        $(this).addClass('hide');
        $(this).parent().find('.play-media').removeClass('hide');
    });

$(window).load(function(){
//            console.log('tes');
            if($('.dataTables_empty').length ==  1){
                $('.dataTables_empty').parent().height(300);
                $('.datatable-footer').hide();
            }

        $('.sale-date-trigger i').each(function(){
            $(this).css('line-height',$(this).parent().height()+'px');
        });


            
        });