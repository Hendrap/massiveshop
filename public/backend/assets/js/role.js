                        $(document).ready(function() {

                           var childClass = 'text-bold';
                           var entry_table = $('.entry-table').DataTable({
                                "order": [],
                                "searching" : true,
                                "lengthChange": false,
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "ajax": entryGetDataUrl,
                                "columns": [
                                    { "data": "name", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:childClass
                                    }, 

                                    { "data": "created", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];                                     
                                    },
                                    sClass:"td-date"

                                    },   


                                    { "data": "updated", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];                                      
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];                                 
                                    },
                                    sClass:"text-right table-actions"

                                    },           

                                    { "data": "category", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[4];                                 
                                    },
                                    sClass:"hide"

                                    }                        

                                ],
                                "columnDefs": [
                                    {
                                        "targets": 'no-sort',
                                        "orderable": false,
                                    }
                                ]
                            });
                           
                            var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                });
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                            $('.table-select2').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('.table-select2.with-search').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: 1,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13){
                                    entry_table.search($(this).val()).draw();                                    
                                }
                            });
                        });