                        var entry_table = {};
                        function toggleSorting(status){
                            var cols = entry_table.settings()[0]['aoColumns'];
                            for (var i = cols.length - 1; i >= 0; i--) {
                                cols[i].bSortable = status;
                            }
                            $("#search_the_table").prop('disabled',!status);
                            $("select").prop('disabled',!status);
                        }

                        function showEntryTableLoader(block){
                            $(block).block({
                                             message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                               overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                }
                                            });
                        }
                        $(document).ready(function() {
                             $('.set-order').click(function(e) {
                                e.preventDefault();
                                $('td.sorter, th.sorter').addClass('show-row');
                                $(this).addClass('hide');
                                $('.set-order-done').removeClass('hide');
                                var current = entry_table.page.info();
                                current = current.page;
                                entry_table.page(current + 1).order([0,'asc']).columns(0).search('').draw();
                            });

                            $('.set-order-done').click(function(e) {
                                e.preventDefault();
                                var that = $(this);
                                var items = $(".entry-data-container");
                                var data = [];
                                var obj = {};
                                var pageLength = entry_table.page.info().length;
                                var currentPage = entry_table.page.info().page + 1;

                                for (var i = 0; i < items.length; i++) {
                                    obj = {};
                                    obj.entry_id = $(items[i]).attr('data-entry-id');
                                    obj.sequence = (i + 1 + (entry_table.page.info().page * pageLength));
                                    obj.before =  $(items[i]).attr('data-before');
                                    obj.after =  $(items[i]).attr('data-after');
                                    data.push(obj);
                                }
                               
                                showEntryTableLoader($('.entry-table tbody'));
                                $.ajax({
                                    url:urlSaveSequence,
                                    data:{type:entryType,data:data,pageLength:pageLength,currentPage:currentPage},
                                    type:"POST",
                                    dataType:"json",
                                    success:function(data)
                                    {
                                        $.unblockUI();
                                        if(data.status == 1){
                                            $('td.sorter, th.sorter').removeClass('show-row');
                                            $(that).addClass('hide');
                                            $('.set-order').removeClass('hide');
                                            entry_table.page(currentPage + 1).order([1,'asc']).columns(1).search('').draw();
                                            toggleSorting(true);
                                        }
                                    }
                                });
                            });



                           var childClass = 'title-column';
                           entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                            if(typeof entry_table.ajax != 'undefined')
                                            {
                                                $(e.target).find('tbody').html('');
                                            }
                                            
                                            showEntryTableLoader(block)
                                     }else{
                                        $.unblockUI();
                                        $(".blockUI").remove();
                                        checktablestate(entry_table);
                                        if(entry_table.order().length == 1)
                                        {
                                            if(entry_table.order()[0][0] == 0)
                                            {
                                                 $('td.sorter, th.sorter').addClass('show-row');
                                                 toggleSorting(false);
                                                 
                                            }
                                        }
                                     }

                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "rowReorder": true,
                                rowReorder: {
                                    update: false
                                },
                                "order": [],
                                "pageLength": 50,
                                "searching" : true,
                                "lengthChange": false,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "ajax": {
                                    "url": entryGetDataUrl,
                                    "type": "POST"
                                },
                                "columns": [

                                     { "data": "title", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:"sorter"
                                    },


                                    { "data": "title", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    },
                                    sClass:childClass
                                    },


                                    { "data": "user", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    }

                                    },

                                    { "data": "published", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:"hide"

                                    },


                                    { "data": "created", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[4];
                                    },
                                    sClass:"td-date"

                                    },


                                    { "data": "last_modified", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[5];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "status", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[6];
                                    }

                                    },

                                     { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[7];
                                    },
                                    sClass:"text-right table-actions"

                                    },

                                    { "data": "category", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[8];
                                    },
                                    sClass:"hide"

                                    }

                                ],
                                "columnDefs": [
                                    {
                                        "targets": 'no-sort',
                                        "orderable": false,
                                    }
                                ]
                            });

                            

                            


                            entry_table.on( 'row-reorder', function ( e, diff, edit ) {
                               
                            });
                           
                            var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                });
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                            $('.table-select2').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('.table-select2.with-search').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: 1,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13){
                                    entry_table.search($(this).val()).draw();
                                }
                            });
                        });

                        $(document).ready(function(){
                            if(typeof entry_table.draw == 'function' && typeof getUrlVars()['search'] != "undefined"){
                                 entry_table.search(getUrlVars()['search']).draw();
                            }
                        });