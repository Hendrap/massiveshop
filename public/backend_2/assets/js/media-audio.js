                        var entry_table = {};
                        $(document).ready(function() {

                           var childClass = 'title-column';
                           entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                            $('.dataTable, .dataTable tbody').css('position','static');
                                            $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                        $.unblockUI();
                                        $(".blockUI").remove();
                                        $('.dataTable, .dataTable tbody').css('position','relative');
                                        checktablestate(entry_table);
                                     }

                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "order": [],
                                "searching" : true,
                                "lengthChange": false,
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "pageLength": 50,
                                "ajax": {
                                    "url": entryGetDataUrl,
                                    "type": "POST"
                                },
                                "columns": [
                                     { "data": "obj", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    }

                                    },

                                    { "data": "name", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    },
                                    sClass:"title-column"

                                    },


                                    { "data": "created", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    },
                                    sClass:"td-date"

                                    },


                                    { "data": "last_modified", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "file_size", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[4];
                                    },
                                    sClass:"td-date"

                                    },

                                     { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[5];
                                    },
                                    sClass:"text-right table-actions"

                                    },

                                    { "data": "category", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[6];
                                    },
                                    sClass:"hide"

                                    }

                                ],
                                "columnDefs": [
                                    {
                                        "targets": 'no-sort',
                                        "orderable": false,
                                    }
                                ]
                            });
                           
                            var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                });
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                            $('.table-select2').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('.table-select2.with-search').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: 1,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13){
                                    entry_table.search($(this).val()).draw();
                                }
                            });
                        });
