$(document).ready(function(){

	

	

	var homebanner = new Swiper('.banner-home-swiper', {

		speed: 600,

		autoplay: 5000,

		loop: true,

	});

	var homeproduct = new Swiper('.home-product-swiper', {

		speed: 600,

		autoplay: 4000,

		loop: true,

		pagination: '.swiper-pagination.home-product.banner',

		paginationClickable: true,

	});

	var hometesti = new Swiper('.home-testi-swiper', {

		speed: 600,

		autoplay: 4000,

		loop: true,

		paginationClickable: true,

	});


	var homeproductnew = new Swiper('.home-product-new-swiper', {

		speed: 600,

		autoplay: 4000,

		spaceBetween: 21,

		slidesPerView: 3,

		loop: false,

		pagination: '.swiper-pagination.home-product.new',

		paginationClickable: true,

		breakpoints: {

			// when window width is <= 320px

			480: {

				slidesPerView: 1,

			}

		}

	});

	var recproduct = new Swiper('.rec-product-swiper', {

		speed: 600,

		spaceBetween: 0,

		slidesPerView: 3,

		pagination: '.swiper-pagination.home-product.new',

		paginationClickable: true,

		breakpoints: {

			// when window width is <= 320px

			480: {

				slidesPerView: 1,

			}

		}

	});

	

	var philosophy = new Swiper('.philosophy-swiper', {

		speed: 600,

		spaceBetween: 25,

		slidesPerView: 1,

		pagination: '.swiper-pagination.home-product.new',

		paginationClickable: true,

		

	});

	

//    if(recproduct.slides.length < 4){

//        $('.swiper-pagination.home-product.new').hide();

//    }

	

//    $(window).resize(function(){

//  mySwiper.params.slidesPerView = 4;

//    mySwiper.reInit();

//})

	

	//add to cart

	

	// $('.btn.add-to-cart').click(function(){

	

		

	// });

	$('.add-cart-message').hover(function(){

//        console.log('hover');

		$(this).dequeue().stop();

	});

	

	//register radio

	

	

	$('.radio-reg').click(function() {
		console.log($(this).val());
		if($(this).val() == 'no') {

			$('.shipping-info-reg').show();

		}

		else if($(this).val() == 'yes') {

			$('.shipping-info-reg').hide();

		}

	});

	$('#checkbox-ship').click(function() {

		console.log('addd');

		$('.shipping-info-reg').toggle();

	});

	//payment method

	

	$('.method-select').on('change', function() {

		

		if($(this).val() == 'credit'){

			$('.method').removeClass('active');

			$('.method.credit-card').addClass('active');

			

		}

		else if($(this).val() == 'bank'){

			$('.method').removeClass('active');

			$('.method.bank-transfer').addClass('active');

		}
		
		else if($(this).val() == 'paypal'){

			$('.method').removeClass('active');

			$('.method.paypal').addClass('active');

		}

		

	});

	

	//login popup

	

	$('.login-trigger').click(function(e){

		e.preventDefault();

		$('#loginPopUp').toggleClass('active');

	});

	$('.change-currency-trigger').click(function(e){

		e.preventDefault();

		$('#listCurrency').toggleClass('active');

	});
	

	

	//mobile menu

	if($('body').innerWidth() <= 1023){

			$('nav.menu').addClass('collapse-menu');

		}

	$(window).resize(function(){

		

		if($('body').innerWidth() <= 1023){

			$('nav.menu').addClass('collapse-menu');

		}

		else {

			$('nav.menu').removeClass('collapse-menu');

		}

		

	});

	$('.mob-menu-trigger').click(function(e){

		e.preventDefault();

		$('nav.menu').toggleClass('collapse-menu');

	});

	//scrollbottom
	
	$('.scrolldown').click(function(e){

		e.preventDefault();

		var n = $('.banner-home-swiper').height();
		n = n + $('header').height();
		if($('body').height() < 1023){
			vh = $(window).height() - 141;
			console.log(vh);
		}
		else{
			vh = $(window).height() - 130;
			console.log(vh);
		}
		
//		if($('body').height() < 1023){
//			 offsetbattery =  141;
//
//		}
//		else{
//			offsetbattery =  80;
//		}
//
//		targetscroll = $('#main-container-home').offset().top - offsetbattery;
//		console.log(targetscroll);
		$('html, body').animate({ scrollTop: vh }, 500);

	});
	
	//submenu
	if($('body').width() < 1023){
			$('.submenu-trigger').click(function(e){
			e.preventDefault();
			$(this).parent().toggleClass('dropped');
			$(this).parent().find('submenu').toggleClass('dropped');
		});
	}
	$(window).resize(function(){
		
		if($('body').width() < 1023){
				$('.submenu-trigger').click(function(e){
				e.preventDefault();
				$(this).parent().toggleClass('dropped');
				$(this).parent().find('submenu').toggleClass('dropped');
			});
		}
		
	});
	
	
	$(window).scroll(function(){
		var scroll = $(window).scrollTop();
		if(scroll > 150){
			$('header').addClass('hide-top');
			$('.login-pop').addClass('popiah');
		}
		else{
			$('header').removeClass('hide-top');
			$('.login-pop').removeClass('popiah');
		}
	});
	
	//account popup location
	var contwidth;
	var login_pop_position;
	var bodywidth;
	
	function popiah(){
		bodywidth = $('body').width();
		contwidth = $('.main-container').outerWidth();
		login_pop_position = bodywidth - contwidth;
		login_pop_position = login_pop_position / 2;
		$('.login-pop').css('right',login_pop_position);
	};
	
	popiah();
	
	$(window).resize(function(){
		popiah();
		
	});
	
	
});

