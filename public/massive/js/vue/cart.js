// window.Vue = require('vue');
// Vue.use(require('vue-resource'));

Vue.component("cart-item",{
  template : "#cart-item-template",
  props: ["item"],
  methods: {

    // methods start
    formatPrice : function(value) {
        let val = (value/1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")

    },
    removeItemFromCart : function(itemIdToDelete){
      
      var index = this.$parent.cartData.indexOf(itemIdToDelete);
      this.$parent.cartData.splice(index, 1);
      
      this.$http.post(CartRemoveItemUrl,{item_id:itemIdToDelete}).then((response) => {
          console.log("deleted ID : "+itemIdToDelete);
      });

    }
    // end of methods
  }
});

var shopping = new Vue({
    el: "#shopping-cart",
    data: {
      cartData: [],
    },
    computed: {
      sumTotal : function(){
          total = this.cartData.reduce((acc, item) => acc + (item.qty * item.original_price), 0)
          return this.formatPrice(total)

      },
      itemCount : function(){
          return this.cartData.length
      }
    },
    methods: {
      // methods start
      formatPrice : function(value) {
        let val = (value/1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")

      },
      getItemCart : function(){
        this.$http.get(getDataCartUrl).then( (response) => {
            this.cartData = response.body;
        });
      },
      addItemToCart : function(item){
        
        // response = this.saveCartIntoDb(item);
        console.log(item);
        this.$http.post(CartPostUrl, item).then( (response) => {
            console.log(response.body);
            var data = response.body;

            if(data.status == 1){
              //add to vue cart view
              var index = this.getCartIndex(this.cartData, item.item_id);
              if(index == null){ //add new data
                this.cartData.push({ 
                  item_id: item.item_id, 
                  session_id: "",
                  product_name : item.product_name, 
                  qty : 1, 
                  original_price : item.original_price, 
                })  
              }
              else{ // product exist in cart
                this.cartData[index].qty += parseInt(item.qty);
              }

            }

        });
        
        
        $("#shopping-cart .megamenu").css('display','block').animate({ "transition" : "all 0.3s ease" });

      },

      getCartIndex : function(arr, item_id){
        for (var i = 0; i < arr.length; i++) {
          if (arr[i].item_id == item_id) {
            return i;
          }
        }
        return null;

      },

    }
});

