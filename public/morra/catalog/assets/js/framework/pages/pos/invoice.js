    Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
        c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "," : t, 
        s = n < 0 ? "-" : "", 
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
     };

    function reloadPopOver()
    {
        var items = $('[data-popup=popover]');
        $.each(items,function(i,k){
            if($(k).attr('data-img-src') != "" && $(k).attr('data-img-src') != null & $(k).attr('data-img-src') != undefined && $(k).attr('data-img-src') != assetBaseURL)
            {
                $(k).popover();
            }
        });
        
    }
    function unReactiveData(data){
        return JSON.parse(JSON.stringify(data));
    }
    
    function showLoading(el)
    {
        $(el).block({
            message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: '10px 15px',
                color: '#333',
                width: 'auto',
                "margin-top": '120px',
            }
        });
    }
    
    function hideLoading()
    {
        $(".blockUI").remove()
    }

    function errorMsg(msg)
    {   
        if(typeof msg == 'undefined') msg = "";
         swal({
            title: "ERRROR",
            text: msg,
            confirmButtonColor: "#EF5350",
            type: "error"
         });
    }

    function successMsg(msg)
    {
        if(typeof msg == 'undefined') msg = "All changes has been saved successfuly";
        swal({
            title: "SUCCESS",
            text: msg,
            confirmButtonColor: "#66BB6A",
            type: "success"
        });        
    }


    var objAddress = {
        email:"",
        districtCity:"",
        first_name:"",
        last_name:"",    
        city:"",
        district:"",
        province:"",
        address:"",
        phone:"",
        postal_code:"",
        type:"",
        id:0,
        country:"Indonesia",
        user_id:0
    };

    var defaultCustomer = {
        email:"",
        first_name:"",
        last_name:"",
        id:0,
        day:1,
        month:"January",
        year:1990,
        gender:"Male",  
        phone:""
    }

    data.modalCustomer = unReactiveData(defaultCustomer);
    data.activeBilling = unReactiveData(objAddress);
    data.activeShipping = unReactiveData(objAddress);
    data.modalAddress = unReactiveData(objAddress);
    data.selectedCustomer = unReactiveData(defaultCustomer);
   
    var vuePOS = new Vue({
        el:"#POS",
        data:data,
        created:function(){
            $("#pageLabel").html('<i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Outgoing</span> - New Order');
        },
        ready:function(){
            var vm = this;
             $(document).ready(function() {
                $(document).on('change','.subTotal',function(e){
                    var currentProduct = unReactiveData(vm.products[parseInt($(this).attr('data-index'))]);
                    vm.products[parseInt($(this).attr('data-index'))].discount = (-1 * parseInt(e.target.value - (currentProduct.stock_qty * currentProduct.original_price)));
                });


                $("#store").change(function(e){
                    for (var i = vm.stores.length - 1; i >= 0; i--) {
                        if(parseInt(vm.stores[i].id) == parseInt($("#store").val())){
                            vm.sources = vm.stores[i].sources;
                            vm.$nextTick(function(){
                                $("#source").val("").change();
                            })
                        }
                    }
                });

                 $("#source").change(function(e){
                    for (var i = vm.sources.length - 1; i >= 0; i--) {
                        if(parseInt(vm.sources[i].id) == parseInt($("#source").val())){
                            vm.paymentMethods = vm.sources[i].payments;
                            vm.$nextTick(function(){
                                $("#paymentMethod").val("").change();
                            })
                        }
                    }
                });



                $("#disocunt_amount").keypress(function(e){
                    if(e.keyCode == 37)
                    {
                        var percent = parseFloat(e.target.value);
                        if(percent < 100)
                        {
                            vm.total_discount = parseFloat(parseFloat(percent/100).toFixed(2)) * (vm.subTotal + vm.shipping_amount);
                        }
                    }
                });


                $('#order_date').daterangepicker({
                    timePicker: true,
                    opens: "left",
                    applyClass: 'bg-slate-600',
                    cancelClass: 'btn-default',
                    singleDatePicker: true,
                    autoApply : true,
                    locale: {
                        format: 'DD MMM YYYY hh:mm a'
                    }

                 });
                $("#paymentMethod").change(function(e){
                    vm.paymentMethod = $("#paymentMethod").val();
                });
                $("#dropDownShippingMethod").change(function(e){
                    vm.carrier_id = $("#dropDownShippingMethod").val();
                });
                $("#autocompleteDistrictCity").autocomplete({
                     source: urlGetCityProvince,
                     minLength: 1
                });

                $("#add-customer").focusin(function(e){
                    // if($(".customer-search-result-list").find('li').length > 1)
                    // {
                        if($("#add-customer").val().trim() != ""){
                            $('.customer-search-result-list').addClass('show'); 
                        }                   
                    //}
                });
                $("#add-customer").keyup(function(e){
                    if($("#add-customer").val().trim() != ""){
                            $('.customer-search-result-list').addClass('show'); 
                        }   
                    // if($("#add-customer").val().trim() != ""){
                    //         $('.customer-search-result-list').addClass('show'); 
                    // }       
                });

                $("#add-customer, #add-product").click(function(e){
                    e.stopPropagation();
                });
                        
                $("#add-product").focusin(function(e){
                    e.stopPropagation();
                    $('.product-search-result-list').addClass('show');
                });

                $(document).click(function(){
                   $('.product-search-result-list').removeClass('show');
                });

                $(document).click(function(){
                   $('.customer-search-result-list').removeClass('show');
                });
                                    
                $('.date-picker').daterangepicker({
                    timePicker: true,
                    opens: "left",
                    applyClass: 'bg-slate-600',
                    cancelClass: 'btn-default',
                    singleDatePicker: true,
                    autoApply : true,
                    locale: {
                        format: 'MM/DD/YYYY h:mm a'
                    }
                });
                        
                // $('select').select2({
                //      minimumResultsForSearch: Infinity,
                //      width: '100%'
                // });


                        
                $('.address-select').each(function(){
                    var thisplaceholder = $(this).attr('placeholder');
                     $(this).select2({
                            minimumResultsForSearch: Infinity,
                            width: '100%',
                            placeholder: thisplaceholder,
                        });
                });
                        
                $('.select-status').select2({
                    minimumResultsForSearch: Infinity,
                    width: '100px',
                    placeholder: 'Select Status',
                });
                        
                function formataddressdropdown (address) {
                  if (!address.id) { return address.text; }
                  var $address = $(
                    '<div class="address-dropdown-option"><h5 class="address-dropdown-label">' + address.element.value + '</h5><h6 class="text-muted">' + address.element.getAttribute('data-detail') + '</h6></div>'
                  );
                  return $address;
                };

                $('.address-dropdown,.select-date').each(function(){
                    
                    var thisplaceholder = $(this).attr('placeholder');
                    $(this).select2({
                        minimumResultsForSearch: Infinity,
                        templateResult: formataddressdropdown,
                        placeholder : thisplaceholder,
                    });
                    
                });
                        
                $(".checkbox-gift-wrap").change(function() {
                    if(this.checked) {
                        $('.gift-wrap-textarea').prop('disabled',false);
                    }
                    else{
                         $('.gift-wrap-textarea').prop('disabled',true);
                    }
                });

                $(".select-date").change(function(e){
                   vm.modalCustomer[$(this).data('model')] = e.target.value;
                });

                $('#dropDownBilling').append('<option data-action="create" data-type="billing" class="new-address-option" value="Create New" data-detail="&nbsp;"></option> ');
                $('#dropDownShipping').append('<option data-action="create" data-type="shipping" class="new-address-option" value="Create New" data-detail="&nbsp;"></option> ');

                
                $('.address-dropdown').change(function(){                    
                    var thisdropdown = $(this);
                    var selectedItem = $(thisdropdown).find('option:selected');
                    if($('.new-address-option').is(':selected')){

                        vm.showModalAddress($(selectedItem).attr('data-action'),$(selectedItem).attr('data-type')); 

                        if($(selectedItem).attr('data-type') == 'billing'){
                            vm.activeBilling = unReactiveData(objAddress);
                        }

                        if($(selectedItem).attr('data-type') == 'shipping'){
                            vm.activeShipping = unReactiveData(objAddress);
                        }
                        thisdropdown.select2("val", "");
                    }

                    if($(selectedItem).attr('data-index') != "" && $(selectedItem).attr('data-index') != undefined)
                    {
                        var col = 'activeShipping';
                        var colList = 'shippingAddressList';
                        if($(selectedItem).attr('data-type') == 'billing'){
                            col = 'activeBilling';
                            colList = 'billingAddressList';
                        }
                        vm[col] = unReactiveData(vm[colList][parseInt($(selectedItem).attr('data-index'))]);
                    }
                    
                });

                vm.reloadOrder(dataOrder);
               
            });
        },
        watch:{
            'customerList':function(val,oldVal)
            {
                if(this.customerList.length == 0)
                {
                    this.showCreateNewButton = true;
                }else{
                    this.showCreateNewButton = false;
                }
            },
            'isDropShip':function(val,oldVal){
                if(this.selectedCustomer.id == 0)
                {
                    this.dropshipper_name = ''
                    this.dropshipper_phone = '';
                }
                if(val == true){
                    this.dropshipper_name = this.selectedCustomer.name;
                    this.dropshipper_phone = this.selectedCustomer.phone;
                }else{
                    this.dropshipper_name = ''
                    this.dropshipper_phone = '';
                }
            },
            'carrier_id':function(val,oldVal){
                this.calculateShippingAmount();
            },
            'products':function(val,oldVal){
               
                if(this.products.length > 0){

                    var ids = this.parseProductToDHLFormat();
                    this.getDHLQuote(
                        ids,
                        this.activeShipping.postal_code,
                        this.activeShipping.city,
                        this.activeShipping.country,
                        function(){
                            vuePOS.calculateShippingAmount()
                        }
                    );

                    this.showProductsEmptyState = false;
                    this.showSearchInputProducts = true;
                }else{
                    this.shippingMethods = [];
                    this.showProductsEmptyState = true;
                    this.showSearchInputProducts = false;
                }

               
            },
            'activeBilling':function(val,oldVal){
                this.billing_id = val.id;
            },
            'activeShipping':function(val,oldVal){

                this.shipping_id = val.id;
                if(parseInt(this.shipping_id) == 0){
                    $("#dropDownShippingMethod").prop('disabled',true);
                    $("#select2-dropDownShippingMethod-container").find('.select2-selection__placeholder').html("Shipping address required");
                }else{
                    $("#dropDownShippingMethod").prop('disabled',false);
                    $("#select2-dropDownShippingMethod-container").find('.select2-selection__placeholder').html("Select Shipping Method");
                }


                var ids = this.parseProductToDHLFormat();
                this.getDHLQuote(
                        ids,
                        this.activeShipping.postal_code,
                        this.activeShipping.city,
                        this.activeShipping.country,
                        function(){
                            vuePOS.calculateShippingAmount()
                        }
                    );
            },
            'inputSearchProduct':function(val,oldVal){
                if(val != oldVal){
                    if(val.trim() != "")
                    {
                        this.getProducts();                        
                    }
                }
            },
            'searchCustomer':function(val,oldVal){
              if(val != oldVal){
                this.getCustomerList();
              }
            },
            'selectedCustomer':function(val,oldVal){
                if(parseInt(val.id) == 0){
                     this.showCustomerInfo = false;
                     $('.customer-search-result-list').removeClass('hide');
                     $("#dropDownShipping").prop('disabled',true)
                     $("#dropDownBilling").prop('disabled',true)
                     $("#select2-dropDownShipping-container").find('.select2-selection__placeholder').html("Customer data required");
                     $("#select2-dropDownBilling-container").find('.select2-selection__placeholder').html("Customer data required");
                }else{
                     $("#dropDownShipping").prop('disabled',false)
                     $("#dropDownBilling").prop('disabled',false)
                     $("#select2-dropDownShipping-container").find('.select2-selection__placeholder').html("Select Shipping Method");
                     $("#select2-dropDownBilling-container").find('.select2-selection__placeholder').html("Select Billing Method");
                     this.showCustomerInfo = true;
                     $('.customer-search-result-list').addClass('hide');
                }
            }
        },
        computed:{
            grandTotal()
            {
                var shipping = parseFloat(this.shipping_amount);
                if(isNaN(shipping)) shipping = 0;
                
                var subtotal = parseFloat(this.subTotal);
                if(isNaN(subtotal)) subtotal = 0;

                var discount = parseFloat(this.total_discount);
                if(isNaN(discount)) discount = 0;

                var total =  parseFloat((shipping + subtotal) - discount);
                if(total <= 0) return 0;
                return total;
            },
            subTotal()
            {
                var subTotalProduct = 0;
                for (var i = this.products.length - 1; i >= 0; i--) {
                    subTotalProduct += parseFloat((this.products[i].stock_qty * this.products[i].original_price) - this.products[i].discount);
                }
                return subTotalProduct;
            },
            totalWeight(){
                var total = 0;
                for (var i = this.products.length - 1; i >= 0; i--) {
                    total += parseFloat(this.products[i].weight) * this.products[i].stock_qty;
                }

                return total;
            },
        },
        methods:{
            parseProductToDHLFormat()
            {
                var ids = new Array();
                for (var i = this.products.length - 1; i >= 0; i--) {
                        ids.push({id:this.products[i].item_id,qty:this.products[i].stock_qty});
                }

                return ids;
            },
            parseLabelForAddress(address)
            {
                var addr = unReactiveData(address);
                var label = this.preventNullValue(addr.first_name) + ' ' + this.preventNullValue(addr.last_name);
                if(label.trim() == "")
                {
                    label = this.preventNullValue(addr.label);
                }else{
                   label += this.addTextPrefix(' - ',addr.label);
                }   
                
                if(label.trim() == "") return addr.address;

                return label;                

            },
            addTextPrefix(prefix,text)
            {
                var txt = this.preventNullValue(text);
                if(txt == "") return "";
                return prefix + text;
            },
            preventNullValue(val){
                if(val == null) return "";

                return val;
            },
            deleteProduct(index)
            {
                var that = this;
                swal({
                    title: "Delete ?",
                    text: "You will not be able to recover deleted items, are you sure want to delete ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: true },
                    function(isConfirm){
                        if(isConfirm){
                            that.products.splice(index,1);
                        }
               });
            },
            parseMoney(x){
                var tmp = parseInt(x);
                if(isNaN(tmp)) return 0;

                return tmp;
            },
            reloadOrder(orderData){
                this.isAllowReqToDHL = false;

                if(parseInt(orderData.id) == 0){
                    $("#dropDownBilling").prop('disabled',true);
                    $("#select2-dropDownBilling-container").find('.select2-selection__placeholder').html("Customer data required");
                    $("#dropDownShipping").prop('disabled',true);
                    $("#select2-dropDownShipping-container").find('.select2-selection__placeholder').html("Customer data required");
                    $("#dropDownShippingMethod").prop('disabled',true);
                    $("#select2-dropDownShippingMethod-container").find('.select2-selection__placeholder').html("Shipping address required");
                    $("#store").change();
                    $("#source").change();
                    setTimeout(function(){
                        initPreventClose();
                        vuePOS.isAllowReqToDHL = true;
                    },2000);
                    return false;
                    
                }


                var title = unReactiveData(pageTitle);
                document.title = pageTitle.replace('[[title]]','Order #' + orderData.order.order_number);
                $("#pageLabel").html('<i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Outgoing</span> - Order #' + orderData.order.order_number);
                this.order_number = orderData.order.order_number;
                this.orderId = orderData.order.id;
                this.selectedCustomer = orderData.user;
                this.billingAddressList = orderData.user.billing_address;
                this.shippingAddressList = orderData.user.shipping_address;
                this.activeShipping = orderData.shipping;
                this.activeBilling = orderData.billing;
                this.billing_id = orderData.billing.id;
                this.shipping_id = orderData.shipping.id;
                this.products = orderData.products;
                this.refnumber = orderData.order.refnumber;
                this.total_discount = orderData.order.global_discount;
                this.carrier_id = orderData.order.carrier_id;
                this.customer_note = orderData.order.customer_note;
                this.dropshipper_name  = unReactiveData(orderData.order.dropshipper_name);
                this.dropshipper_phone = unReactiveData(orderData.order.dropshipper_phone);
                this.gift_wrapping = orderData.order.gift_wrapping;
                this.paymentMethod = orderData.order.payment_method;

                this.$nextTick(function(){
                    $("#dropDownShippingMethod").val(orderData.order.carrier_id).change();
                    $("#dropDownBilling").val(orderData.billing.first_name + ' - ' + orderData.billing.label).change();
                    $("#dropDownShipping").val(orderData.shipping.first_name + ' - ' + orderData.shipping.label).change();
                    $("#store").val(orderData.order.store).change();
                    setTimeout(function(){
                        $("#source").val(orderData.order.source).change();
                        setTimeout(function(){
                            vuePOS.shipping_amount = parseInt(orderData.order.shipping_amount);
                            if(orderData.order.dropshipper_name != null)
                            {
                                if(orderData.order.dropshipper_name.trim() != ''){
                                    vuePOS.isDropShip = true;
                                    $("#isDropShip").prop('checked',true).uniform('update');
                                    setTimeout(function(){
                                        vuePOS.dropshipper_name = unReactiveData(orderData.order.dropshipper_name);
                                        vuePOS.dropshipper_phone = unReactiveData(orderData.order.dropshipper_phone);
                                    },100);

                                }                            
                            }

                            if(orderData.order.gift_wrapping != null)
                            {
                                if(orderData.order.gift_wrapping.trim() != ''){
                                    $("#isGiftWrap").prop('checked',true).change().uniform('update');
                                     vuePOS.isGiftWrap = true;
                                }
                            }

                            $("#order_date").val(orderData.order.order_date).change();
                            $("#paymentMethod").val(orderData.order.payment_method).change();
                            reloadPopOver();                            
                        },100);
                    },500);
                    setTimeout(function(){
                        initPreventClose();
                        vuePOS.isAllowReqToDHL = true;
                    },2000);
                });

            },
            getDHLQuote(idItems,postalCode,city,country,callback)
            {
                if(this.isAllowReqToDHL == false) return false;
                showLoading($("#POS"));
                this.$http.post(urlGetDHLQuote,{ids:idItems,postal_code:postalCode,city:city,country:country},function(data,s,r){
                    hideLoading();
                    if(data.status == 1)
                    {
                        this.shippingMethods = data.shippingMethods;
                        this.$nextTick(function(){

                            $("#dropDownShippingMethod").trigger('change');
                            if(typeof callback == 'function')
                            {
                                callback();
                            }

                        });

                    }
                });
            },
            calculateShippingAmount()
            {

                this.showShippingAmount = false;

                if(this.carrier_id == null || this.carrier_id == ""){
                   
                    this.shipping_amount = 0;

                    return false;
                }

                for (var i = this.shippingMethods.length - 1; i >= 0; i--) {
                    if(this.shippingMethods[i].id == this.carrier_id)
                    {
                        this.shipping_amount = this.shippingMethods[i].value;
                        if(parseInt(this.shipping_amount) > 0)
                        {
                            this.showShippingAmount = true;
                        }
                    }
                }

                // amount = unReactiveData(parseFloat(this.totalWeight * this[this.carrier_id]));
                // this.shipping_amount = parseFloat(unReactiveData(amount)).toFixed(0);
                // if(parseInt(amount) == 0){
                //     this.showShippingAmount = true;
                // }else{
                //     this.showShippingAmount = false;
                // }

                
            },
            selectProduct(index)
            {
                var item = unReactiveData(this.searchProducts[index]);
                this.products.push(item);
                this.searchProducts.splice(index,1);
                this.inputSearchProduct = "";
                this.$nextTick(function(){
                    reloadPopOver();
                });
            },
            initSearchProducts()
            {
                this.showSearchInputProducts = true;
                this.showProductsEmptyState = false;                
            },
           
            showModalAddress(action,type)
            {
                this.errors = [];
                this.showSuccesMessage = false;

                if(this.selectedCustomer.id == 0){
                    errorMsg("Please select user first!");
                    return false;
                }

                var col = 'billing_id';
                var colList = 'billingAddressList';
                if(type == 'shipping'){
                    col = 'shipping_id';
                    colList = 'shippingAddressList';
                }
                
                if(action == 'create'){
                    this.modalAddress = unReactiveData(objAddress);
                    this.modalAddress.type = type;
                }else{
                    var id = this[col];
                    for (var i = this[colList].length - 1; i >= 0; i--) {
                        if(this[colList][i].id == id){
                            this.modalAddress = unReactiveData(this[colList][i]);
                            this.modalAddress.type = type;
                        }
                    }
                }

                $("#modalAddress").modal('show');
            },
            saveAddress()
            {   
                if(this.selectedCustomer.id == 0) return false;

                this.errors = [];
                this.showSuccesMessage = false;
                this.modalAddress.user_id = this.selectedCustomer.id;
                var vm = this;
                var oldId = unReactiveData(this.modalAddress.id);

                this.$http.post(urlSaveAddress,this.modalAddress,function(data,s,r){
                    if(data.status == 1){
                        this.showSuccesMessage = true;
                        var col = 'activeShipping';
                        var colList = 'shippingAddressList';
                        if(data.address.type == 'billing'){
                            col = 'activeBilling';
                            colList = 'billingAddressList';
                        }

                        if(this[col].id == data.address.id){
                            this[col] = unReactiveData(data.address);                          
                        }

                        if(oldId == 0)
                        {
                            this[colList].push(data.address);
                            if(colList == 'shippingAddressList'){
                                this.billingAddressList.push(data.billingAddress);
                            }
                        }

                        for (var i = this[colList].length - 1; i >= 0; i--) {
                            var item = this[colList][i];
                            if(item.id == data.address.id)
                            {
                                this[colList].$set(i,unReactiveData(data.address));
                            }
                        }
                    }
                    successMsg();
                    $("#modalAddress").modal('hide');
                }).error(function(data,s,r){
                    $.each(data,function(i,k){
                        for (var i = k.length - 1; i >= 0; i--) {
                            vm.errors.push(k[i]);
                        }
                    })
                });
            },
            editCustomer()
            {
                this.modalCustomer = unReactiveData(this.selectedCustomer);
                $("#modalAccount").modal('show');
            },
            openCustomerModal()
            {
                this.modalCustomer = unReactiveData(defaultCustomer);
                $("#modalAccount").modal('show');
            },
            changeCustomer()
            {
                this.selectedCustomer = unReactiveData(defaultCustomer);
                this.billingAddressList = [];
                this.shippingAddressList = [];
                this.activeShipping = unReactiveData(objAddress);
                this.activeBilling = unReactiveData(objAddress);
                this.customerList = [];
                this.searchCustomer = "";
                this.$nextTick(function(){
                    $("#dropDownBilling").trigger('change');
                    $("#dropDownShipping").trigger('change');
                    $('.select-date').trigger('change');

                });
            },
            selectCustomer(index)
            {
                this.selectedCustomer = unReactiveData(this.customerList[index]);
                this.shippingAddressList = unReactiveData(this.selectedCustomer.shipping_address);
                this.billingAddressList = unReactiveData(this.selectedCustomer.billing_address);
                this.$nextTick(function(){
                    $("#dropDownBilling").trigger('change');
                    $("#dropDownShipping").trigger('change');
                    $('.select-date').trigger('change');
                });
            },
            getProducts()
            {
              $("#add-product").prop('disabled',true);
               this.$http.post(urlSearchProducts,{q:$("#add-product").val()},function(data,s,r){
                    $('.product-search-result-list').addClass('show');
                    this.searchProducts = data.items;   
                    this.$nextTick(function(){
                      $("#add-product").prop('disabled',false);
                      $("#add-product").focus();
                    });
               });
            },

            getCustomerList(){
               $("#add-customer").prop('disabled',true);
               this.selectedCustomer = unReactiveData(defaultCustomer);
               this.customerList = [];
               this.$http.post(urlGetCustomer,{q:$("#add-customer").val()},function(data,s,r){
                    $('.customer-search-result-list').addClass('show');
                    this.customerList = data;   
                    this.$nextTick(function(){
                      $("#add-customer").prop('disabled',false);
                      $("#add-customer").focus();
                    });
               });
            },
            saveUser(e){
                e.preventDefault();
                var vm = this;
                this.errors = [];
                $("[name=first_name]").parent().removeClass('has-error');
                showLoading($("#modalAccount").find('.modal-body'));
                this.$http.post(urlSaveUser,this.modalCustomer,function(data,s,r){
                    hideLoading();
                    if(data.status == 1)
                    {
                        successMsg();
                        this.selectedCustomer = data.user;
                    }else{
                        this.errors.push(data.msg);
                    }

                }).error(function(data,s,r){
                    hideLoading();
                    $.each(data,function(i,k){
                        $("[name=first_name]").parent().addClass('has-error');
                        for (var x = k.length - 1; x >= 0; x--) {
                            vm.errors.push(k[x]);
                        }
                    })
                });
            },
            validateBeforeSave()
            {
                if(this.selectedCustomer.id == 0){
                    errorMsg("Please select customer first!");
                    return false;
                }
                if(this.activeBilling.address.trim() == "")
                {
                    errorMsg("Please billing address first!");
                    return false;
                }
                if(this.activeShipping.address.trim() == "")
                {
                    errorMsg("Please shipping address first!");
                    return false;
                }
                
                if(this.products.length == 0){
                    errorMsg("Please select product first!");
                    return false;
                }

                for (var i = this.products.length - 1; i >= 0; i--) {
                    var tmpItem = this.products[i];
                    if(tmpItem.stock_qty > tmpItem.stock_available){
                        errorMsg("Out of range!" + "Max qty for " + tmpItem.product_title + " is " + tmpItem.stock_available);
                        return false;
                    }
                }

                return true;

            },
            getOrderFields()
            {
                var fields = ['gift_wrapping','dropshipper_name','dropshipper_phone','customer_note','total_discount','refnumber','billing_id','shipping_id','carrier_id','shipping_amount'];
                return fields;
            },
            placeOrder(){
                var status = this.validateBeforeSave();
                if(status == false) return false;

                showLoading($("#POS"));
                var fields = this.getOrderFields();
                var dataObj = {};
                dataObj.order = {};
                dataObj.order.id = this.orderId;
                dataObj.order.source = $("#source").val();
                dataObj.order.store = $("#store").val();
                dataObj.order.order_date = $("#order_date").val();
                dataObj.order.paymentMethod = this.paymentMethod;
                for (var i = fields.length - 1; i >= 0; i--) {
                    dataObj.order[fields[i]] = this[fields[i]];
                }
                if(this.isGiftWrap == false) dataObj.order.gift_wrapping = "";

                dataObj.products = this.products;
                dataObj.user = this.selectedCustomer;
                dataObj.shippingAddress = this.activeShipping;
                dataObj.billingAddress = this.activeBilling;
                this.$http.post(urlSaveOrder,dataObj,function(data,s,r){
                    hideLoading();
                    if(data.status == 1){
                        this.reloadOrder(data);
                    }
                    successMsg();

                }).error(function(data,s,r){
                    alert("500 Internal Server Error!");
                });
            }
        }
    });
