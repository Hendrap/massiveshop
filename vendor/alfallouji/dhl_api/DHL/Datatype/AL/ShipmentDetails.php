<?php
/**
 * Note : Code is released under the GNU LGPL
 *
 * Please do not change the header of this file
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU
 * Lesser General Public License as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 */

/**
 * File:        ShipmentDetails.php
 * Project:     DHL API
 *
 * @author      Al-Fallouji Bashar
 * @version     0.1
 */

namespace DHL\Datatype\AL; 
use DHL\Datatype\Base;

/**
 * ShipmentDetails Request model for DHL API
 */
class ShipmentDetails extends Base
{
    /**
     * Is this object a subobject
     * @var boolean
     */
    protected $_isSubobject = true;

    /**
     * Parameters of the datatype
     * @var array
     */
    protected $_params = array(
        'AccountType' => array(
            'type' => 'string',
            'required' => false,
            'subobject' => false,
        ), 
        'AccountNumber' => array(
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            // 'multivalues' => true,
        ), 
        'BillToAccountNumber' => array(
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            // 'comment' => 'Weight of piece or shipment',
            // 'fractionDigits' => '3',
            // 'minInclusive' => '0.000',
            // 'maxInclusive' => '999999.999',
            // 'totalDigits' => '10',
            'maxLength' => '9',
        ), 
        'AWBNumber' => array(
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            // 'comment' => 'Unit of weight measurement (K:KiloGram)',
            // 'minLength' => '0',
            'maxLength' => '10',
            // 'enumeration' => 'K,L',
        ), 
        'NumberOfPieces' => array(
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            // 'comment' => '',
            // 'pattern' => '([A-Z0-9])*',
            // 'minLength' => '1',
            // 'maxLength' => '4',
        ), 
        'Weight' => array(
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'comment' => '',
            // 'minLength' => '1',
            // 'maxLength' => '4',
            // 'pattern' => '([A-Z0-9])*',
        ), 
        'WeightUnit' => array(
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            // 'comment' => 'Date only',
            // 'pattern' => '[0-9][0-9][0-9][0-9](-)[0-9][0-9](-)[0-9][0-9]',
        ), 
        'GlobalProductCode' => array(
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'pattern' => '([A-Z0-9])*',
            'minLength' => '1',
            'maxLength' => '4',
        ), 
        'DoorTo' => array(
            'type' => 'DoorTo',
            'required' => false,
            'subobject' => false,
            'comment' => 'Defines the type of delivery service that applies
				to the shipment',
            'length' => '2',
            'enumeration' => 'DD,DA,AA,DC',
        ), 
        'DimensionUnit' => array(
            'type' => 'DimensionUnit',
            'required' => false,
            'subobject' => false,
            'comment' => 'Dimension Unit C (centimeter)',
            'length' => '1',
            'enumeration' => 'C,I',
        ), 
        'InsuredAmount' => array(
            'type' => 'Money',
            'required' => false,
            'subobject' => false,
            'comment' => 'Monetary amount (with 2 decimal precision)',
            'minInclusive' => '0.00',
            'maxInclusive' => '9999999999.99',
        ), 
        'InsuredCurrencyCode' => array(
            'type' => 'PackageType',
            'required' => false,
            'subobject' => false,
            'length' => '3',
            // 'enumeration' => 'BD,BP,CP,DC,DF,DM,ED,EE,FR,JB,JD,JJ,JP,OD,PA,YP',
        ), 
        'Pieces' => array(
            'type' => 'PiecesP',
            'required' => false,
            'subobject' => true,
            // 'multivalues' => true,
            // 'comment' => 'Boolean flag',
            // 'length' => '1',
            // 'enumeration' => 'Y,N',
        ), 
        'CurrencyCode' => array(
            'type' => 'CurrencyCode',
            'required' => false,
            'subobject' => false,
            'comment' => 'ISO currency code',
            'length' => '3',
        ), 
        'SpecialService' => array(
            'type' => 'AdditionalProtection',
            'required' => false,
            'subobject' => false,
            'multivalues' => true,
        ),
    );
}
