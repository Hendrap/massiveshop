# Alpha Catalog#


### Konfigurasi Composer ###

* Tambah repo private catalog ke composer,lalu jalankan -- composer require "morra/catalog":"dev-master" --
* Publish asset "php artisan vendor:publish --tag=morra-catalog --force"
```
#!json

"repositories": [
	{
		"type": "vcs",
		"url": "https://<_username_git_kamu_>@bitbucket.org/Morra/catalog-package.git"
	}
],
"require-dev": {

	"morra/catalog": "dev-master"

}

```

### Fitur ###

* Product
* Variant Item
* Group Item
* Wishlist
* Cart
* Catalog Search
* Shipping Method
* Payment Method
* Checkout
* Orders
* Promo
* ...

### Pemasangan ###

* Cukup buka admin panel 
* Buka /setting/plugins
* Aktifkan Plugin

### Konfigurasi ###

* Buka terminal/console lalu jalankan perintah ini
```
#!php

php artisan catalog.init
```

## Konfigurasi Alpha CMS ##

* Tambahkan relasi ke alpha/Models/Entry.php
```
#!php

public function items(){
		return $this->hasMany('Morra\Catalog\Models\Item','product_id');
	}
```

* Tambahkan relasi ke alpha/Models/Taxonomy.php
```
#!php

public function items(){
		return $this->BelongsToMany('Morra\Catalog\Models\Item');
	}
```

### GIT ###

* Jika code package catalog kamu edit & kamu pengen dapet update dari repo utama ,ini bisa jadi pilihan
* Catalog pakai submodule
* Cukup pindah ke directory vendor/morra/catalog
* Jalankan git status,commit dsb semua jalan kamu bisa rebase/merge dll. karna repo ini pisah dari repo utama dan gak bakal crash

### Pembaharuan ###

* Jalankan "composer update morra/catalog",ini kalau config yg tadi tidak diganti dia langsung ambil dari repo utama
* Kamu bisa pakai branch yg berbeda,atau repo yg berbeda
* Alamat repo bisa diganti dari composer.json(Lihat step pertama)
* Untuk branch -> "dev-master","master" itu branch-nya. Misal ada branch "test" tinggal ganti aja jadi "dev-test"

### Konfigurasi Entry Type ###

Jadi untuk catalog type itu bergantung sama entry type,ada standard penamaan untuk itu. 
Untuk membuat catalog type yg baru ,pertama harus buat entry type dulu dengan slug seperti ini "catalog_variant_product". 
Untuk "product" bisa diganti sesuai keinginan anda misal jadi "catalog_variant_good" atau "catalog_variant_data" dsb.
Setiap Product harus memiliki Item type,setiap product hanya boleh punya 1 Item type.
Untuk membuat Item type tinggal buat entry type baru.

Penamaan item type harus mengikuti standard berikut contohnya:

1. "item_catalog_variant_product" => ini digunakan untuk item variant biasa
2. "itemgroup_catalog_variant_good" => ini digunakan untuk item group/package

Untuk standard penamaan slug(entry type untuk item) sebagai berikut

1. item/itemgroup
2. Nama Slug dari Product

Jadi misal kamu punya Product dengan slug "catalog_variant_barang".
Maka penamaan untuk item type "item_catalog_variant_barang"  atau "itemgroup_catalog_variant_barang"

Untuk membuat Catalog Type baru harus ada entry type untuk product dan entry type untuk item-nya.