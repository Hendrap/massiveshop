    function unReactiveData(data){
        return JSON.parse(JSON.stringify(data));
    }
    
    function showLoading(el)
    {
        $(el).block({
            message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: '10px 15px',
                color: '#333',
                width: 'auto',
            }
        });
    }
    
    function hideLoading()
    {
        $(".blockUI").remove()
    }

    function errorMsg(msg)
    {   
        if(typeof msg == 'undefined') msg = "";
         swal({
            title: "ERRROR",
            text: msg,
            confirmButtonColor: "#EF5350",
            type: "error"
         });
    }

    function successMsg(msg)
    {
        if(typeof msg == 'undefined') msg = "All changes has been saved successfuly";
        swal({
            title: "SUCCESS",
            text: msg,
            confirmButtonColor: "#66BB6A",
            type: "success"
        });        
    }


    var data = {
        orderId:0,
        order_number:"",
        customer_note:"",
        dropshipper_name:"",
        dropshipper_phone:"",
        isDropShip:false,
        jne_reg:0,
        jne_yes:0,
        jne_oke:0,
        customerList: [],
        selectedCustomer :{},
        showCustomerInfo :false,
        shippingAddressList: [],
        billingAddressList :[],
        billing_id :0,
        shipping_id: 0,
        changedBilling :false,
        changedShipping: false,
        searchCustomer :"",
        shippingMethods: [],
        errors:[],
        carrier_id:'',
        shipping_amount:0,
        tracking_number:"",
        real_shipping_amount:0,
        refnumber:"",
        source:1,
        paymentMethod:'bank',
        total_discount:0,
        products:[],
        searchProducts:[],
        showSearchInputProducts:false,
        inputSearchProduct:"",
        isGiftWrap:false,
        gift_wrapping:""
    };

    var objAddress = {
        email:"",
        districtCity:"",
        firstname:"",
        lastname:"",    
        city:"",
        district:"",
        province:"",
        address:"",
        phone:"",
        postcode:"",
        type:"",
        id:0,
        user_id:0
    };

    var defaultCustomer = {
        email:"",
        firstname:"",
        lastname:"",
        id:0,
        day:1,
        month:"January",
        year:1990,
        gender:"Male",  
        phone:""
    }

    data.modalCustomer = unReactiveData(defaultCustomer);
    data.activeBilling = unReactiveData(objAddress);
    data.activeShipping = unReactiveData(objAddress);
    data.modalAddress = unReactiveData(objAddress);
    data.selectedCustomer = unReactiveData(defaultCustomer);
    data.sources = [{id:1,name:"WEB"},{id:2,name:"Tokopedia"},{id:3,name:"BukaLapak"}];
    data.paymentMethods = [{id:'bank',name:'Bank Transfer'},{id:'cc',name:'Credit Card'}];
    var vuePOS = new Vue({
        el:"#POS",
        data:data,
        created:function(){
            $("#pageLabel").html('<i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Outgoing</span> - New Order');
            this.shippingMethods.push({id:'jne_reg','name':'JNE REG'});
            this.shippingMethods.push({id:'jne_yes','name':'JNE YES'});
            this.shippingMethods.push({id:'jne_oke','name':'JNE OKE'});
        },
        ready:function(){
            var vm = this;
             $(document).ready(function() {
                
                $('#order_date').daterangepicker({
                    timePicker: true,
                    opens: "left",
                    applyClass: 'bg-slate-600',
                    cancelClass: 'btn-default',
                    singleDatePicker: true,
                    autoApply : true,
                    locale: {
                        format: 'DD MMM YYYY hh:mm a'
                    }

                 });
                $("#paymentMethod").change(function(e){
                    vm.paymentMethod = $("#paymentMethod").val();
                });
                $("#dropDownShippingMethod").change(function(e){
                    vm.carrier_id = $("#dropDownShippingMethod").val();
                });
                $("#autocompleteDistrictCity").autocomplete({
                     source: urlGetCityProvince,
                     minLength: 1
                });

                $("#add-customer").focusin(function(e){
                   $('.customer-search-result-list').addClass('show');
                });

                $("#add-customer, #add-product").click(function(e){
                    e.stopPropagation();
                });
                        
                $("#add-product").focusin(function(e){
                    e.stopPropagation();
                    $('.product-search-result-list').addClass('show');
                });

                $(document).click(function(){
                   $('.product-search-result-list').removeClass('show');
                });

                $(document).click(function(){
                   $('.customer-search-result-list').removeClass('show');
                });
                                    
                $('.date-picker').daterangepicker({
                    timePicker: true,
                    opens: "left",
                    applyClass: 'bg-slate-600',
                    cancelClass: 'btn-default',
                    singleDatePicker: true,
                    autoApply : true,
                    locale: {
                        format: 'MM/DD/YYYY h:mm a'
                    }
                });
                        
                $('select').select2({
                     minimumResultsForSearch: Infinity,
                     width: '100%'
                });


                        
                $('.address-select').each(function(){
                    var thisplaceholder = $(this).attr('placeholder');
                     $(this).select2({
                            minimumResultsForSearch: Infinity,
                            width: '100%',
                            placeholder: thisplaceholder,
                        });
                });
                        
                $('.select-status').select2({
                    minimumResultsForSearch: Infinity,
                    width: '100px',
                    placeholder: 'Select Status',
                });
                        
                function formataddressdropdown (address) {
                  if (!address.id) { return address.text; }
                  var $address = $(
                    '<div class="address-dropdown-option"><h5 class="address-dropdown-label">' + address.element.value + '</h5><h6 class="text-muted">' + address.element.getAttribute('data-detail') + '</h6></div>'
                  );
                  return $address;
                };

                $('.address-dropdown,.select-date').each(function(){
                    
                    var thisplaceholder = $(this).attr('placeholder');
                    $(this).select2({
                        minimumResultsForSearch: Infinity,
                        templateResult: formataddressdropdown,
                        placeholder : thisplaceholder,
                    });
                    
                });
                        
                $(".checkbox-gift-wrap").change(function() {
                    if(this.checked) {
                        $('.gift-wrap-textarea').removeClass('hide');
                    }
                    else{
                         $('.gift-wrap-textarea').addClass('hide');
                    }
                });

                $(".select-date").change(function(e){
                   vm.modalCustomer[$(this).data('model')] = e.target.value;
                });

                $('#dropDownBilling').append('<option data-action="create" data-type="billing" class="new-address-option" value="Create New" data-detail="&nbsp;"></option> ');
                $('#dropDownShipping').append('<option data-action="create" data-type="shipping" class="new-address-option" value="Create New" data-detail="&nbsp;"></option> ');

                
                $('.address-dropdown').change(function(){                    
                    var thisdropdown = $(this);
                    var selectedItem = $(thisdropdown).find('option:selected');
                    if($('.new-address-option').is(':selected')){

                        vm.showModalAddress($(selectedItem).attr('data-action'),$(selectedItem).attr('data-type')); 

                        if($(selectedItem).attr('data-type') == 'billing'){
                            vm.activeBilling = unReactiveData(objAddress);
                        }

                        if($(selectedItem).attr('data-type') == 'shipping'){
                            vm.activeShipping = unReactiveData(objAddress);
                        }
                        thisdropdown.select2("val", "");
                    }

                    if($(selectedItem).attr('data-index') != "" && $(selectedItem).attr('data-index') != undefined)
                    {
                        var col = 'activeShipping';
                        var colList = 'shippingAddressList';
                        if($(selectedItem).attr('data-type') == 'billing'){
                            col = 'activeBilling';
                            colList = 'billingAddressList';
                        }
                        vm[col] = unReactiveData(vm[colList][parseInt($(selectedItem).attr('data-index'))]);
                    }
                    
                });

                vm.reloadOrder(dataOrder);


        
            });
        },
        watch:{
            'isDropShip':function(val,oldVal){
                if(this.selectedCustomer.id == 0)
                {
                    this.dropshipper_name = ''
                    this.dropshipper_phone = '';
                }
                if(val == true){
                    this.dropshipper_name = this.selectedCustomer.name;
                    this.dropshipper_phone = this.selectedCustomer.phone;
                }else{
                    this.dropshipper_name = ''
                    this.dropshipper_phone = '';
                }
            },
            'carrier_id':function(val,oldVal){
                this.calculateShippingAmount();
            },
            'products':function(val,oldVal){
                this.calculateShippingAmount();
            },
            'activeBilling':function(val,oldVal){
                this.billing_id = val.id;
            },
            'activeShipping':function(val,oldVal){
                this.shipping_id = val.id;
                this.jne_oke = 0;
                this.jne_reg = 0;
                this.jne_yes = 0;
                if(typeof val.jne != 'undefined' && val.jne != null)
                {
                    if(parseInt(val.jne.jne_oke) > 0) this.jne_oke = parseInt(val.jne.jne_oke);
                    if(parseInt(val.jne.jne_reg) > 0) this.jne_reg = parseInt(val.jne.jne_reg);
                    if(parseInt(val.jne.jne_yes) > 0) this.jne_yes = parseInt(val.jne.jne_yes);
                }
                this.calculateShippingAmount();
            },
            'inputSearchProduct':function(val,oldVal){
                if(val != oldVal){
                    this.getProducts();
                }
            },
            'searchCustomer':function(val,oldVal){
              if(val != oldVal){
                this.getCustomerList();
              }
            },
            'selectedCustomer':function(val,oldVal){
                if(parseInt(val.id) == 0){
                     this.showCustomerInfo = false;
                     $('.customer-search-result-list').removeClass('hide');
                }else{
                     this.showCustomerInfo = true;
                     $('.customer-search-result-list').addClass('hide');
                }
            }
        },
        computed:{
            grandTotal()
            {
                var shipping = parseFloat(this.shipping_amount);
                if(isNaN(shipping)) shipping = 0;
                
                var subtotal = parseFloat(this.subTotal);
                if(isNaN(subtotal)) subtotal = 0;

                var discount = parseFloat(this.total_discount);
                if(isNaN(discount)) discount = 0;

                return parseFloat((shipping + subtotal) - discount);
            },
            subTotal()
            {
                var subTotalProduct = 0;
                for (var i = this.products.length - 1; i >= 0; i--) {
                    subTotalProduct += parseFloat((this.products[i].stock_qty * this.products[i].original_price) - this.products[i].discount);
                }
                return subTotalProduct;
            },
            totalWeight(){
                var total = 0;
                for (var i = this.products.length - 1; i >= 0; i--) {
                    total += parseInt(this.products[i].weight);
                }

                return total;
            },
        },
        methods:{
            reloadOrder(orderData){
                if(parseInt(orderData.id) == 0) return false;

                var title = unReactiveData(pageTitle);
                document.title = pageTitle.replace('[[title]]','Order #' + orderData.order.order_number);
                $("#pageLabel").html('<i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Outgoing</span> - Order #' + orderData.order.order_number);
                this.order_number = orderData.order.order_number;
                this.orderId = orderData.order.id;
                this.selectedCustomer = orderData.user;
                this.billingAddressList = orderData.user.billing_address;
                this.shippingAddressList = orderData.user.shipping_address;
                this.activeShipping = orderData.shipping;
                this.activeBilling = orderData.billing;
                this.billing_id = orderData.billing.id;
                this.shipping_id = orderData.shipping.id;
                this.products = orderData.products;
                this.refnumber = orderData.order.refnumber;
                this.total_discount = orderData.order.global_discount;
                this.carrier_id = orderData.order.carrier_id;
                this.customer_note = orderData.order.customer_note;
                this.dropshipper_name  = orderData.order.dropshipper_name;
                this.dropshipper_phone = orderData.order.dropshipper_phone;
                this.gift_wrapping = orderData.order.gift_wrapping;
                this.paymentMethod = orderData.order.payment_method;

                this.$nextTick(function(){
                    $("#dropDownShippingMethod").val(orderData.order.carrier_id).change();
                    $("#dropDownBilling").val(orderData.billing.firstname + ' - ' + orderData.billing.label).change();
                    $("#dropDownShipping").val(orderData.shipping.firstname + ' - ' + orderData.shipping.label).change();
                    $("#source").val(orderData.order.source).change();
                    setTimeout(function(){
                        vuePOS.shipping_amount = parseInt(orderData.order.shipping_amount);
                        if(orderData.order.dropshipper_name.trim() != ''){
                            vuePOS.isDropShip = true;
                            $("#isDropShip").prop('checked',true).uniform('update');
                        }
                        if(orderData.order.gift_wrapping.trim() != ''){
                            $("#isGiftWrap").prop('checked',true).change().uniform('update');
                             vuePOS.isGiftWrap = true;
                        }

                        $("#order_date").val(orderData.order.order_date).change();
                        $("#paymentMethod").val(orderData.order.payment_method).change();

                    },500);
                });

            },
            calculateShippingAmount()
            {

                if(this.carrier_id == null) return 0;
                if(this.carrier_id.substr(0,4) == 'jne_')
                {
                   var amount = unReactiveData(parseFloat(this.totalWeight * this[this.carrier_id]));
                   this.shipping_amount = unReactiveData(amount);
                   return amount;
                }               
                return 0;
            },
            selectProduct(index)
            {
                var item = unReactiveData(this.searchProducts[index]);
                this.products.push(item);
                this.searchProducts.splice(index,1);
            },
            initSearchProducts()
            {
                this.showSearchInputProducts = true;                
            },
           
            showModalAddress(action,type)
            {
                this.errors = [];
                this.showSuccesMessage = false;

                if(this.selectedCustomer.id == 0){
                    errorMsg("Please select user first!");
                    return false;
                }

                var col = 'billing_id';
                var colList = 'billingAddressList';
                if(type == 'shipping'){
                    col = 'shipping_id';
                    colList = 'shippingAddressList';
                }
                
                if(action == 'create'){
                    this.modalAddress = unReactiveData(objAddress);
                    this.modalAddress.type = type;
                }else{
                    var id = this[col];
                    for (var i = this[colList].length - 1; i >= 0; i--) {
                        if(this[colList][i].id == id){
                            this.modalAddress = unReactiveData(this[colList][i]);
                            this.modalAddress.type = type;
                        }
                    }
                }

                $("#modalAddress").modal('show');
            },
            saveAddress()
            {   
                if(this.selectedCustomer.id == 0) return false;

                this.errors = [];
                this.showSuccesMessage = false;
                this.modalAddress.user_id = this.selectedCustomer.id;
                var vm = this;
                var oldId = unReactiveData(this.modalAddress.id);

                this.$http.post(urlSaveAddress,this.modalAddress,function(data,s,r){
                    if(data.status == 1){
                        this.showSuccesMessage = true;
                        var col = 'activeShipping';
                        var colList = 'shippingAddressList';
                        if(data.address.type == 'billing'){
                            col = 'activeBilling';
                            colList = 'billingAddressList';
                        }

                        if(this[col].id == data.address.id){
                            this[col] = unReactiveData(data.address);                          
                        }

                        if(oldId == 0)
                        {
                            this[colList].push(data.address);
                        }

                        for (var i = this[colList].length - 1; i >= 0; i--) {
                            var item = this[colList][i];
                            if(item.id == data.address.id)
                            {
                                this[colList].$set(i,unReactiveData(data.address));
                            }
                        }
                        if(data.address.type == 'shipping'){
                            if(typeof data.jne.id != 'undefined'){
                                this.jne_oke = data.jne.jne_oke;
                                this.jne_reg = data.jne.jne_reg;
                                this.jne_yes = data.jne.jne_yes;
                            }
                        }
                    }
                    successMsg();
                    $("#modalAddress").modal('hide');
                }).error(function(data,s,r){
                    $.each(data,function(i,k){
                        for (var i = k.length - 1; i >= 0; i--) {
                            vm.errors.push(k[i]);
                        }
                    })
                });
            },
            editCustomer()
            {
                this.modalCustomer = unReactiveData(this.selectedCustomer);
                $("#modalAccount").modal('show');
            },
            openCustomerModal()
            {
                this.modalCustomer = unReactiveData(defaultCustomer);
                $("#modalAccount").modal('show');
            },
            changeCustomer()
            {
                this.selectedCustomer = unReactiveData(defaultCustomer);
                this.billingAddressList = [];
                this.shippingAddressList = [];
                this.activeShipping = unReactiveData(objAddress);
                this.activeBilling = unReactiveData(objAddress);
                this.customerList = [];
                this.$nextTick(function(){
                    $("#dropDownBilling").trigger('change');
                    $("#dropDownShipping").trigger('change');
                    $('.select-date').trigger('change');
                });
            },
            selectCustomer(index)
            {
                this.selectedCustomer = unReactiveData(this.customerList[index]);
                this.shippingAddressList = unReactiveData(this.selectedCustomer.shipping_address);
                this.billingAddressList = unReactiveData(this.selectedCustomer.billing_address);
                this.$nextTick(function(){
                    $("#dropDownBilling").trigger('change');
                    $("#dropDownShipping").trigger('change');
                    $('.select-date').trigger('change');
                });
            },
            getProducts()
            {
              $("#add-product").prop('disabled',true);
               this.$http.post(urlSearchProducts,{q:$("#add-product").val()},function(data,s,r){
                    $('.product-search-result-list').addClass('show');
                    this.searchProducts = data.items;   
                    this.$nextTick(function(){
                      $("#add-product").prop('disabled',false);
                      $("#add-product").focus();
                    });
               });
            },

            getCustomerList(){
               $("#add-customer").prop('disabled',true);
               this.selectedCustomer = unReactiveData(defaultCustomer);
               this.customerList = [];
               this.$http.post(urlGetCustomer,{q:$("#add-customer").val()},function(data,s,r){
                    $('.customer-search-result-list').addClass('show');
                    this.customerList = data;   
                    this.$nextTick(function(){
                      $("#add-customer").prop('disabled',false);
                      $("#add-customer").focus();
                    });
               });
            },
            saveUser(e){
                e.preventDefault();
                var vm = this;
                this.errors = [];
                $("[name=first_name]").parent().removeClass('has-error');
                showLoading($("#modalAccount").find('.modal-body'));
                this.$http.post(urlSaveUser,this.modalCustomer,function(data,s,r){
                    hideLoading();
                    if(data.status == 1)
                    {
                        successMsg();
                        this.selectedCustomer = data.user;
                    }else{
                        this.errors.push(data.msg);
                    }

                }).error(function(data,s,r){
                    hideLoading();
                    $.each(data,function(i,k){
                        $("[name=first_name]").parent().addClass('has-error');
                        for (var x = k.length - 1; x >= 0; x--) {
                            vm.errors.push(k[i]);
                        }
                    })
                });
            },
            validateBeforeSave()
            {
                if(this.selectedCustomer.id == 0){
                    errorMsg("Please select user first!");
                    return false;
                }
                if(this.activeBilling.address.trim() == "")
                {
                    errorMsg("Please billing address first!");
                    return false;
                }
                if(this.activeShipping.address.trim() == "")
                {
                    errorMsg("Please shipping address first!");
                    return false;
                }
                
                if(this.products.length == 0){
                    errorMsg("Please select product first!");
                    return false;
                }

                for (var i = this.products.length - 1; i >= 0; i--) {
                    var tmpItem = this.products[i];
                    if(tmpItem.stock_qty > tmpItem.stock_available){
                        errorMsg("Out of range!" + "Max qty for " + tmpItem.product_title + " is " + tmpItem.stock_available);
                        return false;
                    }
                }

                return true;

            },
            getOrderFields()
            {
                var fields = ['gift_wrapping','dropshipper_name','dropshipper_phone','customer_note','total_discount','refnumber','billing_id','shipping_id','carrier_id','shipping_amount'];
                return fields;
            },
            placeOrder(){
                var status = this.validateBeforeSave();
                if(status == false) return false;

                showLoading($("#POS"));
                var fields = this.getOrderFields();
                var dataObj = {};
                dataObj.order = {};
                dataObj.order.id = this.orderId;
                dataObj.order.source = $("#source").val();
                dataObj.order.order_date = $("#order_date").val();
                dataObj.order.paymentMethod = this.paymentMethod;
                for (var i = fields.length - 1; i >= 0; i--) {
                    dataObj.order[fields[i]] = this[fields[i]];
                }
                if(this.isGiftWrap == false) dataObj.order.gift_wrapping = "";

                dataObj.products = this.products;
                dataObj.user = this.selectedCustomer;
                dataObj.shippingAddress = this.activeShipping;
                dataObj.billingAddress = this.activeBilling;
                this.$http.post(urlSaveOrder,dataObj,function(data,s,r){
                    hideLoading();
                    if(data.status == 1){
                        this.reloadOrder(data);
                    }
                    successMsg();

                }).error(function(data,s,r){
                    alert("500 Internal Server Error!");
                });
            }
        }
    });
