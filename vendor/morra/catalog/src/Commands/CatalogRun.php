<?php

namespace Morra\Catalog\Commands;

use Illuminate\Console\Command;

class CatalogRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'catalog.init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init Catalog';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->checkSetting();
            $this->checkDatabase();
            echo "Done! Happy Shopping!";
        } catch (Exception $e) {
            echo "Ouch Error! Oi Oi Oi A A A A A A BEAUTIFULL ,FIRE TAIGA TIGER YA YA";
            echo "----------Error--------";
            echo $e->getMessage();
            echo "----------Error--------";
        }
        
    }
    private function checkSetting(){
        $setting = \Setting::whereSettingKey('entry_catalog_variant_product')->first();
        if(empty($setting))
        {
            $setting = new \Setting();
        }
        $setting->autoload = 'yes';
        $setting->bundle = 'alpha.entry';
        $setting->setting_key = 'entry_catalog_variant_product';
        if($setting->setting_value == '' || empty($setting->setting_value)){
            $setting->setting_value = serialize(\Config::get('catalog.structure'));            
        }
        $setting->save();


        $setting = \Setting::whereSettingKey('entry_item_catalog_variant_product')->first();
        if(empty($setting)){
            $setting = new \Setting();
        }
        $setting->autoload = 'yes';
        $setting->bundle = 'alpha.entry';
        $setting->setting_key = 'entry_item_catalog_variant_product';
        if($setting->setting_value == '' || empty($setting->setting_value)){
            $setting->setting_value = serialize(\Config::get('catalog.structure-item'));
        }
        $setting->save();


        $setting = \Setting::whereSettingKey('entry_brand')->first();
        if(empty($setting)){
            $setting = new \Setting();
        }
        $setting->autoload = 'yes';
        $setting->bundle = 'alpha.entry';
        $setting->setting_key = 'entry_brand';
        if($setting->setting_value == '' || empty($setting->setting_value)){
            $setting->setting_value = serialize(\Config::get('catalog.structure-brand'));
        }
        $setting->save();



        


        $vars = ['weight_unit','length_unit','currency'];
        foreach ($vars as $key => $value) {
           $setting = \Setting::whereSettingKey($value)->whereBundle('alpha_catalog')->first();
           if(empty($setting)){
                $setting = new \Setting();
           }
           $setting->autoload = 'yes';
           $setting->setting_key = $value;
           $setting->bundle = 'alpha_catalog';
           if($setting->setting_value == '' || empty($setting->setting_value)){
               $setting->setting_value ='';            
           }
           $setting->save();
        }

    }
    private function checkDatabase(){
        
        //alter entry
        \Schema::table('entries', function($table)
        {
            if (!\Schema::hasColumn('entries', 'catalog_type')){
                $table->string('catalog_type', 20);
            }
            if (!\Schema::hasColumn('entries', 'brand_id')){
                $table->string('brand_id', 20);
            }
            if (!\Schema::hasColumn('entries', 'brand_count')){
                $table->integer('brand_count')->default(0);
            }
            if (!\Schema::hasColumn('entries', 'media_id')){
                $table->integer('media_id')->default(0);
            }
        });

        \Schema::table('taxonomies', function($table)
        {
            if (!\Schema::hasColumn('taxonomies', 'data')){
                $table->text('data');
            }
             if (!\Schema::hasColumn('taxonomies', 'store_data')){
                $table->text('store_data');
            }


        });



        //create table item
        if (!\Schema::hasTable('catalog_items')){
            \Schema::create('catalog_items', function($table)
            {
                $table->increments('id');
                $table->bigInteger('product_id');
                $table->integer('order_limit')->default(0);
                $table->string('item_title'); 
                $table->string('item_description'); 
                $table->string('sku');    
                $table->string('type'); 
                $table->string('status');    
                $table->string('stock_status');  
                $table->string('schedule_sale');
                $table->integer('stock');
                $table->integer('stock_available');
                $table->decimal('price',12, 4);
                $table->decimal('sale_price',12, 4);
                $table->decimal('msrp',12, 4);
                $table->dateTime('sale_start_date');
                $table->dateTime('sale_end_date');
                $table->timestamps();   

            });
        }
       

        //create table item metas
        if (!\Schema::hasTable('catalog_item_metas')){
            \Schema::create('catalog_item_metas', function($table)
            {
                $table->increments('id');
                $table->bigInteger('item_id');
                $table->string('meta_key'); 
                $table->string('meta_name'); 
                $table->integer('meta_value_int'); 
                $table->string('meta_value_text'); 
                $table->decimal('meta_value_decimal',12, 4);
                $table->dateTime('meta_value_date');
                $table->timestamps();   

            });
        }

        //item media
        if (!\Schema::hasTable('item_media')){
            \Schema::create('item_media', function($table)
            {
                $table->increments('id');
                $table->bigInteger('item_id');
                $table->bigInteger('media_id'); 
                $table->timestamps();   
            });
        }

        //item taxonomy
        if (!\Schema::hasTable('item_taxonomy')){
            \Schema::create('item_taxonomy', function($table)
            {
                $table->increments('id');
                $table->bigInteger('item_id');
                $table->bigInteger('taxonomy_id'); 
                $table->timestamps();   
            });
        }

        //item group
        if (!\Schema::hasTable('catalog_item_group')){
            \Schema::create('catalog_item_group', function($table)
            {
                $table->increments('id');
                $table->bigInteger('parent');
                $table->string('item_id'); 
                $table->timestamps();   
            });
        }

        //wishlist
        if (!\Schema::hasTable('catalog_wishlists')){
            \Schema::create('catalog_wishlists', function($table)
            {
                $table->increments('id');
                $table->bigInteger('item_id');
                $table->bigInteger('user_id');
                $table->string('status');
                $table->timestamps();   
            });
        }

        //wishlist
        if (!\Schema::hasTable('catalog_carts')){
            \Schema::create('catalog_carts', function($table)
            {
                $table->increments('id');
                $table->bigInteger('item_id');
                $table->string('session_id');
                $table->bigInteger('user_id');
                $table->integer('qty');
                $table->timestamps();   
            });
        }
        //catalog search
        if (!\Schema::hasTable('catalog_search')){
            \Schema::create('catalog_search', function($table)
            {
                $table->increments('id');
                $table->bigInteger('item_id');
                $table->bigInteger('product_id');
                $table->integer('qty');
                $table->timestamps();   
            });
        }
        //catalog jne
        if (!\Schema::hasTable('catalog_jne')){
            \Schema::create('catalog_jne', function($table)
            {
                $table->increments('id');
                $table->string('code');
                $table->string('district');
                $table->string('regency');
                $table->decimal('jne_oke',12, 4);
                $table->decimal('jne_reg',12, 4);
                $table->decimal('jne_yes',12, 4);
                $table->timestamps();   
            });
            \Morra\Catalog\Models\Jne::insert(\Config::get('catalog.jne-table'));
        }

        if (!\Schema::hasTable('catalog_addresses')){
            \Schema::create('catalog_addresses', function($table)
            {
                $table->increments('id');
                $table->bigInteger('user_id');
                $table->bigInteger('created_by');
                $table->bigInteger('modified_by');

                $vars = array('label','type','email','firstname','lastname','company','address','address_1','address_2','city','postcode','country','county','state','district','province','phone','mobile_phone');
                foreach($vars as $v)
                {
                    $table->string($v,200);
                }
                $table->integer('jne_id');
                $table->integer('is_checkout')->default(0);
                $table->timestamps();   
            });
           
        }

        if(!\Schema::hasTable('catalog_orders')){
            \Schema::create('catalog_orders', function($table){
                $table->increments('id');
                $table->string('order_number');
                $table->bigInteger('user_id');
                $table->integer('settlement')->default(0);
                $table->decimal('subtotal',12,2);
                $table->decimal('total_discount',12,2);
                $table->decimal('global_discount',12,2);
                $table->decimal('subtotal_after_discount',12,2);
                $table->decimal('subtotal_before_discount',12,2);
                $table->decimal('discount_from_items',12,2);
                $table->decimal('global_and_item_discount',12,2);
                $table->string('refnumber');
                $table->integer('billing_id');
                $table->integer('shipping_id');
                $table->string('dropshipper_name');
                $table->string('dropshipper_phone');
                $table->string('payment_address');
                $table->string('shipping_address');
                $table->dateTime('order_date');
                $table->string('source');
                $table->text('gift_wrapping');
                $table->decimal('shipping_amount',12,2);    
                $table->decimal('real_shipping_amount',12,2);
                $table->decimal('grand_total',12,2);
                $table->decimal('tax',12,2);
                $table->string('customer_note');
                $table->string('note');
                $table->integer('carrier_id');
                $table->string('carrier_name');
                $table->string('carrier_level');
                $table->string('tracking_number');
                $table->dateTime('shipping_date');
                $table->string('confirmation');
                $table->dateTime('confirmation_date');     
                $table->text('cancel_reason');
                $table->dateTime('cancel_date');
                $table->string('status');
                $table->bigInteger('modified_by');
                $vars = array('method','email','firstname','lastname','company','address_1','address_2','city','postcode','country','county','state','district','province','phone','mobile_phone');
                foreach($vars as $v)
                {
                    $table->string('payment_'.$v,200);
                }
                foreach($vars as $v)
                {
                    $table->string('shipping_'.$v,200);
                }
                
                $table->text('data');

                $table->timestamps();   
            });
        }

        \Schema::table('catalog_orders', function($table)
        {
            if (!\Schema::hasColumn('catalog_orders', 'store')){
                $table->integer('store')->default(0);
            }
        });



        if(!\Schema::hasTable('catalog_order_details')){
            \Schema::create('catalog_order_details', function($table){
                $table->increments('id');                
                $table->bigInteger('order_id');
                $table->bigInteger('product_id');
                $table->bigInteger('item_id');
                $table->string('product_title');
                $table->string('item_title');
                $table->text('product_desc');
                $table->text('item_desc');
                $table->string('sku');
                $table->string('type');
                $table->decimal('original_price',12,4);
                $table->decimal('discount',12,4);
                $table->decimal('total',12,4);
                $table->decimal('original_total',12,4);
                $table->decimal('weight',12,4);
                $table->string('brand_name');
                $table->integer('qty');
                $table->text('image_original');
                $table->text('image_thumbnail');
                $table->string('status');   
                $table->bigInteger('modified_by');
                $table->text('data');
                $table->timestamps();   
            });
        }

         if(!\Schema::hasTable('catalog_orders_promo')){
            \Schema::create('catalog_orders_promo', function($table){
                $table->increments('id');                
                $table->bigInteger('order_id');
                $table->bigInteger('order_detail_id');
                $table->string('promo_type');
                $table->string('promo_name');
                $table->string('level');
                $table->decimal('amount',12,4);
                $table->text('promo_code');
                $table->text('data');
                $table->timestamps();   
            });
        }

        if(!\Schema::hasTable('catalog_payment_banktransfer')){
            \Schema::create('catalog_payment_banktransfer', function($table){
                $table->increments('id');
                $table->bigInteger('order_id');
                $table->bigInteger('user_id');
                $table->dateTime('payment_date');
                $table->decimal('amount',12,4);
                $vars = array('bank','user_account','user_holder','user_bank','refnumber','status');
                foreach($vars as $v){
                    $table->string($v);
                }
                $table->timestamps();   
            });
        }

        if(!\Schema::hasTable('catalog_promos')){
            \Schema::create('catalog_promos', function($table){
                $table->increments('id');
                $table->bigInteger('user_id');
                $table->string('coupon_code',250);
                $table->string('coupon_type',250);
                $table->string('algorithm',250);
                $table->string('title',250);
                $table->string('status',250);
                $table->decimal('rule_detail',12,4);
                $table->dateTime('start');
                $table->dateTime('end');
                $table->text('content');
                $table->text('rules');
                $table->decimal('min_order',12,4);
                $table->timestamps();   
            });
        }
        
        if(!\Schema::hasTable('catalog_log_promos')){
            \Schema::create('catalog_log_promos', function($table){
                $table->increments('id');
                $table->bigInteger('order_id');
                $table->bigInteger('promo_id');
                $table->bigInteger('promo_type');
                $table->decimal('amount',12,4);
                $table->timestamps();   
            });
        }


        if(!\Schema::hasTable('catalog_order_activities')){
            \Schema::create('catalog_order_activities', function($table){
                $table->increments('id');
                $table->bigInteger('order_id');
                $table->bigInteger('user_id');
                $table->string('type');
                $table->text('info');
                $table->timestamps();   
            });
        }

         if (!\Schema::hasTable('catalog_accounts')){
            \Schema::create('catalog_accounts', function($table)
            {
                $table->increments('id');
                $table->string('name'); 
                $table->text('description'); 
                $table->text('sales_channels');
                $table->timestamps();   

            });
        }

         if (!\Schema::hasTable('catalog_payment_channels')){
            \Schema::create('catalog_payment_channels', function($table)
            {
                $table->increments('id');
                $table->string('name'); 
                $table->text('description'); 
                $table->timestamps();   

            });
        }

         if (!\Schema::hasTable('catalog_sales_channels')){
            \Schema::create('catalog_sales_channels', function($table)
            {
                $table->increments('id');
                $table->string('name'); 
                $table->text('description'); 
                $table->string('type');
                $table->text('payment_methods');
                $table->timestamps();   

            });
        }



        \DB::table('catalog_accounts')->insert([
                'id' => 1,
                'name' => 'Bank Account',
                'sales_channels' => '["1"]',
                'description' => 'Bank Account',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

       \DB::table('catalog_payment_channels')->insert([
                'id' => 1,
                'name' => 'Bank Transfer',
                'description' => 'Bank Transfer',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

       \DB::table('catalog_payment_channels')->insert([
                'id' => 2,
                'name' => 'Credit Card',
                'description' => 'Credit Card',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

       \DB::table('catalog_payment_channels')->insert([
                'id' => 3,
                'name' => 'Cash',
                'description' => 'Cash',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

       \DB::table('catalog_payment_channels')->insert([
                'id' => 4,
                'name' => 'Tokopedia Escrow',
                'description' => 'Tokopedia Escrow',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

    \DB::table('catalog_sales_channels')->insert([
                'id' => 1,
                'name' => 'Web',
                'description' => 'Web',
                'type' => 'default',
                'payment_methods' => '["1"]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

    \DB::table('catalog_sales_channels')->insert([
                'id' => 2,
                'name' => 'Offline',
                'description' => 'Offline',
                'type' => 'default',
                'payment_methods' => '["1","3"]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

    \DB::table('catalog_sales_channels')->insert([
                'id' => 3,
                'name' => 'Tokopedia',
                'description' => 'Tokopedia',
                'type' => 'marketplace',
                'payment_methods' => '["4"]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);



     

        

    }
}
   