<?php 
return [
	'CatalogItem' => 'Morra\Catalog\Models\Item',
	'CatalogGroup' => 'Morra\Catalog\Models\ItemGroup',
	'CatalogItemMeta' => 'Morra\Catalog\Models\ItemMeta',
	'CatalogWishlist' => 'Morra\Catalog\Models\Wishlist',
	'CatalogSearch' => 'Morra\Catalog\Models\Search',
	'CatalogSync' => 'Morra\Catalog\Models\Sync',
	'CatalogCart' => 'Morra\Catalog\Facades\Cart',
	'CatalogShippingMethod' => 'Morra\Catalog\Facades\ShippingMethod',
	'CatalogAddress' => 'Morra\Catalog\Models\Address',
	'CatalogOrder' => 'Morra\Catalog\Models\Order',
	'CatalogOrderDetail' => 'Morra\Catalog\Models\OrderDetail',
	'CatalogOrderPromo' => 'Morra\Catalog\Models\OrderPromo',
	'CatalogBankTransfer' => 'Morra\Catalog\Models\BankTransfer',
	'CatalogCheckout' => 'Morra\Catalog\Facades\Checkout',
	'CatalogPromo' => 'Morra\Catalog\Models\Promo',
	'CatalogLogPromo' => 'Morra\Catalog\Models\LogPromo',
	'CatalogPromoContainer' => 'Morra\Catalog\Facades\PromoContainer',
	'CatalogPOS' => 'Morra\Catalog\Facades\POS',
	'CatalogOrderActivity' => 'Morra\Catalog\Models\OrderActivity',
	'CatalogAccount' => 'Morra\Catalog\Models\Account',
	'CatalogPaymentChannel' => 'Morra\Catalog\Models\PaymentChannel',
	'CatalogSalesChannel' => 'Morra\Catalog\Models\SalesChannel',
	'CatalogMail' => 'Morra\Catalog\Facades\Mail'

];