<?php 

return [
	'slug' => 'brand',
	'single' => 'Brand',
	'plural' => 'Brands',
	'show_ui' => 'yes',
	'metas' => [
		(object)['meta_key' => 'address','meta_name'=>'Address','meta_data_type'=>'text'],
	]
];