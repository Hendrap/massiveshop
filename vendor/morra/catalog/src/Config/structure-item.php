<?php 

return [
	'slug' => 'item_catalog_variant_product',
	'single' => 'Item',
	'plural' => 'Items',
	'show_ui' => 'no',
	'metas' => [
		// (object)['meta_key' => 'weight','meta_name'=>'Weight','meta_data_type'=>'text'],
		// (object)['meta_key' => 'length','meta_name'=>'Length','meta_data_type'=>'text'],
		// (object)['meta_key' => 'width','meta_name'=>'Width','meta_data_type'=>'text'],
		// (object)['meta_key' => 'height','meta_name'=>'Height','meta_data_type'=>'text']
	]
];