<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;
use Logger;
use Entry;
use CatalogAccount;
use CatalogPaymentChannel;
use CatalogSalesChannel;

class AccountController extends AlphaController
{
    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_setting';
        $this->layout->active = 'alpha_catalog_payment_account';
    }

    public function index()
    {
    	$accounts = CatalogAccount::get();
        $channels = CatalogSalesChannel::get();
    	$this->layout->title = setPageTitle("Payment Accounts");
    	$this->layout->content = view('catalog::admin.pages.account.index',[
    			'accounts' => $accounts,
                'channels' => $channels
    		]);
    }

    public function saveData(Request $req)
    {
    	$account = CatalogAccount::find($req->id);
    	if(empty($account)) $account = new CatalogAccount();
    	$account->name = $req->name;
    	$account->description = $req->description;
        $account->sales_channels = json_encode($req->sales_channels);
    	$account->save();

        $html = '';
        $html .= view('catalog::admin.pages.account.grid',['account'=>$account]);
    	return json_encode(array('status'=>1,'account'=>$account,'html'=>$html));
    }	

    public function deleteData(Request $req)
    {
    	$account = CatalogAccount::find($req->id);
    	$payment = CatalogPaymentChannel::whereAccountId($account->id)->update(['account_id'=>0]);
    	$account->delete();

    	return redirect()->back();
    }


 }