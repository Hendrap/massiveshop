<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;
use Logger;
use Entry;


class BrandController extends AlphaController
{
    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_stock';
        $this->layout->active = 'alpha_catalog_brands';
    }

    public function getData(Request $req){
    	$type = 'brand';
    	//container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');
        //categgory
        $isSearch = false;
        
        $base = new Entry();            
        
        $recordsTotal = $base->where('status','!=','deleted')->whereEntryType($type);
       
        $recordsTotal = $recordsTotal->count();

        //filter entry
        $entries = $base->whereEntryType($type)->where('entries.status','!=','deleted');


        if(!empty($search['value']))
        {
            $isSearch = true;
            $entries = $entries->where(function($q)use($search){
                $q->where('entries.title','like','%'.$search['value'].'%');
                $q->orWhere('entries.content','like','%'.$search['value'].'%');
                $q->orWhere('entries.excerpt','like','%'.$search['value'].'%');
            });
        }        
        //ambil data

        //entry status
        $isStatus = false;
        $status = $req->input('columns');
        if(!empty($status[5]['search']['value']))
        {
            $status = $status[5]['search']['value'];
            if($status == 'scheduled_publish')
            {
                $entries = $entries->whereStatus('published');
                $entries = $entries->where('published_at','>',date('Y-m-d H:i:s'));
            }else{
                $entries = $entries->whereStatus($status);
            }
            $isStatus = true;
        }

        $recordsFiltered = $entries->count();

        
        $entries = $entries->take($take)->skip($start);
        //sorting
        $cols = ['title','brand_count','created_at','updated_at'];
        $order = $req->input('order');
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $entries = $entries->orderBy($col,$order[0]['dir']);
            }else{
                $entries = $entries->orderBy('entries.created_at','desc');
            }
        }else{
            $entries = $entries->orderBy('entries.created_at','desc');
        }
        

        $entries = $entries->with(['medias'])->get();
        
        $cols = ['title','brand_count','created_at','updated_at','action'];
        $view = 'catalog::admin.pages.brands.template';
        $base = [];
        foreach ($cols as $col) {
            $base[$col] = '';
        }

      
        foreach ($entries as $key => $value) {
            $item = $base;
            $item['entry_parent'] = $value->entry_parent;
            $html = '';
            $html .= view($view,['entry'=>$value,'type'=>$type,'isSearch'=>$isSearch]);
            $item['html'] = $html;

            $data[] = $item;
        }


        Logger::info("View Entry : ".$type);

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);

    }

    public function create(Request $req)
    {
    	$type = 'brand';

    	$brand = new Entry();
    	$brand->title = '[:en]'.$req->name;
    	$brand->content = '[:en]'.$req->desc;
    	$brand->entry_type = 'brand';
    	$brand->status = 'published';
		$brand->published_at = date('Y-m-d H:i:s');
    	$brand->author = app('AdminUser')->user->id;
    	$brand->modified_by = app('AdminUser')->user->id;
 		$slug = str_slug($req->name);

        $originalSlug = $slug;
        $check = Entry::where('id','!=',0)->where('slug','like',$slug.'%')->count();
        if($check > 0){
            $slug .= '-'.$check;
        }

        if($brand->slug != $slug && $type == 'page'){
            $routeList = getRouteList();
            $locale = \Config::get('alpha.application.default_locale').'/';
            $testRoute = $slug;
            $isMultiLang = false;
            if(count(\Config::get('alpha.application.locales')) > 1)
            {
                $isMultiLang = true;
                $testRoute = $locale.$slug;
            }

            while (in_array($testRoute, $routeList)) {
                $check++;
                $slug = $originalSlug.'-'.$check;
                $testRoute = $slug;
                if($isMultiLang) $testRoute = $locale.$slug;
            }
        }
        $brand->slug = $slug;
        
        $brand->brand_count = 0;
    	$brand->save();

    	$entry = Entry::find($brand->id);

    	$cols = ['title','brand_count','created_at','updated_at','action'];
        $view = 'catalog::admin.pages.brands.template';
        $base = [];
        foreach ($cols as $col) {
            $base[$col] = '';
        }


    	$item = $base;
        $item['entry_parent'] = 0;
        $html = '';
        $html .= view($view,['entry'=>$entry,'type'=>'brand','isSearch'=>false]);
        $item['html'] = $html;
        
    	return json_encode(['status'=>1,'html'=>$item]);
    }

    public function edit(Request $req)
    {
    	$brand = Entry::find($req->id);
    	if(empty($brand)) return json_encode(['status'=>0]);

    	$brand->title = '[:en]'.$req->name;
    	$brand->content = '[:en]'.$req->desc;
        $image = "";
        if($req->file('image')){
            $media = mediaUploader('image',$req);
            $image = asset($media->path);
            $brand->medias()->sync([$media->id]);

        }
    	$brand->save();

    	return json_encode(array('status'=>1,'image'=>$image));
    }

    public function index(Request $req)
    {
    	$this->layout->title = setPageTitle("Brands");
    	$this->layout->content = view('catalog::admin.pages.brands.index');
    }

}