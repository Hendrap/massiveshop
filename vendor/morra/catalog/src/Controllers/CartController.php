<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;
use Logger;
use Entry;
use CatalogCart;
use CatalogSync;
use Morra\Catalog\Models\Cart as CartCoreModel; 


class CartController extends AlphaController
{
    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_stock';
        $this->layout->active = 'alpha_catalog_shopping_carts';
    }

    public function deleteCart($id = 0,Request $req)
    {
        $cart = CartCoreModel::find($id);
        if(!empty($cart)){
           $response = CatalogCart::removeItemFromCart($cart->item_id,$cart->user_id);            
           if(!empty($response['item']))
           {
                CatalogSync::doSyncProducts([$response['item']->product_id]);
           }
        }
        
        return redirect()->back();

    }

    public function index(){
        $this->layout->title = setPageTitle("Shopping Carts");
        $this->layout->content = view('catalog::admin.pages.cart.index');
    }

    public function getData(Request $req)
    {
        //container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');
        //order
        $order = $req->input('order');
        //categgory
        $isSearch = false;
       
        $base = new CartCoreModel();            
       
        $recordsTotal = $base;
        $recordsTotal = $recordsTotal->count();

        //filter entry
        $carts = $base;


        if(!empty($search['value']))
        {
            $isSearch = true;
            $carts = $carts->where(function($q)use($search){
                $q->where('catalog_carts.id','like','%'.$search['value'].'%');
            });
        }


         //total entry yg kefilter
        $recordsFiltered = $carts->count();
        

       
        $carts = $carts->take($take)->skip($start);
        //sorting
        $cols = ['id','qty','original_price','updated_at'];
        
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $carts = $carts->orderBy($col,$order[0]['dir']);
            }else{
                $carts = $carts->orderBy('catalog_carts.updated_at','desc');
            }
        }else{
            $carts = $carts->orderBy('catalog_carts.updated_at','desc');
        }
        

        $carts = $carts->with(['item','item.entry','item.entry.medias','user','user.metas'])->get();


        $cols = ['id','qty','original_price','updated_at','action'];
        $view = 'catalog::admin.pages.cart.grid';
        $base = [];
        foreach ($cols as $col) {
            $base[$col] = '';
        }
        
        foreach ($carts as $key => $value) {

            $item = $base;
            $html = '';
            $html .= view($view,['cart'=>$value]);
            $item['html'] = $html;
            $data[] = $item;
        }


        Logger::info("View Shipping Carts");

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);

    }

    

 }