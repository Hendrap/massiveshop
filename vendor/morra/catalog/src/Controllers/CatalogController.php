<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \CatalogOrder;
use \DB;

class CatalogController extends AlphaController
{
	public function index(Request $req){

		$this->layout->active = 'alpha_catalog_stat';
		$selectYear = $req->input('year',date('Y'));
		$data = CatalogOrder::select([DB::raw('count(id) as total_order'),DB::raw('sum(grand_total) as total_amount'),DB::raw("DATE_FORMAT(created_at,'%m') as selected_date")])
		->where(DB::raw("DATE_FORMAT(created_at,'%Y')"),'=',$selectYear)
		->groupBy(DB::raw("DATE_FORMAT(created_at,'%m-&Y')"))
		->get();

		$revenue = [];
		$order = [];

		for ($i=1; $i <=  12; $i++) { 
			$res[$i] = 0;
			$order[$i] = 0;
		}

		foreach ($data as $key => $value) {
			$res[(int)$value->selected_date] = $value->total_amount;
			$order[(int)$value->selected_date] = $value->total_order;
		}
		$totalRevenue = [];
		foreach ($res as $key => $value) {
			$totalRevenue[] = (float)$value;
		}

		$totalOrder = [];
		foreach ($order as $key => $value) {
			$totalOrder[] = (float)$value;
		}

		$years = CatalogOrder::select([DB::raw("DATE_FORMAT(created_at,'%Y') as year")])
		->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y')"))
		->get();

		$totalProduct = \Entry::where('entry_type','like','catalog_variant_%')->whereStatus('published')->count();
		$totalItem = \CatalogItem::count();
		$totalWishlist = \CatalogWishlist::count();
		$totalAllOrder = \CatalogOrder::count();
		$totalOrderRevenue = \CatalogOrder::sum('grand_total');


		$this->layout->title = setPageTitle("Catalog Statistics");
		$this->layout->content = view('catalog::admin.pages.stat',['totalAllOrder'=>$totalAllOrder,'totalWishlist'=>$totalWishlist,'totalOrderRevenue'=>$totalOrderRevenue,'totalItem'=>$totalItem,'totalProduct'=>$totalProduct,'selectYear'=>$selectYear,'years'=>$years,'totalOrder' => $totalOrder,'totalRevenue'=>$totalRevenue]);
	}
}