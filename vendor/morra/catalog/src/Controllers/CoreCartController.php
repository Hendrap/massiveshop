<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use CatalogOrder;
use CatalogItem;
class CoreCartController extends BaseController
{
	public function clean(Request $req){
		set_time_limit(0);

		if($req->input('token') != \Config::get('catalog.cart.encryptCart')) exit();

		\CatalogCart::cleanUpExpiredCart(\Config::get('catalog.cart.period'));

		echo "Done!";

	}


	public function cleanUpExpiredOrder()
	{
		$date = new \DateTime();
        $date = $date->sub(new \DateInterval('P'.config('catalog.cart.periodExpiredOrder').'D'));

        $idsProduct = [];
        $orders = CatalogOrder::with(['details','user','user.metas'])->where('created_at', '<=', $date)->where('status', '=', 'PENDING')->get();
        if(!empty($orders))
        {
        	foreach ($orders as $order) {
        		foreach ($order->details as $key => $value) {
        			$item = CatalogItem::find($value->item_id);
	                if(!empty($item))
	                {
	                    $item->stock_available = $item->stock_available + $value->qty;
		                $item->save();
		                $idsProduct[] = $item->product_id;
	                }
        		}

        		//$order->cancel_date = date('Y-m-d H:i:s');
        		//$order->cancel_reason = 'Expired';
        		$order->status = 'CANCELLED';
        		$order->save();

       //  		$user = $order->user;

			    // if(!empty($user)){
			    // 	if(class_exists('App\PopitoiMailGuy'))
			    // 	{
			    //     	$name = getEntryMetaFromArray($user->metas,'first_name').' '.getEntryMetaFromArray($user->metas,'last_name');
			    //     	if(trim($name) == '') $name = $user->email;
			    //     	app('App\PopitoiMailGuy',[$user->email])->sendEmailOrderCancel($order,$name,$user);
			    // 	}                    
			    // }


        	}
        }


        echo "Order Cancel<br>";
	}
}