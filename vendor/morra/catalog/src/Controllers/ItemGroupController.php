<?php 

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;

class ItemGroupController extends AlphaController
{
	public function getAdditionalInfo($type = ''){

        $metas = getMetaFromEntry($type);

        $taxoInfo = getTaxonomyFromEntry($type);
        $taxoTypes = $taxoInfo->taxoTypes;
        $taxo = $taxoInfo->taxo;
        $taxonomies = getTaxonomiesForEntry($taxoTypes);
        return (object)['taxonomies' => $taxonomies,'metas'=>$metas,'taxo'=>$taxo];
    }
	public function create($product_id = 0)
	{
		$product = \Entry::find($product_id);
		if(empty($product)) abort(404);

		$type = 'itemgroup_'.$product->entry_type;

		$config = getEntryConfig($type);

		$info = $this->getAdditionalInfo($type);
		
		$this->layout->active = 'alpha_catalog_type_'.$product->entry_type;
		$this->layout->title = setPageTitle("Create Item");
		$this->layout->content = view('catalog::admin.pages.item-groups.create',[
			'product' => $product,
			'config' => $config,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
			]);
	}
	public function edit($product_id = 0,$id = 0)
	{
		$product = \Entry::find($product_id);
		if(empty($product)) abort(404);

		$item = \CatalogItem::with(['entry','metas','medias','taxonomies','group','group.item','group.item.entry'])->find($id);
		if(empty($item)) abort(404);

		$type = 'itemgroup_'.$product->entry_type;

		$config = getEntryConfig($type);
		$info = $this->getAdditionalInfo($type);
		$selectedTaxo = [];
		
		if(!empty($item->taxonomies)){
            foreach ($item->taxonomies as $key => $value) {
                $selectedTaxo[] = $value->id;
            }
        }

        $this->layout->active = 'alpha_catalog_type_'.$product->entry_type;
		$this->layout->title = setPageTitle("Edit Item");
		$this->layout->content = view('catalog::admin.pages.item-groups.edit',[
			'item' => $item,
			'config' => $config,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
            'selectedTaxo'=>$selectedTaxo,
            'type'=>$type,
            'product'=>$product
			]);

	}
	public function save($product_id = 0,$id = 0,Request $req){

		$product = \Entry::find($product_id);
		if(empty($product)) abort(404);

		$type = 'item_'.$product->entry_type;

		$config = getEntryConfig($type);


		$rules = [
		'item_title' => 'required',
		'price' => 'required|numeric',
		'stock' => 'required|numeric',
		'sale_price'=>'numeric',
		'msrp'=>'numeric'
		];
		$this->validate($req,$rules);


		$input = $req->input();
		$item = \CatalogItem::find($id);
		if(empty($item)){
			$item = new \CatalogItem();
		}
		$item->product_id = $product_id;
		
		$metas = $req->input('metas',[]);
		$taxonomies = $req->input('taxonomies',[]);
		$medias = $req->input('media',[]);
		$groups = $req->input('group_items',[]);


		unset($input['group_items']);
		unset($input['_token']);
		unset($input['id']);
		unset($input['metas']);
		unset($input['taxonomies']);
		unset($input['media']);

		if($id == 0){
			$input['stock_available'] = $input['stock'];
		}else{
			$qtyChange = $item->stock - $input['stock'];
			$input['stock_available'] = $item->stock_available - $qtyChange;
		}

		foreach ($input as $key => $value) {
			$item->{$key} = $value;
		}

		$item->type = 'group';			

		$item->save();
		$item->saveMetaFromInput(@$config['metas'],$metas);
		$item->taxonomies()->sync([]);  
		$item->taxonomies()->sync($taxonomies);

		if(!empty($taxonomies)){
            foreach ($taxonomies as $key => $value) {
                $tmpTaxo = \Taxonomy::find($value);
                if(!empty($tmpTaxo)){
	                $tmpTaxo->count = $tmpTaxo->count + 1;
	                $tmpTaxo->save();                	
                }
            }
        }
        
        $item->medias()->sync([]);
        if($id != 0){
            $medias = array_reverse($medias);            
        }

        $item->medias()->sync($medias);

        \CatalogGroup::whereParent($item->id)->delete();
        
        if(!empty($groups)){
	        foreach ($groups as $key => $value) {
	        	$group = new \CatalogGroup();
	        	$group->parent = $item->id;
	        	$group->item_id = $value;
	        	$group->save();
	        }        	
        }
        
        \CatalogSearch::rebuildItem($item);

        return redirect()->route('catalog_item_group_edit',[$product_id,$item->id])->with('msg','Data Saved!');

	}

}