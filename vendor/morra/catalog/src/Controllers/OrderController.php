<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;
use CatalogBankTransfer;
use CatalogOrder;
use CatalogOrderDetail;
use CatalogSync;
use CatalogPOS;
use CatalogOrderActivity;
use Logger;
use DB;
use User;
use Taxonomy;

class OrderController extends AlphaController
{
    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_orders';
        $this->layout->active = 'alpha_catalog_orders_main';
    }

    public function cancelPickup($orderId)
    {
         $response = app('App\DHL',[config('alpha.application.dhl.env')])->cancelPickUp($orderId);

         if(!(string)$response->Response->Status->ActionStatus == 'Error')
         {
            $order = CatalogOrder::find($orderId);
            $order->status = 'SHIPMENT';
            $order->save();
         }else{
            $msg = "";
            foreach ($response->Response->Status->Condition as $key => $value) {
                $msg .= $value->ConditionData;
            }

            $msg = str_replace("\n",' ',$msg);
            
            return redirect()->back()->with('special_error_dhl',$msg);
            
         }

         return redirect()->back();
    }

    public function pickup(Request $req)
    {
        $order = CatalogOrder::find($req->order_id);
        if(empty($order)) return json_encode(['status'=>0,'msg'=>"Order not found"]);

        $response =  app('App\DHL',[config('alpha.application.dhl.env')])->pickup($req);

        if((string)$response->Note->ActionNote == 'Success')
        {
            $order->status = 'PICKUP';
            $order->save();

            return json_encode(['status'=>1]);
        }

        $msg = 'Internal Server Error!';

        if((string)$response->Response->Status->ActionStatus == 'Error')
        {
            $msg = "";
            foreach ($response->Response->Status->Condition as $key => $value) {
                $msg .= $value->ConditionData;
            }
        }

        return json_encode(['status'=>0,'msg'=>$msg]);

    }

    public function reqShipment(Request $req)
    {
        $order = CatalogOrder::find($req->order_id);
        if(empty($order)) return json_encode(['status'=>0]);

        $response = app('App\DHL',[config('alpha.application.dhl.env')])->validateShipment($req->order_id,'Y',$req->ship_date);
        if(isset($response->Response->Status->ActionStatus))
        {
            return json_encode(['status'=>0,'msg'=>$response->Response->Status->Condition->ConditionData,'url'=>asset('uploads/pdf/dhl-'.$req->order_id.'.pdf')]);
        }else{
            $order->status = 'SHIPMENT';
            $order->save();
            
        }
        
        return json_encode(['status'=>1,'url'=>asset('uploads/pdf/dhl-'.$req->order_id.'.pdf')]);
    }

    public function settlement(Request $req){
        $order = CatalogOrder::find($req->id);
        $settlement = 0;
        if($req->status == "true") $settlement = 1;
        $order->settlement = $settlement;
        $order->save();

        return json_encode(array('status'=>1));
    }

    public function printPacking($id = 0)
    {
        $this->layout = "";

        $storeData = [];
        $store = [];
        $order = CatalogOrder::with(['user','user.metas','details','details.item','user.billingAddress','user.shippingAddress','user.shippingAddress.jne'])->find($id);
        $carrierName = CatalogPOS::getShippingMethodFromOrder($order);
        
        if($order->store != 1300)
        {
            $store = Taxonomy::find($order->store);            
        }
        
        if(!empty($store))
        {
            $storeData = json_decode($store->store_data);
        }

        if(!empty($order->dropshipper_name) && !empty($order->dropshipper_phone))
        {
            $store = (object)[
                'name' => $order->dropshipper_name
            ];

            $storeData = (object)[
                'phone' => $order->dropshipper_phone
            ];
        }


        if(empty($order)) abort(404);
        $order = app('Morra\Catalog\Controllers\PosController')->parseOrder($order);

        return  view('catalog::admin.pages.print.packing',[
                'order' => $order,
                'carrierName' => $carrierName,
                'store' => $store,
                'storeData' => $storeData
            ]);
    }

    public function printInvoice($id = 0)
    {
        $this->layout = "";

        $order = CatalogOrder::with(['user','user.metas','details','details.item','user.billingAddress','user.shippingAddress','user.shippingAddress.jne'])->find($id);
        $carrierName = CatalogPOS::getShippingMethodFromOrder($order);
        if(empty($order)) abort(404);
        $order = app('Morra\Catalog\Controllers\PosController')->parseOrder($order);
        
        return  view('catalog::admin.pages.print.invoice',[
                'order' => $order,
                'carrierName' => $carrierName
            ]);
    }


    public function exportData()
    {
        $this->layout = "";
        $filename = "orders-".date(app('AlphaSetting')->getSetting('date_format').' '.app('AlphaSetting')->getSetting('time_format')).".csv";
        $fp = fopen('php://output', 'w');

        $addressFields = ['first_name','last_name','address','phone','city','district','province','postcode'];
        $select = ['order_number','payment_method','carrier_id','order_date','source','shipping_amount','global_discount','subtotal_after_discount','grand_total'];

        foreach ($addressFields as $key => $value) {
           $select[] = 'payment_'.$value;
        }
        foreach ($addressFields as $key => $value) {
            $select[] = 'shipping_'.$value;
        }

        $orders = CatalogOrder::select($select)->get()->toArray();        

       foreach ($select as $key => $value) {
           $header[] = strtoupper(str_replace('_', ' ', $value));
       }
       
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp, $header);

        foreach ($orders as $key => $value) {
            fputcsv($fp, $value);
        }
        exit;
    }


	public function getData(Request $req){
		//container data
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');
        //order
        $order = $req->input('order');
        //categgory
        $isSearch = false;
        $selectedCategory = '';
       
        $base = new CatalogOrder();            
       
        $recordsTotal = $base;
        $recordsTotal = $recordsTotal->count();

        //filter entry
        $orders = $base;


        if(!empty($search['value']))
        {
            $isSearch = true;
            $orders = $orders->where(function($q)use($search){
                $q->where('catalog_orders.order_number','like','%'.$search['value'].'%');
                $q->orWhere('catalog_orders.refnumber','like','%'.$search['value'].'%');
                $q->orWhere('catalog_orders.payment_first_name','like','%'.$search['value'].'%');
                $q->orWhere('catalog_orders.payment_last_name','like','%'.$search['value'].'%');
                $q->orWhere('catalog_orders.shipping_first_name','like','%'.$search['value'].'%');
                $q->orWhere('catalog_orders.shipping_last_name','like','%'.$search['value'].'%');

            });
        }


        $reqCols = $req->input('columns');
        if(!empty($reqCols[5]['search']['value']))
        {
            $orders = $orders->whereStatus($reqCols[5]['search']['value']);
        }

        if(!empty($reqCols[2]['search']['value']))
        {
            $orders = $orders->whereSource($reqCols[2]['search']['value']);
        }

        if(!empty($reqCols[3]['search']['value']))
        {

            $defaultTime = ['today','last_7_days','last_30_days'];
            if(in_array($reqCols[3]['search']['value'], $defaultTime)){
                if($reqCols[3]['search']['value'] == 'today')
                {
                    $orders = $orders->where(DB::raw("DATE_FORMAT(order_date,'%Y-%m-%d')"),'=',date('Y-m-d'));
                }

                if($reqCols[3]['search']['value'] == 'last_7_days')
                {
                    $orders = $orders->whereBetween('order_date',[date('Y-m-d H:i:s',strtotime('- 7 days')),date('Y-m-d H:i:s')]);
                }

                if($reqCols[3]['search']['value'] == 'last_30_days')
                {
                    $orders = $orders->whereBetween('order_date',[date('Y-m-d H:i:s',strtotime('- 30 days')),date('Y-m-d H:i:s')]);
                }

            }else{
                $dates = explode(' - ', $reqCols[3]['search']['value']);
                $startDate = date('Y-m-d H:i:s',strtotime($dates[0]));
                $endDate = date('Y-m-d H:i:s',strtotime($dates[1]));
                $orders = $orders->whereBetween('order_date',array($startDate,$endDate));
            }
            
        }



         //total entry yg kefilter
        $recordsFiltered = $orders->count();
        

       
        $orders = $orders->take($take)->skip($start);
        //sorting
        $cols = ['order_number','grand_total','source','order_date','updated_at','settlement','status'];
        
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $orders = $orders->orderBy($col,$order[0]['dir']);
            }else{
                $orders = $orders->orderBy('catalog_orders.order_date','desc');
            }
        }else{
            $orders = $orders->orderBy('catalog_orders.order_date','desc');
        }
        

        $orders = $orders->get();


        $cols = ['order_number','grand_total','source','order_date','updated_at','settlement','status','action'];
        $view = 'catalog::admin.pages.order.templates.grid';
        $base = [];
        foreach ($cols as $col) {
            $base[$col] = '';
        }
        
        foreach ($orders as $key => $value) {
            $item = $base;
            $html = '';
            $html .= view($view,['order'=>$value]);
            $item['html'] = $html;
            $data[] = $item;
        }


        Logger::info("View Orders");

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);

	}

	public function index(Request $req){
		$this->layout->title = setPageTitle("Orders");
		$this->layout->content = view('catalog::admin.pages.order.index');
	}

    public function saveShippingInfo(Request $req)
    {
        $order = CatalogOrder::with('details')->find($req->order_id);
        if(empty($order)) return json_encode(['status'=>0]);
        $order->shipping_date = date('Y-m-d H:i:s',strtotime($req->shipping_date));
        $order->real_shipping_amount =  $req->shipping_amount;
        $order->status = 'SHIPPED'; 
        $order->save();

        $shippingMethod = CatalogPOS::getShippingMethodFromOrder($order);      
        CatalogOrderActivity::addActivity($order->id,'SHIPPED',"Order shipped via ".$shippingMethod." (Tracking # ".$order->tracking_number.")");     

        $user = User::with('metas')->find($order->user_id);
        if(!empty($user))
        {
            if(class_exists('App\Http\Controllers\CheckoutController'))
            {
                app('App\Http\Controllers\CheckoutController')->sendEmailOrder($order->id,'order-shipped');
            }                                
        }


        return json_encode(['status'=>1]);
    }

    public function confirmPayment(Request $req)
    {
        $order = CatalogOrder::with('details')->find($req->order_id);
        if(empty($order)) return json_encode(['status'=>0]);
        $prevPayment = 0;

        $order->refnumber = $req->refno;
        
        if($order->payment_method == 1){
            $bank = CatalogBankTransfer::whereOrderId($order->id)->first();
            if(!empty($bank)){
                $prevPayment = $bank->amount;
            }else{
                $bank = new CatalogBankTransfer();
            }
            $bank->order_id = $order->id;
            $bank->amount = $req->payment_amount + $prevPayment;
            $bank->refnumber = $req->refno;
            $bank->payment_date = date('Y-m-d H:i:s',strtotime($req->payment_date));
            $bank->save();
         }

         
            if(($req->payment_amount + $prevPayment) >= $order->grand_total){
                $order->status = 'APPROVED';
                if($order->payment_method == 1){
                    $order->settlement = 1;
                }

                $user = User::with('metas')->find($order->user_id);
                if(!empty($user))
                {
                    if(class_exists('App\Http\Controllers\CheckoutController'))
                    {
                       app('App\Http\Controllers\CheckoutController')->sendEmailOrder($order->id,'payment-confirmation');
                    }                    
                }
            }



        $order->save();            
        $info = "Payment received (IDR ".number_format($req->payment_amount).")";
        if(!empty($req->refno)) $info .= 'Ref. No('.$req->refno.')';
        CatalogOrderActivity::addActivity($order->id,'APPROVED',$info);

        return json_encode(['status'=>1]);
    }

    public function cancelOrder(Request $req)
    {

        $order = CatalogOrder::with('details')->find($req->order_id);
        if(empty($order)) abort(404);

        $idsProduct = [];
        $orderDetails = CatalogOrderDetail::whereOrderId($order->id)->get();
        foreach ($orderDetails as $key => $value) {
            $item = \CatalogItem::find($value->item_id);
            $item->stock_available = $item->stock_available + $value->qty;
            $item->save();
            $idsProduct[] = $item->product_id;
        }
        
        $order->settlement = 0;
        $order->cancel_date = date('Y-m-d H:i:s');
        $order->cancel_reason = $req->cancel_reason;
        $order->status = 'CANCELLED';
        $order->save();

        $user = User::with('metas')->find($order->user_id);

        if(!empty($user)){
            if(class_exists('App\Http\Controllers\CheckoutController'))
            {
                app('App\Http\Controllers\CheckoutController')->sendEmailOrder($order->id,'order-cancelation');
            }                    
       }


        CatalogSync::doSyncProducts($idsProduct);

       return json_encode(['status'=>1]);
    }

	public function changeOrderStatus($id = 0,$status = 'PENDING')
	{
		$statuses = CatalogPOS::getOrderStatus();
		if(!in_array($status, $statuses)) abort(404);

		$order = CatalogOrder::find($id);
		if(empty($order)) abort(404);
       
        if($status == 'CANCELLED')
        {
            $this->cancelOrder($id,$status);            
        }
		$order->status = $status;
		$order->save();

        CatalogOrderActivity::addActivity($order->id,$status);     


		return redirect()->back()->with('msg','Data Saved!');
	}

	public function detail($id = 0){
		if($id == 0) abort(404);

		$order = CatalogOrder::with(['user','user.metas','details','details.item','user.billingAddress','user.shippingAddress','user.shippingAddress.jne'])->find($id);
		if(empty($order)) abort(404);
		$this->layout->title = setPageTitle("View Order #".$order->order_number);

		$paymentMethod = CatalogPOS::getPaymentMethodFromOrder($order);
		$source = CatalogPOS::getSourceFromOrder($order);
		$shippingMethod = CatalogPOS::getShippingMethodFromOrder($order);

        $log = CatalogOrderActivity::with(['user'])->whereOrderId($order->id)->orderBy('created_at','asc')->get();
        $order = app('Morra\Catalog\Controllers\PosController')->parseOrder($order);
        
		
		$this->layout->content = view('catalog::admin.pages.order.detail',[
			'order'=>$order,
			'paymentMethod'=>$paymentMethod,
			'source' => $source,
			'shippingMethod' => $shippingMethod,
            'log' => $log
			]);

	}
}