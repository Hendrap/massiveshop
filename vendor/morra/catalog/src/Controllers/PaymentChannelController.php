<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;
use Logger;
use Entry;
use CatalogPaymentChannel;
use CatalogAccount;

class PaymentChannelController extends AlphaController
{
    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_setting';
        $this->layout->active = 'alpha_catalog_payment_method';

    }


    public function index()
    {
        $accounts = CatalogAccount::get();
        $payments = CatalogPaymentChannel::get();
        $this->layout->title = setPageTitle("Payment Methods");
        $this->layout->content = view('catalog::admin.pages.payment.index',[
                'accounts' => $accounts,
                'payments' => $payments
            ]);
    }

    public function saveData(Request $req)
    {
        $payment = CatalogPaymentChannel::find($req->id);
        if(empty($payment)) $payment = new CatalogPaymentChannel();
        $payment->name = $req->name;
        $payment->description = $req->description;
        $payment->save();


        $html = '';
        $html .= view('catalog::admin.pages.payment.grid',['payment'=>$payment]);


        return json_encode(array('status'=>1,'payment'=>$payment,'html'=>$html));
    }   

    public function deleteData(Request $req)
    {
        $payment = CatalogPaymentChannel::find($req->id);
        $payment->delete();

        return redirect()->back();
    }



 }