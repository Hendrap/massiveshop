<?php
/*
 *
 * @author CadisEtramaDiRaizel (Luthfifs97@gmail.com)
 * 
 * 
*/
namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Config;
use Alpha\Core\AlphaController;
use Morra\Catalog\Core\Shipping\Jne;
use Morra\Catalog\Models\Jne as JneModel;
use CatalogAddress;
use CatalogBankTransfer;
use CatalogItem;
use CatalogOrder;
use CatalogOrderDetail;
use DB;
use Taxonomy;
use User;
use Usermeta;
use CatalogOrderActivity;
use CatalogPOS;
use CatalogMail;
use CatalogSync;

class PosController extends AlphaController
{
	function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_orders';
        $this->layout->active = 'alpha_catalog_orders_main';
    }

    public function getDHLQUote(Request $req)
    {
    	

		$qty = [];
		$ids = [];

		foreach ($req->ids as $key => $value) {
		    $qty[$value['id']] = $value['qty'];
		    $ids[] = $value['id'];
		}

		$items = CatalogItem::with(['entry','entry.metas'])->whereIn('id',$ids)->get();    	

		$parsedItems = [];

		$total = 0;

        foreach ($items as $key => $value) {
        	$total += $value->price;

            for ($i=0; $i < $qty[$value->id]; $i++) { 

            	$shipping_weight = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.weight');
				if($shipping_weight <= 0) $shipping_weight = 1;

				$shipping_width = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.width');
				if($shipping_width <= 0) $shipping_width = 1;
				
				$shipping_height = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.height');
				if($shipping_height <= 0) $shipping_height = 1;

				$shipping_depth = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.depth');
				if($shipping_depth <= 0) $shipping_depth = 1;

                $parsedItems[] = [
                    'height' => $shipping_height,
                    'depth' => $shipping_depth,
                    'width' => $shipping_width,
                    'weight' => $shipping_weight,
                    'iten_id' => $value->id
                ];
            }
        }


    	$availableShippingMethods = config('catalog.shipping');

    	$res = app('App\DHL')->getQuote(
                    app('App\Http\Controllers\CheckoutController')->generateOriginAddressDHLFormat(),
                    app('App\Http\Controllers\CheckoutController')->generateTargetAddressDHLFormat(
                    	$req->postal_code,
                    	$req->city,
                    	$req->country
                    ),
                    $parsedItems,
                    $total

              );

		if(!empty($res['shippingMethods']))
		{
			foreach ($res['shippingMethods'] as $key => $value) {
				foreach ($availableShippingMethods as $k => $v) {
					if($v->id == $value->id)
					{
						$availableShippingMethods[$k]->value = $value->ShippingCharge;
					}
				}
			}
		}

		return json_encode(['status' => 1,'shippingMethods' => $availableShippingMethods]);


    }

    public function parseItemsToDHLFormat($items = array())
    {
    	$parsedItems = [];
        foreach ($items as $key => $value) {
            for ($i=0; $i < $value->qty; $i++) { 
                $parsedItems[] = [
                    'height' => round($value->shipping_height,2),
                    'depth' => round($value->shipping_depth,2),
                    'width' => round($value->shipping_width,2),
                    'weight' => round($value->shipping_weight,2),
                    'item_id' => $value->item_id
                ];
            }
        }

        return $parsedItems;
    }

	public function index(Request $req,$id = 0){
		$order = ['id'=>$id];
		$availableShippingMethods = config('catalog.shipping');
		
		setCurrency('idr');

		if($id > 0){
			$order = CatalogOrder::with(['user','shipping','billing','user.metas','details','details.item','user.billingAddress','user.shippingAddress','user.shippingAddress.jne'])->where('status','!=','CANCELLED')->find($id);
			if(empty($order)) abort(404);


			foreach ($availableShippingMethods as $key => $value) {
				if($value->id == $order->carrier_id)
				{
					$availableShippingMethods[$key]->value = $order->shipping_amount;
				}
			}


			 $res = app('App\DHL')->getQuote(
                    app('App\Http\Controllers\CheckoutController')->generateOriginAddressDHLFormat(),
                    app('App\Http\Controllers\CheckoutController')->generateTargetAddressDHLFormat(
                    	$order->shipping->postal_code,
                    	$order->shipping->city,
                    	$order->shipping->country
                    ),
                    $this->parseItemsToDHLFormat($order->details),
                    $order->subtotal_after_discount
              );

			if(!empty($res['shippingMethods']))
			{
				foreach ($res['shippingMethods'] as $key => $value) {
					if($value->id != $order->carrier_id)
					{
						foreach ($availableShippingMethods as $k => $v) {
							if($v->id == $value->id)
							{
								$availableShippingMethods[$k]->value = $value->ShippingCharge;
							}
						}
					}
				}
			}




			$this->layout->title = setPageTitle("Order #".$order->order_number);
			$order = $this->parseOrder($order);
		}else{
			$this->layout->title = setPageTitle("New Order");
		}

		$countries = DB::table('countries')->get();

		$pageTitle = setPageTitle("[[title]]");
		$this->layout->content = view('catalog::admin.pages.pos.view',[
			'availableShippingMethods' => $availableShippingMethods,
			'countries' => $countries,
			'order' => $order,
			'pageTitle' => $pageTitle
		]);
		
	}


	public function getUser(Request $req)
	{
		$data = [];

		$q = $req->q;

		$users = User::with(['metas','billingAddress','shippingAddress','shippingAddress.jne'])->select(array('users.id','users.email','users.username'))
		->join('usermetas',function($j){
		
			$j->on('usermetas.user_id','=','users.id');
		
		})->where(function($w){
			$w->where('meta_key','=','first_name');
			$w->orWhere('meta_key','=','last_name');
			$w->orWhere('meta_key','=','phone');
		})->where(function($w) use($q){
			$w->where('username','like','%'.$q.'%');
			$w->orWhere('email','like','%'.$q.'%');
			$w->orWhere('meta_value_text','like','%'.$q.'%');
		})->where(function($w){
			$w->where('status','!=','converted');
			$w->where('status','!=','deleted');
		})->take(30)->groupBy('user_id')->orderBy('email','asc')->get();

		foreach ($users as $key => $value) {
			$data[] = $this->parseUser($value);
		}
		return json_encode($data);
	}

	public function parseUser($value)
	{
		$billingAddress = $value->billingAddress;
		$shippingAddress = $value->shippingAddress;

		foreach ($billingAddress as $key => $address) {
			$billingAddress[$key]->districtCity = $address->district.', '.$address->city;
		}
		foreach ($shippingAddress as $key => $address) {
			$shippingAddress[$key]->districtCity = $address->district.', '.$address->city;
		}
		return [
					'id' => $value->id,
					'email' => $value->email,
					'day' => (int)getEntryMetaFromArray($value->metas,'day'),
					'month' => (int)getEntryMetaFromArray($value->metas,'month'),
					'year' => (int)getEntryMetaFromArray($value->metas,'year'),
					'billing_address' => $billingAddress,
					'shipping_address' => $shippingAddress,
					'first_name' => getEntryMetaFromArray($value->metas,'first_name'),
					'last_name' => getEntryMetaFromArray($value->metas,'last_name'),
					'phone' => getEntryMetaFromArray($value->metas,'phone'),
					'gender' => getEntryMetaFromArray($value->metas,'gender'),
					'address' => getEntryMetaFromArray($value->metas,'address'),
					'name' => getEntryMetaFromArray($value->metas,'first_name').' '.getEntryMetaFromArray($value->metas,'last_name'),
			];


	}

	public function validateDuplicateEmail($email = '')
	{
		$user = User::whereEmail($email)->first();

		return count($user);
	}

	public function saveUser(Request $req)
	{
		$this->validate($req,[
				'first_name' => 'required',
				'phone' => 'required|numeric'
			]);


		

		$user = User::find((int)$req->id);


		if(!empty($req->email))
		{
			if(!empty($user))
			{
				if($user->email != $req->email){
					if(!$this->validateDuplicateEmail($req->email)){
						return json_encode(array('status'=>0,'msg'=>'Duplicate Email Address!'));
					}
				}
			}

			if(empty($user))
			{

				if($this->validateDuplicateEmail($req->email) > 0){
					return json_encode(array('status'=>0,'msg'=>'Duplicate Email Address!'));
				}
			}
			
		}

		$isNew = false;
		$isSendEmail = false;

		if(empty($user)){
			$user = new User();
			$isNew = true;
		}
			


		$user->email = $req->email;
		if(empty($req->email))
		{
			$tmpName = Str::slug($req->phone);
			$countUser = User::where('email','like',$tmpName.'%')->count();
			
			if($countUser > 0){
				$tmpName .= '-'.$countUser;
			}

			$tmpName .= '@pos-customer.popitoi.com';
			$user->email = $tmpName;
			$user->type = 'customer-pos';
		}else{
			if($isNew == true) $isSendEmail = true;
		}

		if(empty($user->status)){
			$user->status = 'disabled';
		}

		$user->save();
		$metas = ['first_name'=>'first_name','last_name'=>'last_name','address'=>'address','phone'=>'phone','day'=>'day','month'=>'month','year'=>'year','gender'=>'gender'];

		$deleted = Usermeta::where(function($w) use($metas){
			foreach ($metas as $key => $value) {
				if($key == 'first_name'){
					$w->where('meta_key','=',$value);
				}else{
					$w->orWhere('meta_key','=',$value);
				}
			}
		})->whereUserId($user->id)->delete();

		$insertToMeta = [];
		foreach ($metas as $key => $value) {
			$insertToMeta[] = [
				'user_id' => $user->id,
				'meta_key' => $value,
				'meta_value_text' => $req->{$key}
			];
		}

		
		if(!empty($insertToMeta))
		{
			DB::table('usermetas')->insert($insertToMeta);
		}

		if($isSendEmail == true){
			CatalogMail::setEmail([$user->email])->sendEmailUserWellcome(base64_encode($user->email),$req->first_name.' '.$req->last_name);
		}

		$user = User::with(['metas','billingAddress','shippingAddress'])->find($user->id);
		$user = $this->parseUser($user);
		return json_encode(array('status'=>1,'user'=>$user));
	}

	public function cloneAddress($fields,$obj,$type)
	{
		$address = new CatalogAddress();
		foreach ($fields as $key => $value) {
			$address->{$value} = $obj->{$value};
		}
		$address->type = $type;
		$address->save();

		return $address;
	}

	public function saveAddress(Request $req)
	{
		$this->validate($req,[
				'address' => 'required',
				'phone' => 'numeric'
			]);

		$fields = ['label','country','type','email','user_id','first_name','last_name','address','address_2','phone','district','city','province','postal_code'];
		$address = CatalogAddress::find($req->id);
		if(empty($address)) $address = new CatalogAddress();

		foreach ($fields as $key => $value) {
			$address->{$value} = $req->{$value};
		}

		$districtCity = explode(', ', $req->districtCity);
		$address->district = @$districtCity[0];
		$address->city = @$districtCity[1];
		$jne = [];
		if($req->type == 'shipping')
		{
			$tmpJne = JneModel::whereRegency($address->city)->whereDistrict($address->district)->first();
			if(!empty($tmpJne)){
				$jne = $tmpJne;
				$address->jne_id = $jne->id;
			}
		}
		$address->save();
		

		$billingAddress = [];

		if($req->id == 0 && $req->type == 'shipping')
		{
			$fields[] = 'jne_id';
			$billingAddress = $this->cloneAddress($fields,$address,'billing');
		}

		$address = CatalogAddress::with(['jne'])->find($address->id);
		$address->districtCity = $req->districtCity;

		return json_encode(array('status'=>1,'address'=>$address,'jne'=>$jne,'billingAddress'=>$billingAddress));
	}

	public function getCityDistrict(Request $req)
	{
		$search = $req->term;
		$list = JneModel::where(function($q)use($search){
			$q->where('district','like','%'.$search.'%');
			$q->orWhere('regency','like','%'.$search.'%');
		})->take(30)->get();

		$data = [];
		foreach ($list as $key => $value) {
			$data[] = $value->district.', '.$value->regency;
		}

		return json_encode($data);	
	}


	public function getCity(Request $req)
    {
        $data = [];
        $jne = new Jne();
        $cities = $jne->getCity($req->term);
        foreach ($cities as $key => $value) {
            $data[] = $value->regency;
        }

        return json_encode($data);
    }

    public function getDistrict(Request $req)
    {
        $data = [];
        $jne = new Jne();
        $cities = $jne->getDistrict($req->city,$req->term,false);
        foreach ($cities as $key => $value) {
            $data[] = $value->district;
        }

        return json_encode($data);
    }

	public function getShippingMethods(Request $req)
	{
		$couriers = [];
        $couriersDB = JneModel::where('regency','like','%'.$req->city.'%')->orWhere('district','like','%'.$req->district.'%')->first();

        if(!empty($couriersDB)){
            $types = ['reg','oke','yes'];
            foreach ($types as $key => $value) {
                 $couriers[] = (object)['value'=>'jne_'.$value,'amount' => $couriersDB->{'jne_'.$value}];
            }
        }
        return json_encode(array('status'=>1,'couriers'=>$couriers));
	}

	public function searchProducts(Request $req)
	{
		$q = $req->q;
		$data = [];

		$items = CatalogItem::with(['entry','entry.medias','entry.metas','entry.brand'])->join('entries',function($join){
			 $join->on('catalog_items.product_id', '=', 'entries.id');
		})->where(function($w) use($q){
			$words = explode(' ', $q);
			foreach ($words as $key => $value) {
				$w->where('entries.title','like','%'.$value.'%');					
			}
			
		})->where('entries.status','!=','deleted')->where('catalog_items.stock_available','>',0)
		->where(function($w){
			$w->where('price','>',0);
			$w->orWhere('sale_price','>',0);
		})->take(30)->select('catalog_items.*')->groupBy('catalog_items.id')->get();

		foreach ($items as $key => $value) {

			$price = str_replace(',','',number_format(castingPrice($value)));
			$weight = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.weight');

			$shipping_weight = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.weight');
			if($shipping_weight <= 0) $shipping_weight = 1;

			$shipping_width = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.width');
			if($shipping_width <= 0) $shipping_width = 1;
				
			$shipping_height = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.height');
			if($shipping_height <= 0) $shipping_height = 1;

			$shipping_depth = (float)getEntryMetaFromArray(@$value->entry->metas,'shipping_info.depth');
			if($shipping_depth <= 0) $shipping_depth = 1;


			$data[] = [
				'shipping_weight' => $shipping_weight,
				'shipping_width' => $shipping_width,
				'shipping_height' => $shipping_height,
				'shipping_depth' => $shipping_depth,
				'item_id' => $value->id,
				'product_id' => $value->entry->id,
				'product_title' => parseMultiLang($value->entry->title),
				'item_title' => $value->item_title,
				'sku' => $value->sku,
				'brand_name' => parseMultiLang(@$value->entry->brand->title),
				'price' => $price,
				'stock_available' => $value->stock_available,
				'stock_qty' => 1,
				'discount' => 0,
				'image_thumbnail' => asset(getCropImage(@$value->entry->medias[0]->path,'default')),
				'image_original' => asset(@$value->entry->medias[0]->path),
				'total' => $price,
				'weight' => $weight,
				'original_price' => $value->price
			];
		}

		return json_encode(array('status'=>1,'items'=>$data));

	}

	public function getItemFields()
	{
		$itemFields = ['shipping_depth','shipping_weight','shipping_height','shipping_width','weight','image_original','image_thumbnail','brand_name','item_id','product_id','product_title','item_title','sku','original_price'];
		return $itemFields;
	}

	public function getOrderFields()
	{
		$orderFields = ['gift_wrapping','refnumber','shipping_amount','customer_note','billing_id','shipping_id','carrier_id','dropshipper_name','dropshipper_phone'];
		return $orderFields;
	}

	public function parseMoney($number)
	{
		return str_replace(',','',number_format($number));
	}

	public function parseOrder($order)
	{

		$products = [];
		$totalWeight = 0;
		$itemFields = $this->getItemFields();
		foreach ($order->details as $key => $value) {
			$tmp = [];
			foreach ($itemFields as $col) {
				$tmp[$col] = $value->{$col};
			}
			$tmp['original_price'] = ($value->original_price);
			$tmp['stock_qty'] = $value->qty;
			$tmp['stock_available'] = $value->qty + (int)@$value->item->stock_available;
			$tmp['discount'] = ($value->discount);
			$tmp['total'] = ($value->total);
			$totalWeight += ($value->weight * $value->qty);
			$products[] = $tmp;
		}

		$user = $this->parseUser($order->user);

		$parsedOrder = [];
		$orderFields = $this->getOrderFields();
		foreach ($orderFields as $col) {
			$parsedOrder[$col] = $order->{$col};
		}

		$moneyFields = ['shipping_amount','global_discount','grand_total','subtotal','subtotal_after_discount'];
		foreach ($moneyFields as $col) {
			$parsedOrder[$col] = ($order->{$col});
		}

		$parsedOrder['id'] = $order->id;
		$parsedOrder['order_number'] = $order->order_number;
		$parsedOrder['carrier_id'] = $order->carrier_id;
		$parsedOrder['source'] = $order->source;
		$parsedOrder['store'] = $order->store;
		$parsedOrder['order_date'] = date(app('AlphaSetting')->getSetting('date_format').' '.app('AlphaSetting')->getSetting('time_format'),strtotime($order->order_date));
		$parsedOrder['payment_method'] = $order->payment_method;
		$parsedOrder['currency'] = $order->currency;
		$parsedOrder['rate_usd'] = $order->rate_usd;
		$parsedOrder['rate_eur'] = $order->rate_eur;

		$billing = CatalogAddress::with(['jne'])->find($order->billing_id);
		$shipping = CatalogAddress::with(['jne'])->find($order->shipping_id);
			


		return ['status'=>$order->status,'totalWeight'=>$totalWeight,'id'=>$order->id,'billing'=>$billing,'shipping'=>$shipping,'order'=>$parsedOrder,'products'=>$products,'user'=>$user];


	}


	public function placeOrder(Request $request)
	{
		 $addressFields = ['first_name','last_name','address','phone','city','district','province','postal_code','country'];
		 $itemFields = $this->getItemFields();
		 $orderFields = $this->getOrderFields();

		 $data = $request->all();

		 $req = (object)[];
		 $req->order = (object)$data['order'];
		 $req->shippingAddress  = (object)$data['shippingAddress'];
		 $req->billingAddress  = (object)$data['billingAddress'];
		 $req->user  = (object)$data['user'];
		 $req->products = json_decode(json_encode($data['products']));

		 $isNew = false;

		 try {
		 	 DB::beginTransaction();
		 	 $allDiscount = 0;
			 $originalSubTotal = 0;
			 $subTotal = 0;
			 $allDiscount += $req->order->total_discount;		 
			 foreach ($req->products as $key => $value) {
			 	$originalSubTotal +=  ($value->stock_qty * $value->original_price);
			 	$subTotal += (($value->stock_qty * $value->original_price) - $value->discount);
			 	$allDiscount += $value->discount;
			 }
			 $order = CatalogOrder::find($req->order->id);
			 if(empty($order))
			 {
			 	$isNew = true;
			 	$order = new CatalogOrder();
			 	$order->order_number = getOrderNumber();
			 	$order->save();
			 }

			 if(empty($order->status)) $order->status = 'PENDING';

			 foreach ($orderFields as $key => $value) {
			 	$order->{$value} = $req->order->{$value};
			 }
			 $order->user_id = $req->user->id;
			 $order->payment_method = $req->order->paymentMethod;
			 foreach ($addressFields as $key => $value) {
		    	$order->{'payment_'.$value} = $req->billingAddress->{$value};
		    	$order->{'shipping_'.$value} = $req->shippingAddress->{$value};
		     }
		     if(Str::startsWith($req->order->carrier_id,'jne_')){
	            $level = explode('_', $req->order->carrier_id);
	            $order->shipping_method = 'jne';
	            $order->carrier_name ='Jne';
	            $order->carrier_level = ucfirst($level[1]);
	      	 }

				$order->subtotal_before_discount = $originalSubTotal;
				$order->global_and_item_discount = $allDiscount;
				$order->global_discount = $req->order->total_discount;
				$order->subtotal_after_discount = $subTotal;
				$selectedShipping = $req->order->shipping_amount;
				$order->grand_total = ($originalSubTotal + $selectedShipping) - $allDiscount;
				$order->order_date = date('Y-m-d H:i:s',strtotime($req->order->order_date));
				$order->source = $req->order->source;
				$order->store = $req->order->store;
				$order->currency = getCurrency();
				$order->save();

				$oldItemsId = array();
				$newItemsId = array();
				$orderDetails = CatalogOrderDetail::whereOrderId($order->id)->get();
					
				foreach ($orderDetails as $key => $value) {
					$oldItemsId[] = $value->item_id;
				}
					
				foreach ($req->products as $key => $value) {
					$jsonItems[$value->item_id] = $value;
					$newItemsId[] = $value->item_id;
				}

				$products = [];

				$willBeDeleted = array_diff($oldItemsId, $newItemsId);
				if(!empty($willBeDeleted)){
					$tmpOrderDetails = CatalogOrderDetail::whereOrderId($order->id)->whereIn('item_id',$willBeDeleted)->get();
					if(!empty($tmpOrderDetails)){
						foreach ($tmpOrderDetails as $key => $value) {
							$dbItem = CatalogItem::whereId($value->item_id)->first();
							$dbItem->stock_available = $dbItem->stock_available + $value->qty;
							$dbItem->stock_status = 1;
							if($dbItem->stock_available == 0)
							{
								$dbItem->stock_status = 0;
							}
							$dbItem->save();
							$deletedOrderDetail = CatalogOrderDetail::whereOrderId($order->id)->whereId($value->id)->delete();
							$products[] = $dbItem->product_id;
						}
					}
				} 

				$willbeCreated = array_diff($newItemsId,$oldItemsId);
				if(!empty($willbeCreated)){
					foreach ($willbeCreated as $key => $value) {
						
						$tmpItem = $jsonItems[$value];
						$dbItem = CatalogItem::find($tmpItem->item_id);
						$dbItem->stock_available = $dbItem->stock_available - $tmpItem->stock_qty;
						$dbItem->stock_status = 1;
						if($dbItem->stock_available == 0)
						{
							$dbItem->stock_status = 0;
						}
						$dbItem->save();


						$orderDetail = new CatalogOrderDetail();
						foreach ($itemFields as $col) {
							$orderDetail->{$col} = $tmpItem->{$col};
						}

						$orderDetail->order_id = $order->id;
						$orderDetail->qty = $tmpItem->stock_qty;
						$orderDetail->original_price = $tmpItem->price;
						$orderDetail->discount = $tmpItem->discount;
						$orderDetail->total = $tmpItem->stock_qty * $tmpItem->price - $tmpItem->discount;
						$orderDetail->original_total = $tmpItem->stock_qty * $tmpItem->price;
						$orderDetail->save();
						$products[] = $dbItem->product_id;
					}
				}

				$maybeChangeQty = array_intersect($newItemsId,$oldItemsId);
				if(!empty($maybeChangeQty)){
					$tmpOrderDetails = CatalogOrderDetail::whereOrderId($order->id)->whereIn('item_id',$maybeChangeQty)->get();
					foreach ($tmpOrderDetails as $key => $value) {
						$tmpItem = $jsonItems[$value->item_id];

						$dbItem = CatalogItem::whereId($value->item_id)->first();
						$tmpQtyItem = ($dbItem->stock_available + $value->qty) - $tmpItem->stock_qty;
						$dbItem->stock_available = $tmpQtyItem;
						$dbItem->stock_status = 1;
						if($dbItem->stock_available == 0)
						{
							$dbItem->stock_status = 0;
						}

						$dbItem->save();

						$dbOrderDetail = CatalogOrderDetail::find($value->id);
						$dbOrderDetail->qty = $tmpItem->stock_qty;
						$dbOrderDetail->order_id = $order->id;
						$dbOrderDetail->qty = $tmpItem->stock_qty;
						$dbOrderDetail->original_price = $tmpItem->original_price;
						$dbOrderDetail->discount = $tmpItem->discount;
						$dbOrderDetail->total = $tmpItem->stock_qty * $tmpItem->original_price - $tmpItem->discount;
						$dbOrderDetail->original_total = $tmpItem->stock_qty * $tmpItem->original_price;
						$dbOrderDetail->save();
						
						$products[] = $dbItem->product_id;
					}
				}

				if($isNew == true)
				{
					CatalogOrderActivity::addActivity($order->id,'PENDING',"Order received via ".CatalogPOS::getSourceFromOrder($order));
				}

				DB::commit();
		 } catch (Exception $e) {
		 		 DB::rollBack();
		 }
		 

		CatalogSync::doSyncProducts($products);
		$order = CatalogOrder::with(['user','user.metas','details','details.item','user.billingAddress','user.shippingAddress','user.shippingAddress.jne'])->find($order->id);
		if($isNew == true)
		{
			$user = User::with('metas')->find($order->user_id);
			if(!empty($user))
			{
				if(!Str::contains($user->email,'pos-customer.popitoi.com')){
							//sample buat kirim email
					if(class_exists('App\PopitoiMailGuy'))
					{
						$name = getEntryMetaFromArray($user->metas,'first_name').' '.getEntryMetaFromArray($user->metas,'last_name');
						if(trim($name) == '')
						{
							$name = $user->email;
						}

						$data = (object)['id' => 1];
						$data->order = $order;
						$data->order->original_subtotal = $order->subtotal_before_discount;
						$data->orderDetails = $order->details;

						app('App\PopitoiMailGuy',[$user->email])->sendEmailOrder($data,$name,$order->order_number,$user);
					}
				}
			}
		}
		$response = $this->parseOrder($order);
		$response['status'] = 1;

		return json_encode($response);
//		 return json_encode(['status'=>1]);

	}
}