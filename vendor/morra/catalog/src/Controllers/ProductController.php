<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use CatalogItem;
use Entrymeta;
use Taxonomy;
use DB;
use Logger;

class ProductController extends AlphaController
{
    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_stock';
    }

    public function getData($catalogType = '',Request $req){
        $type = 'catalog_variant_'.$catalogType;
        $taxoInfo = getTaxonomyFromEntry($type);
        
        $data = [];
        //pagination
        $start = $req->input('start',0);
        $take = $req->input('length',10);
        //search field
        $search = $req->input('search');
        //categgory
        $isSearch = false;
        $selectedCategory = '';
        $selectedBrand = '';

        $category = $req->input('columns');
       // dd($category);
        if(!empty($category[8]['search']['value']))
        {
            $selectedCategory = $category[8]['search']['value'];
            //dd($selectedCategory);
        }

        if(!empty($category[7]['search']['value']))
        {
            $selectedBrand = $category[7]['search']['value'];
        }



        //total records
        if(!empty($selectedCategory))
        {
            $taxonomy = Taxonomy::find($selectedCategory);
            //dd($taxonomy);

            if(empty($taxonomy)) return json_encode(['data'=>[],'draw'=>$req->input('draw',1),'recordsTotal'=>0,'recordsFiltered'=>0]);
            $base = $taxonomy->entries();
        }else{
            $base = new \Entry();            
        }

        $base = $base->select(['entries.*',
                               'catalog_items.stock_available',
                               'catalog_items.sku',
                               DB::raw('alp_catalog_items.status as item_status'),
                               DB::raw('alp_catalog_items.id as item_id'),
                               'catalog_items.price',
                               'catalog_items.item_title',
                               'catalog_items.stock_status'
                               ])->join('catalog_items',function($j){
                                    $j->on('catalog_items.product_id','=','entries.id');
                               });

        $recordsTotal = $base->where('entries.status','!=','deleted')->where('catalog_items.status','!=','deleted')->where('entries.entry_type','=',$type);
                           

        $recordsTotal = $recordsTotal->count();
        //filter entry
        $entries = $base->whereEntryType($type)->where('entries.status','!=','deleted');


        if(!empty($search['value']))
        {
            $isSearch = true;
            $entries = $entries->where(function($q)use($search){
                $q->where('entries.title','like','%'.$search['value'].'%');
                $q->orWhere('entries.content','like','%'.$search['value'].'%');
                $q->orWhere('entries.excerpt','like','%'.$search['value'].'%');
            });
        }
        //ambil data

        //entry status
        $isStatus = false;
        $status = $req->input('columns');
        if(!empty($status[5]['search']['value']))
        {
            $status = strtolower($status[5]['search']['value']);
            if($status == 'active')
            {
                $entries = $entries->where('entries.status','=','published')->where('catalog_items.stock_status','=',1);
            }

            if($status == 'out-of-stock')
            {
                $entries = $entries->where('catalog_items.stock_status','=',0);
            }

            if($status == 'disabled')
            {
                $entries = $entries->where('entries.status','=','disabled');
            }

            $isStatus = true;
        }

        if(!empty($selectedBrand))
        {
            $entries = $entries->where('brand_id','=',$selectedBrand);
        }

         //total entry yg kefilter
        $recordsFiltered = $entries->count();

         $with = ['media','medias','brand'];
       
        $entries = $entries->take($take)->skip($start)->with($with);
        //sorting

        $cols = ['','title','catalog_items.stock_available','catalog_items.price','entries.updated_at','entries.status'];
        $order = $req->input('order');
        if(!empty($order[0]))
        {
            $col = @$cols[$order[0]['column']];
            if(!empty($col))
            {
                $entries = $entries->orderBy($col,$order[0]['dir']);
            }else{
                $entries = $entries->orderBy('entries.created_at','desc');
            }
        }else{
            $entries = $entries->orderBy('entries.created_at','desc');
        }
        

        $entries = $entries->get();
        
        $cols = ['title','user','published_at','created_at','updated_at','parent','action'];
        $base = [];
        foreach ($cols as $col) {
            $base[$col] = '';
        }

        //dd($taxoInfo->taxo);
        foreach ($entries as $key => $value) {
            $item = $base;
            $item['entry_parent'] = $value->entry_parent;
            $html = '';
            $html .= view('catalog::admin.pages.product.template.table-row',['taxoInfo'=>$taxoInfo,'entry'=>$value,'catalogType'=>$catalogType,'type'=>$type,'isSearch'=>$isSearch]);
            $item['html'] = $html;

            $data[] = $item;
        }


        Logger::info("View Product : ".$type);

        return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);
    }

    public function getAdditionalInfo($type = ''){

        $metas = getMetaFromEntry($type);

        $taxoInfo = getTaxonomyFromEntry($type);
        $taxoTypes = $taxoInfo->taxoTypes;
        $taxo = $taxoInfo->taxo;
        //dd($taxoTypes);
        $taxonomies = getTaxonomiesForEntry($taxoTypes);
        
        $brands = [];   
        $tmpBrands = \Entry::select(['id','title'])->whereEntryType('brand')->where('status','!=','deleted')->get();
        foreach ($tmpBrands as $key => $value) {
            $brands[] = (object)[
                'id' => $value->id,
                'title' => parseMultiLang($value->title)
            ];
        }
        return (object)['brands'=>$brands,'taxonomies' => $taxonomies,'metas'=>$metas,'taxo'=>$taxo];
    }
    
	public function index($catalogType = '')
	{

        $type = 'catalog_variant_'.$catalogType;
        $info = getEntryConfig($type);
        $entryData = $this->getAdditionalInfo($type);
        $this->layout->active = 'alpha_catalog_type_'.$type;
        $this->layout->title = setPageTitle($info['plural']);
        $this->layout->content = view('catalog::admin.pages.product.index',[
                'type' => $type,
                'info'  => $info,
                'entryData' => $entryData,
                'catalogType'=> $catalogType,
            ]);
	}

	public function create($type = '')
	{
		$entryType = 'catalog_variant_'.$type;

		$config = getEntryConfig($entryType);
        $info = $this->getAdditionalInfo($entryType);
        $parsedTaxonomies = app('Alpha\Controllers\Admin\Entry')->castingTaxonomies($info->taxo,$info->taxonomies);
        $infoTaxoContainer = [];
        foreach ($info->taxo as $key => $value) {
            $infoTaxoContainer[$value['slug']] = $value;
        }

        $itemType = 'item_catalog_variant_'.$type;
        $infoItem = $this->getAdditionalInfo($itemType);
        $parsedTaxonomiesItem = app('Alpha\Controllers\Admin\Entry')->castingTaxonomies($infoItem->taxo,$infoItem->taxonomies);
        $infoTaxoContaineritem = [];
        foreach ($infoItem->taxo as $key => $value) {
            $infoTaxoContaineritem[$value['slug']] = $value;
        }

        $this->layout->active = 'alpha_catalog_type_'.$entryType;
		$this->layout->title = setPageTitle("Create ".$config['single']);
		$this->layout->content = view('catalog::admin.pages.product.create',[
			'config'	=> $config,
			'catalogType' => $type,
			'type'		=> $entryType,
			'brands' => $info->brands,
            'taxo' => $info->taxo,
            'metas' => $info->metas,
            'taxonomies' => $info->taxonomies,
            'config'    => $config,
            'parsedTaxonomies' => $parsedTaxonomies,
            'infoTaxoContainer' => $infoTaxoContainer,
            'itemType' => $itemType,
            'infoItem' => $infoItem,
            'parsedTaxonomiesItem' => $parsedTaxonomiesItem,
            'infoTaxoContaineritem' => $infoTaxoContaineritem
			]);
	}
    public function edit($type = '',$id = 0)
    {
        $entry = \Entry::with(['media','taxonomies','metas','activeItems','activeItems.medias','activeItems.taxonomies'])->find($id);
        if(empty($entry)) abort(404);

        $entryType = 'catalog_variant_'.$type;

        $config = getEntryConfig($entryType);
        $info = $this->getAdditionalInfo($entryType);
        $parsedTaxonomies = app('Alpha\Controllers\Admin\Entry')->castingTaxonomies($info->taxo,$info->taxonomies);
        $infoTaxoContainer = [];
        foreach ($info->taxo as $key => $value) {
            $infoTaxoContainer[$value['slug']] = $value;
        }

        $itemType = 'item_catalog_variant_'.$type;
        $infoItem = $this->getAdditionalInfo($itemType);
        $parsedTaxonomiesItem = app('Alpha\Controllers\Admin\Entry')->castingTaxonomies($infoItem->taxo,$infoItem->taxonomies);
        $infoTaxoContaineritem = [];
        foreach ($infoItem->taxo as $key => $value) {
            $infoTaxoContaineritem[$value['slug']] = $value;
        }

        $selectedTaxo = [];
        if(!empty($entry->taxonomies)){
            foreach ($entry->taxonomies as $key => $value) {
                $selectedTaxo[] = $value->id;
            }
        }


        $this->layout->active = 'alpha_catalog_type_'.$entryType;
        $this->layout->title = setPageTitle("Edit ".$config['single']);
        $this->layout->content = view('catalog::admin.pages.product.edit',[
            'config'    => $config,
            'entry' => $entry,
            'catalogType' => $type,
            'type'      => $entryType,
            'brands' => $info->brands,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
            'selectedTaxo' => $selectedTaxo,
            'parsedTaxonomies' => $parsedTaxonomies,
            'infoTaxoContainer' => $infoTaxoContainer,
            'itemType' => $itemType,
            'infoItem' => $infoItem,
            'parsedTaxonomiesItem' => $parsedTaxonomiesItem,
            'infoTaxoContaineritem' => $infoTaxoContaineritem
            ]);
        
    }  
	public function save($catalogType = '',$id = 0,Request $requests){
        
		$type = 'catalog_variant_'.$catalogType;
		$defaultLang = \Config::get('alpha.application.default_locale');
        
        $this->validate($requests,[
            'title.'.$defaultLang => 'required',
            'status' => 'required'
            ]);

        $input = $requests->input();
       
        $vars = ['title','content','excerpt'];

        foreach ($vars as $key => $value) {
            
            ${'default'.ucfirst($value)} = @$input[$value][$defaultLang];
            ${$value} = '';

            foreach ($input[$value] as $k => $v) {

                $tmp = '';
                $tmp = ${'default'.ucfirst($value)};
                if(!empty($v)) $tmp = $v;
                ${$value} .= '[:'.$k.']'.$tmp;

            }

        }
        
        $isNew = false;

        $entry = \Entry::find($id);
        if(empty($entry)){
            $entry = new \Entry();
            $entry->author = app('AdminUser')->user->id;
            $isNew = true;

        }

        $brand = \Entry::find($requests->brand);
        if(!empty($brand)){
            if($isNew == true)
            {
                
                    $brand->brand_count = $brand->brand_count + 1;
                    $brand->save();
                
            }else{
                    if($entry->brand_id != $requests->brand)
                    {
                        $brand->brand_count = $brand->brand_count + 1;
                        $brand->save();                        
                    }

            }
            
        }

        if($requests->brand != $entry->brand_id){
            $oldBrand = \Entry::find($entry->brand_id);
            if(!empty($oldBrand))
            {
                $oldBrand->brand_count = $oldBrand->brand_count - 1;
                if($oldBrand->brand_count < 0) $oldBrand->brand_count = 0;
                $oldBrand->save();
            }        
        }


        $slug = \Request::input('slug',str_slug($defaultTitle));
        if(empty($slug)) $slug = str_slug($defaultTitle);

        $originalSlug = $slug;
        $check = \Entry::where('id','!=',$id)->where('slug','like',$slug.'%')->count();
        if($check > 0){
            $slug .= '-'.$check;
        }

        if($entry->slug != $slug && $type == 'page'){
            $routeList = getRouteList();
            $locale = \Config::get('alpha.application.default_locale').'/';
            $testRoute = $slug;
            $isMultiLang = false;
            if(count(\Config::get('alpha.application.locales')) > 1)
            {
                $isMultiLang = true;
                $testRoute = $locale.$slug;
            }

            while (in_array($testRoute, $routeList)) {
                $check++;
                $slug = $originalSlug.'-'.$check;
                $testRoute = $slug;
                if($isMultiLang) $testRoute = $locale.$slug;
            }
        }


        $metas = array();
        $tmpMetas = app('AlphaSetting')->entries;
        foreach ($tmpMetas as $key => $value) {
            if($value['slug'] == $type){
                $metas = $value['metas'];
            }
        }
        $entry->title = $title;
        $entry->slug = $slug;
        $entry->content = $content;
        $entry->excerpt = $excerpt;
        $entry->entry_type = $type;
        $entry->brand_id = \Request::input('brand',0);
        $entry->catalog_type = 'variant';
        $entry->modified_by = app('AdminUser')->user->id;
        $entry->status = $input['status'];
        $entry->published_at = date('Y-m-d H:i:s',strtotime($input['published_at']));
        if(empty($input['published_at'])){
            $entry->published_at = date('Y-m-d H:i:s');
        }
        $entry->media_id = (int)$requests->image_id;
        $entry->touch();
        $entry->save();
        $entry->saveSEO(@$input['seo']);
        $entry->saveEntryMetaFromInput($metas,$input); 
        $this->saveProductInfo($requests,$entry->id);
        $oldItems = [];
        if($isNew == false)
        {
            $oldItems = CatalogItem::whereProductId($entry->id)->lists('id');
            $oldItems = $oldItems->toArray();
        }
        $this->saveItems($requests,$entry->id,$oldItems);
        $entry->taxonomies()->sync([]);  
        $entry->taxonomies()->sync(\Request::input('taxonomies',[]));
        $entry->touch();
        $entry->save();



        $entry->medias()->sync([]);
        $medias = \Request::input('media',[]);
        if($id != 0){
            $medias = array_reverse(\Request::input('media',[]));            
        }

        $entry->medias()->sync($medias);

        \CatalogSearch::rebuildProduct($entry);
        
        return redirect()->route('catalog_product_variant_edit',[$catalogType,$entry->id])->with('msg','Data Saved!');
	}
    
    public function saveItems($req,$entryId,$oldItems = array())
    {
        $willBeDeleted = [];
        if(!empty($oldItems) && !empty($req->input('item_id'))){
            $willBeDeleted = array_diff($oldItems, $req->input('item_id'));            
        }

        if(!empty($willBeDeleted))
        {
            foreach ($willBeDeleted as $key => $value) {
                $item = CatalogItem::find($value);
                $item->status = 'deleted';
                $item->save();
            }
        }        
        if($req->input('item_id'))
        {

            foreach ($req->input('item_id') as $key => $value) {
                
                $item = CatalogItem::find($value);
                if(empty($item)){
                    if((int)str_replace(',', '', $req->input('qty')[$key]) == 0 && $req->input('sku')[$key] == "" && (int)str_replace(',', '', $req->input('price')[$key] == 0)) continue;
                    $item = new CatalogItem();
                }
                $item->product_id = $entryId;
                $item->sku = $req->input('sku')[$key];
                $item->item_title = $req->input('item_title')[$key];
                $item->order_limit = $req->input('order_limit')[$key];
                $item->price = str_replace(',', '', $req->input('price')[$key]);
                $item->sale_price = str_replace(',', '', $req->input('sale_price')[$key]);
                $item->msrp = str_replace(',', '', $req->input('msrp')[$key]);
                if($value == 0)
                {
                    $item->stock_available =str_replace(',', '', $req->input('qty')[$key]);
                    $item->stock = str_replace(',', '', $req->input('qty')[$key]);
                }else{
                    $qtyChange = $item->stock - str_replace(',', '', $req->input('qty')[$key]);
                    $item->stock_available = $item->stock_available - $qtyChange;
                    $item->stock = str_replace(',', '', $req->input('qty')[$key]);
                }
                $item->stock_status = 1;
                if($item->stock_available == 0)
                {
                    $item->stock_status = 0;
                }

                $item->status = 'active';
                $item->type = 'variant';
                $item->schedule_sale = 0;
                $item->sale_start_date = '0000-00-00 00:00:00';
                $item->sale_end_date = '0000-00-00 00:00:00';

                if(!empty($req->input('sale_start')[$key]) && $item->sale_price > 0){
                    $item->schedule_sale = 1;
                    $item->sale_start_date = date('Y-m-d H:i:s',strtotime($req->input('sale_start')[$key]));
                    $item->sale_end_date = date('Y-m-d H:i:s',strtotime($req->input('sale_end')[$key]));
                }

                $item->save();
                
                $taxonomies = json_decode($req->input('item_taxonomies')[$key]);
                if(!empty($taxonomies)) $item->taxonomies()->sync($taxonomies);

                $medias = json_decode($req->input('item_medias')[$key]);
                if(!empty($medias)) $item->medias()->sync($medias);

            }
        }
    }


    public function saveProductInfo($req,$entryId)
    {
        $vars = ['product_info','shipping_info'];
        $name = ['Product','Shipping'];
        foreach ($vars as $col => $var) {
            if($req->input($var))
            {
                foreach ($req->input($var) as $key => $value) {
                    $meta = Entrymeta::whereEntryId($entryId)->whereMetaKey($var.'.'.$key)->first();
                    if(empty($meta)) $meta = new Entrymeta();
                    $meta->meta_key = $var.'.'.$key;
                    if($key == "weight")
                        $meta->meta_name = $name[$col].' '.$key.' (kg)';
                    else //untuk dimensi
                        $meta->meta_name = $name[$col].' '.$key.' (cm)';
                    $meta->meta_value_text = $value;
                    $meta->entry_id = $entryId;
                    $meta->save();
                }
            }
        }

    }

	public function status($id = 0,$status = '')
	{
		$entry = \Entry::find($id);
        if(empty($entry)) return redirect()->back()->with('err','Product not found!');

        $entry->status = $status;
        $entry->save();

        return redirect()->back()->with('msg','Data Saved!');
	}
}