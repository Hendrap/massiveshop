<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;

class PromoController extends AlphaController
{
//     
// 

    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_promo';
        $this->layout->active = 'alpha_catalog_promo_index';
    }

	public function index(){
		$this->layout->title = setPageTitle("Promo");
		$promos = \CatalogPromo::paginate(30);
		$this->layout->content = view('catalog::admin.pages.promo.index',[
                    'promos' => $promos
                ]);
	}
	public function create(){
		$this->layout->title = setPageTitle("Create Promo");	
		$this->layout->content = view('catalog::admin.pages.promo.create',[
                    'users' => \User::get()
                ]);
	}
	public function edit($id = 0){
		$this->layout->title = setPageTitle("Edit Promo");		
		$promo = \CatalogPromo::find($id);
		if(empty($promo)) abort(404);

		$this->layout->title = setPageTitle("Edit Promo");	
		$this->layout->content = view('catalog::admin.pages.promo.edit',[
                    'promo' => $promo,
                ]);	
	}
	public function save(Request $req){
		
		$messages = [
		    'rule_detail.required' => 'The field Discount Amount/Percent is required!',
		];

		$rules = [
			'coupon_code' => 'required',
			'min_order' => 'required',
			'rule_detail' => 'required',
		];
		$id = (int)$req->promo_id;
		if($id == 0){
			$rules['coupon_code'] = 'required|unique:catalog_promos';
		}
        if($req->algorithm == 'percentage_discount')
        {
            $rules['rule_detail'] = 'numeric|required|max:100';
        }

        $messages = ['rule_detail.max' =>  'The field Discount may not be greater than 100.'];

       
		$this->validate($req,$rules,$messages);
		$promo = \CatalogPromo::find($id);
		if(empty($promo)) $promo = new \CatalogPromo();

		$promo->title = $req->title;
		$promo->content = "";
		$promo->status = $req->status;
		$promo->coupon_type = $req->coupon_type;
		$promo->coupon_code = $req->coupon_code;
		$promo->rule_detail = str_replace(',', '', $req->rule_detail);
		$promo->start = date('Y-m-d H:i:s',strtotime($req->start));
		$promo->end = date('Y-m-d H:i:s',strtotime($req->end));
		$promo->user_id = $req->user_id;
		$promo->algorithm = $req->algorithm;
		$promo->min_order = str_replace(',', '', $req->min_order);
        $promo->rules = $req->rules;
		$promo->save();
        
        return redirect()->route('catalog_promo_edit',[$promo->id])->with('msg','Data Saved!');
	}
        public function getData(Request $req)
        {
            //container data
            $data = [];
            //pagination
            $start = $req->input('start',0);
            $take = $req->input('length',10);
            //search field
            $search = $req->input('search');
            //order
            $order = $req->input('order');
            //categgory
            $isSearch = false;

            $base = new \CatalogPromo;

            $recordsTotal = $base->where('status','!=','deleted');

            $recordsTotal = $recordsTotal->count();

            $promos = $base->where('status','!=','deleted');

            if(!empty($search['value']))
            {
                $isSearch = true;
                $promos = $promos->where(function($q)use($search){
                    $q->where('catalog_promos.title','like','%'.$search['value'].'%');
                    $q->orWhere('catalog_promos.coupon_code','like','%'.$search['value'].'%');
                });
            }
            //ambil data

            //promo status
            $isStatus = false;
            $status = $req->input('columns');

             //total entry yg kefilter
            $recordsFiltered = $promos->count();
            

            $promos = $promos->take($take)->skip($start);
            //sorting
            $cols = ['coupon_code','start','created_at','updated_at','status'];

            if(!empty($order[0]))
            {
                $col = @$cols[$order[0]['column']];
                if(!empty($col))
                {
                    $promos = $promos->orderBy($col,$order[0]['dir']);
                }else{
                    $promos = $promos->orderBy('catalog_promos.created_at','desc');
                }
            }else{
                $promos = $promos->orderBy('catalog_promos.created_at','desc');
            }


            $promos = $promos->get();

            $cols = ['coupon_code','rule_detail','rules','created_at','start','end','status'];
            $view = 'catalog::admin.pages.promo.grid.promo';
            $base = [];
            foreach ($cols as $col) {
                $base[$col] = '';
            }
            
            foreach ($promos as $key => $value) {
                $item = $base;
                $html = '';
                $html .= view($view,['promo'=>$value,'isSearch'=>$isSearch]);
                $item['html'] = $html;

                $data[] = $item;
            }

            return json_encode(['data'=>$data,'draw'=>$req->input('draw',1),'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered]);
        }
        
	public function delete(Request $req){
		\CatalogPromo::find($req->id)->delete();
		return redirect()->back();
	}
        
    public function status($promo_id = 0,$status = ''){
            $promo = \CatalogPromo::find($promo_id);
            if(empty($promo)) abort(404);

            $promo->status = $status;
            $promo->save();

            return redirect()->back()->with('msg','Status Changed!');
	}
}