<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;
use Logger;
use Entry;
use CatalogAccount;
use CatalogPaymentChannel;
use CatalogSalesChannel;

class SalesChannelController extends AlphaController
{
    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_setting';
        $this->layout->active = 'alpha_catalog_sales_channels';
    }

    public function index()
    {
    	$channels = CatalogSalesChannel::get();
        $payments = CatalogPaymentChannel::get();

    	$this->layout->title = setPageTitle("Sales Channels");
    	$this->layout->content = view('catalog::admin.pages.channels.index',[
    			'channels' => $channels,
                'payments' => $payments
    		]);
    }

    public function saveData(Request $req)
    {
    	$channel = CatalogSalesChannel::find($req->id);
    	if(empty($channel)) $channel = new CatalogSalesChannel();
    	$channel->name = $req->name;
    	$channel->description = $req->description;
        $channel->type = $req->type;
        if($req->type == 'marketplace'){
            $payment = CatalogPaymentChannel::whereName($req->name.' '.'Escrow')->first();
            if(empty($payment)) $payment = new CatalogPaymentChannel();
            $payment->name = $req->name.' '.'Escrow';
            $payment->save();

            $channel->payment_methods = json_encode([$payment->id]);
        }else{
            $channel->payment_methods = json_encode($req->payment_methods);
        }
    	$channel->save();

        $html = '';
        $html .= view('catalog::admin.pages.channels.grid',['channel'=>$channel]);
    	return json_encode(array('status'=>1,'channel'=>$channel,'html'=>$html));
    }	

    public function deleteData(Request $req)
    {
    	$channel = CatalogSalesChannel::find($req->id);
    	$channel->delete();

    	return redirect()->back();
    }


 }