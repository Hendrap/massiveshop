<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;

class SettingController extends AlphaController
{
	public function getSetting(){
		$this->layout->title = setPageTitle("Catalog Setting");
		$settings = [];
		$varsGeneralSettings = ['weight_unit','length_unit','currency'];
		foreach ($varsGeneralSettings as $key => $value) {
			$settings[$value] = app('AlphaSetting')->getSetting($value);
		}
		$this->layout->content = view('catalog::admin.pages.setting',[
			'settings' => $settings
			]);
	}
	public function postSetting(){
		$input = \Request::all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			app('AlphaSetting')->updateSetting($key,$value,'yes');
		}
		return redirect()->back()->with('success','Data Saved!');
	}
}