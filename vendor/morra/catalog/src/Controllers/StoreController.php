<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;
use Logger;
use Entry;
use CatalogAccount;
use CatalogPaymentChannel;
use CatalogSalesChannel;
use Taxonomy;

class StoreController  extends AlphaController
{
    function __construct()
    {
        parent::__construct();
        $this->layout->activeParent = 'alpha_catalog_setting';
        $this->layout->active = 'alpha_catalog_stores';
    }

    public function index(){
        $channels = CatalogSalesChannel::get();
    	$stores = Taxonomy::whereTaxonomyType('stores')->get();
    	$this->layout->title = setPageTitle("Stores");
    	$this->layout->content = view('catalog::admin.pages.store.index',[
    			'stores' => $stores,
                'channels' => $channels
    		]);
    }

    public function create()
    {
        $channels = CatalogSalesChannel::get();
        $this->layout->title = setPageTitle("Create Store");
        $this->layout->content = view('catalog::admin.pages.store.create',[
                'channels' => $channels
            ]);
    }

    public function edit($id = 0)
    {

        $store = Taxonomy::find($id);
        if(empty($store)) abort(404);

        $selectedChannels = (array)json_decode($store->data);
        $storeData = json_decode($store->store_data);
        if(empty($storeData))
        {
            $storeData = (object)[
                'id' => '',
                'media_container_image' => '',
                'media_id' => '',
                'email_header' => '',
                'invoice_header' => '',
                'return_address' => '',
                'phone' => '',
                'email_address' => ''
            ];
        }

        $channels = CatalogSalesChannel::get();
        $this->layout->title = setPageTitle("Edit Store");
        $this->layout->content = view('catalog::admin.pages.store.edit',[
                'channels' => $channels,
                'selectedChannels' => $selectedChannels,
                'storeData' => $storeData,
                'store' => $store
            ]);

    }

    public function save(Request $req)
    {
        $this->validate($req,[
                'name' => 'required',
                'email_address' => 'email',
                'phone' => 'numeric'
            ]);

        $store = Taxonomy::find($req->id);
        if(empty($store)) $store = new Taxonomy();
        $store->name = $req->name;
        $store->description = $req->content;
        $store->taxonomy_type = 'stores';
        $store->data = json_encode($req->sales_channels);

        $input = $req->input();
        unset($input['_token']);
        unset($input['content']);
        unset($input['sales_channels']);

        $store->store_data = json_encode($input);
        $store->save();

        return redirect()->route('catalog_store_edit',[$store->id])->with('msg','Data Saved!');

    }   

    public function deleteData(Request $req)
    {
        $store = Taxonomy::find($req->id);
        $store->delete();

        return redirect()->back();
    }



}