<?php

/*
 * Catalog Cart
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
 */


namespace Morra\Catalog\Core;

/**
* 
*/

abstract class Cart
{
	abstract public function addItemToCart();
	abstract public function removeItemFromCart();
	abstract public function cleanUpExpiredCart();
}