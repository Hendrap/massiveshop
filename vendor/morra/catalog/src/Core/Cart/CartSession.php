<?php

/*
 * Catalog Setup
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
 */

namespace Morra\Catalog\Core\Cart;

use Morra\Catalog\Core\Cart as AbsCart;
use Morra\Catalog\Models\Cart as CartCoreModel; 
use CatalogSearch;

class CartSession extends AbsCart
{
	public $itemsId;

	public function addItemToCart($itemId = 0,$qty = 0,$userId = 0,$infinityStock = false)
	{
		if($itemId == 0 || $qty <= 0)
		{
			return array('status'=>0,'msg'=>'Invalid parameters');
		}

		$item = \CatalogItem::find($itemId);
		if(empty($item)) return array('status'=>0,'msg'=>'Item not found');

		if($infinityStock == false)
		{
			if($item->stock_status == 0 || $item->stock_available <= 0 || ($item->stock_available < $qty)) return array('status'=>0,'msg'=>'Item qty not available');			

		}else{

			if($item->stock_status == 0) return array('status'=>0,'msg'=>'Item qty not available'); 

		}

		$cart = $this->getSingleFromCart($itemId,$userId);
		

		if(empty($cart)){
			$cart = new CartCoreModel();
			$cart->user_id = $userId;
			$cart->session_id = ($userId == 0) ? $this->getSessionId() : '';
			$cart->item_id = $itemId;
			$cart->qty = $qty;

		}else{
			$tmpTotal = (int)($cart->qty) + $qty;
			if($item->order_limit > 0 && $tmpTotal > $item->order_limit){
				return array('status'=>0,'msg'=>'The following item has a quantity restriction'); 
			}
			$cart->qty = $tmpTotal;
		}
		#hendra
		$cart->original_price = castingPrice($item);

		$cart->save();
		if($infinityStock == false)
		{
			$item->stock_available = $item->stock_available - $qty;
			$item->save();			
		}

		return array('status'=>1,'item'=>$item);


	}

	public function removeItemFromCart($itemId = 0,$userId = 0)
	{
		$cart = $this->getSingleFromCart($itemId,$userId);

		if(empty($cart)) return array('status'=>1);

		$item = \CatalogItem::find($cart->item_id);

		if(empty($item)) return array('status'=>1);

		$item->stock_available = $item->stock_available + $cart->qty;
		$item->save();

		$cart->delete();

		return array('status'=>1,'item'=>$item);

	}

	public function getSingleFromCart($itemId = 0,$userId = 0)
	{
		$cart = CartCoreModel::whereItemId($itemId);
		
		if($userId == 0){
			$cart = $cart->whereSessionId($this->getSessionId());
		}else{
			$cart = $cart->whereUserId($userId);
		}

		$cart = $cart->first();
		
		return $cart;
	}

	public function translateItemsCart($userId = 0)
	{
		$cart = CartCoreModel::whereSessionId($this->getSessionId())->get();
		foreach ($cart as $key => $value) {
			$item = $value;
			$item->session_id = '';
			$item->user_id = $userId;
			$item->save();
			$before = CartCoreModel::where('id','!=',$item->id)->whereItemId($value->item_id)->whereUserId($userId)->first();
			if(!empty($before)){
				$before->qty = $before->qty + $item->qty;
				$before->save();
				$item->delete();
			}
		}
	}

	public function rollbackItemsCart($cart = array())
	{
		$idsProduct = [];
		foreach ($cart as $key => $value) {
			$item = $value;
			$catalogItem = \CatalogItem::find($item->item_id);

			if(!empty($catalogItem)){
				$catalogItem->stock_available = $catalogItem->stock_available + $item->qty;
				$catalogItem->save();
			}
			$idsProduct[] = $catalogItem->product_id;
			$item->delete();
		}

		if(!empty($idsProduct))
		{
			app('Morra\Catalog\Models\Sync')->doSync($idsProduct);			
		}
	}

	public function cleanUpExpiredCart($days = 7)
	{
		$cart = CartCoreModel::where("updated_at", "<", date("Y-m-d", strtotime("-".$days." days")))->get();
		$this->rollbackItemsCart($cart);
	}

	public function generateSessionId()
	{
		return uniqid();
	}

	public function setSessionId()
	{
		if(!\Session::has('alphaCatalogCart'))
		{
			\Session::put('alphaCatalogCart', $this->generateSessionId());	
			\Session::save();		
		}
	}

	public function getSessionId()
	{
		if(!\Session::get('alphaCatalogCart')) $this->setSessionId();

		return \Session::get('alphaCatalogCart');
	}

	public function afterLogin($userId = 0)
	{
		$this->translateItemsCart($userId);
		\Session::forget('alphaCatalogCart');
	}

	public function beforeLogout($userId = 0)
	{
		$cart = CartCoreModel::whereUserId($userId)->get();
		$this->rollbackItemsCart($cart);
		\Session::forget('alphaCatalogCart');
	}

	public function reload()
	{
		$items = CartCoreModel::select(['item_id']);

		if(\Auth::user())
		{
			$items = $items->whereUserId(\Auth::user()->id);
		}else{
			$items = $items->whereSessionId($this->getSessionId());
		}

		$items = $items->pluck('item_id');
		$this->itemsId = $items;
	}

	public function init()
	{
		$this->reload();
	}

	public function getItems($with = array('item','item.entry'))
	{
		$this->reload();
		$items = [];

		if(!empty($this->itemsId))
		{
			$items = CartCoreModel::with($with)->whereIn('item_id',$this->itemsId);			
			if(\Auth::user())
			{
				$items = $items->whereUserId(\Auth::user()->id);
			}else{
				$items = $items->whereSessionId($this->getSessionId());
			}
			$items = $items->get();
		}
		return $items;
	}

	public function afterCheckout($ids =[])
	{
		if(!empty($ids)){
			CartCoreModel::whereIn('id',$ids)->delete();
		}
	}

	

}	