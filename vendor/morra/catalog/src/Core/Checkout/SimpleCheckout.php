<?php

/*
 * Checkout
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
*/


namespace Morra\Catalog\Core\Checkout;
use Morra\Catalog\Core\Checkout as AbsCheckout;
use CatalogCart;
use CatalogSearch;
use CatalogOrderActivity;

class SimpleCheckout extends AbsCheckout
{	
	public function cleanUp($data = []){

		if(empty($data['orderDetails'])) return false;
 
		$idsCart = [];
		$idsProduct = [];
		foreach ($data['orderDetails'] as $key => $value) {
			$idsCart[] = $value->id;
			$idsProduct[] = $value->product_id;
		}
		//cleanup cart
		CatalogCart::afterCheckout($idsCart);
		//sync products
		if(class_exists('Morra\Catalog\Models\Sync')){
			app('Morra\Catalog\Models\Sync')->doSync($idsProduct);
		}
	}

	public function process($data = array())
	{
		
		try {
			
			\DB::beginTransaction();

			//order 
			$dataOrder = $data['order'];
			$vars = $data['varsOrder'];
			$dataOrderDetails = $data['orderDetails'];
			//order details
			$parsedOrderDetails = array();

			$grandTotal = ($dataOrder->original_subtotal + $dataOrder->shipping_amount) - $dataOrder->global_and_item_discount;

			if($grandTotal < 0){
				$grandTotal = 0;
			}
			
			$order = new \CatalogOrder();
			$order->order_number = getOrderNumber();
			foreach ($vars as $key => $value) {
				$resProp = @$dataOrder->{$value};
				if(empty($resProp)) $resProp = '';
				$order->{$value} = $resProp;
			}
			$order->subtotal_before_discount = $dataOrder->original_subtotal;
			$order->global_and_item_discount = $dataOrder->global_and_item_discount;
			$order->global_discount = $dataOrder->global_discount;
			$order->subtotal_after_discount = $dataOrder->subtotal;
			$order->order_date = date('Y-m-d H:i:s');
			$order->source = 1; //web
			$order->grand_total = $grandTotal;
			$order->save();

			//order details
			$vars = $data['varsOrderDetail'];
			foreach ($dataOrderDetails as $key => $value) {
				
				$orderDetail = new \CatalogOrderDetail();
				$orderDetail->order_id = $order->id;
				
				foreach ($vars as $k => $var) {
					$resProp = @$value->{$var};
					if(empty($resProp)) $resProp = '';
					$orderDetail->{$var} = $resProp;
				}

				$orderDetail->save();
				$parsedOrderDetails[$orderDetail->item_id] = $orderDetail;
			}

			//promos
			if(!empty($data['promos'])){
				foreach ($data['promos'] as $key => $value) {

					$dbPromo = new \CatalogOrderPromo();

					$dbPromo->order_id = $order->id;
					$dbPromo->order_detail_id = 0;

					if($value->level == 'order_detail') $dbPromo->order_detail_id = $parsedOrderDetails[$value->item_id]->id;

					$dbPromo->level = $value->level;
					$dbPromo->amount = $value->amount;
					$dbPromo->promo_code = $value->promo_code;
					$dbPromo->save();


					$originalPromo = \CatalogPromo::whereCouponCode($value->promo_code)->first();
					if(!empty($originalPromo)){
						if($originalPromo->coupon_type == 'one-time'){
							$originalPromo->status = 'disabled';
							$originalPromo->save();						
						}

						//log promo
						$log = new \CatalogLogPromo();
						$log->order_id = $order->id;
						$log->promo_id = $dbPromo->id;
						$log->amount = $value->amount;
						$log->save();
					}

				}
			}

			//payment
			// $payment = app('Alpha_Catalog_PaymentMethod');
			// if(empty($payment)) return array('status'=>0,'msg'=>'Payment method not found!');		

			// $result = $payment->capture($order,$data);

			$result['order_number'] = $order->order_number;
			$result['id'] = $order->id;
			CatalogOrderActivity::addActivity($order->id,'PENDING',"Order received via ".\CatalogPOS::getSourceFromOrder($order));
			\DB::commit();

			return $result;

		} catch (Exception $e) {
			\DB::rollBack();
			return array('status'=>0,'msg'=>'Internal Server Error!');		
		}

	}
}