<?php

namespace Morra\Catalog\Core;
use Entry;
use Taxonomy;
use CatalogPaymentChannel;
use CatalogSalesChannel;

/**
* 
*/
class POS
{
	
	function __construct()
	{
		# code...
	}

	public function getStores(){
		$sources = $this->getSources();
		$storesDB = Taxonomy::whereTaxonomyType('stores')->get();

		$stores = [];
		foreach ($storesDB as $key => $value) {
			$stores[] = (object)[
				'id' => $value->id,
				'name' => $value->name,
				'sources' => $this->parseSource(json_decode($value->data),$sources)
			];
		}

		return $stores;
	}

	public function parseSource($ids,$sources){
		$list = [];
		foreach ($ids as $key => $value) {
			foreach ($sources as $k => $v) {
				if($value == $v->id) $list[] = $v;
			}
		}

		return $list;
	}

	public function getShippingMethodFromOrder($order)
	{
		$tmpShipping = $this->getShippingMethods();
		$shipping = [];
		foreach ($tmpShipping as $key => $value) {
			$shipping[$value->id] = $value->name;
		}

		$shippingMethod = '';
		if(!empty($shipping[$order->carrier_id])) $shippingMethod = $shipping[$order->carrier_id];

		return $shippingMethod;
	}

	public function getPaymentMethodFromOrder($order)
	{
		$tmpPaymentMethods = $this->getPaymentMethods();
		$paymentMethods = [];
		foreach ($tmpPaymentMethods as $key => $value) {
			$paymentMethods[$value->id] = $value->name;
		}

		$paymentMethod = '';
		if(!empty($paymentMethods[$order->payment_method])) $paymentMethod = $paymentMethods[$order->payment_method];

		return $paymentMethod;
	}

	public function getSourceFromOrder($order)
	{
		$tmpSources = $this->getSources();
		$sources = [];
		foreach ($tmpSources as $key => $value) {
			$sources[$value->id] = $value->name;
		}

		$source = '';
		if(!empty($sources[$order->source])) $source = $sources[$order->source];

		return $source;
	}

	public function getShippingMethods(){
		$default = config('catalog.shipping');

		$db = Taxonomy::whereTaxonomyType('shop_shipping_methods')->get();
		foreach ($db as $key => $value) {
			$default[] = (object)[
				'id' => $value->id,
				'name' => $value->name
			];
		}
		return $default;
	}

	public function getPaymentMethods()
	{
		///$default = config('catalog.payments');
		$db = CatalogPaymentChannel::get();
		foreach ($db as $key => $value) {
			$default[] = (object)[
				'id' => $value->id,
				'name' => $value->name
			];
		}

		return $default;

	}

	public function getSources()
	{
		$paymentMethods = $this->getPaymentMethods();

		$db = CatalogSalesChannel::get();
		foreach ($db as $key => $value) {
			$default[] = (object)[
				'id' => $value->id,
				'name' => $value->name,
				'payments' => $this->parsePaymentMethods(json_decode($value->payment_methods),$paymentMethods)
			];
		}

		return $default;
	}

	public function getOrderStatus()
	{
		return config('catalog.order-status');
	}

	public function parsePaymentMethods($requiredIds = array(),$list = array())
	{
		$result = [];
		foreach ($requiredIds as $key => $value) {
			foreach ($list as $v) {
				if($value == $v->id) $result[] = $v;
			}
		}

		return $result;
	}

}