<?php

/*
 * Catalog Cart
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
 */


namespace Morra\Catalog\Core;

/**
* 
*/

abstract class PaymentMethod
{
	abstract public function capture($order);
	abstract public function getPaymentMethodName();
	abstract public function getNicePaymentMethodName();
	abstract public function render();
}