<?php

/*
 * Catalog Bank Transfer
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
*/


namespace Morra\Catalog\Core\PaymentMethod;
use Morra\Catalog\Core\PaymentMethod as AbsPayment;

class Bank extends AbsPayment
{
	public function capture($order,$data = array())
	{
		$payment = new \CatalogBankTransfer();
		$payment->order_id = $order->id;
		$payment->user_id = $order->user_id;
		$payment->amount = 0;
		$paymentData = $data['payment'];
		$vars = array('bank','user_account','user_holder','user_bank','refnumber');
		foreach ($vars as $key => $value) {
			$payment->{$value} = $paymentData->{$value};
		}
		$payment->save();
		return array('redirect'=>false,'url'=>'');
	}
	public function getNicePaymentMethodName(){
		return 'Bank Transfer';
	}
	public function getPaymentMethodName(){
		return 'bank';
	}
	public function render($orderId = 0){
		$payment = \CatalogBankTransfer::whereOrderId($orderId)->first();
		return view('catalog::admin.pages.payment.bank',[
			'payment' => $payment
			]);
	}
}