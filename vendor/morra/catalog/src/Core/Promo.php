<?php

namespace Morra\Catalog\Core;
use CatalogLogPromo;
abstract class Promo
{
	abstract public function process($paramData,$coupon,$isCheckout);
	public function validate($paramData,$coupon){
		$data = $paramData;
		$orderTotal = $data['order']->original_subtotal + $data['order']->shipping_amount;
		
		$user_id = 0;
		if(\Auth::user()) $user_id = \Auth::user()->id;
		//status
		if(!($coupon->status == 'active' || $coupon->status == 'published')){
			return false;
		}
		//user
		if($coupon->user_id != 0 && ($coupon->user_id != $user_id)){

			return false;
		}
		//date
		if(!(time() >= strtotime($coupon->start) && time() <= strtotime($coupon->end)))
		{
			return false;
		}

		if($coupon->coupon_type == 'one-time')
		{
			$log = CatalogLogPromo::wherePromoId($coupon->id)->count();
			if($log > 0) return false;
		}
		//min order
		if($coupon->min_order > 0){
			if(!($orderTotal >= (float)$coupon->min_order)){
				return false;
			}
		}
		return true;
	}
}