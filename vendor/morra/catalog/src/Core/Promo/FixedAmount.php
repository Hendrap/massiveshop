<?php

/*
 * Catalog Bank Transfer
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
*/


namespace Morra\Catalog\Core\Promo;
use Morra\Catalog\Core\Promo as AbsPromo;

class FixedAmount extends AbsPromo
{	
	
	//param array|object:catalog_promos|boolean
	public function process($paramData,$coupon,$isCheckout)
	{
		
		$data = $paramData;
		//validate
		if($this->validate($paramData,$coupon)){
			$data['order']->global_and_item_discount += $coupon->rule_detail;		
			$data['order']->global_discount += $coupon->rule_detail;			
		}
		$data['promos'][] = (object)[
			'level' => 'order',
			'amount' => $coupon->rule_detail,
			'coupon_code' => $coupon->coupon_code
		];
		return $data;

	}
	

	/* //examples level order detail 
	public function process($paramData,$coupon,$isCheckout){
		$data = $paramData;
		//validate
		if(!$this->validate($paramData,$coupon)) return false;

		foreach ($data['orderDetails'] as $key => $value) {
			if($value->item_id == 2){
				
				$data['orderDetails'][$key]->total_discount += $coupon->rule_detail;
				$data['orderDetails'][$key]->final_amount -= $coupon->rule_detail;
				
				$data['promos'][] = (object)[
					'level' => 'order_detail',
					'amount' =>$coupon->rule_detail,
					'coupon_code' => $coupon->coupon_code,
					'item_id' => $value->item_id,
				];

			}
		}

		$newTotal = 0;
		foreach ($data['orderDetails'] as $key => $value) {
			$newTotal += $value->final_amount;
		}

		$data['order']->subtotal = $newTotal;

		return $data;
	}*/
	
}