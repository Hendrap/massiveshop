<?php

/*
 * Catalog Bank Transfer
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
*/


namespace Morra\Catalog\Core\Promo;
use Morra\Catalog\Core\Promo as AbsPromo;

class FreeShipping extends AbsPromo
{	
	//param array|object:catalog_promos|boolean
	public function process($paramData,$coupon,$isCheckout)
	{
		
		$data = $paramData;
		//validate
		if($this->validate($paramData,$coupon)){
			$data['order']->global_and_item_discount += $data['order']->shipping_amount;
			$data['order']->global_discount += $coupon->shipping_amount;			
		}

		$data['promos'][] = (object)[
			'level' => 'order',
			'amount' =>$data['order']->shipping_amount,
			'coupon_code' => $coupon->coupon_code
		];

		return $data;

	}
	
}