<?php

/*
 * Catalog Bank Transfer
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
*/


namespace Morra\Catalog\Core\Promo;
use Morra\Catalog\Core\Promo as AbsPromo;

class PercentageDicount extends AbsPromo
{	
	//param array|object:catalog_promos|boolean
	public function process($paramData,$coupon,$isCheckout)
	{
		
		$data = $paramData;
		//validate
		$total = 0;
		
		if($this->validate($paramData,$coupon)){

			$total = ($data['order']->original_subtotal) - $data['order']->global_discount;
			$total = (float)($coupon->rule_detail / 100) * $total;
			$data['order']->global_and_item_discount += $total;		
			$data['order']->global_discount += $total;		
		}
		$data['promos'][] = (object)[
				'level' => 'order',
				'amount' => $total,
				'promo_code' => $coupon->coupon_code
		];
		
		return $data;

	}
	
}