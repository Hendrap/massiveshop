<?php

namespace Morra\Catalog\Core;

class PromoContainer
{
	public $promos;
	public function init(){
		if(!\Session::has('Alpha_Catalog_PromoList')) {
			\Session::put('Alpha_Catalog_PromoList',[]);
			\Session::save();
		}
				
		$this->promos = \Session::get('Alpha_Catalog_PromoList');
	}
	public function getAllPromo(){
		$this->init();
		return $this->promos;
	}
	public function validate($data,$promoCode){
		$promo = \CatalogPromo::whereCouponCode($promoCode)->first();
		if(empty($promo)) return false;

		$res = app('Alpha_Catalog_Promo_'.$promo->algorithm)->validate($data,$promo);
		return $res;
	}
	public function add($promoCode){
		$promos = \Session::get('Alpha_Catalog_PromoList');
		$promos[$promoCode] = $promoCode;
		\Session::put('Alpha_Catalog_PromoList',$promos);
		\Session::save();
	}
	public function remove($promoCode){
		$promos = \Session::get('Alpha_Catalog_PromoList');
		if(!empty($promos[$promoCode])){
			unset($promos[$promoCode]);
		}
		\Session::put('Alpha_Catalog_PromoList',$promos);
		\Session::save();

	}
	public function process($data,$isCheckout = false){
		$this->init();
		$promos = [];
		if(!empty($this->promos)){
			$promos = \CatalogPromo::whereIn('coupon_code',$this->promos)->get();
		}
		foreach ($promos as $key => $value) {
			$data = app('Alpha_Catalog_Promo_'.$value->algorithm)->process($data,$value,$isCheckout);
		}

		return $data;
	}
}