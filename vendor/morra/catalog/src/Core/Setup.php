<?php

/*
 * Catalog Setup
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
 */


namespace Morra\Catalog\Core;

/**
* 
*/
class Setup
{
	
	function __construct()
	{
			
	}
	
	function run(){
		$this->setMenu();
		$this->setCore();
	}
	public function setCore(){
		
		app()->bind('Alpha_Catalog_Cart','Morra\Catalog\Core\Cart\CartSession');
		app()->bind('Alpha_Catalog_ShippingMethod','Morra\Catalog\Core\Shipping\Jne');
		app()->bind('Alpha_Catalog_Checkout','Morra\Catalog\Core\Checkout\SimpleCheckout');
		app()->bind('Alpha_Catalog_PaymentMethod','Morra\Catalog\Core\PaymentMethod\Bank');
		app()->bind('Alpha_Catalog_POS','Morra\Catalog\Core\POS');
		app()->bind('Alpha_Catalog_Mail','Morra\Catalog\Core\Mail');




		//binding payment method goes heere
		app()->bind('Alpha_Catalog_PaymentMethodList_bank','Morra\Catalog\Core\PaymentMethod\Bank');

		//binding shipping method goes here
		app()->bind('Alpha_Catalog_ShippingMethodList_jne','Morra\Catalog\Core\Shipping\Jne');

		//binding promo
		app()->bind('Alpha_Catalog_PromoContainer','Morra\Catalog\Core\PromoContainer');
		$promos = \Config::get('catalog.promo-algoritm');
		foreach ($promos as $key => $value) {
			app()->bind('Alpha_Catalog_Promo_'.$key,$value->class);
		}




	}
	public function setMenu(){
		// $settings = [];

		// foreach (app('AlphaSetting')->entries as $key => $value) {
		// 	if(strpos($value['slug'],'catalog_') === 0){
		// 		$settings[] = $value;
		// 	}
		// }

		app('AlphaMenu')->addLabel((object)['name' => 'ORDER MANAGEMENT'],'catalog_plugin');
		app('AlphaMenu')->add((object)['label'=>'catalog_plugin','name'=>'Stock & Inventory','class'=>'icon-notebook','link'=>'javascript:void(0)'],'alpha_catalog_stock');
		app('AlphaMenu')->addChildTo('alpha_catalog_stock',(object)['name'=>'Product Catalog','link'=>route('catalog_product_variant_index',['product'])],'alpha_catalog_type_catalog_variant_product');
		app('AlphaMenu')->addChildTo('alpha_catalog_stock',(object)['name'=>'Brands','link'=>route('catalog_brands_index')],'alpha_catalog_brands');
		app('AlphaMenu')->addChildTo('alpha_catalog_stock',(object)['name'=>'Shopping Carts','link'=>route('alpha_catalog_shopping_carts')],'alpha_catalog_shopping_carts');
	

		app('AlphaMenu')->add((object)['label'=>'catalog_plugin','name'=>'Outgoing','class'=>'icon-coins','link'=>'javascript:void(0)'],'alpha_catalog_orders');
		app('AlphaMenu')->addChildTo('alpha_catalog_orders',(object)['name'=>'Sales Orders','link'=>route('catalog_orders_index')],'alpha_catalog_orders_main');

		app('AlphaMenu')->add((object)['label'=>'catalog_plugin','name'=>'Promotions','class'=>'icon-price-tag','link'=>'javascript:void(0)'],'alpha_catalog_promo');
		app('AlphaMenu')->addChildTo('alpha_catalog_promo',(object)['name'=>'All Promotions','link'=>route('catalog_promo_index')],'alpha_catalog_promo_index');
                        

		app('AlphaMenu')->add((object)['label'=>'catalog_plugin','name'=>'Store Setting','class'=>'icon-gear','link'=>'javascript:void(0)'],'alpha_catalog_setting');
		app('AlphaMenu')->addChildTo('alpha_catalog_setting',(object)['name'=>'Stores','link'=>route('catalog_store_index')],'alpha_catalog_stores');
		app('AlphaMenu')->addChildTo('alpha_catalog_setting',(object)['name'=>'Payment Accounts','link'=>route('catalog_accounts_index')],'alpha_catalog_payment_account');
		app('AlphaMenu')->addChildTo('alpha_catalog_setting',(object)['name'=>'Payment Methods','link'=>route('catalog_payment_method_index')],'alpha_catalog_payment_method');
		app('AlphaMenu')->addChildTo('alpha_catalog_setting',(object)['name'=>'Sales Channels','link'=>route('catalog_sales_channels_index')],'alpha_catalog_sales_channels');


	}
}
