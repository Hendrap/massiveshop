<?php

/*
 * Catalog Cart
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
 */


namespace Morra\Catalog\Core\Shipping;

use Morra\Catalog\Core\ShippingMethod;
use Morra\Catalog\Models\Jne as JneModel;
class Jne extends ShippingMethod
{
	
	function __construct()
	{

	}

	public function getShippingMethodType(){
		return 'jne';
	}

	public function getNiceShippingMethodName($order){
		$label =  'JNE';
		if(!empty($order->carrier_level)){
			$label .= ' - '.$order->carrier_level;
		}
		return $label;
	}

	public function getShippingCost($params = array()){
		
		if(empty($params['city']) || empty($params['district']) || empty($params['jne_type']) || empty($params['weight'])) return array('error'=>true,'msg'=>'Uncomplete Parameters!','total'=>0);

		if((float)$params['weight'] <= 0) return array('error'=>true,'msg'=>'Minimum weight is 1','total'=>0);

		$data = JneModel::where('regency','=',$params['city'])->where('district','=',$params['district'])->first();

		if(empty($data)) return array('error'=>true,'msg'=>'Jne\'s data not found','total'=>0);

		$price = $data->{$params['jne_type']};

		if($price <= 0) return array('error'=>true,'msg'=>'Invalid Jne Type','total'=>0);

		$total = $price * (float)$params['weight'];

		return array('error'=>false,'msg'=>'','total'=>$total);

	}

	public function getCityDistrict($keyword = '',$limit = 0)
	{
		$result = JneModel::where('regency','like','%'.$keyword.'%')->orWhere('district','like','%'.$keyword.'%');
		if($limit > 0){
			$result = $result->take($limit);
		}
		$result = $result->groupBy('regency')->get();
		return $result;
	}

	public function getCity($keyword = '',$limit = 0){
		$result = JneModel::where('regency','like','%'.$keyword.'%');
		if($limit > 0){
			$result = $result->take($limit);
		}
		$result = $result->groupBy('regency')->get();
		return $result;
	}

	public function getDistrict($city = '',$keyword = '',$limit = 0){
		$result = JneModel::where('regency','=',$city)->where('district','like','%'.$keyword.'%');
		if($limit > 0){
			$result = $result->take($limit);
		}
		$result = $result->get();
		return $result;

	}

}