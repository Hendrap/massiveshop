<?php 

function castingPrice($item,$isRetail = false){
	if($item->sale_price == 0){
		if($isRetail) return $item->msrp;
		return $item->price;
	}

	if($item->schedule_sale == 0) return $item->sale_price;
	
	$now = strtotime(date('Y-m-d H:i:s'));
	
	if($now < strtotime($item->sale_end_date) && $now > strtotime($item->sale_start_date)) return $item->sale_price; 
	return $item->price;
}

function isSale($item)
{
	if($item->sale_price == 0) return false;

	if($item->schedule_sale == 0 || empty($item->schedule_sale)) return true;
	
	$now = strtotime(date('Y-m-d H:i:s'));
	
	if($now < strtotime($item->sale_end_date) && $now > strtotime($item->sale_start_date)){
		return true;
	}else{
		return false;
	}
}

function diffSalePrice($item)
{
	$total = 0;
	$status = isSale($item);
	if($status == true)
	{
		if($item->price > $item->sale_price)
		{
			$total = ($item->price - $item->sale_price);
		}
	}

	return $total;
}
function translateCartToParsedData()
{
	$cart = CatalogCart::getItems(['item','item.entry','item.entry.metas','item.entry.brand','item.entry.medias']);
	$orderDetails = [];
	$subTotal = 0;
	$totalWeight = 0;
	$totalQty = 0;

	foreach($cart as $v){
		$price = castingPrice($v->item);
		$finalAmount = $v->qty * $price;
		$subTotal += $finalAmount;
		$weight = (float)getEntryMetaFromArray(@$v->item->entry->metas,'shipping_info.weight') * $v->qty;

		$shipping_weight = (float)getEntryMetaFromArray(@$v->item->entry->metas,'shipping_info.weight');
		if($shipping_weight <= 0) $shipping_weight = 1;

		$shipping_width = (float)getEntryMetaFromArray(@$v->item->entry->metas,'shipping_info.width');
		if($shipping_width <= 0) $shipping_width = 1;
		
		$shipping_height = (float)getEntryMetaFromArray(@$v->item->entry->metas,'shipping_info.height');
		if($shipping_height <= 0) $shipping_height = 1;

		$shipping_depth = (float)getEntryMetaFromArray(@$v->item->entry->metas,'shipping_info.depth');
		if($shipping_depth <= 0) $shipping_depth = 1;

		if($weight <= 0) $weight = 1;
		$totalWeight += $weight;

		$orderDetails[] = (object)[
			'id' => $v->id,
			'user_id' => $v->user_id,
			'item_id' => $v->item->id,
			'item_title' => $v->item->item_title,
			'item_desc' => $v->item->item_description,
			'product_id' => $v->item->product_id,
			'product_title' => parseMultiLang(@$v->item->entry->title),
			'product_slug' => $v->item->entry->slug,
			'product_desc' => $v->item->entry->content,
			'sku' => $v->item->sku,
			'original_price' => convertCurrency($price),
			'qty' => $v->qty,
			'qty_total' => $v->qty + $v->item->stock_available,
			'discount' => 0,
			'original_total' => convertCurrency($finalAmount),
			'total' => convertCurrency($finalAmount),
			'image_thumbnail' => asset(getCropImage(@$v->item->entry->medias[0]->path,'product')),
			'image_cart' => asset(getCropImage(@$v->item->entry->medias[0]->path,'cart')),
			'brand_name' => parseMultiLang(@$v->item->entry->brand->title),
			'weight' => $weight,
			'shipping_weight' => $shipping_weight,
			'shipping_width' => $shipping_width,
			'shipping_height' => $shipping_height,
			'shipping_depth' => $shipping_depth
		];

		$totalQty += $v->qty;
	}

	if($totalWeight < 1) $totalWeight = 1;

	$userId = 0;
	if(Auth::user()) $userId = Auth::user()->id;
	$data = array(
		'order' => (object)[
			'user_id' => $userId,
			'global_discount' => 0,
			'idr_subtotal' => $subTotal,
			'original_subtotal' => convertCurrency($subTotal),
			'shipping_amount' => !(empty(Session::get('shipping_fee'))) ? Session::get('shipping_fee') : 0,
			'global_and_item_discount' => 0,
			'subtotal' => convertCurrency($subTotal),
			'totalWeight' => (float)($totalWeight),
			'tax'=>0,
			'totalQty' => $totalQty
		],
		'varsOrder' => defaultOrderVars(),
		'orderDetails' => $orderDetails,
		'varsOrderDetail' => defaulOrderDetailsVars(),
	);

	$data = \CatalogPromoContainer::process($data);

	return $data;
}	