<?php 

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
    }
    return $token;
}

function getOrderNumber($length = 6){
	return getToken($length);
}

function defaultOrderVars(){
	$default =  ['subtotal_before_discount','subtotal_after_discount','tax','tracking_number',
                'user_id','global_discount','dropshipper_name','dropshipper_phone',
                'shipping_amount','billing_id','shipping_id','store',
				'grand_total','customer_note','status','shipping_method',
                'payment_method','order_date','source','discount_from_items',
				'carrier_id','note','shipping_date','global_and_item_discount',
				'confirmation','confirmation_date','modified_by','currency','rate_usd','rate_eur'];


    $vars = array('method','email','first_name','last_name','company','address','address_1','address_2','city','postal_code','country','county','state','district','province','phone','mobile_phone');
    $payment = [];
    $shipping = [];
    foreach ($vars as $key => $value) {
    	$payment[] = 'payment_'.$value;
    	$shipping[] = 'shipping_'.$value;
    }
    $default = array_merge($default,$payment);
    $default = array_merge($default,$shipping);

    return $default;
}

function defaulOrderDetailsVars(){
	return ['discount','original_total','total','product_id','item_id',
            'item_title','product_title','sku','qty','original_price','product_desc',
            'item_desc','type','modified_by','weight','brand_name','image_thumbnail',
            'shipping_weight','shipping_width','shipping_height','shipping_depth'
            ];	
}