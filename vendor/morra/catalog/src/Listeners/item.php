<?php 

Event::listen('eloquent.saving: Morra\Catalog\Models\Item', function($item){
	//original item sebelum update
	$original = (object)$item->getOriginal();
	if(empty($item->getOriginal()))
	{
		$original = $item;
	}

	if($item->isDirty('stock_available') || $item->isDirty('sale_price'))
	{
		app('AlphaSetting')->updateSetting('is_item_dirty','yes');
	}

	if(class_exists('Alpha\Models\Task')){

		if($item->isDirty('sale_price'))
		{
			if($item->sale_price != 0 && $item->sale_price < $item->price)
			{
				Task::createNewTask('price-drop',json_encode((object)["id"=>$item->id]));
			}
		}

		//stcok
		if($item->isDirty('stock_available')){
			//qty item berkurang
			if($original->stock_available > $item->stock_available)
			{
				//low stock
				if($item->stock_available == 1)
				{
					Task::createNewTask('low-qty-item',json_encode((object)["id"=>$item->id]));
				}
			}

			//qty item naik
			if($original->stock_available < $item->stock_available)
			{
				//stock bangkit
				if($original->stock_available == 0 || $original->stock_available == 1)
				{
					Task::createNewTask('up-qty-item',json_encode((object)["id"=>$item->id]));
				}
			}

			//sold out
			if($item->stock_available == 0)
			{
				Task::createNewTask('sold-out-item',json_encode((object)["id"=>$item->id]));
			}
		}
		
	}
});