<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class Item extends AlphaORM
{
	protected $table = 'catalog_items';
	public function entry(){
		return $this->belongsTo('Entry','product_id');
	}
	public function metas(){
		return $this->hasMany('Morra\Catalog\Models\ItemMeta');
	}
    public function group(){
        return $this->hasMany('Morra\Catalog\Models\ItemGroup','parent');
    }
	public function medias(){
		return $this->BelongsToMany('Media');
	}
	public function taxonomies(){
		return $this->BelongsToMany('Taxonomy');
	}
	public function saveMetaFromInput($metas = array(),$input = array()){
        if(empty($metas)) return false;
		foreach ($metas as $key => $value) {
            $meta = \CatalogItemMeta::whereMetaKey($value->meta_key)->whereItemId($this->id)->first();
            if(empty($meta)){
                $meta = new \CatalogItemMeta();
                $meta->meta_key = $value->meta_key;
                $meta->item_id = $this->id;
            }
            $val = '';
            switch ($value->meta_data_type) {
                case 'text':
                    $val = @$input[$value->meta_key];
                    break;
                case 'int':
                    $val = (int)@$input[$value->meta_key];
                    break;
                case 'date':
                    $val = date('Y-m-d H:i:s',strtotime(@$input[$value->meta_key]));
                    break;
            }
            if(!empty($val)){
                $meta->{'meta_value_'.$value->meta_data_type} = $val;                
            }
            $meta->save();
        }
	}

}