<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class ItemGroup extends AlphaORM		
{
	protected $table = 'catalog_item_group';
	public function parent(){
		return $this->belongsTo('Morra\Catalog\Models\Item','parent');
	}
	public function item(){
		return $this->belongsTo('Morra\Catalog\Models\Item','item_id');
	}
}