<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class ItemMeta extends AlphaORM		
{
	protected $table = 'catalog_item_metas';
	public function item(){
		return $this->belongsTo('Morra\Catalog\Models\Item');
	}
}