<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class LogPromo extends AlphaORM		
{
	protected $table = 'catalog_log_promos';
	
	public function order(){
		return $this->belongsTo('Morra\Catalog\Models\Order');
	}
}