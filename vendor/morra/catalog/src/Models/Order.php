<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class Order extends AlphaORM		
{
	protected $table = 'catalog_orders';

	public function user(){
		return $this->belongsTo('User');
	}
	public function details(){
		return $this->hasMany('Morra\Catalog\Models\OrderDetail');
	}
	public function orderPromos(){
		return $this->hasMany('Morra\Catalog\Models\OrderPromo','order_id');
	}

	public function billing()
	{
		return $this->belongsTo('Morra\Catalog\Models\Address','billing_id');
	}

	public function shipping()
	{
		return $this->belongsTo('Morra\Catalog\Models\Address','shipping_id');
	}

	public function oldBilling()
	{
		return $this->belongsTo('Morra\Catalog\Models\Address','billing_address_id');
	}

	public function oldShipping()
	{
		return $this->belongsTo('Morra\Catalog\Models\Address','shipping_address_id');
	}

}