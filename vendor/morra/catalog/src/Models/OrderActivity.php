<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
use Auth;
/**
* 
*/
class OrderActivity extends AlphaORM		
{
	protected $table = 'catalog_order_activities';
	
	public function order()
	{
		return $this->belongsTo('Morra\Catalog\Models\Order');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public static function addActivity($orderId,$type,$info = '')
	{
		$data = new OrderActivity();
		$userId = 0;
		if(Auth::user()) $userId = Auth::user()->id;
		$data->user_id = $userId;
		$data->order_id = $orderId;
		$data->type = $type;
		$data->info = $info;
		$data->save();

	}
}