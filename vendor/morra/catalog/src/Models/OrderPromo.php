<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class OrderPromo extends AlphaORM		
{
	protected $table = 'catalog_orders_promo';

	public function order(){
		return $this->belongsTo('Morra\Catalog\Models\Order','order_id');
	}
	public function orderDetail(){
		return $this->belongsTo('Morra\Catalog\Models\OrderDetail','order_detail_id');
	}
}