<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class PaymentChannel extends AlphaORM		
{
	protected $table = 'catalog_payment_channels';

	public function account(){
		return $this->belongsTo('Morra\Catalog\Models\Account');
	}
	
}