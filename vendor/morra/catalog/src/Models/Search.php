<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class Search extends AlphaORM		
{
	protected $table = 'catalog_search';

	public static function rebuildProduct($entry){
		if(class_exists('Morra\Catalog\Models\Sync')){
			app('Morra\Catalog\Models\Sync')->doSync([$entry->id]);
		}
	}

	public static function rebuildItem($item){
		if(class_exists('Morra\Catalog\Models\Sync')){
			app('Morra\Catalog\Models\Sync')->doSync([$item->product_id]);
		}
	}

	public static function deleteProduct($entry){
		app('App\PopitoiSolrGuy')->delete('id:'.$entry->id);
	}

	public function item(){
		return $this->belongsTo('Morra\Catalog\Models\Item');
	}
	
	public function product(){
		return $this->belongsTo('Entry','product_id');
	}
}