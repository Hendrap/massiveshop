<?php 
namespace Morra\Catalog\Models;
/**
* 
*/
class Sync 
{
	
	public function doSync($data){
		if(function_exists('syncProductsToSolr')){
			syncProductsToSolr($data);
		}
		
	}

	public static function doSyncProducts($data)
	{
		if(function_exists('syncProductsToSolr')){
			syncProductsToSolr($data);
		}
	}

}