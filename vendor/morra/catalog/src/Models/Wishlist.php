<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class Wishlist extends AlphaORM		
{
	protected $table = 'catalog_wishlists';

	public function item(){
		return $this->belongsTo('Morra\Catalog\Models\Item');
	}
	public function user(){
		return $this->belongsTo('User');
	}
}