<?php

/*
 * Alpha Services Provider
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * Ini untuk loader semua keperluan untuk alpha
 */


namespace Morra\Catalog;

use Illuminate\Support\ServiceProvider;
use Illuminate\Filesystem\Filesystem;
class Provider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        app('Morra\Catalog\Core\Setup')->run();
        if(\Config::get('catalog.cart.cron') == false && getLottery() == true){
            \CatalogCart::cleanUpExpiredCart(\Config::get('catalog.cart.period'));
        }

        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        $this->loadConfigs();
        $this->setAliases();
        $this->registerViews();
        $this->loadHelpers();
        $this->loadRoutes();
        $this->setCommands();
        $this->loadListeners();
        $this->publishes([
            __DIR__.'/Assets' => public_path('morra/catalog/assets'),
        ], 'morra-catalog');

    }

   
    /**
     * Load semua helpers
     * 
     * @return void
     */
    public function loadHelpers()
    {
        $helpers = $this->loadFilesFromDir(__DIR__.'/Helpers');
        foreach ($helpers as $key => $value) {
            include $value;
        }
    }   

     /**
     * Register semua views
     * 
     * @return void
     */
    public function registerViews()
    {
         $this->loadViewsFrom(__DIR__ . '/Views', 'catalog');
         
    }

    /**
     * Ini cuma ambil informasi files php yg ada dalam suatu folder/path
     * 
     * @return array
     */
    public function loadFilesFromDir($path = '')
    {
        $files = [];
        if(!empty($path)) $files = \File::allFiles($path); 
        return $files;
    }

    /**
     * Masukin semua config yg ada di folder Config
     * bisa create sendiri untuk developer
     * @return void
     */
    public function loadConfigs()
    {
        $configFiles = $this->loadFilesFromDir(__DIR__ . '/Config');
        foreach ($configFiles as $key => $value) {
            $this->mergeConfigFrom((string)$value, 'catalog.'.str_replace('.php', '', $value->getRelativePathname()));
        }
    }

    /**
     * Set alias untuk core,controllers,dll catalog alpha punya
     * nantinya biar gak perlu nambahin namespace
     * @return void
     */
    public function setAliases()
    {

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $aliases = \Config::get('catalog.aliases');
        foreach ($aliases as $key => $value) {
          $loader->alias($key, $value);
        }

    }

    /**
     * Set artisan commands aja
     * @return void
     */
    public function setCommands()
    {
        $commands = \Config::get('catalog.commands');
        if(empty($commands)) return false;

         $this->commands($commands);

    }

    //include routes
    public function loadRoutes()
    {
        $routes = $this->loadFilesFromDir(__DIR__.'/Routes');
        foreach ($routes as $key => $value) {
            include $value;
        }
    }

    //include listeners
    public function loadListeners()
    {
        $files = $this->loadFilesFromDir(__DIR__.'/Listeners');
        foreach ($files as $key => $value) {
            include $value;
        }
    }

}
