<?php 
Route::group(['middleware' => ['AlphaAdminGroup','AuthAdmin'],'prefix'=>'admin/catalog'], function () {


	


	//product
	if(class_exists('Morra\Catalog\Custom\Product\ProductController')){
		$namespace = 'Morra\Catalog\Custom\Product';
	}else{
		$namespace = 'Morra\Catalog\Controllers';
	}
	
	Route::group(['namespace'=>$namespace],function(){
		Route::get('variant/{type}',['as'=>'catalog_product_variant_index','uses'=>'ProductController@index']);
		Route::any('variant/get-data/{type}',['as'=>'catalog_product_variant_get_data','uses'=>'ProductController@getData']);
		Route::get('variant/{type}/create',['as'=>'catalog_product_variant_create','uses'=>'ProductController@create']);
		Route::get('variant/edit/{type}/{id}',['as'=>'catalog_product_variant_edit','uses'=>'ProductController@edit']);
		Route::post('variant/save/{type}/{id}',['as'=>'catalog_product_variant_save','uses'=>'ProductController@save']);
		Route::get('variant/status/{id}/{status}',['as'=>'catalog_product_variant_status','uses'=>'ProductController@status']);		
	});

	Route::group(['namespace'=>'Morra\Catalog\Controllers'],function(){

		Route::get('/',['as'=>'catalog_stat','uses'=>'CatalogController@index']);
		//setting
		Route::get('setting',['as'=>'catalog_setting','uses'=>'SettingController@getSetting']);
		Route::post('setting',['as'=>'catalog_setting_post','uses'=>'SettingController@postSetting']);


		//wishlist
		Route::get('wishlists',['as'=>'catalog_wishlists_index','uses'=>'WishlistController@index']);
		Route::get('status-wishlist/{id}/{status}',['as'=>'catalog_wishlists_status','uses'=>'WishlistController@status']);
	        
		//promos
		Route::get('promo',['as'=>'catalog_promo_index','uses'=>'PromoController@index']);
	    Route::any('promo-get-data',['as'=>'catalog_promo_get_data','uses'=>'PromoController@getData']);
		Route::get('promo-create',['as'=>'catalog_promo_create','uses'=>'PromoController@create']);
		Route::get('promo-edit/{id}',['as'=>'catalog_promo_edit','uses'=>'PromoController@edit']);
		Route::post('promo-save',['as'=>'catalog_promo_save','uses'=>'PromoController@save']);
		Route::post('promo-delete',['as'=>'catalog_promo_delete','uses'=>'PromoController@delete']);
	    Route::get('promo/status/{id}/{status}',['as'=>'catalog_promo_status','uses'=>'PromoController@status']);
	        
		//orders
		Route::get('orders',['as'=>'catalog_orders_index','uses'=>'OrderController@index']);
		Route::get('order-detail/{id}',['as'=>'catalog_orders_detail','uses'=>'OrderController@detail']);
		Route::post('order-save',['as'=>'catalog_orders_save','uses'=>'OrderController@save']);
		Route::post('order-settlement',['as'=>'catalog_orders_settlement','uses'=>'OrderController@settlement']);

		Route::get('order-change-status/{id}/{status}',['as'=>'catalog_orders_status','uses'=>'OrderController@changeOrderStatus']);
		Route::get('order-cancel-pickup/{id}',['as'=>'catalog_orders_cancel_pickup','uses'=>'OrderController@cancelPickup']);

		Route::post('orders-get-data',['as'=>'catalog_orders_get_data','uses'=>'OrderController@getData']);

		Route::post('orders-confirm-payment',['as'=>'catalog_orders_confirm_payment','uses'=>'OrderController@confirmPayment']);
		Route::post('orders-shipping-info',['as'=>'catalog_orders_shipping_info','uses'=>'OrderController@saveShippingInfo']);
		Route::post('orders-cancel-order',['as'=>'catalog_orders_cancel_order','uses'=>'OrderController@cancelOrder']);
		Route::post('orders-req-shipment',['as'=>'catalog_orders_req_shipment','uses'=>'OrderController@reqShipment']);
		Route::post('orders-pickup',['as'=>'catalog_orders_pick_up','uses'=>'OrderController@pickup']);


		Route::get('orders/export',['as'=>'catalog_orders_export','uses'=>'OrderController@exportData']);
		Route::get('orders/print-packing-list/{id}',['as'=>'catalog_orders_print_packing_list','uses'=>'OrderController@printPacking']);
		Route::get('orders/print-invoice/{id}',['as'=>'catalog_orders_print_invoice','uses'=>'OrderController@printInvoice']);


		//brands
		Route::post('brands/get-data',['as'=>'catalog_brands_get_data','uses'=>'BrandController@getData']);
		Route::post('brands/add',['as'=>'catalog_brands_add_data','uses'=>'BrandController@create']);
		Route::post('brands/edit',['as'=>'catalog_brands_edit_data','uses'=>'BrandController@edit']);
		Route::get('brands',['as'=>'catalog_brands_index','uses'=>'BrandController@index']);


		Route::get('accounts',['as'=>'catalog_accounts_index','uses'=>'AccountController@index']);
		Route::post('save-accounts',['as'=>'catalog_accounts_save','uses'=>'AccountController@saveData']);
		ROute::post('delete-account',['as'=>'catalog_accounts_delete','uses'=>'AccountController@deleteData']);
		

		Route::get('stores',['as'=>'catalog_store_index','uses'=>'StoreController@index']);
		Route::get('stores/create',['as'=>'catalog_store_create','uses'=>'StoreController@create']);
		Route::get('stores/edit/{id}',['as'=>'catalog_store_edit','uses'=>'StoreController@edit']);

		Route::post('save-stores',['as'=>'catalog_store_save','uses'=>'StoreController@save']);
		ROute::post('delete-stores',['as'=>'catalog_store_delete','uses'=>'StoreController@deleteData']);



		Route::get('payment-methods',['as'=>'catalog_payment_method_index','uses'=>'PaymentChannelController@index']);
		Route::post('save-payment-method',['as'=>'catalog_payment_method_save','uses'=>'PaymentChannelController@saveData']);
		ROute::post('delete-payment-method',['as'=>'catalog_payment_method_delete','uses'=>'PaymentChannelController@deleteData']);


		Route::get('sales-channels',['as'=>'catalog_sales_channels_index','uses'=>'SalesChannelController@index']);
		Route::post('save-sales-channels',['as'=>'catalog_sales_channels_save','uses'=>'SalesChannelController@saveData']);
		ROute::post('delete-sales-channels',['as'=>'catalog_sales_channels_delete','uses'=>'SalesChannelController@deleteData']);




		//pos
		Route::group(['prefix'=>'pos'],function(){
			Route::get('view/{id?}',['as'=>'catalog_pos_view','uses'=>'PosController@index']);
			Route::any('get-user',['as'=>'catalog_pos_user','uses'=>'PosController@getUser']);
			Route::post('save-user',['as'=>'catalog_pos_save_user','uses'=>'PosController@saveUser']);
			Route::post('save-address',['as'=>'catalog_pos_save_address','uses'=>'PosController@saveAddress']);
			Route::post('get-dhl-quote',['as'=>'catalog_post_get_dhl','uses'=>'PosController@getDHLQUote']);

			Route::get('get-city-district',['as'=>'catalog_pos_get_city_district','uses'=>'PosController@getCityDistrict']);
			Route::any('search-products',['as'=>'catalog_pos_get_products','uses'=>'PosController@searchProducts']);
			Route::post('place-order',['as'=>'catalog_pos_place_order','uses'=>'PosController@placeOrder']);
		});

		Route::get('shopping-carts',['as'=>'alpha_catalog_shopping_carts','uses'=>'CartController@index']);
		Route::get('delete-cart/{any}',['as'=>'alpha_catalog_delete_cart','uses'=>'CartController@deleteCart']);
		Route::any('shopping-carts-get-data',['as'=>'alpha_catalog_shopping_carts_get_data','uses'=>'CartController@getData']);
	});


});

//clean up cart
Route::get('clean-up-cart',['as'=>'catalog_cart_clean_up','uses'=>'Morra\Catalog\Controllers\CoreCartController@clean']);
Route::get('clean-up-order',['as'=>'catalog_order_clean_up','uses'=>'Morra\Catalog\Controllers\CoreCartController@cleanUpExpiredOrder']);