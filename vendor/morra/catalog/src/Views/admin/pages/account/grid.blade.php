                            <tr>
                                <td class="title-column">
                                        <h6>
                                            <a class="trigger_edit" href="javascript:voi(0)">                                         
                                                {{($account->name)}}
                                            </a>
                                        </h6>
                                    </td>
                                    <td class="alpha-desc">
                                        {{ $account->description }}
                                    </td>
                                    <td class="td-date">
                                        {{date_format(date_create($account->created_at),app('AlphaSetting')->getSetting('date_format'))}}
                                        <br><span class="text-muted">{{date_format(date_create($account->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                    </td>
                                    <td class="td-date">
                                        {{date_format(date_create($account->updated_at),app('AlphaSetting')->getSetting('date_format'))}}
                                        <br><span class="text-muted">{{date_format(date_create($account->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                    </td>
                                    <td class="text-right table-actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                            </button>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a id="data_list_{{$account->id}}" class="edit-data" data-id="{{ $account->id }}" data-desc="{{ strip_tags(($account->description)) }}" data-channels='{!! $account->sales_channels !!}'  data-name="{{ ($account->name) }}" href="javascript:void(0)"><i class="icon-pencil5"></i> Edit</a></li>
                                                <li class="divider"></li>
                                                <li><a href="javascript:voi(0)" class="confirm" data-id="{{$account->id}}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                            </tr>