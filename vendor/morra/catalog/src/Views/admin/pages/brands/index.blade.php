
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Stock Inventory</span> - Brands</h4>
        </div>
        <div class="heading-elements">
            <a href="#" class="btn btn-labeled bg-brand heading-btn" id="addBrand"><b><i class="icon-file-plus"></i></b>New Brand</a>
        </div>
    </div>
</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h1 class="text-semibold">Brands</h1>
                    </div>
                </div>
                <div class="panel-body">
                    <p>All registered brand available.</p>
                </div>
                <div class="table-header clearfix mb-10">
                    <div class="filter-table clearfix pull-left mr-5">
                        <div class="form-group pull-left  mr-5 no-margin-bottom">
                            <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter</label>
                        </div>
                        <div class="form-group pull-left has-feedback  no-margin-bottom">
                            <input type="text" class="form-control" id="search_the_table" style="height:36px">
                            <div class="form-control-feedback" style="line-height:36px;">
                                <i class="icon-search4" style="font-size:12px"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <table id="tableBrands" class="table entry-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th width="90px">Count</th>
                            <th width="140px"><span>Created</span></th>
                            <th width="140px"><span>Modified</span></th>
                            <th width="50px" class="no-sort text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>

        </div>

    </div>

    <div id="modal_brand" class="modal fade" style="display: none;">
        <div class="modal-dialog" style="width:550px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h5 class="modal-title text-bold">Edit Brand</h5>
                </div>

                <div class="modal-body">
                    <div style="display:none" id="modalErrMsg" class="alert bg-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            <span class="text-semibold">Required field(s) error or missing</span>
                    </div>
                    <form>
                        <input type="hidden" name="" id="brand_id">
                        <div class="form-group">
                            <label>Name</label>
                            <input id="brand_name" type="text" class="form-control" placeholder="Brand name" value="Brand Name">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea id="brand_desc" class="form-control" placeholder="Brand description">Brand Description</textarea>
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" name="" id="brand_image" class="form-control">
                            <p id="brand_preview" class="help-block"><a target="_blank" href="javascript:void(0)">Preview Image</a></p>    
                        </div>
                    </form>
                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                    <button id="saveBrand" type="button" class="btn btn-primary select-image">Save</button>
                </div>
            </div>
        </div>
    </div>
    <form style="display:none" id="form_delete" action="{{route('alpha_admin_entry_delete',[0])}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="0" >
                    </form>
     <script>
                    var entry_table = {};
                    $(document).ready(function() {
                        
                        $('#add_new_brand').click(function(e){
                            e.preventDefault();
                            $('#modal_add_brand').modal();
                        });

                        $('.edit_brand').click(function(e){
                            e.preventDefault();
                            $('#modal_edit_brand').modal();
                        });
                        
                    });

                    $(document).ready(function() {

                           var childClass = 'title-column';
                            entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                             $('.dataTable, .dataTable tbody').css('position','static');
                                             $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                         $('.dataTable, .dataTable tbody').css('position','relative');
                                        $.unblockUI();
                                        $(".blockUI").remove();
                                     }
                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "order": [],
                                "pageLength": 50,
                                "searching" : true,
                                "lengthChange": false,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "ajax": {
                                    "url": "{{ route('catalog_brands_get_data') }}",
                                    "type": "POST"
                                },
                                "columns": [

                                    { "data": "title", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:"title-column"
                                    },

                                    { "data": "media_count", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    }

                                    },

                                  
                                    { "data": "created", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "updated", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[4];
                                    },
                                    sClass:"text-right table-actions"

                                    }

                                   
                                ],
                                "columnDefs": [
                                    {
                                        "targets": 'no-sort',
                                        "orderable": false,
                                    }
                                ]
                            });
                           
                            var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                });
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                            $('.table-select2').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('.table-select2.with-search').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: 1,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13){
                                    entry_table.search($(this).val()).draw();
                                }
                            });
                        });
                </script>
                <script type="text/javascript">
                    function initNameToolTip()
                    {
                        $("#brand_name").tooltip({
                                    animation:true,
                                    placement:'bottom',
                                    title:"The name field is required.",
                                    trigger:"focus",
                                    template:'<div class="tooltip red-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                                });
                        $("#brand_name").parent().addClass('has-error');
                    }


                    function initDescToolTip()
                    {
                        $("#brand_desc").tooltip({
                                    animation:true,
                                    placement:'bottom',
                                    title:"The description field is required.",
                                    trigger:"focus",
                                    template:'<div class="tooltip red-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                                });
                        $("#brand_desc").parent().addClass('has-error');    
                    }
                    
                    function destroyInit()
                    {
                         $("#brand_name").tooltip("destroy");
                         $("#brand_desc").tooltip("destroy");
                         $("#brand_name").parent().removeClass('has-error');
                         $("#brand_desc").parent().removeClass('has-error');
                         $("#modalErrMsg").hide();    
                    }

                    initNameToolTip();
                    initDescToolTip();

                    var urlCreateBrand = "{{ route('catalog_brands_add_data') }}";
                    var urlEditBrand = "{{ route('catalog_brands_edit_data') }}";
                    var urlSaveBrand = urlCreateBrand;
                    var opener = {};

                    $("#addBrand").click(function(e){
                        e.preventDefault();
                        urlSaveBrand = urlCreateBrand;
                        destroyInit();
                        $("#brand_name").val("");
                        $("#brand_desc").val("");
                        $("#brand_image").val("");
                        $("#brand_preview").hide();
                        $("#brand_id").val(0);
                        $("#modal_brand").find(".modal-title").html('Create Brand');
                        $("#modal_brand").modal('show');
                    });

                    $(document).on('click','.trigger_edit',function(e){
                        e.preventDefault();
                        $(this).parents('tr').find('.edit-brand').click();
                    });

                    $(document).on('click','.edit-brand',function(e){
                        e.preventDefault();
                        urlSaveBrand = urlEditBrand;
                        destroyInit();
                        opener = $(this).parent().parent().parent().parent().parent();
                        $("#brand_name").val($(this).attr('data-name'));
                        $("#brand_desc").val($(this).attr('data-desc'));
                        $("#brand_id").val($(this).attr('data-id'));
                        $("#brand_image").val("");
                        $("#brand_preview").find('a').attr('href',$(this).attr('data-image'));
                        $("#brand_preview").show();
                        $("#modal_brand").find(".modal-title").html('Edit Brand');
                        $("#modal_brand").modal('show');
                    });

                    $("#saveBrand").click(function(e){
                        e.preventDefault();
                        destroyInit();

                        if($("#brand_name").val().trim() == "" || $("#brand_desc").val().trim() == "")
                        {
                            $("#modalErrMsg").show();

                            if($("#brand_name").val().trim() == ""){
                                 initNameToolTip();
                            }

                            if($("#brand_desc").val().trim() == ""){
                                 initDescToolTip();
                            }
                            return false;
                        }
                        var data = new FormData();
                        data.append('id',$("#brand_id").val());
                        data.append('name',$("#brand_name").val());
                        data.append('desc',$("#brand_desc").val());
                        if(document.getElementById("brand_image").files.length == 1){
                            data.append('image',document.getElementById("brand_image").files[0]);
                        }
                        $.ajax({
                            url:urlSaveBrand,
                            type:"POST",
                            enctype: 'multipart/form-data',
                            processData: false,  
                            contentType: false,   
                            dataType:"json",
                            data:data,
                            success:function(data)
                            {
                                if(data.status == 1)
                                {
                                    $("#modal_brand").modal('hide');
                                    swal({
                                        title: "SUCCESS",
                                        text: "All changes has been saved successfuly",
                                        confirmButtonColor: "#66BB6A",
                                        type: "success",
                                        html: true
                                    });

                                    if(urlSaveBrand == urlCreateBrand)
                                    {
                                        entry_table.row.add(data.html).draw();
                                    }else{
                                        $(opener).find('.edit-brand').attr('data-name',$("#brand_name").val());
                                        $(opener).find('.edit-brand').attr('data-desc',$("#brand_desc").val());
                                        if(data.image != ""){
                                            $(opener).find('.edit-brand').attr('data-image',data.image);                                            
                                        }
                                    }
                                }else{
                                    $("#modalErrMsg").show();
                                }
                            }
                        });
                    });
                </script>
<script type="text/javascript">
                        var entryGetDataUrl = "{{ route('alpha_admin_entry_get_data','brand') }}";
                        
                        $(document).on('click',".confirm",function(e){
                                e.preventDefault();
                                $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).attr('data-id')));
                                $("#form_delete").find("[name=id]").val($(this).data('id'));
                                 swal({
                                title: "Delete ?",
                                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                closeOnConfirm: false },
                                function(isConfirm){
                                    if(isConfirm){
                                        $("#form_delete").submit();
                                    }
                                });
                            });

                    </script>
    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
