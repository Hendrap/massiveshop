                                <td class="title-column">
                                    <h6>
                                        <a class="trigger_edit" href="javascript:voi(0)">                                         
                                            {{parseMultiLang($entry->title)}}
                                        </a>
                                    </h6>
                                </td>
                                |alpha--datatable-separator--|<td>{{(int)$entry->brand_count}}</td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                    {{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('date_format'))}}
                                    <br><span class="text-muted">{{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                |alpha--datatable-separator--|<td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <?php 
                                                $image = "javascript:void(0)";
                                                if(!empty($entry->medias[0])){
                                                    $image = asset($entry->medias[0]->path);
                                                }

                                             ?>
                                            <li><a id="brand_list_{{$entry->id}}" class="edit-brand" data-id="{{ $entry->id }}" data-desc="{{ strip_tags(parseMultiLang($entry->content)) }}"  data-name="{{ parseMultiLang($entry->title) }}" data-image="{{ $image }}" href="javascript:void(0)"><i class="icon-pencil5"></i> Edit</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:voi(0)" class="confirm" data-id="{{$entry->id}}"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>