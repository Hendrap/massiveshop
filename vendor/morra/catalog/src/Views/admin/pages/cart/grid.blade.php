                            <td class="title-column">
                                <h6><a data-modal='modal_view_cart_{{$cart->id}}' href="javascript:void(0)" class="open-cart">CART {{ $cart->id }}</a></h6>
                                @if(!empty($cart->user))
                                <span class="text-muted">{{generateName($cart->user)}}</span>
                                @else
                                <span class="text-muted">{{ $cart->session_id }}</span>
                                @endif
                                {!! view('catalog::admin.pages.cart.modal',['cart'=>$cart]) !!}
                            </td>
                            |alpha--datatable-separator--|<td class="td-date">{{ $cart->qty }}</td>
                            |alpha--datatable-separator--|<td class="td-date">
                                <span class="text-bold table-price">{{ number_format($cart->original_price) }}</span>
                            </td>
                            |alpha--datatable-separator--|<td class="td-date">
                                {{ date(app('AlphaSetting')->getSetting('date_format'),strtotime($cart->updated_at)) }}
                                <br><span class="text-muted">{{ date(app('AlphaSetting')->getSetting('time_format'),strtotime($cart->updated_at)) }}</span>
                            </td>
                            |alpha--datatable-separator--|<td class="text-right table-actions">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="border-top"><a class="deleteCart" href="{{ route('alpha_catalog_delete_cart',[$cart->id]) }}"><i class="icon-trash"></i> Delete</a></li>
                                    </ul>
                                </div>
                            </td>
