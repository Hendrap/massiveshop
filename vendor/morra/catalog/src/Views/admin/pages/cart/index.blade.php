<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Outgoing</span> - Shopping Carts</h4>
        </div>

       

    </div>


</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">


    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h1 class="text-semibold">Shopping Carts</h1>
                        
                    </div>


                </div>
                <div class="panel-body">

                    <p>Active shopping cart list</p>
                    

                </div>


                <div class="table-header clearfix mb-10">

                    <div class="filter-table clearfix pull-left mr-5">
                        <div class="form-group pull-left  mr-5 no-margin-bottom">
                            <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter</label>
                        </div>
                        <div class="form-group pull-left has-feedback  no-margin-bottom">
                            <input type="text" class="form-control" id="search_the_table" style="height:36px">
                            <div class="form-control-feedback" style="line-height:36px;">
                                <i class="icon-search4" style="font-size:12px"></i>
                            </div>
                        </div>

                    </div>






                </div>



                <table class="table entry-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th width="120px">Total Item</th>
                            <th width="160px"><span>Amount (IDR)</span></th>
                            <th width="140px"><span>Expiry</span></th>

                            <th width="50px" class="no-sort text-right"></th>
                        </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>

                <script>
                    $(document).ready(function() {



                         var entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                             $('.dataTable, .dataTable tbody').css('position','static');
                                           $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                        $.unblockUI();
                                        $(".blockUI").remove();
                                         $('.dataTable, .dataTable tbody').css('position','relative');
                                        $("[data-popup=popover]").popover();
                                     }
                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "order": [],
                                "pageLength": 50,
                                "searching" : true,
                                "lengthChange": false,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "ajax": {
                                    "url": "{{ route('alpha_catalog_shopping_carts_get_data') }}",
                                    "type": "POST"
                                },
                                "columns": [

                                    { "data": "id", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:"title-column"
                                    },

                                    { "data": "qty", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    },
                                    sClass:"td-date"

                                    },

                                  
                                    { "data": "original_price", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "updated_at", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[4];
                                    },
                                    sClass:"text-right table-actions"

                                    }

                                   
                                ],
                                "columnDefs": [
                                    {
                                        "targets": 'no-sort',
                                        "orderable": false,
                                    }
                                ]
                            });

                        var filterby;
                        var searchin;

                        $('.filter-table-column').change(function() {

                            thisfilter = $(this);
                            thisfilter.children('option:selected').each(function() {
                                filterby = $(this).val();
                                searchin = thisfilter.attr('searchin');
                            });

                            if (filterby == 'all') {
                                entry_table.columns(searchin).search('').draw();
                            } else {
                                entry_table.columns(searchin).search(filterby).draw();
                            }

                        });

                        $('#search_the_table').keyup(function(e) {
                            if(e.which == 13){
                                entry_table.search($(this).val()).draw();                                
                            }

                        });

                        $('select').select2({
                            minimumResultsForSearch: Infinity,
                            width: 'auto'
                        });

                        $(document).on('click','.open-cart',function(e){
                            e.preventDefault();
                            $("#" + $(this).attr('data-modal')).modal('show');
                        })
                        $('.table-select2').each(function() {

                            select_placeholder = $(this).attr('placeholder');
                            $(this).select2({
                                minimumResultsForSearch: Infinity,
                                placeholder: select_placeholder,
                                width: '200px',
                            });

                        });
                                            
                        
                    });
    

                     $(document).on('click',".deleteCart",function(e){
                                e.preventDefault();
                                var that = $(this);
                                 swal({
                                title: "Delete ?",
                                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                closeOnConfirm: false },
                                function(isConfirm){
                                    if(isConfirm){
                                        window.location.href = $(that).attr('href');
                                    }
                                });
                            });
                </script>


            </div>

        </div>

    </div>


   
    
    



     <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->






