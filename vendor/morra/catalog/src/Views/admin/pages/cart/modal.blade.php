 <div id="modal_view_cart_{{$cart->id}}" class="modal fade modal-view-cart" style="display: none;">
        <div class="modal-dialog" style="width:600px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h5 class="modal-title text-bold">CART {{$cart->id}}</h5>
                </div>

                <div class="modal-body">
                    @if(!empty($cart->user))
                    <p>{{generateName($cart->user)}}, {{$cart->user->email}}</p>
                    @else
                    <p>{{ $cart->session_id }}</p>
                    @endif
                </div>
                <table class="table pb-10 border-bottom border-top" style="border-color:#ddd">
                        
                            <thead>
                            
                                <tr>
                                    <th width="30px">Item</th>
                                    <th></th>
                                    <th width="60px">Qty</th>
                                    <th width="140px"><span>Unit Price (IDR)</span></th>
                                    <th width="120px"><span>Subtotal (IDR)</span></th>
                                </tr>
                                
                            </thead>
                            <tbody>
                                
                                <tr>
                                <?php 
                                    $html = '';

                                    if(!empty($cart->item->entry->medias[0])){
                                        $html = "<img src='".asset(getCropImage($cart->item->entry->medias[0]->path,'default'))."' alt='".parseMultiLang(@$cart->item->entry->title)."' style='width:200px'>";
                                    }

                                 ?>
                                    <td><a href="javascript:void(0)" class="catalog-preview" data-popup="popover" data-trigger="hover" data-content="<?php echo $html ?>" data-html="true"> <i class="icon-camera"></i></a></td>
                                    <td class="text-bold no-padding-left">
                                        <h6>{{ parseMultiLang(@$cart->item->entry->title) }}</h6>
                                        <span class="text-muted">{{ @$cart->item->sku }}</span>
                                    </td>
                                    <td>{{ $cart->qty }}</td>
                                    <td><span class="text-bold text-right display-block pr-8 table-price">{{ number_format($cart->original_price) }}</span></td>
                                    <td><span class="text-bold text-right display-block pr-8 table-price">{{ number_format($cart->original_price * $cart->qty) }}</span></td>
                                </tr>
                                
                                
                            </tbody>
                            
                       </table>
                <div class="modal-footer">
                
                    <div class="pull-right clearfix popup-cart-footer mt-10">
                    
                        <div class="pull-left" style="margin-right:50px;">
                            
                            <h6>Total Item</h6>
                            <span>1</span>
                        
                        </div>
                        <div class="pull-left">
                            <h6>Amount (IDR)</h6>
                            <span>{{ number_format($cart->original_price * $cart->qty) }}</span>
                        </div>
                    
                    </div>
                
                </div>
                
                

            </div>
        </div>
    </div>
    