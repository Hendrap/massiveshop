                            <tr>
                                <td class="title-column">
                                        <h6>
                                            <a class="trigger_edit" href="javascript:voi(0)">                                         
                                                {{($channel->name)}}
                                            </a>
                                        </h6>
                                    </td>
                                    <td class="alpha-desc">
                                        {{ $channel->description }}
                                    </td>
                                    <td class="td-date">
                                        {{date_format(date_create($channel->created_at),app('AlphaSetting')->getSetting('date_format'))}}
                                        <br><span class="text-muted">{{date_format(date_create($channel->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                    </td>
                                    <td class="td-date">
                                        {{date_format(date_create($channel->updated_at),app('AlphaSetting')->getSetting('date_format'))}}
                                        <br><span class="text-muted">{{date_format(date_create($channel->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                    </td>
                                    <td class="text-right table-actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                            </button>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a id="data_list_{{$channel->id}}" class="edit-data" data-type="{{ $channel->type }}" data-payments='{!! $channel->payment_methods !!}' data-id="{{ $channel->id }}" data-desc="{{ strip_tags(($channel->description)) }}"  data-name="{{ ($channel->name) }}" href="javascript:void(0)"><i class="icon-pencil5"></i> Edit</a></li>
                                                <li class="divider"></li>
                                                <li><a href="javascript:voi(0)" class="confirm" data-id="{{$channel->id}}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                            </tr>