                            <?php 
                                if(empty($msrp)) $msrp = 0;
                                if(empty($price)) $price = 0;
                                if(empty($sale_price)) $sale_price = 0;

                                $msrp = str_replace(',', '', $msrp);
                                $price = str_replace(',', '', $price);
                                $sale_price = str_replace(',', '', $sale_price);

                             ?>
                             <tr>
                                <td>
                                    <input type="hidden" value="{{ $item_medias }}" class="media_id" name="item_medias[]">
                                    <input type="hidden" value="{{ $item_taxonomies }}" class="item_taxonomies" name="item_taxonomies[]">
                                    <input type="hidden" value="{{ $item_metas }}" name="item_metas[]">
                                    <input type="hidden" value="{{ $item_id }}" name="item_id[]">
                                    <input readonly="" value="{{ $qty_available }}" name="qty_available[]" class="form-control qty" type="hidden">
                                    <a href="javascript:void(0)" class="select-variant-image" data-toggle="tooltip" data-placement="top" title="Variant Image"><i class="icon-camera"></i></a>&nbsp;&nbsp;
                                    <a href="javascript:void(0)" class="select-variant-category" data-toggle="tooltip" title="Variant Category"><i class="icon-folder2"></i></a>
                                </td>
                                <td><input value="{{ $sku }}" name="sku[]" class="alpha-sku form-control" type="text"></td>
                                <td><input value="{{ $item_title }}" name="item_title[]" class="form-control" type="text"></td>
                                <td><input value="{{ str_replace(',','',number_format($msrp) )}}" name="msrp[]" class="form-control price" type="text"></td>
                                <td><input value="{{ $qty }}" name="qty[]" class="form-control qty" type="text"></td>
                                <td><input value="{{ $order_limit }}" name="order_limit[]" class="form-control qty" type="text"></td>
                                <td><input value="{{ str_replace(',','',number_format($price)) }}" name="price[]" class="form-control price price-check" type="text"></td>
                                <td>
                                    <div class="form-group has-feedback no-margin">
                                        <input value="{{ str_replace(',','',number_format($sale_price)) }}" name="sale_price[]" type="text" class="sale-price-check form-control price">
                                        <div class="sale-date-trigger" style="background:#ccc;">
                                            <a href="javascript:void(0)" class="sales-date"><i class="icon-calendar"></i></a>
                                            <input value="{{ $sale_start }}" class="sale_start" type="hidden" name="sale_start[]">
                                            <input value="{{ $sale_end }}" class="sale_end" type="hidden" name="sale_end[]">
                                        </div>
                                        @if(!empty($sale_start))
                                            <span class="salesdate-string">{{ date('d/m/Y',strtotime($sale_start)).' - '.date('d/m/Y',strtotime($sale_end)) }} </span>
                                        @else
                                            <span class="salesdate-string">&nbsp;</span>
                                        @endif
                                    </div>
                                </td>
                                <td class="pl-10">
                                    <div class="variant-action-container">
                                    <a href="javascript:void(0)" class="delete-variant " data-toggle="tooltip" title="Delete Variant"><i class="icon-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
