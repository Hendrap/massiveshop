<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Outgoing</span> - View Order #{{ $order['order']['order_number'] }}</h4>
        </div>

    </div>


</div>

<!-- /page header -->


<!-- Content area -->
<div class="content">
    <form action="#" method="post" class="form-horizontal">
        <div class="row">
            <div class="col-sm-9">
                <div class="panel panel-flat">
                    <div class="panel-heading clearfix">
                         <div class="pull-right clearfix">
                            @if($order['status'] != 'CANCELLED')
                            <a href="{{ route('catalog_pos_view',[$order['id']]) }}" class="btn bg-grey pull-left mr-10">Edit</a>
                            <div class="btn-group btn-group-fade">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Print<span class="caret display-inline-block ml-5"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a target="_blank" href="{{ route('catalog_orders_print_invoice',[$order['order']['id']]) }}"><i class="icon-calculator2"></i> Invoice</a></li>
                                        <li><a target="_blank" href="{{ route('catalog_orders_print_packing_list',[$order['order']['id']]) }}"><i class="icon-truck"></i> Packing List</a></li>
                                    </ul>
                            </div>
                            @endif
                        </div>
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Order #{{ $order['order']['order_number'] }}</h2>
                        </div>
                    </div>
                    <div class="panel-body">
                       <div class="row mb-50" style="z-index: 99;position: relative;">
                           <div class="col-sm-4">
                                <div class="form-group" style="padding: 0 10px">
                                    <label>Customer Information</label>
                                    <div class="customer-info">
                                        <h5>{{ $order['user']['first_name'].' '.$order['user']['last_name'] }}</h5>
                                        <div class="info-box">
                                            <span>{{ $order['user']['email'] }}</span>
                                            <span>{{ $order['user']['phone'] }}</span>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           <div class="col-sm-4">
                                <div class="form-group" style="padding: 0 10px">
                                    <label>Shipping Address</label>
                                    <div class="customer-info">
                                        <h5>{{ $order['shipping']['first_name'].' '.$order['shipping']['last_name'] }} - {{$order['shipping']['label']}}</h5>
                                        <div class="info-box">
                                            <span>{{ $order['shipping']['address'] }}</span>
                                            <span>{{ $order['shipping']['district'] }}, {{ $order['shipping']['city'] }}</span>
                                            <span>{{ $order['shipping']['province'] }} {{ $order['shipping']['postal_code'] }}</span>
                                        </div>
                                        <div class="info-box">
                                            <span>{{ $order['shipping']['phone'] }}</span>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           <div class="col-sm-4">
                                 <div class="form-group" style="padding: 0 10px">
                                    <label>Billing Address</label>
                                    
                                    <div class="customer-info">
                                        <h5>{{ $order['billing']['first_name'].' '.$order['billing']['last_name'] }} - {{$order['billing']['label']}}</h5>
                                        <div class="info-box">
                                            <span>{{ $order['billing']['address'] }}</span>
                                            <span>{{ $order['billing']['district'] }}, {{ $order['billing']['city'] }}</span>
                                            <span>{{ $order['billing']['province'] }} {{ $order['billing']['postal_code'] }}</span>
                                        </div>
                                        <div class="info-box">
                                            <span>{{ $order['billing']['phone'] }}</span>
                                        </div>
                                       
                                       
                                    </div>
                                </div>
                           </div>
                           
                           
                       </div>
                        
                        

                       <table class="table pb-10 border-bottom table-order-items mb-50"  style="border-color:#ddd;z-index: 95;position: relative;">
                        
                            <thead>
                            
                                <tr>
                                    <th width="30px">Item</th>
                                    <th></th>
                                    <th width="95px">Qty</th>
                                    <th width="135px"><span>Unit Price (IDR)</span></th>
                                    <th width="135px"><span class="no-padding-right">Discount (IDR)</span></th>
                                    <th width="135px"><span>Subtotal (IDR)</span></th>


                                </tr>
                                
                            </thead>
                            <tbody>
                                @foreach($order['products'] as $key => $value)
                                <tr>
                                    <?php 
                                        $html = '';
                                        if(!empty($value['image_thumbnail']) && $value['image_thumbnail'] != asset('')){
                                            $html = "<img src='".$value['image_thumbnail']."' alt='".$value['product_title']."' style='width:200px'>";                                            
                                        }
                                     ?>
                                    @if(!empty($html))
                                    <td><a href="javascript:void(0)" class="catalog-preview" data-popup="popover" data-trigger="hover" data-content="<?php echo $html ?>" data-html="true"> <i class="icon-camera"></i></a></td>
                                    @else
                                    <td><a href="javascript:void(0)" class="catalog-preview no-image"> <i class="icon-camera"></i></a></td>
                                    @endif
                                    <td class="text-bold">
                                        <h6>{{ $value['product_title'] }}</h6>
                                        <span class="text-muted">{{ $value['sku'] }}</span>

                                    </td>
                                    <td class="text-right">{{ $value['stock_qty'] }}</td>
                                    <td><span class="text-bold text-right display-block table-price">{{ formatMoney($value['original_price'],'idr') }}</span></td>
                                    <td class="text-right"><span>{{ formatMoney($value['discount'],'idr') }}</span></td>
                                    <td><span class="text-bold text-right display-block table-price">{{ formatMoney($value['total'],'idr') }}</span></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                       
                         <div class="row pt-20">
                            @if(!empty($order['order']['customer_note']))
                            <div class="col-md-6">
                                <h3 class="customer-note-heading">Customer Note</h3>
                            </div>
                            @endif

                            @if(!empty($order['order']['gift_wrapping']))
                            <div class="col-md-6">
                                <h3 class="customer-note-heading"> <img src="{{ asset('morra/catalog/assets/img/Gift.svg') }}" alt="gift-wrap" style="width:14px;vertical-align:middle;margin-top:-5px;" class="display-inline-block">&nbsp;Gift Warping Required</h3>
                            </div>
                            @endif                        
                        </div>
                        <div class="row order-additional-note-content">
                            @if(!empty($order['order']['customer_note']))
                            <div class="col-md-6">
                                
                                <div class="form-group no-margin">
                                
                                    <p>{{ $order['order']['customer_note'] }}</p>
                                    
                                </div>
                            </div>
                            @endif
                            @if(!empty($order['order']['gift_wrapping']))          
                            <div class="col-md-6">
                                
                                <div class="form-group no-margin">
                                
                                    <p>{{ $order['order']['gift_wrapping'] }}</p>
                                    
                                </div>
                            </div>
                             @endif
                        </div>

                    </div>


                </div>
                <div class="panel panel-flat">

                    <div class="panel-heading clearfix">




                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Transaction Log</h1>
                        </div>
                        

                    </div>
                    <div class="panel-body no-padding"></div>
                    
                    <table class="table table-transaction-log">
                    
                        <thead>
                            <tr>
                                <th width="140px">
                                    <span>Time</span>
                                </th>
                                <th>
                                    Activity
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($log as $key => $value)
                                    <tr>
                                        <td class="td-date">
                                            {{ date(app('AlphaSetting')->getSetting('date_format'),strtotime($value->created_at)) }}
                                            <br><span class="text-muted">{{ date(app('AlphaSetting')->getSetting('time_format'),strtotime($value->created_at)) }}</span>
                                        </td>
                                        <td>
                                            @if(!empty($value->info))
                                                {{ $value->info }}
                                            @else
                                                @if($value->type == 'PACKED')
                                                    Order packed by {{@$value->user->username}}
                                                @endif

                                                @if($value->type == 'CANCELLED')
                                                    Order cancelled by {{@$value->user->username}}
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                        @endforeach

                        </tbody>
                    
                    </table>
                    
                </div>
                

            </div>



            <div class="col-sm-3">

                <div class="panel panel-flat right-small payment-details">
                                    
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            
                            <div class="btn-group btn-group-fade mb-20" style="width:100%">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style="width:100%">{{$order['status']}}<span class="caret pull-right" style="margin-top:3px;"></span></button>
                                    @if($order['status'] != 'CANCELLED') 
                                    <ul class="dropdown-menu" style="width:100%">
                                            @if($order['status'] == 'PENDING')
                                            <li><a data-id="{{$order['id']}}" href="javascript:void(0)" class="confirm-payment-trigger"><i class="icon-checkmark4"></i> Confirm Payment</a></li>
                                            @endif

                                            @if($order['status'] == 'APPROVED')    
                                            <li><a href="{{route('catalog_orders_status',[$order['id'],'PACKED'])}}" class=""><i class="icon-gift"></i>PACKED</a></li>
                                            @endif  

                                            @if($order['status'] == 'PACKED')    
                                            <li><a href="{{route('catalog_orders_status',[$order['id'],'SHIPMENT'])}}" data-id="{{$order['id']}}" class="request-shipment"><i class="icon-gift"></i> Req. Shipment</a></li>
                                            @endif

                                            @if($order['status'] == 'SHIPMENT')    
                                            <li><a href="{{route('catalog_orders_status',[$order['id'],'PICKUP'])}}" data-id="{{$order['id']}}" class="trigger-pickup"><i class="icon-gift"></i> PICKUP</a></li>
                                            @endif     

                                            @if($order['status'] == 'PICKUP')
                                            <li><a data-id="{{$order['id']}}" href="javascript:void(0)" class="trigger_order_shipping"><i class="icon-truck"></i> Ship Order</a></li>
                                            @endif


                                            @if($order['status'] == 'PICKUP')
                                            <li><a data-id="{{$order['id']}}" class="trigger_cancel_pickup" href="{{route('catalog_orders_cancel_pickup',[$order['id']])}}"><i class="icon-cancel-square"></i> Cancel Pickup</a></li>
                                            @endif


                                            @if($order['status'] != 'CANCELLED')
                                            <li class="border-top"><a data-id="{{$order['id']}}" class="trigger_order_cancel" href="{{route('catalog_orders_status',[$order['id'],'CANCELLED'])}}"><i class="icon-cancel-square"></i> Cancel Order</a></li>
                                            @endif
                                    </ul>
                                    @endif
                            </div>
                            
                            
                            <h2 class="text-semibold mb-10">Order Summary</h2>

                        </div>


                    </div>
                    <div class="panel-body">
                        <h6>Amount (IDR)</h6>
                        <span class="total-big">{{ formatMoney($order['order']['grand_total'],'idr') }}</span>
                        
                        <div class="clearfix">
                            
                            <span class="pull-left">Subtotal (IDR)</span>
                            <span class="pull-right">{{ formatMoney($order['order']['subtotal_after_discount'],'idr') }}</span>
                        </div>

                         <div class="clearfix">
                        
                            <span class="pull-left">Discount (IDR)</span>
                            <span class="pull-right">{{ formatMoney($order['order']['global_discount'],'idr') }}</span>
                        </div>

                        <div class="clearfix">
                        
                            <span class="pull-left">Shipping (IDR)</span>
                            <span class="pull-right">{{ formatMoney($order['order']['shipping_amount'],'idr') }}</span>
                        </div>
                        <div class="clearfix">
                        
                            <span class="pull-left">Total item(s)</span>
                            <span class="pull-right">{{ count($order['products']) }}</span>
                        </div>
                        <div class="clearfix">
                        
                            <span class="pull-left">Total weight(Kg)</span>
                            <span class="pull-right">{{ number_format($order['totalWeight']) }}</span>
                        </div>
                        
                        
                        
                    </div>
                    


                </div>
                 @if($order['order']['currency'] != 'idr')
                 <div class="panel panel-flat right-small">

                    <div class="panel-heading pb-20">
                            <div class="panel-heading-title">
                                <h2 class="text-semibold">Rate</h2>
                            </div>
                    </div>
                    <div class="panel-body">
                        <div class="clearfix">
                            <span class="pull-left">USD -> IDR</span>
                            <span class="pull-right">{{ formatMoney($order['order']['rate_usd']) }}</span>
                        </div>
                        <div class="clearfix">
                            <span class="pull-left">EUR -> IDR</span>
                            <span class="pull-right">{{ formatMoney($order['order']['rate_eur']) }}</span>
                        </div>
                        <div class="clearfix">
                            <span class="pull-left">Grand Total IDR</span>
                            <span class="pull-right">{{ formatMoney(convertCurrency($order['order']['grand_total'],'idr'),'idr') }}</span>
                        </div>
                        <div class="clearfix">
                            <span class="pull-left">Grand Total USD</span>
                            <span class="pull-right">{{ convertCurrency($order['order']['grand_total'],'usd',$order['order']['rate_usd']) }}</span>
                        </div>
                        <div class="clearfix">
                            <span class="pull-left">Grand Total EUR</span>
                            <span class="pull-right">{{ convertCurrency($order['order']['grand_total'],'eur',$order['order']['rate_eur']) }}</span>
                        </div>

                    </div>
                
                
                </div>
                @endif

                
                <div class="panel panel-flat right-small">
                
                    <div class="panel-heading pb-20">
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Order Details</h2>
                        </div>
                    </div>
                    <div class="panel-body">

                            <div class="order-detail-view">
                                <i class="icon-calendar52"></i><span class="order-detail-text">{{ $order['order']['order_date'] }}</span>
                            </div>
                        
                            <div class="order-detail-view">
                                 <i class="icon-truck"></i><span class="order-detail-text">{{ $shippingMethod }}</span>
                            </div>
                       
                            <div class="order-detail-view">
                                 <i class="icon-calendar52"></i><span class="order-detail-text">{{ $source }}</span>
                            </div>
                        
                            <div class="order-detail-view">
                                <i class="icon-cart"></i><span class="order-detail-text">{{ $order['order']['refnumber'] }}</span>
                            </div>
                       
                            <div class="order-detail-view">
                                <img src="{{ asset('backend/assets/images/money.svg') }}" alt="money"><span class="order-detail-text">{{ $paymentMethod }}</span>
                            </div>
                       
                    </div>
                
                
                </div>
                

               
                
            </div>
        </div>

    </form>

    
    



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
 <?php echo view('catalog::admin.pages.order.modal.change-status') ?>
<!-- /content area -->
    @if(session('msg'))
    <script type="text/javascript">
        swal({
            title: "SUCCESS",
            text: "Status Updated!",
            confirmButtonColor: "#66BB6A",
            type: "success"
        });        
    </script>
    @endif
    <script>
        $(document).ready(function() {
            
            
            $('.new-product-trigger').click(function(e){
                e.preventDefault();
                $(this).addClass('hide');
                $("#add-product").removeClass('hide');

            });
            

           $("#add-product").focusin(function(){
               
               $('.product-search-result-list').addClass('show');

           });
            $("#add-product").focusout(function(){
               
               $('.product-search-result-list').removeClass('show');

           });
            $("#add-customer").focusin(function(){
               
               $('.customer-search-result-list').addClass('show');

           });
            $("#add-customer").focusout(function(){
               
               $('.customer-search-result-list').removeClass('show');

           });
            
            //autocomplete tolong disesuaikan plugin yg dipakai format search result mengikuti ul.autocomplete-search-result-list
            
            $('.date-picker').daterangepicker({
                    timePicker: true,
                    opens: "left",
                    applyClass: 'bg-slate-600',
                    cancelClass: 'btn-default',
                    singleDatePicker: true,
                    autoApply : true,
                    locale: {
                        format: 'MM/DD/YYYY h:mm a'
                    }
             });
            
           $('select').select2({
                minimumResultsForSearch: Infinity,
                width: '100%'
            });
            
            $('.address-select').each(function(){
                
                var thisplaceholder = $(this).attr('placeholder');
                
                 $(this).select2({
                        minimumResultsForSearch: Infinity,
                        width: '100%',
                        placeholder: thisplaceholder,
                    });
                
                
            });
            
            $('.address-modal-toggle').on('click',function(){
                $('#modal_new_address').modal();
            });
            
            $('.select-status').select2({
                minimumResultsForSearch: Infinity,
                width: '100px',
                placeholder: 'Select Status',
            });
            
            function formataddressdropdown (address) {
              if (!address.id) { return address.text; }
              var $address = $(
                '<div class="address-dropdown-option"><h5 class="address-dropdown-label">' + address.element.value + '</h5><h6 class="text-muted">' + address.element.getAttribute('data-detail') + '</h6></div>'
              );
              return $address;
            };

            $('.address-dropdown').each(function(){
                
                var thisplaceholder = $(this).attr('placeholder');
                $(this).select2({
                    minimumResultsForSearch: Infinity,
                    templateResult: formataddressdropdown,
                    placeholder : thisplaceholder,
                });
                
            });
            
            $('.address-dropdown').on("select2-open", function() {
                add_address_button();
            });
            
            function add_address_button(){
               
                $(".select2-results__options").append('<li class="select2-results__option"><div class="address-dropdown-option"><a href="#" class="address-modal-toggle">New Address</a></div></li>');
            }
            
            
            
        });

    </script>




