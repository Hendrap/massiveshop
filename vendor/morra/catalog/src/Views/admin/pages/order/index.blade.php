<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Outgoing</span> - Sales Order</h4>
        </div>

        <div class="heading-elements">
<!--             <a href="{{ route('catalog_orders_export') }}" class="btn btn-labeled bg-primary heading-btn"><b><i class="icon-printer"></i></b>Export</a>
 -->            <a href="{{ route('catalog_pos_view') }}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-cart"></i></b>New Order</a>
        </div>
        
    </div>


</div>

 
<!-- /page header -->


<!-- Content area -->
<div class="content">

    

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h1 class="text-semibold">Sales Orders</h1>
                        
                    </div>


                </div>
                <div class="panel-body">
                    <p>Manage incoming sales orders</p>
                </div>

                    <div class="table-header datatable-header clearfix">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-5 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter</label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                        
                        <select class="table-select2 pull-left filter-table-column" placeholder="Filter by Role" searchin="5">
                            <option value="all">Show All Status</option>
                            @foreach(CatalogPOS::getOrderStatus() as $key => $value)
                            <option value="{{ $value }}">{{ ucfirst(strtolower($value)) }}</option>
                            @endforeach
                            
                        </select>
                         
                         <select class="table-select2 pull-left filter-table-column" placeholder="Filter by Status" searchin="2">
                            <option value="all">Show All Sales Channels</option>
                            @foreach(CatalogPOS::getSources() as $key => $value)
                            <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach                            
                        </select>
                        <select class="table-select2 pull-left select-sales-date">
                            <option value="all_time">All Time</option>
                            <option value="today">Today</option>
                            <option value="last_7_days">Last 7 Days</option>
                            <option value="last_30_days">Last 30 days</option>
                            <option value="date_range">Date Range</option>
                        </select>

                       <div class="form-group has-feedback no-margin pull-left sales-date hide" style="height:36px;width:210px;">
                                        <input type="text" class="form-control sales-date-value" style="height:36px;">
                                        <div class="sale-date-trigger" style="background:#ccc;height:36px;line-height:36px;">
                                            <i class="icon-calendar"></i>
                                        </div>
                                       
                                        
                        </div>
                        
                        
                        
                        
                        
                    </div>
                    
                    <table class="table entry-table">
                        <thead>
                            <tr>
                                <th>Order</th>
                                <th width="160px"><span>Payment (IDR)</span></th>
                                <th width="30px" class="no-sort"></th>
                                <th width="130px">Sales Channel</th>
                                <th width="150px"><span>Order Date</span></th>
                                <th width="150px"><span>Last Updated</span></th>
                                <th width="120px">Status</th>
                                <th width="50px" class="no-sort text-right"></th>
                            </tr>
                        </thead>
                        <tbody>
                                
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        var entry_table = {};
                    </script>
                    
                    <?php echo view('catalog::admin.pages.order.modal.change-status') ?>
                    <script>
                        function markSettle(el,id,status)
                        {
                            $(el).find('img').show();
                            $(el).find('a').hide();
                            
                            $.ajax({
                                url:"<?php echo route('catalog_orders_settlement') ?>",
                                data:{id:id,status:status},
                                type:"POST",
                                dataType:"json",
                                success:function(data){
                                    $(el).find('img').hide();
                                    $(el).find('a').show();
                                    if(status == true){
                                         $(el).find('.successSettle').show();
                                         $(el).find('.waitSettle').hide();
                                    }

                                    if(status == false)
                                    {
                                         $(el).find('.successSettle').hide();
                                         $(el).find('.waitSettle').show();
                                    }
                                }
                                // error:function()
                                // {

                                // }
                            });
                        }

                        $(document).on('change','.list-order-check',function(e){
                            markSettle($(this).parents('td'),$(this).attr('data-id'),e.target.checked);
                        });
                        $(document).on('click','.triggerCheckbox',function(e){
                            e.preventDefault();
                            $(this).parents('td').find('.list-order-check').trigger('click');
                        });
                        $(document).ready(function() {

                            

                            entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                            if(typeof entry_table.ajax != 'undefined')
                                            {
                                                $(e.target).find('tbody').html('');
                                            }
                                             $('.dataTable, .dataTable tbody').css('position','static');
                                            $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                        $(".styled, .multiselect-container input").uniform({ radioClass: 'choice'});
                                        $.unblockUI();
                                        $(".blockUI").remove();
                                         $('.dataTable, .dataTable tbody').css('position','relative');
                                     }
                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "order": [],
                                "pageLength": 50,
                                "searching" : true,
                                "lengthChange": false,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "ajax": {
                                    "url": "{{ route('catalog_orders_get_data') }}",
                                    "type": "POST"
                                },
                                "columns": [

                                    { "data": "order_number", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:"title-column"
                                    },

                                    { "data": "grand_total", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    }

                                    },

                                  
                                    { "data": "source", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    }

                                    },

                                    { "data": "order_date", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "updated_at", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[4];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "settlement", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[5];
                                    },
                                    sClass:"td-date"

                                    },
                                    { "data": "status", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[6];
                                    }

                                    },



                                    { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[7];
                                    },
                                    sClass:"text-right table-actions"

                                    }

                                   
                                ],
                                "columnDefs": [
                                    {
                                        "targets": 'no-sort',
                                        "orderable": false,
                                    }
                                ]
                            });
                           
                            var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                
                                thisfilter = $(this);
                                thisfilter.children('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                });
                                
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                                
                            });
                            
                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13){
                                    entry_table.search($(this).val()).draw();                                    
                                }
                                 
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: '100%'
                            });
                             $('.table-select2').each(function() {

                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',

                                });

                            });
                        });

                    </script>
                    <script type="text/javascript">                        
                        $(document).on('click',".confirmCancelOrder",function(e){
                                var that = this;
                                e.preventDefault();
                                 swal({
                                title: "Order Cancellation",
                                text: "Items on cancelled orders will be put back in stock, and available for others to buy. Please confirm. ",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, Cancel This Order",
                                closeOnConfirm: false },
                                function(isConfirm){
                                    if(isConfirm){
                                        window.location.href = $(that).attr('href');
                                    }
                                });
                            });

                    </script>

                


            </div>

        </div>

    </div>



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->