<script type="text/javascript" src="{{ asset('backend/assets/js/bootstrap-timepicker.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/css/bootstrap-timepicker.min.css') }}">
<div id="modal_confirm_payment" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:440px">
        <div class="modal-content">
            <div class="modal-header pb-10">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Confirm Payment</h5>
            </div>
            <form>
                <input type="hidden" name="order_id" value="0">
                <div class="modal-body">
                    <div style="display: none" class="alert bg-danger alert-styled-left errorMsgRequired">
                        <span class="text-semibold">Missing required fields.</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label class="no-margin">Payment Date</label>
                            </div>
                            <div class="form-group has-feedback no-margin pull-left" style="height:30px;">
                                <input name="payment_date" type="text" class="form-control payment-date" style="height:30px;">
                                <div class="sale-date-trigger" style="background:#ccc;height:30px;line-height:30px;">
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label class="no-margin">Amount Paid ({{app('AlphaSetting')->getSetting('currency_format')}})</label>
                            </div>
                            <div class="form-group">
                                <input name="payment_amount" type="number" class="form-control">
                            </div>
                       </div>

                       
                    </div>
                    <div class="row">
                       
                       <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label class="no-margin">Reference No. (Optional)</label>
                            </div>
                            <div class="form-group">
                                <input name="refno" type="text" class="form-control">
                            </div>
                       </div>
                    </div>
                </div>
                 <div class="modal-footer clearfix">
                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                    <button id="savePayment" type="button" class="btn btn-primary ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
                    
<div id="modal_shipping" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:440px">
        <div class="modal-content">
            <div class="modal-header pb-10">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Confirm Shipment</h5>
            </div>
            <form>
                <input type="hidden" name="order_id" value="0">
                <div class="modal-body">
                    <div style="display: none" class="alert bg-danger alert-styled-left errorMsgRequired">
                            <span class="text-semibold">Missing required fields.</span>
                        </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label class="no-margin">Date of Shipment</label>
                            </div>
                            <div class="form-group has-feedback no-margin pull-left" style="height:36px;">
                                        <input name="ship_date" type="text" class="form-control payment-date" style="height:36px;">
                                        <div class="sale-date-trigger" style="height:36px;line-height:36px;">
                                            <i class="icon-calendar"></i>
                                        </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label class="no-margin">Amount Paid ({{app('AlphaSetting')->getSetting('currency_format')}})</label>
                            </div>
                            <div class="form-group">
                                <input name="shipping_amount" type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                    <button id="saveShippingInfo" type="button" class="btn btn-primary ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modal_shipment" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:440px">
        <div class="modal-content">
            <div class="modal-header pb-10">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Request Shipment</h5>
            </div>
            <form>
                <input type="hidden" name="order_id" value="0">
                <div class="modal-body">
                    <div style="display: none" class="alert bg-danger alert-styled-left errorMsgRequired">
                            <span class="text-semibold">Missing required fields.</span>
                        </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label class="no-margin">Date of Shipment</label>
                            </div>
                            <div class="form-group has-feedback no-margin pull-left" style="height:36px;">
                                        <input name="ship_date" type="text" class="form-control payment-date" style="height:36px;">
                                        <div class="sale-date-trigger" style="height:36px;line-height:36px;">
                                            <i class="icon-calendar"></i>
                                        </div>
                            </div>
                        </div>
                    </div>
                     
                   
                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                    <button id="saveReqShipment" type="button" class="btn btn-primary ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="modal_cancel_order" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:440px">
        <div class="modal-content">
            <div class="modal-header pb-10">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Cancel Order</h5>
            </div>
            <form>
                <input type="hidden" name="order_id" value="0">
                <div class="modal-body">
                   
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label class="no-margin">Reason</label>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="cancel_reason" id="cancel_reason">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                    <button id="btnCancelOrder" type="button" class="btn btn-primary ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modal_pickup" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:440px">
        <div class="modal-content">
            <div class="modal-header pb-10">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Pickup</h5>
            </div>
            <form>
                <input type="hidden" name="order_id" value="0">
                <div class="modal-body">
                    <div style="display: none" class="alert bg-danger alert-styled-left errorMsgRequired">
                        <span class="text-semibold">Missing required fields.</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group mb-10">
                                <label class="no-margin">Package Location</label>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="package_location" id="package_location">
                                <p class="help-block">Package Location in the pickup place. E.g. Front Desk</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group mb-10">
                                <label class="no-margin">Pickup Date</label>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="pickup_date" id="pickup_date">
                                <p class="help-block">The date at which the pick up has been scheduled</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group mb-10">
                                <label class="no-margin">Ready By Time</label>
                            </div>
                            <div class="form-group">
                                <input value="{{ date('H:i A',strtotime('+1 hours')) }}" class="form-control" type="text" name="ready_time" id="ready_time">
                                <p class="help-block">The time by which the pick will be ready</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group mb-10">
                                <label class="no-margin">Close Time</label>
                            </div>
                            <div class="form-group">
                                <input value="{{ date('H:i A',strtotime('+2 hours')) }}" class="form-control" type="text" name="close_time" id="close_time">
                                <p class="help-block">Closing time of the pickup location.</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group mb-10">
                                <label class="no-margin">Special Instructions (Optional)</label>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="special_instructions" id="special_instructions">
                                <p class="help-block">Specail instructions for pickup</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group mb-10">
                                <label class="no-margin">Door To</label>
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="door_to" id="door_to">
                                    <option value="DD">Door to Door</option>
                                    <option value="DA">Door to Airport</option>
                                    <option value="DC">Door to Door non-compliant</option>
                                </select>
                                <p class="help-block">The flag to indicate whether the shipment is door to or not</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                    <button id="savePickUpOrder" type="button" class="btn btn-primary ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
@if(Session::has('special_error_dhl'))
    swal({
        title: "ERROR",
        text: "{{ Session::get('special_error_dhl') }}",
        confirmButtonColor: "#66BB6A",
        type: "error"
    });    
@endif
            $('.select-sales-date').change(function(){
                                
                                var selectedsalesdate = $(this).find(":selected").attr('value');
                                
                                if(selectedsalesdate == 'date_range'){
                                    $('.sales-date').removeClass('hide');
                                   
                                }else{
                                    $('.sales-date').addClass('hide');
                                    $('.sales-date input').val('');
                                     if(selectedsalesdate == 'all_time') selectedsalesdate = "";
                                     entry_table.columns(3).search(selectedsalesdate).draw();
                                    
                                }

                                
                            });


    $('.sales-date').daterangepicker({
                               "opens": "left",
                                "drops": "down",
                                autoApply:true,
                                locale: {
                                    format: 'DD MMM YYYY'
                                }
                            });

                            $(".sales-date-value").change(function(e){
                                 entry_table.columns(3).search(e.target.value).draw();
                            });

                            $('.sales-date').on('apply.daterangepicker', function(ev, picker) {
                                   var selectedsaledate = picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY');
                                    $('.sales-date-value').val('');
                                    $('.sales-date-value').val(selectedsaledate);
                                    $('.sales-date-value').trigger('change');
                            });
                            

                            $('.payment-date').daterangepicker({
                                    drops: "down",
                                    singleDatePicker: true,
                                    autoApply : true,
                                    locale: {
                                        format: 'DD MMM YYYY'
                                    }
                             });

                            
    function showLoading(el){
         $(el).block({
            message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: '10px 15px',
                color: '#333',
                width: 'auto',
            }
        });
    }
    function hideLoading()
    {
        $(".blockUI").remove()
    }

     $(document).on('click','.trigger-pickup',function(e){
        e.preventDefault();
        $("#modal_pickup").find('[type=hidden]').val($(this).attr('data-id'));
        $(".errorMsgRequired").hide();
        $('#modal_pickup').modal();
    });


     $(document).on('click','.request-shipment',function(e){
        e.preventDefault();
        $("#modal_shipment").find('[type=hidden]').val($(this).attr('data-id'));
        $(".errorMsgRequired").hide();
        $('#modal_shipment').modal();
    });


    $(document).on('click','.confirm-payment-trigger',function(e){
        e.preventDefault();
        $("#modal_confirm_payment").find('[type=hidden]').val($(this).attr('data-id'));
        $(".errorMsgRequired").hide();
        $('#modal_confirm_payment').modal();
    });

    $(document).on('click','.trigger_order_shipping',function(e){
        e.preventDefault();
        $("#modal_shipping").find('[type=hidden]').val($(this).attr('data-id'));
        $("#modal_shipping").find('[name=shipping_method]').val($(this).attr('data-carrier-id')).change();
        $(".errorMsgRequired").hide();
        $('#modal_shipping').modal();
    })

     $(document).on('click','.trigger_order_cancel',function(e){
        e.preventDefault();
        $("#modal_cancel_order").find('[type=hidden]').val($(this).attr('data-id'));
        $('#modal_cancel_order').modal();
    });

    $('input[name="pickup_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });

    $('#ready_time').timepicker({minuteStep:1});
    $('#close_time').timepicker({minuteStep:1});

    

     $(document).on('click',".trigger_cancel_pickup",function(e){
        var that = this;
        e.preventDefault();
         swal({
        title: "Pickip Cancellation",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Cancel This Order",
        closeOnConfirm: false },
        function(isConfirm){
            if(isConfirm){
                window.location.href = $(that).attr('href');
            }
        });
    });


    $("#savePickUpOrder").click(function(e){
            e.preventDefault();
            
            $(".errorMsgRequired").hide();
            if($("#package_location").val().trim() == "" || $("#pickup_date").val().trim() == "" || $("#ready_time").val().trim() == "" || $("#close_time").val().trim() == "")
            {
                $(".errorMsgRequired").show();
                return false;
            }

            showLoading( $("#modal_pickup").find('.modal-content'));

            $.ajax({
                url:"{{ route('catalog_orders_pick_up') }}",
                type:"POST",
                dataType:"json",
                data:$("#modal_pickup").find('form').serialize(),
                success:function(data){
                    hideLoading();
                    if(data.status == 1)
                    {
                        swal({
                            title: "SUCCESS",
                            text: "Status Updated!",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                        });       
                        window.location.reload();
                    }else{
                        swal({
                            title: "ERROR",
                            text: data.msg,
                            confirmButtonColor: "#66BB6A",
                            type: "error"
                        });    
                    }
                }
            });
        })

    $("#saveReqShipment").click(function(e){
            e.preventDefault();
            showLoading( $("#modal_shipment").find('.modal-content'));
            $.ajax({
                url:"{{ route('catalog_orders_req_shipment') }}",
                type:"POST",
                dataType:"json",
                data:$("#modal_shipment").find('form').serialize(),
                success:function(data){
                    hideLoading();
                    if(data.status == 1)
                    {
                       
                        swal({
                            title: "SUCCESS",
                            text: "Status Updated!",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                        });       
                        window.open(data.url);
                        window.location.reload();
                    }else{
                        swal({
                            title: "ERROR",
                            text: data.msg,
                            confirmButtonColor: "#66BB6A",
                            type: "error"
                        });    
                    }
                }
            });
        });


    $("#btnCancelOrder").click(function(e){
        e.preventDefault();
        showLoading( $("#modal_cancel_order").find('.modal-content'));
        $.ajax({
            url:"{{ route('catalog_orders_cancel_order') }}",
            type:"POST",
            dataType:"json",
            data:$("#modal_cancel_order").find('form').serialize(),
            success:function(data){
                hideLoading();
                if(data.status == 1)
                {
                   
                    swal({
                        title: "SUCCESS",
                        text: "Status Updated!",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    });       
                    window.location.reload();
                }
            }
        });
    });


                
    $("#saveShippingInfo").click(function(e){
        e.preventDefault();
        $("#modal_shipping").find('form-group').removeClass('has-error');
        if($("#modal_shipping").find("[name=shipping_amount]").val() == '')
        {
            $(".errorMsgRequired").show();
            $("#modal_shipping").find("[name=shipping_amount]").parent().addClass('has-error');
            return false;
        }
        showLoading( $("#modal_shipping").find('.modal-content'));
        $.ajax({
            url:"{{ route('catalog_orders_shipping_info') }}",
            type:"POST",
            dataType:"json",
            data:$("#modal_shipping").find('form').serialize(),
            success:function(data){
                hideLoading();
                if(data.status == 1)
                {
                   
                    swal({
                        title: "SUCCESS",
                        text: "Status Updated!",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    });       
                    window.location.reload();
                }
            }
        });
    });
    $("#savePayment").click(function(e){
        e.preventDefault();
        $("#modal_confirm_payment").find('form-group').removeClass('has-error');
        if($("#modal_confirm_payment").find("[name=payment_amount]").val() == '')
        {
            $(".errorMsgRequired").show();
            $("#modal_confirm_payment").find("[name=payment_amount]").parent().addClass('has-error');
            return false;
        }
        showLoading( $("#modal_confirm_payment").find('.modal-content'));
        $.ajax({
            url:"{{ route('catalog_orders_confirm_payment') }}",
            type:"POST",
            dataType:"json",
            data:$("#modal_confirm_payment").find('form').serialize(),
            success:function(data){
                hideLoading();
                if(data.status == 1)
                {
                   
                    swal({
                        title: "SUCCESS",
                        text: "Status Updated!",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    });       
                    window.location.reload();
                }
            }
        });
    })       
</script>