<td class="text-bold"><h6><a href="{{ route('catalog_orders_detail',[$order->id]) }}">{{ $order->order_number }}</a></h6><span class="text-muted">{{ $order->shipping_first_name.' '.$order->shipping_lastname }}</span></td>
|alpha--datatable-separator--|<td><span class="payment-ammount">{{ ($order->grand_total <= 0) ? 0 : formatMoney($order->grand_total,'idr') }}</span></td>
|alpha--datatable-separator--|<td>
    <?php 
        $disabled = '';
        if($order->status == 'CANCELLED' || $order->status == 'PENDING') $disabled = 'disabled';
     ?>
    <img src="{{ asset('backend/assets/images/download.gif') }}" style="display: none">
    @if($order->status == 'CANCELLED' || $order->status == 'PENDING')
        <a href="javascript:void(0)"><label class="label label-default">(R)</label></a>   
    @else

    <a style="{{ ($order->settlement == 1) ? 'display: block' : 'display:none'  }}" class="triggerCheckbox successSettle" href="javascript:void(0)"><label style="cursor: pointer;" class="label label-success">(R)</label></a>                   
    <a style="{{ ($order->settlement == 1) ? 'display: none' : 'display:block'  }}" class="triggerCheckbox waitSettle" href="javascript:void(0)"><label style="cursor: pointer;background-color: #f0ad4e;border-color: #f0ad4e;color: black" class="label label-warning">(R)</label></a>   
      

    @endif
    <input style="display: none" {{ $disabled }} {{ ($order->settlement == 1) ? 'checked' : '' }} data-id="{{ $order->id }}" type="checkbox" class="styled list-order-check" name="">
</td>
|alpha--datatable-separator--|<td>{{  CatalogPOS::getSourceFromOrder($order) }}<br><span class="text-muted">{{ $order->refnumber }}</span>
</td>
|alpha--datatable-separator--|<td class="td-date">
{{ date(app('AlphaSetting')->getSetting('date_format'),strtotime($order->order_date)) }}
<br><span class="text-muted">{{ date(app('AlphaSetting')->getSetting('time_format'),strtotime($order->order_date)) }}
</span>
</td>
|alpha--datatable-separator--|<td class="td-date">
{{ date(app('AlphaSetting')->getSetting('date_format'),strtotime($order->updated_at)) }}
<br><span class="text-muted">{{ date(app('AlphaSetting')->getSetting('time_format'),strtotime($order->updated_at)) }}</span>
                                    
</td>
|alpha--datatable-separator--|<td>
    @if($order->status == 'PENDING')
    <span class="label bg-grey">PENDING</span>
    @endif
    @if($order->status == 'APPROVED')
    <span class="label label-success">APPROVED</span>
    @endif

    @if($order->status == 'SHIPMENT')
    <span class="label label-success">Req. Shipment</span>
    @endif

    @if($order->status == 'PICKUP')
    <span class="label label-success">PICKUP</span>
    @endif


    @if($order->status == 'PACKED')
    <span class="label label-info">PACKED</span>
    @endif
    @if($order->status == 'SHIPPED')
    <span class="label bg-success">SHIPPED</span>
    @endif
    @if($order->status == 'CANCELLED')
    <span class="label bg-danger">CANCELLED</span>
    @endif
</td>
|alpha--datatable-separator--|<td class="text-right table-actions">
    <div class="btn-group">
        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="{{ route('catalog_orders_detail',[$order->id]) }}"><i class="icon-eye"></i> View</a></li>
            @if($order->status != 'CANCELLED')
<!--             <li><a href="{{ route('catalog_pos_view',[$order->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
 -->            @endif
<!--             <li><a href="javascript:void(0)"><i class="icon-printer"></i> Print</a></li>
 -->        @if($order->status == 'PENDING')
            <li><a data-id="{{$order->id}}" href="javascript:void(0)" class="confirm-payment-trigger"><i class="icon-checkmark4"></i> Confirm Payment</a></li>
            @endif

            @if($order->status != 'PENDING' && $order->status != 'APPROVED' && $order->status != 'CANCELLED' && $order->status != 'PACKED')
                <li><a target="_blank" href="{{ asset('uploads/pdf/dhl-'.$order->id.'.pdf') }}"><i class="icon-eye"></i> Download AWB</a></li>
            @endif

            @if($order->status  == 'APPROVED')    
            <li><a href="{{route('catalog_orders_status',[$order->id,'PACKED'])}}" class=""><i class="icon-gift"></i>PACKED</a></li>
            @endif  

            @if($order->status  == 'PACKED')    
            <li><a href="{{route('catalog_orders_status',[$order->id,'SHIPMENT'])}}" data-id="{{$order->id}}" class="request-shipment"><i class="icon-gift"></i> Req. Shipment</a></li>
            @endif

            @if($order->status  == 'SHIPMENT')    
            <li><a href="{{route('catalog_orders_status',[$order->id,'PICKUP'])}}" data-id="{{$order->id}}" class="trigger-pickup"><i class="icon-gift"></i> PICKUP</a></li>
            @endif     

            @if($order->status == 'PICKUP')
            <li><a data-id="{{$order->id}}" href="javascript:void(0)" class="trigger_order_shipping"><i class="icon-truck"></i> Ship Order</a></li>
            @endif


            @if($order->status == 'PICKUP')
            <li><a data-id="{{$order->id}}" class="trigger_cancel_pickup" href="{{route('catalog_orders_cancel_pickup',[$order->id])}}"><i class="icon-cancel-square"></i> Cancel Pickup</a></li>
            @endif


            @if($order->status != 'CANCELLED')
            <li class="border-top"><a data-id="{{$order->id}}" class="trigger_order_cancel" href="{{route('catalog_orders_status',[$order->id,'CANCELLED'])}}"><i class="icon-cancel-square"></i> Cancel Order</a></li>
            @endif
        </ul>
    </div>
</td>
