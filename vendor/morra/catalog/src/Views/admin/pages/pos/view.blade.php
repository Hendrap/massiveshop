<style type="text/css">
    .ui-autocomplete{
        z-index: 100000;
    }
    [v-cloak] {
        display: none;
    }
  #select2-dropDownShipping-results li:nth-last-child(1) {
        color:#1E88E5
  }
  #select2-dropDownBilling-results li:nth-last-child(1) {
        color:#1E88E5
  }
</style>
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4 id="pageLabel"><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Outgoing</span> - New Order</h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div v-cloak class="content" id="POS">
    <form action="" method="post" class="form-horizontal">
        <div class="row">
            <div class="col-sm-9">
                <div class="panel panel-flat">
                    <div class="panel-heading clearfix">
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">${ order_number == '' ? 'New Order' : 'Order #' + order_number }</h2>
                        </div>
                    </div>
                    <div class="panel-body">
                       <div class="row mb-50" style="z-index: 99;position: relative;">
                           <div class="col-sm-4">
                                <div class="form-group" style="padding: 0 10px">
                                    <label>Customer Information</label>
                                    <div class="customer-autocomplete-container">
                                        <input v-show="!showCustomerInfo" v-model="searchCustomer" debounce="500" type="text" class="form-control" style="height:36px" id="add-customer" placeholder="Search by Name, Phone or Email">
                                        <div class="customer-search-result">
                                            <ul class="autocomplete-search-result-list customer-search-result-list">
                                                <li track-by="$index" @click="selectCustomer($index)" v-for="customer in customerList">
                                                    <a href="javascript:void(0)">
                                                    <h5><!-- <span class="matches">Wah</span> -->${ preventNullValue(customer.first_name) + ' ' + preventNullValue(customer.last_name) }</h5>
                                                    <h6 class="text-muted"><!-- <span class="matches">Wah</span> --> ${ preventNullValue(customer.email) }</h6>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a @click="openCustomerModal" href="javascript:void(0)" class="trigger-modal-new-user">
                                                        <h5 style="color:#1E88E5">Create New</h5>
                                                        <h6 v-show="showCreateNewButton" class="text-muted">User not found</h6>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div v-show="showCustomerInfo" class="customer-info">
                                            <h5>${ preventNullValue(selectedCustomer.first_name) + ' ' + preventNullValue(selectedCustomer.last_name) }</h5>
                                            <div class="info-box">
                                                <span>${ preventNullValue(selectedCustomer.email)}</span>
                                                <span>${ preventNullValue(selectedCustomer.phone)}</span>
                                            </div>
                                            <a @click="editCustomer" href="javascript:void(0)" class="btn btn-default edit-customer">Edit</a>
                                            <a @click="changeCustomer" href="javascript:void(0)" class="btn btn-default change-customer">Remove</a>
                                            <div class="checkbox">
                                                <label>
                                                    <input id="isDropShip" v-model="isDropShip" type="checkbox" class="styled"> Dropship
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                           </div>
                           <div class="col-sm-4">
                                <div class="form-group" style="padding: 0 10px">
                                    <label>Shipping Address</label>
                                    <div class="address-select-container">
                                        <select id="dropDownShipping" class="address-dropdown" placeholder="Select Shipping Address">
                                            <option></option>
                                            <option data-action="edit" data-type="shipping" data-index="${$index}" v-for="address in shippingAddressList" value="${ parseLabelForAddress(address)  }" data-detail="${preventNullValue(address.address)}">${ parseLabelForAddress(address)  }</option>
                                       </select>
                                    </div>
                                    <div v-show="activeShipping.address != ''" class="customer-info">
                                        <h5>${ parseLabelForAddress(activeShipping) }</h5>
                                        <div class="info-box">
                                            <span>${ preventNullValue(activeShipping.address)  }</span>
                                            <span>${ preventNullValue(activeShipping.district) },${ preventNullValue(activeShipping.city) }</span>
                                            <span>${ preventNullValue(activeShipping.province) } ${ preventNullValue(activeShipping.postal_code) }</span>
                                            <span>${ preventNullValue(activeShipping.country) }</span>

                                        </div>
                                        <div class="info-box">
                                            <span>${ preventNullValue(activeShipping.phone) }</span>
                                        </div>
                                        <a href="javascript:void(0)" @click="showModalAddress('edit','shipping')" class="btn btn-default edit-customer">Edit</a>
                                    </div>
                                </div>
                           </div>
                           <div class="col-sm-4">
                                 <div class="form-group" style="padding: 0 10px">
                                    <label>Billing Address</label>
                                    <div class="address-select-container">
                                        <select id="dropDownBilling" class="address-dropdown" placeholder="Select Billing Address">
                                            <option></option>
                                            <option data-action="edit" data-type="billing" data-index="${$index}"  v-for="address in billingAddressList" value="${ parseLabelForAddress(address)  }" data-detail="${preventNullValue(address.address)}">${ parseLabelForAddress(address) }</option>
                                        </select>
                                    </div>
                                    <div v-show="activeBilling.address != ''" class="customer-info">
                                        <h5>${ parseLabelForAddress(activeBilling) }</h5>
                                        <div class="info-box">
                                            <span>${ preventNullValue(activeBilling.address) }</span>
                                            <span>${ preventNullValue(activeBilling.district) },${ preventNullValue(activeBilling.city) }</span>
                                            <span>${ preventNullValue(activeBilling.province) } ${ preventNullValue(activeBilling.postal_code) }</span>
                                            <span>${ preventNullValue(activeBilling.country) }</span>
                                        </div>
                                        <div class="info-box">
                                            <span>${ preventNullValue(activeBilling.phone) }</span>
                                        </div>
                                        <a href="javascript:void(0)" @click="showModalAddress('edit','billing')" class="btn btn-default edit-customer">Edit</a>
                                    </div>
                                </div>
                           </div>
                       </div>
                       <table class="table pb-10 border-bottom table-order-items" style="border-color:#ddd;z-index: 98;position: relative;">
                        
                            <thead>
                            
                                <tr>
                                    <th width="30px">Item</th>
                                    <th></th>
                                    <th width="95px">Qty</th>
                                    <th width="135px"><span>Unit Price (IDR)</span></th>
                                    <th width="135px"><span>Discount (IDR)</span></th>
                                    <th width="135px"><span>Subtotal (IDR)</span></th>
                                    <th class="no-sort" width="60px"></th>
                                </tr>
                                
                            </thead>
                            <tbody>
                               <tr v-for="product in products">
                                    <td v-if="product.image_thumbnail != null && product.image_thumbnail != ''"><a href="javascript:;" class="catalog-preview" data-container="body" data-popup="popover" data-trigger="hover" data-img-src="${product.image_thumbnail}" data-content="<img src='${product.image_thumbnail}' alt='' style='width:200px'>" data-html="true"> <i class="icon-camera"></i></a></td>
                                    <td v-if="product.image_thumbnail == null || product.image_thumbnail == ''"><a href="javascript:void(0)" class="catalog-preview no-image"> <i class="icon-camera"></i></a></td>
                                    <td class="text-bold">
                                        <h6>${preventNullValue(product.product_title)}</h6>
                                        <span class="text-muted">${preventNullValue(product.sku)}</span>
                                    </td>
                                    <td><input v-model="product.stock_qty" type="number" class="form-control"></td>
                                    <td><span class="text-bold text-right display-block table-price">${parseFloat(product.original_price).formatMoney(0)}</span></td>
                                    <td><input v-model="product.discount" type="number" class="form-control"></td>
                                    <td><input data-index="${$index}" class="form-control subTotal" type="number" number name="" v-bind:value="parseFloat((parseMoney(product.stock_qty) * parseMoney(product.original_price)) - parseMoney(product.discount))"></td>
                                    <td><a @click="deleteProduct($index)" href="javascript:void(0)" class="text-black"><i class="icon-trash"></i></a></td>
                                </tr>
                                <tr class="table-input-row" v-show="showSearchInputProducts">
                                    <td colspan="2">
                                        <div class="add-product-container">
<!--                                             <a href="javascript:void(0)" @click="initSearchProducts" v-show="!showSearchInputProducts">Add Item</a>
 -->                                            <input v-model="inputSearchProduct" debounce="500" v-show="showSearchInputProducts" type="text" class="form-control" placeholder="Search by product name or SKU" id="add-product">
                                            <div v-show="searchProducts.length" class="product-search-result">
                                                <ul class="autocomplete-search-result-list product-search-result-list">
                                                    <li v-for="product in searchProducts">
                                                        <a @click="selectProduct($index)" href="javascript:void(0)">
                                                            <h5>${preventNullValue(product.product_title)} [STOCK : ${product.stock_available}]</h5>
                                                            <h6 class="text-muted">${preventNullValue(product.item_title) + ' ' + preventNullValue(product.sku)}</h6>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                            
                        </table>

                        <div class="table-empty-add-new" v-show="showProductsEmptyState">
                            <a @click="initSearchProducts" href="javascript:void(0)" class="table-input-trigger">Start Adding Item</a>
                        </div>
                        <div class="row pt-20">
                        
                            <div class="col-md-6">
                                <h3 class="customer-note-heading">Customer Note</h3>
                                
                            </div>
                            <div class="col-md-6">
                                <img src="{{ asset('morra/catalog/assets/img/Gift.svg') }}" alt="gift-wrap" style="width:14px;vertical-align:middle;" class="display-inline-block mb-10">
                                <div class="checkbox-inline generate-password display-inline-block checkbox-right mb-5" style="vertical-align:middle;margin-top:-8px;margin-left:-30px;">
                                            
                                            <label>
                                                    <input id="isGiftWrap" v-model="isGiftWrap" type="checkbox" class="styled checkbox-gift-wrap">
                                            </label>
                                            Gift Wrapping
                                </div>
                                
                            </div>
                        
                        </div>
                        <div class="row">
                        
                            <div class="col-md-6">
                                
                                <div class="form-group no-margin">
                                
                                    <textarea v-model="customer_note" class="form-control"></textarea>
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                                <div class="form-group no-margin">
                                
                                    <textarea disabled="" v-model="gift_wrapping" class="form-control gift-wrap-textarea"></textarea>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    
                     
                    

                </div>

                

            </div>


            <div class="col-sm-3">

                <div class="panel right-small payment-details">
                
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Payment Details</h2>

                        </div>


                    </div>
                    <div class="panel-body">
                        <h6>Amount (IDR)</h6>
                        <span class="total-big">${parseFloat(grandTotal).formatMoney(0) }</span>
                        <div class="clearfix">
                            <span class="pull-left">Subtotal (IDR)</span>
                            <span class="pull-right">${parseFloat(subTotal).formatMoney(0)}</span>
                        </div>
                        <div class="clearfix">
                        
                            <span class="pull-left">Shipping (IDR)</span>
                            <span class="pull-right">${parseFloat(shipping_amount).formatMoney(0)}</span>
                        </div>
                           <div class="clearfix">
                        
                            <span class="pull-left">Discount (IDR)</span>
                            <span class="pull-right">${parseFloat(total_discount).formatMoney(0)}</span>
                        </div>
                        
                        <a href="javascript:void(0)" @click="placeOrder" class="btn display-block btn-primary mt-20">Place Order</a>
                        
                    </div>
                
                
                </div>
                
                
                <div class="panel right-small">
                
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Order Details</h2>
                        </div>


                    </div>
                    <div class="panel-body">
                        <label>Order Date</label>
                        <div class="form-group has-feedback no-margin mb-20">
                        <?php 
                            $date = date(app('AlphaSetting')->getSetting('date_format').' '.app('AlphaSetting')->getSetting('time_format'));
                            
                            if($order['id'] > 0)
                            {
                                $date = ($order['order']['order_date']);
                            }
                         ?>


                            <input value="<?php echo $date ?>" id="order_date" type="text" class="form-control">
                            <div class="form-control-feedback ">
                                <i class="icon-calendar"></i>
                            </div>
                        </div>
                        <div class="form-group no-margin mb-20">
                            <label>Shipping Method <span class="text-muted">(${totalWeight} Kg)</span></label>
                            <select id="dropDownShippingMethod" v-model="carrier_id" class="address-select" placeholder="Select Shipping Method">
                                <option v-if="shipping.value > 0" value="${shipping.id}" v-for="shipping in shippingMethods">${shipping.name}</option>
                            </select>
                        </div>
                        <div class="form-group no-margin mb-20">
                            <label>Shipping Amount</label>
                            <input type="number" class="form-control" name="" v-model="shipping_amount">
                        </div>
                        <div class="form-group no-margin mb-20">
                            <label>Discount Amount</label>
                            <input id="disocunt_amount" type="number" class="form-control" name="" v-model="total_discount">
                        </div>
                        <label>Store</label>
                        <div class="form-group no-margin mb-20">
                            <select id='store' class="address-select" placeholder="Select Store">
                                <option selected="" disabled=""></option>
                                <option value="${value.id}" v-for="value in stores">${value.name}</option>
                            </select>
                        </div>
                        <label>Sales Channel</label>
                        <div class="form-group no-margin mb-20">
                            <select id='source' class="address-select" placeholder="Select Sales Channel">
                                <option value="${value.id}" v-for="value in sources">${value.name}</option>
                            </select>
                            <input v-model="refnumber" type="text" class="form-control mt-5" placeholder="Ref. Code / Order Number">
                        </div>
                        <label>Payment Method</label>
                        <div class="form-group no-margin mb-20">
                            <select id="paymentMethod" v-model="paymentMethod" class="address-select" placeholder="Select Payment Method">
                                <option value="${payment.id}" v-for="payment in paymentMethods">${payment.name}</option>
                            </select>
                           
                        </div>
                    </div>
                
                
                </div>
                

                <div style="display: none" class="panel right-small">
                
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Promotion</h2>
                        </div>


                    </div>
                    <div class="panel-body">
                        
                        <div class="coupon-code-unfilled mb-20">
                            <div class="form-group no-margin mb-20">
                                <input type="text" placeholder="coupon code" class="form-control">
                            </div>
                            <a href="#" class="btn display-block btn-primary">Apply</a>
                        </div>
                        
                        <div class="coupon-code-filled mb-20 hide">
                            <div class="form-group no-margin mb-20 text-center">
                                <span class="text-bold">EX12304</span>
                            </div>
                            <a href="#" class="btn display-block btn-danger">Cancel</a>
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
        </div>
    </form>
     <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->
    <div id="modalAccount" class="modal fade" style="display: none;">
        <div class="modal-dialog" style="width:700px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                        <h5 class="modal-title">${ modalCustomer.id == 0 ? 'New' : 'Edit' } Customer</h5>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="alert alert-danger" v-for="error in errors">${error}</div>
                        <div class="row">

                                <div class="col-sm-6">

                                    <div class="form-group">

                                        <label>Name</label>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input v-model="modalCustomer.first_name" type="text" class="form-control" placeholder="First Name" name="first_name">
                                            </div>
                                            <div class="col-sm-6">
                                                <input v-model="modalCustomer.last_name" type="text" class="form-control" placeholder="Last Name" name="last_name">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input v-model="modalCustomer.email" type="email" class="form-control" placeholder="mail@domain.com" name="email">
                                    </div>

                                    

                                     <div class="form-group">
                                        <label>Phone Number</label>
                                        <input v-model="modalCustomer.phone" type="number" class="form-control" placeholder="0812 9999 9999">
                                    </div>


                                    <!-- <div class="form-group">

                                        <label>Password</label>

                                        <div class="row password-form">
                                            <div class="col-sm-6">
                                                <input type="password" class="form-control" placeholder="Type Password" name="password">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="password" class="form-control" placeholder="Re-Type Password" name="retype_password">
                                            </div>
                                        </div>
                                        <div class="row password-generated hide">
                                            <div class="col-sm-12">
                                                <p class="no-margin">Password will be automatically generated and will be notified to user by email.</p>
                                            </div>
                                        </div>
                                        <div class="checkbox generate-password">
                                            <label>
                                                    <input type="checkbox" class="styled checkbox-password">
                                                    Generate Password
                                            </label>
                                        </div>
                                    </div> -->

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Date Of Birth</label>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-4">
                                            <select data-model="day" v-model="modalCustomer.day" class="select-date" placeholder="Day" width="100%">
                                                @for($i=1;$i<=31;$i++)
                                                <option data-detail="" value="{{$i}}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                            </div>
                                            
                                            <div class="col-sm-4">
                                            <select data-model="month" v-model="modalCustomer.month" class="select-date" placeholder="Month" width="100%">
                                                @for($i=1;$i<=12;$i++)
                                                <option data-detail="" value="{{ date('F',strtotime('2016-'.$i.'-01')) }}">{{ date('F',strtotime('2016-'.$i.'-01')) }}</option>
                                                @endfor
                                            </select>
                                            </div>
                                            
                                            <div class="col-sm-4">
                                            <select data-model="year" v-model="modalCustomer.year" class="select-date" placeholder="Year" width="100%">
                                                @for($i=1960;$i<=2000;$i++)
                                                    <option data-detail="" value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label>Gender</label>
                                        <br>

                                        <div class="clearfix">
                                            <label class="radio-inline">
                                                <input v-model="modalCustomer.gender" value="Male" type="radio" name="gender" class="styled control-primary"> Male
                                            </label>
                                            <label class="radio-inline">
                                                <input v-model="modalCustomer.gender" value="Female" type="radio" name="gender" class="styled control-primary"> Female
                                            </label>

                                        </div>

                                    </div>
                                    
                                   



                                </div>

                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-grey" data-dismiss="modal">Cancel</button>
                        <button @click="saveUser($event)" type="button" class="btn btn-primary add-slider-to-editor">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="modalAddress" class="modal fade" style="display: none;">
        <div class="modal-dialog" style="width:600px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                        <h5 class="modal-title">${ modalAddress.id == 0 ? 'New' : 'Edit' } Address</h5>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="alert alert-danger" v-for="error in errors">${error}</div>
                        <div class="form-group">
                            <label>Address Label</label>
                            <input v-model="modalAddress.label" type="text" class="form-control" placeholder="Home, Office, Grandma's, etc">
                          
                        </div>
                        
                        <div class="form-group">
                            <label>Recipient Name</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input v-model="modalAddress.first_name" type="text" placeholder="First Name" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <input v-model="modalAddress.last_name" type="text" placeholder="Last Name" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                        
                            <label>Address</label>
                            
                            <textarea v-model="modalAddress.address" class="form-control"></textarea>
                            
                        </div>
                      
                        <div class="form-group">
                            <label>District, City</label>
                           
                            <input id="autocompleteDistrictCity" v-model="modalAddress.districtCity" type="text" class="form-control"> <!-- autocomplete-->
                          
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-10">
                                    <label>Province</label>
                                    <input v-model="modalAddress.province" type="text" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                  <label>Postal Code</label>
                                  <input v-model="modalAddress.postal_code" type="text" class="form-control" placeholder="80999">
                                </div>
                                
                            </div>
                          
                        </div>

                        <div class="form-group">
                            <label>Country</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <select class="form-control" v-model="modalAddress.country">
                                        @foreach($countries as $country)
                                        <option value="{{ $country->name }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Phone Number</label>
                            <div class="row">
                                <div class="col-sm-4">
                                  <input v-model="modalAddress.phone" type="text" class="form-control" placeholder="0812 222 2222">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                                    <button type="button" class="btn bg-grey" data-dismiss="modal">Cancel</button>
                                    <button @click="saveAddress" type="button" class="btn btn-primary add-slider-to-editor">Save</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /content area -->
<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/framework/vue.js') }}"></script>
<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/framework/vue-resource.min.js') }}"></script>
<script type="text/javascript">
    Vue.http.headers.common['X-CSRF-TOKEN'] = "{{ csrf_token() }}";
    Vue.config.delimiters = ['${', '}'];
    var dataOrder = <?php echo json_encode($order) ?>;
    var pageTitle = "<?php echo $pageTitle ?>";
    var assetBaseURL = "<?php echo asset("") ?>";
</script>
<script type="text/javascript">
    var urlGetCustomer = "<?php echo route("catalog_pos_user") ?>";
    var urlSaveUser = "<?php echo route('catalog_pos_save_user') ?>";
    var urlSaveAddress = "<?php echo route('catalog_pos_save_address') ?>";
    var urlGetCityProvince = "<?php echo route('catalog_pos_get_city_district') ?>";
    var urlSearchProducts = "<?php echo route('catalog_pos_get_products') ?>";
    var urlSaveOrder = "<?php echo route('catalog_pos_place_order') ?>";
    var urlGetDHLQuote = "<?php echo route('catalog_post_get_dhl') ?>";
    var  data = {
        orderId:0,
        order_number:"",
        customer_note:"",
        dropshipper_name:"",
        dropshipper_phone:"",
        isDropShip:false,
        jne_reg:0,
        jne_yes:0,
        jne_oke:0,
        customerList: [],
        selectedCustomer :{},
        showCustomerInfo :false,
        shippingAddressList: [],
        billingAddressList :[],
        billing_id :0,
        shipping_id: 0,
        changedBilling :false,
        changedShipping: false,
        searchCustomer :"",
        shippingMethods: [],
        errors:[],
        carrier_id:'',
        shipping_amount:0,
        tracking_number:"",
        real_shipping_amount:0,
        refnumber:"",
        source:1,
        paymentMethod:'bank',
        total_discount:0,
        products:[],
        searchProducts:[],
        showSearchInputProducts:false,
        inputSearchProduct:"",
        isGiftWrap:false,
        gift_wrapping:"",
        showShippingAmount:false,
        showCreateNewButton:false,
        showProductsEmptyState:true
    };
    data.stores = <?php echo json_encode(CatalogPOS::getStores()); ?>;
    data.sources = [];
    data.paymentMethods = [];
    data.shippingMethods = <?php echo json_encode($availableShippingMethods); ?>;
    data.isAllowReqToDHL = false;
</script>
<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/framework/pages/pos/invoice.js') }}"></script>