
<!doctype html>
<html>

<head>
    <title>Invoice No {{ $order['order']['order_number'] }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('morra/catalog/assets/print/css/receipt.css') }}">
</head>
<?php 
    $date = date('Y/n/j');
    $time = date('g:i:s A');
?>
<body onload="window.print();">
        <div class=" section section-two">
        
    
        <table style="font-weight:700;padding-bottom:0;padding-top:22px" cellspacing="0" cellpadding="0" align="center" border="0" width="500">
            <tr>
                <td>
                  Order #{{ $order['order']['order_number'] }}
                </td>
                <td style="text-align:right;font-weight:400;font-size:9px;">
                   Date: {{ $order['order']['order_date'] }}
                </td>
            </tr>
        </table>
        <table class="order-detail" cellspacing="0" cellpadding="0" align="center" border="0" width="500" style="padding-top:15px">
            <tr>
                <th style="font-weight:400;width:30%;">
                    ITEM
                </th>
                <th style="font-weight:400">
                    TOTAL (IDR)
                </th>
            </tr>
            @foreach($order['products'] as $key => $value)
            <tr>
                <td>
                    <span class="item-name">{{ $value['product_title'] }}</span>
                    <span class="sku">SKU: {{ $value['sku'] }}</span>
                    <span>{{ $value['stock_qty'] }} @ {{ number_format($value['original_price']) }}) - {{ number_format($value['discount']) }}</span>
                </td>
                <td>
                    {{ number_format($value['total']) }}
                </td>
            </tr>
            @endforeach
           
            
            
        </table>
        <table class="order-total" cellspacing="0" cellpadding="0" align="center" border="0" width="500">
            <tr>
                <td>
                    <span class="totalweight">TOTAL WEIGHT (kg): <b>{{$order['totalWeight']}}</b></span>
                </td>
                <td style="text-align:right">
                    <span>ITEM COUNT: <b>{{ count($order['products']) }}</b></span>
                </td>
            </tr>
            
        </table>
       <table class="instruction1" cellspacing="0" cellpadding="0" align="center" border="0" width="500" style="padding-top:10px;">
            <tr>
                <td style="padding-top:10px;font-weight:400;font-size:11px;">
                    {{$date}}&nbsp;{{$time}}
                </td>
                <td style="text-align:right;padding-top:10px;font-weight:700;">
                    <span style="height:auto;line-height:1em;!important;font-size:10px;">{{ $order['status'] }}&nbsp;&nbsp;&nbsp;R</span>
                </td>
            </tr>
        </table>
        
        <div class="section-footer">
            <span>Thank You For Shopping</span>
        </div>
    </div>
    <div class=" section section-one">
        <table class="shipping-info" cellspacing="0" cellpadding="0" align="center" border="0" width="500">
            <tr>

                <td style="padding-right:10px;">
                    <h6>Shipping :</h6>
                    <p class="receiver">{{ $order['shipping']->firstname.' '.$order['shipping']->lastname }}</p>

                    <p class="address last">{{ $order['shipping']->address }}<br>{{ $order['shipping']->district }} {{ $order['shipping']->city }}<br>{{ $order['shipping']->district }} {{ $order['shipping']->province }} {{ $order['shipping']->postcode }}</p>

                    <p class="phone">{{ $order['shipping']->phone }}</p>


                </td>
               <td style="padding-left:10px;">
                    <h6>Billing :</h6>
                    <p class="receiver">{{ $order['billing']->firstname.' '.$order['billing']->lastname }}</p>

                    <p class="address last">{{ $order['billing']->address }}<br>{{ $order['billing']->district }} {{ $order['billing']->city }}<br>{{ $order['billing']->district }} {{ $order['billing']->province }} {{ $order['billing']->postcode }}</p>

                    <p class="phone">{{ $order['billing']->phone }}</p>


                </td>

            </tr>

            



        </table>
        
      
        <table class="shippingdetail" cellspacing="0" cellpadding="0" align="center" border="0" width="500">
            <tr>
                <td style="width:70%">
                    <span><b>{{ $carrierName }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>#{{ $order['order']['order_number'] }}</span>
                </td>
                
                <td align="right">
                    <span style="font-size:9px;">{{$date}}&nbsp;{{$time}}</span>
                </td>
                <td align="right" style="padding-right:0px;width:15px;">
                    <span>R</span>
                </td>
            
            </tr>
            
        </table>
        
    </div>

   
</body>

</html>
