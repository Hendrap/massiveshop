<!doctype html>
<html>
<?php 
    $date = date('Y/n/j');
    $time = date('g:i:s A');
?>
<head>
     <title>Order #{{ $order['order']['order_number'] }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('morra/catalog/assets/print/css/receipt.css') }}">
</head>

<body onload="window.print();">
        <div class=" section section-two">
        
    
        <table style="font-weight:700;padding-bottom:0;padding-top:22px" cellspacing="0" cellpadding="0" align="center" border="0" width="500">
            <tr>
                <td>
                   ORDER #{{ $order['order']['order_number'] }}
                </td>
                <td style="text-align:right;font-weight:400;font-size:9px;">
                    {{$date}}&nbsp;{{$time}}
                </td>
            </tr>
        </table>
        <table class="order-detail" cellspacing="0" cellpadding="0" align="center" border="0" width="500" style="padding-top:15px">
            <tr>
                <th style="font-weight:400">
                    ITEM
                </th>
                <th style="font-weight:400">
                    QTY
                </th>
            </tr>

                    <?php 
                        $totalQty = 0;
                     ?>
            
                    @foreach($order['products'] as $key => $value)
                    <tr>
                        <td>
                             <span class="item-name">{{ $value['product_title'] }}</span>
                            <span class="sku">SKU: {{ $value['sku'] }}</span>
                        </td>
                        <td>
                            {{ $value['stock_qty'] }}
                            <?php $totalQty += $value['stock_qty']; ?>
                        </td>
                    </tr>
                    @endforeach
        </table>
        <table class="order-total" cellspacing="0" cellpadding="0" align="center" border="0" width="500">
            <tr>
                <td>
                    <span class="totalweight">TOTAL WEIGHT (kg): <b>{{ ($order['totalWeight']) }}</b></span>
                </td>
                <td style="text-align:right">
                    <span>ITEM COUNT: <b>{{ count($order['products']) }}</b></span>
                </td>
            </tr>
            
        </table>
       <table class="instruction1" cellspacing="0" cellpadding="0" align="center" border="0" width="500" style="padding-top:10px;">
            <tr>
                <td style="padding-top:10px;font-weight:400;font-size:11px;">
                    {{$date}} {{$time}}
                </td>
                <td style="text-align:right;padding-top:10px;font-weight:700;">
                    <span style="height:auto;line-height:1em;!important;font-size:10px;">{{ $order['status'] }}&nbsp;&nbsp;&nbsp;R</span>
                </td>
            </tr>
        </table>
        
        <div class="section-footer">
            <span>Thank You For Shopping</span>
        </div>
    </div>
    <div class=" section section-one">
        <table class="shipping-info" cellspacing="0" cellpadding="0" align="center" border="0" width="500">

            <tr>

                <!--<td>To:</td>-->

                <td colspan="2">

                    <p class="receiver">{{ $order['shipping']->firstname.' '.$order['shipping']->lastname }}</p>
                    <p class="address last">{{ $order['shipping']->address }}<br>{{ $order['shipping']->district }}<br>{{ $order['shipping']->city }} {{ $order['shipping']->postcode }}<br>{{ $order['shipping']->province }}</p>
                    <p class="phone">{{ $order['shipping']->phone }}</p>



                </td>

            </tr>

            @if(!empty($store) && !empty($storeData))
            <tr>
                <td>From:</td>
                <td>
                    <p class="sender">{{$store->name}} / {{$storeData->phone}}</p>
                </td>
            </tr>
            @endif



        </table>
        
        <table class="dimensiontable" cellspacing="0" cellpadding="0" align="center" border="0" width="500">
            <tr>
                <td >
                    <div class="dimension">

                        <div class="size">
                            <div class="container"></div>&nbsp;&nbsp;&nbsp;cm
                        </div>
                        <div class="weight">
                            <div class="container"></div>&nbsp;&nbsp;&nbsp;kg
                        </div>

                    </div>
                </td>
            </tr>
        </table>
        
        <table class="shippingdetail" cellspacing="0" cellpadding="0" align="center" border="0" width="500">
            <tr>
                <td style="width:50%">
                    <span><b>
                        {{ $carrierName }}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>#{{ $order['order']['order_number'] }}</span>
                </td>
                
                <td align="right">
                    <span style="font-size:9px;">{{$date}}&nbsp;{{$time}}</span>
                </td>
                <td align="right" style="padding-right:0px;width:15px;">
                    <span>R</span>
                </td>
            
            </tr>
            
        </table>
        
    </div>

     <div class=" section section-two">
        
        <div class="section-title">
        
           PACKER COPY
            
        </div>
        <br>
         <table style="padding:0">
         
             <tr>
                <td>
                     <span class="totalweight">
                        TOTAL QUANTITY: <b>{{$totalQty}}</b>
                     </span>
                 </td>
                 <td>
                     <span class="totalweight" style="float:right">
                        TOTAL WEIGHT(kg): <b>{{$order['totalWeight']}}</b>
                     </span>
                 </td>
             </tr>
         
         </table>
          <table class="instruction" cellspacing="0" cellpadding="0" align="center" border="0" width="500" style="padding-top:20px;padding-bottom:0px;">
            <tr>
                @if(!empty($order['order']['customer_note']))
                <td style="padding-right:30px;width:70%;vertical-align:middle">
                    <span style="padding-top:2px;vertical-align:middle;">Instruction</span>
                   
                   
                </td>
                @endif

                @if(!empty($order['order']['gift_wrapping']))
                <td style="text-align:right;vertical-align:middle;">
                    <div style="height:24px">
                        <div class="img-cont"><img src="http://beta.popitoi.com/public/bundles/site/img/gift.jpg" style="width:15px;"></div><span style="line-height:24px;padding-top:2px;vertical-align:middle;">Gift Wrap</span>
                    </div>
                </td>
                @endif
               
            </tr>
              <tr>
                <td colspan="2">
                     <p style="padding-top:10px;margin-top:0px;">{{ $order['order']['customer_note'] }}</p>
                  </td>
              </tr>
            
        </table>
         <table class="instruction"  style="padding-top:0px;"cellspacing="0" cellpadding="0" align="center" border="0" width="500">
             
             <tr>
                <td style="width:40%!important;font-weight:700;">
                    <span>ORDER #{{$order['order']['order_number']}}</span>
                </td>
                 <td style="text-align:right;font-weight:400;font-size:11px;">
                    <span>{{$date}}&nbsp;{{$time}}</span>
                </td>
             </tr>
             
         </table>
         
         <table class="signs" style="padding-top:20px;"cellspacing="0" cellpadding="0" align="center" border="0" width="500">
         
             <tr>
                <td>
                    <div>
                        <div class="sign-container"></div>
                        <span>Packer</span>
                    </div>
                 </td>
                 <td>
                     <div style="float:right">
                        <div class="sign-container"></div>
                        <span>Pick Up</span>
                     </div>
                 </td>
                
             </tr>
             
         </table>
         
    </div>
</body>

</html>