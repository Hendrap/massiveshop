<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Stock &amp; Inventory</span> - Edit Product</h4>
        </div>

    </div>


</div>
<div class="content" id="vueProduct">
    <form action="{{ route('catalog_product_variant_save',[$catalogType,$entry->id]) }}" method="post" class="form-horizontal">
        <div class="row">
            <div class="col-sm-9">
                <div class="panel panel-flat">
                    <div class="panel-heading clearfix">
                        @if(count(Config::get('alpha.application.locales')) > 1)
                        <div class="pull-right">
                            <select width="200" id="changeLang" class="select-language">
                                @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                    <option value="{{ $key }}">{{ $lang }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">{{ parseMultiLang($entry->title) }}</h2>
                        </div>
                    </div>
                    <div class="panel-body">
                        <p class="content-group-lg">Created on {{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($entry->created_at),app('AlphaSetting')->getSetting('time_format'))}} by {{ generateName($entry->user) }}, last modified on {{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('date_format'))}} at {{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</p>

                        @if(count($errors) > 0)
                        @foreach($errors->all() as $key => $error)
                        @if($key == 0)
                        <div class="alert bg-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            @if(Illuminate\Support\Str::contains($error,'required'))
                                <span class="text-semibold">Required field(s) error or missing</span>
                            @else
                                <span class="text-semibold">{{ str_replace('en.','',$error) }}</span>
                            @endif
                        </div>
                        @endif
                        @endforeach
                        @endif

                        <div class="form-group">                            
                            <label class="control-label col-lg-2">Product Name</label>
                              <div class="col-lg-10 clearfix">
                                @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                    <input style="display:none" id="title_{{$key}}" name="title[{{$key}}]" type="text" class="form-control" value="{{old('title.'.$key,parseMultiLang($entry->title,$key))}}">
                                @endforeach
                                <div class="post-url pull-left"><?php echo url($config['slug']) ?>/<span class="slug">{{ old('slug',$entry->slug) }}</span>
                                    <input type="text" style="display:none" name="slug" class="slug form-control" value="{{ old('slug',$entry->slug) }}">
                                </div>&nbsp;&nbsp;&nbsp;
                                 <div class="display-inline-block mt-10 post-url-buttons">
                                    <a href="javascript:void(0)" class="edit-url-custom">Edit</a>
                                    <a href="javascript:void(0)" class="save-url-custom" style="display:none"><i class="icon-checkmark4" style="font-size:10px;"></i></a>
                                        <span class="separator-url-form" style="display:none;opacity:0;">|</span>
                                    <a href="javascript:void(0)" class="cancel-edit-url-custom" style="display:none"><i class="icon-cross" style="font-size:15px;"></i></a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Brand</label>
                            <div class="col-lg-4">
                               <input id="tokenBrands" name="brand" type="text" class="form-control" placeholder="Type Brand name"> <!-- autocomplete -->
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-lg-2">Photos</label>
                            <div class="col-lg-12">
                               {!! view('alpha::admin.entry.templates.media-edit',['entry'=>$entry]) !!}
                            </div>
                        </div>
                       <!--  <div class="form-group">
                            <label class="control-label col-lg-2">Featured Image</label>
                             <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-10">
                                        <input readonly="" style="height: 36px" class="form-control" type="text" name="image_name" value="{{ old('image_name',@$entry->media->path) }}">
                                    </div>   
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-default" id="btnFeaturedImage">Select Image</button>
                                    </div> 
                                </div>
                               <input style="display: none;" type="text" name="image_id" value="{{ old('image_id',@$entry->media->id) }}">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="control-label col-lg-12">Description</label>
                              @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                <div class="col-lg-12">
                                    <textarea style="display:none" id="content_{{$key}}" name="content[{{$key}}]" rows="10"  class="text-editor" cols="80" >{{old('content.'.$key,parseMultiLang($entry->content,$key))}}</textarea>
                                 </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Excerpt</label>
                            <div class="col-lg-9">
                                 @foreach(Config::get('alpha.application.locales') as $key  => $lang)
                                    <textarea style="display:none" id="excerpt_{{$key}}" name="excerpt[{{$key}}]" class="form-control">{{ old('excerpt.'.$key,strip_tags(parseMultiLang($entry->excerpt,$key)))}}</textarea>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Product Dimension (cm)</label>
                            <div class="col-lg-9">
                               <input name="product_info[width]" value="{{ old('product_info.width',getEntryMetaFromArray($entry->metas,'product_info.width')) }}" type="text" class="numeric-prop form-control display-inline-block" placeholder="Width"  style="width:60px;"> <input name="product_info[height]" value="{{ old('product_info.height',getEntryMetaFromArray($entry->metas,'product_info.height')) }}" type="text" class="numeric-prop form-control display-inline-block" placeholder="height" style="width:60px;">  <input name="product_info[depth]" value="{{ old('product_info.depth',getEntryMetaFromArray($entry->metas,'product_info.depth')) }}" type="text" class="numeric-prop form-control display-inline-block" placeholder="depth" style="width:60px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Product Weight (kg)</label>
                            <div class="col-lg-9">
                               <input type="text" name="product_info[weight]" value="{{ old('product_info.weight',getEntryMetaFromArray($entry->metas,'product_info.weight')) }}" class="numeric-prop form-control display-inline-block" placeholder=""  style="width:60px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Shipping Dimension (cm)</label>
                            <div class="col-lg-9">
                               <input name="shipping_info[width]" value="{{ old('shipping_info.width',getEntryMetaFromArray($entry->metas,'shipping_info.width')) }}" type="text" class="numeric-prop form-control display-inline-block" placeholder="Width"  style="width:60px;">  <input name="shipping_info[height]" value="{{ old('shipping_info.height',getEntryMetaFromArray($entry->metas,'shipping_info.height')) }}" type="text" class="numeric-prop form-control display-inline-block" placeholder="height" style="width:60px;"> <input type="text" name="shipping_info[depth]" value="{{ old('shipping_info.depth',getEntryMetaFromArray($entry->metas,'shipping_info.depth')) }}" class="numeric-prop form-control display-inline-block" placeholder="depth" style="width:60px;">
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-lg-3">Shipping Weight (kg)</label>
                            <div class="col-lg-9">
                               <input name="shipping_info[weight]" value="{{ old('shipping_info.weight',getEntryMetaFromArray($entry->metas,'shipping_info.weight')) }}" type="text" class="numeric-prop form-control display-inline-block" placeholder=""  style="width:60px;">
                            </div>
                        </div>
                         @if(!empty($metas))
                        <?php $nMedia = 0; ?>
                        @foreach($metas as $meta)
                            @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'media_container' & $nMedia == 0)   
                                <div class="form-group">
                                <label class="control-label col-lg-3">{{ $meta->meta_name }}</label>
                                    <div class="col-lg-9">
                                        {!! view('alpha::admin.entry.templates.media-edit',['entry'=>$entry]) !!}
                                    </div>
                                </div>
                                <?php $nMedia++; ?>
                            @endif
                        @endforeach


                        @foreach($metas as $meta)
                             @if(!empty($meta->meta_key) && !empty($meta->meta_key) && (@$meta->meta_type == 'default' || empty($meta->meta_type)))                        
                                @if($meta->meta_key == "comingsoon")
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{ $meta->meta_name }}</label>
                                    <?php 
                                        $sizes = config('alpha.style.grids');
                                        $metaSize = @$sizes[$meta->meta_size];
                                        $old = getEntryMetaFromArray($entry->metas,$meta->meta_key);
                                        if(empty($metaSize)) $metaSize = 'col-lg-9';
                                     ?>
                                    <div class="{{ $metaSize }}">
                                        <select name="metas[comingsoon]" class="select form-control">
                                            <option value="yes" @if($old == "yes") selected @endif>Yes</option>
                                            <option value="no" @if($old == "no") selected @endif>No</option>
                                        </select>
                                    </div>
                                </div>
                                @else
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{ $meta->meta_name }}</label>
                                    <?php 
                                        $sizes = config('alpha.style.grids');
                                        $metaSize = @$sizes[$meta->meta_size];
                                        if(empty($metaSize)) $metaSize = 'col-lg-9';
                                     ?>
                                    <div class="{{ $metaSize }}">
                                        <?php echo view('alpha::admin.meta.'.$meta->meta_data_type,[
                                                            'meta_key' => $meta->meta_key,
                                                            'value' => old('metas.'.$meta->meta_key,getEntryMetaFromArray($entry->metas,$meta->meta_key))
                                                             ]) ?>
                                    </div>
                                </div>
                                @endif
                                
                            @endif
                        @endforeach


                        @foreach($metas as $meta)

                        @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'file_upload')                        
                            <div class="form-group" id="meta_{{$meta->meta_key}}">
                                <label class="control-label col-lg-3">{{ $meta->meta_name }}</label>
                                <div class="col-lg-9">
                                <div class="input-group">
                                    <input value="{{ getEntryMetaFromArray($entry->metas,$meta->meta_key) }}" readonly="" name="file_upload[{{$meta->meta_key}}]" id="file_upload_{{$meta->meta_key}}" style="height: 36px" type="text" class="form-control" placeholder="File name">
                                    <input class="meta_uploader" data-target="file_upload_{{$meta->meta_key}}" type="file" name="uploader[{{$meta->meta_key}}]" style="display:none">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-uploader-metas" type="button">
                                        @if(empty(getEntryMetaFromArray($entry->metas,$meta->meta_key)))
                                        Upload File
                                        @else
                                        Remove
                                        @endif
                                        </button>
                                    </span>
                                </div>
                                </div>
                            </div>
                        @endif

                        @if(!empty($meta->meta_key) && !empty($meta->meta_key) && @$meta->meta_type == 'media')         
                             <div class="form-group" id="meta_{{$meta->meta_key}}">
                                <label class="control-label col-lg-3">{{ $meta->meta_name }}</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <input value="{{ getEntryMetaFromArray($entry->metas,$meta->meta_key) }}" readonly="" name="media_container[{{$meta->meta_key}}]" id="media_container{{$meta->meta_key}}" style="height: 36px" type="text" class="form-control" placeholder="Media name">
                                        <input class="media_uploader" type="text" id="media_ids{{$meta->meta_key}}" name="media_ids[{{$meta->meta_key}}]" style="display:none">
                                        <span class="input-group-btn">
                                            <button data-meta="{{$meta->meta_key}}" class="btn btn-default btn-uploader-media" type="button">
                                            @if(empty(getEntryMetaFromArray($entry->metas,$meta->meta_key)))
                                            Open Media Library
                                            @else
                                            Remove
                                            @endif
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endif



                        @endforeach
                        @endif
                    </div>
                </div>

               

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Product Variant</h2>
                        </div>
                    </div>
                    <div class="panel-body">
                        <p class="content-group-lg">Add variant specific categories (color, size, etc) and photos (in respect to color, shape or other options). Title will used in addition to the product name.</p>
                        <table class="table-variant table" id="tblContainer">
                            <thead>
                                 <tr>
                                    <th width="75px" class="no-sort-var"></th>
                                    <th class="no-sort-var" width="150px">SKU</th>
                                    <th class="no-sort-var">Title</th>
                                    <th class="no-sort-var" width="110px">MSRP</th>
                                    <th class="no-sort-var" width="70px">QTY</th>
                                    <th class="no-sort-var" width="70px">Limit</th>
                                    <th class="no-sort-var" width="110px">Price</th>
                                    <th class="no-sort-var" width="140px">Sale Price</th>
                                    <th width="45px" class="no-sort-var"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!empty(old('item_id')))
                            @foreach(old('item_id') as $key => $value)
                              {!! 
                                  view('catalog::admin.pages.items.templates.item',[
                                        'item_id' => $value,
                                        'item_title' => old('item_title')[$key],
                                        'order_limit' => old('order_limit')[$key],
                                        'sku' => old('sku')[$key],
                                        'msrp' => old('msrp')[$key],
                                        'qty' => old('qty')[$key],
                                        'qty_available' => old('qty_available')[$key],
                                        'price' => old('price')[$key],
                                        'sale_price' => old('sale_price')[$key],
                                        'sale_start' => old('sale_start')[$key],
                                        'sale_end' => old('sale_end')[$key],
                                        'item_medias' => old('item_medias')[$key],
                                        'item_taxonomies' => old('item_taxonomies')[$key],
                                        'item_metas' => old('item_metas')[$key],
                                  ]) 

                              !!}
                            @endforeach

                            @else

                            @if(!empty($entry->activeItems))
                                <?php 
                                    foreach ($entry->activeItems as $key => $value) {
                                        $itemTaxonomies = [];
                                        $itemMedias = [];
                                        if(!empty($value->taxonomies))
                                        {
                                            foreach ($value->taxonomies as $k => $v) {
                                                $itemTaxonomies[] = $v->id;
                                            }
                                        }

                                        if(!empty($value->medias))
                                        {
                                            foreach ($value->medias as $k => $v) {
                                                $itemMedias[] = $v->id;
                                            }
                                        }

                                        echo view('catalog::admin.pages.items.templates.item',[
                                            'item_id' => $value->id,
                                            'item_title' => $value->item_title,
                                            'sku' => $value->sku,
                                            'order_limit' => $value->order_limit,
                                            'msrp' => $value->msrp,
                                            'qty' => $value->stock,
                                            'qty_available' => $value->stock_available,
                                            'price' => $value->price,
                                            'sale_price' => $value->sale_price,
                                            'sale_start' => ($value->sale_start_date == '0000-00-00 00:00:00') ? '' : $value->sale_start_date,
                                            'sale_end' => ($value->sale_end_date == '0000-00-00 00:00:00') ? '' : $value->sale_end_date,
                                            'item_medias' => json_encode($itemMedias),
                                            'item_taxonomies' => json_encode($itemTaxonomies),
                                            'item_metas' => ''
                                      ]); 
                                    }

                                ?>

                                @endif

                                @endif

                                @if(empty(old('item_id')) && empty($entry->activeItems))
                                  {!! 

                                      view('catalog::admin.pages.items.templates.item',[
                                        'item_id'  => 0,
                                        'item_title' => '',
                                        'order_limit' => 0,
                                        'sku' => '',
                                        'msrp' => 0,
                                        'qty' => 0,
                                        'qty_available' => 0,
                                        'price' => 0,
                                        'sale_price' => 0,
                                        'sale_start' => '',
                                        'sale_end' => '',
                                        'item_medias' => '',
                                        'item_taxonomies' => '',
                                        'item_metas' => '',
                                      ]) 


                                  !!}
                                @endif
                                </tbody>
                        </table>
                    </div>
                </div>
                 <div class="panel panel-flat">
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Search Engine Optimization</h2>
                        </div>
                    </div>
                     <div class="panel-body">
                        
                        <fieldset>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="seo[title]" value="{{ old('seo.title',getEntryMetaFromArray($entry->metas,'seo_title')) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Keyword</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="seo[keys]" value="{{ old('seo.keys',getEntryMetaFromArray($entry->metas,'seo_keys')) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Description</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="seo[desc]" value="" style="height:80px;">{{ old('seo.desc',getEntryMetaFromArray($entry->metas,'seo_desc')) }}</textarea>
                            </div>
                        </div>
                            
                        </fieldset>
                        
                    </div>
                </div>
            </div>


            <div class="col-sm-3">

                <div class="panel right-small">
                
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h2 class="text-semibold">Publish</h2>

                        </div>


                    </div>
                    <div class="panel-body">
                    <?php $status = ['featured','published','draft','disabled','scheduled_publish'] ?>
                    <?php

                        $selectedStatus = old('status',$entry->status);
                        if(strtotime($entry->published_at) > time())
                        {
                            $selectedStatus = 'scheduled_publish';
                        }
                     ?>
                        
                        <fieldset>
                        <div class="form-group no-margin-left no-margin-right">
                            <select class="select-two form-control @if(count($errors->get('status'))) invalid-input @endif" name="status" id="publish_option">
                              @foreach($status as $s)
                                    <option <?php echo ($s == $selectedStatus) ? 'selected' : '' ?> value="{{ $s }}"><?php $s = str_replace('_publish','',$s) ?>{{  Illuminate\Support\Str::title($s) }}</option>
                                @endforeach
                            </select>
                        </div>
                       <div class="form-group has-feedback publish-date no-margin-left no-margin-right" style="<?php echo (old('status',$selectedStatus) == 'scheduled_publish') ? '' : 'display: none' ?> ">
                            <input type="text" name="published_at" class="date-picker form-control" value="{{ date('d M Y g:i a',strtotime(old('published_at',$entry->published_at))) }}">
                            <div class="form-control-feedback">
                                <i class="icon-calendar" style="line-height:30px;"></i>
                            </div>
                        </div>
                        <div class="form-group no-margin-bottom no-margin-left no-margin-right">
                            <button type="submit" class="btn btn-primary pull-right ml-10">Save</button>
                            <a href="{{route('alpha_admin_entry_index',[$type])}}" class="btn bg-slate pull-right">Cancel</a>
                        </div>
                        </fieldset>
                    </div>
                
                
                </div>
                
                
                 <div class="panel right-small">
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">Taxonomy</h5>
                        </div>
                    </div>
                    <div class="panel-body">
                        
                        <fieldset>
                        
                        @foreach($parsedTaxonomies['triTaxo'] as $key => $value)
                            <div class="form-group no-margin-left no-margin-right">
                                <label>{{  $infoTaxoContainer[$key]['single']  }}</label>
                                @foreach($value as $item)
                                    <div class="checkbox">
                                        <label>
                                            <input <?php echo in_array($item->id, old('taxonomies',$selectedTaxo)) ? 'checked' : '' ?> name="taxonomies[]" value="{{ $item->id }}" type="checkbox" class="styled">
                                            {{$item->name}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                        
                        @foreach($parsedTaxonomies['fiveTaxo'] as $key => $value)
                        <div class="form-group no-margin-left no-margin-right">
                            <div class="multi-select-full">
                                <select data-placeholder="Select {{ strtolower($infoTaxoContainer[$key]['single']) }}" name="taxonomies[]" class="multiselect-filtering" multiple="multiple">
                                @foreach($value as $item)
                                    <option <?php echo in_array($item->id, old('taxonomies',$selectedTaxo)) ? 'selected' : '' ?> value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        @endforeach
                        
                                            
                         <div class="form-group no-margin-left no-margin-right" style="margin-bottom:5px">
                            <label>Keyword Tags</label>
                            <input placeholder="Your keyword tags" data-role="tagsinput" value="{{ old('metas.alpha_keyword_tags',getEntryMetaFromArray($entry->metas,'alpha_keyword_tags')) }}" name="metas[alpha_keyword_tags]" type="text" class="keyword-tag form-control" style="width:100%;!important" readonly>
                        </div>
                        <div class="form-group clearfix add-keywords no-margin-left no-margin-right">
                             <div class="input-group">
                                 <input type="text" placeholder="Start typing your keyword tags" class="form-control pull-left keyword-input">
                                 <span class="input-group-btn">
                                    <button class="btn btn-primary pull-left add-tag-button" type="button">+</button>
                                </span>
                             </div>
                        </div>
                            
                            
                        </fieldset>
                    </div>
                
                
                </div>
                
            </div>
        </div>
        {{ csrf_field() }}
    </form>
    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->
    {!! view('catalog::admin.pages.product.template.modal-variant',['parsedTaxonomiesItem'=>$parsedTaxonomiesItem]) !!}
    {!! view('alpha::admin.entry.templates.response-handler',['errors'=>@$errors]) !!}
    {!! view('alpha::admin.entry.templates.entry-script',['editMode'=>true]) !!}
    {!! view('alpha::admin.entry.templates.edit-slug',['entry'=>$entry]) !!}

    
</div>

<link rel="stylesheet" type="text/css" href="{{ asset('morra/catalog/assets/css/token-input-facebook.css') }}">
<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/autoNumeric-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/jquery.tokeninput.js') }}"></script>
{!! view('catalog::admin.pages.product.template.catalog-script',['brands'=>$brands,'entry'=>$entry,'errors'=>@$errors  ]) !!}
