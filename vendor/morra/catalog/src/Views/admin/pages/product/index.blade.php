<!-- Content area -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Stock &amp; Inventory</span> - Product Catalog</h4>
        </div>

        <div class="heading-elements">
            <a href="{{ route('catalog_product_variant_create',[$catalogType]) }}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>New Product</a>
        </div>
        
    </div>


</div>

<div class="content">

    

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h1 class="text-semibold">Product Catalog</h1>
                        
                    </div>

                   
                </div>
                <div class="panel-body">

                    <p>Manage product information and availability</p>

                </div>
                    

                     <div class="table-header datatable-header clearfix">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-5 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter</label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
                        
                        <select class="table-select2 pull-left filter-table-column" placeholder="Filter by Brand" searchin="7">
                            <option value="all">All Brands</option>
                            @foreach($entryData->brands as $key => $value)
                            <option value="{{$value->id}}">{{ $value->title }}</option>
                            @endforeach
                        </select>
                         
                         <select class="table-select2 pull-left filter-table-column" placeholder="Filter by Availability" searchin="5">
                            <option value="all">All Availability</option>
                            <option value="Active">Active</option>
                            <option value="out-of-stock">Out of Stock</option>
                            <option value="disabled">Disabled</option>
                        </select>
                         
                        <select class="table-select2 pull-left filter-table-column" placeholder="Filter by Category" searchin="8">
                            <option value="all">All Categories</option>
                            @foreach($entryData->taxonomies as $key => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach
                         </select>
                        
                       
                        
                    </div>
                    
                    <table class="table entry-table table-striped">
                        <thead>
                            <tr>
                                <th width="50px" class="no-sort"></th>
                                <th>Name</th>
                                <th width="120px">Stock</th>
                                <th width="119px"><span>Price</span></th>
                                <th width="150px"><span>Modified</span></th>
                                <th width="130px">Status</th>
                                <th width="50px" class="no-sort text-right"></th>
                                <th width="50px" class="no-sort hide">Brand</th>
                                <th width="50px" class="no-sort hide">Category</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <script>
                    var entry_table = {};
                      $(document).ready(function() {

                           var childClass = 'title-column';
                            entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                            if(typeof entry_table.ajax != 'undefined')
                                            {
                                                 $(e.target).find('tbody').html('');
                                            }
                                             $('.dataTable, .dataTable tbody').css('position','static');
                                            $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                        $.unblockUI();
                                        $(".blockUI").remove();
                                         $('.dataTable, .dataTable tbody').css('position','relative');
                                        $('[data-popup=popover]').popover();
                                     }
                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "order": [],
                                "pageLength": 50,
                                "searching" : true,
                                "lengthChange": false,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "processing": true,
                                "serverSide": true,
                                "ajax": {
                                    "url": "<?php echo route('catalog_product_variant_get_data',[$catalogType]) ?>",
                                    "type": "POST"
                                },
                                "columns": [
                                    { "data": "title", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[0];
                                    },
                                    sClass:""
                                    },


                                    { "data": "user", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[1];
                                    },
                                    sClass:"title-column"

                                    },

                                    { "data": "published", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[2];
                                    },
                                    sClass:"td-date"

                                    },


                                    { "data": "created", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[3];
                                    },
                                    sClass:""

                                    },


                                    { "data": "last_modified", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[4];
                                    },
                                    sClass:"td-date"

                                    },

                                    { "data": "status", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[5];
                                    },
                                    sClass:""

                                    },

                                     { "data": "action", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[6];
                                    },
                                    sClass:"text-right table-actions"

                                    },

                                    { "data": "brand", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[7];
                                    },
                                    sClass:"hide"

                                    },
                                    <?php foreach ($entryData->taxo as $key => $value): ?>
                                    { "data": "{{$value['slug']}}", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[{{$key + 8}}];
                                    },
                                    sClass:"hide"

                                    },
                                    <?php endforeach ?>
                                    <?php if(empty($entryData->taxo)){ ?>
                                        { "data": "category", "render": function(data, type, full, meta){
                                        var html = full.html.split('|alpha--datatable-separator--|');
                                        return html[8];
                                    },
                                    sClass:"hide"

                                    },
                                    <?php } ?>
                                ],
                                "columnDefs": [
                                    {
                                        "targets": 'no-sort',
                                        "orderable": false,
                                    }
                                ]
                            });
                           
                            var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                });
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                            });
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });

                            $('.table-select2').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });

                            $('.table-select2.with-search').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: 1,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });

                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13){
                                    entry_table.search($(this).val()).draw();
                                }
                            });

                        });

                        $(document).ready(function() {                            
                            
                            
                            $('select').select2({
                                minimumResultsForSearch: Infinity,
                                width: 'auto'
                            });
                            $('.table-select2').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',

                                });

                            });
                        });

                    </script>


                


            </div>

        </div>

    </div>



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<script type="text/javascript">
                        var status = "{{session('msg')}}";
                        if (status === 'Status Changed!') {
                                swal({
                                    title: "SUCCESS",
                                    text: "All changes has been saved successfuly",
                                    confirmButtonColor: "#66BB6A",
                                    type: "success",
                                    closeOnConfirm: false,
                                    },function(isConfirm){
                                        swal.disableButtons();
                                        swal.close();
                                    });
                        }
                    </script>
                    <script type="text/javascript">                        
                        $(document).on('click',".confirm",function(e){
                                var that = this;
                                e.preventDefault();
                                 swal({
                                title: "Delete ?",
                                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                closeOnConfirm: false },
                                function(isConfirm){
                                    if(isConfirm){
                                        window.location.href = $(that).attr('href');
                                    }
                                });
                            });

                    </script>
<!-- /content area -->