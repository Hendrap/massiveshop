<div id="modalCatalogProduct" class="modal fade in" role="dialog">
    <div class="modal-dialog" style="width:750px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title text-bold">Featured Image</h5>
            </div>
            <div class="modal-body">
                 <div class="media-popup-content">
                    <div id="bodymodalCatalogProduct" class="row" style="position: relative; zoom: 1;">
                        <div class="col-sm-3">
                            <div class="media-popup-thumbnail">
                                 <img onerror="" src="" alt="image name">
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>
<script>
        $(document).ready(function() {
            $("#vueProduct form").submit(function(e){
                var list = $("input.invalid-price");
                if(list.length)
                {
                    e.preventDefault();
                    $.each(list,function(i,k){
                        $(k).focus();
                    });
                }
            });
            var asseturl = "<?php echo asset('') ?>";
            $("#btnFeaturedImage").click(function(e){
                e.preventDefault();
                var photos = $(".product-photo");
                var html = "";
                $("#bodymodalCatalogProduct").html('')
                $.each(photos,function(i,k){
                    var container = document.createElement("div");
                    container.className = 'col-md-3 img-featured-image';
                    container.id = "featured-image-" + $(k).find('.mid').val();
                    var div = document.createElement('div');
                    div.className = 'media-popup-thumbnail';
                    var img = document.createElement("img");
                    img.src = $(k).find('img').attr('src');
                    div.appendChild(img);
                    container.appendChild(div);
                    $("#bodymodalCatalogProduct").append(container);
                });
                $("#modalCatalogProduct").modal('show');
            });
            $(document).on('dblclick','.img-featured-image',function(e){
                e.preventDefault();
                var id = $(this).attr('id').replace('featured-image-','');
                var name = $(this).find('img').attr('src').replace(asseturl,'');
                $("#imageName").html(name);
                $("[name=image_name]").val(name);
                $("[name=image_id]").val(id);
                $("#modalCatalogProduct").modal('hide');
            });
            var tableItem = $('#tblContainer').DataTable({
                                "order": [],
                                "pageLength": 1000,
                                "searching" : false,
                                "lengthChange": false,
                                "dom": 'rt<"clear">',
                                "columnDefs": [{
                                    "targets": 'no-sort-var',
                                    "orderable": false,
                                }]
            });


            @if(count($errors))
            
                var sku = $(".alpha-sku");
                $.each(sku,function(i,k){
                    if($(k).val().trim() == ''){
                        $(k).css('border-color','red');
                        $(k).tooltip({
                            animation:true,
                            placement:'bottom',
                            title:"The sku field is required.",
                            trigger:"focus",
                            template:'<div class="tooltip red-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                        });
                    }
                })
            @endif
           
            $('.add-product-photo-trigger').click(function(e){
                 e.preventDefault();
                $('#modal_add_image').modal();
            });
            $('.select-variant-image').click(function(e){
                e.preventDefault();
                $('#modal_variant-image').modal();
            });

            $('.sales-date').click(function(e){
                e.preventDefault();
            })
            
            
            // $('#select_availability').select2({
            //     placeholder: 'select availability',
            //     minimumResultsForSearch: 5,
            // });
            
            // $('#select_watchdog').select2({
            //     minimumResultsForSearch: Infinity,
            // });
            
            $('.filter-table-column').select2({
                    minimumResultsForSearch: Infinity,
                    width: '200px',
            });
        
            autosize($('textarea'));
        });

    </script>
    <script type="text/javascript">
    var templateItem = '<?php echo newLine(view('catalog::admin.pages.items.templates.item',[
                                    'item_id'  => 0,
                                    'item_title' => '',
                                    'sku' => '',
                                    'order_limit' => 0,
                                    'msrp' => 0,
                                    'qty' => 0,
                                    'qty_available' => 0,
                                    'price' => 0,
                                    'sale_price' => 0,
                                    'sale_start' => '',
                                    'sale_end' => '',
                                    'item_medias' => '',
                                    'item_taxonomies' => '',
                                    'item_metas' => '',
                                  ])); ?>';
    var openerEl = false;
    var openerMedia = false;

    function initItem(el)
    {
        var parent = $(el).parent().parent().parent().parent();

        $(el).daterangepicker({
            "opens": "left",
            "drops": "up",
            autoApply:true,
        });
            
        $(el).on('apply.daterangepicker', function(ev, picker) {
            var selectedsaledate = picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY');
            $(el).parent().find('.sale_start').val(picker.startDate.format('YYYY-MM-DD'));
            $(el).parent().find('.sale_end').val(picker.endDate.format('YYYY-MM-DD'));
            $(el).parent().parent().find('.salesdate-string').html('');
            $(el).parent().parent().find('.salesdate-string').html(selectedsaledate);
        });


        var qtyEl = $(parent).find('.qty');
        var priceEl = $(parent).find('.price');

        $(priceEl).autoNumeric('init',{aSign:'', aSep: ',', aDec: '.', mDec: 0,wEmpty: 'zero' });
        $(qtyEl).autoNumeric('init',{aSign:'', aSep: '', aDec: ' ', mDec: 0,wEmpty: 'zero' });

         $('[data-toggle="tooltip"]').tooltip();
    }

    $(document).on('keyup','.price-check',function(e){
         var parent = $(this).parents('tr');
         $(parent).find('.sale-price-check').trigger('keyup');
    });

    $(document).on('keyup','.sale-price-check',function(e){

        var parent = $(this).parents('tr');
        var salePrice = parseInt($(this).autoNumeric('get'));
        var normalPrice  = parseInt($(parent).find('.price-check').autoNumeric('get'));
        console.log([salePrice,normalPrice]);
        if(salePrice >= normalPrice){
             console.log("Fire");
             $(this).addClass('invalid-price');
             $(this).popover({
                 template: '<div class="popover bg-danger"><div class="arrow"></div><div class="popover-content" style="padding:5px;"></div></div>',
                 content:"Sale price cannot be more than normal price <br> Make sure sale end date is in the future",
                 placement:'top',
                 html:true,
                 trigger:'focus'
            }).popover('show');

        }else{
            $(this).popover('destroy');
            $(this).removeClass('invalid-price');
        }
    });
    
    $(document).on('click','.add-variant',function(e){
        e.preventDefault();
        $("#tblContainer").append(templateItem);
        initItem( $(".sales-date").get( $(".sales-date").length - 1 ) );
    });
    $(document).on('click','.table-variant tbody tr:last-of-type td input',function(e){
        e.preventDefault();
        $("#tblContainer").append(templateItem);
        initItem( $(".sales-date").get( $(".sales-date").length - 1 ) );
    });
    $(document).on('click','.delete-variant',function(e){
            var that = this;
            e.preventDefault();
            swal({
                title: "Delete ?",
                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false },
                function(isConfirm){
                       if(isConfirm){
                        $(that).parents('tr').remove();
                        swal.close();
                    }
            });
     });



    $(document).on('click','.select-variant-category',function(e){
        e.preventDefault();
        var dropDowns = $("#modal_variant_category").find(".multiselect-filtering");
        $.each(dropDowns,function(i,k){
            $(k).multiselect("deselectAll",false);
            $(k).multiselect('refresh');
        });

        $("#modal_variant_category").find('li').removeClass('active');
        $("#modal_variant_category").find('span').removeClass('checked');

        var el = $(this).parent().parent();
        var currentValue = $(el).find('.item_taxonomies').val();
        
        openerEl = $(el).find('.item_taxonomies');
        if(currentValue.trim() != "")
        {
            currentValue = JSON.parse(currentValue);
            if(currentValue.length)
            {
                 $.each(dropDowns,function(i,k){
                    $(k).multiselect("select",currentValue);
                    $(k).multiselect("refresh");
                 });

                 var checkbox = $("#modal_variant_category").find('[type=checkbox]');
                 $.each(checkbox,function(i,k){
                    if(currentValue.indexOf(parseInt($(k).val())) >= 0)
                    {
                         
                        $(k).parents('span').addClass('checked');
                        $(k).parents('li').addClass('active');                        
                    }
                 });

            }
        }
        $('#modal_variant_category').modal();
    });
    
    $(document).on('click','.select-variant-image',function(e){
         e.preventDefault();
         var params = {};
         params.page = 1;
         params.medias = [];
         var photos = $(".product-photo");
         $.each(photos,function(i,k){
            params.medias.push($(k).find('.mid').val());
        });

         if(params.medias.length == 0)
         {
            params.medias.push(0);
         }

         $("#labelEntryMedia").html("Select photos(s) from your products uploaded photos which matches your current variant.");
         openerMedia = $(this).parent().parent().find(".media_id");
         loadMedias("<?php echo route('alpha_admin_media_entry') ?>" + '?' + $.param(params),function(){
                    $("#modalEntryMedia").modal('show');    
                    editorName = 'medias.opener';
                    $(".media-popup-header").hide();
                    $("#defaultModalHeader").hide();
                    $("#catalogModalHeader").show();

                    $("#modalDialogEntryMedia").css('width','650px');
                    $("#containerRowEntryMedia").addClass('image-list');
                    $("#containerRowEntryMedia").removeClass('media-popup-content')


                });
    });

    $("#saveItemTaxo").click(function(e){
        e.preventDefault();
        var selected = new Array();
        var checkedItem = $("#modal_variant_category").find('option:selected');
        for (var i = checkedItem.length - 1; i >= 0; i--) {
          selected.push(parseInt($(checkedItem[i]).val()));
        }

        if(openerEl)
        {
            $(openerEl).val(JSON.stringify(selected));
        }

        $("#modal_variant_category").modal('hide');

    });

    var items = $(".sales-date");

    for(i=0;i<=items.length;i++)
    {
        initItem(items[i]);
    }
    var brands = <?php echo json_encode($brands) ?>;
    <?php 
        $brandId = old('brand',@$entry->brand_id);
        if(empty($brandId)) $brandId = 0;
    ?>
    var oldId = <?php echo $brandId; ?>;
    var selectedBrand = new Array();
    var isPrePopulate = false;
    for (var i = brands.length - 1; i >= 0; i--) {
        if(parseInt(brands[i].id) == parseInt(oldId)){
            selectedBrand.push(brands[i]);
            isPrePopulate = true;
        }
    }

    $("#tokenBrands").tokenInput(brands, {
        hintText: "Start typing the brand name",
       // placeholder: "Type Brand name",
        noResultsText: "Not found? New brand names need to be registered first.",
        searchingText: "Searching ...",
        minChars: 1,
        deleteText:'<i class="icon-cross" style="font-size:15px;"></i>',
        tokenLimit: 1,
        prePopulate:selectedBrand,
        processPrePopulate:isPrePopulate,
        preventDuplicates: true,
        propertyToSearch: "title",
        resultsFormatter: function(item){ 
            return "<li>" + "<div style='display: inline-block;'><div class='full_name'>" +" " + item.title + "</div><div class='email'> </div></div></li>";
        },  
        onAdd: function (item) {
            $("#tokenBrands").val(item.id);
        },
        onDelete: function (item) {
            $("#tokenBrands").val(0);
        },
        onResult: function (results) {
            return results;
        }
     });

     $(".numeric").autoNumeric('init',{aSign:'', aSep: '', aDec: ' ', mDec: 0,wEmpty: 'zero' });
     $(".numeric-prop").autoNumeric('init',{aPad: false,aSign:'', aSep: '', aDec: '.', mDec: 2,wEmpty: 'zero' });

</script>