<div id="modal_variant_category" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:450px">
        <div class="modal-content">
            <div class="modal-header border-bottom" style="border-color: #ddd;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h2 class="text-semibold modal-title">Select Categories</h5>
                <span style="margin-top:20px;display:block;margin-bottom: 20px;">Select categories matching current product variant</span>
            </div>

            <div class="modal-body">
                @foreach($parsedTaxonomiesItem as $key => $taxonomies)
                @foreach($taxonomies as $k => $value)
                <div class="select-category-row">
                 
                    <div class="multi-select-full">
                        <select data-placeholder="Select {{ Illuminate\Support\Str::title(Illuminate\Support\Str::plural(str_replace('-',' ',strtolower($k)))) }}" name="taxonomies_item[]" class="multiselect-filtering" multiple="multiple">
                                @foreach($value as $taxoKey =>  $taxoItem)
                                <option value="{{$taxoItem->id}}">{{$taxoItem->name}}</option>
                                 @endforeach
                        </select>
                    </div>
                </div>
                @endforeach
                @endforeach
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                <button id="saveItemTaxo" type="button" class="btn btn-primary select-image">Save</button>
            </div>
        </div>
    </div>
</div>
