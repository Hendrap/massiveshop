                                <?php 
                                    $html = '';
                                    // if(!empty($entry->media))
                                    // {
                                    //     $html = "<img src='".asset(getCropImage(@$entry->media->path,'default',true))."' alt='".parseMultiLang($entry->title)."' style='width:200px'>";
                                    // }
                                    
                                    if(!empty($entry->medias[0]))
                                    {
                                        $html = "<img src='".asset(getCropImage(@$entry->medias[0]->path,'default',true))."' alt='".parseMultiLang($entry->title)."' style='width:200px'>";
                                    }

                                 ?>
                                @if(!empty($html))
                                <td><a href="javascript:void(0)" class="catalog-preview" data-popup="popover" data-trigger="hover" data-content="<?php echo $html ?>" data-html="true"> <i class="icon-camera"></i></a></td>
                                @else
                                <td><a href="javascript:void(0)" class="catalog-preview no-image"> <i class="icon-camera"></i></a></td>
                                @endif
                                |alpha--datatable-separator--|<td class="column-title">
                                    <h6><a href="{{ route('catalog_product_variant_edit',[$catalogType,$entry->id]) }}">{{ parseMultiLang($entry->title) }} {{ !empty($entry->item_title) ? '('.$entry->item_title.')' : '' }}</a></h6>
                                    <span class="text-muted">
                                        @if(!empty($entry->sku))
                                           {{ !empty($entry->brand) ? parseMultiLang($entry->brand->title).' - ' : '' }}{{ $entry->sku }}
                                        @else
                                            # {{$entry->item_id}}
                                        @endif
                                    </span>
                                
                                </td>
                                |alpha--datatable-separator--|<td>{{ ($entry->stock_available) }}</td>
                                |alpha--datatable-separator--|<td>
                                  <span class="payment-ammount">
                                       {{ number_format($entry->price) }}
                                    </span>
                                </td>
                                |alpha--datatable-separator--|<td class="td-date">
                                            {{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('date_format'))}}
                                            <br><span class="text-muted">{{date_format(date_create($entry->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                </td>
                                
                                |alpha--datatable-separator--|<td>
                                @if($entry->status == 'published' && $entry->item_status == 'active' && $entry->stock_status == 1)
                                <span class="label bg-success">Active</span>
                                @elseif($entry->status == 'disabled')
                                <span class="label bg-danger">Disabled</span>
                                 @elseif($entry->stock_status == 0)
                                <span class="label bg-warning" style="color:black">Out of Stock</span>
                                @else
                                <span class="label bg-info">{{ ucfirst($entry->status) }}</span>
                                @endif
                                </td>
                                |alpha--datatable-separator--|<td class="text-right table-actions">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('catalog_product_variant_edit',[$catalogType,$entry->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
                                            @if($entry->status != 'disabled')
                                            <li><a href="{{ route('catalog_product_variant_status',[$entry->id,'disabled']) }}"><i class="icon-cancel-square"></i> Disable</a></li>
                                            @endif
                                            @if($entry->status != 'published')
                                            <li><a href="{{ route('catalog_product_variant_status',[$entry->id,'published']) }}"><i class="icon-checkmark4"></i> Publish</a></li>
                                            @endif
                                            <li class="border-top"><a class="confirm" href="{{ route('catalog_product_variant_status',[$entry->id,'deleted']) }}"><i class="icon-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                                |alpha--datatable-separator--|<td class="hide">Ini Brand</td>

                                @if(empty($taxoInfo->taxo))
                                |alpha--datatable-separator--|<td class="hide">Ini Cat</td>
                                @endif
                                @foreach($taxoInfo->taxo as $key => $item)
                                        |alpha--datatable-separator--|<td class="hide">Ini Cat</td>
                                @endforeach
