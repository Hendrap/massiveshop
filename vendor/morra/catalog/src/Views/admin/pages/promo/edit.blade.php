<style type="text/css">
    .ui-autocomplete, .customer-search-result{
        z-index: 100000;
    }
    [v-cloak] {
        display: none;
    }
     .token-input-list{
        width: 100%!important;
    }
</style>
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Promotions </span> - Edit Promotion</h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="">Promo Code</a></li>
                <li class="active">Edit Promo Code</li>
            </ul>
        </div>
    </div>
    
    <form name="alphaForm" enctype="multipart/form-data" action="{{route('catalog_promo_save')}}" method="POST" class="form-horizontal">
        <div class="row">
            <div class="col-sm-9">
                <div class="panel panel-flat">
                    <div class="panel-heading clearfix">
                        <div class="panel-heading-title">
                            <div style="width:50%">
                                <h5 class="panel-title">Edit Promotions</h5>
                            </div> 
                        </div>
                    </div>

                    <div class="panel-body">
                        @if(count($errors) > 0)
                        @foreach($errors->all() as $key => $error)
                        @if($key == 0)
                        <div class="alert bg-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            @if(Illuminate\Support\Str::contains($error,'required'))
                                <span class="text-semibold">Required field(s) error or missing</span>
                            @else
                                <span class="text-semibold">{{ str_replace('en.','',$error) }}</span>
                            @endif
                        </div>
                        @endif
                        @endforeach
                        @endif
                        <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
                           <span class="text-semibold">Required field(s) error or missing</span>
                        </div>

                        <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
                            <span class="text-semibold">Wrong format found</span>
                        </div>


                        <fieldset>
                        
                            <div class="form-group">
                                <label class="control-label col-lg-2">Title</label>
                                <div class="col-lg-10 clearfix">
                                    <input id="title" name="title" type="text" class="form-control" value="{{ old('title',$promo->title) }}">
                                </div>
                            </div>
                            <div class="form-group @if(count($errors->get('rules'))) invalid-input @endif">
                                <label class="control-label col-lg-2">Terms & Conditions</label>
                                <div class="col-lg-10">
                                   <textarea style="display: none" id="rules" name="rules" rows="10"  class="text-editor" cols="80" >{{ old('rules',$promo->rules) }}</textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-lg-2">Coupon Code</label>
                                <div class="col-lg-2">
                                     <input style="height: 36px;" id="coupon_code" name="coupon_code" type="text" class="form-control" value="{{ old('coupon_code',$promo->coupon_code) }}">   
                                </div>
                                <div class="col-lg-1">
                                    <a class="btn btn-default" id='generate-code' type='button'>Generate</a>
                                    <script>
                                        $(document).ready(function() {
                                            $(document).on('click',"#generate-code",function(e){
                                                var text = "";
                                                var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                                                var numeric = "0123456789";
                                                for( var i=0; i < 3; i++ )
                                                    text += uppercase.charAt(Math.floor(Math.random() * uppercase.length));
                                                for( var i=0; i < 3; i++ )
                                                    text += numeric.charAt(Math.floor(Math.random() * numeric.length));
                                                $('input[name=coupon_code]').val(text);
                                            });
                                        });
                                        
                                    </script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Coupon Type</label>
                                <div class="col-lg-3">
                                    <select class="form-control" name="coupon_type" value='{{$promo->coupon_type}}'>
                                        <?php 
                                            $types = [
                                                'common' => 'Unlimited',
                                                'one-time' => 'One Time Use'
                                            ];
                                         ?>
                                         <option disabled="" selected="" value="">Select Coupon Type</option>
                                        @foreach($types as $key => $value)
                                        <option {{ old('coupon_type',$promo->coupon_type) == $key ? 'selected' : '' }} value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Discount Type</label>
                                <div class="col-lg-3">
                                    <select id="algorithm" class="form-control" name='algorithm'>
                                        <option disabled="" selected="" value="">Select Coupon Type</option>
                                        @foreach(config('catalog.promo-algoritm') as $key => $value)
                                        <option {{ old('algorithm',$promo->algorithm) == $key ? 'selected' : ''  }} value="{{ $key }}">{{ $value->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Min. Order Required ({{app('AlphaSetting')->getSetting('currency_format')}})</label>
                                <div class="col-lg-3">
                                    <input value="{{ old('min_order',str_replace(',','',number_format($promo->min_order))) }}" name="min_order" id="min_order" type="text" class="money form-control">   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2" id="rule"></label>
                                <div class="col-lg-3">
                                    <input name="rule_detail" type="hidden" class="form-control" value="{{ old('rule_detail',str_replace(',','',number_format((int)$promo->rule_detail))) }}">   
                                    <input value="{{ old('percent',str_replace(',','',number_format((int)$promo->rule_detail))) }}" name="percent" type="text" class="percent helper-input form-control" id="percent" style="display: none">
                                    <input value="{{ old('amount',str_replace(',','',number_format((int)$promo->rule_detail))) }}" name="amount" type="text" class="money helper-input form-control" id="amount" style="display: none">
                                    <p class="help-block" id="helpLabel"></p>
                                </div>

                                    
                                    <script type="text/javascript">
                                    $("#algorithm").change(function(e){
                                        $(".helper-input").hide();
                                        $(".helper-input").val($("[name=rule_detail]").val());
                                        if(e.target.value == 'percentage_discount')
                                        {
                                             $("#rule").html('Percent ' + '(%)');
                                             $("#helpLabel").html("");
                                             $("#helpLabel").hide();
                                             $("#percent").trigger('change');
                                             $("#percent").show();
                                        }

                                        if(e.target.value == 'fixed_amount' || e.target.value == 'free_shipping')
                                        {
                                            $("#rule").html('Discount Amount ' + '({{app('AlphaSetting')->getSetting('currency_format')}})');
                                            $("#amount").trigger('change');
                                            $("#amount").show();

                                            if(e.target.value == 'fixed_amount')
                                            {                                            
                                                $("#helpLabel").html("Please set discount amount in {{app('AlphaSetting')->getSetting('currency_format')}}");
                                            }
                                            if(e.target.value == 'free_shipping')
                                            {                                            
                                                $("#helpLabel").html("Please set amount limit in {{app('AlphaSetting')->getSetting('currency_format')}}. Or, set 0 for no limit.");
                                            }
                                            $("#helpLabel").show();
                                        }

                                        if(e.target.value == '' || e.target.value == null)
                                        {
                                            $("#helpLabel").html("");
                                            $("#helpLabel").hide();
                                            $("#rule").parent().hide();
                                        }else{
                                             $("#rule").parent().show();
                                        }   
                                    });

                                    $(".helper-input").change(function(e){
                                        $("[name=rule_detail]").val(e.target.value);
                                    });

                                    $("#algorithm").trigger('change');
                                </script>


                            </div>
                            <div class="form-group">
                                <input type="hidden" name="user_id" value="{{ old('user_id',$promo->user_id) }}" id="input_user_id">
                                <label class="control-label col-lg-2">Customer</label>
                                <div class="col-lg-3">
                                     <input type="text" class="form-control" id="customer">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Valid Date</label>
                                <div class="col-lg-3" id="rangepicker">
                                     <?php 
                                        $format = app('AlphaSetting')->getSetting('date_format');
                                        $defaultDate = date($format,strtotime($promo->start)).' - '.date($format,strtotime($promo->end));
                                     ?>

                                    <?php $format = app('AlphaSetting')->getSetting('date_format'); ?>
                                    <input value="{{ old('rangepicker',$defaultDate) }}" name="rangepicker" type="text" class="form-control" style="height:36px;">
                                    <div class="sale-date-trigger" style="background:#ccc;height:36px;line-height:36px;width: 50px;">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <input type="hidden" name="start" id="start">
                                    <input type="hidden" name="end" id="end">
                                </div>
                             </div>

                          
                        </fieldset>
                        
                    </div>

                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="panel right-small">
                    <div class="panel-heading">
                        <div class="panel-heading-title">
                            <h5 class="panel-title">Status</h1>
                        </div>
                    </div>
                    <div class="panel-body">
                    <?php $status = ['active','disabled'] ?>
                        
                        <fieldset>
                        <div class="form-group no-margin-left no-margin-right">
                            <select class="select-two form-control @if(count($errors->get('status'))) invalid-input @endif" name="status" id="publish_option">
                               @foreach($status as $s)
                                    <option <?php echo ($s == $promo->status) ? 'selected' : '' ?> value="{{ $s }}"><?php $s = str_replace('_publish','',$s) ?>{{  Illuminate\Support\Str::title($s) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group no-margin-bottom no-margin-left no-margin-right">
                            <button type="submit" class="btn btn-primary pull-right ml-10">Save</button>
                            <a href="{{route('catalog_promo_index')}}" class="btn bg-slate pull-right">Cancel</a>
                        </div>
                        </fieldset>
                    </div>
                
                
                </div>
            </div>
        </div>
        <input type='hidden' name="promo_id" value="{{$promo->id}}">
    </form>
    <!-- Footer -->
   {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
 <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace("rules",{height: '400px'});
    $(document).ready(function(){
        initPreventClose();
    })
</script>

<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/autoNumeric-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('morra/catalog/assets/js/jquery.tokeninput.js') }}"></script>

<script type="text/javascript">
    var urlGetCustomer = "<?php echo route("catalog_pos_user") ?>";
</script>
<script type="text/javascript">
    var selectedUser = new Array();
    var isPrePopulate = false;
    var user = {};
    <?php  
        $user = User::find(old('user_id',$promo->user_id));
        if(!empty($user)){
     ?>
        user = <?php echo json_encode($user) ?>;
        selectedUser.push(user);
        isPrePopulate = true;
    <?php } ?>

    $("#customer").tokenInput(urlGetCustomer, {
        hintText: "Start typing the customer name",
       // placeholder: "Type Brand name",
        noResultsText: "Not found? New customer names need to be registered first.",
        searchingText: "Searching ...",
        minChars: 1,
        deleteText:'<i class="icon-cross" style="font-size:15px;"></i>',
        tokenLimit: 1,
        prePopulate:selectedUser,
        processPrePopulate:isPrePopulate,
        preventDuplicates: true,
        propertyToSearch: "email",
        resultsFormatter: function(item){ 
            return "<li>" + "<div style='display: inline-block;'><div class='full_name'>" +" " + item.email + "</div><div class='email'> </div></div></li>";
        },  
        onAdd: function (item) {
            $("#input_user_id").val(item.id);
        },
        onDelete: function (item) {
            $("#input_user_id").val(0);
        },
        onResult: function (results) {
            return results;
        }
     });

       <?php 
        $rangeDate = old('rangepicker',$defaultDate);
        $rangeDate = explode(' - ', $rangeDate);
     ?>

     $("#rangepicker").daterangepicker({
            "opens": "left",
            "drops": "up",
            autoApply:true,
            locale: {
              format: 'DD MMM YYYY'
            },
            startDate:"{{ $rangeDate[0] }}",
            endDate:"{{ $rangeDate[1] }}"
        });

     $("#rangepicker").on('apply.daterangepicker', function(ev, picker) {
            $('#start').val(picker.startDate.format('YYYY-MM-DD'));
            $('#end').val(picker.endDate.format('YYYY-MM-DD'));
             $("[name=rangepicker]").val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'))
    });
    var drp = $('#rangepicker').data('daterangepicker');
    $("#rangepicker").trigger('apply.daterangepicker',[drp,drp]);

    $('.percent').autoNumeric('init',{aSign:'', aSep: '', aDec: ' ', mDec: 0,wEmpty: 'zero',vMax:100 });
    $('.money').autoNumeric('init',{aSign:'', aSep: ',', aDec: '.', mDec: 0,wEmpty: 'zero' });



</script>


<?php
    $err = count($errors);
    ?>
   

   {!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'coupon_code' => 'required',
                    'title' => 'required',
                    'coupon_type' => 'required',
                    'algorithm' => 'required',
                    'min_order' => 'required',
                    'rule_detail' => 'required',
                ]

        ]); 
   !!}


