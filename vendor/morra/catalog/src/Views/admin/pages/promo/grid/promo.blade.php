
<td class="title-column">
    <h6>
            <a href="{{ route('catalog_promo_edit',$promo->id) }}">
            {{$promo->title}}
            @if(empty($promo->title))
                {{ $promo->coupon_code }}
            @endif
            </a>
    </h6>
        <?php 
            $amount = app('AlphaSetting')->getSetting('currency_format').' '.number_format($promo->rule_detail).' discount';
            if($promo->algorithm == 'percentage_discount')
            {
                $amount = $promo->rule_detail.'% Discount';
            }
        ?>
            <span class="text-muted">
                Coupon code:  {{ $promo->coupon_code }} {{ $amount }} {{app('AlphaSetting')->getSetting('currency_format')}} {{number_format($promo->min_order)}} min. order required.
            </span>

</td>

|alpha--datatable-separator--|
<td class="td-date">
    {{date_format(date_create($promo->start),app('AlphaSetting')->getSetting('date_format'))}} -  {{date_format(date_create($promo->end),app('AlphaSetting')->getSetting('date_format'))}}
    <br><span class="text-muted">{{date_format(date_create($promo->start),app('AlphaSetting')->getSetting('time_format'))}} - {{date_format(date_create($promo->end),app('AlphaSetting')->getSetting('time_format'))}}</span>
</td>


|alpha--datatable-separator--|
<td class="td-date">
    {{date_format(date_create($promo->created_at),app('AlphaSetting')->getSetting('date_format'))}}
    <br><span class="text-muted">{{date_format(date_create($promo->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
</td>

|alpha--datatable-separator--|
<td class="td-date">
    {{date_format(date_create($promo->end),app('AlphaSetting')->getSetting('date_format'))}}
    <br><span class="text-muted">{{date_format(date_create($promo->end),app('AlphaSetting')->getSetting('time_format'))}}</span>
</td>

@if($promo->status == 'active')
|alpha--datatable-separator--|<td class=><span class="label bg-success">Active</span></td>
@elseif($promo->status == 'disabled')
|alpha--datatable-separator--|<td class="hide"><span class="label bg-danger">Disabled</span></td>
@else
|alpha--datatable-separator--|<td class="hide"><span class="label bg-danger">Disabled</span></td>
@endif


|alpha--datatable-separator--|<td class="text-right table-actions">
    <div class="btn-group">
        <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
        </button>

        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="{{ route('catalog_promo_edit',$promo->id) }}"><i class="icon-pencil5"></i> Edit</a></li>
            @if($promo->status == 'active')
            <li><a href="{{ route('catalog_promo_status',[$promo->id,'active']) }}"><i class="icon-cancel-square"></i> Disable</a></li>
            @endif
            @if($promo->status == 'disabled')
            <li><a href="{{ route('catalog_promo_status',[$promo->id,'active']) }}"><i class="icon-checkmark4"></i> Publish</a></li>
            @endif
            <li class="divider"></li>
            <li><a href="javascript:void(0)" class="confirm_delete" data-id="{{$promo->id}}"><i class="icon-trash"></i> Delete</a></li>
        </ul>
    </div>
</td>