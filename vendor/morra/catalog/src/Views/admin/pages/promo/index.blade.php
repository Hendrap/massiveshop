<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Promotions</span> - All Promotions</h4>
        </div>
        <div class="heading-elements">
            <a href="{{route('catalog_promo_create')}}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>New Promo</a>
        </div>
    </div>
</div>

<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="panel panel-flat">

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
           
                <li><a href="#">Promotions</a></li>
                <li class="active">Promo Codes</li>

            </ul>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-flat">

                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h1 class="text-semibold">Promotions</h1>
                    </div>

                   
                </div>
                <div class="panel-body">
                    <p>Manage available promotions</p>
                </div> 
                    <div class="table-header datatable-header clearfix">
                    
                        <div class="filter-table clearfix pull-left mr-5">
                            <div class="form-group pull-left  mr-5 no-margin-bottom">
                                <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter:&nbsp;</label>
                            </div>
                            <div class="form-group pull-left has-feedback  no-margin-bottom">
                                <input type="text" class="form-control" id="search_the_table" style="height:36px">
                                <div class="form-control-feedback" style="line-height:36px;">
                                        <i class="icon-search4" style="font-size:12px"></i>
                                </div>
                            </div>
                            
                        </div>
 
                    </div>
                    
                    
                    <table class="table entry-table table-striped">
                        <thead>
                            <tr>
                                <th width="350px">Promotion</th>
                                <th class="">Valid Date</th>
                                <th class="">Created</th>
                                <th class="">Modified</th>
                                <th>Status</th>
                                <th class="no-sort text-right">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                       
                        </tbody>
                    </table>
                    
                    <form style="display:none" id="form_delete" action="{{route('catalog_promo_delete',[0])}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="0" >
                    </form>

            </div>

        </div>

    </div>



    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->

<script type="text/javascript">
    var entryGetDataUrl = "{{ route('catalog_promo_get_data') }}";

    $(document).on('click',".confirm_delete",function(e){
        e.preventDefault();
        $("#form_delete").find("[name=id]").val($(this).data('id'));
        swal({
        title: "Delete ?",
        text: "You will not be able to recover deleted items, are you sure want to delete ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false },
        function(isConfirm){
            if(isConfirm){
                $("#form_delete").submit();
            }
        });
    });

</script>
<script type="text/javascript">
    var status = "{{session('msg')}}";
    if (status === 'Status Changed!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success",
                closeOnConfirm: false,
                },function(isConfirm){
                    swal.disableButtons();
                    swal.close();
                });
    }
</script>

<script>                      
    var entry_table = {};
    function toggleSorting(status){
        var cols = entry_table.settings()[0]['aoColumns'];
        for (var i = cols.length - 1; i >= 0; i--) {
            cols[i].bSortable = status;
        }
        $("#search_the_table").prop('disabled',!status);
        $("select").prop('disabled',!status);
    }

    function showEntryTableLoader(block){
        $(block).block({
                         message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                           overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#333',
                                width: 'auto',
                            }
                        });
    }
    $(document).ready(function() {
         $('.set-order').click(function(e) {
            e.preventDefault();
            $(this).addClass('hide');
            $('.set-order-done').removeClass('hide');
            var current = entry_table.page.info();
            current = current.page;
            entry_table.page(current + 1).order([0,'asc']).columns(0).search('').draw();
        });

        $('.set-order-done').click(function(e) {
            e.preventDefault();
            var that = $(this);
            var items = $(".entry-data-container");
            var data = [];
            var obj = {};
            var pageLength = entry_table.page.info().length;
            var currentPage = entry_table.page.info().page + 1;

            for (var i = 0; i < items.length; i++) {
                obj = {};
                obj.entry_id = $(items[i]).attr('data-entry-id');
                obj.sequence = (i + 1 + (entry_table.page.info().page * pageLength));
                obj.before =  $(items[i]).attr('data-before');
                obj.after =  $(items[i]).attr('data-after');
                data.push(obj);
            }

            showEntryTableLoader($('.entry-table tbody'));
            $.ajax({
                url:urlSaveSequence,
                data:{type:entryType,data:data,pageLength:pageLength,currentPage:currentPage},
                type:"POST",
                dataType:"json",
                success:function(data)
                {
                    $.unblockUI();
                    if(data.status == 1){
                        $(that).addClass('hide');
                        $('.set-order').removeClass('hide');
                        entry_table.page(currentPage + 1).order([1,'asc']).columns(1).search('').draw();
                        toggleSorting(true);
                    }
                }
            });
        });



       entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                var block = $('.entry-table');
                 if(processing){
                    $('.dataTable, .dataTable tbody').css('position','static');
                    if(typeof entry_table.ajax != 'undefined'){
                        $(e.target).find('tbody').html('');
                    }
                    showEntryTableLoader(block)
                 }else{
                    $.unblockUI();
                    $(".blockUI").remove();
                    $('.dataTable, .dataTable tbody').css('position','relative');
                 }
                 $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
        }).DataTable({
            "rowReorder": true,
            rowReorder: {
                update: false
            },
            "order": [],
            "pageLength": 50,
            "searching" : true,
            "lengthChange": false,
            "oLanguage": {
                "sProcessing":' ' 
            },
            "dom": 'rt<"datatable-footer"ilp><"clear">',
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": entryGetDataUrl,
                "type": "POST"
            },
            "columns": [

                { "data": "", "render": function(data, type, full, meta){
                    var html = full.html.split('|alpha--datatable-separator--|');
                    return html[0];
                },
                sClass:""
                },
                
                { "data": "", "render": function(data, type, full, meta){
                    var html = full.html.split('|alpha--datatable-separator--|');
                    return html[1];
                },
                sClass:"td-date"
                },
                
                { "data": "", "render": function(data, type, full, meta){
                    var html = full.html.split('|alpha--datatable-separator--|');
                    return html[2];
                },
                sClass:"td-date"
                },
                
                { "data": "", "render": function(data, type, full, meta){
                    var html = full.html.split('|alpha--datatable-separator--|');
                    return html[3];
                },
                sClass:"td-date"
                },
                { "data": "", "render": function(data, type, full, meta){
                    var html = full.html.split('|alpha--datatable-separator--|');
                    return html[4];
                },
                sClass:""
                },
                { "data": "action", "render": function(data, type, full, meta){
                    var html = full.html.split('|alpha--datatable-separator--|');
                    return html[5];
                },
                sClass:"text-right table-actions"

                },

            ],
            "columnDefs": [
                {
                    "targets": 'no-sort',
                    "orderable": false,
                }
            ]
        });


        entry_table.on( 'row-reorder', function ( e, diff, edit ) {

        });

        var filterby;
        var searchin;

        $('.filter-table-column').change(function(){
            thisfilter = $(this);
            thisfilter.find('option:selected').each(function(){
                filterby = $(this).val();
                searchin = thisfilter.attr('searchin');
            });
            if(filterby == 'all'){
                entry_table.columns( searchin ).search('').draw();
            }
            else{
                entry_table.columns( searchin ).search(filterby).draw();
            }
        });

        $('select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
        $('.table-select2').each(function() {
            select_placeholder = $(this).attr('placeholder');
            $(this).select2({
                minimumResultsForSearch: Infinity,
                placeholder: select_placeholder,
                width: '200px',
            });
        });
        $('.table-select2.with-search').each(function() {
            select_placeholder = $(this).attr('placeholder');
            $(this).select2({
                minimumResultsForSearch: 1,
                placeholder: select_placeholder,
                width: '200px',
            });
        });
        $('#search_the_table').keyup(function(e){
            if(e.which == 13){
                entry_table.search($(this).val()).draw();
            }
        });
    });
</script>




