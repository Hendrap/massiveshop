<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title">Setting Catalog</h5>
					</div>
	<div class="panel-body">
		@if(session('success'))
           			<div class="alert alert-success">
           				{{session('success')}}
           			</div>
         		   @endif
		<form class="form-horizontal" method="POST" action="">
			{{ csrf_field() }}
			<fieldset class="content-group">
				<?php foreach ($settings as $key => $value) { ?>
				<div class="form-group">
					<label class="control-label col-lg-2">{{ucfirst(str_replace('_',' ',$key))}}</label>
					<div class="col-lg-10">
						 <input name="{{$key}}" type="text" class="form-control" id="{{$key}}" value="{{$value}}">
					</div>
				</div>
				<?php } ?>
			</fieldset>
			<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
			</div>
		</form>
	</div>					
</div>