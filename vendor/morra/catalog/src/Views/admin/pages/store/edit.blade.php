<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Stores</span> - Edit Store</h4>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="">Stores</a></li>
                <li class="active">New Store</li>
            </ul>
        </div>
    </div>

    <div style="display: none" id="errorMsgRequired" class="alert bg-danger alert-styled-left">
       <span class="text-semibold">Required field(s) error or missing</span>
    </div>

    <div style="display: none" id="errorMsgWrongFormat" class="alert bg-danger alert-styled-left">
        <span class="text-semibold">Wrong format found</span>
    </div>
    
    <form name="alphaForm" enctype="multipart/form-data" action="{{ route('catalog_store_save') }}" method="POST" class="form-horizontal">
        <input type="hidden" value="{{$store->id}}" name="id">
        <div class="row">
            <div class="col-sm-9">
                <div class="panel panel-flat">
                    <div class="panel-heading clearfix">
                        <div class="panel-heading-title">
                            <div style="width:50%">
                                <h5 class="panel-title">Main Content</h5>
                            </div> 
                        </div>
                    </div>

                    <div class="panel-body">
                        @if(count($errors) > 0)
                        @foreach($errors->all() as $key => $error)
                        @if($key == 0)
                        <div class="alert bg-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            @if(Illuminate\Support\Str::contains($error,'required'))
                                <span class="text-semibold">Required field(s) error or missing</span>
                            @else
                                <span class="text-semibold">{{ str_replace('en.','',$error) }}</span>
                            @endif
                        </div>
                        @endif
                        @endforeach
                        @endif
                        
                        <fieldset>
                        
                            <div class="form-group">
                                <label class="control-label col-lg-2">Name</label>
                                <div class="col-lg-10 clearfix">
                                    <input id="name" name="name" type="text" class="form-control" value="{{old('name',$store->name)}}">
                                </div>
                            </div>
                            <div class="form-group @if(count($errors->get('content'))) invalid-input @endif">
                                <div class="col-lg-12">
                                   <textarea style="display:none" id="content" name="content" rows="10"  class="text-editor" cols="80" >{{old('content',$store->description)}}</textarea>
                                </div>
                            </div>

                              <div class="form-group">
                                <label class="control-label col-lg-2">Sales Channels</label>
                                <div class="col-lg-2 clearfix">
                                   <select placeholder="Select Sales Channels" class="multiselect-filtering" name="sales_channels[]" id="sales_channels" multiple>
                                        @foreach($channels as $value)
                                        <?php
                                                $select = '';
                                                if(in_array($value->id, $selectedChannels)) $select = 'selected';
                                         ?>
                                        <option {{ $select }} value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                   </select>
                                </div>
                            </div>




                            <div class="form-group" id="meta_image">
                                <label class="control-label col-lg-2">Logo</label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <input value="{{ old('media_container_image',$storeData->media_container_image) }}" readonly="" name="media_container_image" id="media_containerimage" style="height: 36px" type="text" class="form-control" placeholder="Media name">
                                        <input class="media_uploader" type="text" id="media_idsimage" name="media_id" value="{{ old('media_id',$storeData->media_id) }}" style="display:none">
                                        <span class="input-group-btn">
                                            <button data-meta="image" class="btn btn-default btn-uploader-media" type="button">Open Media Library</button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Email Header</label>
                                <div class="col-lg-10 clearfix">
                                    <textarea class="form-control" name="email_header" id="email_header">{{ old('email_header',$storeData->email_header) }}</textarea>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label col-lg-2">Invoice Header</label>
                                <div class="col-lg-10 clearfix">
                                    <textarea class="form-control" name="invoice_header" id="invoice_header">{{ old('invoice_header',$storeData->invoice_header) }}</textarea>
                                </div>
                            </div>


                             <div class="form-group">
                                <label class="control-label col-lg-2">Return Address</label>
                                <div class="col-lg-10 clearfix">
                                    <textarea class="form-control" name="return_address" id="return_address">{{ old('return_address',$storeData->return_address) }}</textarea>
                                </div>
                             </div>

                             <div class="form-group">
                                <label class="control-label col-lg-2">Phone Number</label>
                                <div class="col-lg-4 clearfix">
                                    <input type="text" class="form-control" name="phone" id="phone" value="{{ old('phone',$storeData->phone) }}">
                                </div>
                             </div>


                              <div class="form-group">
                                <label class="control-label col-lg-2">Email Address</label>
                                <div class="col-lg-4 clearfix">
                                    <input type="text" class="form-control" name="email_address" id="email_address" value="{{ old('email_address',$storeData->email_address) }}">
                                </div>
                             </div>


                             <div class="form-group">
                                <div class="text-right">
                                  <a href="{{ route('catalog_store_index') }}" class="btn bg-slate">Cancel</a>
                                  <button type="submit" class="btn btn-primary ml-10">Save</button>
                                </div>
                             </div>
                             
                           

                            

                        </fieldset>
                        
                    </div>

                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
              


                  
                
            </div>
           
        </div>
    </form>
    <!-- Footer -->
   {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
<?php 
        $err = count($errors);
        $errMsg = "";
?>
<script type="text/javascript">
    $(document).ready(function(){
        var status = "{{session('msg')}}";
        var err = "{{$err}}";
        if (status === 'Data Saved!') {
            swal({
                title: "SUCCESS",
                text: "All changes has been saved successfuly",
                confirmButtonColor: "#66BB6A",
                type: "success",
                html: true
                });
        }
    })
</script>


{!! view('alpha::admin.error-script',[
                'errors'=>$errors,
                'fields' => [
                    'name' => 'required',
                    'email_address' => 'valid_email',
                    'phone' => 'numeric'
                ]

            ]); 
        !!}    <script type="text/javascript" src="{{ asset('backend/assets/js/ckeditor/ckeditor.js') }}"></script>
    <script>
    $(document).on('click','.delete-img-entry-media',function(e){
        e.preventDefault();
        var that = $(this)
        swal({
            title: "Delete ?",
            text: "You will not be able to recover deleted items, are you sure want to delete ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true },
            function(isConfirm){
                if(isConfirm){
                    $(that).parent().parent().parent().remove();
                }
            });
    });

    $( "#containerMedia" ).sortable({
      revert: true
    });
    



 
    CKEDITOR.replace("content",{height: '400px'});
   





    $('.additional-info-panel .date-picker').addClass('datepicker-top').removeClass('date-picker');
        
    $('.datepicker-top').daterangepicker({
                    timePicker: true,
                    opens: "left",
                    applyClass: 'bg-slate-600',
                    cancelClass: 'btn-default',
                    singleDatePicker: true,
                    autoApply : true,
                    drops: "up",
                    locale: {
                        format: 'MM/DD/YYYY h:mm a'
                    }
    });

        $(document).ready(function() {
            templateAddToEntry = '<?php echo newLine(view('alpha::admin.media.templates.img-entry-media',['img'=>'','id'=>0])) ?>';

                        initPreventClose();

           $('.select-language').each(function() {

                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    width: '200px',
                                });

            });

             $('.entry-parent').each(function() {
                select_placeholder = $(this).attr('placeholder');
                $(this).select2({
                     placeholder: select_placeholder,
                });
             });

            
            autosize($('textarea'));
            if($("#publish_option").val() == 'scheduled_publish')
            {
                $(".publish-date").show();
            }
            
        });

        var elem = $('.add-edit-product-photos-content');
        var eleminner = $('.product-photo-container');
        var eleminnertwo = $('.product-photo-container-add');
            
            function checkimagescroll(){
               
                totalinnerwidth = eleminner.innerWidth() + eleminnertwo.innerWidth() - 12;
                if(elem.scrollLeft() + elem.width() ==  totalinnerwidth){
                    $('.product-photo-navigation.next').addClass('hide');
                }
                if(elem.scrollLeft() + elem.width() <  totalinnerwidth){
                    $('.product-photo-navigation.next').removeClass('hide');
                }
                if(elem.scrollLeft()  ==  0){
                    $('.product-photo-navigation.prev').addClass('hide');
                }
                if(elem.scrollLeft()  >  0){
                    $('.product-photo-navigation.prev').removeClass('hide');
                }
            }
            checkimagescroll();
            
            
            $('.product-photo-navigation.next a').click(function(e){
                e.preventDefault();
                elem.get(0).scrollLeft += 250;
                checkimagescroll();
            });
            $('.product-photo-navigation.prev a').click(function(e){
                e.preventDefault();
                elem.get(0).scrollLeft -= 250;
                checkimagescroll();
            });

    </script>
    <style>
        
        .bootstrap-tagsinput input {
            
            width: 100%!important;
        }
        
    </style>
    <script type="text/javascript">
        $(".btn-uploader-metas").click(function(e){
            e.preventDefault();
            if($(this).text().trim() == "Upload File")
            {
                $(this).parent().parent().parent().find('[type=file]').click();
                $(this).text("Remove");
            }else{
                $(this).parent().parent().parent().find('[type=file]').val("");
                $(this).parent().parent().parent().find('[type=text]').val("");
                $(this).text("Upload File");
            }
            
        });
        $(".meta_uploader").change(function(e){
            $("#" +  $(this).data('target')).val($(this).get(0).files[0].name);
        });

        $(".btn-uploader-media").click(function(e){
            e.preventDefault();
            if($(this).text().trim() == "Open Media Library")
            {
                var meta = $(this).data('meta');
                loadMedias("<?php echo route('alpha_admin_media_entry')."?page=1" ?>",function(){
                    $("#modalEntryMedia").modal('show');
                    editorName = 'metas.' + meta;
                });
            }else{
                $(this).parent().parent().find("input").val("");
                $(this).parent().parent().find("button").html("Open Media Library")
            }

        });
    </script>

