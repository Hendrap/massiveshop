                            <tr>
                                <td class="title-column">
                                        <h6>
                                            <a href="{{ route('catalog_store_edit',[$store->id]) }}">                                         
                                                {{($store->name)}}
                                            </a>
                                        </h6>
                                    </td>
                                    <td class="alpha-desc">
                                        {{ strip_tags($store->description) }}
                                    </td>
                                    <td class="td-date">
                                        {{date_format(date_create($store->created_at),app('AlphaSetting')->getSetting('date_format'))}}
                                        <br><span class="text-muted">{{date_format(date_create($store->created_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                    </td>
                                    <td class="td-date">
                                        {{date_format(date_create($store->updated_at),app('AlphaSetting')->getSetting('date_format'))}}
                                        <br><span class="text-muted">{{date_format(date_create($store->updated_at),app('AlphaSetting')->getSetting('time_format'))}}</span>
                                    </td>
                                    <td class="text-right table-actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-icon dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                                            </button>
                                            <?php if(empty($store->data)) $store->data = '[]'; ?>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="{{ route('catalog_store_edit',[$store->id]) }}"><i class="icon-pencil5"></i> Edit</a></li>
                                                <li class="divider"></li>
                                                <li><a href="javascript:voi(0)" class="confirm" data-id="{{$store->id}}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                            </tr>