
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Catalog Setting</span> - Stores</h4>
        </div>
        <div class="heading-elements">
            <a href="{{ route('catalog_store_create') }}" class="btn btn-labeled bg-brand heading-btn"><b><i class="icon-file-plus"></i></b>New Store</a>
        </div>
    </div>
</div>


<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h1 class="text-semibold">Stores</h1>
                    </div>
                </div>
                <div class="panel-body">
                    <p>All registered store available.</p>
                </div>
                <div class="table-header clearfix mb-10">
                    <div class="filter-table clearfix pull-left mr-5">
                        <div class="form-group pull-left  mr-5 no-margin-bottom">
                            <label style="height: 36px; line-height: 36px;margin-bottom: 0px;">Filter</label>
                        </div>
                        <div class="form-group pull-left has-feedback  no-margin-bottom">
                            <input type="text" class="form-control" id="search_the_table" style="height:36px">
                            <div class="form-control-feedback" style="line-height:36px;">
                                <i class="icon-search4" style="font-size:12px"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <table id="tableData" class="table entry-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th width="140px"><span>Created</span></th>
                            <th width="140px"><span>Modified</span></th>
                            <th width="50px" class="no-sort text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($stores as $store)
                                {!! 
                                        view('catalog::admin.pages.store.grid',[
                                            'store' => $store
                                        ]) 
                                !!}
                            @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </div>

    <div id="modal_data" class="modal fade" style="display: none;">
        <div class="modal-dialog" style="width:550px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h5 class="modal-title text-bold">Edit Store</h5>
                </div>

                <div class="modal-body">
                    <div style="display:none" id="modalErrMsg" class="alert bg-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            <span class="text-semibold">Required field(s) error or missing</span>
                    </div>
                    <form>
                        <input type="hidden" name="" id="data_id">
                        <div class="form-group">
                            <label>Name</label>
                            <input id="data_name" type="text" class="form-control" placeholder="Name" value="Name">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea id="data_desc" class="form-control" placeholder="Description">Description</textarea>
                        </div>

                        <div class="form-group">
                            <label>Sales Channels</label>
                            <br>
                            <select class="form-control" id="data_sales_channels" multiple="">
                                @foreach($channels as $channel)
                                    <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                                @endforeach
                            </select>
                        </div>


                    </form>
                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn bg-slate" data-dismiss="modal">Cancel</button>
                    <button id="saveData" type="button" class="btn btn-primary select-image">Save</button>
                </div>
            </div>
        </div>
    </div>
    <form style="display:none" id="form_delete" action="{{route('catalog_store_delete')}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="0" >
                    </form>
     <script>
                    var entry_table = {};
                    $(document).ready(function() {
                        
                        $('#add_new_data').click(function(e){
                            e.preventDefault();
                            $('#modal_add_data').modal();
                        });

                        $('.edit_data').click(function(e){
                            e.preventDefault();
                            $('#modal_edit_data').modal();
                        });
                        
                    });

                    $(document).ready(function() {

                           var childClass = 'title-column';
                            entry_table = $('.entry-table').on( 'processing.dt', function ( e, settings, processing ) {
                                    var block = $('.entry-table');
                                     if(processing){
                                             $('.dataTable, .dataTable tbody').css('position','static');
                                             $(block).block({
                                                message: '<span class="text-semibold"><i class="icon-spinner10 spinner position-left"></i>&nbsp;Loading Data</span>',
                                                overlayCSS: {
                                                    backgroundColor: '#fff',
                                                    opacity: 0.8,
                                                    cursor: 'wait'
                                                },
                                                css: {
                                                    border: 0,
                                                    padding: '10px 15px',
                                                    color: '#333',
                                                    width: 'auto',
                                                   
                                                }
                                            });
                                     }else{
                                         $('.dataTable, .dataTable tbody').css('position','relative');
                                        $.unblockUI();
                                        $(".blockUI").remove();
                                     }
                                     $( document ).trigger( "alpha--datatable-processing",[processing,$('.entry-table'),block,e]);
                            }).DataTable({
                                "order": [],
                                "pageLength": 50,
                                "searching" : true,
                                "lengthChange": false,
                                "oLanguage": {
                                    "sProcessing":' ' 
                                },
                                "dom": 'rt<"datatable-footer"ilp><"clear">',
                                "columnDefs": [
                                    {
                                        "targets": 'no-sort',
                                        "orderable": false,
                                    }
                                ]
                            });
                           
                            var filterby;
                            var searchin;
                            
                            $('.filter-table-column').change(function(){
                                thisfilter = $(this);
                                thisfilter.find('option:selected').each(function(){
                                    filterby = $(this).val();
                                    searchin = thisfilter.attr('searchin');
                                });
                                if(filterby == 'all'){
                                    entry_table.columns( searchin ).search('').draw();
                                }
                                else{
                                    entry_table.columns( searchin ).search(filterby).draw();
                                }
                            });
                            
                            $('#data_sales_channels').select2({
                                placeholder:"Select Sales Channels",
                                width: '200px'
                            });
                            $('.table-select2').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: Infinity,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('.table-select2.with-search').each(function() {
                                select_placeholder = $(this).attr('placeholder');
                                $(this).select2({
                                    minimumResultsForSearch: 1,
                                    placeholder: select_placeholder,
                                    width: '200px',
                                });
                            });
                            $('#search_the_table').keyup(function(e){
                                if(e.which == 13){
                                    entry_table.columns(0).search($(this).val()).draw();
                                }
                            });
                        });
                </script>
                <script type="text/javascript">

                    var globalData = {};
                    function initNameToolTip()
                    {
                        $("#data_name").tooltip({
                                    animation:true,
                                    placement:'bottom',
                                    title:"The name field is required.",
                                    trigger:"focus",
                                    template:'<div class="tooltip red-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                                });
                        $("#data_name").parent().addClass('has-error');
                    }


                    function initDescToolTip()
                    {
                        $("#data_desc").tooltip({
                                    animation:true,
                                    placement:'bottom',
                                    title:"The description field is required.",
                                    trigger:"focus",
                                    template:'<div class="tooltip red-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                                });
                        $("#data_desc").parent().addClass('has-error');    
                    }

                    function initPaymentsToolTip()
                    {
                        $(".select2-selection.select2-selection--multiple").css('border-color','red');
                        $("#data_sales_channels").tooltip({
                                    animation:true,
                                    placement:'bottom',
                                    title:"The payment methods field is required.",
                                    trigger:"focus",
                                    template:'<div class="tooltip red-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                                });
                        $("#data_sales_channels").parent().addClass('has-error');    
                    }
                    


                    
                    function destroyInit()
                    {
                         $("#data_name").tooltip("destroy");
                         $("#data_desc").tooltip("destroy");
                         $("#data_sales_channels").tooltip("destroy");
                         $("#data_name").parent().removeClass('has-error');
                         $("#data_desc").parent().removeClass('has-error');
                         $("#data_sales_channels").parent().removeClass('has-error');
                         $("#modalErrMsg").hide();    
                         $(".select2-selection.select2-selection--multiple").css('border-color',"rgb(221, 221, 221)");

                    }

                    initNameToolTip();
                    initDescToolTip();

        
                    var urlSaveData = "<?php echo route('catalog_store_save'); ?>";
                    var opener = {};

                    $("#addData").click(function(e){
                        e.preventDefault();
                        destroyInit();
                        $("#data_name").val("");
                        $("#data_desc").val("");
                        $("#data_id").val(0);
                        $("#data_sales_channels").val("").change();
                        $("#modal_data").find(".modal-title").html('Create Store');
                        $("#modal_data").modal('show');
                    });

                    $(document).on('click','.trigger_edit',function(e){
                        e.preventDefault();
                        $(this).parents('tr').find('.edit-data').click();
                    });

                    $(document).on('click','.edit-data',function(e){
                        e.preventDefault();
                        destroyInit();
                        opener = $(this).parents('tr')
                        $("#data_name").val($(this).attr('data-name'));
                        $("#data_desc").val($(this).attr('data-desc'));
                        $("#data_id").val($(this).attr('data-id'));
                        $("#data_sales_channels").val(JSON.parse($(this).attr('data-channels'))).change();
                        $("#modal_data").find(".modal-title").html('Edit Store');
                        $("#modal_data").modal('show');
                    });

                    $("#saveData").click(function(e){
                        e.preventDefault();
                        destroyInit();

                        if($("#data_name").val().trim() == "" || $("#data_desc").val().trim() == "" || $("#data_sales_channels").val() == null)
                        {
                            $("#modalErrMsg").show();

                            if($("#data_name").val().trim() == ""){
                                 initNameToolTip();
                            }

                            if($("#data_desc").val().trim() == ""){
                                 initDescToolTip();
                            }

                            if($("#data_sales_channels").val() == null){
                                initPaymentsToolTip();
                            }
                            return false;
                        }
                        $.ajax({
                            url:urlSaveData,
                            type:"POST",
                            dataType:"json",
                            data:{id:$("#data_id").val(),name:$("#data_name").val(),description:$("#data_desc").val(),sales_channels:$("#data_sales_channels").val()},
                            success:function(data)
                            {
                                if(data.status == 1)
                                {
                                    $("#modal_data").modal('hide');
                                    swal({
                                        title: "SUCCESS",
                                        text: "All changes has been saved successfuly",
                                        confirmButtonColor: "#66BB6A",
                                        type: "success",
                                        html: true
                                    });

                                    if(parseInt($("#data_id").val()) == 0)
                                    {
                                        entry_table.row.add($(data.html)).draw();
                                    }else{
                                        $(opener).find('.edit-data').attr('data-name',$("#data_name").val());
                                        $(opener).find('.edit-data').attr('data-desc',$("#data_desc").val());
                                        $(opener).find('.edit-data').attr('data-channels',data.store.data);
                                        $(opener).find('.title-column').find('a').html($("#data_name").val());
                                        $(opener).find('.alpha-desc').html($("#data_desc").val());
                                        entry_table.rows().every( function () {
                                                var d = this.data();
                                                d.counter++; 
                                                this.invalidate(); 
                                            } );
                                             
                                            entry_table.draw();
                                    }
                                }else{
                                    $("#modalErrMsg").show();
                                }
                            }
                        });
                    });
                </script>
                <script type="text/javascript">                        
                        $(document).on('click',".confirm",function(e){
                                e.preventDefault();
                              //  $("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).attr('data-id')));
                                $("#form_delete").find("[name=id]").val($(this).data('id'));
                                 swal({
                                title: "Delete ?",
                                text: "You will not be able to recover deleted items, are you sure want to delete ?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                closeOnConfirm: false },
                                function(isConfirm){
                                    if(isConfirm){
                                        $("#form_delete").submit();
                                    }
                                });
                            });

                    </script>
    <!-- Footer -->
    {!! view('alpha::admin.partials.content-footer') !!}
    <!-- /footer -->

</div>
<!-- /content area -->
