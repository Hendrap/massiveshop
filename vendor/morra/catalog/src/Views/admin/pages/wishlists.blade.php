<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">Catalog Wishlists</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="panel-heading">
						<hr>
						<div class="row">
							<div class="col-md-4">
								<b>Catalog Wishlists</b> 
							</div>
							<div class="col-md-2"></div>
						</div>
						<hr>
					</div>
				@if(session('msg'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-success">{{session('msg')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				@if(session('err'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-danger">{{session('err')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Item</th>
						<th>User</th>
						<th>Status</th>
						<th>Created at</th>
						<th>Update at</th>
						<th></th>
					</tr>

					@foreach($data as $item)
					<tr>
						<?php 
							$title = [];
							$title[] = parseMultiLang(@$item->item->entry->title);
							$title[] = $item->item->item_title;
							$cleanTitle = implode(' - ', $title);
							$route = 'catalog_item_variant_edit';
							if($item->item->type == 'group'){
								$route = 'catalog_item_group_edit';
							}
							$link = route($route,[@$item->item->entry->id,@$item->item->id]);
						 ?>
						<td><a target="_blank" href="<?php echo $link ?>">{{ $cleanTitle }}</a></td>
						<td><a href="{{ route('admin.user.edit',[$item->user->id]) }}" target="_blank">{{@$item->user->username}}</a></td>
						
						<td>{{ ucfirst($item->status) }}</span></td>
						<td>{{ date('d F Y',strtotime($item->created_at)) }}</td>
						<td>{{ date('d F Y',strtotime($item->updated_at)) }}</td>

						<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">
											<li><a class="confirm" href="{{ route('catalog_wishlists_status',[$item->id,'deleted']) }}"><i class=" icon-x"></i> Delete</a></li>
										</ul>
								</div>
						</td>				
					</tr>
					@endforeach
				</table>
			</div>
			
		
		<div class="row">
			<div class="col-md-6">
				<?php
					 if(!empty($_GET)){
						foreach ($_GET as $key => $value) {
							$data = $data->appends([$key => $value]);
						}
					 }
				?>
				{!! $data->render() !!}
			</div>
		</div>


<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				window.location.href = $(this).attr('href');
			}
		});

	});
	
</script>